﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Core.Validation
{
    public sealed class AtleastOneAttribute:Attribute
    {
        public int AttributeIndex { get; set; }
        public string AttributeGroup { get; set; }
    }
}
