﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Core.Validation
{
    [AttributeUsage(AttributeTargets.Class)]
    public class AtLeastOneOfTwoAttribute : ValidationAttribute
    {
        public string GroupName { get; set; }
        public override bool IsValid(object value)
        {
            var model = value;
            
            if (model != null)
            {
                var propetiesInfo=model.GetType().GetProperties();
                if (propetiesInfo != null && propetiesInfo.Length >= 2)
                {
                    foreach (var proInfo in propetiesInfo)
                    {
                        var attributes=proInfo.GetCustomAttributes(typeof(AtleastOneAttribute), true);
                        if (attributes != null && attributes.Length > 0)
                        {
                            foreach (var attrib in attributes)
                            {
                                AtleastOneAttribute atleastOneAttrib = attrib as AtleastOneAttribute;
                                if (atleastOneAttrib != null &&
                                                            (
                                                            (!string.IsNullOrEmpty(atleastOneAttrib.AttributeGroup) && atleastOneAttrib.AttributeGroup.Equals(this.GroupName))
                                                            || (string.IsNullOrEmpty(this.GroupName) &&  string.IsNullOrEmpty(atleastOneAttrib.AttributeGroup))
                                                            )
                                    
                                    )
                                {
                                    if (proInfo.GetValue(value) != null)
                                        return true;
                                }

                            }
                        }
                    }
                }
                
            }
            return false;
        }
    }
}
