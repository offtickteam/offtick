﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Core.Cryptography
{
    public interface ISecretStrategy
    {
          bool InitialStrategy(string algorithm);
         string Encrypt(string value);
         string Decrypt(string value);
    }
}
