﻿using Offtick.Core.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Core.Cryptography
{
    public class DesCryptography:ISecretStrategy
    {
        private byte[] key = null;
        private bool isInitialized = false;
        private readonly string NotInitializedErrorKey = "CryptographyNotInitializedErrorKey";
        private readonly string configKey = "CryptographyKey";

        public DesCryptography()
        {
            InitialStrategy();

        }
        public bool InitialStrategy()
        {
            return InitialStrategy(ConfigFactory.GetConfigManager(ConfigManagerType.WebConfig).ReadSetting(configKey));
        }
        public bool InitialStrategy(string algorithm)
        {
            if (string.IsNullOrEmpty(algorithm))
                return false;
            key = ASCIIEncoding.ASCII.GetBytes(algorithm);
            isInitialized = true;
            return true;
        }

        public  string Encrypt(string originalString)
        {
            if (!isInitialized)
                throw new CryptographicException(ConfigFactory.GetConfigManager(ConfigManagerType.WebConfig).ReadSetting(NotInitializedErrorKey));

            if (String.IsNullOrEmpty(originalString))
            {
                throw new ArgumentNullException
                       ("The string which needs to be encrypted can not be null.");
            }
            DESCryptoServiceProvider cryptoProvider = new DESCryptoServiceProvider();
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream,
                cryptoProvider.CreateEncryptor(key, key), CryptoStreamMode.Write);
            StreamWriter writer = new StreamWriter(cryptoStream);
            writer.Write(originalString);
            writer.Flush();
            cryptoStream.FlushFinalBlock();
            writer.Flush();
            return Convert.ToBase64String(memoryStream.GetBuffer(), 0, (int)memoryStream.Length);
        }


        public  string Decrypt(string cryptedString)
        {
            if (String.IsNullOrEmpty(cryptedString))
            {
                throw new ArgumentNullException
                   ("The string which needs to be decrypted can not be null.");
            }
            DESCryptoServiceProvider cryptoProvider = new DESCryptoServiceProvider();
            MemoryStream memoryStream = new MemoryStream
                    (Convert.FromBase64String(cryptedString));
            CryptoStream cryptoStream = new CryptoStream(memoryStream,
                cryptoProvider.CreateDecryptor(key, key), CryptoStreamMode.Read);
            StreamReader reader = new StreamReader(cryptoStream);
            return reader.ReadToEnd();
        }
    }
}
