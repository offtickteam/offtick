﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Core.Utility
{
    public interface IConfigManager
    {

        void WriteSetting(string key, string value);
        string ReadSetting(string key);
        void RemoveSetting(string key);
    }
}
