﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Core.Utility
{

    public class ConfigFactory
    {
        public static IConfigManager GetConfigManager(ConfigManagerType configType)
        {
            switch (configType)
            {
                case ConfigManagerType.WebConfig:
                    return new WebConfigManager();
                case ConfigManagerType.SQLConfig:
                    return null;
            }
            return null;
        }
    }
}
