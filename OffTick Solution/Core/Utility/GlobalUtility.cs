﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Core.Utility
{
    public class GlobalUtility
    {
        public static string GetStringValueOfDate(DateTime? dateTime)
        {
            if (dateTime == null)
                return string.Empty;
            return dateTime.Value.ToString("yyyyMMddHHmmss");
        }

        public static int ConvertGUIDToInt(Guid guid)
        {
            //extract an integer from the beginning of the Guid
            byte[] _bytes = guid.ToByteArray();
            int i = ((int)_bytes[0]) | ((int)_bytes[1] << 8) | ((int)_bytes[2] << 16) | ((int)_bytes[3] << 24);
            return i;
        }

      
    }

}
