﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Mvc.Ajax;

namespace Offtick.Core.Utility
{
    public static class ExtensionHelper
    {
        public static MvcHtmlString PersianCalenderFor<TModel, TValue>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TValue>> expression, string imgSrc)
        {


            string id = ExpressionHelper.GetExpressionText(expression);
            string frameId = id + "_frame";


            var textBoxString = System.Web.Mvc.Html.InputExtensions.TextBoxFor(htmlHelper, expression).ToHtmlString();

            textBoxString = string.Format(textBoxString.Replace("<input", "<input maxlength=\"10\" usedatepicker=\"true\" oncontextmenu=\"ShowDatePicker('{0}', '{1}'); return false;\"   ondblclick=\"this.value ='';\" style=\"width:80px;\" "), id, frameId);

            string tagBlock = "<table style='direction: rtl' cellpadding='0' cellspacing='0'><tr><td style='width: 80px;'>{0}</td>   <td style='width: 5px;'/><td style='width: 15px;'> <img onclick=\"; ShowDatePicker('{1}', '{2}');\" alt='' height='15' width='15' src='{3}'/>   <td style='vertical-align: baseline; width: 5px; direction: rtl;'> <div id='{2}' style='text-align: Rigth; position: absolute; visibility: hidden' /></td></tr></table>";
            tagBlock = string.Format(tagBlock, textBoxString, id, frameId, imgSrc);

            return MvcHtmlString.Create(tagBlock);



        }

        public static MvcHtmlString PersianCalender(this HtmlHelper htmlHelper, string name, object value, string imgSrc)
        {
            string frameId = name + "_frame";
            var textBoxString = System.Web.Mvc.Html.InputExtensions.TextBox(htmlHelper, name, value).ToHtmlString();
            textBoxString = string.Format(textBoxString.Replace("<input", "<input maxlength=\"10\" usedatepicker=\"true\" oncontextmenu=\"ShowDatePicker('{0}', '{1}'); return false;\"   ondblclick=\"this.value ='';\" style=\"width:80px;\" "), name, frameId);
            string tagBlock = "<table style='direction: rtl' cellpadding='0' cellspacing='0'><tr><td style='width: 80px;'>{0}</td>   <td style='width: 5px;'/><td style='width: 15px;'> <img onclick=\"; ShowDatePicker('{1}', '{2}');\" alt='' height='15' width='15' src='{3}'/>   <td style='vertical-align: baseline; width: 5px; direction: rtl;'> <div id='{2}' style='text-align: Rigth; position: absolute; visibility: hidden' /></td></tr></table>";
            tagBlock = string.Format(tagBlock, textBoxString, name, frameId, imgSrc);

            return MvcHtmlString.Create(tagBlock);
        }


        
            public static IHtmlString ImageActionLink(this AjaxHelper helper, string imageUrl, string altText, string actionName, object routeValues, AjaxOptions ajaxOptions, object htmlAttributes = null)
            {
                var builder = new TagBuilder("img");
                builder.MergeAttribute("src", imageUrl);
                builder.MergeAttribute("alt", altText);
                builder.MergeAttributes(new RouteValueDictionary(htmlAttributes));
                var link = helper.ActionLink("[replaceme]", actionName, routeValues, ajaxOptions).ToHtmlString();
                return MvcHtmlString.Create(link.Replace("[replaceme]", builder.ToString(TagRenderMode.SelfClosing)));
            }



        //public static string Ratings(this HtmlHelper helper, PostModel post)
        //{
        //    StringBuilder sb = new StringBuilder();
        //    sb.AppendFormat("<span class='rating' rating='{0}' post='{1}' title='Click to cast vote'>", post.Rating, post.ID);
        //    string formatStr = "<img src='/Content/images/{0}' alt='star' width='5' height='12' class='star' value='{1}' />";
        //    for (Double i = .5; i <= 5.0; i = i + .5)
        //    {
        //        if (i <= post.Rating)
        //        {
        //            sb.AppendFormat(formatStr, (i * 2) % 2 == 1 ? "star-left-on.gif" : "star-right-on.gif", i);
        //        }
        //        else
        //        {
        //            sb.AppendFormat(formatStr, (i * 2) % 2 == 1 ? "star-left-off.gif" : "star-right-off.gif", i);
        //        }
        //    }
        //    sb.AppendFormat(" <span>Currently rated {0} by {1} people</span>", post.Rating, post.Raters);
        //    sb.Append("</span>");
        //    return sb.ToString();
        //}
    }
}
