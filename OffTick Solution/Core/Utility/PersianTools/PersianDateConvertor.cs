﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Core.Utility.PersianTools
{
   
    public static class PersianDateConvertor
    {
        private static PersianCalendar persianCalendar = new PersianCalendar();

        /// <summary>
        /// get a date like 1390-01-16 and return its gregorian date
        /// </summary>
        /// <param name="pStringDate"></param>
        /// <returns></returns>
        public static DateTime ToGregorian(string pStringDate)
        {
            try
            {
                int year = Convert.ToInt16(pStringDate.Substring(0, 4));
                int month = Convert.ToInt16(pStringDate.Substring(5, 2));
                int day = Convert.ToInt16(pStringDate.Substring(8, 2));
                DateTime dtEn = persianCalendar.ToDateTime(year, month, day, 0, 0, 0, 0);
                return dtEn;
            }
            catch
            {
                return DateTime.MinValue;
            }
        }

        /// <summary>
        /// get a date like 1390-01-16 and return its gregorian date
        /// </summary>
        /// <param name="pStringDate"></param>
        /// <returns></returns>
        public static string ToGregorianString(string pStringDate)
        {
            try
            {
                DateTime dtEn = ToGregorian(pStringDate);
               return string.Format("{0}-{1}-{2}", dtEn.Year, dtEn.Month.ToString().PadLeft(2, '0'), dtEn.Day.ToString().PadLeft(2, '0'));
            }
            catch
            {
                DateTime dtEn = DateTime.MinValue;
               return string.Format("{0}-{1}-{2}", dtEn.Year, dtEn.Month.ToString().PadLeft(2, '0'), dtEn.Day.ToString().PadLeft(2, '0'));
                
            }
        }


        /// <summary>
        /// return a persian string such as 1390-01-16
        /// </summary>
        /// <param name="gregorian"></param>
        /// <returns></returns>
        public static string ToKhorshidiDate(DateTime gregorian)
        {
            return string.Format("{0}-{1}-{2}", persianCalendar.GetYear(gregorian).ToString(), persianCalendar.GetMonth(gregorian).ToString().PadLeft(2, '0'), persianCalendar.GetDayOfMonth(gregorian).ToString().PadLeft(2, '0'));
        }

        /// <summary>
        /// return a persian string such as 1390-01-16 12:03:04
        /// </summary>
        /// <param name="gregorian"></param>
        /// <returns></returns>
        public static string ToKhorshidiDateTime(DateTime gregorian)
        {
            return ToKhorshidiDateTime(gregorian, true);
        }
        /// <summary>
        /// return a persian string such as 1390-01-16 12:03:04
        /// </summary>
        /// <param name="gregorian"></param>
        /// <returns></returns>
        public static string ToKhorshidiDateTime(DateTime gregorian,bool containSeconds)
        {
            string format=containSeconds?"{0}-{1}-{2} {3}:{4}:{5}":"{0}-{1}-{2} {3}:{4}";
            return string.Format(format,
                persianCalendar.GetYear(gregorian).ToString(),
                persianCalendar.GetMonth(gregorian).ToString().PadLeft(2, '0'),
                persianCalendar.GetDayOfMonth(gregorian).ToString().PadLeft(2, '0'),
                persianCalendar.GetHour(gregorian).ToString().PadLeft(2, '0'),
                persianCalendar.GetMinute(gregorian).ToString().PadLeft(2, '0'),
                containSeconds?persianCalendar.GetSecond(gregorian).ToString().PadLeft(2, '0'):string.Empty
                );
          
        }
    }
}
