﻿namespace Offtick.Core.Utility.PersianTools
{
    public enum PersianDayOfWeek
    {
        شنبه = 1,
        یکشنبه,
        دوشنبه,
        ﺳﻪشنبه,
        چهارشنبه,
        پنجشنبه,
        جمعه
    }

    public enum PersianMonth
    {
        فروردین = 1,
        اردیبهشت,
        خرداد,
        تیر,
        مرداد,
        شهریور,
        مهر,
        آبان,
        آذر,
        دی,
        بهمن,
        اسفند
    }

    public enum PersianDayOfWeekAbbr
    {
        ش = 1,
        ی,
        د,
        س,
        چ,
        پ,
        ج
    }
}
