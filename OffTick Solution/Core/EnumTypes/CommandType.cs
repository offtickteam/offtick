﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Core.EnumTypes
{
   public enum CommandType
    {
       Add=0,
       Edit=1,
       Delete=2
    }
}
