﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Core.EnumTypes
{
    public enum AddStatus
    {
        Added = 0,
        RejectedByExitBefore = 1,
        RejectedByParentNotExit=2,


        RejectedByInvalidParameter = 6,
        RejectedByNonEffectiveQuery = 7,
        RejectedByInternalSystemError = 8,
        RejectedByAccessDenied = 9,

    }

}
