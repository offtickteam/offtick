﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Core.EnumTypes
{
    public enum TravelTypeEnum :short
    {
        Air=1,
        Land=2,
        Train=3,
        Sea=4,

        None=10,
    }
}
