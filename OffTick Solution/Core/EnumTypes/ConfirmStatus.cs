﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Core.EnumTypes
{
    public enum ConfirmStatus
    {
        NotDecide=1,
        Confirmed=2,
        NotConfirmed=3
    }
}
