﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Core.EnumTypes
{
    public enum EditStatus
    {
        Edited = 0,
        RejectedByNotExit = 1,

        RejectedByNonEffectiveQuery = 7,
        RejectedByInternalSystemError = 8,
        RejectedByAccessDenied = 9
    }
}
