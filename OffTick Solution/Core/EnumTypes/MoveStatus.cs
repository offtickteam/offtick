﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Core.EnumTypes
{
    public enum MoveStatus
    {
        Moved=0,
        RejectedByNotExitSource=1,
        RejectedByNotExitDestination=2,
        RejectedByIsNotParentDestination=3,


        RejectedByNonEffectiveQuery=7,
        RejectedByInternalSystemError=8,
        RejectedByAccessDenied=9,
        
    }
}
