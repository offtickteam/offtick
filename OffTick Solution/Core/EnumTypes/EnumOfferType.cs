﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Core.EnumTypes
{
    public enum EnumOfferType
    {
        Snap=1
        ,Gallery=2
        ,Advertisement=3
        ,Sale=4
        ,Post=5
        ,Food=6
        ,Video=7
        ,Tour=8
    }
}
