﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Core.EnumTypes
{
    public enum RoleType
    {
        Personal=1,
        Company=2,
        Doctor=4,
        ReportUser=8,
        Resturant=9,
        Administrator = 64,
        Tester = 128,
        Developer = 256
    }
}
