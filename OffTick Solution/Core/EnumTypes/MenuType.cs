﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Core.EnumTypes
{
    public enum MenuType :short
    {
        ParentMenu = 0,
        ChildMenu = 1,
        DirectLink = 2
    }
}
