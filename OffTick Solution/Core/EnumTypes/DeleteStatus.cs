﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Core.EnumTypes
{
    public enum DeleteStatus
    {
        Deleted = 0,
        RejectedByExistSubItems = 1,
        RejectedByNotExit = 2,
        DeletedBeforeDeliver = 3,
        DeletedBeforeRead = 4,

        RejectedByInvalidParameter = 6,
        RejectedByNonEffectiveQuery = 7,
        RejectedByInternalSystemError = 8,
        RejectedByAccessDenied = 9
    }
}
