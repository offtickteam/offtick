﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Core.EnumTypes
{
    public  enum  FollowStatus
    {
        none=0,
        Requested=1,
        Follow=2,
        Rejected=3,
    }
}
