﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Core.Exceptions
{
     class ReplaceExpressionVisitor
        : ExpressionVisitor
    {
        private readonly Expression _oldValue;
        private readonly Expression _newValue;

        public ReplaceExpressionVisitor(Expression oldValue, Expression newValue)
        {
            _oldValue = oldValue;
            _newValue = newValue;
        }

        public override Expression Visit(Expression node)
        {
            if (node == _oldValue)
                return _newValue;
            return base.Visit(node);
        }
    }
    public static class ExpressionExtensions
    {
        public static Expression<Func<T, bool>> And<T>(this Expression<Func<T, bool>> leftExpression, Expression<Func<T, bool>> rightExpression)
        {
            if (leftExpression == null) return rightExpression;
            if (rightExpression == null) return leftExpression;
            var paramExpr = Expression.Parameter(typeof(T));
            //var exprBody = Expression.And(leftExpression.Body, rightExpression.Body);
           // exprBody = (BinaryExpression)new ParameterReplacer(paramExpr).Visit(exprBody);
           // return Expression.Lambda<Func<T, bool>>(exprBody, paramExpr);


            var leftVisitor = new ReplaceExpressionVisitor(leftExpression.Parameters[0], paramExpr);
            var left = leftVisitor.Visit(leftExpression.Body);

            var rightVisitor = new ReplaceExpressionVisitor(rightExpression.Parameters[0], paramExpr);
            var right = rightVisitor.Visit(rightExpression.Body);
            return Expression.Lambda<Func<T, bool>>(
            Expression.And(left, right), paramExpr);

            
        }

        public static Expression<Func<T, bool>> Or<T>(this Expression<Func<T, bool>> leftExpression, Expression<Func<T, bool>> rightExpression)
        {
            if (leftExpression == null) return rightExpression;
            if (rightExpression == null) return leftExpression;
            var paramExpr = Expression.Parameter(typeof(T));
           // var exprBody = Expression.Or(leftExpression.Body, rightExpression.Body);
          //  exprBody = (BinaryExpression)new ParameterReplacer(paramExpr).Visit(exprBody);
           // return Expression.Lambda<Func<T, bool>>(exprBody, paramExpr);

            var leftVisitor = new ReplaceExpressionVisitor(leftExpression.Parameters[0], paramExpr);
            var left = leftVisitor.Visit(leftExpression.Body);

            var rightVisitor = new ReplaceExpressionVisitor(rightExpression.Parameters[0], paramExpr);
            var right = rightVisitor.Visit(rightExpression.Body);
            return Expression.Lambda<Func<T, bool>>(
            Expression.Or(left, right), paramExpr);
        }
    }

    class ParameterReplacer : ExpressionVisitor
    {
        private readonly ParameterExpression _parameter;

        protected override Expression VisitParameter(ParameterExpression node)
        {
            return base.VisitParameter(_parameter);
        }

        internal ParameterReplacer(ParameterExpression parameter)
        {
            _parameter = parameter;
        }
    }

}
