﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Core.Exceptions
{
    public class ExceptionBase:Exception
    {
        public DateTime SnapshotDateTime;
        public string UserName;
        public string MadouleName;
        public ExceptionLevel Level;
        
        public ExceptionBase(string msg)
            : base(msg)
        {
            this.SnapshotDateTime = DateTime.Now;
            UserName = string.Empty;
            MadouleName = string.Empty;
            Level = ExceptionLevel.Abuse;
        }
        public string SnapshotDateTimeToString()
        {
            return this.SnapshotDateTime.ToString("yyyyMMddHHmmss");
        }

    }
}
