﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Offtick.Business.SmsService
{
    public class MagfaSmsHttpHandler : MagfaSmsHandlerBase, IMagfaSmsHandler
    {
        private String END_POINT_URL;
        private String SENDER_NUMBER;

        private static readonly String ENQUEUE_METHOD_CALL = "enqueue";
        private static readonly String CREDIT_METHOD_CALL = "getCredit";
        private static readonly String REAL_MESSAGE_METHOD_CALL = "getRealMessageStatus";
        private static readonly String MESSAGE_STATUS_METHOD_CALL = "getMessageStatus";
        private static readonly String MESSAGE_METHOD_CALL = "getMessageId";


        public MagfaSmsHttpHandler(string magfaEndPoint,string senderNumber, String username, String password, String domain)
            : base(username, password, domain)
        {
            END_POINT_URL = magfaEndPoint;
            SENDER_NUMBER = senderNumber;
        }

        public long enqueue( String recipientNumber, String text, String udh, String encoding, long checkingMessageId)
        {
            HttpRequestHandler requestHandler = new HttpRequestHandler();
            //making the url string
            StringBuilder sb = new StringBuilder(END_POINT_URL);
            sb.Append("service=").Append(ENQUEUE_METHOD_CALL).Append("&");
            sb.Append("username=").Append(username).Append("&");
            sb.Append("password=").Append(password).Append("&");
            sb.Append("from=").Append(SENDER_NUMBER).Append("&");
            sb.Append("to=").Append(recipientNumber).Append("&");
            sb.Append("domain=").Append(domain).Append("&");
            sb.Append("message=").Append(HttpUtility.UrlEncode(text)).Append("&");
            sb.Append("udh=").Append(udh).Append("&");
            sb.Append("coding=").Append(encoding).Append("&");
            sb.Append("chkmessageid=").Append(checkingMessageId);
            String urlString = sb.ToString();

            String result = requestHandler.get(urlString);
            return long.Parse(result);
        }
        public long enqueue( String recipientNumber, String text, String encoding)
        {
            HttpRequestHandler requestHandler = new HttpRequestHandler();
            //making the url string
            StringBuilder sb = new StringBuilder(END_POINT_URL);
            sb.Append("service=").Append(ENQUEUE_METHOD_CALL).Append("&");
            sb.Append("username=").Append(username).Append("&");
            sb.Append("password=").Append(password).Append("&");
            sb.Append("from=").Append(SENDER_NUMBER).Append("&");
            sb.Append("to=").Append(recipientNumber).Append("&");
            sb.Append("domain=").Append(domain).Append("&");
            sb.Append("message=").Append(HttpUtility.UrlEncode(text)).Append("&");
            //sb.Append("coding=").Append(encoding).Append("&");
            String urlString = sb.ToString();

            String result = requestHandler.get(urlString);
            return long.Parse(result);
        }

        public Single getCredit()
        {
            HttpRequestHandler requestHandler = new HttpRequestHandler();

            //making the url string
            StringBuilder sb = new StringBuilder(END_POINT_URL);
            sb.Append("service=").Append(CREDIT_METHOD_CALL).Append("&");
            sb.Append("username=").Append(username).Append("&");
            sb.Append("password=").Append(password).Append("&");
            sb.Append("domain=").Append(domain);
            String urlString = sb.ToString();

            return Single.Parse(requestHandler.get(urlString));
        }

        public int getRealMessageStatus(long messageId)
        {
            HttpRequestHandler requestHandler = new HttpRequestHandler();

            //making the url string
            StringBuilder sb = new StringBuilder(END_POINT_URL);
            sb.Append("service=").Append(REAL_MESSAGE_METHOD_CALL).Append("&");
            sb.Append("username=").Append(username).Append("&");
            sb.Append("password=").Append(password).Append("&");
            sb.Append("messageId").Append(messageId);
            String urlString = sb.ToString();

            return int.Parse(requestHandler.get(urlString));
        }

        public int getMessageStatus(long messageId)
        {
            HttpRequestHandler requestHandler = new HttpRequestHandler();

            //making the url string
            StringBuilder sb = new StringBuilder(END_POINT_URL);
            sb.Append("service=").Append(MESSAGE_STATUS_METHOD_CALL).Append("&");
            sb.Append("username=").Append(username).Append("&");
            sb.Append("password=").Append(password).Append("&");
            sb.Append("messageId").Append(messageId);
            String urlString = sb.ToString();

            return int.Parse(requestHandler.get(urlString));
        }

        public long getMessageIdSample(long checkingMessageId)
        {
            HttpRequestHandler requestHandler = new HttpRequestHandler();

            //making the url string
            StringBuilder sb = new StringBuilder(END_POINT_URL);
            sb.Append("service=").Append(MESSAGE_METHOD_CALL).Append("&");
            sb.Append("username=").Append(username).Append("&");
            sb.Append("password=").Append(password).Append("&");
            sb.Append("chkmessageid=").Append(checkingMessageId);
            String urlString = sb.ToString();

            return long.Parse(requestHandler.get(urlString));
        }
    }
}
