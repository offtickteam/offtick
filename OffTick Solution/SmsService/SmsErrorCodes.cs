﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.SmsService
{
    public static class SmsErrorCodes
    {
        public static string GetErrorCode(long errorCode)
        {
            
            string propertyName="SmsCode"+errorCode.ToString();
             Type type = typeof(SmsMessages);
             var propertyInfo=type.GetProperty(propertyName);
             if (propertyInfo != null)
                 return (string)propertyInfo.GetValue(null, null);
             else

                 return "Undifined";
            
        }
    }
}
