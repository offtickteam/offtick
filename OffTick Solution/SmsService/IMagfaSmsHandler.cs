﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.SmsService
{
    public interface IMagfaSmsHandler
    {
        long enqueue(String recipientNumber, String text, String udh, String encoding, long checkingMessageId);
        long enqueue(String recipientNumber, String text, String encoding);
    }
}
