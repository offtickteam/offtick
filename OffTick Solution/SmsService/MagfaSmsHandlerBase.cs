﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.SmsService
{
    public abstract class MagfaSmsHandlerBase
    {
        protected readonly String username;
        protected readonly String password;
        protected readonly String domain;

        public MagfaSmsHandlerBase(String username, String password, String domain){
            this.username = username;
            this.password = password;
            this.domain = domain;
        }
    }
}
