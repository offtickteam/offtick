//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Offtick.Data.Context.ExpertOnlinerContexts
{
    using System;
    using System.Collections.Generic;
    
    public partial class QuestionAnswer
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public QuestionAnswer()
        {
            this.QuestionAnswerAttachments = new HashSet<QuestionAnswerAttachment>();
            this.QuestionAnswerLikes = new HashSet<QuestionAnswerLike>();
        }
    
        public System.Guid AnswerId { get; set; }
        public System.Guid QuestionId { get; set; }
        public System.Guid UserId { get; set; }
        public System.DateTime DateTime { get; set; }
        public string Body { get; set; }
    
        public virtual Question Question { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<QuestionAnswerAttachment> QuestionAnswerAttachments { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<QuestionAnswerLike> QuestionAnswerLikes { get; set; }
    }
}
