//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Offtick.Data.Context.ExpertOnlinerContexts
{
    using System;
    using System.Collections.Generic;
    
    public partial class Application
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Application()
        {
            this.Application1 = new HashSet<Application>();
            this.ApplicationHollands = new HashSet<ApplicationHolland>();
            this.ApplicationHollandResults = new HashSet<ApplicationHollandResult>();
            this.ApplicationMajorChoiceResults = new HashSet<ApplicationMajorChoiceResult>();
            this.ApplicationMBTIs = new HashSet<ApplicationMBTI>();
            this.ApplicationMBTIResults = new HashSet<ApplicationMBTIResult>();
        }
    
        public long ApplicationID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public Nullable<System.Guid> OwnerUserId { get; set; }
        public bool IsActive { get; set; }
        public Nullable<short> PresentationOrder { get; set; }
        public Nullable<long> ParentApplicationID { get; set; }
        public string Symbol { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Application> Application1 { get; set; }
        public virtual Application Application2 { get; set; }
        public virtual MembershipUser MembershipUser { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ApplicationHolland> ApplicationHollands { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ApplicationHollandResult> ApplicationHollandResults { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ApplicationMajorChoiceResult> ApplicationMajorChoiceResults { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ApplicationMBTI> ApplicationMBTIs { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ApplicationMBTIResult> ApplicationMBTIResults { get; set; }
    }
}
