//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Offtick.Data.Context.ExpertOnlinerContexts
{
    using System;
    using System.Collections.Generic;
    
    public partial class QuestionAnswerAttachment
    {
        public System.Guid AttachmentId { get; set; }
        public System.Guid AnswerId { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public string MimeType { get; set; }
        public string Description { get; set; }
        public System.DateTime DateTime { get; set; }
    
        public virtual QuestionAnswer QuestionAnswer { get; set; }
    }
}
