//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Offtick.Data.Context.ExpertOnlinerContexts
{
    using System;
    using System.Collections.Generic;
    
    public partial class OfferPrivacy
    {
        public System.Guid OfferGeneralPrivacyId { get; set; }
        public bool IsViewerVisible { get; set; }
        public bool IsBuyerVisible { get; set; }
        public System.DateTime LastDatetime { get; set; }
        public bool IsOnlyVisibleForFollowers { get; set; }
    
        public virtual Offer Offer { get; set; }
    }
}
