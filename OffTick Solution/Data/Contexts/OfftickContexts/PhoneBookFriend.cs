//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Offtick.Data.Context.ExpertOnlinerContexts
{
    using System;
    using System.Collections.Generic;
    
    public partial class PhoneBookFriend
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PhoneBookFriend()
        {
            this.PhoneBookFriendEmails = new HashSet<PhoneBookFriendEmail>();
            this.PhoneBookFriendPhones = new HashSet<PhoneBookFriendPhone>();
        }
    
        public System.Guid PhoneBookFriendId { get; set; }
        public System.Guid UserId { get; set; }
        public string Name { get; set; }
        public string Family { get; set; }
        public string Company { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PhoneBookFriendEmail> PhoneBookFriendEmails { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PhoneBookFriendPhone> PhoneBookFriendPhones { get; set; }
    }
}
