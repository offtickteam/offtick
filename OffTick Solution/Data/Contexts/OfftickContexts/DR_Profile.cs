//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Offtick.Data.Context.ExpertOnlinerContexts
{
    using System;
    using System.Collections.Generic;
    
    public partial class DR_Profile
    {
        public long Serial { get; set; }
        public System.Guid UserId { get; set; }
        public string RegistrationNumber { get; set; }
        public string OfficeNumber { get; set; }
        public string OfficeAddress { get; set; }
        public string OfficeLisencePicture { get; set; }
    
        public virtual MembershipUser MembershipUser { get; set; }
    }
}
