//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Offtick.Data.Context.ExpertOnlinerContexts
{
    using System;
    using System.Collections.Generic;
    
    public partial class Gallery
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Gallery()
        {
            this.Gallery1 = new HashSet<Gallery>();
            this.GalleryImages = new HashSet<GalleryImage>();
            this.GalleryOffers = new HashSet<GalleryOffer>();
            this.GallerySliders = new HashSet<GallerySlider>();
        }
    
        public System.Guid GalleryId { get; set; }
        public System.Guid UserId { get; set; }
        public Nullable<System.Guid> ParentGaleryId { get; set; }
        public System.DateTime CreationDateTime { get; set; }
        public System.DateTime UpdateDateTime { get; set; }
        public Nullable<bool> IsDefault { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Gallery> Gallery1 { get; set; }
        public virtual Gallery Gallery2 { get; set; }
        public virtual MembershipUser MembershipUser { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<GalleryImage> GalleryImages { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<GalleryOffer> GalleryOffers { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<GallerySlider> GallerySliders { get; set; }
    }
}
