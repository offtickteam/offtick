﻿using Offtick.Data.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Data.Entities.MembershipEntities
{
    public class MembershipUserInfo:EntityBase
    {
        public MembershipUserInfo(string userName,string userId,string name,string family)
            : base(ObjectState.Persited)
        {
        }

        public string UserName { get; private set; }
        public string UserId { get; private set; }
        public string Name { get; private set; }
        public string Family { get; private set; }
    }
}
