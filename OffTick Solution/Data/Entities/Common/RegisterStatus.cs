﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Data.Entities.Common
{
    public enum RegisterStatus
    {
        ExitUserName=0
        ,ExitEmail
        ,InvalidRoleName
        ,InnerSystemFail
        ,Success
    }
}
