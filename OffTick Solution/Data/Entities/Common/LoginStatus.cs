﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Data.Entities.Common
{
    public enum LoginStatus
    {
         Success=0
        ,InvalidUserName=1
        ,InvalidPassword=2
        ,PreActiveUser=3
        ,InactiveUser=4
        ,BlockUser=5
        ,DisabledUser=6
    }
}
