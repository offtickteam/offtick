﻿using Offtick.Core.EnumTypes;
using Offtick.Data.Context.ExpertOnlinerContexts;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Data.Entities.Common
{
    public class BusinessObjectBase<T1,T2,T3>
        where T1 : ExpertOnlinerFoundationEntities
        where T2: DbSet<T3>
        where T3:class
    {
        private T1 context;
        private T2 dbSet;
        public BusinessObjectBase(T1 context, T2 dbSet)
        {
            this.context = context;
            this.dbSet = dbSet;

        }
        public T3 GetObject(Guid objId)
        {
            return default(T3);
        }
    }
}
