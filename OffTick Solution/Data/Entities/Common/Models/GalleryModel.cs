﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Data.Entities.Common.Models
{
    public class GalleryModel
    {
        public System.Guid GalleryId { get; set; }
        public System.Guid UserId { get; set; }
        public string Title { get; set; }
        public Nullable<System.Guid> ParentGaleryId { get; set; }
        public System.DateTime CreationDateTime { get; set; }
        public System.DateTime UpdateDateTime { get; set; }
        public string Description { get; set; }
        public Nullable<bool> IsDefault { get; set; }
        public short LangaugeId { get; set; }
    }
}
