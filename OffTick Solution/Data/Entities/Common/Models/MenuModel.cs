﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Data.Entities.Common.Models
{
    public class MenuModel
    {
        public System.Guid MenuId { get; set; }
        public System.Guid UserId { get; set; }
        public Nullable<System.Guid> ParentId { get; set; }
        public string Title { get; set; }
        public System.DateTime LastDateTime { get; set; }
        public short MenuType { get; set; }
        public string Url { get; set; }
        public bool IsSystemMenu { get; set; }
        public short MenuOrder { get; set; }
        public Nullable<short> SystemicId { get; set; }
        public string IconUrl { get; set; }
        public short LangaugeId { get; set; }

    }
}
