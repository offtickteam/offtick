﻿using Offtick.Core.EnumTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Data.Entities.Common.Models.Chat
{
    public class ChatUserModel
    {

        public string firstName { get; set; }
        public string lastName { get; set; }
        public string userName { get; set; }
        public string imageThumbnail { get; set; }
        public FollowStatus fallowStatus { get; set; }
    }
}
