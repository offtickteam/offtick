﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Data.Entities.Common.Models
{
    public class MenuContentModel
    {
        public System.Guid ChildId { get; set; }
        public System.Guid MenuId { get; set; }
        public System.DateTime LastDateTime { get; set; }
        public string Body { get; set; }
        public short LangaugeId { get; set; }

    }
}
