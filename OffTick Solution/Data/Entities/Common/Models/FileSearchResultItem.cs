﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Data.Entities.Common.Models
{
    public class FileSearchResultItem
    {
        public FileSearchResultItem(string mime, bool isImage, bool hasThumbnail, string thumbnailAddress, string fullAddress)
        {
            this.Mime = mime;
            this.IsImage = IsImage;
            this.HasThumbnailIcon = hasThumbnail;
            this.ThumbnailAddress = !string.IsNullOrEmpty(thumbnailAddress) ? thumbnailAddress.Replace("/", @"\") : thumbnailAddress;
            this.FullAddress = !string.IsNullOrEmpty(fullAddress) ? fullAddress.Replace("/", @"\") : fullAddress; ;
        }
        public string Mime { get; private set; }
        public bool IsImage { get; private set; }
        public bool HasThumbnailIcon { get; private set; }
        public string ThumbnailAddress { get; private set; }
        public string FullAddress { get; private set; }
    }
}
