﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Data.Entities.Common
{
    public enum ActionResponseFromServerToClientResultStatusEnum
    {
        Success,
        Failed,
        NoResponse,
        TimeOut,
        ExpiredSession,
    }


    public class ActionResponseFromServerToClient
    {
        public static string NearbySearchResultServerToClientKey = "NearbySearchResultServerToClient";
        public static string FollowSearchResultServerToClientKey = "FollowSearchResultServerToClient";

        public static string UserEntitiesServerToClientKey = "UserEntitiesServerToClient";
        public static string UserEntityServerToClientKey = "UserEntityServerToClient";
        public static string EmptyObjectServerToClientKey = "EmptyObjectServerToClient";
        public static string ProfileInfoServerToClientKey = "ProfileInfoServerToClient";
        public static string ProfileOffersServerToClientKey = "ProfileOffersServerToClient";
        public static string FollowCountsServerToClientKey = "FollowCountsServerToClient";
        public static string OfferCommentsServerToClientKey = "OfferCommentsServerToClient";
        public static string OfferLikesServerToClientKey = "OfferLikesServerToClient";
        public static string SearchUserDTOServerToClientKey = "SearchUserDTOServerToClient";
        

        //added time: 1396.03.11
        public static string OffersServerToClientKey = "OffersServerToClient";


        public static string ChatMembersClientKey = "ChatMembersClientKey";

        public ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum resultStatus, string error, string message, object dataObject, string dataObjectType)
        {
            switch (resultStatus)
            {
                case ActionResponseFromServerToClientResultStatusEnum.Success:
                    this.ResultStatus = "Success";
                    break;
                case ActionResponseFromServerToClientResultStatusEnum.Failed:
                    this.ResultStatus = "Failed";
                    break;
                case ActionResponseFromServerToClientResultStatusEnum.NoResponse:
                    this.ResultStatus = "NoResponse";
                    break;
                case ActionResponseFromServerToClientResultStatusEnum.TimeOut:
                    this.ResultStatus = "TimeOut";
                    break;
                case ActionResponseFromServerToClientResultStatusEnum.ExpiredSession:
                    this.ResultStatus = "ExpiredSession";
                    break;
            }
            this.Error = error;
            this.Message = message;
            this.DataObject = dataObject;
            this.DataObjectType = dataObjectType;

        }


        public string ResultStatus { get; private set; }
        public string Error { get; private set; }
        public string Message { get; private set; }
        //public string DataObject { get; private set; }
        public object DataObject { get; private set; }
        public string DataObjectType { get; private set; }
    }
}
