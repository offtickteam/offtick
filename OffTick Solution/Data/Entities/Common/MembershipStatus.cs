﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Data.Entities.Common
{
    public enum MembershipStatus
    {
        PreActive=1,
        Active=2,
        InActive=3,
        Block=4,
        Disabled=5
    }
}
