﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Data.Entities.Common
{
    [Serializable()]
    public abstract class EntityBase
    {
        public EntityBase()
            : this(ObjectState.Created)
        {
        }

        public EntityBase(ObjectState state)
        {
            this.ObjectID = new Guid();
            this.TimeSpan = new TimeSpan();
            this.State = state;
            this.PropertyChanged += new Action(() =>
            {
                TimeSpan = new TimeSpan();
                if (this.State == ObjectState.Persited)
                {
                    this.State = ObjectState.Changed;
                }
            });

            this.EntityPersisting += new Action(() =>
            {
                this.State = ObjectState.Persited;
            });
        }

        public void PersistEntity()
        {
            onEntityPersisting();
        }





        private void onEntityPersisting()
        {
            if (this.EntityPersisting != null)
                EntityPersisting();
        }

        public Guid ObjectID { get; private set; }
        public TimeSpan TimeSpan { get; private set; }
        public ObjectState State { get; private set; }


        public Action PropertyChanged;
        public Action EntityPersisting;
    }
}
