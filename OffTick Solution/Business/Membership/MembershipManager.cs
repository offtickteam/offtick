﻿using Offtick.Business.Services.Storage;
using Offtick.Business.Services.Storage.File;
using Offtick.Core.Cryptography;
using Offtick.Core.Utility;
using Offtick.Data.Context.ExpertOnlinerContexts;
using Offtick.Data.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Offtick.Core.EnumTypes;
using System.Linq.Expressions;
using Offtick.Core.Exceptions;
using Offtick.Business.Services.Managers;
using Offtick.Core.Utility.PersianTools;
using System.Runtime.InteropServices.WindowsRuntime;

namespace Offtick.Business.Membership
{
    public class MembershipManager:BaseManager
    {
        private static MembershipManager instance;
        public static MembershipManager Instance
        {
            get
            {
                if (instance != null)
                    return instance;
                lock (typeof(MembershipManager))
                {
                    if (instance == null)
                        instance= new MembershipManager();
                    return instance;
                }
            }
        }

        private MembershipManager()
        {
            this.secretStrategy = new DesCryptography();
            
        }

        public string getDecryptPassword(string userName)
        {
            var db = DataContextManager.GetPersitantStorage();
             var user=db.MembershipUsers.FirstOrDefault(e => e.UserName.Equals(userName));
            if (user == null)
                return string.Empty;
            return secretStrategy.Decrypt(user.Password);
        }


        public LoginStatus LoginMember(string userName, string password)
        {
            var db = DataContextManager.GetPersitantStorage();
            var user=db.MembershipUsers.FirstOrDefault(e => e.UserName.Equals(userName));

           
           
            //invalid username and password
            if (user == null)
                return LoginStatus.InvalidUserName;
            if (user != null && !secretStrategy.Decrypt(user.Password).Equals(password))
                return LoginStatus.InvalidPassword;


            //check user status
            var userStatus=user.MembershipUserStatus.FirstOrDefault();
            if (userStatus == null)
                return LoginStatus.PreActiveUser;

            LoginStatus loginStatus = LoginStatus.BlockUser ;
            if (userStatus != null && userStatus.MembershipStatu!=null)
            {
                switch (userStatus.MembershipStatu.StatusModel)
                {
                    case "PreActive":
                        loginStatus = LoginStatus.PreActiveUser;
                        break;
                    case "Active":
                        loginStatus = LoginStatus.Success;
                        break;
                    case "InActive":
                        loginStatus = LoginStatus.InactiveUser;
                        break;
                    case "Block":
                        loginStatus = LoginStatus.BlockUser;
                        break;
                    case "Disabled":
                        loginStatus = LoginStatus.DisabledUser;
                        break;

                    default:
                        loginStatus = LoginStatus.BlockUser;
                        break;
                }
            }

            return loginStatus;
            
        }

        public RegisterStatus InitialRegisterMember(string userName, string password, string firstName, string lastName, string gender, DateTime? birthDate, string email, string domainUrl, string address, string roleName)
        {
            return InitialRegisterMember(userName, password, firstName, lastName, gender, birthDate, email, domainUrl, address, roleName, string.Empty,string.Empty);
        }
        public RegisterStatus InitialRegisterMember(string userName, string password, string firstName, string lastName, string gender, DateTime? birthDate, string email, string domainUrl, string address, string roleName, string phoneNumber, HttpPostedFileBase picture, string userId = "-")
        {
            string newFileName = string.Empty;
            Guid guidUserId = "-".Equals(userId) ? Guid.NewGuid() : Guid.Parse(userId);
            userId = guidUserId.ToString();
            if (picture != null)
            {
                ImageFileManager ifm = new ImageFileManager(userId);
                string fileName = picture.FileName;
                newFileName = ifm.Save(picture, fileName);
            }
            return InitialRegisterMember(userName, password, firstName, lastName, gender, birthDate, email, domainUrl, address, roleName, phoneNumber, newFileName,userId);
        }

        public RegisterStatus InitialRegisterMember(string userName, string password, string firstName, string lastName, string gender, DateTime? birthDate, string email, string domainUrl, string address, string roleName, string phoneNumber, string pictureFileName,string userId="-")
        {
            Guid userGuid = Guid.NewGuid();
            if (ExistUser(userName))
                return RegisterStatus.ExitUserName;

            MembershipRole role = FindRole(roleName);
            if (role == null)
                return RegisterStatus.InvalidRoleName;

            //MembershipStatu status = GetMemberStatus("PreActive");
            MembershipStatu status = GetMemberStatus("Active");
            MembershipUserStatu membershipStatus = new MembershipUserStatu()
            {
                UserStatusId = Guid.NewGuid(),
                MembershipStatu = status,
                DateTime = DateTime.Now

            };

            MembershipUsersInRole memberRole = new MembershipUsersInRole()
            {

                UsersRolesId =  Guid.NewGuid(),
                CreatedBy = GetSystemGuid(),
                CreationTime = DateTime.Now,
                MembershipRole = role,
            };




            
            MembershipUser user = new MembershipUser()
            {
                UserId ="-".Equals(userId)? Guid.NewGuid():Guid.Parse(userId),
                UserName = userName,
                BirthDate = birthDate,
                Email = email,
                FirstName = firstName,
                LastName = lastName,
                Gender = gender,
                Password = secretStrategy.Encrypt(password),
                DomainUrl = domainUrl,
                Address = address,
                PhoneNumber = phoneNumber,
                RegisterDateTime=DateTime.Now,
            };

            user.MembershipUsersInRoles.Add(memberRole);
            user.MembershipUserStatus.Add(membershipStatus);

            var allLangs =Offtick.Business.Services.Managers.LangaugeManager.Instance.GetLangauges();
            foreach (var lang in allLangs)
            {
                user.MembershipLangauges.Add(new MembershipLangauge()
                {
                    ConfigurationId = Guid.NewGuid(),
                    LanguageId = lang.LanguageId,
                    IsDefault = lang.Caluture.Equals("fa-IR"),
                });
            }

            var db = DataContextManager.GetPersitantStorage();
            try
            {
                if (!string.IsNullOrEmpty(pictureFileName))
                {
                    user.Picture = pictureFileName;
                }
                db.MembershipUsers.Add(user);

                db.SaveChanges();

                SecurityManager.Instance.UpdateGeneralSecurityForUser(user, true, true,true,true);


                //  Offtick.Business.Services.Managers.MenuBuilder.Instance.AddMenu(null, 1, "تماس با ما", Core.EnumTypes.MenuType.ChildMenu, string.Empty, "icon-headphones-3", true, "ContactUs", userName);
                return RegisterStatus.Success;
            }
            catch
            {
                return RegisterStatus.InnerSystemFail;
            }
            finally
            {
                // DataContextManager.FreeStorage();
            }


        }


        public AddStatus AddOrUpdateDoctorProfile(Guid userId, string RegistrationNumber, string OfficeNumber, string OfficeAddress, HttpPostedFileBase lisence)
        {
            string NewFileName = string.Empty;
            if (lisence != null)
            {
                ImageFileManager imgFileManager = new ImageFileManager(userId.ToString());
                NewFileName = imgFileManager.Save(lisence, lisence.FileName);
            }

            return AddOrUpdateDoctorProfile(userId, RegistrationNumber, OfficeNumber, OfficeAddress, NewFileName);
        }

        public AddStatus AddOrUpdateDoctorProfile(Guid userId, string RegistrationNumber, string OfficeNumber, string OfficeAddress, string lisenceFileName)
        {
            var user = GetUser(userId);
            if (user == null)
                return AddStatus.RejectedByParentNotExit;

            var drProfile = user.DR_Profile.FirstOrDefault();
            if (drProfile == null)
            {
                drProfile = new DR_Profile();
                user.DR_Profile.Add(drProfile);
            }
            drProfile.RegistrationNumber = RegistrationNumber;
            drProfile.OfficeNumber = OfficeNumber;
            drProfile.OfficeAddress = OfficeAddress;
            if (!string.IsNullOrEmpty(lisenceFileName))
            {
                drProfile.OfficeLisencePicture = lisenceFileName;
            }
            int effected = DataContextManager.GetPersitantStorage().SaveChanges();
            if (effected > 0)
                return AddStatus.Added;
            else
                return AddStatus.RejectedByNonEffectiveQuery;
        }


        public string GetCurrentUserName()
        {
            return System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.User != null ? System.Web.HttpContext.Current.User.Identity.Name : string.Empty;
        }

        public MembershipUser GetCurrentUser()
        {
            
            string userName =System.Web.HttpContext.Current!=null && System.Web.HttpContext.Current.User!=null? System.Web.HttpContext.Current.User.Identity.Name:string.Empty;
            return GetUser(userName);

            
        }

        public MembershipUser GetUser(string userName)
        {
            MembershipUser user = null;
            if (!string.IsNullOrEmpty(userName))
            {
                Offtick.Data.Context.ExpertOnlinerContexts.ExpertOnlinerFoundationEntities db = DataContextManager.GetPersitantStorage();
                user = db.MembershipUsers.FirstOrDefault(e => e.UserName.ToLower().Equals(userName.ToLower()));
            }
            return user;
            
        }
        public MembershipUser GetUserByEmail(string email)
        {
            MembershipUser user = null;
            if (!string.IsNullOrEmpty(email))
            {
                Offtick.Data.Context.ExpertOnlinerContexts.ExpertOnlinerFoundationEntities db = DataContextManager.GetPersitantStorage();
                user = db.MembershipUsers.FirstOrDefault(e => e.Email.ToLower().Equals(email.ToLower()));
            }
            return user;

        }
        public MembershipUser GetUserByPhoneNumber(string phoneNumber)
        {
            MembershipUser user = null;
            if (!string.IsNullOrEmpty(phoneNumber))
            {
                Offtick.Data.Context.ExpertOnlinerContexts.ExpertOnlinerFoundationEntities db = DataContextManager.GetPersitantStorage();
                user = db.MembershipUsers.FirstOrDefault(e => e.PhoneNumber.ToLower().Equals(phoneNumber.ToLower()));
            }
            return user;

        }

        public IList<MembershipUser> GetAllUsers(int take,int skip)
        {
                Offtick.Data.Context.ExpertOnlinerContexts.ExpertOnlinerFoundationEntities db = DataContextManager.GetPersitantStorage();
                return db.MembershipUsers.OrderByDescending(e=>e.MembershipUserStatus.FirstOrDefault().DateTime).Skip(skip).Take(take).ToList();
        }

        public IQueryable<MembershipUser> GetAllUsers()
        {
            Offtick.Data.Context.ExpertOnlinerContexts.ExpertOnlinerFoundationEntities db = DataContextManager.GetPersitantStorage();
            return db.MembershipUsers.AsQueryable();
        }

        public IList<MembershipUser> GetUsersByCriteria(string[] searchCriterias,int take, int skip)
        {
            Offtick.Data.Context.ExpertOnlinerContexts.ExpertOnlinerFoundationEntities db = DataContextManager.GetPersitantStorage();
            Expression<Func<MembershipUser, bool>> ex = null;
            foreach (var criteria in searchCriterias)
                ex = ex.Or(e => e.FirstName.Contains(criteria) 
                    || e.LastName.Contains(criteria)
                    || e.Email.Contains(criteria)
                    || e.UserName.Contains(criteria));
            return db.MembershipUsers.Where(ex).OrderByDescending(e => e.MembershipUserStatus.FirstOrDefault().DateTime).Skip(skip).Take(take).ToList();
        }

        public IQueryable<MembershipUser> GetUsersByCriteria(string name, string userName, string email, string MBTI, string dateFrom, string dateTo, string phoneNumber, bool? suspendedUser)
        {
            Offtick.Data.Context.ExpertOnlinerContexts.ExpertOnlinerFoundationEntities db = DataContextManager.GetPersitantStorage();
            Expression<Func<MembershipUser, bool>> ex = null;
            if (!string.IsNullOrEmpty(name))
                ex = ex.And(e => e.FirstName.Contains(name) || e.LastName.Contains(name));
            if (!string.IsNullOrEmpty(userName))
                ex = ex.And(e => e.UserName.Contains(userName));
            if (!string.IsNullOrEmpty(email))
                ex = ex.And(e => e.Email.Contains(email));
            if (!string.IsNullOrEmpty(MBTI))
                ex = ex.And(e => e.ApplicationMBTIResults.FirstOrDefault(m=>m.Result.Equals(MBTI))!=null);
            if (!string.IsNullOrEmpty(phoneNumber))
                ex = ex.And(e => e.PhoneNumber.Contains(phoneNumber));
            if (!string.IsNullOrEmpty(dateFrom))
            {
                DateTime dtFrom = PersianDateConvertor.ToGregorian(dateFrom);
                if(dtFrom!=DateTime.MinValue)
                    ex = ex.And(e => e.RegisterDateTime.HasValue && e.RegisterDateTime.Value >= dtFrom);
            }
            if (!string.IsNullOrEmpty(dateTo))
            {
                DateTime dtTo = PersianDateConvertor.ToGregorian(dateTo);
                if (dtTo != DateTime.MinValue)
                ex = ex.And(e => e.RegisterDateTime.HasValue && e.RegisterDateTime.Value <= dtTo);
            }

            
            

            if(suspendedUser.HasValue && suspendedUser.Value)
              ex=ex.And(e=>e.MembershipUserStatus.FirstOrDefault().StatusId!=(short)MembershipStatus.Active);
            if (ex != null)
                return db.MembershipUsers.Where(ex).OrderByDescending(e => e.MembershipUserStatus.FirstOrDefault().DateTime);
            else
                return db.MembershipUsers.OrderByDescending(e => e.MembershipUserStatus.FirstOrDefault().DateTime);
        }
        public IQueryable<MembershipUser> GetUsersByCriteria(string name, string userName, string email, string MBTI, string phoneNumber, bool? onlyEmptyDetailedCode)
        {
            Offtick.Data.Context.ExpertOnlinerContexts.ExpertOnlinerFoundationEntities db = DataContextManager.GetPersitantStorage();
            Expression<Func<MembershipUser, bool>> ex = null;
            if (!string.IsNullOrEmpty(name))
                ex = ex.And(e => e.FirstName.Contains(name) || e.LastName.Contains(name));
            if (!string.IsNullOrEmpty(userName))
                ex = ex.And(e => e.UserName.Contains(userName));
            if (!string.IsNullOrEmpty(email))
                ex = ex.And(e => e.Email.Contains(email));
            if (!string.IsNullOrEmpty(MBTI))
                ex = ex.And(e => e.ApplicationMBTIResults.FirstOrDefault(m => m.Result.Equals(MBTI)) != null);
            if (!string.IsNullOrEmpty(phoneNumber))
                ex = ex.And(e => e.PhoneNumber.Contains(phoneNumber));
          




            if (onlyEmptyDetailedCode.HasValue && onlyEmptyDetailedCode.Value)
                ex = ex.And(e => !e.DetailedCode.HasValue);
            if (ex != null)
                return db.MembershipUsers.Where(ex).OrderByDescending(e => e.MembershipUserStatus.FirstOrDefault().DateTime);
            else
                return db.MembershipUsers.OrderByDescending(e => e.MembershipUserStatus.FirstOrDefault().DateTime);
        }



        public IQueryable<MembershipUser> GetUsersProfileByCriteria(string name, string userName, string email, string dateFrom, string dateTo, string phoneNumber, bool? suspendedSite, bool? permitToDistributeOnMainSite)
        {
            Offtick.Data.Context.ExpertOnlinerContexts.ExpertOnlinerFoundationEntities db = DataContextManager.GetPersitantStorage();
            Expression<Func<MembershipUser, bool>> ex = null;
            if (!string.IsNullOrEmpty(name))
                ex = ex.And(e => e.FirstName.Contains(name) || e.LastName.Contains(name));
            if (!string.IsNullOrEmpty(userName))
                ex = ex.And(e => e.UserName.Contains(userName));
            if (!string.IsNullOrEmpty(email))
                ex = ex.And(e => e.Email.Contains(email));
            if (!string.IsNullOrEmpty(phoneNumber))
                ex = ex.And(e => e.PhoneNumber.Contains(phoneNumber));
            if (!string.IsNullOrEmpty(dateFrom))
            {
                DateTime dtFrom = PersianDateConvertor.ToGregorian(dateFrom);
                if (dtFrom != DateTime.MinValue)
                    ex = ex.And(e => e.RegisterDateTime.HasValue && e.RegisterDateTime.Value >= dtFrom);
            }
            if (!string.IsNullOrEmpty(dateTo))
            {
                DateTime dtTo = PersianDateConvertor.ToGregorian(dateTo);
                if (dtTo != DateTime.MinValue)
                    ex = ex.And(e => e.RegisterDateTime.HasValue && e.RegisterDateTime.Value <= dtTo);
            }




           if (suspendedSite.HasValue && suspendedSite.Value)
                ex = ex.And(e => e.ProfileSetting.ProfileConfirmStatusId!= (int)ConfirmStatus.Confirmed);

               if (permitToDistributeOnMainSite.HasValue && suspendedSite.Value)
                   ex = ex.And(e => e.ProfileSetting.PublishOnMainPageConfirmStatusId != (int)ConfirmStatus.Confirmed);
         
            
            if (ex != null)
                return db.MembershipUsers.Where(ex).OrderByDescending(e => e.MembershipUserStatus.FirstOrDefault().DateTime);
            else
                return db.MembershipUsers.OrderByDescending(e => e.MembershipUserStatus.FirstOrDefault().DateTime);
        }


        public MembershipUser GetUser(Guid userId)
        {
            MembershipUser user = null;
            
                Offtick.Data.Context.ExpertOnlinerContexts.ExpertOnlinerFoundationEntities db = DataContextManager.GetPersitantStorage();
                user = db.MembershipUsers.FirstOrDefault(e => e.UserId.Equals(userId));
            return user;
        }

        public MembershipCity GetCity(Guid cityId)
        {
            MembershipCity city = null;

            Offtick.Data.Context.ExpertOnlinerContexts.ExpertOnlinerFoundationEntities db = DataContextManager.GetPersitantStorage();
            city = db.MembershipCities.FirstOrDefault(e => e.CityId.Equals(cityId));
            return city;
        }
        public IQueryable<MembershipCity> GetCities()
        {
            return GetCities(null);
        }
        public IQueryable<MembershipCity> GetCities(Guid? stateId)
        {
            Offtick.Data.Context.ExpertOnlinerContexts.ExpertOnlinerFoundationEntities db = DataContextManager.GetPersitantStorage();
            var cities=db.MembershipCities.AsQueryable();
            if (stateId != null && stateId.HasValue)
                cities = cities.Where(e => e.StateId.Equals(stateId.Value));
            return cities;
        }
        public IQueryable<MembershipCity> GetCities(string cityName, bool? onlyEmptyDetailCode)
        {
            Offtick.Data.Context.ExpertOnlinerContexts.ExpertOnlinerFoundationEntities db = DataContextManager.GetPersitantStorage();
            var query = db.MembershipCities.Where(e => e.CityName.Contains(cityName));
            if (onlyEmptyDetailCode.HasValue && onlyEmptyDetailCode.Value == true)
                query = query.Where(e => e.DetailedCode == null);
            return query.OrderBy(e => e.CityId);
        }
        public MembershipCity getCityById(Guid cityId)
        {
            Offtick.Data.Context.ExpertOnlinerContexts.ExpertOnlinerFoundationEntities db = DataContextManager.GetPersitantStorage();
            return db.MembershipCities.FirstOrDefault(e => e.CityId.Equals(cityId));
        }
        public EditStatus updatecityDetailedCode(Guid cityId, int detailedCode)
        {
            var city = MembershipManager.Instance.getCityById(cityId);
            if (city != null)
                city.DetailedCode = detailedCode;
            else
                return EditStatus.RejectedByNotExit;
            int effected = DataContextManager.GetPersitantStorage().SaveChanges();
            if (effected > 0)
                return EditStatus.Edited;
            else
                return EditStatus.RejectedByNonEffectiveQuery;
        }


        public IQueryable<MembershipState> GetStates(Guid?  countryId=null)
        {
            Offtick.Data.Context.ExpertOnlinerContexts.ExpertOnlinerFoundationEntities db = DataContextManager.GetPersitantStorage();
            var states = db.MembershipStates.AsQueryable();
            if(countryId.HasValue)
                states= states.Where(e=>e.CountryId.Equals(countryId.Value));
            return states;
        }
        public IQueryable<MembershipState> GetStates(string stateName, bool? onlyEmptyDetailCode)
        {
            Offtick.Data.Context.ExpertOnlinerContexts.ExpertOnlinerFoundationEntities db = DataContextManager.GetPersitantStorage();
            var query = db.MembershipStates.Where(e => e.StateName.Contains(stateName));
            if (onlyEmptyDetailCode.HasValue && onlyEmptyDetailCode.Value == true)
                query = query.Where(e => e.DetailedCode == null);
            return query.OrderBy(e => e.StateId);
        }
        public MembershipState getStateById(Guid stateId)
        {
            Offtick.Data.Context.ExpertOnlinerContexts.ExpertOnlinerFoundationEntities db = DataContextManager.GetPersitantStorage();
            return db.MembershipStates.FirstOrDefault(e => e.StateId.Equals(stateId));
        }
        public EditStatus updateStateDetailedCode(Guid stateId, int detailedCode)
        {
            var state = MembershipManager.Instance.getStateById(stateId);
            if (state != null)
                state.DetailedCode = detailedCode;
            else
                return EditStatus.RejectedByNotExit;
            int effected = DataContextManager.GetPersitantStorage().SaveChanges();
            if (effected > 0)
                return EditStatus.Edited;
            else
                return EditStatus.RejectedByNonEffectiveQuery;
        }


        public IQueryable<MembershipCountry> GetCountries(string countryName,bool? onlyEmptyDetailCode)
        {
            Offtick.Data.Context.ExpertOnlinerContexts.ExpertOnlinerFoundationEntities db = DataContextManager.GetPersitantStorage();
            var query = db.MembershipCountries.AsQueryable();
            if (!string.IsNullOrEmpty(countryName))
                query= query.Where(e => e.CountryName.Contains(countryName));
            if (onlyEmptyDetailCode.HasValue && onlyEmptyDetailCode.Value == true)
                query = query.Where(e => e.DetailedCode == null);
            return query.OrderBy(e=>e.CountryId);
        }

        public MembershipCountry getCountryById(Guid countryId)
        {
            Offtick.Data.Context.ExpertOnlinerContexts.ExpertOnlinerFoundationEntities db = DataContextManager.GetPersitantStorage();
            return db.MembershipCountries.FirstOrDefault(e => e.CountryId.Equals(countryId));
        }
        public EditStatus updateCountryDetailedCode(Guid countryId, int detailedCode)
        {
            var country = MembershipManager.Instance.getCountryById(countryId);
            if (country != null)
                country.DetailedCode = detailedCode;
            else
                return EditStatus.RejectedByNotExit;
            int effected = DataContextManager.GetPersitantStorage().SaveChanges();
            if (effected > 0)
                return EditStatus.Edited;
            else
                return EditStatus.RejectedByNonEffectiveQuery;
        }

        public EditStatus updateUserDetailedCode(Guid userId, int detailedCode)
        {
            var user = MembershipManager.Instance.GetUser(userId);
            if (user != null)
                user.DetailedCode = detailedCode;
            else
                return EditStatus.RejectedByNotExit;
            int effected = DataContextManager.GetPersitantStorage().SaveChanges();
            if (effected > 0)
                return EditStatus.Edited;
            else
                return EditStatus.RejectedByNonEffectiveQuery;
        }
        private MembershipRole FindRole(string roleName)
        {
            var db = DataContextManager.GetPersitantStorage();
            return db.MembershipRoles.FirstOrDefault(e => e.RoleName.Equals(roleName));
            
        }

        public bool ExistUser(string userName)
        {
            var db = DataContextManager.GetPersitantStorage();
            return db.MembershipUsers.FirstOrDefault(e=>e.UserName.Equals(userName))!=null;
        }
        public bool ExistEmail(string email)
        {
            var db = DataContextManager.GetPersitantStorage();
            return db.MembershipUsers.FirstOrDefault(e =>  e.Email.Equals(email)) != null;
        }
        public bool ExistPhoneNumber(string phoneNumber)
        {
            var db = DataContextManager.GetPersitantStorage();
            return db.MembershipUsers.FirstOrDefault(e => e.PhoneNumber.Equals(phoneNumber)) != null;
        }

        public MembershipStatu GetMemberStatus(string status)
        {
          
            var db = DataContextManager.GetPersitantStorage();
           return db.MembershipStatus.FirstOrDefault(e => e.StatusModel.Equals(status));
        }


        public bool SaveMapData(Guid cityId,MembershipUser user)
        {
            if (user == null)
                return false;

            var db = DataContextManager.GetPersitantStorage();
            var location = user.MembershipLocations.FirstOrDefault();
            if (location == null)
            {
                location = new MembershipLocation()
                {
                    LocationId = Guid.NewGuid(),
                };
                user.MembershipLocations.Add(location);
            }
            location.CityId = cityId;
            int changeCount=db.SaveChanges();
            return changeCount > 0;
        }


        public IList<MembershipLocation> GetLocationsOfUser(Guid userId)
        {
            var db = DataContextManager.GetPersitantStorage();
            return db.MembershipLocations.Where(e => e.UserId.Equals(userId)).ToList();
        }


        public MembershipIntroduceInfo GetIntroduceInfo(Guid userId)
        {
            if ( userId == null)
                return null;
            var db = DataContextManager.GetPersitantStorage();
            return db.MembershipIntroduceInfoes.FirstOrDefault(e => e.IntroduceInfoId == userId);

        }

        public void SaveIntroduceInfo(Guid userId, string value,bool isAboutUs)
        {
            if (string.IsNullOrEmpty(value) || userId==null)
                return;

            var obj = GetIntroduceInfo(userId);
            var db = DataContextManager.GetPersitantStorage();
            if (obj != null)
            {
                if (isAboutUs)
                    obj.AboutUs = value;
                else
                    obj.ContactUs = value;
            }
            else
            {
                db.MembershipIntroduceInfoes.Add(new MembershipIntroduceInfo
                {
                      IntroduceInfoId=userId,
                     AboutUs=isAboutUs?value:null,
                     ContactUs=!isAboutUs?value:null,
                });
            }

            int changeCount=db.SaveChanges();
        }


      


        public IList<News> GetNewsOfMember(string userName, int take, Nullable<int> skip)
        {
            var user = GetUser(userName);
            if (user != null)
            {
                return user.News.Take(take).ToList();
            }
            return null;
        }

        private Guid GetSystemGuid()
        {

            return Guid.Parse(ConfigFactory.GetConfigManager(ConfigManagerType.WebConfig).ReadSetting("SystemGUID"));
        }
        private ISecretStrategy secretStrategy;



        public string GetPathOfNewsIMage(Guid userId, string fileUrl)
        {
            NewsFileManager imageManager = new Offtick.Business.Services.Storage.File.NewsFileManager(userId.ToString());
            return imageManager.GetRelativeFullFileName(userId.ToString(), fileUrl, true);
        }


        #region updates
        public LoginStatus ChangePassword(string oldPassword, string newPassword)
        {
            return ChangePassword(this.GetCurrentUser(), oldPassword, newPassword);
        }
        public LoginStatus ChangePassword(MembershipUser user,string oldPassword, string newPassword)
        {
            var db = DataContextManager.GetPersitantStorage();
            if (user != null && !secretStrategy.Decrypt(user.Password).Equals(oldPassword))
                return LoginStatus.InvalidPassword;
            else
            {
                user.Password = secretStrategy.Encrypt(newPassword);
                db.SaveChanges();
                return LoginStatus.Success;
            }

        }
        public EditStatus ResetPassword(MembershipUser user, string newPassword)
        {
            if (user == null)
                return EditStatus.RejectedByNotExit;
            else
            {
                user.Password = secretStrategy.Encrypt(newPassword);
                return this.AcceptChanges()>0 ?   EditStatus.Edited:EditStatus.RejectedByNonEffectiveQuery;
            }

        }

        public EditStatus ChangeUserName(string oldUserName, string newUserName)
        {
            var db = DataContextManager.GetPersitantStorage();
            var user = GetCurrentUser();

            if (user == null || user.UserName!=oldUserName)
                return EditStatus.RejectedByNotExit;
            else
            {
                user.UserName = newUserName;
                int effected=db.SaveChanges();
                return effected>0? EditStatus.Edited:  EditStatus.RejectedByNonEffectiveQuery;
            }

        }

        public EditStatus ChangePhoto(MembershipUser user, HttpPostedFileBase picture)
        {
            if (user!=null && picture != null)
            {
                ImageFileManager ifm = new ImageFileManager(user.UserId.ToString());
                string path = ifm.GetFullFileName(user.Picture, ImageFileManager.ImageState.Normal);
                 if(ifm.IsExit(path)) ifm.Delete(path);

                 path = ifm.GetFullFileName(user.Picture, ImageFileManager.ImageState.Thumbnail16);
                 if (ifm.IsExit(path)) ifm.Delete(path);

                 path = ifm.GetFullFileName(user.Picture, ImageFileManager.ImageState.Thumbnail32);
                 if (ifm.IsExit(path)) ifm.Delete(path);

                 path = ifm.GetFullFileName(user.Picture, ImageFileManager.ImageState.Thumbnail64);
                 if (ifm.IsExit(path)) ifm.Delete(path);

                 path = ifm.GetFullFileName(user.Picture, ImageFileManager.ImageState.Thumbnail96);
                 if (ifm.IsExit(path)) ifm.Delete(path);

                 path = ifm.GetFullFileName(user.Picture, ImageFileManager.ImageState.Thumbnail128);
                 if (ifm.IsExit(path)) ifm.Delete(path);

                 path = ifm.GetFullFileName(user.Picture, ImageFileManager.ImageState.Thumbnail256);
                 if (ifm.IsExit(path)) ifm.Delete(path);

                 path = ifm.GetFullFileName(user.Picture, ImageFileManager.ImageState.Thumbnail320);
                 if (ifm.IsExit(path)) ifm.Delete(path);

                 path = ifm.GetFullFileName(user.Picture, ImageFileManager.ImageState.Thumbnail800);
                 if (ifm.IsExit(path)) ifm.Delete(path);



                string fileName = picture.FileName;
                string newFileName = ifm.Save(picture, fileName,800);
                user.Picture = newFileName;

                int effected = this.AcceptChanges();
                if (effected > 0)
                    return EditStatus.Edited;
                else
                    return EditStatus.RejectedByNonEffectiveQuery;
                
            }
            return EditStatus.RejectedByNotExit;
        }

        public EditStatus UpdateInfo(MembershipUser user, string firstName, string lastName, string gender, DateTime? birthDate, string domainUrl, string address,  string PhoneNumber,Guid cityId,string email)
        {
            if (user == null)
                return EditStatus.RejectedByNotExit;
            user.FirstName = firstName;
            user.LastName = lastName;
            user.Gender = gender;
            user.BirthDate = birthDate;
            
            user.DomainUrl = domainUrl;
            user.Address = address;
            user.PhoneNumber = PhoneNumber;
            user.Email = email;
            var roleName=user.MembershipUsersInRoles.FirstOrDefault().MembershipRole.RoleName;
            this.SaveMapData(cityId, user);
            //if (roleName == "Doctor")
            //{
            //    MembershipManager.Instance.AddOrUpdateDoctorProfile(user.UserName, RegistrationNo, OfficeNumber, address, null);
            //}
            int effected = this.AcceptChanges();
            return effected > 0 ? EditStatus.Edited : EditStatus.RejectedByNonEffectiveQuery;
        }

        public EditStatus UpdateSpecification(MembershipUser user, string specification,string responsibleName,string responsiblePhone,string responsibleEmail)
        {
            if (user == null)
                return EditStatus.RejectedByNotExit;
            var info=user.MembershipIntroduceInfo;

            if (info == null)
            {
                info=user.MembershipIntroduceInfo = new MembershipIntroduceInfo();
            }
            info.Specification = specification;
            info.ResponsibleEmail = responsibleEmail;
            info.ResponsibleName = responsibleName;
            info.ResponsiblePhone = responsiblePhone;

            int effected = this.AcceptChanges();
            return effected > 0 ? EditStatus.Edited : EditStatus.RejectedByNonEffectiveQuery;
        }
        public EditStatus ChangeStatus(MembershipUser user, MembershipStatus status)
        {
            if (user == null)
                return EditStatus.RejectedByNotExit;
            var userStatus=user.MembershipUserStatus.FirstOrDefault();
            userStatus.StatusId = (short)status;
            int effected = this.AcceptChanges();
            return effected > 0 ? EditStatus.Edited : EditStatus.RejectedByNonEffectiveQuery;
        }
        #endregion


        #region Profile & Visitors

        public AddStatus AddVisitor(MembershipUser owner, MembershipUser visitor)
        {
            if (owner == null)
                return AddStatus.RejectedByInvalidParameter;


            if (owner.ProfileVisit == null)
                owner.ProfileVisit = new ProfileVisit() { };
            if (visitor != null)
            {
                var visitorEntity = owner.ProfileVisit.ProfileVisitors.FirstOrDefault(e => e.VisitorUserId.Equals(visitor.UserId));
                if (visitorEntity == null)
                {
                    visitorEntity = new ProfileVisitor();
                    visitorEntity.VisitorUserId = visitor.UserId;
                    owner.ProfileVisit.ProfileVisitors.Add(visitorEntity);
                }
                visitorEntity.DateTime = DateTime.Now;
                visitorEntity.VisitCount++;
            }

          

            owner.ProfileVisit.VisitCount++;
            owner.ProfileVisit.LastDateTime = DateTime.Now;
            
            int effectedCount = this.AcceptChanges();
            if (effectedCount > 0)
                return AddStatus.Added;
            else
                return AddStatus.RejectedByNonEffectiveQuery;
        }
        public IList<MembershipUser> GetListOfVisitors(MembershipUser user)
        {
            if (user == null || !user.MembershipPrivacy.IsVisitorVisible || user.ProfileVisit==null)
                return null;
            var results = from v in user.ProfileVisit.ProfileVisitors
                          where v.MembershipUser.MembershipPrivacy.IsVisitingVisible
                          select v.MembershipUser;
            return results.ToList();
        }


        public EditStatus ChangeProfileStatus(MembershipUser user, ConfirmStatus status)
        {
            if (user == null)
                return EditStatus.RejectedByNotExit;
            var profileSetting = user.ProfileSetting;
            if (profileSetting == null)
            {
                user.ProfileSetting = new ProfileSetting()
                {
                    ProfileConfirmStatusId = 1,
                    PublishOnIndividualSiteConfirmStatusId = 1,
                    PublishOnMainPageConfirmStatusId = 1
                };
                profileSetting = user.ProfileSetting;
            }

            profileSetting.ProfileConfirmStatusId = (int)status;
            int effected = this.AcceptChanges();
            return effected > 0 ? EditStatus.Edited : EditStatus.RejectedByNonEffectiveQuery;
        }
        public EditStatus ChangePublishOnMainPageStatus(MembershipUser user, ConfirmStatus status)
        {
            if (user == null)
                return EditStatus.RejectedByNotExit;
            var profileSetting = user.ProfileSetting;
            if (profileSetting == null)
            {
                user.ProfileSetting = new ProfileSetting()
                {
                    ProfileConfirmStatusId = 1,
                    PublishOnIndividualSiteConfirmStatusId = 1,
                    PublishOnMainPageConfirmStatusId = 1
                };
                profileSetting = user.ProfileSetting;
            }

            profileSetting.PublishOnMainPageConfirmStatusId = (int)status;
            int effected = this.AcceptChanges();
            return effected > 0 ? EditStatus.Edited : EditStatus.RejectedByNonEffectiveQuery;
        }
        #endregion


        #region Accounting Actions
        public EditStatus UpdateDetailCode(MembershipUser user, int detailedCode)
        {
            try
            {
                if (user == null)
                    return EditStatus.RejectedByNotExit;
                var entities = this.GetPersitantStorage();
                user.DetailedCode = detailedCode;

                int effected = entities.SaveChanges();
                if (effected > 0)
                    return EditStatus.Edited;
                else
                    return EditStatus.RejectedByNonEffectiveQuery;
            }
            catch
            {
                return EditStatus.RejectedByInternalSystemError;
            }
        }
        public EditStatus UpdateDetailCode(MembershipCountry country, int detailedCode)
        {
            try
            {
                if (country == null)
                    return EditStatus.RejectedByNotExit;
                var entities = this.GetPersitantStorage();
                country.DetailedCode = detailedCode;

                int effected = entities.SaveChanges();
                if (effected > 0)
                    return EditStatus.Edited;
                else
                    return EditStatus.RejectedByNonEffectiveQuery;
            }
            catch
            {
                return EditStatus.RejectedByInternalSystemError;
            }
        }
        public EditStatus UpdateDetailCode(MembershipState state, int detailedCode)
        {
            try
            {
                if (state == null)
                    return EditStatus.RejectedByNotExit;
                var entities = this.GetPersitantStorage();
                state.DetailedCode = detailedCode;

                int effected = entities.SaveChanges();
                if (effected > 0)
                    return EditStatus.Edited;
                else
                    return EditStatus.RejectedByNonEffectiveQuery;
            }
            catch
            {
                return EditStatus.RejectedByInternalSystemError;
            }
        }
        public EditStatus UpdateDetailCode(MembershipCity city, int detailedCode)
        {
            try
            {
                if (city == null)
                    return EditStatus.RejectedByNotExit;
                var entities = this.GetPersitantStorage();
                city.DetailedCode = detailedCode;

                int effected = entities.SaveChanges();
                if (effected > 0)
                    return EditStatus.Edited;
                else
                    return EditStatus.RejectedByNonEffectiveQuery;
            }
            catch
            {
                return EditStatus.RejectedByInternalSystemError;
            }
        }
        #endregion
    }
}
