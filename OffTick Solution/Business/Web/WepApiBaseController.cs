﻿using Offtick.Business.Services.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Offtick.Business.Web
{
    public class WepApiBaseController: ApiController
    {
        public Action<IDataContextContainer> assignContext;

        public WepApiBaseController(IDataContextContainer objectContext)
        {
            
            if (assignContext != null)
                assignContext(objectContext);
            DataContextContainer dct = (DataContextContainer)objectContext;
            DataContextManager.SetDataContextContainer(dct);


        }
    }
}
