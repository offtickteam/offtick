﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.Web
{
    public class GalleryImageHelper
    {
        public static System.Web.Mvc.MvcHtmlString CreateGalleryMenu(IEnumerable<Offtick.Data.Context.ExpertOnlinerContexts.Gallery> galleries,short langaugeId,string menuName,string cssClassNameOfLi,string updateTargetId)
        {
           
            Func<string,string,string,string> createAjaxLink=(title,galleryId,targetId)=>
                {
                    return string.Format("<a data-ajax=\"true\" data-ajax-mode=\"replace\" data-ajax-update=\"#{0}\" data-ajax-url=\"/User/Gallery/GalleryBodyManagement?galleryId={1}\" href=\"/User/Gallery/GalleryBodyManagement?galleryId={1}\">{2}</a>", targetId, galleryId, title);

                };


            Func<Offtick.Data.Context.ExpertOnlinerContexts.Gallery, short, string> getTitle = (g1, l1) =>
                {
                    if (g1 == null)
                        return string.Empty;
                    else
                        return g1.Title;
                };

            StringBuilder sb = new StringBuilder();
            if (string.IsNullOrEmpty(menuName))
                sb.Append("<ul>");
            else
            {
                sb.AppendFormat("{0}", createAjaxLink("گالری تصاویر", "", updateTargetId));
                sb.AppendFormat("<ul id=\"{0}\">", menuName);
                
            }

            if(galleries!=null)
                foreach (var gallery in galleries)
                {
                    if (gallery.Gallery1 != null && gallery.Gallery1.Count != 0)
                        sb.AppendFormat("<li>{0}{1} </li>", createAjaxLink(getTitle(gallery,langaugeId),gallery.GalleryId.ToString(),updateTargetId), CreateGalleryMenu(gallery.Gallery1, langaugeId,string.Empty, cssClassNameOfLi,updateTargetId));
                    else
                        sb.AppendFormat("<li>{0}</li>", createAjaxLink(getTitle(gallery, langaugeId), gallery.GalleryId.ToString(), updateTargetId));
                }
                sb.Append("</ul>");
            return new System.Web.Mvc.MvcHtmlString(sb.ToString());

        }

        
    }
}
