﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Offtick.Data.Context.ExpertOnlinerContexts;
using Offtick.Business.Services.Storage;

namespace Offtick.Business.Web
{

    public abstract class BaseController : Controller
    {
        //
        // GET: /Default1/

        public Action<DataContextContainer> assignContext;

        public BaseController( DataContextContainer objectContext)
        {
            if (assignContext != null)
                assignContext(objectContext);
            DataContextManager.SetDataContextContainer(objectContext);
            

        }

        

    }
}
