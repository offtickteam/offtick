﻿using Offtick.Core.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.Web
{
    public class SMTPService
    {
        private string _fromMail { get; set; }
        private string fromPassword { get; set; }


        public SMTPService()
        {
            var configManager = ConfigFactory.GetConfigManager(ConfigManagerType.WebConfig);

            this._fromMail = configManager.ReadSetting("infoEmailAddress");
            this.fromPassword = configManager.ReadSetting("infoEmailPassword");
        }
        public void SendMail(string toMail,string title,string body)
        {
            SendMail(toMail, title, body, null, null);
        }
        public void SendMail(IList<string> toMails, string title, string body)
        {
            SendMail(toMails, title, body, null, null);
        }

        public void SendMail(string toMail, string title, string body,System.IO.Stream stream,string fileName)
        {
            SendMail(this._fromMail, toMail, title, body, stream, fileName);
        }
        public void SendMail(IList<string> toMails, string title, string body, System.IO.Stream stream, string fileName)
        {
            SendMail(this._fromMail, toMails, title, body, stream, fileName);
        }


        public void SendMail(string fromMail, IList<string> toMails, string title, string body, System.IO.Stream stream, string fileName)
        {
            try
            {
                SmtpClient client = new SmtpClient();
                var configManager = ConfigFactory.GetConfigManager(ConfigManagerType.WebConfig);

                // client.Port = 587;
                client.Host = configManager.ReadSetting("SMTPHost");
                // client.EnableSsl = true;
                //client.Timeout = 10000;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Credentials = new System.Net.NetworkCredential(this._fromMail, this.fromPassword);



                MailMessage mm = new MailMessage();
                //(fromMail, toMail, title, body);
                mm.From = new MailAddress(fromMail);
                foreach (string toMailAddr in toMails)
                    mm.To.Add(new MailAddress(toMailAddr));
                mm.Subject = title;
                mm.Body = body;

                if (stream != null && !string.IsNullOrEmpty(fileName))
                    mm.Attachments.Add(new System.Net.Mail.Attachment(stream, fileName));

                mm.IsBodyHtml = true;
                mm.BodyEncoding = UTF8Encoding.UTF8;
                mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;


                client.Send(mm);
            }
            catch(Exception ex)
            {
                
            }
        }
        public void SendMail(string fromMail,string toMail, string title, string body, System.IO.Stream stream, string fileName)
        {
            this.SendMail(fromMail, new List<string>() { toMail }, title, body, stream, fileName);
        }
    }
}
