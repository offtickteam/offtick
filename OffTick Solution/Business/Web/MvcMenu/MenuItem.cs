﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.Web.MvcMenu
{
    public class MenuItem
    {
        public MenuItem():this(false)
        {
        }
        public MenuItem(bool isRoot)
        {
            this.IsRoot = isRoot;
            this.MenuChilds = new List<MenuItem>();
        }

        public string Title { get; set; }
        public string MenuId { get; set; }
        public string CssClass { get; set; }
        public string ImageUrl { get; set; }
        public int Order { get; set; }
        public bool IsRoot { get;private set; }
        public string LinkUrl { get; set; }


        public bool IsParent
        {
            get
            {
                return  MenuChilds.Count != 0;
            }
        }
        public bool IsDirectLink
        {
            get
            {
                return !string.IsNullOrEmpty(LinkUrl);
            }
        }

        public IList<MenuItem> MenuChilds { get; set; }
    }
}
