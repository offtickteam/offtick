﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.Web.MvcMenu
{
    public class JQueryMenuFactory : IMvcMenuIFactory
    {
        public System.Web.Mvc.MvcHtmlString CreateMenu(MenuItem menus, string parentCssClassName, string menuDivId, string menuULId, string menuCssClassName, string updateTargetId)
        {
            Func<string, string, string, string> createAjaxLink = (title, menuId, targetId) =>
            {
                return string.Format("<a data-ajax=\"true\" data-ajax-mode=\"replace\" data-ajax-update=\"#{0}\" data-ajax-url=\"/User/MenuBuilder/MenuBuilderParser?menuId={1}\" href=\"/User/MenuBuilder/MenuBuilderParser?menuId={1}\">{2}</a>", targetId, menuId, title);
            };

            Func<string, string, string, string, string> createMenuItem = (title, parentClass, menuId, targetId) =>
            {
                return string.Format("<a href=\"#\" class=\"{0}\"  data-ajax=\"true\" data-ajax-mode=\"replace\" data-ajax-update=\"#{1}\" data-ajax-url=\"/User/MenuBuilder/MenuBuilderParser?menuId={2}\"><span>{3}</span></a>", parentClass, targetId, menuId, title);
            };




            //StringBuilder sb = new StringBuilder();
            //if (!menus.IsRoot)
            //{
            //    sb.Append("<ul>");
            //}
            //else
            //{
            //    sb.AppendFormat("{0}", createAjaxLink("طراحی منوی سایت", "", updateTargetId));
            //    sb.AppendFormat("<div id=\"{0}\"><ul class=\"{0}\">", menuDivId, menuCssClassName);

            //}
            //if (menus != null)
            //    foreach (var menu in menus.MenuChilds)
            //    {
            //        if (menu.IsParent)
            //            sb.AppendFormat("<li>{0}{1} </li>", createMenuItem(menu.Title, "parent", menu.MenuId.ToString(), updateTargetId), CreateMenu(menu.MenuChilds, "parent", "", "", "", updateTargetId));
            //        else
            //            sb.AppendFormat("<li>{0}</li>", createAjaxLink(menu.Title, menu.MenuId.ToString(), updateTargetId));
            //    }
            //if (string.IsNullOrEmpty(menuDivId))
            //{
            //    sb.Append("</ul>");
            //}
            //else
            //{
            //    sb.Append("</ul></div>");
            //}


            StringBuilder sb = new StringBuilder();
            if (menus != null)
            {
                if (menus.IsRoot)
                {
                    sb.AppendFormat("{0}", createAjaxLink("Select Root Menu", "", updateTargetId));
                    sb.AppendFormat("<div id=\"{0}\"><ul class=\"{0}\">", menuDivId, menuCssClassName);
                    foreach (var menu in menus.MenuChilds)
                    {
                        sb.Append(CreateMenu(menu, parentCssClassName, string.Empty, string.Empty, string.Empty, updateTargetId));
                    }
                    sb.Append("</ul></div>");
                }
                else
                {
                    if (menus.IsParent)
                    {
                        sb.AppendFormat("<li> {0}<ul>", createMenuItem(menus.Title, "parent", menus.MenuId.ToString(), updateTargetId));

                        foreach (var menu in menus.MenuChilds.OrderByDescending(e => e.Order))
                        {

                            sb.Append(CreateMenu(menu, parentCssClassName, string.Empty, string.Empty, string.Empty, updateTargetId));
                        }
                        sb.Append(" </ul></li>");
                    }
                    else
                    {
                        sb.AppendFormat("<li>{0}</li>", createAjaxLink(menus.Title, menus.MenuId.ToString(), updateTargetId));
                    }
                }
            }
            return new System.Web.Mvc.MvcHtmlString(sb.ToString());
        }

    }
}
