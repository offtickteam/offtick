﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.Web.MvcMenu
{
    public class MVCMenuBuilder
    {
        public static System.Web.Mvc.MvcHtmlString MvcMenuBuilder(MenuItem menus
            , string parentCssClassName,
            string menuDivId,
            string menuULId,
            string menuCssClassName,
            string updateTargetId,
            Offtick.Business.Web.MvcMenu.MvcMenuType type
            )
        {
            IMvcMenuIFactory factory = null;
            switch (type)
            {
                case MvcMenu.MvcMenuType.JQueryDefault:
                     factory = new JQueryMenuFactory();
                    return factory.CreateMenu(menus, parentCssClassName, menuDivId,menuULId, menuCssClassName, updateTargetId);
                case MvcMenuType.Nictitate:
                    factory = new NictitateMvcMenuViewingMode();
                    return factory.CreateMenu(menus, parentCssClassName, menuDivId,menuULId, menuCssClassName, updateTargetId);
            }
            return new System.Web.Mvc.MvcHtmlString(string.Empty);
        }

        public static MenuItem ConvertToMenuItem(IEnumerable<Offtick.Data.Context.ExpertOnlinerContexts.Menu> menus)
        {
            if(menus!=null)
            {
                MenuItem menu=new MenuItem(true);
                
                foreach(var m in menus)
                {
                    var mi = fnConvert(m);
                    menu.MenuChilds.Add(mi);
                }
                return menu;
            }
            return null;
        }
        private static MenuItem fnConvert(Offtick.Data.Context.ExpertOnlinerContexts.Menu mnu,short langauageId)
        {
            MenuItem mi = new MenuItem();
            mi.Title = mnu.Title;
            mi.MenuId = mnu.MenuId.ToString();
            mi.ImageUrl = mnu.IconUrl;
            mi.Order = mnu.MenuOrder;
            mi.LinkUrl = mnu.MenuType == 2 ? mnu.Url : string.Empty;
            if (mnu.Menu1 != null && mnu.Menu1.Count() != 0)
            {
                foreach (var mnuc in mnu.Menu1)
                {
                    var mic = fnConvert(mnuc);
                    mi.MenuChilds.Add(mic);
                }
            }
            return mi;
        }
        
        private static MenuItem fnConvert(Offtick.Data.Context.ExpertOnlinerContexts.Menu mnu)
        {
            short defaultLanguageId=mnu.MembershipUser.MembershipLangauges.First(e=>e.IsDefault).LanguageId;
            return fnConvert(mnu, defaultLanguageId);
        }
    }
}
