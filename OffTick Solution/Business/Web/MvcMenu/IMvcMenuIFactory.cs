﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.Web.MvcMenu
{
    public interface IMvcMenuIFactory
    {
        System.Web.Mvc.MvcHtmlString CreateMenu(MenuItem menus
            , string parentCssClassName,
            string menuId,
            string menuULId,
            string menuCssClassName,
            string updateTargetId);
       
    }
}
