﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.Web.MvcMenu
{
    public class NictitateMvcMenuViewingMode:IMvcMenuIFactory
    {
        private bool isFirstMenu=true;
        public System.Web.Mvc.MvcHtmlString CreateMenu(MenuItem menus, string parentCssClassName, string menuId, string menuULId, string menuCssClassName, string updateTargetId)
        {
            Func<string, string, string, string> createDirectLink = (directUrl, title, icon) =>
            {
                return string.Format("<a   href=\"{0}\">{1}<span>{2}</span></a>", directUrl, icon, title);
            };

            Func<string, string, string, string,string> createAjaxLink = (title, mId, targetId, icon) =>
            {
                return string.Format("<a data-ajax=\"true\" data-ajax-mode=\"replace\" data-ajax-update=\"#{0}\" data-ajax-url=\"/Content/Index?menuId={1}\" href=\"#\">{2}<span>{3}</span></a>", targetId, mId,icon, title);
            };

            Func<string, string, string, string, string> createMenuItem = (title, parentClass, mId, targetId) =>
            {
                return string.Format("<a href=\"#\" class=\"{0}\"  data-ajax=\"true\" data-ajax-mode=\"replace\" data-ajax-update=\"#{1}\" data-ajax-url=\"/Dr/MenuBuilder/MenuBuilderParser?menuId={2}\"><span>{3}</span></a>", parentClass, targetId, mId, title);
            };
           
            

            StringBuilder sb = new StringBuilder();
            if (menus != null)
            {
                if (menus.IsRoot)
                {
                    sb.AppendFormat("<nav id=\"{0}\" ><ul class=\"{1}\" id=\"{2}\">", menuId, menuCssClassName, menuULId);
                    foreach (var menu in menus.MenuChilds.OrderBy(e => e.Order))
                    {
                        sb.Append(CreateMenu(menu, parentCssClassName, string.Empty, string.Empty, string.Empty, updateTargetId));
                    }
                    sb.Append("</ul></nav>");
                }
                else
                {
                    string isFirstLi = string.Empty;
                    if (isFirstMenu)
                    {
                        isFirstLi = " class=\"current-menu-item\" ";
                        isFirstMenu = false;

                    }
                    if (menus.IsParent)
                    {
                        sb.AppendFormat("<li {0}> <a href=\"#\">{1}<span>{2}</span></a><ul>", isFirstLi, (string.IsNullOrEmpty(menus.ImageUrl) ? "" : string.Format("<i class=\"{0}\"></i>", menus.ImageUrl)), menus.Title);
                        //sb.AppendFormat("<li {0}> <a href=\"#\">{1}<span>{2}</span></a><ul>", isFirstLi, (string.IsNullOrEmpty(menus.ImageUrl) ? "" : string.Format("<i data-icon=\"{0}\"></i>", menus.ImageUrl)), menus.Title);
                        foreach (var menu in menus.MenuChilds.OrderBy(e => e.Order))
                        {
                            sb.Append(CreateMenu(menu, parentCssClassName, string.Empty, string.Empty, string.Empty, updateTargetId));
                        }
                        sb.Append(" </ul></li>");
                    }
                    else
                    {
                       // sb.AppendFormat(" <li{3}><a href=\"{0}\">{1}<span>{2}</span></a></li>", menus.MenuId, (string.IsNullOrEmpty(menus.ImageUrl) ? "" : string.Format("<i data-icon=\"{0}\"></i>",menus.ImageUrl)), menus.Title,isFirstLi);
                        if (menus.IsDirectLink)
                        {
                            sb.AppendFormat(" <li {0}>{1}</li>", isFirstLi, createDirectLink(menus.LinkUrl, menus.Title, (string.IsNullOrEmpty(menus.ImageUrl) ? "" : string.Format("<i class=\"{0}\"></i>", menus.ImageUrl))));
                        }
                        else
                            sb.AppendFormat(" <li {0}>{1}</li>", isFirstLi, createAjaxLink(menus.Title, menus.MenuId, updateTargetId, (string.IsNullOrEmpty(menus.ImageUrl) ? "" : string.Format("<i class=\"{0}\"></i>", menus.ImageUrl))));
                    }
                }
            }
         

            return new System.Web.Mvc.MvcHtmlString(sb.ToString());
        }
    }
}
