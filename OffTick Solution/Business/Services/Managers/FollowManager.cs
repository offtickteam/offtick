﻿using Offtick.Core.EnumTypes;
using Offtick.Data.Context.ExpertOnlinerContexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.Services.Managers
{
    public class FollowManager:BaseManager
    {
         private static FollowManager instance;
        public static FollowManager Instance
        {
            get
            {
                if (instance != null)
                    return instance;
                lock (typeof(FollowManager))
                {
                    if (instance == null)
                        instance = new FollowManager();
                    return instance;
                }
            }
        }


        private FollowManager()
        {
        }

        public FollowRequested RequestAFallow(MembershipUser follower, MembershipUser user)
        {

            var followRequested = user.FollowRequesteds.FirstOrDefault(e => e.FollowersUserId.Equals(follower.UserId));
            int effected = 0;
            if (followRequested == null)
            {
                followRequested = new FollowRequested()
                {
                    Datetime = DateTime.Now,
                    FollowersUserId = follower.UserId,
                    IsProtected = false,
                    UserId = user.UserId

                };
                user.FollowRequesteds.Add(followRequested);
                effected = this.AcceptChanges();

            }

            if (!user.IsProtected.HasValue || !user.IsProtected.Value)//not protected uesr
            {
                this.AcceptAFollow(followRequested, string.Empty);
                effected = 1;
            }
            return effected > 0 ? followRequested : null;

        }

        public FollowRejected RejectAFollow(FollowRequested followRequested, string description)
        {
            if (followRequested != null)
            {
                var user = followRequested.MembershipUser;
                var entities = this.GetPersitantStorage();
                entities.FollowRequesteds.Remove(followRequested);

                var followRejected = new FollowRejected()
                {
                    Description = description,
                    Datetime = DateTime.Now,
                    FollowerUserId = followRequested.FollowersUserId,

                };
                user.FollowRejecteds.Add(followRejected);
                int effected = this.AcceptChanges();
                if (effected > 0)
                    return followRejected;
                else
                    return null;
            }
            throw new ArgumentNullException("Null FollowRequested boject");
        }

        public Follow AcceptAFollow(FollowRequested followRequested, string description)
        {
            if (followRequested != null)
            {
                var user = followRequested.MembershipUser;
                var entities = this.GetPersitantStorage();
                entities.FollowRequesteds.Remove(followRequested);
                //user.FollowRequesteds.Remove(followRequested);

                //check to do not adding duplicate follow entity
                Follow follow = user.Follows.FirstOrDefault(e => e.FollowersUserId.Equals(followRequested.FollowersUserId));
                if (follow == null)
                {
                    follow = new Follow()
                    {
                        Datetime = DateTime.Now,
                        FollowersUserId = followRequested.FollowersUserId,
                        IsProtected = false,
                    };
                    user.Follows.Add(follow);
                }
                int effected = this.AcceptChanges();
                if (effected > 0)
                    return follow;
                else
                    return null;
            }
            throw new ArgumentNullException("Null FollowRequested boject");
        }


        public bool RemoveAFollow(Follow follow)
        {
            if (follow != null)
            {
                var entities = this.GetPersitantStorage();
                entities.Follows.Remove(follow);
                int effected = this.AcceptChanges();
                if (effected > 0)
                    return true;
                else
                    return false;
            }
            throw new ArgumentNullException("Null Follow boject");
        }


        public FollowStatus GetFollowStatusFor(string visitorUserName, string otherUserName)
        {
            if (string.IsNullOrEmpty(visitorUserName) == null || string.IsNullOrEmpty(otherUserName))
                return FollowStatus.none;
            return GetFollowStatusFor(Membership.MembershipManager.Instance.GetUser(visitorUserName), Membership.MembershipManager.Instance.GetUser(otherUserName));
        }
        public FollowStatus GetFollowStatusFor(MembershipUser visitorUser, MembershipUser otherUser)
        {
            if (visitorUser == null || otherUser == null)
                return FollowStatus.none;
           var existFollow= otherUser.Follows.FirstOrDefault(e => e.FollowersUserId.Equals(visitorUser.UserId));
           if (existFollow != null)
               return FollowStatus.Follow;
           var exitFollowRequested = otherUser.FollowRequesteds.FirstOrDefault(e => e.FollowersUserId.Equals(visitorUser.UserId));
           if (exitFollowRequested != null)
               return FollowStatus.Requested;

           var rejectedFollowRequested = otherUser.FollowRejecteds.FirstOrDefault(e => e.FollowerUserId.Equals(visitorUser.UserId));
           if (rejectedFollowRequested != null)
               return FollowStatus.Rejected;

           return FollowStatus.none;
        }

        public string getFollowTitleForFollowStatus(FollowStatus followStatus)
        {
            switch (followStatus)
            {
              
                case FollowStatus.Follow:
                    return "Following";
                case FollowStatus.Rejected:
                    return "Rejected";
                case FollowStatus.Requested:
                    return "Requested";
                case FollowStatus.none:
                default:
                    return "Follow";
            }
        }


        public string getFollowTitleForFollowStatus_Report(FollowStatus followStatus)
        {
            switch (followStatus)
            {

                case FollowStatus.Follow:
                    return "Following";
                case FollowStatus.Rejected:
                    return "Rejected";
                case FollowStatus.Requested:
                    return "Requested";
                case FollowStatus.none:
                default:
                    return "";
            }
        }

        public FollowRequested GetFollowRequestedById(long id)
        {
            var entities = this.GetPersitantStorage();
            return entities.FollowRequesteds.FirstOrDefault(e => e.FollowRequestedId.Equals(id));
        }
    }
}
