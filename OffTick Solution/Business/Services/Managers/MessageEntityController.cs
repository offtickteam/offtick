﻿
using Offtick.Business.Membership;
using Offtick.Business.Services.Storage.File;
using Offtick.Core.EnumTypes;
using Offtick.Data.Context.ExpertOnlinerContexts;
using Offtick.Business.Services.RealtimeSearvices.EventDrivenService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace  Offtick.Business.Services.Managers
{
    //public class MessageEntityController : EntityController<Message, System.Data.Entity.DbSet<Message>>
    //{
    //    public MessageEntityController(Action submitAction ):base(DataContextManager.GetPersitantStorage().Messages,submitAction)
    //    {
    //    }


    //}

    public class MessageEntityController:BaseManager
    {
        private static MessageEntityController instance;
        public static MessageEntityController Instance
        {
            get
            {
                if (instance != null)
                    return instance;
                lock (typeof(MessageEntityController))
                {
                    if (instance == null)
                        instance = new MessageEntityController();
                    return instance;
                }
            }
        }

        public AddStatus SaveMessage(Message message)
        {
            try
            {
                MembershipUser user = MembershipManager.Instance.GetUser(message.SenderUserId);
                if (message != null)
                {

                    user.Messages.Add(message);
                    int effected = AcceptChanges();
                    if (effected > 0)
                        return AddStatus.Added;
                    else
                        return AddStatus.RejectedByNonEffectiveQuery;
                }
                else
                    return AddStatus.RejectedByParentNotExit;
            }
            catch
            {
                return AddStatus.RejectedByInternalSystemError;
            }
        }

        public EditStatus UploadMessageFile(string userName, string messageId, string fileType, string fileData, out string returnFileName)
        {

            Guid msgId;
            returnFileName = string.Empty;
            if (Guid.TryParse(messageId, out msgId))
            {
                var message = MessageEntityController.Instance.GetMessage(msgId);

                if (message != null)
                {
                    if (!message.MembershipUser.UserName.Equals(userName))
                        return EditStatus.RejectedByAccessDenied;

                    //to do: the bellow work just for images, must add other solutions to work for every file type 
                    byte[] byteArrayOfImage = Convert.FromBase64String(fileData);
                    ImageFileManager imgFileManager = new ImageFileManager(message.MembershipUser.UserId.ToString());
                    var image = imgFileManager.GetImageFromByteArray(byteArrayOfImage);
                    string fileName = Guid.NewGuid().ToString() + "." + fileType;
                    string savedFileName = imgFileManager.Save(image, fileName);
                    //get message and update its FileName, then create Event

                    message.FileName = savedFileName;

                    int effected = AcceptChanges();
                    if (effected > 0)
                    {
                        returnFileName = savedFileName;
                        PermanentEventDispatcher.Instance.CreateEventSendMessage(msgId, message.MembershipUser1.UserId, null);
                        return EditStatus.Edited;
                    }
                    else
                        return EditStatus.RejectedByNonEffectiveQuery;
                }
            }
            return EditStatus.RejectedByNotExit;
        }

        public string DownloadMessageFile(string userName, string messageId, out string fileName)
        {

            Guid msgId;
            if (Guid.TryParse(messageId, out msgId))
            {
                var message = MessageEntityController.Instance.GetMessage(msgId);
                if (message != null && message.MembershipUser.UserName.Equals(userName))
                {


                    ImageFileManager imgFileManager = new ImageFileManager(message.MembershipUser.UserId.ToString());
                    byte[] bytes = imgFileManager.GetBytesOfImage(imgFileManager.GetFullFileName(message.FileName, false));
                    fileName = message.FileName;
                    return Convert.ToBase64String(bytes);
                }
            }
            fileName = string.Empty;
            return string.Empty;
        }







        public EditStatus ArchiveMessage(string userName, Guid messageId)
        {
            try
            {
                MembershipUser user = MembershipManager.Instance.GetUser(userName);
                if (user != null)
                {
                    Message msg = GetMessage(messageId);
                    if (msg.SenderUserId.Equals(user.UserId) || msg.RecieverUserId.Equals(user.UserId))
                    {
                        msg.IsPermanent = true;
                        int effected = AcceptChanges();
                        if (effected > 0)
                            return EditStatus.Edited;
                        else
                            return EditStatus.RejectedByNonEffectiveQuery;
                    }
                    else
                    {
                        //security warning
                        return EditStatus.RejectedByAccessDenied;
                    }

                }
                else
                    return EditStatus.RejectedByNotExit;
            }
            catch
            {
                return EditStatus.RejectedByInternalSystemError;
            }
        }
        public EditStatus MessageDelivered(string userName, Guid messageId)
        {
            try
            {
                MembershipUser user = MembershipManager.Instance.GetUser(userName);
                if (user != null)
                {
                    Message msg = GetMessage(messageId);
                    if (msg.SenderUserId.Equals(user.UserId) || msg.RecieverUserId.Equals(user.UserId))
                    {
                        if (!msg.IsDelivered.HasValue || !msg.IsDelivered.Value)
                        {
                            msg.IsDelivered = true;
                            msg.DeliveredTimestamp = DateTime.Now;
                        }
                        int effected = AcceptChanges();
                        if (effected > 0)
                            return EditStatus.Edited;
                        else
                            return EditStatus.RejectedByNonEffectiveQuery;
                    }
                    else
                    {
                        //security warning
                        return EditStatus.RejectedByAccessDenied;
                    }

                }
                else
                    return EditStatus.RejectedByNotExit;
            }
            catch
            {
                return EditStatus.RejectedByInternalSystemError;
            }
        }
        public EditStatus MessageReaded(string userName, Guid messageId)
        {
            try
            {
                MembershipUser user = MembershipManager.Instance.GetUser(userName);
                if (user != null)
                {
                    Message msg = GetMessage(messageId);
                    if (msg.SenderUserId.Equals(user.UserId) || msg.RecieverUserId.Equals(user.UserId))
                    {
                        DateTime now = DateTime.Now;
                        if (!msg.IsRead.HasValue || !msg.IsRead.Value)
                        {
                            msg.IsRead = true;
                            msg.ReadTimestamp = DateTime.Now;
                            if (!msg.IsDelivered.HasValue || !msg.IsDelivered.Value)
                            {
                                msg.IsDelivered = true;
                                msg.DeliveredTimestamp = DateTime.Now;
                            }
                        }

                        int effected = AcceptChanges();
                        if (effected > 0)
                            return EditStatus.Edited;
                        else
                            return EditStatus.RejectedByNonEffectiveQuery;
                    }
                    else
                    {
                        //security warning
                        return EditStatus.RejectedByAccessDenied;
                    }

                }
                else
                    return EditStatus.RejectedByNotExit;
            }
            catch
            {
                return EditStatus.RejectedByInternalSystemError;
            }
        }

        public DeleteStatus DeleteSnapchatMessages(DateTime startDateTime, int hour)
        {
            try
            {
                var db = this.GetPersitantStorage();




                {
                    int effected = AcceptChanges();
                    if (effected > 0)
                        return DeleteStatus.Deleted;
                    else
                        return DeleteStatus.RejectedByNonEffectiveQuery;
                }
                // else
                //    return DeleteStatus.RejectedByParentNotExit;
            }
            catch
            {
                return DeleteStatus.RejectedByInternalSystemError;
            }
        }
        public DeleteStatus DeleteMessagesFor(string userName, string otherUserName)
        {
            try
            {
                var db = this.GetPersitantStorage();
                //to do

                {/*
                    int effected = AcceptChanges();
                    if (effected > 0)
                        return DeleteStatus.Deleted;
                    else
                        return DeleteStatus.RejectedByNonEffectiveQuery;
                  * */
                }
                // else
                //    return DeleteStatus.RejectedByParentNotExit;
            }
            catch
            {
                return DeleteStatus.RejectedByInternalSystemError;
            }
            return DeleteStatus.Deleted;
        }

        public DeleteStatus DeleteMessage(string userName, Guid messageId)
        {
            try
            {
                MembershipUser user = MembershipManager.Instance.GetUser(userName);
                if (user != null)
                {
                    Message msg = GetMessage(messageId);
                    if (msg.SenderUserId.Equals(user.UserId) || msg.RecieverUserId.Equals(user.UserId))
                    {
                       

                        PermanentEvent permanentEvent = PermanentEventDispatcher.Instance.GetEventByEntityId(msg.MessageId);
                        //if (permanentEvent != null && permanentEvent.IsNotifiedToClient)
                        //  return DeleteStatus.RejectedByAccessDenied;

                        var db = this.GetPersitantStorage();
                        db.Messages.Remove(msg);
                        //permanentEvent.IsNotifiedToClient = true;//discard messages that must deliver to the reciever

                        int effected = AcceptChanges();
                        if (effected > 0)
                            if (!msg.IsDelivered.HasValue)
                                return DeleteStatus.DeletedBeforeDeliver;
                            else
                                return DeleteStatus.DeletedBeforeRead;
                        else
                            return DeleteStatus.RejectedByNonEffectiveQuery;
                    }
                    else
                    {
                        //security warning
                        return DeleteStatus.RejectedByAccessDenied;
                    }

                }
                else
                    return DeleteStatus.RejectedByNotExit;
            }
            catch
            {
                return DeleteStatus.RejectedByInternalSystemError;
            }
        }
        public DeleteStatus DeleteMessageBeforeRead(string userName, Guid messageId)
        {
            try
            {
                MembershipUser user = MembershipManager.Instance.GetUser(userName);
                if (user != null)
                {
                    Message msg = GetMessage(messageId);
                    if (msg.SenderUserId.Equals(user.UserId) || msg.RecieverUserId.Equals(user.UserId))
                    {
                        if ((msg.IsRead.HasValue && msg.IsRead.Value))
                            return DeleteStatus.RejectedByAccessDenied;

                        PermanentEvent permanentEvent = PermanentEventDispatcher.Instance.GetEventByEntityId(msg.MessageId);
                        //if (permanentEvent != null && permanentEvent.IsNotifiedToClient)
                        //  return DeleteStatus.RejectedByAccessDenied;

                        var db = this.GetPersitantStorage();
                        db.Messages.Remove(msg);
                        permanentEvent.IsNotifiedToClient = true;//discard messages that must deliver to the reciever

                        int effected = AcceptChanges();
                        if (effected > 0)
                            if (!msg.IsDelivered.HasValue)
                                return DeleteStatus.DeletedBeforeDeliver;
                            else
                                return DeleteStatus.DeletedBeforeRead;
                        else
                            return DeleteStatus.RejectedByNonEffectiveQuery;
                    }
                    else
                    {
                        //security warning
                        return DeleteStatus.RejectedByAccessDenied;
                    }

                }
                else
                    return DeleteStatus.RejectedByNotExit;
            }
            catch
            {
                return DeleteStatus.RejectedByInternalSystemError;
            }
        }

        public Message GetMessage(Guid messageId)
        {
            var db = this.GetPersitantStorage();
            return db.Messages.FirstOrDefault(e => e.MessageId.Equals(messageId));
        }

        public Conversation GetConversation(Guid conversationId)
        {
            var db = this.GetPersitantStorage();
            return db.Conversations.FirstOrDefault(e => e.ConversationId.Equals(conversationId));
        }
        public Conversation GetConversation(Guid senderUserId,Guid recieverUserId)
        {
            var db = this.GetPersitantStorage();
            var conversation= db.Conversations.FirstOrDefault(e => (e.SenderUserId.Equals(senderUserId) && e.RecieverUserId.Equals(recieverUserId))||
                (e.SenderUserId.Equals(recieverUserId) && e.RecieverUserId.Equals(senderUserId)));
            if (conversation == null)
            {
                conversation = new Conversation()
                {
                    ConversationId=Guid.NewGuid(),
                    SenderUserId = senderUserId,
                    RecieverUserId = recieverUserId,
                    ReceiverDeleted = false,
                    SenderDeleted = false,
                };
                db.Conversations.Add(conversation);
                this.AcceptChanges();
            }
            return conversation;
        }

        public IList<Message> getListOfAllUnReadMessage(string userNameFrom, string userNameTo)
        {
            var db = this.GetPersitantStorage();
            // return db.Messages.Where(e => e.MembershipUser.UserName.Equals(userNameFrom) && e.MembershipUser1.UserName.Equals(userNameTo) && (e.IsRead == false || e.IsRead==null)&& e.IsDelivered==true).ToList();
            return db.Messages.Where(e => e.MembershipUser.UserName.Equals(userNameFrom) && e.MembershipUser1.UserName.Equals(userNameTo) && (e.IsRead == false || e.IsRead == null)).ToList();
        }

     
    }
}
