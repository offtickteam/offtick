﻿using Offtick.Business.Services.Storage;
using Offtick.Business.Services.Storage.File;
using Offtick.Core.EnumTypes;
using Offtick.Data.Entities.Common.Models;
using Offtick.Data.Context.ExpertOnlinerContexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.Services.Managers
{
   
    public class NewsManager
    {
        private static NewsManager instance;
        public static NewsManager Instance
        {
            get
            {
                if (instance != null)
                    return instance;
                lock (typeof(NewsManager))
                {
                    if (instance == null)
                        instance = new NewsManager();
                    return instance;
                }
            }
        }

        private NewsManager()
        {

        }

        public Offtick.Data.Context.ExpertOnlinerContexts.News GetNews(Guid entityId)
        {
            var entities = DataContextManager.GetPersitantStorage();
            return entities.News.FirstOrDefault(e => e.NewsId.Equals(entityId));
        }

       



        public AddStatus AddFile(short languageId, string title, string body, string userName,IList<string> keywords, System.Web.HttpFileCollectionBase fileCollection)
        {
            try
            {
                DateTime now = DateTime.Now;

                for (int i = 0; i < fileCollection.Count; i++)
                {
                    var entities = DataContextManager.GetPersitantStorage();
                    var fileBase = fileCollection[i];
                    string fileName =fileBase.FileName;
                    NewsFileManager newsFM = new NewsFileManager(userName);
                    string newFileName=newsFM.Save(fileBase, fileName);


                    Offtick.Data.Context.ExpertOnlinerContexts.News entity = new News()
                    {
                        NewsId = Guid.NewGuid(),
                        UserId = Membership.MembershipManager.Instance.GetUser(userName).UserId,
                        DateTime = now,
                        ImageUrl = newFileName,
                    };

                    

                    var entityCulture = new NewsCulture()
                    {
                        CultureId = Guid.NewGuid(),
                        Body = body,
                        Title = title,
                        LangaugeId=languageId,
                    };

                    foreach (string keyword in keywords)
                        entityCulture.NewsKeywords.Add(new NewsKeyword()
                        {
                            DateTime = now,
                            KeywordId = Guid.NewGuid(),
                            Keyword = keyword,
                        });
                    entity.NewsCultures.Add(entityCulture);



                    entities.News.Add(entity);

                    int effected = entities.SaveChanges();
                    if (effected > 0)
                        
                        return AddStatus.Added;
                    else
                        return AddStatus.RejectedByNonEffectiveQuery;
                }
                return AddStatus.RejectedByNonEffectiveQuery;
            }
            catch
            {
                return AddStatus.RejectedByInternalSystemError;
            }
        }



        public Offtick.Data.Context.ExpertOnlinerContexts.File GetFile(Guid fileId)
        {
            var entities = DataContextManager.GetPersitantStorage();
            return entities.Files.FirstOrDefault(e => e.FileId.Equals(fileId));
        }

        public DeleteStatus DeleteNews(Guid newsId)
        {
            var entities = DataContextManager.GetPersitantStorage();

            var news = this.GetNews(newsId);
            if (news != null)
            {
                NewsFileManager newsFM = new NewsFileManager(news.MembershipUser.UserName);
                newsFM.Delete(news.ImageUrl);

                //file.FileCultures.Clear();
                entities.News.Remove(news);
                int effected = entities.SaveChanges();
                if (effected > 0)
                    return DeleteStatus.Deleted;
                else
                    return DeleteStatus.RejectedByNonEffectiveQuery;
            }
            return DeleteStatus.RejectedByNotExit;
        }


        public FileSearchResultItem GetVirtualPathOfImage(string fileName, Guid userId)
        {
            NewsFileManager newsFM = new NewsFileManager(userId.ToString());
            return new FileSearchResultItem(string.Empty, true, true, newsFM.GetRelativeFullFileName(userId.ToString(), fileName, true), newsFM.GetRelativeFullFileName(userId.ToString(), fileName, false));
        }


    }
}
