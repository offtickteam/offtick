﻿using Offtick.Core.EnumTypes;
using Offtick.Core.Utility;
using Offtick.Data.Context.ExpertOnlinerContexts;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.WebPages;

namespace Offtick.Business.Services.Managers
{
    public class AuthenticationManager : BaseManager
    {
        private static AuthenticationManager instance;
        public static AuthenticationManager Instance
        {
            get
            {
                if (instance != null)
                    return instance;
                lock (typeof(AuthenticationManager))
                {
                    if (instance == null)
                        instance = new AuthenticationManager();
                    return instance;
                }
            }
        }

        public IQueryable<MembershipClient> getAllOfClients()
        {
            var entities = this.GetPersitantStorage();
            return entities.MembershipClients.AsQueryable();
        }

        public MembershipClient GetMembershipClient(string clientName)
        {
            var entities = this.GetPersitantStorage();
            return entities.MembershipClients.FirstOrDefault(e => e.ClientName.ToUpper().Equals(clientName.ToUpper()));
        }
        public MembershipClient GetMembershipClient(Guid exceptThisClientId,string clientName)
        {
            var entities = this.GetPersitantStorage();
            return entities.MembershipClients.FirstOrDefault(e =>!e.ClientId.Equals(exceptThisClientId) &&  e.ClientName.ToUpper().Equals(clientName.ToUpper()));
        }

        public MembershipClient GetMembershipClient(Guid clientId)
        {
            var entities = this.GetPersitantStorage();
            return entities.MembershipClients.FirstOrDefault(e => e.ClientId.Equals(clientId));
        }

        public MembershipClient IsClientValidated(string clientName,string clientSecret)
        {
            if (clientName.IsEmpty() || clientName == null || clientSecret.IsEmpty() || clientSecret == null)
                return null;
            var entities = this.GetPersitantStorage();
            return entities.MembershipClients.FirstOrDefault(e => e.ClientName.Equals(clientName) && e.ClientSecret.Equals(clientSecret));
        }


        public AddStatus AddMembershipClient(string clientName, string clientSecret, bool active, int refreshTokenLifeTime, string allowedOrigin)
        {
            AddStatus addStatus = AddStatus.RejectedByInvalidParameter;
            var existedClient = this.GetMembershipClient(clientName);
            if (existedClient != null)
                addStatus = AddStatus.RejectedByExitBefore;
            else
            {
                var entities = this.GetPersitantStorage();
                entities.MembershipClients.Add(new MembershipClient()
                {
                    Active = active,
                    AllowedOrigin = allowedOrigin,
                    ClientName = clientName,
                    ClientSecret = clientSecret,
                    CreatedOn = DateTime.Now,
                    RefreshTokenLifeTime = refreshTokenLifeTime,
                    ClientId = Guid.NewGuid()
                });

                int effected = this.AcceptChanges();
                addStatus = effected > 0 ? AddStatus.Added : AddStatus.RejectedByNonEffectiveQuery;
            }
            return addStatus;
        }
        public EditStatus EditMembershipClient(Guid clientId, string clientName, string clientSecret, bool active, int refreshTokenLifeTime, string allowedOrigin)
        {
            EditStatus editStatus = EditStatus.RejectedByNotExit;
            var existedClient = this.GetMembershipClient(clientId);
            if (existedClient == null)
                editStatus = EditStatus.RejectedByNotExit;
            else
            {
                existedClient.ClientName = clientName;
                existedClient.Active = active;
                existedClient.AllowedOrigin = allowedOrigin;
                existedClient.ClientSecret = clientSecret;
                existedClient.RefreshTokenLifeTime = refreshTokenLifeTime;
                int effected = this.AcceptChanges();
                editStatus=effected > 0? EditStatus.Edited: EditStatus.RejectedByNonEffectiveQuery;
            }
            return editStatus;
        }





        //Add the Refresh token
        public async Task<bool> AddRefreshToken(MembershipRefreshToken refreshToken)
        {
            AddStatus addStatus = AddStatus.RejectedByInvalidParameter;

            var entities = this.GetPersitantStorage();
            var existingToken = entities.MembershipRefreshTokens.FirstOrDefault(r => r.UsesrId == refreshToken.UsesrId
                            && r.ClientId == refreshToken.ClientId);
            if (existingToken != null)
            {
                var result =  RemoveRefreshToken(existingToken);
            }
            entities.MembershipRefreshTokens.Add(refreshToken);
            int effected = this.AcceptChanges();
            addStatus = effected > 0 ? AddStatus.Added : AddStatus.RejectedByNonEffectiveQuery;
            return await Task.FromResult(true);
        }

        //Remove the Refesh Token by id
        public async Task<bool> RemoveRefreshTokenByID(string refreshTokenId)
        {
            return RemoveRefreshToken(FindRefreshToken(refreshTokenId));
        }
        //Remove the Refresh Token
        public  bool RemoveRefreshToken(MembershipRefreshToken refreshToken)
        {
            var entities = this.GetPersitantStorage();
            if (refreshToken != null)
            {
                entities.MembershipRefreshTokens.Remove(refreshToken);
                this.AcceptChanges();
                return true;
            }
            return false;
        }
        //Find the Refresh Token by token ID
        public MembershipRefreshToken FindRefreshToken(string refreshTokenId)
        {
            var entities = this.GetPersitantStorage();
            return entities.MembershipRefreshTokens.FirstOrDefault(e => e.RefreshTokenId == refreshTokenId);
        }
        //Find the Refresh Token by userName and clientId
        public MembershipRefreshToken FindRefreshToken(string userName, string clientId)
        {
            var entities = this.GetPersitantStorage();
            return entities.MembershipRefreshTokens.FirstOrDefault(e => e.MembershipUser.UserName.Equals(userName) && e.MembershipClient.ClientName.Equals(clientId));
        }

        public IQueryable<MembershipRefreshToken> GetRefreshTokens()
        {
            var entities = this.GetPersitantStorage();
            return entities.MembershipRefreshTokens.AsQueryable();
        }
        
        


    }
}
