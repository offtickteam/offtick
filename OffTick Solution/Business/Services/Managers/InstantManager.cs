﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.Services.Managers
{
    public class InstantManager
    {
        private static InstantManager instance;
        public static InstantManager Instance
        {
            get
            {
                if (instance == null)
                    lock (typeof(InstantManager))
                    {
                        if (instance == null)
                            instance = new InstantManager();

                    }
                return instance;
            }
        }
        private InstantManager()
        {
            configs = new Dictionary<Guid, InstantConfig>();
        }


        public Nullable<short> GetDefaultLangaue(Guid viewerId,string userName)
        {
            var config = configs.FirstOrDefault(e => e.Key.Equals(viewerId)).Value;
            if (config != null)
                return config.LanguageId;
            else{
                var user=Membership.MembershipManager.Instance.GetUser(userName);
                if (user != null)
                {
                    var defaultLangaue=user.MembershipLangauges.FirstOrDefault(e => e.IsDefault);
                    if (defaultLangaue != null)
                    {
                        configs.Add(viewerId, new InstantConfig() { LanguageId = defaultLangaue.LanguageId });
                        return defaultLangaue.LanguageId;
                    }
                }

                return null;
            }
            
        }

        public void SetDefaultLangauge(Guid viewerId)
        {
            try
            {
                var config = configs.First(e => e.Key.Equals(viewerId));

            }
            catch
            {
            }

        }

        private Dictionary<Guid, InstantConfig> configs;
    }

    public class InstantConfig{
        public short LanguageId{get;set;}
    }

    public class KeyValueItem<T1, T2>
    {
        public KeyValueItem(T1 key, T2 value)
        {
            this.Key = key;
            this.Value = value;
        }
        public T1 Key;
        private T2 Value;

    }
    public class Dictionary1<t1, T2>
    {
        public Dictionary1()
        {
            list = new List<KeyValueItem<t1, T2>>();
        }
        private IList<KeyValueItem<t1, T2>> list;


        
    }
}
