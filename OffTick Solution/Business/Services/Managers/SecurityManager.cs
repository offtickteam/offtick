﻿using Offtick.Business.Services.Storage;
using Offtick.Core.EnumTypes;
using Offtick.Data.Entities.Common;
using Offtick.Data.Context.ExpertOnlinerContexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.Services.Managers
{
    public class SecurityManager
    {
         private static SecurityManager instance;
        public static SecurityManager Instance
        {
            get
            {
                if (instance != null)
                    return instance;
                lock (typeof(SecurityManager))
                {
                    if (instance == null)
                        instance= new SecurityManager();
                    return instance;
                }
            }
        }

        private SecurityManager()
        {
        }

        public EditStatus UpdateGeneralSecurityForUser(MembershipUser user, bool isViewingVisible, bool isBuyingVisible, bool IsVisitingVisible, bool IsVisitorVisible)
        {
            try
            {

                if (user == null)
                    return EditStatus.RejectedByNotExit;
                var privacy = user.MembershipPrivacy;
                if (privacy == null)
                {
                    privacy = new MembershipPrivacy();
                    user.MembershipPrivacy = privacy;
                }
                privacy.IsBuyingVisible = isBuyingVisible;
                privacy.IsViewingVisible = isViewingVisible;
                privacy.IsVisitingVisible = IsVisitingVisible;
                privacy.IsVisitorVisible = IsVisitorVisible;
                privacy.LastDateTime = DateTime.Now;

                var db = DataContextManager.GetPersitantStorage();
                int effected = db.SaveChanges();
                if (effected > 0)
                    return EditStatus.Edited;
                else
                    return EditStatus.RejectedByNonEffectiveQuery;

            }
            catch
            {
                return EditStatus.RejectedByInternalSystemError;
            }

            
        }

        public EditStatus UpdateGeneralSecurityForOffer(Offer offer, bool isViewerVisible, bool isBuyerVisible, bool IsOnlyVisibleForFollowers=true)
        {
            try
            {

                if (offer == null)
                    return EditStatus.RejectedByNotExit;
                var privacy = offer.OfferPrivacy;
                if (privacy == null)
                {
                    privacy = new OfferPrivacy();
                    offer.OfferPrivacy = privacy;
                }
                privacy.IsBuyerVisible = isBuyerVisible;
                privacy.IsViewerVisible = isViewerVisible;
                privacy.IsOnlyVisibleForFollowers = IsOnlyVisibleForFollowers;
                privacy.LastDatetime = DateTime.Now;

                var db = DataContextManager.GetPersitantStorage();
                int effected = db.SaveChanges();
                if (effected > 0)
                    return EditStatus.Edited;
                else
                    return EditStatus.RejectedByNonEffectiveQuery;

            }
            catch
            {
                return EditStatus.RejectedByInternalSystemError;
            }
        }


        public EditStatus UpdateOfferSecurityForUser(Offer offer, MembershipUser user, bool isViewingVisible, bool isBuyingVisible)
        {
            try
            {

                if (offer == null || user==null)
                    return EditStatus.RejectedByNotExit;
                var privacy = offer.OfferCustomerPrivacies.FirstOrDefault(e => e.OfferId.Equals(offer.OfferId) && e.UserId.Equals(user.UserId));
                if (privacy == null)
                {
                    privacy = new OfferCustomerPrivacy()
                    {
                        OfferId=offer.OfferId,
                        UserId=user.UserId,
                    };
                }
                privacy.IsBuyingVisible = isBuyingVisible;
                privacy.IsViewingVisible = isViewingVisible;
                privacy.LastDatetime = DateTime.Now;

                var db = DataContextManager.GetPersitantStorage();
                db.OfferCustomerPrivacies.Add(privacy);
                int effected = db.SaveChanges();
                if (effected > 0)
                    return EditStatus.Edited;
                else
                    return EditStatus.RejectedByNonEffectiveQuery;

            }
            catch
            {
                return EditStatus.RejectedByInternalSystemError;
            }
        }

        public OfferCustomerPrivacy GetOfferSecurityForUser(Offer offer, MembershipUser user)
        {
            if (offer == null || user == null)
                return null;
            var privacy = offer.OfferCustomerPrivacies.FirstOrDefault(e => e.OfferId.Equals(offer.OfferId) && e.UserId.Equals(user.UserId));
            return privacy;
        }

        public IList<OfferPrivacy> GetOfferSecuritiesForUser( MembershipUser user)
        {
            if ( user == null)
                return null;
            var db = DataContextManager.GetPersitantStorage();
            return db.OfferPrivacies.Where(e => e.Offer.MembershipUser.UserId.Equals(user.UserId)).ToList();
        }
    }
}
