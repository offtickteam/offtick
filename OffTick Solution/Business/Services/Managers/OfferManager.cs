﻿using Offtick.Business.Membership;
using Offtick.Business.Services.RealtimeSearvices.EventDrivenDispatcher.Notification;
using Offtick.Business.Services.RealtimeSearvices.EventDrivenService;
using Offtick.Business.Services.Storage.File;
using Offtick.Core.EnumTypes;
using Offtick.Data.Context.ExpertOnlinerContexts;
using System;
using System.Collections.Generic;
//using System.Data.Objects;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Offtick.Core.Exceptions;
using Offtick.Data.Entities.Common;
using Offtick.Business.Web;
using Offtick.Business.Services.Domain.PdfGenerator;
using Offtick.Core.Utility;

namespace Offtick.Business.Services.Managers
{
   

    public class OfferManager : BaseManager
    {
        private static OfferManager instance;
        public static OfferManager Instance
        {
            get
            {
                if (instance != null)
                    return instance;
                lock (typeof(OfferManager))
                {
                    if (instance == null)
                        instance = new OfferManager();
                    return instance;
                }
            }
        }

        public AddStatus SaveOffer(Offer offer)
        {
            try
            {
                MembershipUser user = MembershipManager.Instance.GetUser(offer.SenderUserId);
                if (offer != null)
                {

                    user.Offers.Add(offer);
                    int effected = AcceptChanges();
                    if (effected > 0)
                        return AddStatus.Added;
                    else
                        return AddStatus.RejectedByNonEffectiveQuery;
                }
                else
                    return AddStatus.RejectedByParentNotExit;
            }
            catch
            {
                return AddStatus.RejectedByInternalSystemError;
            }
        }


        public AddStatus ReportOffer(Offer offer, MembershipUser user,string reportBody)
        {
            try
            {

                if (offer == null || user == null)
                    return AddStatus.RejectedByInvalidParameter;

                offer.OfferReports.Add(new OfferReport()
                {
                     Body=reportBody,
                      Datetime=DateTime.Now,
                       MembershipUser=user
                });

                
                int effected = AcceptChanges();
                if (effected > 0)
                    return AddStatus.Added;
                else
                    return AddStatus.RejectedByNonEffectiveQuery;

            }
            catch
            {
                return AddStatus.RejectedByInternalSystemError;
            }
        }

        public EditStatus UpdateOffer(Guid offerId, string title, string description, short quantity, bool isBuyerVisible, bool isViewerVisible, bool IsOnlyVisibleForFollowers)
        {
            try
            {
                
               
                    var offer=this.GetOffer(offerId);
                    offer.Title = title;
                    offer.Body = description;

                    var foodOffer=offer.OfferFoods.FirstOrDefault();
                    if (foodOffer != null)
                    {
                        foodOffer.Quantity = quantity;
                    }

                    SecurityManager.Instance.UpdateGeneralSecurityForOffer(offer, isViewerVisible, isBuyerVisible,IsOnlyVisibleForFollowers);
                    int effected = AcceptChanges();
                    if (effected > 0)
                        return EditStatus.Edited;
                    else
                        return EditStatus.RejectedByNonEffectiveQuery;
               
            }
            catch
            {
                return EditStatus.RejectedByInternalSystemError;
            }
        }





        public EditStatus UpdateOffer(Guid offerId

                                   , string offerTitle
                                   , string offerTitleLong
                                   )
        {
            try
            {


                var offer = this.GetOffer(offerId);
                offer.Title = offerTitle;
                offer.Body = offerTitleLong;
               // offer.LocationAddress = offerLocationAddress;

                int effected = AcceptChanges();
                if (effected > 0)
                    return EditStatus.Edited;
                else
                    return EditStatus.RejectedByNonEffectiveQuery;

            }
            catch
            {
                return EditStatus.RejectedByInternalSystemError;
            }
        }
        public EditStatus UpdateOfferDetailedCode(Guid offerId

                                 , int detailedCode
                                 )
        {
            try
            {
                var offer = this.GetOffer(offerId);
                if (offer != null)
                    offer.DetailedCode = detailedCode;
                else
                    return EditStatus.RejectedByNotExit;

                int effected = AcceptChanges();
                if (effected > 0)
                    return EditStatus.Edited;
                else
                    return EditStatus.RejectedByNonEffectiveQuery;
            }
            catch
            {
                return EditStatus.RejectedByInternalSystemError;
            }
        }


        public EditStatus UpdateOffer(Guid offerId 

                                    ,string offerTitle 
                                    ,string offerTitleLong 
                                    ,string url
                                    ,long offerOriginalPrice 
                                    ,string offerOriginalTitle 
                                    ,string offerUnit 
                                    ,long offerDiscountPrice 
                                    ,string offerDiscountTitle 
                                    ,short offerDiscountPercent 
                                    ,int offerExpireDate 
                                    ,short offerQuantity 
                                    ,string offerAudeiences 
                                    ,string offerLocationCityId 
                                    ,string offerLocationAddress 
                                    ,string offerSpecification
                                    ,string offerConditions
                                    ,string offerDescriptions 
                                    ,bool showIncomeInPercentage 
                                    ,bool offerHasAddedValueTax )
        {
            try
            {


                var offer = this.GetOffer(offerId);
                offer.Title = offerTitle;
                offer.Body = offerTitleLong;
                offer.LocationAddress = offerLocationAddress;
                offer.Url = url;
                

                var offtickOffer = offer.OfferFoods.FirstOrDefault();
                if (offtickOffer != null)
                {
                    offtickOffer.OriginalPrice = offerOriginalPrice;
                    offtickOffer.OriginalPriceTitle = offerOriginalTitle;
                    offtickOffer.DiscountPriceUnit = offerUnit;
                    offtickOffer.DiscountPrice = offerDiscountPrice;
                    offtickOffer.DiscountPriceTitle = offerDiscountTitle;
                    offtickOffer.DiscountPercent = offerDiscountPercent;
                    offtickOffer.TimeToExpire = offerExpireDate;
                    offtickOffer.Quantity = offerQuantity;
                    //offtickOffer.au = offerDiscountPercent;
                    offtickOffer.Specifications = offerSpecification;
                    offtickOffer.Conditions = offerConditions;
                    offtickOffer.Descriptions = offerDescriptions;
                    offtickOffer.ShowIncomeInPercentage = showIncomeInPercentage;
                    offtickOffer.AddedValueTax = offerHasAddedValueTax ? (Nullable<short>) 9 : null;
                }
                int effected = AcceptChanges();
                if (effected > 0)
                    return EditStatus.Edited;
                else
                    return EditStatus.RejectedByNonEffectiveQuery;

            }
            catch
            {
                return EditStatus.RejectedByInternalSystemError;
            }
        }

        public EditStatus UploadOfferFile(string userName, string offerId, string fileType, string fileData, out string returnFileName)
        {

            Guid msgId;
            returnFileName = string.Empty;
            if (Guid.TryParse(offerId, out msgId))
            {
                var offer = Instance.GetOffer(msgId);

                if (offer != null)
                {
                    if (!offer.MembershipUser.UserName.Equals(userName))
                        return EditStatus.RejectedByAccessDenied;

                    //to do: the bellow work just for images, must add other solutions to work for every file type 
                    byte[] byteArrayOfImage = Convert.FromBase64String(fileData);
                    ImageFileManager imgFileManager = new ImageFileManager(offer.MembershipUser.UserId.ToString());
                    var image = imgFileManager.GetImageFromByteArray(byteArrayOfImage);
                    string fileName = Guid.NewGuid().ToString() + "." + fileType;
                    string savedFileName = imgFileManager.Save(image, fileName);
                    //get message and update its FileName, then create Event

                    offer.FileName = savedFileName;

                    int effected = AcceptChanges();
                    if (effected > 0)
                    {
                        returnFileName = savedFileName;
                        if (offer.MembershipUser.UserName != offer.MembershipUser1.UserName)//means that : it 's not an POST offer, or sender or receiver is one person
                        {
                            PermanentEventDispatcher.Instance.CreateEventSendOffer(msgId, offer.MembershipUser1.UserId, null);
                        }
                        return EditStatus.Edited;
                    }
                    else
                        return EditStatus.RejectedByNonEffectiveQuery;
                }
            }
            return EditStatus.RejectedByNotExit;
        }

        public EditStatus UploadOfferFile(string userName, string offerId, System.Web.HttpFileCollectionBase files, out string returnFileName)
        {
            if (files != null)
                return UploadOfferFile(userName, offerId, files[0], out returnFileName);
            else
            {
                returnFileName = "";
                return EditStatus.RejectedByInternalSystemError;
            }
        }

         public EditStatus UploadOfferFile(string userName, string offerId, System.Web.HttpPostedFileBase file, out string returnFileName)
        {

            Guid msgId;
            returnFileName = string.Empty;
            if (Guid.TryParse(offerId, out msgId))
            {
                var offer = Instance.GetOffer(msgId);

                if (offer != null)
                {
                    if (!offer.MembershipUser.UserName.Equals(userName))
                        return EditStatus.RejectedByAccessDenied;

                    EnumOfferType offerType = (EnumOfferType)offer.OfferType;
                    string newFileName = string.Empty;
                    var fileBase = file;
                    string fileName = fileBase.FileName;

                    if (offerType == EnumOfferType.Post || offerType == EnumOfferType.Food || offerType== EnumOfferType.Tour)
                    {
                        //to do: the bellow work just for images, must add other solutions to work for every file type 
                        ImageFileManager imageManager = new ImageFileManager(offer.MembershipUser.UserId.ToString());
                        newFileName = imageManager.Save(fileBase, fileName);
                    }
                    else
                    {
                        VideoFileManager videoManager = new VideoFileManager(offer.MembershipUser.UserId.ToString());
                        newFileName = videoManager.Save(fileBase, fileName);
                    }

                    //get message and update its FileName, then create Event
                    offer.FileName = newFileName;

                    int effected = AcceptChanges();
                    if (effected > 0)
                    {
                        if (offerType != EnumOfferType.Video)
                        {

                            //add post offer to default gallery
                            var defaultGallery = GalleryManager.Instance.GetDefaultGallery(offer.MembershipUser, true);
                            offer.GalleryOffers.Add(new GalleryOffer()
                            {
                                DateTime = DateTime.Now,
                                Gallery = defaultGallery,
                            });
                            //end of adding  post offer to default gallery
                        }

                        //start of adding security

                        bool justToFollowers = (offerType == EnumOfferType.Food || offerType == EnumOfferType.Video) ? false : true;
                        SecurityManager.Instance.UpdateGeneralSecurityForOffer(offer, true, true, justToFollowers);
                        //end of adding security

                        AcceptChanges();
                        



                        returnFileName = newFileName;
                        if (offer.MembershipUser.UserName != offer.MembershipUser1.UserName)//means that : it 's not an POST offer, or sender or receiver is one person
                        {
                            PermanentEventDispatcher.Instance.CreateEventSendOffer(msgId, offer.MembershipUser1.UserId, null);
                        }
                        return EditStatus.Edited;
                    }
                    else
                        return EditStatus.RejectedByNonEffectiveQuery;
                }
            }
            return EditStatus.RejectedByNotExit;
        }

        public string DownloadOfferFile(string userName, string offerId, out string fileName)
        {
            return DownloadOfferFile(userName, offerId, out fileName, false);
        }
        public string DownloadOfferFile(string userName, string offerId, out string fileName, bool isThumbnail)
        {

            Guid msgId;
            if (Guid.TryParse(offerId, out msgId))
            {
                var offer = Instance.GetOffer(msgId);
                if (offer != null && offer.MembershipUser.UserName.Equals(userName))
                {


                    ImageFileManager imgFileManager = new ImageFileManager(offer.MembershipUser.UserId.ToString());
                    byte[] bytes = imgFileManager.GetBytesOfImage(imgFileManager.GetFullFileName(offer.FileName, isThumbnail));
                    fileName = offer.FileName;
                    return Convert.ToBase64String(bytes);
                }
            }
            fileName = string.Empty;
            return string.Empty;
        }



        public EditStatus ArchiveOffer(string userName, Guid offerId)
        {
            try
            {
                MembershipUser user = MembershipManager.Instance.GetUser(userName);
                if (user != null)
                {
                    Offer msg = GetOffer(offerId);
                    if (msg.SenderUserId.Equals(user.UserId) || msg.RecieverUserId.Equals(user.UserId))
                    {
                        //msg.IsPermanent = true;
                        int effected = AcceptChanges();
                        if (effected > 0)
                            return EditStatus.Edited;
                        else
                            return EditStatus.RejectedByNonEffectiveQuery;
                    }
                    else
                    {
                        //security warning
                        return EditStatus.RejectedByAccessDenied;
                    }

                }
                else
                    return EditStatus.RejectedByNotExit;
            }
            catch
            {
                return EditStatus.RejectedByInternalSystemError;
            }
        }
        public EditStatus OfferDelivered(string userName, Guid offerId)
        {
            try
            {
                MembershipUser user = MembershipManager.Instance.GetUser(userName);
                if (user != null)
                {
                    Offer msg = GetOffer(offerId);
                    if (msg.SenderUserId.Equals(user.UserId) || msg.RecieverUserId.Equals(user.UserId))
                    {
                        if (!msg.IsDelivered.HasValue || !msg.IsDelivered.Value)
                        {
                            msg.IsDelivered = true;
                            msg.DeliveredTimestamp = DateTime.Now;
                        }
                        int effected = AcceptChanges();
                        if (effected > 0)
                            return EditStatus.Edited;
                        else
                            return EditStatus.RejectedByNonEffectiveQuery;
                    }
                    else
                    {
                        //security warning
                        return EditStatus.RejectedByAccessDenied;
                    }

                }
                else
                    return EditStatus.RejectedByNotExit;
            }
            catch
            {
                return EditStatus.RejectedByInternalSystemError;
            }
        }
        public EditStatus OfferReaded(string userName, Guid offerId)
        {
            try
            {
                MembershipUser user = MembershipManager.Instance.GetUser(userName);
                if (user != null)
                {
                    Offer msg = GetOffer(offerId);
                    if (msg.SenderUserId.Equals(user.UserId) || msg.RecieverUserId.Equals(user.UserId))
                    {
                        DateTime now = DateTime.Now;
                        if (!msg.IsRead.HasValue || !msg.IsRead.Value)
                        {
                            msg.IsRead = true;
                            msg.ReadTimestamp = DateTime.Now;
                            if (!msg.IsDelivered.HasValue || !msg.IsDelivered.Value)
                            {
                                msg.IsDelivered = true;
                                msg.DeliveredTimestamp = DateTime.Now;
                            }
                        }

                        int effected = AcceptChanges();
                        if (effected > 0)
                            return EditStatus.Edited;
                        else
                            return EditStatus.RejectedByNonEffectiveQuery;
                    }
                    else
                    {
                        //security warning
                        return EditStatus.RejectedByAccessDenied;
                    }

                }
                else
                    return EditStatus.RejectedByNotExit;
            }
            catch
            {
                return EditStatus.RejectedByInternalSystemError;
            }
        }

        public DeleteStatus DeleteSnapchatOffers(DateTime startDateTime, int hour)
        {
            try
            {
                var db = this.GetPersitantStorage();




                {
                    int effected = AcceptChanges();
                    if (effected > 0)
                        return DeleteStatus.Deleted;
                    else
                        return DeleteStatus.RejectedByNonEffectiveQuery;
                }
                // else
                //    return DeleteStatus.RejectedByParentNotExit;
            }
            catch
            {
                return DeleteStatus.RejectedByInternalSystemError;
            }
        }
        public DeleteStatus DeleteOffersFor(string userName, string otherUserName)
        {
            try
            {
                var db = this.GetPersitantStorage();
                //to do

                {/*
                    int effected = AcceptChanges();
                    if (effected > 0)
                        return DeleteStatus.Deleted;
                    else
                        return DeleteStatus.RejectedByNonEffectiveQuery;
                  * */
                }
                // else
                //    return DeleteStatus.RejectedByParentNotExit;
            }
            catch
            {
                return DeleteStatus.RejectedByInternalSystemError;
            }
            return DeleteStatus.Deleted;
        }
        public DeleteStatus DeleteOfferBeforeRead(string userName, Guid offerId)
        {
            try
            {
                MembershipUser user = MembershipManager.Instance.GetUser(userName);
                if (user != null)
                {
                    Offer msg = GetOffer(offerId);
                    if (msg.SenderUserId.Equals(user.UserId) || msg.RecieverUserId.Equals(user.UserId))
                    {
                        if ((msg.IsRead.HasValue && msg.IsRead.Value) || (msg.CountOfVisited > 0))
                            return DeleteStatus.RejectedByAccessDenied;

                        PermanentEvent permanentEvent = PermanentEventDispatcher.Instance.GetEventByEntityId(msg.OfferId);
                        //if (permanentEvent != null && permanentEvent.IsNotifiedToClient)
                        //  return DeleteStatus.RejectedByAccessDenied;

                        var db = this.GetPersitantStorage();
                        db.Offers.Remove(msg);
                        permanentEvent.IsNotifiedToClient = true;//discard messages that must deliver to the reciever

                        int effected = AcceptChanges();
                        if (effected > 0)
                            if (!msg.IsDelivered.HasValue)
                                return DeleteStatus.DeletedBeforeDeliver;
                            else
                                return DeleteStatus.DeletedBeforeRead;
                        else
                            return DeleteStatus.RejectedByNonEffectiveQuery;
                    }
                    else
                    {
                        //security warning
                        return DeleteStatus.RejectedByAccessDenied;
                    }

                }
                else
                    return DeleteStatus.RejectedByNotExit;
            }
            catch
            {
                return DeleteStatus.RejectedByInternalSystemError;
            }
        }

        public DeleteStatus DeleteOffer(string userName, Guid offerId)
        {
            try
            {
                MembershipUser user = MembershipManager.Instance.GetUser(userName);

                if (user != null)
                {
                    string roleName = user != null ? user.MembershipUsersInRoles.First().MembershipRole.RoleName : string.Empty;
                    Offer msg = GetOffer(offerId);
                    if (msg.SenderUserId.Equals(user.UserId) || msg.RecieverUserId.Equals(user.UserId) || roleName == "Administrator")
                    {
                        //  if ((msg.IsRead.HasValue && msg.IsRead.Value) || (msg.CountOfVisited > 0))
                        //     return DeleteStatus.RejectedByAccessDenied;

                        PermanentEvent permanentEvent = PermanentEventDispatcher.Instance.GetEventByEntityId(msg.OfferId);
                        //if (permanentEvent != null && permanentEvent.IsNotifiedToClient)
                        //  return DeleteStatus.RejectedByAccessDenied;

                        var db = this.GetPersitantStorage();
                        db.Offers.Remove(msg);
                        if (permanentEvent != null)
                            permanentEvent.IsNotifiedToClient = true;//discard messages that must deliver to the reciever

                        int effected = AcceptChanges();
                        if (effected > 0)
                            return DeleteStatus.Deleted;
                        else
                            return DeleteStatus.RejectedByNonEffectiveQuery;
                    }
                    else
                    {
                        //security warning
                        return DeleteStatus.RejectedByAccessDenied;
                    }

                }
                else
                    return DeleteStatus.RejectedByNotExit;
            }
            catch
            {
                return DeleteStatus.RejectedByInternalSystemError;
            }
        }


        public long GetCountOfOfferVisited(Guid offerId)
        {
            var offer = GetOffer(offerId);
            if (offer != null)
                return offer.CountOfVisited;
            else
                return 0;
        }

#region Get_Offers

        


        public IList<Offer> getFollowingOffersForUser(MembershipUser user, int skip, int take,MembershipUser currentUser=null)
        {
            IList<Offer> lsResult = null;
            if (user != null)
            {
                var entities = this.GetPersitantStorage();
                var query = from o in entities.Offers
                            join f in entities.Follows
                            on o.SenderUserId equals f.MembershipUser.UserId
                            where o.SenderUserId == o.RecieverUserId
                            && f.FollowersUserId == user.UserId
                            
                            select o;
                lsResult = query.Skip(skip).Take(take).ToList();

            }
            return lsResult;
        }

        /// <summary>
        /// returns profile offers of a user + private offers if current user follow the user
        /// </summary>
        /// <param name="user"></param>
        /// <param name="skip"></param>
        /// <param name="take"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        public IList<Offer> GetPublicOffersFor(MembershipUser user, int skip, int take, MembershipUser currentUser = null)
        {
            var query = GetPublicOffersFor(user, currentUser);
            if (query != null)
                return query.Skip(skip).Take(take).ToList();
            return null;
        }

        public IQueryable<Offer> GetPublicOffersFor(MembershipUser user, MembershipUser currentUser = null)
        {
            IQueryable<Offer> lsResult = null;
            Guid userId = currentUser != null ? currentUser.UserId : Guid.Empty;

            if (user != null && (user.MembershipUserStatus.FirstOrDefault().MembershipStatu.StatusId == (short)MembershipStatus.Active || user.UserId.Equals(userId)))//means this offer bellong to user ( in this way not important that user is active or not) or offer belong to an active user
            {
                var query = user.Offers.Where(e => e.SenderUserId.Equals(e.RecieverUserId)
                      && (new short[] { (short)EnumOfferType.Post, (short)EnumOfferType.Food, (short)EnumOfferType.Video }).Contains(e.OfferType)
                      && (!e.OfferPrivacy.IsOnlyVisibleForFollowers || (currentUser != null && e.MembershipUser.Follows.FirstOrDefault(a => a.FollowersUserId.Equals(userId)) != null))

                    );
                lsResult = query.OrderByDescending(e => e.ConfirmDateTime).AsQueryable();
            }
            return lsResult;
        }

        /// <summary>
        /// return wall offer of a user in which
        /// 1. contain all of user offer
        /// 2. offers from a user that the user follow him/her in which 
        ///     2.1 user must be active
        ///     2.2 if it is an offtick offer, then offer must not be expire
        ///     2.3 if a post offer add it to result
        /// </summary>
        /// <param name="user"></param>
        /// <param name="skip"></param>
        /// <param name="take"></param>
        /// <returns></returns>
        public IList<Offer> getWallOffersForUser(MembershipUser user, int skip, int take)
        {
            if (user == null)
                return null;
            return  getWallOffersForUser(user).Skip(skip).Take(take).ToList();
        }

        public IQueryable<Offer> getWallOffersForUser(MembershipUser user)
        {
            if (user == null)
                return null;
            IQueryable<Offer> lsResult = null;
            var userId = user.UserId;

            var entities = this.GetPersitantStorage();
            var query = from o in entities.Offers.AsEnumerable()
                        where
                        o.SenderUserId == o.RecieverUserId
                        && (o.OfferType == (short)EnumOfferType.Food || o.OfferType == (short)EnumOfferType.Post || o.OfferType == (short)EnumOfferType.Video || o.OfferType == (short)EnumOfferType.Tour)
                        &&
                        (o.SenderUserId == userId
                                ||
                                (
                                    o.MembershipUser.MembershipUserStatus.FirstOrDefault().MembershipStatu.StatusId == (short)MembershipStatus.Active
                                    && o.MembershipUser.Follows.FirstOrDefault(e => e.FollowersUserId.Equals(userId)) != null
                                   && ((o.OfferType == (short)EnumOfferType.Food
                                           && o.OfferFoods.AsEnumerable().FirstOrDefault(f => !f.TimeToExpire.HasValue || (f.TimeToExpire.HasValue && DateTime.Now < o.DateTime.AddHours(f.TimeToExpire.Value))) != null)
                                      || o.OfferType == (short)EnumOfferType.Post
                                      || o.OfferType == (short)EnumOfferType.Video
                                      || o.OfferType == (short)EnumOfferType.Tour
                                      )
                                 )
                        )
                        select o;
            lsResult = query.OrderByDescending(e => e.ConfirmDateTime).AsQueryable();
            return lsResult;
        }

        public IList<Offer> getMainPageFoodOffers(int skip, int take, Guid? cityId = null, MembershipUser currentUser = null)
        {
            IList<Offer> lsResult = null;
            var query = getMainPageFoodOffers(cityId, currentUser);
            lsResult = query.OrderByDescending(e => e.ConfirmDateTime).Skip(skip).Take(take).ToList();
            return lsResult;
        }

        public IQueryable<Offer> getMainPageFoodOffers( Guid? cityId = null, MembershipUser currentUser = null)
        {
            Guid userId = currentUser != null ? currentUser.UserId : Guid.Empty;
            Guid guidCityId = cityId != null && cityId.HasValue ? cityId.Value : Guid.Empty;

            var entities = this.GetPersitantStorage();
            var query = from o in entities.Offers.AsEnumerable()
                        where
                        o.SenderUserId == o.RecieverUserId
                        && (cityId == null && !cityId.HasValue ? true : (o.LocationCityId.HasValue && o.LocationCityId.Value.Equals(cityId.Value)))
                        && (o.OfferType == (short)EnumOfferType.Food || o.OfferType == (short)EnumOfferType.Post || o.OfferType == (short)EnumOfferType.Video || o.OfferType == (short)EnumOfferType.Tour)
                        && (o.ConfirmStatusId == (short)ConfirmStatus.Confirmed)
                        && (o.SenderUserId == userId
                            || (
                                     o.MembershipUser.MembershipUserStatus.FirstOrDefault().MembershipStatu.StatusId == (short)MembershipStatus.Active
                                     && o.MembershipUser.ProfileSetting != null && o.MembershipUser.ProfileSetting.PublishOnMainPageConfirmStatusId == (short)ConfirmStatus.Confirmed
                                    && (!o.OfferPrivacy.IsOnlyVisibleForFollowers ||
                                                                                    (currentUser != null
                                                                                    && o.OfferPrivacy != null
                                                                                    && o.OfferPrivacy.IsOnlyVisibleForFollowers
                                                                                    && (from f in o.MembershipUser.Follows where f.FollowersUserId == userId select f).Count() > 0))
                                    &&
                                    ((o.OfferType == (short)EnumOfferType.Food
                                        && o.OfferFoods.AsEnumerable().FirstOrDefault(f => f.OfferStatusId == (int)OfferExecutionStatus.Running && (!f.TimeToExpire.HasValue || (f.TimeToExpire.HasValue && DateTime.Now < o.DateTime.AddHours(f.TimeToExpire.Value)))) != null)
                                    ||
                                    (o.OfferType == (short)EnumOfferType.Tour
                                        && o.OfferTours.AsEnumerable().FirstOrDefault(f => f.OfferStatusId == (int)OfferExecutionStatus.Running) != null)
                                     || o.OfferType == (short)EnumOfferType.Post
                                     || o.OfferType == (short)EnumOfferType.Video
                                     )
                                )
                        )

                        select o;


           



            return query.AsQueryable<Offer>();
        }

        public IQueryable<OfferTour> getMainPageTourOffers(Guid? cityId = null, MembershipUser currentUser = null)
        {
            Guid userId = currentUser != null ? currentUser.UserId : Guid.Empty;
            Guid guidCityId = cityId != null && cityId.HasValue ? cityId.Value : Guid.Empty;

            var entities = this.GetPersitantStorage();
            var query = from o in entities.Offers.AsEnumerable()
                        where
                        o.SenderUserId == o.RecieverUserId
                        && (cityId == null && !cityId.HasValue ? true : (o.LocationCityId.HasValue && o.LocationCityId.Value.Equals(cityId.Value)))
                        && (o.OfferType == (short)EnumOfferType.Tour)
                        && (o.ConfirmStatusId == (short)ConfirmStatus.Confirmed)
                        && (o.SenderUserId == userId
                            || (
                                     o.MembershipUser.MembershipUserStatus.FirstOrDefault().MembershipStatu.StatusId == (short)MembershipStatus.Active
                                     && o.MembershipUser.ProfileSetting != null && o.MembershipUser.ProfileSetting.PublishOnMainPageConfirmStatusId == (short)ConfirmStatus.Confirmed
                                    && (!o.OfferPrivacy.IsOnlyVisibleForFollowers ||
                                                                                    (currentUser != null
                                                                                    && o.OfferPrivacy != null
                                                                                    && o.OfferPrivacy.IsOnlyVisibleForFollowers
                                                                                    && (from f in o.MembershipUser.Follows where f.FollowersUserId == userId select f).Count() > 0))
                                    &&
                                    
                                    o.OfferType == (short)EnumOfferType.Tour
                                        && o.OfferTours.AsEnumerable().FirstOrDefault(f => f.OfferStatusId == (int)OfferExecutionStatus.Running) != null
                                    
                                     
                                )
                        )

                        select o.OfferTours.FirstOrDefault();






            return query.AsQueryable<OfferTour>();
        }


        public IList<Offer> getSimularOffers(Offer offer, int skip, int take)
        {
            var query = getSimularOffers(offer);
            return query.Skip(skip).Take(take).ToList();
        }

        public IQueryable<Offer> getSimularOffers(Offer offer)
        {
            
            var entities = this.GetPersitantStorage();
            var query = from o in entities.Offers.AsEnumerable()
                        where
                        o.OfferType == offer.OfferType
                        && o.OfferId != offer.OfferId
                        select o
                        ;
            return query.OrderByDescending(e => e.ConfirmDateTime).AsQueryable();
        }



        public IList<Offer> getOffersByCriteria(string[] searchCriterias, int take, int skip, MembershipUser currentUser = null)
        {
            Guid userId = currentUser != null ? currentUser.UserId : Guid.Empty;

            var db = this.GetPersitantStorage();
            Expression<Func<Offer, bool>> ex = null;
            foreach (var criteria in searchCriterias)
                ex = ex.Or(e => e.Title.Contains(criteria)
                    || e.Body.Contains(criteria)
                    || e.LocationAddress.Contains(criteria)
                    );

            //ex = ex.And(e => !e.OfferPrivacy.IsOnlyVisibleForFollowers || e.SenderUserId == userId||  e.MembershipUser.Follows.FirstOrDefault(f=> f.FollowersUserId==userId)!=null);
            
            return db.Offers.Where(ex).AsEnumerable().Where(e => (!e.OfferPrivacy.IsOnlyVisibleForFollowers || e.SenderUserId == userId||  e.MembershipUser.Follows.FirstOrDefault(f=> f.FollowersUserId==userId)!=null)
                &&   e.MembershipUser.MembershipUserStatus.FirstOrDefault().MembershipStatu.StatusId== (short)MembershipStatus.Active).OrderByDescending(e => e.ConfirmDateTime).Skip(skip).Take(take).ToList();
        }
        public IList<Offer> getOffersByCriteria(string[] searchCriterias, int take, int skip, EnumOfferType offerType, MembershipUser currentUser = null)
        {
            Guid userId = currentUser != null ? currentUser.UserId : Guid.Empty;

            var db = this.GetPersitantStorage();
            Expression<Func<Offer, bool>> ex = null;
            if (searchCriterias == null)
            {
                ex = ex.Or(e => e.OfferType == (short)offerType );
            }
            else
            foreach (var criteria in searchCriterias)
                ex = ex.Or(e => (e.Title.Contains(criteria)
                    || e.Body.Contains(criteria)
                    || e.LocationAddress.Contains(criteria))
                    && e.OfferType==(short)offerType
                    );


            return db.Offers.Where(ex).AsEnumerable().Where(e =>( !e.OfferPrivacy.IsOnlyVisibleForFollowers || e.SenderUserId == userId || e.MembershipUser.Follows.FirstOrDefault(f => f.FollowersUserId == userId) != null)
                 && e.MembershipUser.MembershipUserStatus.FirstOrDefault().MembershipStatu.StatusId == (short)MembershipStatus.Active).OrderByDescending(e => e.ConfirmDateTime).Skip(skip).Take(take).ToList();

        }

        public IQueryable<Offer> getOffersByCriteria(string[] searchCriterias
            , Nullable<EnumOfferType> offerType
            , Nullable<RoleType> roleType
            , Nullable<ConfirmStatus> offerStatus
            ,bool reportedOffers)
        {
            var db = this.GetPersitantStorage();
            Expression<Func<Offer, bool>> ex = null;
            if (searchCriterias != null)
            {
                foreach (var criteria in searchCriterias)
                    ex = ex.Or(e => (e.Title.Contains(criteria)
                        || e.Body.Contains(criteria)
                        || e.LocationAddress.Contains(criteria))
                        );
            }
            if (roleType.HasValue)
                ex = ex.And(e => e.MembershipUser.MembershipUsersInRoles.FirstOrDefault(f => f.RoleId.Equals((short)roleType.Value)) != null);

            if (offerType.HasValue)
               ex=  ex.And(e=>e.OfferType==(short)offerType);
           
            if (offerStatus.HasValue)
                ex=ex.And(e => e.ConfirmStatu.ConfirmStatusId == (int)offerStatus);
            if (reportedOffers)
               ex= ex.And(e => e.OfferReports.FirstOrDefault() != null);

            //return db.Offers.Where(ex).OrderByDescending(e => e.DateTime).Skip(skip).Take(take).ToList();
            if(ex!=null)
                return db.Offers.Where(ex).OrderByDescending(e => e.ConfirmDateTime);
            else
            return db.Offers.OrderByDescending(e => e.ConfirmDateTime);

        }
        public IList<Offer> getFoodOffersByCriteria(int take, int skip, string[] resturanName, string[] foodTitle, string[] address, Guid? cityId, MembershipUser currentUser = null, short? offtickOfferTypeId=null)
        {
            Guid userId = currentUser != null ? currentUser.UserId : Guid.Empty;

            var db = this.GetPersitantStorage();
            Expression<Func<Offer, bool>> ex = null;
            if (resturanName != null)
                foreach (var criteria in resturanName)
                {
                    if (!string.IsNullOrEmpty(criteria.Trim()))
                    {
                        ex = ex.Or(e => e.MembershipUser.FirstName.Contains(criteria)
                            || e.MembershipUser.FirstName.Contains(criteria)
                            );
                    }
                }

            if (foodTitle != null)
                foreach (var criteria in foodTitle)
                {
                    if (!string.IsNullOrEmpty(criteria.Trim()))
                    {
                        ex = ex.Or(e => e.Title.Contains(criteria)
                            || e.Body.Contains(criteria)
                            );
                    }
                }
            if (address != null)
                foreach (var criteria in address)
                {
                    if (!string.IsNullOrEmpty(criteria.Trim()))
                    {
                        ex = ex.Or(e => e.LocationAddress.Contains(criteria)
                            );
                    }
                }
            if (cityId.HasValue)
            {
                ex = ex.Or(e => e.LocationCityId.HasValue && e.LocationCityId.Value.Equals(cityId.Value)
                    );
            }

            ex = ex.And(e => e.OfferType == (short)EnumOfferType.Food);

          

            

           // ex = ex.And(e => !e.OfferPrivacy.IsOnlyVisibleForFollowers || (currentUser != null && e.OfferPrivacy.IsOnlyVisibleForFollowers && e.MembershipUser.Follows.FirstOrDefault(m => m.UserId == userId) != null));

            var entities= db.Offers.Where(ex).AsEnumerable().Where(e => (!e.OfferPrivacy.IsOnlyVisibleForFollowers || e.SenderUserId == userId || e.MembershipUser.Follows.FirstOrDefault(f => f.FollowersUserId == userId) != null)
                 && e.MembershipUser.MembershipUserStatus.FirstOrDefault().MembershipStatu.StatusId == (short)MembershipStatus.Active && e.ConfirmStatusId==(int)ConfirmStatus.Confirmed );
              if (offtickOfferTypeId.HasValue)
            {
                
                  entities=entities.Where(e=>e.OfferFoods.FirstOrDefault(a=>a.OfftickOfferTypeId==offtickOfferTypeId.Value)!=null);
              }
            return entities.OrderByDescending(e => e.ConfirmDateTime).Skip(skip).Take(take).ToList();

          

        }

        public Offer GetOffer(Guid offerId)
        {
            var db = this.GetPersitantStorage();
            return db.Offers.FirstOrDefault(e => e.OfferId.Equals(offerId));
        }

        public OfferFood GetOfferFood(long offerFoodId)
        {
            var db = this.GetPersitantStorage();
            return db.OfferFoods.FirstOrDefault(e => e.OfferFoodId.Equals(offerFoodId));
        }


        public IList<Offer> getListOfAllUnReadOffer(string userNameFrom, string userNameTo)
        {
            var db = this.GetPersitantStorage();
            // return db.Messages.Where(e => e.MembershipUser.UserName.Equals(userNameFrom) && e.MembershipUser1.UserName.Equals(userNameTo) && (e.IsRead == false || e.IsRead==null)&& e.IsDelivered==true).ToList();
            return db.Offers.Where(e => e.MembershipUser.UserName.Equals(userNameFrom) && e.MembershipUser1.UserName.Equals(userNameTo) && (e.IsRead == false || e.IsRead == null)).ToList();
        }

#endregion




        public long LikeOrDislike(MembershipUser user, Offer offer)
        {
            if (user != null && offer != null)
            {
                var offerLiked = offer.OfferLikes.FirstOrDefault(e => e.UserId.Equals(user.UserId));
                var entities = this.GetPersitantStorage();
                if (offerLiked == null)
                {

                    entities.OfferLikes.Add(new OfferLike()
                    {
                        OfferId = offer.OfferId,
                        Datetime = DateTime.Now,
                        UserId = user.UserId

                    });
                }
                else
                {
                    entities.OfferLikes.Remove(offerLiked);
                }
                this.AcceptChanges();
                return offer.OfferLikes.Count();
            }
            return -1;
        }

        public bool IsOfferLikedByUser(MembershipUser user, Offer offer)
        {
            if (user != null && offer != null)
            {
                var offerLiked = offer.OfferLikes.FirstOrDefault(e => e.UserId.Equals(user.UserId));
                if (offerLiked != null)
                    return true;
                else
                    return false;

            }
            return false;
           // throw new ArgumentNullException("offer or user in null");
        }

        public AddStatus AddCommentForOffer(Offer offer, MembershipUser user, string comment,out OfferComment reference)
        {
            reference = null;
            if (user != null && offer != null && !string.IsNullOrEmpty(comment))
            {
                OfferComment offerComment=new OfferComment()
                {
                    Datetime = DateTime.Now,
                    MembershipUser = user,
                    CommentText = comment,
                };
                offer.OfferComments.Add(offerComment);
                int effected = this.AcceptChanges();
                if (effected > 0)
                {
                    reference = offerComment;
                    return AddStatus.Added;
                }
                else
                    return AddStatus.RejectedByInternalSystemError;

            }
            return AddStatus.RejectedByInvalidParameter;
        }

        public OfferComment GetOfferCommentById(long id)
        {
            var db = this.GetPersitantStorage();
            return db.OfferComments.FirstOrDefault(e => e.OfferCommentId.Equals(id));

        }
        public IQueryable<OfferOfftickType> GetOfferOfftickTypes()
        {
            var db = this.GetPersitantStorage();
            return db.OfferOfftickTypes.AsQueryable();
        }
        public OfferOfftickType GetOfferOfftickTypeById(short id)
        {
            var db = this.GetPersitantStorage();
            if (id<=0)
                return null;
            return db.OfferOfftickTypes.FirstOrDefault(e => e.OfferOfftickTypeId.Equals(id));

        }
        public OfferOfftickType GetOfferOfftickTypeByName(string offerName)
        {
            var db = this.GetPersitantStorage();
            if (string.IsNullOrEmpty(offerName))
                return null;
            return db.OfferOfftickTypes.FirstOrDefault(e => e.TypeName.ToLower().Equals(offerName.ToLower()));

        }
        public EditStatus UpdateCommentForOffer(long offerCommentId, string comment)
        {
            var offerComment = this.GetOfferCommentById(offerCommentId);
            if (offerComment == null)
                return EditStatus.RejectedByNotExit;
            return UpdateCommentForOffer(offerComment, comment);
        }

        public EditStatus UpdateCommentForOffer(OfferComment offerComment, string comment)
        {
            if (offerComment == null)
                return EditStatus.RejectedByNotExit;

            if (!string.IsNullOrEmpty(comment))
            {
                offerComment.versionNumber++;
                offerComment.Datetime = DateTime.Now;
                offerComment.CommentText = comment;
                int effected = this.AcceptChanges();
                return effected > 0 ? EditStatus.Edited : EditStatus.RejectedByNonEffectiveQuery;

            }
            return EditStatus.RejectedByInternalSystemError;
        }

        public DeleteStatus RemoveCommentForOffer(long offerCommentId, string userName)
        {
            if (offerCommentId != -1 && !string.IsNullOrEmpty(userName))
            {
                var offerCommented = this.GetOfferCommentById(offerCommentId);
                var offer = offerCommented.Offer;
                var currentUser=MembershipManager.Instance.GetUser(userName);
                string roleName = currentUser != null ? currentUser.MembershipUsersInRoles.First().MembershipRole.RoleName : string.Empty;

                if (offer.MembershipUser.UserName.Equals(userName) || offerCommented.MembershipUser.UserName.Equals(userName) || roleName == "Administrator" || roleName == "Tester" || roleName == "Developer")
                {
                    var db = this.GetPersitantStorage();
                    db.OfferComments.Remove(offerCommented);
                    int effected = this.AcceptChanges();
                    return effected > 0 ? DeleteStatus.Deleted : DeleteStatus.RejectedByNonEffectiveQuery;
                }
                else
                    return DeleteStatus.RejectedByAccessDenied;

            }
            return DeleteStatus.RejectedByInvalidParameter;
        }


        public EditStatus IncreaseOfferViewCount(Offer offer, MembershipUser user)
        {
            if (offer == null)
                return EditStatus.RejectedByNonEffectiveQuery;
            switch ((EnumOfferType)offer.OfferType)
            {
                case EnumOfferType.Tour:
                    var tourOffer = offer.OfferTours.FirstOrDefault();
                    tourOffer.ViewCount += 1;
                    break;
                case EnumOfferType.Food:
                     var foodOffer = offer.OfferFoods.FirstOrDefault();
                     foodOffer.ViewCount += 1;
                    break;
                case EnumOfferType.Post:
                    offer.CountOfVisited += 1;
                    break;
            }
          
            if (user != null)
            {
                var db = this.GetPersitantStorage();
                var offervisited = db.OfferVisiteds.Where(e => e.OfferId.Equals(offer.OfferId) && e.VisitedUserId.Equals(user.UserId)).FirstOrDefault();
                if (offervisited == null)
                {
                    offervisited = new OfferVisited()
                    {
                        OfferId = offer.OfferId,
                        OfferVisitedId = Guid.NewGuid(),
                        VisitedUserId = user.UserId,
                    };
                    db.OfferVisiteds.Add(offervisited);
                }
                offervisited.DateTime = DateTime.Now;
            }
            int effected = this.AcceptChanges();
            if (effected > 0)
                return EditStatus.Edited;
            return EditStatus.RejectedByInternalSystemError;
        }


        public Offer GetMaingPageBigOffer()
        {
            var db = this.GetPersitantStorage();
            var mainOffer=db.OfferWalls.FirstOrDefault(e => e.IsMain == true);
            return mainOffer!=null ? mainOffer.Offer:null;

        }


        #region FOOD












        public AddStatus AddFoodOffer(Offer offer, 
            System.Web.HttpPostedFileBase[] foodImages
            , System.Web.HttpPostedFileBase[] menuImages, long foodOriginalPrice
            , string foodOriginalTitle
            , string url
                                                                                                                        ,string foodUnit,
                                                                                                                        long foodDiscountPrice,
                                                                                                                        string foodDiscountTitle,
                                                                                                                        short foodDiscountPercent,
                                                                                                                        int foodExpireDate,
                                                                                                                        short foodQuantity,
                                                                                                                        string foodAudeiences,
                                                                                                                        string foodSpecification,
                                                                                                                        string foodConditions,
                                                                                                                        string foodDescriptions,
                                                                                                                        Guid? foodLocationCityId,
                                                                                                                        string foodLocationAddress,
                                                                                                                        short offtickOfferTypeId,
                                                                                                                        bool showIncomeInPercentage,
                                                                                                                         short? addedValueTax,
                                                                                                                        ConfirmStatus confirmStatus


                
            )
        {
            string userId = offer.MembershipUser.UserId.ToString();
            AddStatus result = AddStatus.Added;
            if (offer != null)
            {
               var foodOffer= new OfferFood()
                {
                    Conditions = foodConditions,
                    Descriptions = foodDescriptions,
                    DiscountPercent = foodDiscountPercent,
                    DiscountPrice = foodDiscountPrice,
                    DiscountPriceTitle = foodDiscountTitle,
                    DiscountPriceUnit = foodUnit,
                    OfferStatusId = (short)OfferExecutionStatus.Running,
                    OriginalPrice = foodOriginalPrice,
                    OriginalPriceTitle = foodOriginalTitle,
                    Quantity = foodQuantity,
                    Specifications = foodSpecification,
                    TimeToExpire = foodExpireDate,
                    OfftickOfferTypeId=offtickOfferTypeId,
                    ShowIncomeInPercentage=showIncomeInPercentage,
                    AddedValueTax=addedValueTax,
                };
                offer.Url = url;
               offer.ConfirmStatusId = (int)confirmStatus;
               offer.ConfirmDateTime = confirmStatus == ConfirmStatus.Confirmed ? (DateTime?)DateTime.Now : null;
                offer.OfferFoods.Add(foodOffer);
                offer.LocationCityId=foodLocationCityId;
                offer.LocationAddress = foodLocationAddress;
                DateTime now=DateTime.Now;
                int effected = this.AcceptChanges();
                if (effected > 0)
                {
                    SecurityManager.Instance.UpdateGeneralSecurityForOffer(offer, true, true,false);
                    ImageFileManager imageManager = new ImageFileManager(userId);
                    if (foodImages != null && foodImages.Length > 0)
                    {
                        for (int k = 0; k < foodImages.Length; k++)
                        {
                            var foodImage = foodImages[k];
                            if (foodImage != null)
                            {
                                string fileName = foodImage.FileName;
                                string newFileName = imageManager.Save(foodImage, fileName);
                                foodOffer.OfferFoodImages.Add(new OfferFoodImage()
                                {
                                    DateTime = now,
                                    ImageType = (short)FoodOfferImageType.FoodImage,
                                    Name = newFileName,
                                });
                            }
                        }
                    }

                    if (menuImages != null && menuImages.Length > 0)
                    {
                        for (int k = 0; k < menuImages.Length; k++)
                        {
                            var menuImage=menuImages[k];
                            if (menuImage != null)
                            {
                                string fileName = menuImage.FileName;
                                string newFileName = imageManager.Save(menuImage, fileName);
                                foodOffer.OfferFoodImages.Add(new OfferFoodImage()
                                {
                                    DateTime = now,
                                    ImageType = (short)FoodOfferImageType.FoodMenu,
                                    Name = newFileName,
                                });
                            }
                        }
                    }
                    effected=this.AcceptChanges();
                }
                result= effected>0?  AddStatus.Added: AddStatus.RejectedByInternalSystemError;

            }
            return result;
        }

        public AddStatus OrderFoodOffer(Offer offer, MembershipUser user, short count,long saleOrderId,string saleReferenceId,bool isBuyingVisible,PdfService pDFService)
        {
            var foodOffer = offer.OfferFoods.FirstOrDefault();
            var countOfOffer = foodOffer.Quantity;
            var countOfSoled = foodOffer.OfferFoodOrders.Sum(e=>e.Quantity);
            var remain = countOfOffer - countOfSoled;
            
            if (count > remain)
                return AddStatus.RejectedByAccessDenied;
            var offerOrdered = new OfferFoodOrder()
            {
                DateTime = DateTime.Now,
                UserId = user.UserId,
                Quantity = count,
                PaymentAmount = count * foodOffer.DiscountPrice,
                SaleOrderId = saleOrderId,
                SaleRefrenceId=saleReferenceId,
            };
           Offtick.Business.Services.Domain.PdfGenerator.PdfService pdfService = pDFService;
            string userName=user.UserName;
            string offerTitle=offer.Body;
            string recieverName=string.Format("{0} {1}",user.FirstName,user.LastName);
            string validFrom = Offtick.Core.Utility.PersianTools.PersianDateConvertor.ToKhorshidiDate(offer.DateTime);
            string validTo =!foodOffer.TimeToExpire.HasValue? string.Empty: Offtick.Core.Utility.PersianTools.PersianDateConvertor.ToKhorshidiDate(offer.DateTime.AddHours(foodOffer.TimeToExpire.Value));
            
            var fileManager = new Offtick.Business.Services.Storage.File.ImageFileManager(offer.MembershipUser.UserId.ToString());
            string fileName = fileManager.GetFullFileName(  offer.FileName,true);
            for (int i = 0; i < count; i++)
            {
                string offtickCode=Guid.NewGuid().ToString();
                var introduceInfo = offer.MembershipUser.MembershipIntroduceInfo;
                string responsiblePhone=introduceInfo!=null?introduceInfo.ResponsiblePhone:string.Empty;

                string pdfFileName = pdfService.CreatePdf(user.UserId.ToString(), offerTitle, offtickCode, recieverName, foodOffer.OriginalPrice.ToString(), foodOffer.DiscountPrice.ToString(), validFrom, validTo, offer.MembershipUser.Address, responsiblePhone, fileName, foodOffer.Conditions, foodOffer.Specifications, saleOrderId.ToString(), saleReferenceId);

                offerOrdered.OfferFoodOrderDetails.Add(new OfferFoodOrderDetail()
                {
                     OrderCode=offtickCode,
                     Status = (short)FoodOfferOrderStatus.Valid,
                     FileName=pdfFileName,

                });
                
            }
            foodOffer.OfferFoodOrders.Add(offerOrdered);
            int effected = this.AcceptChanges();
            if (effected > 0)
            {
                SMTPService mailService = new SMTPService();
                PdfFileManager pdfFileManager = new PdfFileManager(offer.MembershipUser.UserId.ToString());
                string filePath = pdfFileManager.GetFullFileName(fileName);
                if (pdfFileManager.IsExit(filePath))
                {

                    var stream = new System.IO.FileStream(filePath,  System.IO.FileMode.Open);
                    mailService.SendMail("info@offTick.com", user.Email, "", "", stream, fileName);

                    
                }
               
            }
            SecurityManager.Instance.UpdateOfferSecurityForUser(offer, user, isBuyingVisible, isBuyingVisible);
         var configManager = ConfigFactory.GetConfigManager(ConfigManagerType.WebConfig);

            var voucherDescription=string.Format(configManager.ReadSetting("voucherDescription"),count,offer.Title,offer.MembershipUser.FirstName);
            VoucherManager.Instance.AddVoucher(offerOrdered.OfferFoodOrderId, voucherDescription, offerOrdered.PaymentAmount.HasValue?offerOrdered.PaymentAmount.Value:offerOrdered.Quantity*offerOrdered.OfferFood.DiscountPrice, DateTime.Now);
            return effected>0?   AddStatus.Added:  AddStatus.RejectedByNonEffectiveQuery;
        }


        public bool isExpiredFoodOffer(Offer offer)
        {
            bool result = false;
            if(offer!=null && offer.OfferFoods.FirstOrDefault()!=null){
                var foodOffer=offer.OfferFoods.FirstOrDefault();
                if(foodOffer.TimeToExpire.HasValue &&  offer.DateTime.AddHours( foodOffer.TimeToExpire.Value)<DateTime.Now)
                    result=true;

            }
            return result;
        }


        public IList<OfferFoodOrder> getAllOfSaledOfferBySaleOrderId(long saleOrderId)
        {
            var entities = this.GetPersitantStorage();
            return entities.OfferFoodOrders.Where(e => e.SaleOrderId.Equals(saleOrderId)).ToList();
        }

        public IQueryable<OfferFoodOrder> getAllOfOrderedOfferForUserName(Guid userId, int take, int skip)
        {
            int count = 0;
            return getAllOfOrderedOfferForUserName(userId, take, skip,out count);
         }
        public IQueryable<OfferFoodOrder> getAllOfOrderedOfferForUserName(Guid userId, int take, int skip, out int count)
        {
            var entities = this.GetPersitantStorage();
            var query = entities.OfferFoodOrders.Where(e => e.UserId.Equals(userId));
            count = query.Count();
            return query.OrderByDescending(e => e.DateTime).Skip(skip).Take(take);
        }

        public IQueryable<OfferFoodOrder> getAllOfSoldOffersForUserName(Guid userId, int take, int skip)
        {
            int count = 0;
            return getAllOfSoldOffersForUserName(userId, take, skip, out count);
        }

        public IQueryable<OfferFoodOrder> getAllOfSoldOffersForUserName(Guid userId, int take, int skip,out int count)
        {
            var entities = this.GetPersitantStorage();
            var query = entities.OfferFoodOrders.Where(e => e.OfferFood.Offer.MembershipUser.UserId.Equals(userId));
            count = query.Count();
            return query.OrderByDescending(e => e.DateTime).Skip(skip).Take(take);
        }
        public IQueryable<OfferFoodOrder> getAllOfSoldOffersForUserName(Guid userId,string offerName)
        {
            var entities = this.GetPersitantStorage();
            return entities.OfferFoodOrders.Where(e => e.OfferFood.Offer.MembershipUser.UserId.Equals(userId) && (e.OfferFood.Offer.Title.Contains(offerName)||e.OfferFood.Offer.Body.Contains(offerName)));
        }
        public IQueryable<OfferFoodOrder> getAllOfSoldOffersForUserName(Guid userId, Guid offerId)
        {
            var entities = this.GetPersitantStorage();
            return entities.OfferFoodOrders.Where(e => e.OfferFood.Offer.MembershipUser.UserId.Equals(userId) && e.OfferFood.Offer.OfferId==offerId );
        }
        public IQueryable<OfferFoodOrder> getAllOfSoldOffersForUserName(Guid userId)
        {
            var entities = this.GetPersitantStorage();
            return entities.OfferFoodOrders.Where(e => e.OfferFood.Offer.MembershipUser.UserId.Equals(userId));
        }
        public IQueryable<OfferFoodOrder> getAllOfBuyedOffersForUserName(Guid userId)
        {
            var entities = this.GetPersitantStorage();
            return entities.OfferFoodOrders.Where(e => e.MembershipUser.UserId.Equals(userId));
        }

        public IQueryable<OfferFoodOrder> getAllOfOrderedOffers()
        {
            var entities = this.GetPersitantStorage();
            return entities.OfferFoodOrders.AsQueryable();
        }
        public IQueryable<Offer> getAllOfOffers()
        {
            var entities = this.GetPersitantStorage();
            return entities.Offers.AsQueryable();
        }

        public EditStatus SetOfferWall(Offer offer)
        {
            try
            {
                if (offer == null)
                    return EditStatus.RejectedByNotExit;

                var entities = this.GetPersitantStorage();
                var offerWall = entities.OfferWalls.FirstOrDefault(e => e.IsMain == true);
                if (offerWall == null)
                {
                    offerWall = new OfferWall();
                    entities.OfferWalls.Add(offerWall);
                }
                offerWall.IsMain = true;
                offerWall.Offer = offer;
                int effected = entities.SaveChanges();
                if (effected > 0)
                    return EditStatus.Edited;
                else
                    return EditStatus.RejectedByNonEffectiveQuery;
            }
            catch
            {
                return EditStatus.RejectedByInternalSystemError;
            }

        }
        #endregion 
        

        #region Tour
        public AddStatus AddtourOffer(Offer offer,
           System.Web.HttpPostedFileBase[] tourImages
           , System.Web.HttpPostedFileBase[] menuImages, long tourOriginalPrice
           , string tourOriginalTitle
           , string url,
                                                                                                           string tourUnit,
                                                                                                                     long tourDiscountPrice,
                                                                                                                       string tourDiscountTitle,
                                                                                                                       short tourDiscountPercent,
                                                                                                                       int tourExpireDate,
                                                                                                                       short tourQuantity,
                                                                                                                       string tourSpecification,
                                                                                                                       string tourConditions,
                                                                                                                       string tourDescriptions,
                                                                                                                       string tourLocationAddress,
                                                                                                                       bool showIncomeInPercentage,
                                                                                                                        short? addedValueTax,
                                                                                                                        bool  tourShowBuyButton        ,
                                                                                                                        bool  tourShowOffButton        ,
                                                                                                                        bool   tourShowInstantButton   ,
                                                                                                                        Guid? tourSourceCityId,
                                                                                                                        Guid? tourDestinationCityId,
                                                                                                                        DateTime? tourWentDate            ,
                                                                                                                        DateTime? tourReturnDate,
                                                                                                                        string tourAgencyName          ,
                                                                                                                        TravelTypeEnum tourTravelType  ,                                                                                                        
                                                                                                                         ConfirmStatus confirmStatus  
             



           )
        {
            string userId = offer.MembershipUser.UserId.ToString();
            AddStatus result = AddStatus.Added;
            if (offer != null)
            {
                var tourOffer = new OfferTour()
                {
                    Conditions = tourConditions,
                    Descriptions = tourDescriptions,
                    DiscountPercent = tourDiscountPercent,
                    DiscountPrice = tourDiscountPrice,
                    DiscountPriceTitle = tourDiscountTitle,
                    DiscountPriceUnit = tourUnit,
                    OfferStatusId = (short)OfferExecutionStatus.Running,
                    OriginalPrice = tourOriginalPrice,
                    OriginalPriceTitle = tourOriginalTitle,
                    Quantity = tourQuantity,
                    Specifications = tourSpecification,
                    TimeToExpire = tourExpireDate,
                    ShowIncomeInPercentage = showIncomeInPercentage,
                    AddedValueTax = addedValueTax,
                    ShowBuyButton = tourShowBuyButton,
                    ShowOffButton=tourShowOffButton,
                    ShowInstantButton=tourShowInstantButton,
                    SourceCityId=tourSourceCityId,
                    DestinationCityId=tourDestinationCityId,
                    WentDateTime=tourWentDate,
                    ReturnDateTime=tourReturnDate,
                    TravelTypeId=(int)tourTravelType,
                    AgancyName=tourAgencyName,
                };
                offer.Url = url;
                offer.ConfirmStatusId = (int)confirmStatus;
                offer.ConfirmDateTime = confirmStatus == ConfirmStatus.Confirmed ? (DateTime?)DateTime.Now : null;
                offer.OfferTours.Add(tourOffer);
                offer.LocationCityId = null;
                offer.LocationAddress = tourLocationAddress;
                DateTime now = DateTime.Now;
                int effected = this.AcceptChanges();
                if (effected > 0)
                {
                    SecurityManager.Instance.UpdateGeneralSecurityForOffer(offer, true, true, false);
                    ImageFileManager imageManager = new ImageFileManager(userId);
                    if (tourImages != null && tourImages.Length > 0)
                    {
                        for (int k = 0; k < tourImages.Length; k++)
                        {
                            var tourImage = tourImages[k];
                            if (tourImage != null)
                            {
                                string fileName = tourImage.FileName;
                                string newFileName = imageManager.Save(tourImage, fileName);
                                tourOffer.OfferTourImages.Add(new OfferTourImage()
                                {
                                    DateTime = now,
                                    ImageType = (short)OfferImageType.Image,
                                    Name = newFileName,
                                });
                            }
                        }
                    }

                    if (menuImages != null && menuImages.Length > 0)
                    {
                        for (int k = 0; k < menuImages.Length; k++)
                        {
                            var menuImage = menuImages[k];
                            if (menuImage != null)
                            {
                                string fileName = menuImage.FileName;
                                string newFileName = imageManager.Save(menuImage, fileName);
                                tourOffer.OfferTourImages.Add(new OfferTourImage()
                                {
                                    DateTime = now,
                                    ImageType = (short)OfferImageType.Menu,
                                    Name = newFileName,
                                });
                            }
                        }
                    }
                    effected = this.AcceptChanges();
                }
                result = effected > 0 ? AddStatus.Added : AddStatus.RejectedByInternalSystemError;

            }
            return result;
        }

        
        #endregion

        #region Admin Actions
        public EditStatus ChangeStatusForOffer(OfferFood offer, OfferExecutionStatus executionStatus)
        {
            try
            {
                if (offer == null)
                    return EditStatus.RejectedByNotExit;
                var entities = this.GetPersitantStorage();
                offer.OfferStatusId = (short)executionStatus;

                int effected = entities.SaveChanges();
                if (effected > 0)
                    return EditStatus.Edited;
                else
                    return EditStatus.RejectedByNonEffectiveQuery;
            }
            catch
            {
                return EditStatus.RejectedByInternalSystemError;
            }
        }
        public EditStatus ConfirmOffer(Offer offer)
        {
            try
            {
                if (offer == null)
                    return EditStatus.RejectedByNotExit;
                var entities = this.GetPersitantStorage();
                offer.ConfirmStatusId = (short)ConfirmStatus.Confirmed;
                offer.ConfirmDateTime = DateTime.Now;

                int effected = entities.SaveChanges();
                if (effected > 0)
                    return EditStatus.Edited;
                else
                    return EditStatus.RejectedByNonEffectiveQuery;
            }
            catch(Exception ex)
            {
                return EditStatus.RejectedByInternalSystemError;
            }
        }
        public EditStatus NotConfirmOffer(Offer offer)
        {
            try
            {
                if (offer == null)
                    return EditStatus.RejectedByNotExit;
                var entities = this.GetPersitantStorage();
                offer.ConfirmStatusId = (short)ConfirmStatus.NotConfirmed;

                int effected = entities.SaveChanges();
                if (effected > 0)
                    return EditStatus.Edited;
                else
                    return EditStatus.RejectedByNonEffectiveQuery;
            }
            catch
            {
                return EditStatus.RejectedByInternalSystemError;
            }
        }
        #endregion

        #region Accounting Actions
        public EditStatus UpdateDetailCode(Offer offer, int detailedCode)
        {
            try
            {
                if (offer == null)
                    return EditStatus.RejectedByNotExit;
                var entities = this.GetPersitantStorage();
                offer.DetailedCode = detailedCode;

                int effected = entities.SaveChanges();
                if (effected > 0)
                    return EditStatus.Edited;
                else
                    return EditStatus.RejectedByNonEffectiveQuery;
            }
            catch
            {
                return EditStatus.RejectedByInternalSystemError;
            }
        }
      
        #endregion

    }

    public enum OfferExecutionStatus
    {
        PreStarted=1,
        RequestForRunning=2,
        Running=3,
        Suspended=4,
        Stoped=5,
        Finished=6,
    }      
     public enum FoodOfferImageType
    {
         FoodImage=1,
         FoodMenu=2,
    }
     public enum  FoodOfferOrderStatus
     {
         Valid=0,
         Used=1,
         Expired=2,
         Abused=3,
         Canceled=4,
         Invalid=10
     }
}                              
                               
                               