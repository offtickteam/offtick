﻿using Offtick.Business.Services.Storage;
using Offtick.Business.Services.Storage.File;
using Offtick.Core.EnumTypes;
using Offtick.Data.Entities.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.Services.Managers
{
    public class ContentManager
    {
          private static ContentManager instance;
        public static ContentManager Instance
        {
            get
            {
                if (instance != null)
                    return instance;
                lock (typeof(GalleryManager))
                {
                    if (instance == null)
                        instance = new ContentManager();
                    return instance;
                }
            }
        }

        private ContentManager()
        {
            
        }

        public Offtick.Data.Context.ExpertOnlinerContexts.Menu GetMenu(Guid menuId)
        {
            var entities = DataContextManager.GetPersitantStorage();
            return entities.Menus.FirstOrDefault(e => e.MenuId.Equals(menuId));
        }

        public AddStatus AddContactMessage(Offtick.Data.Context.ExpertOnlinerContexts.ContactMessage message)
        {
            if (message == null || message.RecieverUserId==default(Guid))
                return AddStatus.RejectedByInternalSystemError;

            try
            {
                var entities = DataContextManager.GetPersitantStorage();
                message.DateTime = DateTime.Now;
                message.MessageId = Guid.NewGuid();
                
                entities.ContactMessages.Add(message);
                int effected = entities.SaveChanges();
                if (effected > 0)
                    return AddStatus.Added;
                else
                    return AddStatus.RejectedByNonEffectiveQuery;
            }catch{
                return AddStatus.RejectedByInternalSystemError;
            }
        }


        public AddStatus AddFile(short languageId, string title, string description, string userId, System.Web.HttpFileCollectionBase fileCollection)
        {
            try
            {

                


                for (int i = 0; i < fileCollection.Count; i++)
                {
                    var entities = DataContextManager.GetPersitantStorage();
                    var fileBase = fileCollection[i];
                    string fileName = fileBase.FileName;
                    string fileExtension=System.IO.Path.GetExtension(fileName);
                    fileExtension=!string.IsNullOrEmpty(fileExtension)?fileExtension.Substring(1).ToUpper():string.Empty;
                    FileManager imageManager = ContentFileFactory.GetFileManager(fileExtension, userId);
                    string newFileName = imageManager.Save(fileBase, fileName);
                    
                    var fileTypeObj = entities.FileTypes.FirstOrDefault(e => e.Mime.Equals(fileExtension));
                    short fileTypeId = fileTypeObj != null ? fileTypeObj.TypeId : (short)1000;


                    Offtick.Data.Context.ExpertOnlinerContexts.File entity = new Offtick.Data.Context.ExpertOnlinerContexts.File()
                    {
                        FileId = Guid.NewGuid(),
                        UserId =Guid.Parse( userId),
                        DateTime = DateTime.Now,
                        Url = newFileName,
                        TypeId = fileTypeId,
                        Description = description,
                        Title = title,
                    };

                 

                    
                    entities.Files.Add(entity);

                    int effected=entities.SaveChanges();
                    if (effected > 0)
                        return AddStatus.Added;
                    else
                        return AddStatus.RejectedByNonEffectiveQuery;
                }
                return AddStatus.RejectedByNonEffectiveQuery;
            }
            catch(Exception ex)
            {
                  string serverPath = System.Web.HttpRuntime.AppDomainAppPath;
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(serverPath+"/Files/Logs/errorLogger.txt",true))
                {
                    file.WriteLine(ex.Message+ ex.InnerException!=null? ex.InnerException.Message:string.Empty);
                }
                return AddStatus.RejectedByInternalSystemError;
            }
        }

        public FileSearchResultItem GetVirtualPathOfFile(string fileName, string mime, string userId)
        {
            FileManager fileManager = ContentFileFactory.GetFileManager(mime, userId);
            FileSearchResultItem result=null;
            if (fileManager is ImageContentFileManager)
            {
                ImageContentFileManager icfm=(ImageContentFileManager)fileManager;
                result=new FileSearchResultItem(mime,true,true,icfm.GetAbsoluteThumnailFileAddress(fileName) ,icfm.GetAbsoluteFileAddress(fileName) );
            }
            if (fileManager is ContentFileManager)
            {
                ContentFileManager cfm = (ContentFileManager)fileManager;
                result = new FileSearchResultItem(mime, false, true, @"\Content\Images\Document-icon96.png", cfm.GetAbsoluteFileName(fileName));
            }
            return result;
        }


        public Offtick.Data.Context.ExpertOnlinerContexts.File GetFile(Guid fileId)
        {
            var entities = DataContextManager.GetPersitantStorage();
            return entities.Files.FirstOrDefault(e => e.FileId.Equals(fileId));
        }

        public DeleteStatus DeleteFile(Guid fileId)
        {
            var entities = DataContextManager.GetPersitantStorage();
            
            var file = this.GetFile(fileId);
            if (file != null)
            {
                var fileM=ContentFileFactory.GetFileManager(file.FileType.Mime, file.MembershipUser.UserId.ToString());
                fileM.Delete(file.Url);

                //file.FileCultures.Clear();
                entities.Files.Remove(file);
                int effected = entities.SaveChanges();
                if (effected > 0)
                    return DeleteStatus.Deleted;
                else
                    return DeleteStatus.RejectedByNonEffectiveQuery;
            }
            return DeleteStatus.RejectedByNotExit;
        }

        

    }
}
