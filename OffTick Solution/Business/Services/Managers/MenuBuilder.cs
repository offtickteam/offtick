﻿using Offtick.Business.Services.Storage;
using Offtick.Core.EnumTypes;
using Offtick.Data.Context.ExpertOnlinerContexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.Services.Managers
{
    public class MenuBuilder
    {
        private static MenuBuilder instance;
        public static MenuBuilder Instance
        {
            get
            {
                if (instance != null)
                    return instance;
                lock (typeof(MenuBuilder))
                {
                    if (instance == null)
                        instance = new MenuBuilder();
                    return instance;
                }
            }
            
        }
        private MenuBuilder()
        {
        }

        public Offtick.Data.Context.ExpertOnlinerContexts.Menu GetMenu(Guid menuId)
        {
            var entities = DataContextManager.GetPersitantStorage();
            return GetMenu(entities, menuId);
        }
        public Offtick.Data.Context.ExpertOnlinerContexts.Menu GetMenu(ExpertOnlinerFoundationEntities entities, Guid menuId)
        {
            return entities.Menus.FirstOrDefault(e => e.MenuId.Equals(menuId));
        }


        public AddStatus AddMenu(Nullable<Guid> parentId, short langaugeId, string title, MenuType menuType, string url, string iconUrl, bool isSystemMenu)
        {
            return AddMenu(parentId, langaugeId,title, menuType, url, iconUrl,isSystemMenu, string.Empty, Membership.MembershipManager.Instance.GetCurrentUser().UserId);

        }

        public AddStatus AddMenu(Nullable<Guid> parentId,short langaugeId, string title, MenuType menuType, string url, string iconUrl, bool isSystemMenu, string systemicSectionName, Guid userId)
        {
            var entities = DataContextManager.GetPersitantStorage();
            var menu = new Menu();
            
            var langauge= LangaugeManager.Instance.GetLangaugeById(langaugeId);
                if(langauge==null )
                {
                    return AddStatus.RejectedByInternalSystemError;
                }

            if (!string.IsNullOrEmpty(systemicSectionName))
            {
                var systemic=entities.MenuSystemics.FirstOrDefault(e=>e.SectionName.Equals(systemicSectionName));
                if (systemic != null)
                    menu.MenuSystemic = systemic;
            }

            try
            {
                menu.MenuId = Guid.NewGuid();
                
                menu.MenuType = (short)menuType;
                menu.ParentId = parentId != default(Guid) ? parentId : null;
                menu.Url = url;
                menu.IconUrl = iconUrl;
                menu.IsSystemMenu = isSystemMenu;
                menu.LastDateTime = DateTime.Now;
                menu.UserId = userId;



                menu.Title = title;
                menu.LastDateTime = DateTime.Now;



                entities.Menus.Add(menu);
                int effected = entities.SaveChanges();
                if (effected > 0)
                    return AddStatus.Added;
                else
                    return AddStatus.RejectedByNonEffectiveQuery;
            }
            catch
            {
                return AddStatus.RejectedByInternalSystemError;
            }

        }

        public EditStatus EditMenuTitle(Guid menuCultureId, string title)
        {
            var entities = DataContextManager.GetPersitantStorage();
            var menu = entities.Menus.FirstOrDefault(e => e.MenuId.Equals(menuCultureId));
            if (menu != null)
            {
                try
                {
                    menu.Title = title;
                    int effected = entities.SaveChanges();
                    if (effected > 0)
                        return EditStatus.Edited;
                    else
                        return EditStatus.RejectedByNonEffectiveQuery;
                }
                catch
                {
                    return EditStatus.RejectedByInternalSystemError;
                }
            }
            return EditStatus.RejectedByNotExit;
        }



        public DeleteStatus DeleteMenu(Guid menuId)
        {
            var entities = DataContextManager.GetPersitantStorage();
            var menu = this.GetMenu(entities, menuId);
            if (menu != null)
            {
                if (menu.MenuType==0 && menu.Menu1.Count()>0)
                {
                    return DeleteStatus.RejectedByExistSubItems;
                }
                else
                {
                    entities.Menus.Remove(menu);
                    int effected= entities.SaveChanges();
                    if (effected > 0)
                        return DeleteStatus.Deleted;
                    else
                        return DeleteStatus.RejectedByNonEffectiveQuery;
                }
            }
            return DeleteStatus.RejectedByNotExit;
        }

        public MoveStatus MoveMenu(Guid sourceMenuId, Guid destinationMenuId)
        {
            var entities = DataContextManager.GetPersitantStorage();
            var sourceMenu = this.GetMenu(entities, sourceMenuId);
            if (sourceMenu == null)
                return MoveStatus.RejectedByNotExitSource;
            else
            {

                var destinationMenu = this.GetMenu(entities, destinationMenuId);
                if (destinationMenu == null)
                    return MoveStatus.RejectedByNotExitDestination;
                else
                {
                    if (destinationMenu.MenuType != (int)MenuType.ParentMenu)
                        return MoveStatus.RejectedByIsNotParentDestination;
                    else
                    {
                        try
                        {
                            sourceMenu.Menu2 = destinationMenu;
                            destinationMenu.Menu1.Add(sourceMenu);
                            int effected = entities.SaveChanges();
                            if (effected > 0)
                                return MoveStatus.Moved;
                            else
                                return MoveStatus.RejectedByNonEffectiveQuery;
                        }
                        catch
                        {
                            return MoveStatus.RejectedByInternalSystemError;
                        }
                    }
                }
            }
            
        }



        public AddStatus AddMenuContent(Guid menuId, short langaugeId,string content)
        {
            var entities = DataContextManager.GetPersitantStorage();
           /* var langauge = LangaugeManager.Instance.GetLangaugeById(langaugeId);
            if (langauge == null)
            {
                return AddStatus.RejectedByInternalSystemError;
            }*/

            var menuContent = new MenuContent();
            
            DateTime now = DateTime.Now;

            try
            {

                menuContent.Body = content;
                menuContent.LastDateTime = now;

                
                menuContent.LastDateTime = now;
                menuContent.ContentId = Guid.NewGuid();
                menuContent.MenuId = menuId;

                if (GetMenu(menuId) == null)
                    return AddStatus.RejectedByParentNotExit;


                entities.MenuContents.Add(menuContent);
                int effected = entities.SaveChanges();
                if (effected > 0)
                    return AddStatus.Added;
                else
                    return AddStatus.RejectedByNonEffectiveQuery;
            }
            catch
            {
                return AddStatus.RejectedByInternalSystemError;
            }

        }

        public EditStatus EditMenuContentByMenuId(Guid menuContentCultureId, string content)
        {
            var entities = DataContextManager.GetPersitantStorage();
            var menu = entities.MenuContents.FirstOrDefault(e=>e.ContentId.Equals(menuContentCultureId));
            if (menu == null)
                return EditStatus.RejectedByNotExit;


            try
            {
                menu.Body = content;
                menu.LastDateTime = DateTime.Now;
                int effected = entities.SaveChanges();
                if (effected > 0)
                    return EditStatus.Edited;
                else
                    return EditStatus.RejectedByNonEffectiveQuery;
            }
            catch
            {
                return EditStatus.RejectedByInternalSystemError;
            }

        }
        public EditStatus EditMenuContentByMenuId(Guid menuId,short languageId, string content)
        {
            var entities = DataContextManager.GetPersitantStorage();
            var menu = entities.MenuContents.FirstOrDefault(e => e.MenuId.Equals(menuId) );
            if (menu == null)
                return EditStatus.RejectedByNotExit;
            


            try
            {
                menu.Body = content;
                menu.LastDateTime = DateTime.Now;
                int effected = entities.SaveChanges();
                if (effected > 0)
                    return EditStatus.Edited;
                else
                    return EditStatus.RejectedByNonEffectiveQuery;
            }
            catch
            {
                return EditStatus.RejectedByInternalSystemError;
            }

        }
    }
}
