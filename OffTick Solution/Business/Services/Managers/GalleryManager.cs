﻿using Offtick.Business.Services.Storage;
using Offtick.Business.Services.Storage.File;
using Offtick.Core.EnumTypes;
using Offtick.Data.Context.ExpertOnlinerContexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.Services.Managers
{
    public class GalleryManager : BaseManager
    {
        private static GalleryManager instance;
        public static GalleryManager Instance
        {
            get
            {
                if (instance != null)
                    return instance;
                lock (typeof(GalleryManager))
                {
                    if (instance == null)
                        instance = new GalleryManager();
                    return instance;
                }
            }
        }

        private GalleryManager()
        {
            
        }


        public AddStatus AddGallery(string title,string desciption, Nullable<Guid> parentGaleryId,bool isDefault)
        {
            var LanguageId = Membership.MembershipManager.Instance.GetCurrentUser().MembershipLangauges.First(e => e.IsDefault).LanguageId;
            

            try
            {
                var entities = DataContextManager.GetPersitantStorage();
                if (isDefault)
                {
                    Guid userId = Membership.MembershipManager.Instance.GetCurrentUser().UserId;
                    var defaultGallery = entities.Galleries.FirstOrDefault(e => e.UserId == userId && e.IsDefault.HasValue && e.IsDefault.Value);
                    if (defaultGallery != null)
                    {
                        return AddStatus.RejectedByInternalSystemError;
                    }
                }
                Offtick.Data.Context.ExpertOnlinerContexts.Gallery gallery = new Gallery()
                {
                    GalleryId = Guid.NewGuid(),
                    //Title = title,
                    //Description = desciption,
                    MembershipUser = Membership.MembershipManager.Instance.GetCurrentUser(),
                    CreationDateTime = DateTime.Now,
                    ParentGaleryId = parentGaleryId,
                    UpdateDateTime = DateTime.Now,
                    IsDefault = isDefault,
                      Description = desciption,
                    Title = title,
                };

              

                



                entities.Galleries.Add(gallery);
                int effected = entities.SaveChanges();
                if (effected > 0)
                    return AddStatus.Added;
                else
                    return AddStatus.RejectedByNonEffectiveQuery;
            }
            catch
            {
                return AddStatus.RejectedByInternalSystemError;
            }
        }

        public EditStatus EditGallery(string title, string desciption, Guid galleryId, bool isDefault)
        {
            try
            {

                var entities = DataContextManager.GetPersitantStorage();
                if (isDefault)
                {
                    var userId=Membership.MembershipManager.Instance.GetCurrentUser().UserId;
                    var defaultGallery = entities.Galleries.FirstOrDefault(e => e.UserId == userId && e.IsDefault.HasValue && e.IsDefault.Value);
                    if (defaultGallery != null)
                    {
                        if (defaultGallery.GalleryId != galleryId)
                        {
                            return EditStatus.RejectedByInternalSystemError;
                        }
                    }
                }

                Offtick.Data.Context.ExpertOnlinerContexts.Gallery gallery = GetGallery(galleryId);
               // var LanguageId = Membership.MembershipManager.Instance.GetCurrentUser().MembershipLangauges.First(e => e.IsDefault).LanguageId;
               // var galleryCalture = gallery.GalleryCaltures.FirstOrDefault(e => e.LanguageId.Equals(LanguageId));

                if (gallery != null)
                {
                    gallery.Title = title;
                    gallery.Description = desciption;
                    gallery.UpdateDateTime = DateTime.Now;
                    gallery.IsDefault = isDefault;
                };

                int effected = entities.SaveChanges();
                if (effected > 0)
                    return EditStatus.Edited;
                else
                    return EditStatus.RejectedByNonEffectiveQuery;
            }
            catch
            {
                return EditStatus.RejectedByInternalSystemError;
            }
        }



        public void AddImgageGallery(string title,string description,Guid galleryId,Guid userId,System.Web.HttpFileCollectionBase fileCollection)
        {
            ImageFileManager imageManager = new ImageFileManager(userId.ToString());
            for (int i = 0; i < fileCollection.Count; i++)
            {
                var fileBase = fileCollection[i];
                string fileName = fileBase.FileName;
                string newFileName = imageManager.Save(fileBase, fileName);


                Offtick.Data.Context.ExpertOnlinerContexts.GalleryImage galleryImage = new GalleryImage()
                {
                    // Title = title,
                    //Description = description,
                    GalleryId = galleryId,
                    DateTime = DateTime.Now,
                    ImageId = Guid.NewGuid(),
                    ImageUrl = newFileName,
                    Description = description,
                    Title = title,
                };

              

                var entities = DataContextManager.GetPersitantStorage();
                entities.GalleryImages.Add(galleryImage);
                entities.SaveChanges();
            }
        }


        public Offtick.Data.Context.ExpertOnlinerContexts.Gallery GetDefaultGallery(string userName)
        {
            
            var user = Membership.MembershipManager.Instance.GetUser(userName);
            return GetDefaultGallery(user, false);
        }

        public Offtick.Data.Context.ExpertOnlinerContexts.Gallery GetDefaultGallery(MembershipUser user,bool createIfNotExist)
        {
            
            if (user != null)
            {
                var defaultGallery= user.Galleries.FirstOrDefault(e => e.IsDefault.HasValue && e.IsDefault.Value);
                if (defaultGallery == null && createIfNotExist)
                {
                    var entities = DataContextManager.GetPersitantStorage();
                    defaultGallery = new Gallery()
                    {
                       GalleryId=Guid.NewGuid(),
                        Description = "Default Gallery",
                        Title = "Default Gallery",
                        IsDefault = true,
                        CreationDateTime = DateTime.Now,
                        UpdateDateTime = DateTime.Now,
                    };
                    user.Galleries.Add(defaultGallery);
                    entities.SaveChanges();
                }
                return defaultGallery;
            }
            return null;
        }



        public Offtick.Data.Context.ExpertOnlinerContexts.Gallery GetGallery(Guid galleryId)
        {
            var entities = DataContextManager.GetPersitantStorage();
            return entities.Galleries.FirstOrDefault(e => e.GalleryId.Equals(galleryId));
        }


        public Offtick.Data.Context.ExpertOnlinerContexts.GalleryImage GetGalleryImage(Guid galleryImageId)
        {
            var entities = DataContextManager.GetPersitantStorage();
            return entities.GalleryImages.FirstOrDefault(e => e.ImageId.Equals(galleryImageId));
        }
        public bool DeleteGalleryImage(Guid galleryImageId)
        {
            var galleryImage = GetGalleryImage(galleryImageId);
            if (galleryImage != null)
            {
                var entities = DataContextManager.GetPersitantStorage();
                entities.GalleryImages.Remove(galleryImage);
                return entities.SaveChanges()>0;
            }
            else
                return false;
        }

        public bool EditGalleryImageInfo(Guid galleryImageId,string title,string description)
        {
            var LanguageId = Membership.MembershipManager.Instance.GetCurrentUser().MembershipLangauges.First(e => e.IsDefault).LanguageId;
            var galleryImage = GetGalleryImage(galleryImageId);
            if (galleryImage != null)
            {
                var entities = DataContextManager.GetPersitantStorage();


                galleryImage.Title = title;
                galleryImage.Description = description;
                return entities.SaveChanges() > 0;
            }
            else
                return false;
        }


        public string GetVirtualPathOfGalleryImage(string fileName, Guid userId, bool isThumnail)
        {
            ImageFileManager imageManager = new ImageFileManager(userId.ToString());
            return imageManager.GetRelativeFullFileName(userId.ToString(), fileName, isThumnail);
        }












        public long LikeOrDislike(MembershipUser user, GalleryImage galleryImage)
        {
            if (user != null && galleryImage != null)
            {
                var galleryImageVote = galleryImage.GalleryImageVotes.FirstOrDefault(e => e.VoterUserId.Equals(user.UserId));
                var entities = this.GetPersitantStorage();
                if (galleryImageVote == null)
                {

                    entities.GalleryImageVotes.Add(new GalleryImageVote()
                    {
                         DateTime=DateTime.Now
                         , Value=1
                         , VoteId=Guid.NewGuid()
                         , VoterUserId=user.UserId
                         ,ImageId=galleryImage.ImageId
                         

                    });
                }
                else
                {
                    entities.GalleryImageVotes.Remove(galleryImageVote);
                }
                this.AcceptChanges();
                return galleryImage.GalleryImageVotes.Count();
            }
            return -1;
        }

        public bool IsGalleryImageLikedByUser(MembershipUser user, GalleryImage galleryImage)
        {
            if (user != null && galleryImage != null)
            {
                var offerLiked = galleryImage.GalleryImageVotes.FirstOrDefault(e => e.VoterUserId.Equals(user.UserId));
                if (offerLiked != null)
                    return true;

            }
            throw new ArgumentNullException("offer or user in null");
        }

        public AddStatus AddCommentForGalleryImage(GalleryImage galleryImage, MembershipUser user, string comment)
        {
            if (user != null && galleryImage != null && !string.IsNullOrEmpty(comment))
            {
                galleryImage.GalleryImageComments.Add(new GalleryImageComment()
                {
                    CommentId=Guid.NewGuid()
                    , DateTime=DateTime.Now
                    , SubscriberUserId=user.UserId
                    , Text=comment
                   
                });
                int effected = this.AcceptChanges();
                if (effected > 0)
                    return AddStatus.Added;
                else
                    return AddStatus.RejectedByInternalSystemError;

            }
            return AddStatus.RejectedByInvalidParameter;
        }

        public GalleryImageComment GetGalleryImageCommentById(string commentId)
        {
            var db = this.GetPersitantStorage();
            return db.GalleryImageComments.FirstOrDefault(e => e.CommentId.Equals(commentId));

        }
        public GalleryImageComment UpdateCommentForGalleryImage(string galleryImageCommentId, string comment)
        {
            if (!string.IsNullOrEmpty( galleryImageCommentId) && !string.IsNullOrEmpty(comment))
            {
                var entityCommented = this.GetGalleryImageCommentById(galleryImageCommentId);
                
                entityCommented.DateTime = DateTime.Now;
                entityCommented.Text = comment;
                this.AcceptChanges();
                return entityCommented;
            }
            return null;
        }

        public void RemoveCommentForGalleryImager(string galleryImageCommentId, string comment)
        {
            if (!string.IsNullOrEmpty(galleryImageCommentId) && !string.IsNullOrEmpty(comment))
            {
                var galleryImageCommented = this.GetGalleryImageCommentById(galleryImageCommentId);
                var galleryImage = galleryImageCommented.GalleryImage;
                galleryImage.GalleryImageComments.Remove(galleryImageCommented);
                this.AcceptChanges();

            }
        }



        public IList<GalleryOffer> GetGalleryOffers(Gallery gallery, int take, int skip)
        {
            if (gallery == null)
                return null;
            return gallery.GalleryOffers.Skip(skip).Take(take).ToList();
        }

    }
}
