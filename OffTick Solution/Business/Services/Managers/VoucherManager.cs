﻿using Offtick.Core.EnumTypes;
using Offtick.Core.Utility;
using Offtick.Data.Context.ExpertOnlinerContexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.Services.Managers
{
   public  class VoucherManager: BaseManager
    {
        private static VoucherManager instance;
        public static VoucherManager Instance
        {
            get
            {
                if (instance != null)
                    return instance;
                lock (typeof(VoucherManager))
                {
                    if (instance == null)
                        instance = new VoucherManager();
                    return instance;
                }
            }
        }

        public IQueryable<AccVoucher> getAllOfVouchers()
        {
            var entities = this.GetPersitantStorage();
            return entities.AccVouchers.AsQueryable();
        }

        public AddStatus AddVoucher( long foodOrderId, string description, long priceOfService, DateTime dateTime)
        {
            var cachAccountId =0;
            var incomeServiceAccountId = 0;
            AddStatus addStatus = AddStatus.RejectedByInvalidParameter;

            var configManager = ConfigFactory.GetConfigManager(ConfigManagerType.WebConfig);
            
            
            if (!int.TryParse(configManager.ReadSetting("CacheAccountId"), out cachAccountId) || !int.TryParse(configManager.ReadSetting("IncomeServiceAccountId"), out incomeServiceAccountId) || foodOrderId < 0 || string.IsNullOrEmpty(description))
                return addStatus;

            var cachAccount = this.GetAccountById(cachAccountId);//hesabe mojoodi naghd => Daraee => zatan Bedehkar
            var incomeServiceAccount = this.GetAccountById(incomeServiceAccountId);//hesabe daramad khadamat => Sarmaye=> Zatan Bestankar
            if (cachAccount == null || incomeServiceAccount == null)
                return addStatus;

            var foodOrder = OfferManager.Instance.getAllOfOrderedOffers().FirstOrDefault(e => e.OfferFoodOrderId.Equals(foodOrderId));
            if (foodOrder == null)
                return addStatus;

            var entities = this.GetPersitantStorage();
            entities.AccVouchers.Add(new AccVoucher()
            {
                AccountId = cachAccountId,
                Credit = null,
                Debit = priceOfService,
                DateTime = dateTime,
                Description = description,
                OrderId = foodOrderId,
            });
            entities.AccVouchers.Add(new AccVoucher()
            {
                AccountId = incomeServiceAccountId,
                Credit = priceOfService,
                Debit = null,
                DateTime = dateTime,
                Description = description,
                OrderId = foodOrderId,
            });

            int effected = this.AcceptChanges();
            if (effected > 0)
            {
                addStatus = AddStatus.Added;
            }
            else
            {
                addStatus = AddStatus.RejectedByNonEffectiveQuery;
            }

            return addStatus;
        }

        public AccAccount GetAccountById(int accId)
        {
            var entities = this.GetPersitantStorage();
            return entities.AccAccounts.FirstOrDefault(e => e.AccountId.Equals(accId));
        }
        public IQueryable<AccAccount> GetAllAccounts()
        {
            var entities = this.GetPersitantStorage();
            return entities.AccAccounts.AsQueryable();
        }
    }
}
