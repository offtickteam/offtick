﻿using Offtick.Core.EnumTypes;
using Offtick.Data.Context.ExpertOnlinerContexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Offtick.Core.Exceptions;
using System.Linq.Expressions;
namespace Offtick.Business.Services.Managers
{
    public class ApplicationManager:BaseManager
    {

        private static string mbtiSessionKey;
        private static string hollandSessionKey;
        private static string majorChoiceSessionKey;
        private static string randomNumberSessionKey;

           private static ApplicationManager instance;
        public static ApplicationManager Instance
        {
            get
            {
                if (instance != null)
                    return instance;
                lock (typeof(ApplicationManager))
                {
                    if (instance == null)
                        instance = new ApplicationManager();
                    return instance;
                }
            }
        }


        private ApplicationManager()
        {
            mbtiSessionKey = "mbtiSessionResutObjectKey";
            hollandSessionKey = "hollandSessionResutObjectKey";
            majorChoiceSessionKey = "majorChoiceSessionResutObjectKey";
            randomNumberSessionKey = "randomNumberSessionObjectKey";
        }

        public IList<Application> GetAllApplications()
        {
            var db = this.GetPersitantStorage();
            return db.Applications.Where(e => e.IsActive == true && !e.ParentApplicationID.HasValue).OrderByDescending(e=>e.PresentationOrder).ToList();
        }
        public IList<Application> GetApplicationsByCriteria(string[] searchCriterias)
        {
            var db = this.GetPersitantStorage();
            
             Expression<Func<Application, bool>> ex=null;
            foreach (var criteria in searchCriterias)
                ex = ex.Or(e => e.Name.Contains(criteria) || e.Description.Contains(criteria));

            return db.Applications.Where(ex).ToList();
            

        }


        public Application GetApplicationById(long applicationId)
        {
            var db = this.GetPersitantStorage();
            return db.Applications.FirstOrDefault(e => e.ApplicationID.Equals(applicationId));
        }

        public ApplicationRandomNumber GetApplicationRandomNumberByNumber(long number)
        {
            var db = this.GetPersitantStorage();
            return db.ApplicationRandomNumbers.FirstOrDefault(e => e.Number.Equals(number));
        }


        public long CountOfTaken(Application application)
        {
            int taken = 0;
            if (application != null)
            {
                switch (application.Name)
                {
                    case "MBTI":
                        taken = GetMBTIApplication().ApplicationMBTIResults.Count();
                        break;
                    case "Holland":
                        taken = GetHollandApplication().ApplicationHollandResults.Count();
                        break;
                    case "SelectMajor":
                        taken = GetSelectMajorApplication().ApplicationMajorChoiceResults.Count();
                        break;
                }
            }
            return taken;
        }
        public long CountOfTaken(long applicationId)
        {
            var app = GetApplicationById(applicationId);
            return CountOfTaken(app);
        }

        #region Holland
        public Application GetHollandApplication()
        {
            return this.GetApplicationById(2);
        }

        public IList<ApplicationHolland> getHollandApplicationQuestionsByRange(int numberOfQuestionInOnePage,int pageNumber,int level)
        {
            int skip=(pageNumber-1)*numberOfQuestionInOnePage;
            var hollandApplication = GetHollandApplication();
            if (hollandApplication != null)
            {
                if (level == 1)
                    return hollandApplication.ApplicationHollands.Where(e => e.Level == level).Skip(skip).Take(numberOfQuestionInOnePage).ToList();
                else return hollandApplication.ApplicationHollands.Where(e => e.Level == 2).ToList();
            }
            return null;
        }
        public ApplicationHolland getHollandApplicationQuestionById(long questionId)
        {
            var hollandApplication = GetHollandApplication();
            if (hollandApplication != null)
            {
                return hollandApplication.ApplicationHollands.FirstOrDefault(e => e.ApplicationHollandId.Equals(questionId));
            }
            return null;
        }
        public ApplicationHollandResult GetHollandResultForUser(MembershipUser user)
        {
            if (user != null)
                return user.ApplicationHollandResults.FirstOrDefault();
            else
                return (ApplicationHollandResult)System.Web.HttpContext.Current.Session[hollandSessionKey]; 
        }

        public bool isExistedAnswerForHolland(MembershipUser user, long questionId)
        {
            
                var hollandObject = GetHollandResultForUser(user);
                if (hollandObject == null)
                    return false;

                if (hollandObject != null)
                {
                    var answerExisted = hollandObject.ApplicationHollandResultDetails.FirstOrDefault(e => e.ApplicationHollandId.Equals(questionId));
                    if (answerExisted != null)
                        return true;
                }
            
            return false;
        }
        public bool isValidPreviousAnswersForHolland(MembershipUser user, long questionId)
        {
           
                if (questionId == 0 || questionId == 1)
                    return true;
                var hollandObject = GetHollandResultForUser(user);
                if (hollandObject == null)
                    return false;

                var question = getHollandApplicationQuestionById(questionId);

                if (question != null)
                {
                    long previousQuestionIdMustToExist = this.getPreviousHollandQuestion(user, questionId);
                    var answerExisted = hollandObject.ApplicationHollandResultDetails.FirstOrDefault(e => e.ApplicationHollandId.Equals(previousQuestionIdMustToExist));
                    if (answerExisted != null)
                        return true;
                }
            
            return false;
        }
        public long getNextHollandQuestion(MembershipUser user, long questionId)
        {
            if (!isValidAnswersForHolland(user))
            {
                if (questionId >= 0 && questionId < 228)
                    return ++questionId;
            }
            return 0;
        }
        public long getLastHollandQuestion(MembershipUser user)
        {
            if (!isValidAnswersForHolland(user))
            {
                var hollandResult = this.GetHollandResultForUser(user);
                if (hollandResult != null)
                {
                    var lastQuestion = hollandResult.ApplicationHollandResultDetails.OrderBy(e => e.ApplicationHollandId).LastOrDefault();
                    if (lastQuestion != null)
                        return lastQuestion.ApplicationHollandId;
                }
                else
                    return 0;

            }
            return -1;
        }


        public long getPreviousHollandQuestion(MembershipUser user, long questionId)
        {
            if (!isValidAnswersForHolland(user))
            {
                if (questionId == 1)
                    return 1;
                if (questionId > 1 && questionId <= 228)
                    return --questionId;
               
            }
            return 0;
        }

        public bool isValidAnswersForHolland(MembershipUser user)
        {
            var hollandResult = this.GetHollandResultForUser(user);
            return isValidAnswersForHolland(hollandResult);
        }
        public bool isValidAnswersForHolland(ApplicationHollandResult hollandResult)
        {
            if (hollandResult != null && !string.IsNullOrEmpty(hollandResult.Result) && hollandResult.Result.Length == 3)
                return true;
            return false;
        }

        public bool ResetHollandFor(MembershipUser user)
        {
            bool result = false;
            if (user != null)
            {
                var hollandResult = this.GetHollandResultForUser(user);
                if (isValidAnswersForHolland(user))
                {
                    var db = this.GetPersitantStorage();
                    if (hollandResult.PasswordNumber.HasValue)
                        System.Web.HttpContext.Current.Session[randomNumberSessionKey] = hollandResult.PasswordNumber.Value;
                    db.ApplicationHollandResults.Remove(hollandResult);
                    int effected = this.AcceptChanges();
                    result = effected > 0 ? true : false;
                }
            }
            else
            {
                System.Web.HttpContext.Current.Session[hollandSessionKey] = null;
                result = true;
            }
            return result;
        }

        public AddStatus AddOrUpdateAnswerForHolland(MembershipUser user, long questionId, short value)
        {
            var hollandObject = GetHollandResultForUser(user);
            if (hollandObject != null && !hollandObject.PasswordNumber.HasValue)
            {
                var randomNumberObject=System.Web.HttpContext.Current.Session[randomNumberSessionKey];
                if (randomNumberObject != null)
                {
                    hollandObject.PasswordNumber = (long)randomNumberObject;
                }
            }

            if (user != null)
            {
                
                if (hollandObject == null)
                {
                    hollandObject = new ApplicationHollandResult()
                    {
                        ApplicationId = this.GetHollandApplication().ApplicationID,
                        Datetime = DateTime.Now,
                    };
                    user.ApplicationHollandResults.Add(hollandObject);
                    this.AcceptChanges();
                }
                var question = getHollandApplicationQuestionById(questionId);

                if (hollandObject != null && question != null)
                {

                    var answerExisted = hollandObject.ApplicationHollandResultDetails.FirstOrDefault(e => e.ApplicationHollandId.Equals(questionId));
                    if (answerExisted != null)
                    {
                        answerExisted.Value = value;
                        answerExisted.DateTime = DateTime.Now;
                    }
                    else
                    {
                        hollandObject.ApplicationHollandResultDetails.Add(new ApplicationHollandResultDetail()
                        {
                            Value = value,
                            ApplicationHollandId = questionId,
                            DateTime = DateTime.Now,
                        });
                    }

                    int effeted = this.AcceptChanges();

                    if (effeted > 0)
                    {
                        if (this.getNextHollandQuestion(user, questionId) == 0)
                            calculteHollandValueFor(hollandObject);
                        return AddStatus.Added;
                    }
                    else
                        return AddStatus.RejectedByNonEffectiveQuery;
                }
            }
            else
            {
                
                if (hollandObject == null)
                {
                    hollandObject = new ApplicationHollandResult()
                    {
                        ApplicationId = this.GetHollandApplication().ApplicationID,
                        Datetime = DateTime.Now,
                    };
                    System.Web.HttpContext.Current.Session[hollandSessionKey]= hollandObject;
                    
                }
                var question = getHollandApplicationQuestionById(questionId);

                if (hollandObject != null && question != null)
                {

                    var answerExisted = hollandObject.ApplicationHollandResultDetails.FirstOrDefault(e => e.ApplicationHollandId.Equals(questionId));
                    if (answerExisted != null)
                    {
                        answerExisted.Value = value;
                        answerExisted.DateTime = DateTime.Now;
                    }
                    else
                    {
                        hollandObject.ApplicationHollandResultDetails.Add(new ApplicationHollandResultDetail()
                        {
                            Value = value,
                            ApplicationHollandId = questionId,
                            DateTime = DateTime.Now,
                        });
                    }
                    
                        if (this.getNextHollandQuestion(user, questionId) == 0)
                            calculteHollandValueFor(hollandObject);
                        return AddStatus.Added;
                    
                }
            }
            return AddStatus.RejectedByParentNotExit;
        }


        public IList<ApplicationHollandInterpret> getHollandInterpretFor(string result)
        {
            if (!string.IsNullOrEmpty(result) && result.Length == 3)
            {
                string cat1 = result.Substring(0, 1);
                string cat2 = result.Substring(1, 1);
                string cat3 = result.Substring(2, 1);
                var db = this.GetPersitantStorage();
                return db.ApplicationHollandInterprets.Where(e => e.Category1.Equals(cat1) && e.Category2.Equals(cat2) && e.Category3.Equals(cat3)).ToList();
            }
            return null;
        }

        private void calculteHollandValueFor(ApplicationHollandResult hollandTest)
        {
            string category = "";
            if (hollandTest != null)
            {
                 var db = this.GetPersitantStorage();

                 var questionCategoryA = db.ApplicationHollands.Where(e => e.Category == "A").ToList();
                 var questionCategoryC = db.ApplicationHollands.Where(e => e.Category == "C").ToList();
                 var questionCategoryE = db.ApplicationHollands.Where(e => e.Category == "E").ToList();
                 var questionCategoryI = db.ApplicationHollands.Where(e => e.Category == "I").ToList();
                 var questionCategoryR = db.ApplicationHollands.Where(e => e.Category == "R").ToList();
                 var questionCategoryS = db.ApplicationHollands.Where(e => e.Category == "S").ToList();

                 var a_count = (from a in hollandTest.ApplicationHollandResultDetails
                                join q in questionCategoryA on a.ApplicationHollandId equals q.ApplicationHollandId
                                select a).Sum(e => e.Value);

                 var c_count = (from a in hollandTest.ApplicationHollandResultDetails
                                join q in questionCategoryC on a.ApplicationHollandId equals q.ApplicationHollandId
                                select a).Sum(e => e.Value);

                 var e_count = (from a in hollandTest.ApplicationHollandResultDetails
                                join q in questionCategoryE on a.ApplicationHollandId equals q.ApplicationHollandId
                                select a).Sum(e => e.Value);


                 var i_count = (from a in hollandTest.ApplicationHollandResultDetails
                                join q in questionCategoryI on a.ApplicationHollandId equals q.ApplicationHollandId
                                select a).Sum(e => e.Value);

                 var r_count = (from a in hollandTest.ApplicationHollandResultDetails
                                join q in questionCategoryR on a.ApplicationHollandId equals q.ApplicationHollandId
                                select a).Sum(e => e.Value);


                 var s_count = (from a in hollandTest.ApplicationHollandResultDetails
                                join q in questionCategoryS on a.ApplicationHollandId equals q.ApplicationHollandId
                                select a).Sum(e => e.Value);


                Dictionary<string, int> dics = new Dictionary<string,int>();
                dics.Add("A", a_count);
                dics.Add("C", c_count);
                dics.Add("E", e_count);
                dics.Add("I", i_count);
                dics.Add("R", r_count);
                dics.Add("S", s_count);
                var sorted=from pair in dics
	                            orderby pair.Value descending
	                            select pair;
                int counter=0;
                foreach (var pair in sorted)
                {
                    counter++;
                    category += pair.Key;
                    if (counter == 3)
                        break;
                }
            }
            hollandTest.Result = category;


            var currentUser = Membership.MembershipManager.Instance.GetCurrentUser();
            if (currentUser != null)
            {
                this.AcceptChanges();
                if (hollandTest.PasswordNumber.HasValue)
                    ApplicationManager.instance.MarkRandomNumberAsUsed(hollandTest.PasswordNumber.Value);
                
            }
        }

        public bool persistHollandResultForCurrentUser()
        {
            var user = Membership.MembershipManager.Instance.GetCurrentUser();

            var hollandResultSessionBased = (ApplicationHollandResult)System.Web.HttpContext.Current.Session[hollandSessionKey];
            if (user != null && hollandResultSessionBased != null && isValidAnswersForHolland(hollandResultSessionBased))
            {
                if (hollandResultSessionBased.PasswordNumber.HasValue)
                    ApplicationManager.instance.MarkRandomNumberAsUsed(hollandResultSessionBased.PasswordNumber.Value);

                this.ResetMBTIFor(user);
                user.ApplicationHollandResults.Add(hollandResultSessionBased);
                int effected = this.AcceptChanges();
                if (effected > 0)
                    return true;
            }
            return false;
        }
        #endregion
        
        
        #region MBTI
        public Application GetMBTIApplication()
        {
            return this.GetApplicationById(1);
        }
        public ApplicationMBTI getMBTIApplicationQuestionById(long questionId)
        {
            var mbtiApplication = GetMBTIApplication();
            if (mbtiApplication != null)
            {
                return mbtiApplication.ApplicationMBTIs.FirstOrDefault(e => e.ApplicationMBTIId.Equals(questionId));
            }
            return null;
        }
        public ApplicationMBTIResult GetMBTIResultForUser(MembershipUser user)
        {
            if (user != null)
                return user.ApplicationMBTIResults.FirstOrDefault();
            else
                return (ApplicationMBTIResult)System.Web.HttpContext.Current.Session[mbtiSessionKey];
        }

        public bool isExistedAnswerForMBTI(MembershipUser user, long questionId)
        {

                var mbtiObject = GetMBTIResultForUser(user);
                if (mbtiObject == null)
                    return false;

                if (mbtiObject != null )
                {
                    var answerExisted = mbtiObject.ApplicationMBTIResultDetails.FirstOrDefault(e => e.ApplicationMBTIId.Equals(questionId));
                    if (answerExisted != null)
                        return true;
                }
            
            return false;
        }

        public bool isValidPreviousAnswersForMBTI(MembershipUser user, long questionId)
        {

            if (questionId == 0 || questionId == 1)
                return true;
            var mbtiObject = GetMBTIResultForUser(user);
            if (mbtiObject == null)
                return false;

            var question = getMBTIApplicationQuestionById(questionId);

            if (question != null)
            {
                long previousQuestionIdMustToExist = this.getPreviousMBTIQuestion(user, questionId);
                var answerExisted = mbtiObject.ApplicationMBTIResultDetails.FirstOrDefault(e => e.ApplicationMBTIId.Equals(previousQuestionIdMustToExist));
                if (answerExisted != null)
                    return true;
            }

            return false;
        }
        public IList<ApplicationMBTI> GetNextMBTIQuestions(string mbtiResult)
        {
            if (isValidAnswersForMBTI(mbtiResult))
                return null;
            IList<ApplicationMBTI> ls = new List<ApplicationMBTI>();
            long questionId = 28;
            do
            {
                 questionId = getNextMBTIQuestion(mbtiResult, questionId);
                if(questionId!=0)
                    ls.Add(getMBTIApplicationQuestionById( questionId));
            } while (questionId!=0);
            return ls;
        }

        private long getNextMBTIQuestion(string mbtiResult, long questionId)
        {
            if (questionId >= 0 && questionId < 28)
                return ++questionId;
            else
            {
                int index4 = (int)(questionId % 4);
               
                bool e_i_flag = mbtiResult.Substring(0, 1).Equals("-");
                bool s_n_flag = mbtiResult.Substring(1, 1).Equals("-");
                bool t_f_flag = mbtiResult.Substring(2, 1).Equals("-");
                bool p_j_flag = mbtiResult.Substring(3, 1).Equals("-");

                long nextIndex = -1;
                switch (index4)
                {
                    case 0:
                        if (e_i_flag == true) nextIndex = questionId + 1;
                        else if (s_n_flag == true) nextIndex = questionId + 2;
                        else if (t_f_flag == true) nextIndex = questionId + 3;
                        else if (p_j_flag == true) nextIndex = questionId + 4;


                        break;
                    case 1:
                        if (s_n_flag == true) nextIndex = questionId + 1;
                        else if (t_f_flag == true) nextIndex = questionId + 2;
                        else if (p_j_flag == true) nextIndex = questionId + 3;
                        else if (e_i_flag == true) nextIndex = questionId + 4;


                        break;
                    case 2:

                        if (t_f_flag == true) nextIndex = questionId + 1;
                        else if (p_j_flag == true) nextIndex = questionId + 2;
                        else if (e_i_flag == true) nextIndex = questionId + 3;
                        else if (s_n_flag == true) nextIndex = questionId + 4;

                        break;
                    case 3:
                        if (p_j_flag == true) nextIndex = questionId + 1;
                        else if (e_i_flag == true) nextIndex = questionId + 2;
                        else if (s_n_flag == true) nextIndex = questionId + 3;
                        else if (t_f_flag == true) nextIndex = questionId + 4;

                        break;
                }
                if (nextIndex <= 88)
                    return nextIndex;
                else
                    return 0;
            }
        }
        public long getNextMBTIQuestion(MembershipUser user, long questionId)
        {
            if (!isValidAnswersForMBTI(user))
            {
                var mbtiResult = GetMBTIResultForUser(user);
                string result = mbtiResult.Result;
                return getNextMBTIQuestion(result, questionId);
            }
            return 0;
        }
        public long getLastMBTIQuestion(MembershipUser user)
        {
            if ( !isValidAnswersForMBTI(user))
            {
                var mbtiResult = this.GetMBTIResultForUser(user);
                if (mbtiResult != null)
                {
                    var lastQuestion=mbtiResult.ApplicationMBTIResultDetails.OrderBy(e => e.ApplicationMBTIId).LastOrDefault();
                    if (lastQuestion != null)
                        return lastQuestion.ApplicationMBTIId;
                }
                else
                    return 0;

            }
            return -1;
        }


      
        public long getPreviousMBTIQuestion(MembershipUser user, long questionId)
        {
            if (!isValidAnswersForMBTI(user))
            {
                if (questionId == 1)
                    return 1;
                if (questionId > 1 && questionId <= 28)
                    return --questionId;
                else
                {
                    int index4 = (int)(questionId % 4);
                    var mbtiResult = GetMBTIResultForUser(user);
                    string result = mbtiResult.Result;

                    
                    bool e_i_flag = result.Substring(0, 1).Equals("-");
                    bool s_n_flag = result.Substring(1, 1).Equals("-");
                    bool t_f_flag = result.Substring(2, 1).Equals("-");
                    bool p_j_flag = result.Substring(3, 1).Equals("-");

                    long prevIndex = -1;
                    switch (index4)
                    {
                        case 0:
                             if (t_f_flag == true) prevIndex = questionId - 1;
                            else if (s_n_flag == true) prevIndex = questionId - 2;
                            else if (e_i_flag == true) prevIndex = questionId - 3;
                            if (p_j_flag == true) prevIndex = questionId - 4;
                            break;

                        case 1:
                            if (p_j_flag == true) prevIndex = questionId - 1;
                            else if (t_f_flag == true) prevIndex = questionId - 2;
                            else if (s_n_flag == true) prevIndex = questionId - 3;
                            else if (e_i_flag == true) prevIndex = questionId - 4;
                            break;

                        case 2:
                             if (e_i_flag == true) prevIndex = questionId -1 ;
                            else if (p_j_flag == true) prevIndex = questionId - 2;
                             else if (t_f_flag == true) prevIndex = questionId - 3;
                            else if (s_n_flag == true) prevIndex = questionId - 4;

                            break;
                        case 3:
                             if (s_n_flag == true) prevIndex = questionId - 1;
                            else if (e_i_flag == true) prevIndex = questionId - 2;
                            else if (p_j_flag == true) prevIndex = questionId - 3;
                            else if (t_f_flag == true) prevIndex = questionId - 4;

                            break;
                    }
                    if (prevIndex <= 28)
                        return 28;
                    return prevIndex;
                }
            }
            return 0;
        }

        public bool isValidAnswersForMBTI(MembershipUser user)
        {
            var mbtiResult = this.GetMBTIResultForUser(user);
            return isValidAnswersForMBTI(mbtiResult);
        }
        public bool isValidAnswersForMBTI(ApplicationMBTIResult mbtiResult)
        {
            if (mbtiResult != null && mbtiResult.Result != null && !string.IsNullOrEmpty(mbtiResult.Result) && !mbtiResult.Result.Contains('-') && mbtiResult.Result.Length == 4)
                return true;
            return false;
        }
        public bool isValidAnswersForMBTI(string mbtiResult)
        {
            if (mbtiResult != null  && !string.IsNullOrEmpty(mbtiResult) && !mbtiResult.Contains('-') && mbtiResult.Length == 4)
                return true;
            return false;
        }
        public bool ResetMBTIFor(MembershipUser user)
        {
            bool result=false;
            if (user != null)
            {
                var mbtiResult = this.GetMBTIResultForUser(user);
                if (isValidAnswersForMBTI(user))
                {
                    var db = this.GetPersitantStorage();
                    db.ApplicationMBTIResults.Remove(mbtiResult);
                    int effected = this.AcceptChanges();
                    result = effected > 0 ? true : false;
                }
            }
            else
            {
                System.Web.HttpContext.Current.Session[mbtiSessionKey] = null;
                result = true;
            }
            return result;
        }

        public AddStatus AddOrUpdateAnswerForMBTI(MembershipUser user, Dictionary<short, short> answers)
        {
            if (user == null)
                return AddStatus.RejectedByInvalidParameter;
            var mbtiObject = user.ApplicationMBTIResults.FirstOrDefault();
            if(mbtiObject==null)
            { 
                 mbtiObject = new ApplicationMBTIResult()
                {
                    ApplicationId = this.GetMBTIApplication().ApplicationID,
                    Datetime = DateTime.Now,
                };
                user.ApplicationMBTIResults.Add(mbtiObject);
                this.AcceptChanges();
            }

            foreach (var pair in answers)
            {
                var question = getMBTIApplicationQuestionById(pair.Key);
                string category = pair.Value == 1 ? question.AnswerACategory : question.AnswerBCategory;
                mbtiObject.ApplicationMBTIResultDetails.Add(new ApplicationMBTIResultDetail()
                {
                    AnswerCategory = category,
                    ApplicationMBTIId = pair.Key,
                    DateTime = DateTime.Now,
                });
            }
            int effeted = this.AcceptChanges();
            calculteMBTIValueFor(mbtiObject);
            return AddStatus.Added;
        }

        public AddStatus AddOrUpdateAnswerForMBTI(MembershipUser user, long questionId, short answerId)
        {
            if (user != null)
            {
                var mbtiObject = GetMBTIResultForUser(user);
                if (mbtiObject == null)
                {
                    mbtiObject = new ApplicationMBTIResult()
                    {
                        ApplicationId = this.GetMBTIApplication().ApplicationID,
                        Datetime = DateTime.Now,
                    };
                    user.ApplicationMBTIResults.Add(mbtiObject);
                    this.AcceptChanges();
                }
                var question = getMBTIApplicationQuestionById(questionId);

                if (mbtiObject != null && question != null)
                {

                    var answerExisted = mbtiObject.ApplicationMBTIResultDetails.FirstOrDefault(e => e.ApplicationMBTIId.Equals(questionId));
                    string category = answerId == 1 ? question.AnswerACategory : question.AnswerBCategory;
                    if (answerExisted != null)
                    {
                        answerExisted.AnswerCategory = category;
                        answerExisted.DateTime = DateTime.Now;
                    }
                    else
                    {
                        mbtiObject.ApplicationMBTIResultDetails.Add(new ApplicationMBTIResultDetail()
                        {
                            AnswerCategory = category,
                            ApplicationMBTIId = questionId,
                            DateTime = DateTime.Now,
                        });
                    }

                    int effeted = this.AcceptChanges();

                    if (effeted > 0)
                    {
                        if (questionId == 28 || (questionId > 84 && this.getNextMBTIQuestion(user, questionId) == 0))
                            calculteMBTIValueFor(mbtiObject);
                        return AddStatus.Added;
                    }
                    else
                        return AddStatus.RejectedByNonEffectiveQuery;
                }
            }
            else
            {
                var mbtiObject = GetMBTIResultForUser(user);
                if (mbtiObject == null)
                {
                    mbtiObject = new ApplicationMBTIResult()
                    {
                        ApplicationId = this.GetMBTIApplication().ApplicationID,
                        Datetime = DateTime.Now,
                    };
                     System.Web.HttpContext.Current.Session[mbtiSessionKey]=mbtiObject;
                }
                var question = getMBTIApplicationQuestionById(questionId);

                if (mbtiObject != null && question != null)
                {

                    var answerExisted = mbtiObject.ApplicationMBTIResultDetails.FirstOrDefault(e => e.ApplicationMBTIId.Equals(questionId));
                    string category = answerId == 1 ? question.AnswerACategory : question.AnswerBCategory;
                    if (answerExisted != null)
                    {
                        answerExisted.AnswerCategory = category;
                        answerExisted.DateTime = DateTime.Now;
                    }
                    else
                    {
                        mbtiObject.ApplicationMBTIResultDetails.Add(new ApplicationMBTIResultDetail()
                        {
                            AnswerCategory = category,
                            ApplicationMBTIId = questionId,
                            DateTime = DateTime.Now,
                        });
                    }

                    if (questionId == 28 || (questionId > 84 && this.getNextMBTIQuestion(user, questionId) == 0))
                        calculteMBTIValueFor(mbtiObject);
                    return AddStatus.Added;

                }
            }
            return AddStatus.RejectedByParentNotExit;
        }

        private void calculteMBTIValueFor(ApplicationMBTIResult mbtiTest)
        {
            string category="";
            if (mbtiTest != null)
            {
                var e_count = mbtiTest.ApplicationMBTIResultDetails.Count(e => e.AnswerCategory == "E");
                var I_count = mbtiTest.ApplicationMBTIResultDetails.Count(e => e.AnswerCategory == "I");
                category += e_count > I_count ? "E" : I_count > e_count ? "I" : "-";

                var s_count = mbtiTest.ApplicationMBTIResultDetails.Count(e => e.AnswerCategory == "S");
                var n_count = mbtiTest.ApplicationMBTIResultDetails.Count(e => e.AnswerCategory == "N");
                category += s_count > n_count ? "S" : n_count > s_count ? "N" : "-";

                var t_count = mbtiTest.ApplicationMBTIResultDetails.Count(e => e.AnswerCategory == "T");
                var f_count = mbtiTest.ApplicationMBTIResultDetails.Count(e => e.AnswerCategory == "F");
                category += t_count > f_count ? "T" : f_count > t_count ? "F" : "-";

                var j_count = mbtiTest.ApplicationMBTIResultDetails.Count(e => e.AnswerCategory == "J");
                var p_count = mbtiTest.ApplicationMBTIResultDetails.Count(e => e.AnswerCategory == "P");
                category += j_count > p_count ? "J" : p_count > j_count ? "P" : "-";

            }
            mbtiTest.Result = category;
            if(mbtiTest.MembershipUser!=null) //add this condition to consider only registered user, the session object will not persist only when user is registered
                this.AcceptChanges();
        }

        public bool persistMBTIResultForCurrentUser()
        {
            var user=Membership.MembershipManager.Instance.GetCurrentUser();
            
            var mbtiResultSessionBased=(ApplicationMBTIResult)System.Web.HttpContext.Current.Session[mbtiSessionKey];
            if (user != null && mbtiResultSessionBased != null && isValidAnswersForMBTI(mbtiResultSessionBased))
            {
                this.ResetMBTIFor(user);
                user.ApplicationMBTIResults.Add(mbtiResultSessionBased);
                int effected=this.AcceptChanges();
                if (effected > 0)
                    return true;
                
            }
            return false;
        }
        #endregion


        #region SelectMajor
        public Application GetSelectMajorApplication()
        {
            return this.GetApplicationsByCriteria(new[] { "SelectMajor" })[0];
        }

        public IList<ApplicationMajorChoiceHighSchool> GetHighSchoolMajors()
        {
              var db = this.GetPersitantStorage();
              return db.ApplicationMajorChoiceHighSchools.ToList();
        }
        public IList<ApplicationMajorChoiceCity> GetUniversityCities(int highSchoolMajorId)
        {
            var db = this.GetPersitantStorage();
            return db.ApplicationMajorChoiceCities.Where(e => e.HighSchoolMajorId.Equals(highSchoolMajorId)).ToList();
        }
        public IList<ApplicationMajorChoiceUniversity> GetUniversities(int highSchoolMajorId)
        {
            var db = this.GetPersitantStorage();
            return db.ApplicationMajorChoiceUniversities.Where(e => e.HighSchoolMajorId.Equals(highSchoolMajorId)).ToList();
        }
        public IList<ApplicationMajorChoiceUniversityMajor> GetUniversityMajors(int highSchoolMajorId)
        {
            var db = this.GetPersitantStorage();
            return db.ApplicationMajorChoiceUniversityMajors.Where(e => e.HighSchoolMajorId.Equals(highSchoolMajorId)).ToList();
        }






        public ApplicationMajorChoiceResult GetMajorChoiceResultForUser(MembershipUser user)
        {
            if (user != null)
                return user.ApplicationMajorChoiceResults.FirstOrDefault();
            else
                return (ApplicationMajorChoiceResult)System.Web.HttpContext.Current.Session[majorChoiceSessionKey];
        }

        public bool isExistedChoiceForMajorChoice(MembershipUser user, int cityId,int majorId,int universityId)
        {

            var majorChoicebject = GetMajorChoiceResultForUser(user);
            if (majorChoicebject == null)
                return false;

            if (majorChoicebject != null && majorChoicebject.ApplicationMajorChoiceResultDetails!=null)
            {

                var answerExisted = majorChoicebject.ApplicationMajorChoiceResultDetails.FirstOrDefault
                    (e => e.ApplicationMajorChoiceCityId.Equals(cityId)
                    && e.ApplicationMajorChoiceUniversityId.Equals(universityId)
                    && e.ApplicationMajorChoiceUniversityMajorId.Equals(majorId));
                if (answerExisted != null)
                    return true;
            }

            return false;
        }
        public bool isExistedGradeForMajorChoice(MembershipUser user, int? cityId, int? cityGrade, int? majorId, int? majorGrade, int? universityId, int? universityGrade)
        {

            var majorChoicebject = GetMajorChoiceResultForUser(user);
            if (majorChoicebject == null)
                return false;

            if (majorChoicebject != null )
            {

                var answerExisted = majorChoicebject.ApplicationMajorChoiceResultDetails.FirstOrDefault
                    (e => e.ApplicationMajorChoiceCityId.Equals(cityId)
                    && e.ApplicationMajorChoiceUniversityId.Equals(universityId)
                    && e.ApplicationMajorChoiceUniversityMajorId.Equals(majorId));
                if (answerExisted != null)
                    return true;
            }

            return false;
        }

       

    

        public bool isCalculatedForMajorChoice(MembershipUser user)
        {
            var result = this.GetMajorChoiceResultForUser(user);
            return isCalculatedForMajorChoice(result);
        }
        public bool isCalculatedForMajorChoice(ApplicationMajorChoiceResult result)
        {
            if (result != null && result.IsCalculated && result.ApplicationMajorChoiceResultDetails!=null && result.ApplicationMajorChoiceResultDetails.Count>0)
                return true;
            return false;
        }

        public bool ResetMajorChoiceFor(MembershipUser user)
        {
            bool result = false;
            if (user != null)
            {
                var resultObject = this.GetMajorChoiceResultForUser(user);
                if (resultObject != null)
                {
                    var db = this.GetPersitantStorage();
                    if(resultObject.PasswordNumber.HasValue)
                    System.Web.HttpContext.Current.Session[randomNumberSessionKey] = resultObject.PasswordNumber.Value;

                    db.ApplicationMajorChoiceResults.Remove(resultObject);
                    int effected = this.AcceptChanges();
                    result = effected > 0 ? true : false;
                }
              
            }
            else
            {
                System.Web.HttpContext.Current.Session[majorChoiceSessionKey] = null;
                result = true;
            }
            return result;
        }


        public AddStatus AddOrUpdateRandomNumber( long randomNumber)
        {
            System.Web.HttpContext.Current.Session[randomNumberSessionKey] = randomNumber;
            return AddStatus.Added;
        }
        public AddStatus MarkRandomNumberAsUsed(long randomNumber)
        {
            var entity=this.GetApplicationRandomNumberByNumber(randomNumber);
            if (entity != null)
            {
                entity.IsUsed = true;
                int effeted = this.AcceptChanges();
                if (effeted > 0)
                {
                    return AddStatus.Added;
                }
                else
                    return AddStatus.RejectedByNonEffectiveQuery;
            }
            return AddStatus.Added;
        }

        public AddStatus AddOrUpdateMajorChoiceHighSchool(MembershipUser user, int highSchoolId)
        {
            var randomNumber = (long)System.Web.HttpContext.Current.Session[randomNumberSessionKey];

            var choiceResult = GetMajorChoiceResultForUser(user);
            if (choiceResult == null)
            {
                choiceResult = new ApplicationMajorChoiceResult()
                {
                    ApplicationId = this.GetSelectMajorApplication().ApplicationID,
                    DateTime = DateTime.Now,
                };
                if (user != null)
                    user.ApplicationMajorChoiceResults.Add(choiceResult);
            }



            
            if ( choiceResult.ApplicationMajorChoiceHighSchoolId != highSchoolId)
            {
                choiceResult.IsCalculated = false;
                if (choiceResult.ApplicationMajorChoiceHighSchoolId != 0)//existed before
                    ApplicationManager.Instance.ResetMajorChoiceFor(user);
            }

            
            choiceResult.ApplicationMajorChoiceHighSchoolId = highSchoolId;
            choiceResult.PasswordNumber = randomNumber;
            if (user != null)
            {
                int effeted = this.AcceptChanges();
                if (effeted > 0)
                {
                    return AddStatus.Added;
                }
                else
                    return AddStatus.RejectedByNonEffectiveQuery;
            }
            else
            {
                System.Web.HttpContext.Current.Session[majorChoiceSessionKey] = choiceResult;
                return AddStatus.Added;
            }
        }

        public DeleteStatus ClearMajorChoices(MembershipUser user)
        {
             var choiceResult = GetMajorChoiceResultForUser(user);
             if (choiceResult != null)
             {
                 if (user == null)
                     choiceResult.ApplicationMajorChoiceResultDetails.Clear();
                 else
                 {
                     var itemsToDelete = choiceResult.ApplicationMajorChoiceResultDetails.ToList();
                     var entities = this.GetPersitantStorage();
                     foreach (var itemToDelete in itemsToDelete)
                         entities.ApplicationMajorChoiceResultDetails.Remove(entities.ApplicationMajorChoiceResultDetails.First(e=>e.ApplicationMajaorChoiceResultDetailId==itemToDelete.ApplicationMajaorChoiceResultDetailId));
                         //entities.Entry(itemToDelete).State = System.Data.EntityState.Deleted;
                   //  choiceResult.ApplicationMajorChoiceResultDetails.Clear();
                    
                 }

                 if (user != null)
                 {
                     int effeted = this.AcceptChanges();
                     if (effeted > 0)
                         return DeleteStatus.Deleted;
                 }
                 else
                 {
                     System.Web.HttpContext.Current.Session[majorChoiceSessionKey] = choiceResult;
                     return DeleteStatus.Deleted;
                 }
             }
                   return DeleteStatus.RejectedByNonEffectiveQuery;
        }
        public AddStatus AddOrUpdateMajorChoice(MembershipUser user, int cityId, int majorId, int universityId)
        {

            var choiceResult = GetMajorChoiceResultForUser(user);
            if (choiceResult == null)
            {
                choiceResult = new ApplicationMajorChoiceResult()
                {
                    ApplicationId = this.GetSelectMajorApplication().ApplicationID,
                    DateTime = DateTime.Now,
                };
                if (user != null)
                    user.ApplicationMajorChoiceResults.Add(choiceResult);
            }

            choiceResult.IsCalculated = false;
            if (!isExistedChoiceForMajorChoice(user, cityId, majorId, universityId))
            {

                choiceResult.ApplicationMajorChoiceResultDetails.Add(new ApplicationMajorChoiceResultDetail
                {
                    ApplicationMajorChoiceCityId = cityId,
                    ApplicationMajorChoiceUniversityMajorId = majorId,
                    ApplicationMajorChoiceUniversityId = universityId,
                });

               
            }

            if (user != null)
            {
                int effeted = this.AcceptChanges();
                if (effeted > 0)
                {
                    return AddStatus.Added;
                }
                else
                    return AddStatus.RejectedByNonEffectiveQuery;
            }
            else
            {
                System.Web.HttpContext.Current.Session[majorChoiceSessionKey] = choiceResult;
                return AddStatus.Added;
            }
        }
        public AddStatus AddOrUpdateMajorChoiceWeight(MembershipUser user, int cityWeight, int majorWeight, int universityWeight)
        {

            var choiceResult = GetMajorChoiceResultForUser(user);
            if (choiceResult == null)
            {
                choiceResult = new ApplicationMajorChoiceResult()
                {
                    ApplicationId = this.GetSelectMajorApplication().ApplicationID,
                    DateTime = DateTime.Now,
                };
                if (user != null)
                    user.ApplicationMajorChoiceResults.Add(choiceResult);
            }

            choiceResult.IsCalculated = false;
            choiceResult.CityWeight = cityWeight;
            choiceResult.MajorWeight = majorWeight;
            choiceResult.UniversityWeight = universityWeight;

            if (user != null)
            {
                int effeted = this.AcceptChanges();
                if (effeted > 0)
                {
                    return AddStatus.Added;
                }
                else
                    return AddStatus.RejectedByNonEffectiveQuery;
            }
            else
            {
                System.Web.HttpContext.Current.Session[majorChoiceSessionKey] = choiceResult;
                return AddStatus.Added;
            }
        }

        public AddStatus RefreshMajorChoiceGrades(MembershipUser user)
        {
            var choiceResult = GetMajorChoiceResultForUser(user);
            if (choiceResult == null)
                return AddStatus.RejectedByParentNotExit;

            choiceResult.IsCalculated = false;

            var entities = this.GetPersitantStorage();

            //create grade objects based on choices that user select them from step 2
            var distinctCities = choiceResult.ApplicationMajorChoiceResultDetails.Select(e=>e.ApplicationMajorChoiceCityId).Distinct().ToList();
            var distinctUniversities = choiceResult.ApplicationMajorChoiceResultDetails.Select(e=>e.ApplicationMajorChoiceUniversityId).Distinct().ToList();
            var distinctMajors = choiceResult.ApplicationMajorChoiceResultDetails.Select(e => e.ApplicationMajorChoiceUniversityMajorId).Distinct().ToList();

            //start of create CityGrade object
            //cleaning cities that removed from selected cites
            var cityKeys = choiceResult.ApplicationMajorChoiceResultCityGrades.Select(e => e.ApplicationMajorChoiceCityId).Distinct().ToList();
            foreach (var key in cityKeys)
            {
                if (!distinctCities.Contains(key))
                {
                    if (user == null)
                    {
                        var itemToDelete = choiceResult.ApplicationMajorChoiceResultCityGrades.First(e => e.ApplicationMajorChoiceCityId.Equals(key));
                        choiceResult.ApplicationMajorChoiceResultCityGrades.Remove(itemToDelete);
                    }
                    else
                    {
                        var itemToDelete = entities.ApplicationMajorChoiceResultCityGrades.First(e => e.ApplicationMajorChoiceCityId.Equals(key));
                        entities.ApplicationMajorChoiceResultCityGrades.Remove(itemToDelete);
                    }
                    
                }
            }
            //adding new selected cities to CityGrade object
            foreach (var key in distinctCities)
            {
                //not exist before adding new item with grade 0 else do not change before value and let it to remain
                if (choiceResult.ApplicationMajorChoiceResultCityGrades.FirstOrDefault(e => e.ApplicationMajorChoiceCityId.Equals(key)) == null)
                    choiceResult.ApplicationMajorChoiceResultCityGrades.Add(new ApplicationMajorChoiceResultCityGrade()
                    {
                         ApplicationMajorChoiceCityId=key,
                         CityGrade=0,
                    });
            }
            //end of create cityGrade Object



            //start of create universityGrade
            //cleaning universities that removed from selected universities
            var universityKeys = choiceResult.ApplicationMajorChoiceResultUniversityGrades.Select(e => e.ApplicationMajorChoiceUniversityId).Distinct().ToList();
            foreach (var key in universityKeys)
            {
                if (!distinctUniversities.Contains(key))
                {
                    if (user == null)
                    {

                        var itemToDelete = choiceResult.ApplicationMajorChoiceResultUniversityGrades.First(e => e.ApplicationMajorChoiceUniversityId.Equals(key));
                        choiceResult.ApplicationMajorChoiceResultUniversityGrades.Remove(itemToDelete);
                    }
                    else
                    {

                        var itemToDelete = entities.ApplicationMajorChoiceResultUniversityGrades.First(e => e.ApplicationMajorChoiceUniversityId.Equals(key));
                        entities.ApplicationMajorChoiceResultUniversityGrades.Remove(itemToDelete);
                    }

                }
            }
            //adding new selected cities to CityGrade object
            foreach (var key in distinctUniversities)
            {
                //not exist before adding new item with grade 0 else do not change before value and let it to remain
                if (choiceResult.ApplicationMajorChoiceResultUniversityGrades.FirstOrDefault(e => e.ApplicationMajorChoiceUniversityId.Equals(key)) == null)
                    choiceResult.ApplicationMajorChoiceResultUniversityGrades.Add(new ApplicationMajorChoiceResultUniversityGrade()
                    {
                        ApplicationMajorChoiceUniversityId = key,
                        UniversityGrade = 0,
                    });
            }
            //end of create universityGrade Object


            //start of create universityGrade
            //cleaning cities that removed from selected cites
            var majorKeys = choiceResult.ApplicationMajorChoiceResultMajorGrades.Select(e => e.ApplicationMajorChoiceUniversityMajorId).Distinct().ToList();
            foreach (var key in majorKeys)
            {
                if (!distinctMajors.Contains(key))
                {
                    if (user == null)
                    {

                        var itemToDelete = choiceResult.ApplicationMajorChoiceResultMajorGrades.First(e => e.ApplicationMajorChoiceUniversityMajorId.Equals(key));
                        choiceResult.ApplicationMajorChoiceResultMajorGrades.Remove(itemToDelete);
                    }
                    else
                    {

                        var itemToDelete = entities.ApplicationMajorChoiceResultMajorGrades.First(e => e.ApplicationMajorChoiceUniversityMajorId.Equals(key));
                        entities.ApplicationMajorChoiceResultMajorGrades.Remove(itemToDelete);
                    }
                  
                }
            }
            //adding new selected cities to CityGrade object
            foreach (var key in distinctMajors)
            {
                //not exist before adding new item with grade 0 else do not change before value and let it to remain
                if (choiceResult.ApplicationMajorChoiceResultMajorGrades.FirstOrDefault(e => e.ApplicationMajorChoiceUniversityMajorId.Equals(key)) == null)
                    choiceResult.ApplicationMajorChoiceResultMajorGrades.Add(new ApplicationMajorChoiceResultMajorGrade()
                    {
                        ApplicationMajorChoiceUniversityMajorId = key,
                        MajorGrade = 0,
                    });
            }
            //end of create universityGrade Object


            if (user != null)
            {
                int effeted = this.AcceptChanges();
                if (effeted > 0)
                {
                    return AddStatus.Added;
                }
                else
                    return AddStatus.RejectedByNonEffectiveQuery;
            }
            else
            {
                System.Web.HttpContext.Current.Session[majorChoiceSessionKey] = choiceResult;
                return AddStatus.Added;
            }
        }
        public AddStatus AddOrUpdateMajorGrade(MembershipUser user, int? cityId, int? cityGrade, int? majorId, int? majorGrade, int? universityId, int? universityGrade)
        {

            var choiceResult = GetMajorChoiceResultForUser(user);
            if (choiceResult == null)
            {
                choiceResult = new ApplicationMajorChoiceResult()
                {
                    ApplicationId = this.GetSelectMajorApplication().ApplicationID,
                    DateTime = DateTime.Now,
                };
                if (user != null)
                    user.ApplicationMajorChoiceResults.Add(choiceResult);
            }

            choiceResult.IsCalculated = false;
            
            if (cityId.HasValue && cityGrade.HasValue)
            {
                var cityGradeObject=choiceResult.ApplicationMajorChoiceResultCityGrades.FirstOrDefault(e => e.ApplicationMajorChoiceCityId.Equals(cityId.Value));
                if (cityGradeObject == null)
                {
                    cityGradeObject = new ApplicationMajorChoiceResultCityGrade() { };
                    choiceResult.ApplicationMajorChoiceResultCityGrades.Add(cityGradeObject);
                }
                cityGradeObject.CityGrade = cityGrade.Value;
            }
            if (majorId.HasValue && majorGrade.HasValue)
            {
                var majorGradeObject = choiceResult.ApplicationMajorChoiceResultMajorGrades.FirstOrDefault(e => e.ApplicationMajorChoiceUniversityMajorId.Equals(majorId.Value));
                if (majorGradeObject == null)
                {
                    majorGradeObject = new ApplicationMajorChoiceResultMajorGrade() { };
                    choiceResult.ApplicationMajorChoiceResultMajorGrades.Add(majorGradeObject);
                }
                majorGradeObject.MajorGrade = majorGrade.Value;
            }
            if (universityId.HasValue && universityGrade.HasValue)
            {
                var universityGradeObject = choiceResult.ApplicationMajorChoiceResultUniversityGrades.FirstOrDefault(e => e.ApplicationMajorChoiceUniversityId.Equals(universityId.Value));
                if (universityGradeObject == null)
                {
                    universityGradeObject = new ApplicationMajorChoiceResultUniversityGrade() { };
                    choiceResult.ApplicationMajorChoiceResultUniversityGrades.Add(universityGradeObject);
                }
                universityGradeObject.UniversityGrade = universityGrade.Value;
            }

            if (user != null)
            {
                int effeted = this.AcceptChanges();
                if (effeted > 0)
                {
                    return AddStatus.Added;
                }
                else
                    return AddStatus.RejectedByNonEffectiveQuery;
            }
            else
            {
                System.Web.HttpContext.Current.Session[majorChoiceSessionKey] = choiceResult;
                return AddStatus.Added;
            }
        }

        public void CalculteMajorChoiceFor(MembershipUser user)
        {
            var choiceResult = GetMajorChoiceResultForUser(user);
            CalculteMajorChoiceFor(choiceResult);
        }

        public void CalculteMajorChoiceFor(ApplicationMajorChoiceResult choiceResult)
        {

            if (choiceResult != null)
            {
                choiceResult.IsCalculated = true;
                foreach (var choiceDetailEntity in choiceResult.ApplicationMajorChoiceResultDetails)
                {
                    var cityGrade=choiceResult.ApplicationMajorChoiceResultCityGrades.First(e=>e.ApplicationMajorChoiceCityId.Equals(choiceDetailEntity.ApplicationMajorChoiceCityId)).CityGrade;
                    var universityGrade = choiceResult.ApplicationMajorChoiceResultUniversityGrades.First(e => e.ApplicationMajorChoiceUniversityId.Equals(choiceDetailEntity.ApplicationMajorChoiceUniversityId)).UniversityGrade;
                    var majorGrade = choiceResult.ApplicationMajorChoiceResultMajorGrades.First(e => e.ApplicationMajorChoiceUniversityMajorId.Equals(choiceDetailEntity.ApplicationMajorChoiceUniversityMajorId)).MajorGrade;

                    choiceDetailEntity.CalculatedGrade = (choiceResult.CityWeight * cityGrade)
                        + (choiceResult.MajorWeight * universityGrade)
                        + (choiceResult.UniversityWeight * majorGrade);
                }
            }

            if (choiceResult.MembershipUser != null) //add this condition to consider only registered user, the session object will not persist only when user is registered
            {
                this.AcceptChanges();
                if (choiceResult.PasswordNumber.HasValue)
                    ApplicationManager.instance.MarkRandomNumberAsUsed(choiceResult.PasswordNumber.Value);
            }
            else
                System.Web.HttpContext.Current.Session[majorChoiceSessionKey] = choiceResult;
        }

        public bool PersistChoiceMajorResultForCurrentUser()
        {
            var user = Membership.MembershipManager.Instance.GetCurrentUser();

            var choiceResultSessionBased = (ApplicationMajorChoiceResult)System.Web.HttpContext.Current.Session[majorChoiceSessionKey];
            if (user != null && choiceResultSessionBased != null && isCalculatedForMajorChoice(choiceResultSessionBased))
            {
                if (choiceResultSessionBased.PasswordNumber.HasValue)
                    ApplicationManager.instance.MarkRandomNumberAsUsed(choiceResultSessionBased.PasswordNumber.Value);

                this.ResetMajorChoiceFor(user);
                user.ApplicationMajorChoiceResults.Add(choiceResultSessionBased);
                int effected = this.AcceptChanges();
                if (effected > 0)
                    return true;

            }
            return false;
        }
        #endregion

    }
}
