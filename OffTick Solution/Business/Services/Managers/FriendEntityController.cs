﻿
using Offtick.Business.Membership;
using Offtick.Core.EnumTypes;
using Offtick.Data.Context.ExpertOnlinerContexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.Services.Managers
{
    public class FriendEntityController : BaseManager
    {
        private static FriendEntityController instance;
        public static FriendEntityController Instance
        {
            get
            {
                if (instance != null)
                    return instance;
                lock (typeof(FriendEntityController))
                {
                    if (instance == null)
                        instance = new FriendEntityController();
                    return instance;
                }
            }
        }


        private FriendEntityController()
        {
        }

        public Friend GetFriendById(Guid friendId)
        {
            return this.GetPersitantStorage().Friends.FirstOrDefault(e => e.FriendId.Equals(friendId));
        }

        public DeleteStatus DeleteFriendByFiendId(string userName, Guid friendId)
        {
            try
            {
                var db = this.GetPersitantStorage();

                MembershipUser user = MembershipManager.Instance.GetUser(userName);
                if (user != null)
                {
                    var friendEntity =
                        user.Friends.FirstOrDefault(e => e.FirendUserId.Equals(friendId));
                    if (friendEntity != null)
                    {
                        db.Friends.Remove(friendEntity);
                        int effected = AcceptChanges();
                        if (effected > 0)
                            return DeleteStatus.Deleted;
                        else
                            return DeleteStatus.RejectedByNonEffectiveQuery;
                    }
                }
                return DeleteStatus.RejectedByNotExit;
            }
            catch
            {
                return DeleteStatus.RejectedByInternalSystemError;
            }
        }

        public EditStatus BlockFriendByFiendId(string userName, Guid friendId)
        {
            try
            {
                var db = this.GetPersitantStorage();

                MembershipUser user = MembershipManager.Instance.GetUser(userName);
                if (user != null)
                {
                    var friendEntity =
                        user.Friends.FirstOrDefault(e => e.FirendUserId.Equals(friendId));
                    if (friendEntity != null)
                    {
                        friendEntity.Blocked = true;
                        int effected = AcceptChanges();
                        if (effected > 0)
                            return EditStatus.Edited;
                        else
                            return EditStatus.RejectedByNonEffectiveQuery;
                    }
                }
                return EditStatus.RejectedByNotExit;
            }
            catch
            {
                return EditStatus.RejectedByInternalSystemError;
            }
        }
        public EditStatus UnBlockFriendByFiendId(string userName, Guid friendId)
        {
            try
            {
                var db = this.GetPersitantStorage();

                MembershipUser user = MembershipManager.Instance.GetUser(userName);
                if (user != null)
                {
                    var friendEntity =
                        user.Friends.FirstOrDefault(e => e.FirendUserId.Equals(friendId));
                    if (friendEntity != null)
                    {
                        friendEntity.Blocked = false;
                        int effected = AcceptChanges();
                        if (effected > 0)
                            return EditStatus.Edited;
                        else
                            return EditStatus.RejectedByNonEffectiveQuery;
                    }
                }
                return EditStatus.RejectedByNotExit;
            }
            catch
            {
                return EditStatus.RejectedByInternalSystemError;
            }
        }

        public bool IsFriendBlocked(string receiverUserName, string senderUserName)
        {
            var db = this.GetPersitantStorage();
            MembershipUser user = MembershipManager.Instance.GetUser(receiverUserName);
            if (user != null)
            {
                var friendEntity = user.Friends.Where(e => e.MembershipUser1.UserName.Equals(senderUserName)).FirstOrDefault();
                if (friendEntity != null && friendEntity.Blocked.HasValue)
                    return friendEntity.Blocked.Value;
            }
            return false;
        }


        public FollowRequested RequestAFallow(MembershipUser follower, MembershipUser user)
        {

            var followRequested = user.FollowRequesteds.FirstOrDefault(e => e.FollowersUserId.Equals(follower.UserId));
            int effected = 0;
            if (followRequested == null)
            {
                followRequested = new FollowRequested()
                {
                    Datetime = DateTime.Now,
                    FollowersUserId = follower.UserId,
                    IsProtected = false,
                    UserId = user.UserId

                };
                user.FollowRequesteds.Add(followRequested);
                effected = this.AcceptChanges();

            }

            if (!user.IsProtected.HasValue || !user.IsProtected.Value)//not protected uesr
            {
                this.AcceptAFollow(followRequested, string.Empty);
                effected = 1;
            }
            return effected > 0 ? followRequested : null;

        }

        public FollowRejected RejectAFollow(FollowRequested followRequested, string description)
        {
            if (followRequested != null)
            {
                var user = followRequested.MembershipUser;
                var entities = this.GetPersitantStorage();
                entities.FollowRequesteds.Remove(followRequested);

                var followRejected = new FollowRejected()
                {
                    Description = description,
                    Datetime = DateTime.Now,
                    FollowerUserId = followRequested.FollowersUserId,

                };
                user.FollowRejecteds.Add(followRejected);
                int effected = this.AcceptChanges();
                if (effected > 0)
                    return followRejected;
                else
                    return null;
            }
            throw new ArgumentNullException("Null FollowRequested boject");
        }

        public Follow AcceptAFollow(FollowRequested followRequested, string description)
        {
            if (followRequested != null)
            {
                var user = followRequested.MembershipUser;
                var entities = this.GetPersitantStorage();
                entities.FollowRequesteds.Remove(followRequested);
                //user.FollowRequesteds.Remove(followRequested);

                //check to do not adding duplicate follow entity
                Follow follow = user.Follows.FirstOrDefault(e => e.FollowersUserId.Equals(followRequested.FollowersUserId));
                if (follow == null)
                {
                    follow = new Follow()
                    {
                        Datetime = DateTime.Now,
                        FollowersUserId = followRequested.FollowersUserId,
                        IsProtected = false,
                    };
                    user.Follows.Add(follow);
                }
                int effected = this.AcceptChanges();
                if (effected > 0)
                    return follow;
                else
                    return null;
            }
            throw new ArgumentNullException("Null FollowRequested boject");
        }


        public bool RemoveAFollow(Follow follow)
        {
            if (follow != null)
            {
                var entities = this.GetPersitantStorage();
                entities.Follows.Remove(follow);
                int effected = this.AcceptChanges();
                if (effected > 0)
                    return true;
                else
                    return false;
            }
            throw new ArgumentNullException("Null Follow boject");
        }

    }
}
