﻿using Offtick.Business.Services.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.Services.Managers
{
    public abstract class BaseManager

    {
        protected int AcceptChanges()
        {
            var db = GetPersitantStorage();
            return db.SaveChanges();
        }

        protected Offtick.Data.Context.ExpertOnlinerContexts.ExpertOnlinerFoundationEntities GetPersitantStorage()
        {
            return DataContextManager.GetPersitantStorage();
        }
    }
}
