﻿using Offtick.Business.Services.Storage;
using Offtick.Core.EnumTypes;
using Offtick.Data.Context.ExpertOnlinerContexts;
using Microsoft.Owin.Security.Provider;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.Services.Managers
{
    public class LangaugeManager : BaseManager
    {
        private static LangaugeManager instance;
        public static LangaugeManager Instance
        {
            get
            {
                if (instance != null)
                    return instance;
                lock (typeof(LangaugeManager))
                {
                    if (instance == null)
                        instance = new LangaugeManager();
                    return instance;
                }
            }
            
        }
        private LangaugeManager()
        {
        }

        public IList<Offtick.Data.Context.ExpertOnlinerContexts.Language> GetLangauges()
        {
            var entities = DataContextManager.GetPersitantStorage();
            return entities.Languages.ToList();
        }

        public IList<Offtick.Data.Context.ExpertOnlinerContexts.MembershipLangauge> GetMemberLangauges(Guid userId)
        {
            var entities = DataContextManager.GetPersitantStorage();
            return entities.MembershipLangauges.Where(e => e.UserId.Equals(userId)).ToList();
        }

        public Offtick.Data.Context.ExpertOnlinerContexts.Language GetLangaugeById(short languageId)
        {
            var entities = DataContextManager.GetPersitantStorage();
            return entities.Languages.FirstOrDefault(e => e.LanguageId.Equals(languageId));
        }

        public short GetDefaultLangaugeId()
        {
            return GetDefaultLangaugeIdOfUser(Membership.MembershipManager.Instance.GetCurrentUserName());
        }

        public short GetDefaultLangaugeIdOfUser(string userName)
        {
            var entities = DataContextManager.GetPersitantStorage();
            return Membership.MembershipManager.Instance.GetUser(userName).MembershipLangauges.FirstOrDefault(e => e.IsDefault).LanguageId;
        }


        public Offtick.Data.Context.ExpertOnlinerContexts.Language GetLangauge(string englishTitle, string localTitle, string calture)
        {
            var entities = DataContextManager.GetPersitantStorage();
            return entities.Languages.FirstOrDefault(e => e.TitleEnglish.ToUpper().Equals(englishTitle.ToUpper()) || e.TitleLocal.ToUpper().Equals(localTitle.ToUpper()) || e.Caluture.ToUpper().Equals(calture.ToUpper()));
        }

        public Offtick.Data.Context.ExpertOnlinerContexts.Language GetLangauge(short? notEqualToThisLanguageId,string englishTitle, string localTitle, string calture)
        {
            var entities = DataContextManager.GetPersitantStorage();
            if(notEqualToThisLanguageId.HasValue)
                return entities.Languages.FirstOrDefault(e =>!e.LanguageId.Equals(notEqualToThisLanguageId.Value) &&  (e.TitleEnglish.ToUpper().Equals(englishTitle.ToUpper()) || e.TitleLocal.ToUpper().Equals(localTitle.ToUpper()) || e.Caluture.ToUpper().Equals(calture.ToUpper())));
            else
            return entities.Languages.FirstOrDefault(e => e.TitleEnglish.ToUpper().Equals(englishTitle.ToUpper()) || e.TitleLocal.ToUpper().Equals(localTitle.ToUpper()) || e.Caluture.ToUpper().Equals(calture.ToUpper()));
        }
        public Offtick.Data.Context.ExpertOnlinerContexts.Language GetLangauge(short languageId)
        {
            var entities = DataContextManager.GetPersitantStorage();
            return entities.Languages.FirstOrDefault(e => e.LanguageId.Equals(languageId));
        }


        public AddStatus AddLangauge(short languageId, string englishTitle, string localTitle, string calture)
        {
            var entities = DataContextManager.GetPersitantStorage();
            var existed = GetLangauge(englishTitle, localTitle, calture);
            if (existed != null)
                return AddStatus.RejectedByExitBefore;
            entities.Languages.Add(new Offtick.Data.Context.ExpertOnlinerContexts.Language()
            {
                LanguageId=languageId,
                Caluture = calture,
                TitleEnglish = englishTitle,
                TitleLocal = localTitle
            });
            int effected = this.AcceptChanges();
            AddStatus addStatus = effected > 0 ? AddStatus.Added : AddStatus.RejectedByNonEffectiveQuery;
            return addStatus;
        }

        public EditStatus UpdateLangauge(short languageId, string englishTitle, string localTitle, string calture)
        {
            var entities = DataContextManager.GetPersitantStorage();
            var existed = entities.Languages.FirstOrDefault(e =>e.LanguageId.Equals(languageId));
            if (existed == null)
                return EditStatus.RejectedByNotExit;

            existed.Caluture = calture;
            existed.TitleEnglish = englishTitle;
            existed.TitleLocal = localTitle;
            
            int effected = this.AcceptChanges();
            EditStatus addStatus = effected > 0 ? EditStatus.Edited : EditStatus.RejectedByNonEffectiveQuery;
            return addStatus;

        }


        public MultiLanguageErrorCode GetMultiLanguageErrorCode(short languageId, string errorCode, string package)
        {
            var entities = DataContextManager.GetPersitantStorage();
            return entities.MultiLanguageErrorCodes.FirstOrDefault(e => e.LanguageId.Equals(languageId) && e.ErrorCode.Equals(errorCode) && e.Package.Equals(package));
        }

        public AddStatus AddMultiLanguageErrorCode( short languageId, string errorCode, string meaning, string package)
        {
            var entities = DataContextManager.GetPersitantStorage();
            var existed = GetMultiLanguageErrorCode(languageId, errorCode, package);
            if (existed != null)
                return AddStatus.RejectedByExitBefore;
            entities.MultiLanguageErrorCodes.Add(new Offtick.Data.Context.ExpertOnlinerContexts.MultiLanguageErrorCode()
            {
                ErrorCode = errorCode,
                LanguageId = languageId,
                Meaning = meaning,
                Package = package,
                MultiLanguageId = Guid.NewGuid()
            });
            int effected = this.AcceptChanges();
            AddStatus addStatus = effected > 0 ? AddStatus.Added : AddStatus.RejectedByNonEffectiveQuery;
            return addStatus;
        }

        public EditStatus UpdateMultiLanguageErrorCode(Guid MultiLanguageId, short LanguageId, string errorCode, string Meaning,string Package)
        {
            var entities = DataContextManager.GetPersitantStorage();
            var existed = entities.MultiLanguageErrorCodes.FirstOrDefault(e => e.MultiLanguageId.Equals(MultiLanguageId));
            if (existed == null)
                return EditStatus.RejectedByNotExit;

            existed.LanguageId = LanguageId;
            existed.Meaning = Meaning;
            existed.Package = Package;
            existed.ErrorCode =errorCode;

            int effected = this.AcceptChanges();
            EditStatus addStatus = effected > 0 ? EditStatus.Edited : EditStatus.RejectedByNonEffectiveQuery;
            return addStatus;
        }

        public IQueryable<MultiLanguageErrorCode> GetMultiLanguageErrorCodes()
        {
            var entities = DataContextManager.GetPersitantStorage();
            return entities.MultiLanguageErrorCodes.AsQueryable();
        }
    }
}
