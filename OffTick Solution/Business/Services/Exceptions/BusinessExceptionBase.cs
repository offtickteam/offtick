﻿using Offtick.Core.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.Services.Exceptions
{
    public class BusinessExceptionBase:ExceptionBase
    {
         
        public BusinessExceptionBase(string msg)
            : base(msg)
        {
            this.UserName = Membership.MembershipManager.Instance.GetCurrentUserName();
        }
    }
}
