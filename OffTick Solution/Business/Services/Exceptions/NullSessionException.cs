﻿using Offtick.Core.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.Services.Exceptions
{
    public class NullSessionException:ExceptionBase
    {
        public NullSessionException() : this("No Session Exsit, please sign in again.") { }
        public NullSessionException(string msg)
            : base(msg)
        {
        }
    }
}
