﻿using Offtick.Core.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.Services.Exceptions
{
    public class ExceptionFactory
    {
        public static ExceptionBase CreateExeption( string msg)
        {
            return CreateExeption(ExceptionLevel.Abuse, msg);
        }
        public static ExceptionBase CreateExeption(ExceptionLevel level, string msg)
        {
            return new BusinessExceptionBase(msg)
            {
                Level = level
            };
        }
    }
}
