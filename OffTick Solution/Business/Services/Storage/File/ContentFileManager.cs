﻿using Offtick.Business.Services.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.Services.Storage.File
{
    public class ContentFileManager:FileManager
    {
       internal string fullDirectoryPath;
       internal string virtualDirectoryPath;



        public ContentFileManager(string userId)
            : base()
        {
            if (string.IsNullOrEmpty(userId))
                throw new NullSessionException();
            string serverPath = System.Web.HttpRuntime.AppDomainAppPath;
            virtualDirectoryPath=@"/Files/"+userId+"/Content/";
            fullDirectoryPath = serverPath + virtualDirectoryPath;
            if (!this.IsExistPath(fullDirectoryPath))
                this.CreatePath(fullDirectoryPath);
        }


        public override string Save(System.Web.HttpPostedFileBase fileBase, string fileName)
        {
            string newFileName = string.Format("{0}{1}{2}",
                System.IO.Path.GetFileNameWithoutExtension(fileName),
                DateTime.Now.ToString("yyyyMMddHHmmss"),
                System.IO.Path.GetExtension(fileName));
            string fullFileName = this.GetFullFileName(newFileName);
            base.Save(fileBase, fullFileName);


            return newFileName;

        }

        public override void Delete(string fileName)
        {
            base.Delete(GetFullFileName(fileName));
        }

        public string GetFullFileName(string fileName)
        {
            return fullDirectoryPath + fileName;
        }

        public string GetAbsoluteFileName(string fileName)
        {
            return virtualDirectoryPath + fileName;
        }
        

    }
}
