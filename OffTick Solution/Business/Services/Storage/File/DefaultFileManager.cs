﻿using Offtick.Business.Services.Exceptions;
using Offtick.Core.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.Services.Storage.File
{
    public class DefaultFileManager : FileManager
    {
        public string relativeDirectoryPath;
        public  string directoryPath;

        public override string Save(System.Web.HttpPostedFileBase fileBase, string fileName, int? maxValue)
        {
            int seed = int.Parse(DateTime.Now.ToString("MMddHHmmss"));
            string newFileName = string.Format("{0}{1}_{2}{3}",
                System.IO.Path.GetFileNameWithoutExtension(fileName),
                DateTime.Now.ToString("yyyyMMddHHmmss"),
                GlobalUtility.ConvertGUIDToInt(Guid.NewGuid()),
                System.IO.Path.GetExtension(fileName));

            string fullFileName = this.GetFullFileName(newFileName);
            base.Save(fileBase, fullFileName, null);

            return newFileName;

        }



        public DefaultFileManager(string userId,string baseDirectoryName)
            : base()
        {
            if (string.IsNullOrEmpty(userId))
                throw new NullSessionException();
            string serverPath = System.Web.HttpRuntime.AppDomainAppPath;
            relativeDirectoryPath =string.Format("/Files/{0}/{1}/",baseDirectoryName, userId) ;
            directoryPath = serverPath + relativeDirectoryPath;
            if (!this.IsExistPath(directoryPath))
                this.CreatePath(directoryPath);
        }

        /// <summary>
        /// get just the file name and return its phisical place on the server
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public string GetFullFileName(string fileName)
        {

            string fileNameWithOutExtension = System.IO.Path.GetFileNameWithoutExtension(fileName);
            return directoryPath + fileNameWithOutExtension + System.IO.Path.GetExtension(fileName);
        }

        public string GetRelativeFullFileName(string userId, string fileName)
        {
            string fileNameWithOutExtension = System.IO.Path.GetFileNameWithoutExtension(fileName);
            return relativeDirectoryPath+ fileNameWithOutExtension + System.IO.Path.GetExtension(fileName);
        }


    }
}

