﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.Services.Storage.File
{
    public class ContentFileFactory
    {
        public static FileManager GetFileManager(string mime,string userId)
        {
            bool isImage = IsImage(mime);
            if (isImage)
                return new ImageContentFileManager(userId);
            else
                return new ContentFileManager(userId);
        }
        private static bool IsImage(string fileMime)
        {
            if (string.IsNullOrEmpty(fileMime))
                return false;
            
            string[] mimes = { "JPEG", "PNG", "GIF" };
            return mimes.Contains(fileMime.ToUpper()) ? true : false;

        }
    }
}
