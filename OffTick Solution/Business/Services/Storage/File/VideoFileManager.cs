﻿using Offtick.Business.Services.Exceptions;
using Offtick.Core.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.Services.Storage.File
{
    public class VideoFileManager:DefaultFileManager
    {
        

        public VideoFileManager(string userId)
            : base(userId,"Videos")
        {
            
        }
      
    }
}

