﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Offtick.Business.Services.Storage.File
{
    public interface IFileManager
    {
         string Save(HttpPostedFileBase fileBase, String fileName,int? maxValue);
        string Save(System.Drawing.Image image, String fileName,int? maxValue);

         string Save(HttpPostedFileBase fileBase, String fileName);
        string Save(System.Drawing.Image image, String fileName);

         bool IsExit(string fileName);
         void Delete(string fileName);
         void Rename(string sourcefileName,string destinationFileName);
         string GetPath(string fileName);

         bool IsExistPath(string directoryPath);
         void CreatePath(string directoryPath);
         byte[] GetBytesOfImage(string imagePath);
         System.Drawing.Image GetImageFromByteArray(byte[] array);
    }
}
