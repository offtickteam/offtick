﻿using Offtick.Business.Services.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.Services.Storage.File
{
    public class PdfFileManager:FileManager
    {
          public static string fullDirectoryPath;
          public static string relativeDirectoryPath;

          public override string Save(System.Web.HttpPostedFileBase fileBase, string fileName)
        {
            string newFileName = string.Format("{0}{1}{2}",
                System.IO.Path.GetFileNameWithoutExtension(fileName),
                DateTime.Now.ToString("yyyyMMddHHmmss"),
                System.IO.Path.GetExtension(fileName));
            string fullFileName = this.GetFullFileName(newFileName);
            base.Save(fileBase, fullFileName);
            

            

            return newFileName;
            
        }
        public override void Delete(string fileName)
        {
            string fullFileName = this.GetFullFileName(fileName);
         
            if (IsExit(fullFileName))
                base.Delete(fullFileName);
           

        }
       
     

        public PdfFileManager(string userId)
            : base()
        {
            if (string.IsNullOrEmpty(userId))
                throw new NullSessionException();
            string serverPath = System.Web.HttpRuntime.AppDomainAppPath;
            relativeDirectoryPath = @"/Files/" + userId + "/Pdf/";
            fullDirectoryPath = serverPath +relativeDirectoryPath;
            if (!this.IsExistPath(fullDirectoryPath))
                this.CreatePath(fullDirectoryPath);
         
        }

        /// <summary>
        /// get just the file name and return its phisical place on the server
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public string GetFullFileName(string fileName)
        {
            return fullDirectoryPath+ fileName;
        }

        public string GetRelativeFullFileName(string userId,string fileName)
        {
            return string.Format("{0}{1}", relativeDirectoryPath, fileName);
        }

        public void InitializeImageFileManager()
        {
            FileManager fileMan = new FileManager();
            fileMan.CreatePath(fullDirectoryPath);
        }
    }
}
