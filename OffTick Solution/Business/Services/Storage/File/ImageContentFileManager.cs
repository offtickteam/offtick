﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.Services.Storage.File
{
    public class ImageContentFileManager:FileManager
    {

        private ContentFileManager contentFileManager;
        public string virtualThumnailDirectoryPath
        {
            get
            {
                return contentFileManager.virtualDirectoryPath + @"Thumnail\";
            }
        }
        public string phisicalThumnailDirectoryPath
        {
            get
            {
                return contentFileManager.fullDirectoryPath + @"Thumnail\";
            }
        }

        public ImageContentFileManager(string userId)
            : base()
        {
            contentFileManager = new ContentFileManager(userId);
        }

        public override string Save(System.Web.HttpPostedFileBase fileBase, string fileName, int? maxValue)
        {
            string newFileName=contentFileManager.Save(fileBase, fileName,maxValue);
            if (!contentFileManager.IsExistPath(phisicalThumnailDirectoryPath))
                contentFileManager.CreatePath(phisicalThumnailDirectoryPath);
            SaveThumbail(contentFileManager.GetFullFileName(newFileName), phisicalThumnailDirectoryPath+newFileName, 128);
            return newFileName;
        }

        public override void Delete(string fileName)
        {
            contentFileManager.Delete(fileName);
            deleteThumbnail(phisicalThumnailDirectoryPath + fileName);
        }

        private void SaveThumbail(string fileName, string thumbnailFileName, int maxValue)
        {
            if (!string.IsNullOrEmpty(fileName) && IsExit(fileName))
            {
                System.Drawing.Image fullImage = System.Drawing.Image.FromFile(fileName);
                int thumbnailWidth = fullImage.Width >= fullImage.Height ? maxValue : maxValue * Math.Min(fullImage.Height, fullImage.Width) / Math.Max(fullImage.Height, fullImage.Width);
                int thumbnailHeight = fullImage.Height >= fullImage.Width ? maxValue : maxValue * Math.Min(fullImage.Height, fullImage.Width) / Math.Max(fullImage.Height, fullImage.Width);
                var thumbnailImage = fullImage.GetThumbnailImage(thumbnailWidth, thumbnailHeight, () => false, IntPtr.Zero);
                thumbnailImage.Save(thumbnailFileName);
                fullImage.Dispose();
                thumbnailImage.Dispose();
            }
        }

        private void deleteThumbnail(string fileName)
        {
            FileManager fManager = new FileManager();
            fManager.Delete(fileName);
        }

        public string GetAbsoluteThumnailFileAddress(string fileName)
        {
            return virtualThumnailDirectoryPath + fileName;
        }

        public string GetFullThumnailFileAddress(string fileName)
        {
            return phisicalThumnailDirectoryPath + fileName;
        }

        public string GetFullFileAddress(string fileName)
        {
            return contentFileManager.GetFullFileName(fileName);
        }

        public string GetAbsoluteFileAddress(string fileName)
        {
            return contentFileManager.GetAbsoluteFileName(fileName);
        }
    }
}
