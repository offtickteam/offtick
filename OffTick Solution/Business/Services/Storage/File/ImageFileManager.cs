﻿using Offtick.Business.Services.Exceptions;
using Offtick.Core.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.Services.Storage.File
{
    public class ImageFileManager:FileManager
    {
        public enum ImageState
        {
            Normal,
            Thumbnail800,
            Thumbnail320,
            Thumbnail256,
            Thumbnail128,
            Thumbnail96,
            Thumbnail64,
            Thumbnail32,
            Thumbnail16
        }

        public static string fullImageDirectoryPath;
        public static string thumbnailImageDirectoryPath;

        public override string Save(System.Web.HttpPostedFileBase fileBase, string fileName, int? maxValue)
        {
            int seed =int.Parse(DateTime.Now.ToString("MMddHHmmss"));
            string newFileName = string.Format("{0}{1}_{2}{3}",
                System.IO.Path.GetFileNameWithoutExtension(fileName),
                DateTime.Now.ToString("yyyyMMddHHmmss"),
                GlobalUtility.ConvertGUIDToInt(Guid.NewGuid()),
                System.IO.Path.GetExtension(fileName));
            
            string fullFileName = this.GetFullFileName(newFileName, false);
            base.Save(fileBase, fullFileName,null);

            string t16 = this.GetFullFileName(newFileName,  ImageState.Thumbnail16);
            base.Save(fileBase, t16, 16);

            string t32 = this.GetFullFileName(newFileName, ImageState.Thumbnail32);
            base.Save(fileBase, t32, 32);

            string t64 = this.GetFullFileName(newFileName, ImageState.Thumbnail64);
            base.Save(fileBase, t64, 64);

            string t96 = this.GetFullFileName(newFileName, ImageState.Thumbnail96);
            base.Save(fileBase, t96, 96);

            string t128 = this.GetFullFileName(newFileName, ImageState.Thumbnail128);
            base.Save(fileBase, t128, 128);

            string t256 = this.GetFullFileName(newFileName, ImageState.Thumbnail256);
            base.Save(fileBase, t256, 256);

            string t320 = this.GetFullFileName(newFileName, ImageState.Thumbnail320);
            base.Save(fileBase, t320, 320);

            string t800 = this.GetFullFileName(newFileName, ImageState.Thumbnail800);
            base.Save(fileBase, t800, 800);

            
            
            //SaveThumbail(fullFileName, thumbnailFileName, 128);


            

            return newFileName;
            
        }
     



        public ImageFileManager(string userId)
            : base()
        {
            if (string.IsNullOrEmpty(userId))
                throw new NullSessionException();
            string serverPath = System.Web.HttpRuntime.AppDomainAppPath;
            fullImageDirectoryPath=serverPath+@"\\Images\\Galleris\\"+userId+"\\full\\";
            if (!this.IsExistPath(fullImageDirectoryPath))
                this.CreatePath(fullImageDirectoryPath);
            thumbnailImageDirectoryPath = serverPath+@"\\Images\\Galleris\\" + userId + "\\thumbnail\\";
            if (!this.IsExistPath(thumbnailImageDirectoryPath))
                this.CreatePath(thumbnailImageDirectoryPath);
        }

        /// <summary>
        /// get just the file name and return its phisical place on the server
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public string GetFullFileName(string fileName, bool isthumbnail)
        {
            ImageState imgState = isthumbnail ? ImageState.Thumbnail128 : ImageState.Normal;
            return GetFullFileName(fileName, imgState);
        }
        public string GetFullFileName(string fileName, ImageState imageState)
        {
            string fileNameWithOutExtension = System.IO.Path.GetFileNameWithoutExtension(fileName);
            
            if (imageState == ImageState.Thumbnail16)
                fileNameWithOutExtension = fileNameWithOutExtension + "16";
            if (imageState == ImageState.Thumbnail32)
                fileNameWithOutExtension = fileNameWithOutExtension + "32";
            if (imageState == ImageState.Thumbnail64)
                fileNameWithOutExtension = fileNameWithOutExtension + "64";
            if (imageState == ImageState.Thumbnail96)
                fileNameWithOutExtension = fileNameWithOutExtension + "96";
            if (imageState == ImageState.Thumbnail128)
                fileNameWithOutExtension = fileNameWithOutExtension + "128";
            if (imageState == ImageState.Thumbnail256)
                fileNameWithOutExtension = fileNameWithOutExtension + "256";
            if (imageState == ImageState.Thumbnail320)
                fileNameWithOutExtension = fileNameWithOutExtension + "320";
            if (imageState == ImageState.Thumbnail800)
                fileNameWithOutExtension = fileNameWithOutExtension + "800";
            

            return (imageState != ImageState.Normal ? thumbnailImageDirectoryPath : fullImageDirectoryPath) + fileNameWithOutExtension + System.IO.Path.GetExtension(fileName);
        }

        public string GetRelativeFullFileName(string userId, string fileName, bool isThumbnail)
        {
            return GetRelativeFullFileName(userId, fileName, !isThumbnail ? ImageState.Normal : ImageState.Thumbnail128);
        }

        public string GetRelativeFullFileName(string userId, string fileName, ImageState imageState)
        {
            string fileNameWithOutExtension = System.IO.Path.GetFileNameWithoutExtension(fileName);

            if (imageState == ImageState.Thumbnail16)
                fileNameWithOutExtension = fileNameWithOutExtension + "16";
            if (imageState == ImageState.Thumbnail32)
                fileNameWithOutExtension = fileNameWithOutExtension + "32";
            if (imageState == ImageState.Thumbnail64)
                fileNameWithOutExtension = fileNameWithOutExtension + "64";
            if (imageState == ImageState.Thumbnail96)
                fileNameWithOutExtension = fileNameWithOutExtension + "96";
            if (imageState == ImageState.Thumbnail128)
                fileNameWithOutExtension = fileNameWithOutExtension + "128";
            if (imageState == ImageState.Thumbnail256)
                fileNameWithOutExtension = fileNameWithOutExtension + "256";
            if (imageState == ImageState.Thumbnail320)
                fileNameWithOutExtension = fileNameWithOutExtension + "320";
            if (imageState == ImageState.Thumbnail800)
                fileNameWithOutExtension = fileNameWithOutExtension + "800";
            


            string selectedWay = imageState== ImageState.Normal ? "\\full\\" : "\\thumbnail\\";
            return string.Format("{0}{1}{2}{3}", @"\\Images\\Galleris\\", userId, selectedWay, fileNameWithOutExtension + System.IO.Path.GetExtension(fileName));
        }

      
    }
}

