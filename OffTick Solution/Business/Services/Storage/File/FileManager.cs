﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.Services.Storage.File
{
    public class FileManager:IFileManager
    {
        private void saveOptimal(System.IO.Stream stream, string fileName,int maxValue)
        {
            System.Drawing.Image fullImage = System.Drawing.Image.FromStream(stream);
            saveOptimal(fullImage, fileName, maxValue);
        }
        private void saveOptimal( System.Drawing.Image fullImage  , string fileName, int maxValue)
        {
            

            int width = fullImage.Width >= fullImage.Height ? maxValue : maxValue * Math.Min(fullImage.Height, fullImage.Width) / Math.Max(fullImage.Height, fullImage.Width);
            int height = fullImage.Height >= fullImage.Width ? maxValue : maxValue * Math.Min(fullImage.Height, fullImage.Width) / Math.Max(fullImage.Height, fullImage.Width);

            string imageFormat = string.Format("width={0};height={1};format=jpg;mode=max",width,height);
            ImageResizer.ImageJob i = new ImageResizer.ImageJob(fullImage, fileName, new ImageResizer.ResizeSettings(imageFormat
            ));
            i.Build();
        }

        public virtual string Save(System.Web.HttpPostedFileBase fileBase, string fileName)
        {
            return Save(fileBase, fileName, null);
        }
        public virtual string Save(System.Web.HttpPostedFileBase fileBase, string fileName, int? maxValue)
        {
            if (IsExit(fileName))
                throw new ExitFileException("File is exist! please choose another file name");
            if (maxValue == null || !maxValue.HasValue)
                fileBase.SaveAs(fileName);
            else
                this.saveOptimal(fileBase.InputStream, fileName, maxValue.Value);
            return fileName;
        }
        
        public virtual string Save(System.Drawing.Image image, String fileName)
        {
            return Save(image, fileName, null);
        }
        public virtual string Save(System.Drawing.Image image, String fileName, int? maxValue)
        {
            if (IsExit(fileName))
                throw new ExitFileException("File is exist! please choose another file name");
            if (maxValue==null || !maxValue.HasValue)
                image.Save(fileName);
            else
                this.saveOptimal(image, fileName, maxValue.Value);
            image.Dispose();
            return fileName;
        }
        public bool IsExit(string fileName)
        {
            return System.IO.File.Exists(fileName);
        }

        public virtual void Delete(string fileName)
        {
            if (!IsExit(fileName))
                throw new ExitFileException("File is not exist! please choose another file name");
            System.IO.File.Delete(fileName);
        }

       public void Rename(string sourcefileName,string destinationFileName)
        {
            if (!IsExit(sourcefileName))
                throw new ExitFileException("File is exist! please choose another file name");

            if (IsExit(destinationFileName))
                throw new ExitFileException("File is exist! please choose another file name");

            System.IO.File.Move(sourcefileName, destinationFileName);
        }

        public string GetPath(string fileName)
        {
            System.IO.FileInfo fInfo = new System.IO.FileInfo(fileName);
            return fInfo.DirectoryName;
        }

        public void CreatePath(string directoryPath)
        {
            if (IsExistPath(directoryPath))
                throw new NotExistFileException("Dicrectory Path is exist!");

            System.IO.Directory.CreateDirectory(directoryPath);
        }

        public bool IsExistPath(string directoryPath)
        {
            return (System.IO.Directory.Exists(directoryPath));
        }
        public byte[] GetBytesOfImage(string imagePath)
        {
            if (IsExit(imagePath))
            {
                //FileStream fS = new FileStream(imagePath, FileMode.Open, FileAccess.Read);
                //fS.Seek(0, SeekOrigin.Begin);
                //byte[] bytes = new byte[fS.Length];
                //fS.Read(bytes, 0, (int)fS.Length);
                //fS.Close();
                //return bytes;

                System.Drawing.Image im = System.Drawing.Image.FromFile(imagePath);

                MemoryStream ms = new MemoryStream();

                im.Save(ms, im.RawFormat);

                byte[] array = ms.ToArray();

                //return Convert.ToBase64String(array);
                return array;
            }
            else return null;
        }
        public System.Drawing.Image GetImageFromByteArray(byte[] array)
        {
            MemoryStream ms = new MemoryStream(array);
            System.Drawing.Image returnImage = System.Drawing.Image.FromStream(ms);
            return returnImage;
        }
    }
}
