﻿using Offtick.Business.Services.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.Services.Storage.File
{
    public class NewsFileManager:FileManager
    {
         public static string fullImageDirectoryPath;
        public static string thumbnailImageDirectoryPath;

        public override string Save(System.Web.HttpPostedFileBase fileBase, string fileName )
        {
            string newFileName = string.Format("{0}{1}{2}",
                System.IO.Path.GetFileNameWithoutExtension(fileName),
                DateTime.Now.ToString("yyyyMMddHHmmss"),
                System.IO.Path.GetExtension(fileName));
            string fullFileName = this.GetFullFileName(newFileName, false);
            string thumbnailFileName = this.GetFullFileName(newFileName, true);
            base.Save(fileBase, fullFileName);
            
            SaveThumbail(fullFileName, thumbnailFileName, 128);

            

            return newFileName;
            
        }
        public override void Delete(string fileName)
        {
            string fullFileName = this.GetFullFileName(fileName, false);
            string thumbnailFileName = this.GetFullFileName(fileName, true);
            if (IsExit(fullFileName))
                base.Delete(fullFileName);
            if (IsExit(thumbnailFileName))
                base.Delete(thumbnailFileName);

        }
       
        public void SaveThumbail(string fileName,string thumbnailFileName, int maxValue)
        {
            if (!string.IsNullOrEmpty(fileName) && IsExit(fileName))
            {
                System.Drawing.Image fullImage = System.Drawing.Image.FromFile(fileName);
                int thumbnailWidth = fullImage.Width >= fullImage.Height ? maxValue : maxValue * Math.Min(fullImage.Height, fullImage.Width) / Math.Max(fullImage.Height, fullImage.Width);
                int thumbnailHeight = fullImage.Height >= fullImage.Width ? maxValue : maxValue * Math.Min(fullImage.Height, fullImage.Width) / Math.Max(fullImage.Height, fullImage.Width);
                var thumbnailImage = fullImage.GetThumbnailImage(thumbnailWidth, thumbnailHeight, () => false, IntPtr.Zero);
                thumbnailImage.Save(thumbnailFileName);
                
            }
        }

        public NewsFileManager(string userId)
            : base()
        {
            if (string.IsNullOrEmpty(userId))
                throw new NullSessionException();
            string serverPath = System.Web.HttpRuntime.AppDomainAppPath;
            fullImageDirectoryPath = serverPath + @"/Files/" + userId + "/News/full/";
            if (!this.IsExistPath(fullImageDirectoryPath))
                this.CreatePath(fullImageDirectoryPath);
            thumbnailImageDirectoryPath = serverPath + @"/Files/" + userId + "/News/thumbnail/";
            if (!this.IsExistPath(thumbnailImageDirectoryPath))
                this.CreatePath(thumbnailImageDirectoryPath);
        }

        /// <summary>
        /// get just the file name and return its phisical place on the server
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public string GetFullFileName(string fileName, bool isthumbnail)
        {
            return (isthumbnail ? thumbnailImageDirectoryPath : fullImageDirectoryPath) + fileName;
        }

        public string GetRelativeFullFileName(string userId,string fileName, bool isThumbnail)
        {
            string selectedWay = !isThumbnail ? "/News/full/" : "/News/thumbnail/";
            return string.Format("{0}{1}{2}{3}", @"/Files/", userId, selectedWay, fileName);
        }

        public void InitializeImageFileManager()
        {
            FileManager fileMan = new FileManager();
            fileMan.CreatePath(fullImageDirectoryPath);
            fileMan.CreatePath(thumbnailImageDirectoryPath);
        }
    }
}
