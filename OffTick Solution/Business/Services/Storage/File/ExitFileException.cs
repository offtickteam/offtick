﻿using Offtick.Business.Services.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.Services.Storage.File
{
    public class ExitFileException:BusinessExceptionBase
    {
        public ExitFileException(string msg)
            : base(msg)
        {
        }

    }
}
