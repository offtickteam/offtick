﻿using Offtick.Core.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Offtick.Business.Services.Storage
{
    public class DataContextManager
    {
        private static readonly string containerKey = "DataContextContainerkey";

        public static DataContextContainer Container
        {
            get
            {
                // object _container = System.Web.HttpContext.Current != null ? System.Web.HttpContext.Current.Items[containerKey] : null;
                // return (_container as DataContextContainer);


                bool useOwinContext = HttpContext.Current.GetOwinContext() != null;
                DataContextContainer _container = null;
                if (useOwinContext)
                    _container = HttpContext.Current.GetOwinContext().Get<DataContextContainer>(containerKey);
                else//if it isn't owin context search for httpweb context if not found return null;
                    _container = System.Web.HttpContext.Current != null ? (DataContextContainer)System.Web.HttpContext.Current.Items[containerKey] : null;
                
                return _container ;
            }
            private set
            {
                /*DataContextContainer _container = System.Web.HttpContext.Current != null ? System.Web.HttpContext.Current.Items[containerKey] as DataContextContainer : null;
                if (_container != null && _container!=value)
                    _container.Dispose();
                System.Web.HttpContext.Current.Items[containerKey] = value;*/
                bool useOwinContext = HttpContext.Current.GetOwinContext() != null;
                DataContextContainer _container = null;
                if (useOwinContext)
                    _container = HttpContext.Current.GetOwinContext().Get<DataContextContainer>(containerKey);
                else _container = System.Web.HttpContext.Current != null ? System.Web.HttpContext.Current.Items[containerKey] as DataContextContainer : null;
                if (_container != null && _container!=value)
                    _container.Dispose();
                if (useOwinContext)
                    HttpContext.Current.GetOwinContext().Set<DataContextContainer>(containerKey, value);
                else
                    System.Web.HttpContext.Current.Items[containerKey] = value;
            }
        }
         
        public static Offtick.Data.Context.ExpertOnlinerContexts.ExpertOnlinerFoundationEntities GetPersitantStorage()
        {
            if (Container != null)
            {
                if (Container.DataContext != null && Container.DataContext.Database.Connection.State!= System.Data.ConnectionState.Open)
                    Container.DataContext.Database.Connection.Open();
                return Container.DataContext;
            }
            else
                throw new NullReferenceException("The DataContextContainer is null, Please try again");
        }

        public static void FreeStorage()
        {
            Container = null;
        }


        public static void SetDataContextContainer(DataContextContainer container)
        {
            DataContextManager.Container = container;
        }
    }
}
