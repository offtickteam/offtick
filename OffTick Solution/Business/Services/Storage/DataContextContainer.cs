﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Offtick.Data.Context.ExpertOnlinerContexts;

namespace Offtick.Business.Services.Storage
{
    public class DataContextContainer:IDataContextContainer
    {
        public DataContextContainer()
        {
            this.DataContext = new ExpertOnlinerFoundationEntities();
        }


        internal ExpertOnlinerFoundationEntities DataContext { get; private set; }
        private bool isDisposed = false;
        public Action Disposing;

        public void Dispose()
        {
            if (DataContext != null && !isDisposed)
            {
               if(DataContext.Database.Connection.State== System.Data.ConnectionState.Open)
                   DataContext.Database.Connection.Close();
                DataContext.Dispose();
                isDisposed = true;
                if (Disposing != null)
                    Disposing();
            }
            
        }
    }
}
