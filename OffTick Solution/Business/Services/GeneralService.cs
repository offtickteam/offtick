﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.Services
{
    public class GeneralService

    {
        public static string GetThumbnailOf(Guid userId, string fileName)
        {
            string url = string.Empty;
            if (string.IsNullOrEmpty(fileName))
            {
                url = "/Content/images/user-avatar.png";
            }
            else
            {
                var fileManager = new Offtick.Business.Services.Storage.File.ImageFileManager(userId.ToString());
                url = fileManager.GetRelativeFullFileName(userId.ToString(), fileName, true);
            }
            return url;
        }
    }
}
