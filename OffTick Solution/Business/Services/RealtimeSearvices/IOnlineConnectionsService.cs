﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.Services.RealtimeSearvices
{
    public interface IOnlineConnectionsService
    {
        bool isOnlineByUserName(string userName);
        bool isOnlineByConnectionId(string connectionId);
        string getConnectionIdOfUser(string userName);
        string getUsernameOfConnection(string connectionId);
        
        void Disconnect(string userName,string connectionId);
        void Connect(string userName, string connectionId,string deviceToken);
        void Reconnection(string username, string connectionId);
         ConnectionData getDataOfUser(string userName);
         void ChangeRunningState(string userName, ClientRunningState runningState);
         void UpdateLastActivityByUserName(string userName);

        
    }
}
