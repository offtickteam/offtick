﻿using Offtick.Business.Services.RealtimeSearvices.EventDrivenDispatcher.Notification;
using Offtick.Data.Context.ExpertOnlinerContexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.Services.RealtimeSearvices.EventDrivenDispatcher.EventRaisedReactions
{
    public class EventRaisedReactionStartTyping : EventRaisedReaction
    {

        private BidirectionalInteractionNotification notification;
        public EventRaisedReactionStartTyping(BidirectionalInteractionNotification notification)
        {
            this.notification = notification;
        }
        public override object DoReaction(PermanentEvent permanentEvent)
        {
            if (notification != null)
            {
                string receiverClientId = conService.getConnectionIdOfUser(notification.SecondUserName);
                if (!string.IsNullOrEmpty(receiverClientId))//means user is online
                {
                    ChatService.GetCurrentChatService().Clients.Client(receiverClientId).cRpcStartTypingAck(notification);
                }
            }
            return null;
        }
    }
}
