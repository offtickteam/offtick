﻿using Offtick.Business.Services.RealtimeSearvices.EventDrivenDispatcher.Notification;
using Offtick.Data.Context.ExpertOnlinerContexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.Services.RealtimeSearvices.EventDrivenDispatcher.EventRaisedReactions
{
    public class EventRaisedReactionDeleteMessageBeforeReadAck : EventRaisedReaction
    {
        public override object DoReaction(PermanentEvent permanentEvent)
        {
            if (permanentEvent != null && permanentEvent.EntityId.HasValue)
            {


                var clientServerResponse = ClientServerNotificationFactory.CreateClientServerNotification(permanentEvent);
                if (clientServerResponse != null && clientServerResponse.NotificationObject != null)
                {

                    MessageAckNotification notification = (MessageAckNotification)clientServerResponse.NotificationObject;

                    var toClient = conService.getConnectionIdOfUser(notification.UserName);
                    if (!string.IsNullOrEmpty(toClient))//means user is online
                    {

                        ChatService.GetCurrentChatService().Clients.Client(toClient).cRpcDeleteMessageBeforeReadAckOnSender(notification);
                    }
                }
            }
            return null;

        }
    }
}
