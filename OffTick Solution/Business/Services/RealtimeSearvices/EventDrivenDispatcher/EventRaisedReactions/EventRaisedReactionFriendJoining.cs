﻿using Offtick.Business.Services.BusinessObjectController;
using Offtick.Business.Services.RealtimeSearvices.EventDrivenDispatcher.Notification;
using Offtick.Business.Services.RealtimeSearvices.PushNotificationService;
using Offtick.Data.Context.ExpertOnlinerContexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.Services.RealtimeSearvices.EventDrivenDispatcher.EventRaisedReactions
{
    public class EventRaisedReactionFriendJoining:EventRaisedReaction
    {
        public override object DoReaction(PermanentEvent permanentEvent)
        {
            if (permanentEvent != null && permanentEvent.EntityId.HasValue)
            {

                string eventId = string.Empty;
                string eventTimeStamp = string.Empty;

               
              
               

                var toClient = conService.getConnectionIdOfUser(permanentEvent.MembershipUser.UserName);
                var stateData = conService.getDataOfUser(permanentEvent.MembershipUser.UserName);
                var friendEnity=FriendEntityController.Instance.GetFriendById(permanentEvent.EntityId.Value);
                var phoneNumberEntity=friendEnity.MembershipUser1.MembershipPhoneNumbers.FirstOrDefault();
                if (phoneNumberEntity == null)
                {
                    //exception area !!
                }
                string phoneNumber = phoneNumberEntity!=null? phoneNumberEntity.PhoneNumber:"";
                  
                if (stateData != null)//means user is online
                {
                    if (stateData.RunningState == ClientRunningState.Foreground)
                    {
                        var clientServerNotification= ClientServerNotificationFactory.CreateClientServerNotification(permanentEvent);
                        if (clientServerNotification != null && clientServerNotification.NotificationObject != null)
                        {
                            FriendJoiningNotification notification = (FriendJoiningNotification)clientServerNotification.NotificationObject;
                            ChatService.GetCurrentChatService().Clients.Client(toClient).cRpcFriendJoining(notification);
                        }
                    }
                    else
                    {
                        GeneralPushService pushService = new GeneralPushService();
                        pushService.PushNotification(stateData.DeviceToken, "new contact");
                    }
                }
            }
            return null;

        }
    }
}
