﻿using Offtick.Business.Membership;
using Offtick.Business.Services.RealtimeSearvices.EventDrivenDispatcher.Notification;
using Offtick.Core.Utility.PersianTools;
using Offtick.Data.Context.ExpertOnlinerContexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace Offtick.Business.Services.RealtimeSearvices.EventDrivenDispatcher.EventRaisedReactions
{
    public class EventRaisedReactionSendMessage : EventRaisedReaction
    {
        private SendMessageNotification realTimeMessage;
        public EventRaisedReactionSendMessage(SendMessageNotification realTimeMessage)
        {
            this.realTimeMessage = realTimeMessage;
        }
        public override object DoReaction(PermanentEvent permanentEvent)
        {
            if (permanentEvent != null && permanentEvent.EntityId.HasValue)
            {

                string toClient = string.Empty;
                SendMessageNotification notification = null;
                ConnectionData stateData = null;
                if (realTimeMessage != null)
                {
                    toClient = conService.getConnectionIdOfUser(realTimeMessage.ToUserName);
                    stateData = conService.getDataOfUser(realTimeMessage.ToUserName);
                    notification = new SendMessageNotification(realTimeMessage.EventId, realTimeMessage.EventTimestamp)
                    {
                        Datetime = realTimeMessage.Datetime,
                        DatetimeFa= realTimeMessage.DatetimeFa,
                        FromConnectionId = conService.getConnectionIdOfUser(realTimeMessage.FromUserName),
                        ToConnectionId = conService.getConnectionIdOfUser(realTimeMessage.ToUserName),
                        FromUserName = realTimeMessage.FromUserName,
                        ToUserName = realTimeMessage.ToUserName,
                        Message = realTimeMessage.Message,
                        MessageId = realTimeMessage.MessageId,
                        MessageType = realTimeMessage.MessageType,
                        Latitude = realTimeMessage.Latitude,
                        Longitude = realTimeMessage.Longitude,
                        Flags=realTimeMessage.Flags,
                        Timestamp=realTimeMessage.Timestamp
                    };

                }
                else
                {
                    var clientServerNotification=ClientServerNotificationFactory.CreateClientServerNotification(permanentEvent);
                    notification = (SendMessageNotification)clientServerNotification.NotificationObject;
                    toClient = conService.getConnectionIdOfUser(notification.ToUserName);
                    stateData = conService.getDataOfUser(notification.ToUserName);

                }


                
                
                if (stateData != null && stateData.RunningState == ClientRunningState.Foreground)//means user is online
                {
                    ChatService.GetCurrentChatService().Clients.Client(toClient).cRpcRecieveMessage(notification);
                }
                else
                {
                    //GeneralPushService pushService = new GeneralPushService();
                    //string deviceToken = stateData != null ? stateData.DeviceToken : MembershipManager.Instance.GetUserPhoneToken(notification.ToUserName);
                    //string message = string.Empty;
                    //if(notification.MessageType=="Image")
                    //    message=string.Format("{0} send you an image",notification.FromUserName);
                    //else
                    //    if(notification.isSafeModificationFlagOn())
                    //        message=string.Format("{0} send you a text message",notification.FromUserName);
                    //    else
                    //        message=notification.Message;
                    //pushService.PushNotification(deviceToken, message);

                }

            }
            return null;

        }
    }
}

