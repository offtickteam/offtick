﻿
using Offtick.Business.Services.Managers;
using Offtick.Business.Services.RealtimeSearvices.EventDrivenDispatcher.Notification;
using Offtick.Data.Context.ExpertOnlinerContexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.Services.RealtimeSearvices.EventDrivenDispatcher.EventRaisedReactions
{
    public class EventRaisedReactionReadMessage : EventRaisedReaction
    {
        public override object DoReaction(PermanentEvent permanentEvent)
        {
            if (permanentEvent != null && permanentEvent.EntityId.HasValue)
            {
                string eventId = string.Empty;
                string eventTimeStamp = string.Empty;
                eventId = permanentEvent.PermanentEventId.ToString();
                eventTimeStamp = permanentEvent.RaisedTimestamp.ToString("yyyyMMddHHmmss"); 
           
                    var messageEntity = MessageEntityController.Instance.GetMessage(permanentEvent.EntityId.Value);
                   var fromUserName = messageEntity.MembershipUser.UserName;
                   //var toUserName = messageEntity.MembershipUser1.UserName;
                   var dateTime = messageEntity.DateTime.ToString("yyyyMMddHHmmss");
                   //var message = messageEntity.Body;
                   var messageId = messageEntity.MessageId.ToString();
             

                var toClient = conService.getConnectionIdOfUser(fromUserName);
                if (!string.IsNullOrEmpty(toClient))//means user is online
                {
                    MessageAckNotification ackNotification = new MessageAckNotification(eventId, eventTimeStamp)
                    {
                        
                        MessageId = messageId,
                        UserName=fromUserName,
                    };
                    ChatService.GetCurrentChatService().Clients.Client(toClient).cRpcReadMessageAck(ackNotification);
                }
            }
            return null;

        }
    }
}
