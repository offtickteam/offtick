﻿using Offtick.Business.Services.RealtimeSearvices.EventDrivenDispatcher.Notification;

using Offtick.Data.Context.ExpertOnlinerContexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Offtick.Business.Membership;



namespace Offtick.Business.Services.RealtimeSearvices.EventDrivenDispatcher.EventRaisedReactions
{
    public class EventRaisedReactionSendOffer : EventRaisedReaction
    {
        private OfferNotification offerEntityNotificationExisted;
        public EventRaisedReactionSendOffer(OfferNotification realTimeMessage)
        {
            this.offerEntityNotificationExisted = realTimeMessage;
        }
        public override object DoReaction(PermanentEvent permanentEvent)
        {
            if (permanentEvent != null && permanentEvent.EntityId.HasValue)
            {

                string toClient = string.Empty;
                OfferNotification notification = null;
                ConnectionData stateData = null;
                if (offerEntityNotificationExisted != null)
                {
                    toClient = conService.getConnectionIdOfUser(offerEntityNotificationExisted.ToUserName);
                    stateData = conService.getDataOfUser(offerEntityNotificationExisted.ToUserName);
                    notification = offerEntityNotificationExisted;

                }
                else
                {
                    var clientServerNotification=ClientServerNotificationFactory.CreateClientServerNotification(permanentEvent);
                    notification = (OfferNotification)clientServerNotification.NotificationObject;
                    toClient = conService.getConnectionIdOfUser(notification.ToUserName);
                    stateData = conService.getDataOfUser(notification.ToUserName);

                }


                
                
                if (stateData != null && stateData.RunningState == ClientRunningState.Foreground)//means user is online
                {
                    ChatService.GetCurrentChatService().Clients.Client(toClient).cRpcRecieveOffer(notification);
                }
                else
                {
                    GeneralPushService pushService = new GeneralPushService();
                    string deviceToken = stateData != null ? stateData.DeviceToken : MembershipManager.Instance.GetUserPhoneToken(notification.ToUserName);
                    string message = string.Format("{0} send you an offer",notification.FromUserName);
                  
                    pushService.PushNotification(deviceToken, message);

                }

            }
            return null;

        }
    }
}

