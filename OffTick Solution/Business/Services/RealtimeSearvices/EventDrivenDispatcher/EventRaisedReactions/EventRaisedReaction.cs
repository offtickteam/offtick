﻿using Offtick.Data.Context.ExpertOnlinerContexts;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.Services.RealtimeSearvices.EventDrivenDispatcher.EventRaisedReactions
{
    public abstract class EventRaisedReaction
    {
        public abstract object DoReaction(PermanentEvent permanentEvent);
        public static IOnlineConnectionsService conService = OnlineConnectionsService.Instance;
    }
}
