﻿using Offtick.Business.Services.RealtimeSearvices.EventDrivenDispatcher.Notification;
using Offtick.Data.Context.ExpertOnlinerContexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.Services.RealtimeSearvices.EventDrivenDispatcher.EventRaisedReactions
{

    public class EventRaisedReactionCancelTyping : EventRaisedReaction
    {

        private BidirectionalInteractionNotification notification;
        public EventRaisedReactionCancelTyping(BidirectionalInteractionNotification notification)
        {
            this.notification = notification;
        }
        public override object DoReaction(PermanentEvent permanentEvent)
        {
            if (notification != null)
            {
                string receiverClientId = conService.getConnectionIdOfUser(notification.SecondUserName);
                if (!string.IsNullOrEmpty(receiverClientId))//means user is online
                {
                    ChatService.GetCurrentChatService().Clients.Client(receiverClientId).cRpcCancelTypingAck(notification);
                }
            }
            return null;
        }
    }
}
