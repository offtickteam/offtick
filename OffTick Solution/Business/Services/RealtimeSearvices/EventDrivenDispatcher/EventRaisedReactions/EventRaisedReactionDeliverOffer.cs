﻿using Offtick.Business.Services.BusinessObjectController;
using Offtick.Data.Context.ExpertOnlinerContexts;
using Offtick.Business.Services.RealtimeSearvices.EventDrivenDispatcher.Notification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.Services.RealtimeSearvices.EventDrivenDispatcher.EventRaisedReactions
{
    public class EventRaisedReactionDeliverOffer:EventRaisedReaction
    {

        private MessageAckNotification notification;
        public EventRaisedReactionDeliverOffer(MessageAckNotification notification)
        {
            this.notification = notification;
        }
        public override object DoReaction(PermanentEvent permanentEvent)
        {
            string eventId = string.Empty;
            string eventTimeStamp = string.Empty;

            string senderUserName = string.Empty;
            Guid messageId;
            if (notification == null)
            {
                var messageEntity = OfferEntityController.Instance.GetOffer(permanentEvent.EntityId.Value);
                eventId = permanentEvent.PermanentEventId.ToString();
                eventTimeStamp = permanentEvent.RaisedTimestamp.ToString("yyyyMMddHHmmss"); 


                senderUserName = messageEntity.MembershipUser.UserName;
                messageId = messageEntity.OfferId;
            }
            else
            {
                eventId = this.notification.EventId;
                eventTimeStamp = this.notification.EventTimestamp;

                senderUserName = this.notification.UserName;
                messageId = Guid.Parse(this.notification.MessageId);
            }

            string senderClientId = conService.getConnectionIdOfUser(senderUserName);
            if (!string.IsNullOrEmpty(senderClientId))//means user is online
            {
                MessageAckNotification ackNotification = new MessageAckNotification(eventId, eventTimeStamp);
                ackNotification.MessageId = messageId.ToString();
                ackNotification.UserName = senderUserName;

                ChatService.GetCurrentChatService().Clients.Client(senderClientId).cRpcDeliverOfferMessageAck(ackNotification);
            }
            

            return null;
        }
    }
}
