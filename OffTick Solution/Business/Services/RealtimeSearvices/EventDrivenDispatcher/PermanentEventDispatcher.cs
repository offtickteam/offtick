﻿using Offtick.Business.Membership;
using Offtick.Business.Services.Managers;
using Offtick.Data.Context.ExpertOnlinerContexts;
using Offtick.Business.Services.RealtimeSearvices.EventDrivenDispatcher;
using Offtick.Business.Services.RealtimeSearvices.EventDrivenDispatcher.EventRaisedReactions;
using Offtick.Business.Services.RealtimeSearvices.EventDrivenDispatcher.Notification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.Services.RealtimeSearvices.EventDrivenService
{
    public delegate void PermanentEventDelegate(PermanentEvent permanentEvent, object shadowOfPermanentEvent);
    public class PermanentEventDispatcher : BaseManager
    {


        private event PermanentEventDelegate EventRaised;
        private event PermanentEventDelegate EventDelivered;

        private PermanentEventDispatcher()
        {
            EventRaised = new PermanentEventDelegate((permanentEvent, shadowOfPermanentEvent) =>
            {
                var currentChatService = ChatService.GetCurrentChatService();
                EventRaisedReaction ev = null;
                if (currentChatService != null)
                {
                    if(permanentEvent!=null)
                    {
                        switch ((PermanentEventEntityTypeEnum)permanentEvent.EntityTypeId)
                        {
                            case PermanentEventEntityTypeEnum.Message:
                                {
                                    switch ((PermanentEventActionTypeEnum)permanentEvent.ActionTypeId)
                                    {
                                        case PermanentEventActionTypeEnum.Insert:
                                            ev = new EventRaisedReactionSendMessage((SendMessageNotification)shadowOfPermanentEvent);
                                            break;
                                        case PermanentEventActionTypeEnum.Update:
                                            break;
                                        case PermanentEventActionTypeEnum.Delete:
                                            ev = new EventRaisedReactionDeleteMessage();
                                            break;
                                        case PermanentEventActionTypeEnum.Delivered:
                                            ev = new EventRaisedReactionDeliverMessage((MessageAckNotification)shadowOfPermanentEvent);
                                            break;
                                        case PermanentEventActionTypeEnum.Readed:
                                            ev = new EventRaisedReactionReadMessage();
                                            break;
                                        case PermanentEventActionTypeEnum.Typing:
                                            ev = new EventRaisedReactionStartTyping((BidirectionalInteractionNotification)shadowOfPermanentEvent);
                                            break;
                                        case PermanentEventActionTypeEnum.CancelTyping:
                                            ev = new EventRaisedReactionCancelTyping((BidirectionalInteractionNotification)shadowOfPermanentEvent);
                                            break;
                                        case PermanentEventActionTypeEnum.DeleteBeforeRead:
                                            ev = new EventRaisedReactionDeleteMessageBeforeRead();
                                            break;
                                        case PermanentEventActionTypeEnum.DeleteBeforeReadAck:
                                            ev = new EventRaisedReactionDeleteMessageBeforeReadAck();
                                            break;
                                        case PermanentEventActionTypeEnum.None:
                                            break;
                                    }
                                    break;
                                }
                            //case PermanentEventEntityTypeEnum.Friend:
                            //    {
                            //        switch ((PermanentEventActionTypeEnum)permanentEvent.ActionTypeId)
                            //        {
                            //            case PermanentEventActionTypeEnum.Insert:
                            //                ev = new EventRaisedReactionFriendJoining();
                            //                break;
                            //            case PermanentEventActionTypeEnum.Update:
                            //                ev = new EventRaisedReactionFriendJoining();
                            //                break;

                            //        }
                            //    }
                            //    break;
                            //case PermanentEventEntityTypeEnum.Offer:
                            //    {
                            //        switch ((PermanentEventActionTypeEnum)permanentEvent.ActionTypeId)
                            //        {
                            //            case PermanentEventActionTypeEnum.Insert:
                            //                ev = new EventRaisedReactionSendOffer((OfferNotification)shadowOfPermanentEvent);
                            //                break;
                            //            case PermanentEventActionTypeEnum.Update:
                            //                break;
                            //            case PermanentEventActionTypeEnum.Delete:
                            //                break;
                            //            case PermanentEventActionTypeEnum.Delivered:
                            //                ev = new EventRaisedReactionDeliverOffer((MessageAckNotification)shadowOfPermanentEvent);
                            //                break;
                            //            case PermanentEventActionTypeEnum.Readed:
                            //                ev = new EventRaisedReactionReadOffer();
                            //                break;
                            //            case PermanentEventActionTypeEnum.DeleteBeforeRead:
                            //                ev = new EventRaisedReactionDeleteOfferBeforeRead();
                            //                break;
                            //            case PermanentEventActionTypeEnum.DeleteBeforeReadAck:
                            //                ev = new EventRaisedReactionDeleteOfferBeforeReadAck();
                            //                break;
                            //            case PermanentEventActionTypeEnum.None:
                            //                break;
                            //        }
                            //        break;
                            //    }
                            case PermanentEventEntityTypeEnum.None:
                                break;

                        }
                    }
                }
                if (ev != null)
                    ev.DoReaction(permanentEvent);

            });
            EventDelivered = new PermanentEventDelegate((permanentEvent, shadowOfPermanentEvent) =>
            {
                var currentChatService=ChatService.GetCurrentChatService();
                if (currentChatService != null)
                {
                   //raising event
                }
            });
           

        }


        private static PermanentEventDispatcher instance;
        public static PermanentEventDispatcher Instance
        {
            get
            {
                if (instance != null)
                    return instance;
                lock (typeof(PermanentEventDispatcher))
                {
                    if (instance == null)
                        instance = new PermanentEventDispatcher();
                    return instance;
                }
            }
        }


        public IList<PermanentEvent> getEventsOfUser(Guid userId)
        {
            IList<PermanentEvent> result = null;
            MembershipUser user = MembershipManager.Instance.GetUser(userId);
            if (user != null)
            {
                result= user.PermanentEvents.Where(e => e.IsNotifiedToClient.Equals(false)).ToList();
            }
            return result;
        }

        public void NotifyEventsOfUserDelivered(Guid userId,IList<Guid> eventIds){
            if (eventIds != null && eventIds.Count > 0)
                foreach (var eventId in eventIds)
                    NotifyEventsOfUserDelivered(userId, eventId);
        }



         public void NotifyEventsOfUserDelivered(Guid userId,Guid eventId){
             MembershipUser user = MembershipManager.Instance.GetUser(userId);
             if (user != null)
             {
                var eventDelivered= user.PermanentEvents.FirstOrDefault(e => e.PermanentEventId.Equals(eventId));
                eventDelivered.IsNotifiedToClient = true;
                 
                eventDelivered.ProcessTimestamp = DateTime.Now;
               int efftected= this.AcceptChanges();

                 if (EventDelivered != null)
                     EventDelivered(null,null);
             }
        }

        /// <summary>
        /// if userId has no value then permanent event hasn't create and only a shadow of event was use
        /// </summary>
        /// <param name="actionType"></param>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="userId"></param>
        /// <param name="detailLevel"></param>
        /// <param name="moreData"></param>
        /// <param name="shadowOfEvent"></param>
        /// <returns></returns>
         public PermanentEvent CreateEvent(PermanentEventActionTypeEnum actionType,PermanentEventEntityTypeEnum entityType,Guid? entityId,Guid? userId,Int16 detailLevel,string moreData,object shadowOfEvent)
         {
             PermanentEvent permanentEvent = new PermanentEvent()
             {
                 ActionTypeId = (short)actionType,
                 EntityTypeId = (short)entityType,
                 RaisedTimestamp = DateTime.Now,
                 DetailLevel = detailLevel,
                 IsNotifiedToClient = false,
                 PermanentEventId = Guid.NewGuid(),
                 MoreData = moreData

             };
             if (userId.HasValue)
             {
                 MembershipUser user = MembershipManager.Instance.GetUser(userId.Value);
                 permanentEvent.UserId = userId.Value;
                 permanentEvent.EntityId = entityId;
                 if (user != null)
                 {
                     
                     user.PermanentEvents.Add(permanentEvent);
                     int effected = AcceptChanges();

                     if (effected > 0)
                     {
                         if (shadowOfEvent is NotificationBase)
                         {
                             NotificationBase shadow = (NotificationBase)shadowOfEvent;
                             shadow.EventId = permanentEvent.PermanentEventId.ToString();
                             shadow.EventTimestamp = permanentEvent.RaisedTimestamp.ToString("yyyyMMddHHmmss");
                         }
                         onEventRaised(permanentEvent, shadowOfEvent);
                     }
                 }
             }
             else// on the fly event that does not need to save in database
             {
                 onEventRaised(permanentEvent, shadowOfEvent);
             }
             return permanentEvent;
         }

         public int MarkEventAsProcessedByClient(string userName, List<string> eventIds)
         {
             if (!string.IsNullOrEmpty(userName) && eventIds != null && eventIds.Count > 0)
             {
                 DateTime now = DateTime.Now; 
                 foreach (var eventId in eventIds)
                 {
                     Guid guidEventId;
                     if (Guid.TryParse(eventId, out guidEventId))
                     {
                         var ev = GetEventById(guidEventId);
                         if (ev != null && ev.MembershipUser.UserName.Equals(userName))
                         {
                             ev.IsNotifiedToClient = true;
                             ev.ProcessTimestamp = now;
                             
                         }
                     }
                 }
                return AcceptChanges();
             }
             return -1;
         }


         public void onEventRaised(PermanentEvent permanentEvent, object shadowOfEvent)
         {
             if (EventRaised != null)
                 EventRaised(permanentEvent, shadowOfEvent);
         }
         public PermanentEvent CreateEventSendMessage(Guid entityId,Guid userId, object shadowOfPermanentEvent)
         {
             return CreateEvent(PermanentEventActionTypeEnum.Insert, PermanentEventEntityTypeEnum.Message, entityId, userId, -1, string.Empty, shadowOfPermanentEvent);
         }
         public PermanentEvent CreateEventSendOffer(Guid entityId, Guid userId, object shadowOfPermanentEvent)
         {
             return CreateEvent(PermanentEventActionTypeEnum.Insert, PermanentEventEntityTypeEnum.Offer, entityId, userId, -1, string.Empty, shadowOfPermanentEvent);
         }

         public PermanentEvent GetEventById(Guid eventId)
         {
              var db = GetPersitantStorage();
             return db.PermanentEvents.FirstOrDefault(e => e.PermanentEventId.Equals(eventId));
         }

         public PermanentEvent GetEventByEntityId(Guid entityId)
         {
             var db = GetPersitantStorage();
             return db.PermanentEvents.FirstOrDefault(e => e.EntityId.Value.Equals(entityId));
         }


    }
}
