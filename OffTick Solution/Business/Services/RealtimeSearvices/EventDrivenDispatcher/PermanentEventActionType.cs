﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.Services.RealtimeSearvices.EventDrivenService
{
    public enum PermanentEventActionTypeEnum:short
    {
        None = 0,
        Insert = 1,
        Update = 2,
        Delete = 3,
        DeleteBeforeRead = 4,
        DeleteBeforeReadAck = 5,

        Delivered=10,
        DeliveredAckC2S=11,
        DeliveredAckS2C=12,

        Readed=20,
        ReadedAckC2S=21,
        ReadedAckS2C=22,

        //enum with valuge grather than 70, do not saved in database, only aperates on the fly
        Typing=71,
        CancelTyping = 72,
        
    }
}
