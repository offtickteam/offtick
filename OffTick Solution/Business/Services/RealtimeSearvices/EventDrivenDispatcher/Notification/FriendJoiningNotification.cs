﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.Services.RealtimeSearvices.EventDrivenDispatcher.Notification
{
    public class FriendJoiningNotification : NotificationBase
    {

        public FriendJoiningNotification(string eventId, string eventTimestamp, string managedObjectIdString)
            : base(eventId, eventTimestamp, managedObjectIdString)
        {
        }

        public FriendJoiningNotification(string eventId, string eventTimestamp)
            : this(eventId, eventTimestamp, string.Empty)
        {
        }

        public string CellPhoneNumber { get; set; }
        public string ContactEntityId { get; set; }
        public string ContactUserName { get; set; }
        public string FirstName { get; set; }
        public string ImageFull { get; set; }
        public string ImageThumbnail { get; set; }
        public bool IsAnonymous { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
    }
}
