﻿using Offtick.Business.Membership;
using Offtick.Business.Services.Managers;
using Offtick.Business.Services.RealtimeSearvices.EventDrivenService;
using Offtick.Business.Services.Storage.File;
using Offtick.Core.Utility;
using Offtick.Core.Utility.PersianTools;
using Offtick.Data.Context.ExpertOnlinerContexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.Services.RealtimeSearvices.EventDrivenDispatcher.Notification
{
    public class ClientServerNotificationFactory
    {
        public static ClientServerNotification CreateClientServerNotification(PermanentEvent pEvent)
        {
            ClientServerNotification resultObj = null;
            if (pEvent != null)
            {
                switch ((PermanentEventEntityTypeEnum)pEvent.EntityTypeId)
                {
                    case PermanentEventEntityTypeEnum.Message:
                        {
                            switch ((PermanentEventActionTypeEnum)pEvent.ActionTypeId)
                            {
                                case PermanentEventActionTypeEnum.Insert:
                                   
                                    var messageObj = MessageEntityController.Instance.GetMessage( pEvent.EntityId.Value);
                                    if (messageObj != null)
                                    {
                                        SendMessageNotification notification = new SendMessageNotification(pEvent.PermanentEventId.ToString(),  GlobalUtility.GetStringValueOfDate(pEvent.RaisedTimestamp));
                                        notification.Datetime = GlobalUtility.GetStringValueOfDate( messageObj.DateTime);
                                        notification.DatetimeFa = PersianDateConvertor.ToKhorshidiDateTime(messageObj.DateTime,false);
                                        notification.FromUserName = messageObj.MembershipUser.UserName;
                                       
                                        notification.MessageId = messageObj.MessageId.ToString();
                                        notification.ToUserName = messageObj.MembershipUser1.UserName.ToString();
                                        notification.EventId = pEvent.PermanentEventId.ToString();
                                        var location = messageObj.Location;
                                        
                                        notification.Latitude = location!=null && location.Latitude.HasValue? location.Latitude.ToString():string.Empty;
                                        notification.Longitude = location != null && location.Longitude.HasValue ? location.Longitude.ToString() : string.Empty;
                                        notification.Message = messageObj.Body;
                                        if (messageObj.MessageType == 0)
                                        {
                                            notification.MessageType = "Text";
                                        }
                                        else
                                        {

                                            ImageFileManager imgFileManager = new ImageFileManager(messageObj.SenderUserId.ToString());
                                            var bytesOfImageThumbnail = imgFileManager.GetBytesOfImage(imgFileManager.GetFullFileName(messageObj.FileName, true));

                                            notification.MessageType = "Image";
                                            notification.FileName = messageObj.FileName;
                                            notification.FileThumbnail = Convert.ToBase64String(bytesOfImageThumbnail);
                                        }
                                        notification.Flags = messageObj.Flags;
                                        notification.Timestamp = messageObj.SnapTime;
                                       
                                        resultObj = new ClientServerNotification(CilentServerNotificationType.SendMessageNotification, notification);
                                    }
                                    
                                    break;
                                case PermanentEventActionTypeEnum.Update:
                                    break;
                                case PermanentEventActionTypeEnum.Delete:
                                    break;
                                case PermanentEventActionTypeEnum.DeleteBeforeRead:
                                      string eventId = pEvent.PermanentEventId.ToString();
                                      string eventTimeStamp = GlobalUtility.GetStringValueOfDate(pEvent.RaisedTimestamp) ;
                                      var messageId = pEvent.EntityId.Value.ToString();
                                        var toUserName = pEvent.MembershipUser.UserName;
                                        MessageAckNotification ackNotification = new MessageAckNotification(pEvent.PermanentEventId.ToString(), eventTimeStamp)
                                        {
                                            MessageId = messageId,
                                            UserName = toUserName,
                                        };
                                        resultObj = new ClientServerNotification(CilentServerNotificationType.DeleteMessageBeforeReadNotification, ackNotification);
                                    break;
                                case PermanentEventActionTypeEnum.DeleteBeforeReadAck:
                                    var senderUser = MembershipManager.Instance.GetUser(pEvent.UserId);
                                    if (senderUser != null)
                                    {
                                        MessageAckNotification notification = new MessageAckNotification(pEvent.UserId.ToString(), GlobalUtility.GetStringValueOfDate( pEvent.RaisedTimestamp))
                                        {

                                            MessageId = pEvent.EntityId.Value.ToString(),
                                            UserName = MembershipManager.Instance.GetUser(pEvent.UserId).UserName,
                                            ExtraData=pEvent.DetailLevel.HasValue && pEvent.DetailLevel.Value==1?"1":"0",
                                        };
                                        resultObj = new ClientServerNotification(CilentServerNotificationType.DeleteMessageBeforeReadAckNotification, notification);
                                    }
                                    break;
                                    
                                case PermanentEventActionTypeEnum.Delivered:
                                      var deliveredMessage = MessageEntityController.Instance.GetMessage( pEvent.EntityId.Value);
                                      if (deliveredMessage != null && deliveredMessage.DeliveredTimestamp.HasValue)
                                      {
                                          MessageAckNotification notification = new MessageAckNotification(pEvent.PermanentEventId.ToString(),  GlobalUtility.GetStringValueOfDate(pEvent.RaisedTimestamp));
                                          notification.MessageId = deliveredMessage.MessageId.ToString();
                                          notification.EventTimestamp =  GlobalUtility.GetStringValueOfDate(deliveredMessage.DeliveredTimestamp);
                                          notification.UserName = deliveredMessage.MembershipUser.UserName;
                                          notification.EventId = pEvent.PermanentEventId.ToString();
                                          resultObj = new ClientServerNotification(CilentServerNotificationType.DeliveredMessageNotification, notification);
                                      }
                                    break;
                                case PermanentEventActionTypeEnum.Readed:
                                      var ReadedMessage = MessageEntityController.Instance.GetMessage( pEvent.EntityId.Value);
                                      if (ReadedMessage != null && ReadedMessage.ReadTimestamp.HasValue)
                                      {
                                          MessageAckNotification notification = new MessageAckNotification(pEvent.PermanentEventId.ToString(), GlobalUtility.GetStringValueOfDate( pEvent.RaisedTimestamp));
                                          notification.MessageId = ReadedMessage.MessageId.ToString();
                                          notification.EventTimestamp = GlobalUtility.GetStringValueOfDate( ReadedMessage.ReadTimestamp);
                                          notification.UserName = ReadedMessage.MembershipUser.UserName;
                                          notification.EventId = pEvent.PermanentEventId.ToString();
                                          resultObj = new ClientServerNotification(CilentServerNotificationType.ReadMessageNotification, notification);
                                      }
                                    break;
                                case PermanentEventActionTypeEnum.None:
                                    break;
                            }
                            break;
                        }
                    case PermanentEventEntityTypeEnum.Friend:
                        switch ((PermanentEventActionTypeEnum)pEvent.ActionTypeId)
                        {
                            case PermanentEventActionTypeEnum.Update:
                            case PermanentEventActionTypeEnum.Insert:

                                var friendEntity=FriendEntityController.Instance.GetFriendById(pEvent.EntityId.Value);
                                if (friendEntity != null)
                                {
                                    var imgFileManager = new ImageFileManager(friendEntity.FirendUserId.ToString());
                                    byte[] img32 = imgFileManager.GetBytesOfImage(imgFileManager.GetFullFileName(friendEntity.MembershipUser1.Picture, true));
                                    byte[] imgOriginal = imgFileManager.GetBytesOfImage(imgFileManager.GetFullFileName(friendEntity.MembershipUser1.Picture, false));


                                    FriendJoiningNotification notification = new FriendJoiningNotification(pEvent.PermanentEventId.ToString(),  GlobalUtility.GetStringValueOfDate(pEvent.RaisedTimestamp));
                                    var phoneEntity= friendEntity.MembershipUser1.MembershipPhoneNumbers.FirstOrDefault();
                                    notification.CellPhoneNumber =phoneEntity!=null ? phoneEntity.FullPhoneNumber:string.Empty;
                                    notification.ContactEntityId = friendEntity.MembershipUser1.UserId.ToString();
                                    notification.ContactUserName = friendEntity.MembershipUser1.UserName;
                                    notification.FirstName = friendEntity.MembershipUser1.FirstName;
                                    notification.ImageFull = imgOriginal!=null?Convert.ToBase64String(imgOriginal):string.Empty;
                                    notification.ImageThumbnail =img32!=null? Convert.ToBase64String(img32):string.Empty;
                                    notification.IsAnonymous =false;
                                    notification.LastName = friendEntity.MembershipUser1.LastName;
                                    notification.UserName = friendEntity.MembershipUser.UserName;
                                    notification.EventId = pEvent.PermanentEventId.ToString();
                                    resultObj = new ClientServerNotification(CilentServerNotificationType.FriendJoiningNotification, notification);
                                }
                                
                                break;
                           
                                
                            case PermanentEventActionTypeEnum.Delete:
                                break;
                            default://no other modes are are valid
                                break;

                        }
                        break;
                    //case PermanentEventEntityTypeEnum.Offer:
                    //    {
                    //     switch ((PermanentEventActionTypeEnum)pEvent.ActionTypeId)
                    //        {
                    //            case PermanentEventActionTypeEnum.Insert:
                                   
                    //                var messageObj = OfferEntityController.Instance.GetOffer( pEvent.EntityId.Value);
                    //                if (messageObj != null)
                    //                {
                    //                    OfferNotification notification = new OfferNotification(pEvent.PermanentEventId.ToString(), GlobalUtility.GetStringValueOfDate( pEvent.RaisedTimestamp));
                    //                    notification.Datetime =  GlobalUtility.GetStringValueOfDate(messageObj.DateTime);
                    //                    notification.FromUserName = messageObj.MembershipUser.UserName;

                    //                    notification.OfferId = messageObj.OfferId.ToString();
                    //                    notification.ToUserName = messageObj.MembershipUser1.UserName.ToString();
                    //                    notification.EventId = pEvent.PermanentEventId.ToString();
                    //                    var location = messageObj.Location;
                    //                    notification.Latitude = location != null && location.Latitude.HasValue ? location.Latitude.ToString() : string.Empty;
                    //                    notification.Longitude = location != null && location.Longitude.HasValue ? location.Longitude.ToString() : string.Empty;
                    //                    notification.Body = messageObj.Body;
                    //                    notification.CountOfVisited = messageObj.CountOfVisited;
                    //                    notification.OfferType = messageObj.OfferType;

                    //                    ImageFileManager imgFileManager = new ImageFileManager(messageObj.SenderUserId.ToString());
                    //                    byte[] bytesOfImageThumbnail=null;
                    //                  //  if (messageObj.OfferType == 1)
                    //                       bytesOfImageThumbnail= imgFileManager.GetBytesOfImage(imgFileManager.GetFullFileName(messageObj.MembershipUser.Picture, ImageState.Thumbnail64));
                    //                    //else
                    //                      //  bytesOfImageThumbnail=imgFileManager.GetBytesOfImage(imgFileManager.GetFullFileName(messageObj.FileName, ImageState.Thumbnail64));
                    //                    var bytesOfImageFull = imgFileManager.GetBytesOfImage(imgFileManager.GetFullFileName(messageObj.FileName, ImageState.Normal));
                    //                    notification.FileName = messageObj.FileName;
                    //                    notification.FileThumbnail = Convert.ToBase64String(bytesOfImageThumbnail);
                    //                    //notification.FileData = Convert.ToBase64String(bytesOfImageFull); //do not attach fileData to signalR Objects


                    //                    notification.Flags = messageObj.Flags;
                    //                    notification.TimeStamp = messageObj.SnapTime;

                    //                    resultObj = new ClientServerNotification(CilentServerNotificationType.SendOfferNotification, notification);
                    //                }
                                    
                    //                break;
                    //            case PermanentEventActionTypeEnum.Update:
                    //                break;
                    //            case PermanentEventActionTypeEnum.Delete:
                    //                break;
                    //            case PermanentEventActionTypeEnum.DeleteBeforeRead:
                    //                  string eventId = pEvent.PermanentEventId.ToString();
                    //                  string eventTimeStamp = GlobalUtility.GetStringValueOfDate( pEvent.RaisedTimestamp);
                    //                  var messageId = pEvent.EntityId.Value.ToString();
                    //                    var toUserName = pEvent.MembershipUser.UserName;
                    //                    MessageAckNotification ackNotification = new MessageAckNotification(pEvent.PermanentEventId.ToString(), eventTimeStamp)
                    //                    {
                    //                        MessageId = messageId,
                    //                        UserName = toUserName,
                    //                    };
                    //                    resultObj = new ClientServerNotification(CilentServerNotificationType.DeleteOfferBeforeReadNotification, ackNotification);
                    //                break;
                    //            case PermanentEventActionTypeEnum.DeleteBeforeReadAck:
                    //                var senderUser = MembershipManager.Instance.GetUser(pEvent.UserId);
                    //                if (senderUser != null)
                    //                {
                    //                    MessageAckNotification notification = new MessageAckNotification(pEvent.UserId.ToString(),GlobalUtility.GetStringValueOfDate( pEvent.RaisedTimestamp))
                    //                    {

                    //                        MessageId = pEvent.EntityId.Value.ToString(),
                    //                        UserName = MembershipManager.Instance.GetUser(pEvent.UserId).UserName,
                    //                        ExtraData=pEvent.DetailLevel.HasValue && pEvent.DetailLevel.Value==1?"1":"0",
                    //                    };
                    //                    resultObj = new ClientServerNotification(CilentServerNotificationType.DeleteOfferBeforeReadAckNotification, notification);
                    //                }
                    //                break;
                                    
                    //            case PermanentEventActionTypeEnum.Delivered:
                    //                var deliveredMessage = OfferEntityController.Instance.GetOffer(pEvent.EntityId.Value);
                    //                  if (deliveredMessage != null && deliveredMessage.DeliveredTimestamp.HasValue)
                    //                  {
                    //                      MessageAckNotification notification = new MessageAckNotification(pEvent.PermanentEventId.ToString(), GlobalUtility.GetStringValueOfDate( pEvent.RaisedTimestamp));
                    //                      notification.MessageId = deliveredMessage.OfferId.ToString();
                    //                      notification.EventTimestamp = GlobalUtility.GetStringValueOfDate( deliveredMessage.DeliveredTimestamp);
                    //                      notification.UserName = deliveredMessage.MembershipUser.UserName;
                    //                      notification.EventId = pEvent.PermanentEventId.ToString();
                    //                      resultObj = new ClientServerNotification(CilentServerNotificationType.DeliveredOfferNotification, notification);
                    //                  }
                    //                break;
                    //            case PermanentEventActionTypeEnum.Readed:
                    //                var ReadedMessage = OfferEntityController.Instance.GetOffer(pEvent.EntityId.Value);
                    //                  if (ReadedMessage != null && ReadedMessage.ReadTimestamp.HasValue)
                    //                  {
                    //                      MessageAckNotification notification = new MessageAckNotification(pEvent.PermanentEventId.ToString(), GlobalUtility.GetStringValueOfDate( pEvent.RaisedTimestamp));
                    //                      notification.MessageId = ReadedMessage.OfferId.ToString();
                    //                      notification.EventTimestamp = GlobalUtility.GetStringValueOfDate( ReadedMessage.ReadTimestamp);
                    //                      notification.UserName = ReadedMessage.MembershipUser.UserName;
                    //                      notification.EventId = pEvent.PermanentEventId.ToString();
                    //                      resultObj = new ClientServerNotification(CilentServerNotificationType.ReadOfferNotification, notification);
                    //                  }
                    //                break;
                    //            case PermanentEventActionTypeEnum.None:
                    //                break;
                    //        }
                    //    }
                    //    break;
                    
                    case PermanentEventEntityTypeEnum.None:
                        break;


                }
                
            }
            
            return resultObj;
        }

    }
}
