﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.Services.RealtimeSearvices.EventDrivenDispatcher.Notification
{
    public class NotificationBase
    {
        [JsonProperty("EventId")]
        public string EventId { get; set; }

        [JsonProperty("EventTimestamp")]
        public string EventTimestamp { get; set; }

        [JsonProperty("ManagedObjectIdString")]
        public string ManagedObjectIdString { get; set; }

        public NotificationBase(string eventId, string eventTimestamp, string managedObjectIdString)
        {
            this.EventId = eventId;
            this.EventTimestamp = eventTimestamp;
            this.ManagedObjectIdString = managedObjectIdString;
        }


    }
}
