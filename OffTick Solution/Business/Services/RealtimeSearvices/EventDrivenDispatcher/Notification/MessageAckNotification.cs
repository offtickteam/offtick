﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.Services.RealtimeSearvices.EventDrivenDispatcher.Notification
{
    public class MessageAckNotification : NotificationBase
    {
        public MessageAckNotification(string eventId, string eventTimestamp, string managedObjectIdString)
            : base(eventId, eventTimestamp, managedObjectIdString)
        {
        }
        public MessageAckNotification(string eventId, string eventTimestamp)
            : this(eventId, eventTimestamp, string.Empty)
        {
        }
        public MessageAckNotification() : this(string.Empty, string.Empty, string.Empty) { }
        public string MessageId { get; set; }
        public string UserName { get; set; }
        public string ExtraData { get; set; }
        

    }
}
