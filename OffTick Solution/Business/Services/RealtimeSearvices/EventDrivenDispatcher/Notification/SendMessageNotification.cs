﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.Services.RealtimeSearvices.EventDrivenDispatcher.Notification
{

    public enum SendMessageType
    {
        Text=0,
        Image=1
    }
  
    public class SendMessageNotification : NotificationBase
    {
        public SendMessageNotification() : this(string.Empty, string.Empty, string.Empty) { }
        public SendMessageNotification(string eventId, string eventTimestamp)
            : this(eventId, eventTimestamp, string.Empty)
        {
        }
        public SendMessageNotification(string eventId, string eventTimestamp, string managedObjectIdString)
            : base(eventId, eventTimestamp, managedObjectIdString)
        {
        }

        [JsonProperty("FromUserName")]
        public string FromUserName { get; set; }

        [JsonProperty("FromConnectionId")]
        public string FromConnectionId { get; set; }

        [JsonProperty("ToUserName")]
        public string ToUserName { get; set; }

        [JsonProperty("ToConnectionId")]
        public string ToConnectionId { get; set; }

        [JsonProperty("Message")]
        public string Message { get; set; }

        [JsonProperty("Datetime")]
        public string Datetime { get; set; }

        [JsonProperty("DatetimeFa")]
        public string DatetimeFa { get; set; }

        [JsonProperty("MessageId")]
        public string MessageId { get; set; }

        [JsonProperty("MessageType")]
        public string MessageType { get; set; }

        [JsonProperty("Latitude")]
        public string Latitude { get; set; }

        [JsonProperty("Longitude")]
        public string Longitude { get; set; }

        [JsonProperty("FileName")]
        public string FileName { get; set; }

        [JsonProperty("FileThumbnail")]
        public string FileThumbnail { get; set; }

        [JsonProperty("Flags")]
        public int Flags { get; set; }

        [JsonProperty("Timestamp")]
        public int Timestamp { get; set; }


        //ReadAckFlag=1
        //SafeModificationFlag=2
        public bool isSafeModificationFlagOn()
        {

            return isSafeModificationFlagOn(this.Flags);
        }
        public bool isReadAckFlagOn()
        {

            return isReadAckFlagOn(this.Flags);
        }

        public static bool isSafeModificationFlagOn(int flags)
        {

            if (flags == 2 || flags == 3)
                return true;
            return false;
        }


        public static bool isReadAckFlagOn(int flags)
        {

            if (flags == 1 || flags == 3)
                return true;
            return false;
        }
    }
}
