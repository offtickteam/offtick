﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.Services.RealtimeSearvices.EventDrivenDispatcher.Notification
{


    public class OfferNotification:NotificationBase
    {
        public OfferNotification() : this(string.Empty, string.Empty, string.Empty) { }
        public OfferNotification(string eventId, string eventTimestamp):this(eventId,eventTimestamp,string.Empty)

        {
        }
        public OfferNotification(string eventId, string eventTimestamp, string managedObjectIdString)
            : base(eventId, eventTimestamp, managedObjectIdString)
        {
        }

        public string FromUserName { get; set; }
        public string ToUserName { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public string Datetime { get; set; }
        public string OfferId { get; set; }
        public short  OfferType { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string FileName { get; set; }
        public string FileData { get; set; }
        public string FileThumbnail { get; set; }
        public int Flags { get; set; }
        public int TimeStamp { get; set; }
        public long CountOfVisited { get; set; }


        //ReadAckFlag=1
        //SafeModificationFlag=2
        public bool isSafeModificationFlagOn()
        {

            return isSafeModificationFlagOn(this.Flags);
        }
        public bool isReadAckFlagOn()
        {

            return isReadAckFlagOn(this.Flags);
        }

        public static bool isSafeModificationFlagOn(int flags)
        {

            if (flags == 2 || flags == 3)
                return true;
            return false;
        }


        public static bool isReadAckFlagOn(int flags)
        {

            if (flags == 1 || flags == 3)
                return true;
            return false;
        }
    }
}
