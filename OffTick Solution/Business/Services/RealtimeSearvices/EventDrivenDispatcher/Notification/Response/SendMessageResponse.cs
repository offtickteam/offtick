﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.Services.RealtimeSearvices.EventDrivenDispatcher.Notification.Response
{
     public class SendMessageResponse
    {
         [JsonProperty("MessageId")]
         public string MessageId { get; set; }

         [JsonProperty("DatetimeFa")]
         public string DatetimeFa { get; set; }
    }
}
