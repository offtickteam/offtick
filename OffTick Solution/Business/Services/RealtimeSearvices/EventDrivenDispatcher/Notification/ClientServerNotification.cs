﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.Services.RealtimeSearvices.EventDrivenDispatcher.Notification
{
    public enum CilentServerNotificationType
    {
        SendMessageNotification,
        ReadMessageNotification,
        FriendJoiningNotification,
        DeliveredMessageNotification,
        DeleteMessageBeforeReadNotification,
        DeleteMessageBeforeReadAckNotification,


        SendOfferNotification,
        ReadOfferNotification,
        DeliveredOfferNotification,
        DeleteOfferBeforeReadNotification,
        DeleteOfferBeforeReadAckNotification,
    }
    public class ClientServerNotification
    {
        public string TypeOfNotification {  get; private set; }
        public object NotificationObject { get; set; }


        public ClientServerNotification(CilentServerNotificationType type, object notification)
        {
            if (notification != null)
            {
                switch (type)
                {
                    case CilentServerNotificationType.DeliveredMessageNotification:

                        this.TypeOfNotification = "DeliveredMessageNotification";
                        if(notification is MessageAckNotification)
                            this.NotificationObject = notification;
                        else
                            throw new InvalidCastException(" notification type dismatch");
                        break;
                    case CilentServerNotificationType.FriendJoiningNotification:
                        this.TypeOfNotification = "FriendJoiningNotification";
                        if (notification is FriendJoiningNotification)
                            this.NotificationObject = notification;
                        else
                            throw new InvalidCastException(" notification type dismatch");
                        break;
                    case CilentServerNotificationType.ReadMessageNotification:
                        this.TypeOfNotification = "ReadMessageNotification";
                        if (notification is MessageAckNotification)
                            this.NotificationObject = notification;
                        else
                            throw new InvalidCastException(" notification type dismatch");
                        break;
                    case CilentServerNotificationType.SendMessageNotification:
                        this.TypeOfNotification = "SendMessageNotification";
                        if (notification is SendMessageNotification)
                            this.NotificationObject = notification;
                        else
                            throw new InvalidCastException(" notification type dismatch");
                        break;
                    case CilentServerNotificationType.DeleteMessageBeforeReadNotification:
                        this.TypeOfNotification = "DeleteMessageBeforeReadNotification";
                        if (notification is MessageAckNotification)
                            this.NotificationObject = notification;
                        else
                            throw new InvalidCastException(" notification type dismatch");
                        break;
                    case CilentServerNotificationType.DeleteMessageBeforeReadAckNotification:
                        this.TypeOfNotification = "DeleteMessageBeforeReadAckNotification";
                        if (notification is MessageAckNotification)
                            this.NotificationObject = notification;
                        else
                            throw new InvalidCastException(" notification type dismatch");
                        break;






                    case CilentServerNotificationType.DeliveredOfferNotification:

                        this.TypeOfNotification = "DeliveredOfferNotification";
                        if (notification is MessageAckNotification)
                            this.NotificationObject = notification;
                        else
                            throw new InvalidCastException(" notification type dismatch");
                        break;
                    case CilentServerNotificationType.ReadOfferNotification:
                        this.TypeOfNotification = "ReadOfferNotification";
                        if (notification is MessageAckNotification)
                            this.NotificationObject = notification;
                        else
                            throw new InvalidCastException(" notification type dismatch");
                        break;
                    case CilentServerNotificationType.SendOfferNotification:
                        this.TypeOfNotification = "SendOfferNotification";
                        if (notification is OfferNotification)
                            this.NotificationObject = notification;
                        else
                            throw new InvalidCastException(" notification type dismatch");
                        break;
                    case CilentServerNotificationType.DeleteOfferBeforeReadNotification:
                        this.TypeOfNotification = "DeleteOfferBeforeReadNotification";
                        if (notification is MessageAckNotification)
                            this.NotificationObject = notification;
                        else
                            throw new InvalidCastException(" notification type dismatch");
                        break;
                    case CilentServerNotificationType.DeleteOfferBeforeReadAckNotification:
                        this.TypeOfNotification = "DeleteOfferBeforeReadAckNotification";
                        if (notification is MessageAckNotification)
                            this.NotificationObject = notification;
                        else
                            throw new InvalidCastException(" notification type dismatch");
                        break;
                }
            }
        }
    }
}
