﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.Services.RealtimeSearvices.EventDrivenService
{
    public enum  PermanentEventEntityTypeEnum :short
    {
        None = 0,
        Message = 1,
        Friend = 2,
        Offer=3,
    }
}
