﻿using Microsoft.AspNet.SignalR.Hubs;
using Offtick.Business.Services.RealtimeSearvices.EventDrivenDispatcher;
using Offtick.Business.Services.RealtimeSearvices.EventDrivenDispatcher.Notification;
using Offtick.Business.Services.RealtimeSearvices.EventDrivenService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.SignalR;
using Offtick.Business.Services.Storage;
using Offtick.Data.Entities.Common;
using Offtick.Business.Membership;
using Offtick.Core.EnumTypes;
using Offtick.Data.Context.ExpertOnlinerContexts;
using Offtick.Business.Services.Managers;
using System.Data.Entity.Spatial;
using Offtick.Core.Utility.PersianTools;
using Offtick.Business.Services.RealtimeSearvices.EventDrivenDispatcher.Notification.Response;



namespace Offtick.Business.Services.RealtimeSearvices
{
   

    [HubName("ChatService")]
    public class ChatService : Microsoft.AspNet.SignalR.Hub
    {
        public ChatService()
        {
            if (System.Web.HttpContext.Current != null)
            {
                
                if (DataContextManager.Container == null)
                {
                    DataContextContainer objectContext = new DataContextContainer();
                    DataContextManager.SetDataContextContainer(objectContext);
                }
                 
                
            }
        }

        //protected override void Dispose(bool disposing)
        //{
        //    if (System.Web.HttpContext.Current != null)
        //    {
        //        DataContextManager.FreeStorage();
        //    }
        //    if (disposing)
        //        base.Dispose(disposing);
        //}
        static IOnlineConnectionsService conService = OnlineConnectionsService.Instance;


        #region Message
       
        public ActionResponseFromServerToClient SRpcSendMessage(SendMessageNotification notification)
        {
            try
            {
               

                if (notification != null)
                {
                    conService.UpdateLastActivityByUserName(notification.FromUserName);
                    if (FriendEntityController.Instance.IsFriendBlocked(notification.ToUserName, notification.FromUserName))
                        return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Empty, "your are blocked by " + notification.ToUserName, string.Empty, string.Empty);

                    notification.Datetime = DateTime.Now.ToString("yyyyMMddHHmmss");
                    notification.DatetimeFa = PersianDateConvertor.ToKhorshidiDateTime(DateTime.Now,false);
                    //check from connection id
                    Guid messageId = Guid.NewGuid();
                    notification.FromConnectionId =this.Context!=null ? this.Context.ConnectionId:null;
                    notification.MessageId = messageId.ToString();
                    Int16 messageType = (short)(notification.MessageType == "Text" ? 0 : 1);

                    //save messages
                    //MessageEntityController mc = new MessageEntityController(null);
                    var toUser = MembershipManager.Instance.GetUser(notification.ToUserName);
                    bool hasGeoLocation = !string.IsNullOrEmpty(notification.Latitude) && !string.IsNullOrEmpty(notification.Longitude);
                    if (toUser != null)
                    {
                        Guid recieverUserId = toUser.UserId;
                        Guid senderUserId=MembershipManager.Instance.GetUser(notification.FromUserName).UserId;
                        Conversation conversation=MessageEntityController.Instance.GetConversation(senderUserId, recieverUserId);

                        var messageToSave = new Offtick.Data.Context.ExpertOnlinerContexts.Message()
                        {
                            Conversation = conversation,
                            Body = notification.Message,
                            DateTime = DateTime.Now,
                            MessageId = messageId,
                            SenderUserId = senderUserId,
                            RecieverUserId = recieverUserId,
                            Title = "",
                            MessageType = messageType,
                            Location = hasGeoLocation ? DbGeography.FromText(string.Format("POINT({0} {1})", notification.Latitude, notification.Longitude)) : null,
                            Flags = notification.Flags,
                            SnapTime = notification.Timestamp,
                        };
                        var saveResult = MessageEntityController.Instance.SaveMessage(messageToSave);

                        //send Message
                        if (saveResult == AddStatus.Added)
                        {
                            if (messageType == (short)SendMessageType.Text)
                            {
                                PermanentEventDispatcher.Instance.CreateEventSendMessage(messageId, recieverUserId, notification);
                            }
                            return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, string.Empty, new SendMessageResponse() { MessageId = messageToSave.MessageId.ToString(), DatetimeFa = notification.DatetimeFa }, "SendMessageResponse");

                        }
                        else
                        {
                            return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, saveResult.ToString(), string.Empty, string.Empty, string.Empty);
                        }
                    }
                }
                return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "Notification is empty", string.Empty, string.Empty, string.Empty);
            }
            finally
            {
               
            }
        }


        public ActionResponseFromServerToClient SRpcRequestArchiveMessage(MessageAckNotification notification)
        {
            try
            {
               

                Guid msgId = Guid.Parse(notification.MessageId);
                string userName = conService.getUsernameOfConnection(this.Context.ConnectionId);
                conService.UpdateLastActivityByUserName(userName);
                MessageEntityController.Instance.ArchiveMessage(userName, msgId);

                // this.Clients.Client(this.Context.ConnectionId).ReguestArchiveMessageAck(notification.MessageId);
                return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, string.Empty, "Sucess", string.Empty);
                // return "sucess";
            }
            catch
            {
                return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Empty, "Failed", string.Empty, string.Empty);
                //return "failed";
            }
            finally
            {
               
            }
        }

        public ActionResponseFromServerToClient SRpcDeliverMessage(MessageAckNotification notification)
        {
            try
            {
               
                Guid msgId = Guid.Parse(notification.MessageId);
                string userName = conService.getUsernameOfConnection(this.Context.ConnectionId);
                conService.UpdateLastActivityByUserName(userName);
                MessageEntityController.Instance.MessageDelivered(userName, msgId);
                var msg = MessageEntityController.Instance.GetMessage(Guid.Parse(notification.MessageId));
                if (msg != null && SendMessageNotification.isReadAckFlagOn(msg.Flags))
                    PermanentEventDispatcher.Instance.CreateEvent(PermanentEventActionTypeEnum.Delivered, PermanentEventEntityTypeEnum.Message, msgId, msg.MembershipUser.UserId, 0, string.Empty, null);



                return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, string.Empty, "Sucess", string.Empty);
                // return "sucess";
            }
            catch
            {
                return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Empty, "Failed", string.Empty, string.Empty);
                //return "failed";
            }
            finally
            {
               
            }
        }
        public ActionResponseFromServerToClient SRpcReadMessage(MessageAckNotification notification)
        {
            try
            {
               
                Guid msgId = Guid.Parse(notification.MessageId);
                string userName = conService.getUsernameOfConnection(this.Context.ConnectionId);
                conService.UpdateLastActivityByUserName(userName);
                MessageEntityController.Instance.MessageReaded(userName, msgId);

                var msg = MessageEntityController.Instance.GetMessage(Guid.Parse(notification.MessageId));
                if (msg != null && SendMessageNotification.isReadAckFlagOn(msg.Flags))
                    PermanentEventDispatcher.Instance.CreateEvent(PermanentEventActionTypeEnum.Readed, PermanentEventEntityTypeEnum.Message, msgId, msg.MembershipUser.UserId, 0, string.Empty, null);


                return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, string.Empty, "Sucess", string.Empty);
                // return "sucess";
            }
            catch
            {
                return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Empty, "Failed", string.Empty, string.Empty);
                //return "failed";
            }
            finally
            {
               
            }
        }
        /// <summary>
        /// batch read message, occur when a bulck of message deliver to user and user is not in chat page, then he/she come back to chat page, this method runs and
        /// in batch mode mark all of messages as readed in which deliver to user but not read
        /// </summary>
        /// <param name="notification">FirstUsername:sender, SecondUserName:reciever</param>
        /// <returns></returns>
        public ActionResponseFromServerToClient SRpcReadMessages(BidirectionalInteractionNotification notification)
        {
            try
            {
               
                conService.UpdateLastActivityByUserName(notification.SecondUserName);
                var listOfUnread = MessageEntityController.Instance.getListOfAllUnReadMessage(notification.FirstUserName, notification.SecondUserName);
                foreach (var unread in listOfUnread)
                {
                    Guid msgId = unread.MessageId;
                    if (SendMessageNotification.isReadAckFlagOn(unread.Flags))// to do, && receiver.readAckFlag==on
                    {
                        MessageEntityController.Instance.MessageReaded(notification.SecondUserName, msgId);
                        PermanentEventDispatcher.Instance.CreateEvent(PermanentEventActionTypeEnum.Readed, PermanentEventEntityTypeEnum.Message, msgId, unread.MembershipUser.UserId, 0, string.Empty, null);
                    }
                }
                return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, string.Empty, "Sucess", string.Empty);
            }
            catch
            {
                return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Empty, "Failed", string.Empty, string.Empty);
                //return "failed";
            }
            finally
            {
               
            }
        }


        public ActionResponseFromServerToClient SRpcStartTyping(BidirectionalInteractionNotification notification)
        {
            try
            {
               
                if (notification != null)
                {
                    conService.UpdateLastActivityByUserName(notification.FirstUserName);
                    PermanentEventDispatcher.Instance.CreateEvent(PermanentEventActionTypeEnum.Typing, PermanentEventEntityTypeEnum.Message, null, null, 0, string.Empty, notification);
                    return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, string.Empty, "Sucess", string.Empty);
                }
                return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Empty, string.Empty, "Failed", string.Empty);
            }
            finally
            {
               
            }
        }

        public ActionResponseFromServerToClient SRpcCancelTyping(BidirectionalInteractionNotification notification)
        {
            try
            {
               
                if (notification != null)
                {
                    conService.UpdateLastActivityByUserName(notification.FirstUserName);
                    PermanentEventDispatcher.Instance.CreateEvent(PermanentEventActionTypeEnum.CancelTyping, PermanentEventEntityTypeEnum.Message, null, null, 0, string.Empty, notification);
                    return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, string.Empty, "Sucess", string.Empty);

                }
                return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Empty, string.Empty, "Failed", string.Empty);
            }
            finally
            {
               
            }
        }

        public ActionResponseFromServerToClient SRpcCheckOnlineStatus(BidirectionalInteractionNotification notification)
        {
            try
            {
               
                if (notification != null)
                {
                    var dataOfReguestedUser = conService.getDataOfUser(notification.SecondUserName);
                    if (dataOfReguestedUser == null)
                        return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "", "Sucess", string.Empty);
                    else
                        return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, dataOfReguestedUser.GetLastActivityString(), "Sucess", string.Empty);
                }
                return null;
            }
            finally
            {
               
            }
        }

        public ActionResponseFromServerToClient SRpcDeleteMessage(MessageAckNotification notification)
        {
            try
            {

                string actualUserName = conService.getUsernameOfConnection(this.Context.ConnectionId);
                if (actualUserName.Equals(notification.UserName))
                {
                    Guid msgId = Guid.Parse(notification.MessageId);
                    var msg = MessageEntityController.Instance.GetMessage(msgId);
                    if (msg != null)
                    {
                        var deleteResult = MessageEntityController.Instance.DeleteMessage(notification.UserName, msgId);
                        if (deleteResult == DeleteStatus.DeletedBeforeDeliver || deleteResult == DeleteStatus.RejectedByNotExit || deleteResult == DeleteStatus.RejectedByNonEffectiveQuery || deleteResult == DeleteStatus.Deleted)
                        {
                            var receiverUserId = msg.SenderUserId;
                            PermanentEventDispatcher.Instance.CreateEvent(PermanentEventActionTypeEnum.Delete, PermanentEventEntityTypeEnum.Message, msgId, receiverUserId, 1, string.Empty, null);
                            return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "message has  been deleted ", notification, string.Empty);
                        }
                        if (deleteResult == DeleteStatus.Deleted)
                        {
                            var receiverUserId = msg.RecieverUserId;
                            PermanentEventDispatcher.Instance.CreateEvent(PermanentEventActionTypeEnum.Delete, PermanentEventEntityTypeEnum.Message, msgId, receiverUserId, -1, string.Empty, null);
                            return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "message has  been deleted",notification, string.Empty);

                        }
                        if (deleteResult == DeleteStatus.RejectedByAccessDenied || deleteResult == DeleteStatus.RejectedByInternalSystemError)
                        {
                            var receiverUserId = msg.SenderUserId;
                            //PermanentEventDispatcher.Instance.CreateEvent(PermanentEventActionTypeEnum.DeleteBeforeReadAck, PermanentEventEntityTypeEnum.Message, msgId, receiverUserId, 0, string.Empty, null);
                            return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Empty, "Error, Access Denied,message has been delivered", notification, string.Empty);
                        }
                    }
                }
                else
                {
                    //todo : security alert
                    return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Empty, "Error, Access Denied", "", string.Empty);

                }
                return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Empty, "Error, Please try later", "", string.Empty);
            }
            finally
            {

            }
        }

        public ActionResponseFromServerToClient SRpcDeleteConversationBeforeRead(MessageAckNotification notification)
        {
            try
            {

                string actualUserName = conService.getUsernameOfConnection(this.Context.ConnectionId);
                if (actualUserName.Equals(notification.UserName))
                {
                    Guid msgId = Guid.Parse(notification.MessageId);
                    var msg = MessageEntityController.Instance.GetMessage(msgId);
                    if (msg != null)
                    {
                        var deleteResult = MessageEntityController.Instance.DeleteMessageBeforeRead(notification.UserName, msgId);
                        if (deleteResult ==DeleteStatus.DeletedBeforeDeliver || deleteResult == DeleteStatus.RejectedByNotExit || deleteResult ==DeleteStatus.RejectedByNonEffectiveQuery)
                        {
                            var receiverUserId = msg.SenderUserId;
                            PermanentEventDispatcher.Instance.CreateEvent(PermanentEventActionTypeEnum.DeleteBeforeReadAck, PermanentEventEntityTypeEnum.Message, msgId, receiverUserId, 1, string.Empty, null);
                            return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "message has  been deleted before read", "Sucess", string.Empty);
                        }
                        if (deleteResult == DeleteStatus.DeletedBeforeRead)
                        {
                            var receiverUserId = msg.RecieverUserId;
                            PermanentEventDispatcher.Instance.CreateEvent(PermanentEventActionTypeEnum.DeleteBeforeRead, PermanentEventEntityTypeEnum.Message, msgId, receiverUserId, -1, string.Empty, null);
                            return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "message has  been deleted before read", "Sucess", string.Empty);

                        }
                        if (deleteResult == DeleteStatus.RejectedByAccessDenied || deleteResult == DeleteStatus.RejectedByInternalSystemError)
                        {
                            var receiverUserId = msg.SenderUserId;
                            //PermanentEventDispatcher.Instance.CreateEvent(PermanentEventActionTypeEnum.DeleteBeforeReadAck, PermanentEventEntityTypeEnum.Message, msgId, receiverUserId, 0, string.Empty, null);
                            return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Empty, "Error, Access Denied,message has been delivered", "", string.Empty);
                        }
                    }
                }
                else
                {
                    //todo : security alert
                    return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Empty, "Error, Access Denied", "", string.Empty);

                }
                return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Empty, "Error, Please try later", "", string.Empty);
            }
            finally
            {

            }
        }

        public ActionResponseFromServerToClient SRpcDeleteConversationBeforeReadAck(MessageAckNotification notification, bool operationResult)
        {

            Guid msgId = Guid.Parse(notification.MessageId);
            //var msg = MessageEntityController.Instance.GetMessage(msgId);
            var receiverUserId = MembershipManager.Instance.GetUser(notification.UserName).UserId;

            if (operationResult)
            {

                PermanentEventDispatcher.Instance.CreateEvent(PermanentEventActionTypeEnum.DeleteBeforeReadAck, PermanentEventEntityTypeEnum.Message, msgId, receiverUserId, 1, string.Empty, null);
                return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, string.Empty, "Sucess", string.Empty);
            }
            else
            {
                PermanentEventDispatcher.Instance.CreateEvent(PermanentEventActionTypeEnum.DeleteBeforeReadAck, PermanentEventEntityTypeEnum.Message, msgId, receiverUserId, 0, string.Empty, null);
                return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, string.Empty, "Sucess", string.Empty);
            }

        }

        #endregion

        #region Offer
        public ActionResponseFromServerToClient SRpcSendOffer(OfferNotification notification)
        {
            try
            {


                if (notification != null)
                {
                    conService.UpdateLastActivityByUserName(notification.FromUserName);
                    if (FriendEntityController.Instance.IsFriendBlocked(notification.ToUserName, notification.FromUserName))
                        return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Empty, "your are blocked by " + notification.ToUserName, string.Empty, string.Empty);

                    notification.Datetime = DateTime.Now.ToString("yyyyMMddHHmmss");

                    //check from connection id
                    Guid messageId = Guid.NewGuid();

                    notification.OfferId = messageId.ToString();
                    //Int16 messageType = (short)(notification.MessageType == "Text" ? 0 : 1);

                    //save messages
                    //MessageEntityController mc = new MessageEntityController(null);
                    var toUser = MembershipManager.Instance.GetUser(notification.ToUserName);
                    bool hasGeoLocation = !string.IsNullOrEmpty(notification.Latitude) && !string.IsNullOrEmpty(notification.Longitude);
                    if (toUser != null)
                    {
                        Guid recieverUserId = toUser.UserId;

                        var messageToSave = new Offer()
                        {
                            Body = notification.Body,
                            DateTime = DateTime.Now,
                            OfferId = messageId,
                            SenderUserId = MembershipManager.Instance.GetUser(notification.FromUserName).UserId,
                            RecieverUserId = recieverUserId,
                            Title = notification.Title,
                            OfferType = notification.OfferType,
                            Location = hasGeoLocation ?DbGeography.FromText(string.Format("POINT({0} {1})", notification.Latitude, notification.Longitude)) : null,
                            Flags = notification.Flags,
                            SnapTime = notification.TimeStamp,
                            ConfirmStatusId= (int)ConfirmStatus.NotDecide
                        };
                        var saveResult = OfferManager.Instance.SaveOffer(messageToSave);

                        //send Message
                        if (saveResult == AddStatus.Added)
                        {
                            /* if (messageType == (short)SendMessageType.Text)
                             {
                                 PermanentEventDispatcher.Instance.CreateEventSendOffer(messageId, recieverUserId, notification);
                             }*/
                            return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, string.Empty, messageToSave.OfferId.ToString(), string.Empty);

                        }
                        else
                        {
                            return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Empty, string.Empty, string.Empty, string.Empty);
                        }
                    }
                }
                return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Empty, string.Empty, string.Empty, string.Empty);
            }
            finally
            {

            }
        }

        public ActionResponseFromServerToClient SRpcDeliverOffer(MessageAckNotification notification)
        {
            try
            {

                Guid msgId = Guid.Parse(notification.MessageId);
                string userName = conService.getUsernameOfConnection(this.Context.ConnectionId);
                conService.UpdateLastActivityByUserName(userName);
                OfferManager.Instance.OfferDelivered(userName, msgId);
                var msg = MessageEntityController.Instance.GetMessage(Guid.Parse(notification.MessageId));
                if (msg != null && SendMessageNotification.isReadAckFlagOn(msg.Flags))
                    PermanentEventDispatcher.Instance.CreateEvent(PermanentEventActionTypeEnum.Delivered, PermanentEventEntityTypeEnum.Offer, msgId, msg.MembershipUser.UserId, 0, string.Empty, null);



                return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, string.Empty, "Sucess", string.Empty);
                // return "sucess";
            }
            catch
            {
                return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Empty, "Failed", string.Empty, string.Empty);
                //return "failed";
            }
            finally
            {

            }
        }

        public ActionResponseFromServerToClient SRpcReadOffer(MessageAckNotification notification)
        {
            try
            {

                Guid msgId = Guid.Parse(notification.MessageId);
                string userName = conService.getUsernameOfConnection(this.Context.ConnectionId);
                conService.UpdateLastActivityByUserName(userName);
                OfferManager.Instance.OfferReaded(userName, msgId);

                var msg = OfferManager.Instance.GetOffer(Guid.Parse(notification.MessageId));
                if (msg != null && SendMessageNotification.isReadAckFlagOn(msg.Flags))
                    PermanentEventDispatcher.Instance.CreateEvent(PermanentEventActionTypeEnum.Readed, PermanentEventEntityTypeEnum.Offer, msgId, msg.MembershipUser.UserId, 0, string.Empty, null);


                return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, string.Empty, "Sucess", string.Empty);
                // return "sucess";
            }
            catch
            {
                return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Empty, "Failed", string.Empty, string.Empty);
                //return "failed";
            }
            finally
            {

            }
        }
        /// <summary>
        /// batch read message, occur when a bulck of message deliver to user and user is not in chat page, then he/she come back to chat page, this method runs and
        /// in batch mode mark all of messages as readed in which deliver to user but not read
        /// </summary>
        /// <param name="notification">FirstUsername:sender, SecondUserName:reciever</param>
        /// <returns></returns>
        public ActionResponseFromServerToClient SRpcReadOffers(BidirectionalInteractionNotification notification)
        {
            try
            {

                conService.UpdateLastActivityByUserName(notification.SecondUserName);
                var listOfUnread = OfferManager.Instance.getListOfAllUnReadOffer(notification.FirstUserName, notification.SecondUserName);
                foreach (var unread in listOfUnread)
                {
                    Guid msgId = unread.OfferId;
                    if (SendMessageNotification.isReadAckFlagOn(unread.Flags))// to do, && receiver.readAckFlag==on
                    {
                        OfferManager.Instance.OfferReaded(notification.SecondUserName, msgId);
                        PermanentEventDispatcher.Instance.CreateEvent(PermanentEventActionTypeEnum.Readed, PermanentEventEntityTypeEnum.Offer, msgId, unread.MembershipUser.UserId, 0, string.Empty, null);
                    }
                }
                return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, string.Empty, "Sucess", string.Empty);
            }
            catch
            {
                return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Empty, "Failed", string.Empty, string.Empty);
                //return "failed";
            }
            finally
            {

            }
        }

        public ActionResponseFromServerToClient SRpcDeleteOfferBeforeRead(MessageAckNotification notification)
        {
            try
            {

                string actualUserName = conService.getUsernameOfConnection(this.Context.ConnectionId);
                if (actualUserName.Equals(notification.UserName))
                {
                    Guid msgId = Guid.Parse(notification.MessageId);
                    var msg = OfferManager.Instance.GetOffer(msgId);
                    if (msg != null)
                    {
                        var deleteResult = OfferManager.Instance.DeleteOfferBeforeRead(notification.UserName, msgId);
                        if (deleteResult ==DeleteStatus.DeletedBeforeDeliver || deleteResult ==DeleteStatus.RejectedByNotExit || deleteResult == DeleteStatus.RejectedByNonEffectiveQuery)
                        {
                            var receiverUserId = msg.SenderUserId;
                            PermanentEventDispatcher.Instance.CreateEvent(PermanentEventActionTypeEnum.DeleteBeforeReadAck, PermanentEventEntityTypeEnum.Offer, msgId, receiverUserId, 1, string.Empty, null);
                            return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "message has  been deleted before read", "Sucess", string.Empty);
                        }
                        if (deleteResult == DeleteStatus.DeletedBeforeRead)
                        {
                            var receiverUserId = msg.RecieverUserId;
                            PermanentEventDispatcher.Instance.CreateEvent(PermanentEventActionTypeEnum.DeleteBeforeRead, PermanentEventEntityTypeEnum.Offer, msgId, receiverUserId, -1, string.Empty, null);
                            return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "message has  been deleted before read", "Sucess", string.Empty);

                        }
                        if (deleteResult == DeleteStatus.RejectedByAccessDenied || deleteResult == DeleteStatus.RejectedByInternalSystemError)
                        {
                            var receiverUserId = msg.SenderUserId;
                            //PermanentEventDispatcher.Instance.CreateEvent(PermanentEventActionTypeEnum.DeleteBeforeReadAck, PermanentEventEntityTypeEnum.Offer, msgId, receiverUserId, 0, string.Empty, null);
                            return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Empty, "Error, Access Denied,message has been delivered", "", string.Empty);
                        }
                    }
                }
                else
                {
                    //todo : security alert
                    return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Empty, "Error, Access Denied", "", string.Empty);

                }
                return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Empty, "Error, Please try later", "", string.Empty);
            }
            finally
            {

            }
        }

        public ActionResponseFromServerToClient SRpcDeleteOfferBeforeReadAck(MessageAckNotification notification, bool operationResult)
        {

            Guid msgId = Guid.Parse(notification.MessageId);
            //var msg = MessageEntityController.Instance.GetMessage(msgId);
            var receiverUserId = MembershipManager.Instance.GetUser(notification.UserName).UserId;

            if (operationResult)
            {

                PermanentEventDispatcher.Instance.CreateEvent(PermanentEventActionTypeEnum.DeleteBeforeReadAck, PermanentEventEntityTypeEnum.Offer, msgId, receiverUserId, 1, string.Empty, null);
                return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, string.Empty, "Sucess", string.Empty);
            }
            else
            {
                PermanentEventDispatcher.Instance.CreateEvent(PermanentEventActionTypeEnum.DeleteBeforeReadAck, PermanentEventEntityTypeEnum.Offer, msgId, receiverUserId, 0, string.Empty, null);
                return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, string.Empty, "Sucess", string.Empty);
            }

        }

        #endregion

        #region Contact
        public ActionResponseFromServerToClient SRpcDeleteContactForId(string userName, string contactId)
        {
            try
            {

                string actualUserName = conService.getUsernameOfConnection(this.Context.ConnectionId);
                if (actualUserName.Equals(userName))
                {
                    Guid contactEntityId;
                    if (!Guid.TryParse(contactId, out contactEntityId))
                        return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Empty, "Invalid parameter", "", string.Empty);

                    var deleteResult = FriendEntityController.Instance.DeleteFriendByFiendId(userName, contactEntityId);
                    if (deleteResult == DeleteStatus.Deleted || deleteResult == DeleteStatus.RejectedByNotExit || deleteResult == DeleteStatus.RejectedByNonEffectiveQuery)
                        return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, string.Empty, "Sucess", string.Empty);

                    if (deleteResult == DeleteStatus.RejectedByAccessDenied || deleteResult ==DeleteStatus.RejectedByInternalSystemError)
                        return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Empty, "Error, Access Denied", "", string.Empty);

                }
                else
                {
                    //todo : security alert
                    return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Empty, "Error, Access Denied", "", string.Empty);

                }
                return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Empty, "Error, Please try later", "", string.Empty);
            }
            finally
            {

            }
        }

        public ActionResponseFromServerToClient SRpcDeleteConversationFor(string userName, string otherUserName)
        {
            try
            {

                string actualUserName = conService.getUsernameOfConnection(this.Context.ConnectionId);
                if (actualUserName.Equals(userName))
                {

                    var deleteResult = MessageEntityController.Instance.DeleteMessagesFor(userName, otherUserName);
                    if (deleteResult == DeleteStatus.Deleted || deleteResult == DeleteStatus.RejectedByNotExit || deleteResult == DeleteStatus.RejectedByNonEffectiveQuery)
                        return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, string.Empty, "Sucess", string.Empty);

                    if (deleteResult == DeleteStatus.RejectedByAccessDenied || deleteResult == DeleteStatus.RejectedByInternalSystemError)
                        return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Empty, "Error, Access Denied", "", string.Empty);

                }
                else
                {
                    //todo : security alert
                    return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Empty, "Error, Access Denied", "", string.Empty);

                }
                return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Empty, "Error, Please try later", "", string.Empty);
            }
            finally
            {

            }
        }


        public ActionResponseFromServerToClient SRpcBlockContactFor(string userName, string contactId)
        {

            string actualUserName = conService.getUsernameOfConnection(this.Context.ConnectionId);
            if (actualUserName.Equals(userName))
            {
                Guid contactEntityId;
                if (Guid.TryParse(contactId, out contactEntityId))
                {
                    var result = FriendEntityController.Instance.BlockFriendByFiendId(userName, contactEntityId);
                    if (result == EditStatus.Edited || result == EditStatus.RejectedByNonEffectiveQuery)
                        return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "Sucess", string.Empty, string.Empty);

                }
                else
                    return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Empty, "Error, Invalid parameter", "", string.Empty);

            }
            else
            {
                //todo : security alert
                return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Empty, "Error, Access Denied", "", string.Empty);

            }

            return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Empty, "Error, Please try later", "", string.Empty);

        }
        public ActionResponseFromServerToClient SRpcUnBlockContactFor(string userName, string contactId)
        {

            string actualUserName = conService.getUsernameOfConnection(this.Context.ConnectionId);
            if (actualUserName.Equals(userName))
            {
                Guid contactEntityId;
                if (Guid.TryParse(contactId, out contactEntityId))
                {
                    var result = FriendEntityController.Instance.UnBlockFriendByFiendId(userName, contactEntityId);
                    if (result == EditStatus.Edited || result == EditStatus.RejectedByNonEffectiveQuery)
                        return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "Sucess", string.Empty, string.Empty);

                }
                else
                    return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Empty, "Error, Invalid parameter", "", string.Empty);

            }
            else
            {
                //todo : security alert
                return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Empty, "Error, Access Denied", "", string.Empty);

            }
            return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Empty, "Error, Please try later", "", string.Empty);

        }
        #endregion

        public void SRpcSyncClientToServer()
        {
            string userName = conService.getUsernameOfConnection(this.Context.ConnectionId);
            var userEntity=MembershipManager.Instance.GetUser(userName);
            if (userEntity != null)
            {
                IList<PermanentEvent> events = PermanentEventDispatcher.Instance.getEventsOfUser(userEntity.UserId);
                // to do , just take last FriendJoiningNotification and discard the olders that belongs to a specific user
                if (events != null)
                {
                    var resultList = new List<Offtick.Business.Services.RealtimeSearvices.EventDrivenDispatcher.Notification.ClientServerNotification>();
                    foreach (var ev in events)
                    {
                        var notification = ClientServerNotificationFactory.CreateClientServerNotification(ev);
                        if (notification != null && notification.NotificationObject!=null)
                            resultList.Add(notification);
                    }
                    if (resultList.Count > 0)
                        this.Clients.Client(this.Context.ConnectionId).cRpcSyncClientToServer(resultList);
                }
            }
            
        }
        public ActionResponseFromServerToClient SRpcEventProcessDone(List<string> eventIds)
        {
            if (eventIds!=null && eventIds.Count>0)
            {
                string userName=conService.getUsernameOfConnection(this.Context.ConnectionId);
                int count=PermanentEventDispatcher.Instance.MarkEventAsProcessedByClient(userName,eventIds);
                if (count > 0)
                    return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, string.Empty, null, string.Empty);
            }
            
            return null;
        }

        public ActionResponseFromServerToClient SRpcConnect(string userName, string deviceToken)
        {
            if (userName != null)
            {
                conService.Connect(userName, Context.ConnectionId,deviceToken);
                return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, string.Empty, null, string.Empty);
            }
            return new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "empty user name", string.Empty, null, string.Empty);
        }

        public void SRpcChangeRunningState(string userName,int state)
        {
            if(state>=0 && state<=3)
                conService.ChangeRunningState(userName, (ClientRunningState)state);
            
        }


        public void SRpcDisconnect(string userName)
        {
            if (userName != null)
            {
                conService.Disconnect(userName, Context.ConnectionId);
            }
           
        }


        public override System.Threading.Tasks.Task OnConnected()
        {

         
          

            return base.OnConnected();
        }
        public override System.Threading.Tasks.Task OnReconnected()
        {
            return base.OnReconnected();
            
            
        }
        

        

       
        public static  Microsoft.AspNet.SignalR.IHubContext GetCurrentChatService()
        {
            return Microsoft.AspNet.SignalR.GlobalHost.ConnectionManager.GetHubContext<ChatService>();
            
        }




       
    }
}
