﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.Services.RealtimeSearvices
{
    public enum ClientRunningState
    {
        None=0,
        Foreground=1,
        Background=2,
        Closed=3
    }

    public class ConnectionData
    {
        public string UserName { get; set; }
        public string ConnectionId { get; set; }
        public string DeviceToken { get; set; }
        public ClientRunningState RunningState { get; set; }
        public Nullable<DateTime> LastActivity { get; set; }

        public string GetLastActivityString()
        {
            if (!this.LastActivity.HasValue)
                return "";

            TimeSpan timeSpan = DateTime.Now.Subtract(this.LastActivity.Value);

            if ((RunningState == ClientRunningState.Foreground || RunningState== ClientRunningState.Foreground)&& (timeSpan.Days==0 && timeSpan.Hours==0 && timeSpan.Minutes<5))
                return "online";

            
            if (timeSpan.Days == 0)
            {
                if (timeSpan.Hours == 0)
                {
                    if (timeSpan.Minutes < 5)
                        return string.Format("online {0} minutes ago", timeSpan.Minutes);
                    else
                        return string.Format("online {0} minutes ago", timeSpan.Minutes);
                }
                if(timeSpan.Hours==1)
                {
                    return string.Format("online {0} minutes ago", timeSpan.Hours*60+timeSpan.Minutes);
                }
                return string.Format("online {0} hours ago", timeSpan.Hours);
            }
            return string.Format("online {0} days ago", timeSpan.Days);
            

        }
    }


    public class OnlineConnectionsService:IOnlineConnectionsService
    {
        
        private OnlineConnectionsService()
        {
            onlineConnections = new System.Collections.Concurrent.ConcurrentDictionary<string, ConnectionData>();
        }
        private static OnlineConnectionsService instance;
        private static object syncRoot = new Object();
        public static OnlineConnectionsService Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new OnlineConnectionsService();
                    }
                }

                return instance;
            }
        }





        

        public bool isOnlineByUserName(string userName)
        {
            if (this.onlineConnections.Keys.Contains(userName))
            {
                var data = onlineConnections[userName];
                return data!=null && !string.IsNullOrEmpty(data.ConnectionId);
            }
            else
                return false;
        }

        public bool isOnlineByConnectionId(string connectionId)
        {
            return !string.IsNullOrEmpty(getUsernameOfConnection(connectionId));
        }
        public string getConnectionIdOfUser(string userName)
        {
            if (isOnlineByUserName(userName))
                return onlineConnections[userName].ConnectionId;
            else
                return string.Empty;
        }

        public ConnectionData getDataOfUser(string userName)
        {
            if (isOnlineByUserName(userName))
                return onlineConnections[userName];
            else
                return null;

        }


        public string getUsernameOfConnection(string connectionId)
        {
            if (string.IsNullOrEmpty(connectionId))
                return string.Empty;
            var entity = onlineConnections.FirstOrDefault(e =>
            {
                if (e.Value.ConnectionId.Equals(connectionId))
                    return true;
                else
                    return false;
            });
            return entity.Key;
        }

        public void Disconnect(string userName, string connectionId)
        {
            if (isOnlineByUserName(userName))
            {
               // ConnectionData data = null;
              //  onlineConnections.TryRemove(userName, out data);
                var dataOfConnection=getDataOfUser(userName);
                if (dataOfConnection != null)
                {
                    dataOfConnection.LastActivity = DateTime.Now;
                }
            }

        }

        public void Connect(string userName, string connectionId,string deviceToken)
        {
            if (!this.onlineConnections.Keys.Contains(userName))
            {
                onlineConnections.TryAdd(userName, new ConnectionData { ConnectionId = connectionId, DeviceToken = deviceToken, UserName = userName });
            }
            else
            {
                //reconnect to do 
                onlineConnections[userName].ConnectionId = connectionId;
                if (!string.IsNullOrEmpty(deviceToken))
                {
                    onlineConnections[userName].DeviceToken = deviceToken;
                }
            }
            var dataOfConnection= onlineConnections[userName];
            dataOfConnection.RunningState = ClientRunningState.Foreground;
            UpdateLastActivity(dataOfConnection);
            
        }

        public void Reconnection(string username, string connectionId)
        {
            Connect(username, connectionId,string.Empty);
        }

        public void ChangeRunningState(string userName, ClientRunningState runningState)
        {
            if (this.onlineConnections.ContainsKey(userName))
            {
                var data = this.onlineConnections[userName];
                UpdateLastActivity(data);
                if (data != null)
                {
                    data.RunningState = runningState;
                }
                if (runningState == ClientRunningState.Closed)
                    this.Disconnect(userName, string.Empty);
                
            }
        }


        public void UpdateLastActivityByUserName(string userName)
        {
            var dataOfConnection = this.getDataOfUser(userName);
            UpdateLastActivity(dataOfConnection);
        }
        public void UpdateLastActivityByConnectionId(string connectionId)
        {
            throw new NotImplementedException();
        }
        private void UpdateLastActivity(ConnectionData connectionData)
        {
            if (connectionData != null)
            {
                connectionData.LastActivity = DateTime.Now;
            }
        }

        



        private System.Collections.Concurrent.ConcurrentDictionary<string, ConnectionData> onlineConnections;


       
    }
}
