﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewayService
{
    public interface IPaeymentGatewayLogger
    {
        int AddLog(long? OrderId, long? SaleReferenceId, string refId, short? ActionResultCode, short? ActionType, Guid? UserId, string Description);
        //void AddLog(long? OrderId, long? SaleOrderId, long? SaleReferenceId, string refId, short? ActionResultCode, short? ActionType, Guid? UserId, string Description);
        IList<AccountPgwLog> GetLogs(long orderId);
        IList<AccountPgwLog> GetLogs(string refId);
        IList<AccountPgwLog> GetLogs(Guid UserId, int take, int skip);

    }

}
