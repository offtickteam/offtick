﻿using Offtick.Core.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewayService
{
    public delegate void PaymentGatewayPaySuccessAction(long orderId, string refId);

    public delegate void PaymentGatewaySuccessAction(long orderId);
    public delegate void PaymentGatewayFailedAction(long orderId,int responseCode,string exeptionMessage);
    public delegate void PaymentGatewayComebackAction(string refId, short resCode, long? saleOrderId, long? saleReferenceId);


    public class PaymentGatewayEndpoint
    {
        public string pgwSite{ get;private set;}
        public string callBackUrl{ get;private set;}
        public Int64 terminalId{ get;private set;}
        public string userName{ get;private set;}
        public string userPassword{ get;private set;}

        private static object lockObject = new object();
        static short recycleCounter = 0;
        IPaeymentGatewayLogger logger;



        public PaymentGatewayEndpoint()
        {
            var configManager = ConfigFactory.GetConfigManager(ConfigManagerType.WebConfig);
            this.pgwSite = configManager.ReadSetting("pgwSite");
            this.callBackUrl = configManager.ReadSetting("callBackUrl");
            this.terminalId =long.Parse( configManager.ReadSetting("terminalId"));
            this.userName = configManager.ReadSetting("userName");
            this.userPassword = configManager.ReadSetting("userPassword");
            logger = new PaymentGatewayLogger();
        }
        public PaymentGatewayEndpoint(string pgwSite, string callBackUrl, Int64 terminalId, string userName, string userPassword)
        {
            this.pgwSite = pgwSite;
            this.callBackUrl = callBackUrl;
            this.terminalId = terminalId;
            this.userName = userName;
            this.userPassword = userPassword;
        }

        


    

        public void ComebackFromPaymentGateway(PaymentGatewayComebackAction comebackAction, Guid? userId, bool doVerify, bool doReversal)
        {
            BypassCertificateError();
            var Request=System.Web.HttpContext.Current.Request;
           string refId = Request.Params["RefId"];
           string resCode = Request.Params["ResCode"];
           string saleOrderId = Request.Params["SaleOrderId"];
           string saleReferenceId = Request.Params["SaleReferenceId"];

            short intResCode=-1;
            long longSaleOrderId;
            long longSaleReferenceId;

           bool hasResCode= short.TryParse(resCode,out intResCode);
           intResCode = hasResCode ? intResCode : (short)-1;
            bool hasOrderId = long.TryParse(saleOrderId, out longSaleOrderId);
            bool hasReferenceId = long.TryParse(saleReferenceId, out longSaleReferenceId);

          


            logger.AddLog(hasOrderId ? (long?)longSaleOrderId : null
                            , hasReferenceId ? (long?)longSaleReferenceId : null
                            , refId
                            ,hasResCode? (short?)intResCode:null
                            , (short?)PaymentGatewayActionType.ComebackFromPaymentGateway
                            , userId
                            , string.Empty);

            if (intResCode == 0 && doVerify)
            {
                this.VerifyRequest(userId, longSaleOrderId, longSaleOrderId, longSaleReferenceId, null, null);
            }
            if (intResCode != 0 && doReversal)
            {
                this.ReversalRequest(userId, longSaleOrderId, longSaleOrderId, longSaleReferenceId, null, null);
            }

           if (comebackAction != null)
               comebackAction(refId, intResCode, hasOrderId ? (long?)longSaleOrderId : null, hasReferenceId ? (long?)longSaleReferenceId : null);

        }

        public long CreateNewOrderId()
        {
         
            lock(lockObject){
                recycleCounter = (short)((recycleCounter + (short)1) % 100);
                string dateTime = string.Format("{0}{1}", DateTime.Now.ToString("yyyyMMddHHmmssfff"),recycleCounter);
                long orderId = long.Parse(dateTime);
                return orderId;
            }
        }



        public void PayRequest(Guid? userId, long orderId, long amount, string date, string time, string additionalData, long payerId, PaymentGatewayPaySuccessAction successAction, PaymentGatewayFailedAction failAction)
        {
            
            try
            {
                
                string result;
                BypassCertificateError();
                BPService.PaymentGatewayClient bpService = new BPService.PaymentGatewayClient();
                result = bpService.bpPayRequest(terminalId,
                              userName,
                              userPassword,
                              orderId,
                              amount,
                              date,
                              time,
                              additionalData,
                              callBackUrl,
                              payerId.ToString());//to do : this line changed because ot change on BPService and must be chacked

                String[] resultArray = result.Split(',');
                string responseCode = resultArray[0];

                string refId = string.Empty;

                short intResponseCode = -1;
                short.TryParse(responseCode, out intResponseCode);

                if (intResponseCode == 0)
                {
                   // if (successAction != null)
                   // {
                        if (resultArray.Length == 2)
                        {
                            refId = resultArray[1];
                            successAction(orderId, refId);
                        }
                        else
                        {
                            failAction(orderId,-1,string.Format("unexpected length, array length:{0}, refID:{1}", resultArray[1]));
                        }
                   // }
                }
                else
                {

                  //  if (failAction != null)
                        failAction(orderId, intResponseCode,string.Empty);
                }

                logger.AddLog(orderId
                  , null
                  , refId
                  , intResponseCode
                  , (short?)PaymentGatewayActionType.RequestPayment
                  , userId
                  , "Exception");
            }
            catch (Exception ex)
            {
                logger.AddLog(orderId
                    , null
                    , null
                    , null
                    , (short?)PaymentGatewayActionType.RequestPayment
                    , userId
                    , "Exception:" + ex.Message.Substring(0,Math.Min(180,ex.Message.Length)));

               //if (failAction != null)
                    failAction(orderId, -1,ex.Message);
            }
            
        }



        public void VerifyRequest(Guid? userId, long verifyOrderId, long verifySaleOrderId, long verifySaleReferenceId, PaymentGatewaySuccessAction successAction, PaymentGatewayFailedAction failAction)
        {
            try
            {
                BypassCertificateError();
                string result;



                BPService.PaymentGatewayClient bpService = new BPService.PaymentGatewayClient();
                result = bpService.bpVerifyRequest(this.terminalId,
                    this.userName,
                    this.userPassword,
                    verifyOrderId,
                    verifySaleOrderId,
                    verifySaleReferenceId);

                int intResponseCode = -1;
                int.TryParse(result, out intResponseCode);


                logger.AddLog(verifyOrderId
                            , verifySaleReferenceId
                            , null
                            , (short?)intResponseCode
                            , (short?)PaymentGatewayActionType.VerifyRequest
                            , userId
                            , string.Empty);


                if (intResponseCode == 0)
                {
                    if (successAction != null)
                        successAction(verifyOrderId);
                }
                else
                {

                    if (failAction != null)
                        failAction(verifyOrderId, intResponseCode,string.Empty);
                }
            }
            catch (Exception exp)
            {
                logger.AddLog(verifyOrderId
                       , verifySaleReferenceId
                       , null
                       , null
                       , (short?)PaymentGatewayActionType.VerifyRequest
                       , userId
                       , "Exception");
                if (failAction != null)
                    failAction(verifyOrderId, -1,exp.Message);
            }
        }


        public void InquiryRequest(Guid? userId, long orderId, long saleOrderId, long saleReferenceId, PaymentGatewaySuccessAction successAction, PaymentGatewayFailedAction failAction)
        {
            try
            {
                BypassCertificateError();
                string result;


                BPService.PaymentGatewayClient bpService = new BPService.PaymentGatewayClient();
                result = bpService.bpInquiryRequest(this.terminalId,
                   this.userName,
                    this.userPassword,
                     orderId,
                    saleOrderId,
                    saleReferenceId);
                int intResponseCode = -1;
                int.TryParse(result, out intResponseCode);

                if (intResponseCode == 0 || intResponseCode==45)
                {
                    if (successAction != null)
                        successAction(orderId);
                }
                else
                {

                    if (failAction != null)
                        failAction(orderId, intResponseCode,string.Empty);
                }
            }
            catch (Exception exp)
            {
                if (failAction != null)
                    failAction(orderId, -1,exp.Message);
            }
        }


        public void ReversalRequest(Guid? userId, long orderId, long saleOrderId, long saleReferenceId, PaymentGatewaySuccessAction successAction, PaymentGatewayFailedAction failAction)
        {
            try
            {
                BypassCertificateError();
                string result;



                BPService.PaymentGatewayClient bpService = new BPService.PaymentGatewayClient();
                result = bpService.bpReversalRequest(this.terminalId,
                   this.userName,
                    this.userPassword,
                       orderId,
                    saleOrderId,
                    saleReferenceId);
                int intResponseCode = -1;
                int.TryParse(result, out intResponseCode);

                logger.AddLog(orderId
                         , saleReferenceId
                         , null
                         , (short?)intResponseCode
                         , (short?)PaymentGatewayActionType.ReversalRequest
                         , userId
                         , string.Empty);

                if (intResponseCode == 0)
                {
                    if (successAction != null)
                        successAction(orderId);
                }
                else
                {

                    if (failAction != null)
                        failAction(orderId, intResponseCode,string.Empty);
                }
            }
            catch (Exception exp)
            {
                logger.AddLog(orderId
                       , saleReferenceId
                       , null
                       , null
                       , (short?)PaymentGatewayActionType.ReversalRequest
                       , userId
                       , "Exception");

                if (failAction != null)
                    failAction(orderId, -1,exp.Message);
            }
        }


        public void SettleRequest(Guid? userId, long orderId, long saleOrderId, long saleReferenceId, PaymentGatewaySuccessAction successAction, PaymentGatewayFailedAction failAction)
        {
            try
            {
                BypassCertificateError();
                string result;



                BPService.PaymentGatewayClient bpService = new BPService.PaymentGatewayClient();
                result = bpService.bpSettleRequest(this.terminalId,
                   this.userName,
                    this.userPassword,
                       orderId,
                    saleOrderId,
                    saleReferenceId);
                int intResponseCode = -1;
                int.TryParse(result, out intResponseCode);

                if (intResponseCode == 0)
                {
                    if (successAction != null)
                        successAction(orderId);
                }
                else
                {

                    if (failAction != null)
                        failAction(orderId, intResponseCode,string.Empty);
                }
            }
            catch (Exception exp)
            {
                if (failAction != null)
                    failAction(orderId, -1,exp.Message);
            }
        }


        void BypassCertificateError()
        {
            ServicePointManager.ServerCertificateValidationCallback +=
                delegate(
                    Object sender1,
                    X509Certificate certificate,
                    X509Chain chain,
                    SslPolicyErrors sslPolicyErrors)
                {
                    return true;
                };
        }

        public string ParseResponseCode(int responseCode)
        {
            string responseCodeAppSettingKey = string.Format("responseCode{0}", responseCode);
            var configManager = ConfigFactory.GetConfigManager(ConfigManagerType.WebConfig);
            string response= configManager.ReadSetting(responseCodeAppSettingKey);
            return response;


        }

        
    }
}
