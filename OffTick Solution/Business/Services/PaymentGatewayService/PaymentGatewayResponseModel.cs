﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewayService
{
    public class PaymentGatewayResponseModel
    {
        public string RefId{get;set;}
        public short ResCode{get;set;}
        public long? SaleReferenceId {get;set;}
        public long? SaleOrderId { get; set; } 
    }
}
