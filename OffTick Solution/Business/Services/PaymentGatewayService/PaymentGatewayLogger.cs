﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewayService
{
    public class PaymentGatewayLogger:IPaeymentGatewayLogger
    {

        public int AddLog(long? orderId,  long? saleReferenceId, string refId, short? actionResultCode, short? actionType, Guid? userId, string description)
        {
            OffTickDBEntities entities = new OffTickDBEntities();
            entities.AccountPgwLogs.Add(new AccountPgwLog()
            {
                OrderId = orderId,
                ActionResultCode = actionResultCode,
                ActionType = actionType,
                Datetime = DateTime.Now,
                Description = description,
                RefId = refId,
                SaleReferenceId = saleReferenceId,
                UserId = userId
            });
            return entities.SaveChanges();
        }

        public IList<AccountPgwLog> GetLogs(long orderId)
        {
            OffTickDBEntities entities = new OffTickDBEntities();
            return entities.AccountPgwLogs.Where(e => e.OrderId.Equals(orderId)).ToList();
        }

        public IList<AccountPgwLog> GetLogs(string refId)
        {
            OffTickDBEntities entities = new OffTickDBEntities();
            return entities.AccountPgwLogs.Where(e => e.RefId.Equals(refId)).ToList();
        }

        public IList<AccountPgwLog> GetLogs(Guid UserId, int take, int skip)
        {
            OffTickDBEntities entities = new OffTickDBEntities();
            return entities.AccountPgwLogs.Where(e => e.UserId.Equals(UserId)).Skip(skip).Take(take).ToList();
        }
    }
}
