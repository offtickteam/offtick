﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewayService
{
    public enum PaymentGatewayActionType
    {
        RequestPayment=1,
        ComebackFromPaymentGateway=2,
        VerifyRequest = 3,
        SettleRequest = 4,
        InqueryRequest = 5,
        ReversalRequest = 6,
        

    }
}
