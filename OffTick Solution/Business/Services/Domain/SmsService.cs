﻿using Offtick.Business.SmsService;
using Offtick.Core.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.Services.Domain
{
    public class SmsService
    {
            private static SmsService instance;
        public static SmsService Instance
        {
            get
            {
                if (instance != null)
                    return instance;
                lock (typeof(SubsciptionService))
                {
                    if (instance == null)
                        instance = new SmsService();
                    return instance;
                }
            }
        }

        private SmsService()
        {
            
        }

        public long SendMagfaSms(string endPoint, string domainName, string userName, string password, string sender, string receipt, string text)
        {
            IMagfaSmsHandler smsHandler = new MagfaSmsHttpHandler(endPoint, sender, userName, password, domainName);
            return smsHandler.enqueue( receipt, text, "UTF-8");
        }

        public long SendMagfaSms(string receipt, string text)
        {
            var configManager = ConfigFactory.GetConfigManager(ConfigManagerType.WebConfig);

            var smsMagfaEndpint = configManager.ReadSetting("smsMagfaEndpint");
            var smsMagfaSender = configManager.ReadSetting("smsMagfaSender");
            var smsMagfaUserName = configManager.ReadSetting("smsMagfaUserName");
            var smsMagfaPassword = configManager.ReadSetting("smsMagfaPassword");
            var smsMagfaDomain = configManager.ReadSetting("smsMagfaDomain");
            return SendMagfaSms(smsMagfaEndpint, smsMagfaDomain, smsMagfaUserName, smsMagfaPassword, smsMagfaSender, receipt, text);
        }
    }
}
