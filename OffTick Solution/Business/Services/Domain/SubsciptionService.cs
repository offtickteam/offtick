﻿using Offtick.Business.Services.Storage;
using Offtick.Business.Web;
using Offtick.Core.EnumTypes;
using Offtick.Data.Context.ExpertOnlinerContexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.Services.Domain
{
    public class SubsciptionService
    {
         private static SubsciptionService instance;
        public static SubsciptionService Instance
        {
            get
            {
                if (instance != null)
                    return instance;
                lock (typeof(SubsciptionService))
                {
                    if (instance == null)
                        instance = new SubsciptionService();
                    return instance;
                }
            }
        }

        private SubsciptionService()
        {
            
        }

        public AddStatus AddUserToSubsciptionService(string userId, string cityId)
        {
            Guid userGuidId;
            if (!Guid.TryParse(userId, out userGuidId))
                return AddStatus.RejectedByInvalidParameter;

            Guid cityGuidId;
            if (!Guid.TryParse(cityId, out cityGuidId))
                return AddStatus.RejectedByInvalidParameter;

            var user = Membership.MembershipManager.Instance.GetUser(userGuidId);
            var city = Membership.MembershipManager.Instance.GetCity(cityGuidId);

            return AddUserToSubsciptionService(user, city);
            

        }

        public AddStatus AddUserToSubsciptionService(MembershipUser user, MembershipCity city)
        {
            if (user == null || city == null)
                return AddStatus.RejectedByInvalidParameter;

            var exitedInterest=user.SubscriptionInterests.FirstOrDefault(e=>e.CityId==city.CityId);
            if (exitedInterest != null)
                return AddStatus.RejectedByExitBefore;

            var db = DataContextManager.GetPersitantStorage();
            db.SubscriptionInterests.Add(new SubscriptionInterest()
            {
                MembershipCity = city,
                MembershipUser = user,
                Datetime = DateTime.Now,
            });
            int effected = db.SaveChanges();
            if (effected > 0)
                return AddStatus.Added;
            else
                return AddStatus.RejectedByNonEffectiveQuery;

        }


        public DeleteStatus RemoveUserFromSubsciptionService(string userId, string cityId)
        {
            Guid userGuidId;
            if (!Guid.TryParse(userId, out userGuidId))
                return DeleteStatus.RejectedByInvalidParameter;

            Guid cityGuidId;
            if (!Guid.TryParse(cityId, out cityGuidId))
                return DeleteStatus.RejectedByInvalidParameter;

            var user = Membership.MembershipManager.Instance.GetUser(userGuidId);
            var city = Membership.MembershipManager.Instance.GetCity(cityGuidId);

            return RemoveUserFromSubsciptionService(user, city);


        }

        public DeleteStatus RemoveUserFromSubsciptionService(MembershipUser user, MembershipCity city)
        {
            if (user == null || city == null)
                return DeleteStatus.RejectedByInvalidParameter;

            var exitedInterest = user.SubscriptionInterests.FirstOrDefault(e => e.CityId == city.CityId);
            if (exitedInterest == null)
                return DeleteStatus.RejectedByNotExit;

            var db = DataContextManager.GetPersitantStorage();
            db.SubscriptionInterests.Remove(exitedInterest);
            int effected = db.SaveChanges();
            if (effected > 0)
                return DeleteStatus.Deleted;
            else
                return DeleteStatus.RejectedByNonEffectiveQuery;

        }


        public IList<MembershipUser> ListofSubscriberForCity(string cityId)
        {
            Guid cityGuidId;
            if (!Guid.TryParse(cityId, out cityGuidId))
                return null;
             var city = Membership.MembershipManager.Instance.GetCity(cityGuidId);

             return ListofSubscriberForCity(city);
        }

        public IList<MembershipCity> ListofCities()
        {
            var db = DataContextManager.GetPersitantStorage();
            return db.MembershipCities.ToList();

        }
        public IList<MembershipUser> ListofSubscriberForCity(MembershipCity city)
        {
            if (city == null)
                return null;
            var db = DataContextManager.GetPersitantStorage();
            return db.SubscriptionInterests.Where(e => e.CityId == city.CityId).Select(e => e.MembershipUser).ToList();
        }


        public async Task SubscribeOffers(IList<Offer> offers, MembershipCity city)
        {
            Action action = () =>
            {
                var subscribers = this.ListofSubscriberForCity(city);
                IList<string> emails = subscribers.Select(e => e.Email).ToList();
                SMTPService smtpService = new SMTPService();
                string bodyOfEmail = "";//create body from offers
                IList<string> chunck10Emails = new List<string>();
                for (int j = 0; j < emails.Count;j++ )
                {
                    string emailAddr = emails[j];
                    chunck10Emails.Add(emailAddr);
                    if (j % 10 == 0 || j==emails.Count-1)
                    {
                        if (chunck10Emails != null && chunck10Emails.Count != 0)
                            smtpService.SendMail(chunck10Emails, "New Offers", bodyOfEmail);
                    }
                    chunck10Emails.Clear();
                }
            };
            await Task.Run(action);
        }
    }
}
