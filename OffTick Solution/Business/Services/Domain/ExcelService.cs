﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Offtick.Business.Services.Domain
{
    public class ExcelService
    {
        public static void ExportToExcel1<T>(List<T> lsObject, string fileName)
        {


            System.Web.UI.WebControls.DataGrid dg = new System.Web.UI.WebControls.DataGrid();
            dg.AllowPaging = false;
            dg.DataSource = lsObject;
            dg.DataBind();

            System.Web.HttpContext.Current.Response.Clear();
            System.Web.HttpContext.Current.Response.Buffer = true;
            System.Web.HttpContext.Current.Response.ContentEncoding = Encoding.UTF8;
            System.Web.HttpContext.Current.Response.Charset = "";
            System.Web.HttpContext.Current.Response.AddHeader("Content-Disposition",
              "attachment; filename=" + fileName);

            System.Web.HttpContext.Current.Response.ContentType =
              "application/vnd.ms-excel";
            System.IO.StringWriter stringWriter = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter htmlTextWriter =
              new System.Web.UI.HtmlTextWriter(stringWriter);
            dg.RenderControl(htmlTextWriter);
            System.Web.HttpContext.Current.Response.Write(stringWriter.ToString());
            System.Web.HttpContext.Current.Response.End();
        }


        public static FileContentResult ExportToExcel<T>(List<T> lsObject, string fileName)
        {


            System.Web.UI.WebControls.DataGrid dg = new System.Web.UI.WebControls.DataGrid();
            dg.AllowPaging = false;
            dg.DataSource = lsObject;
            dg.DataBind();

            //System.Web.HttpContext.Current.Response.Clear();
            //System.Web.HttpContext.Current.Response.Buffer = true;
            //System.Web.HttpContext.Current.Response.ContentEncoding = Encoding.UTF8;
            //System.Web.HttpContext.Current.Response.Charset = "";
            //System.Web.HttpContext.Current.Response.AddHeader("Content-Disposition",              "attachment; filename=" + fileName);

            //System.Web.HttpContext.Current.Response.ContentType =
            //  "application/vnd.ms-excel";
            System.Web.HttpContext.Current.Response.ContentEncoding = Encoding.UTF8;
            System.IO.StringWriter stringWriter = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter htmlTextWriter =
              new System.Web.UI.HtmlTextWriter(stringWriter);
            dg.RenderControl(htmlTextWriter);


            var bytesOfExcel = System.Text.Encoding.UTF8.GetBytes(stringWriter.ToString());
            System.Web.HttpContext.Current.Response.AppendHeader("content-disposition", "inline; filename=" + fileName);

            // Return the output stream
            return new FileContentResult(bytesOfExcel, "application/vnd.ms-excel");
        }

        public static byte[] GetBytes<T>(IList<T> lsObject)
        {
            System.Web.UI.WebControls.DataGrid dg = new System.Web.UI.WebControls.DataGrid();
            dg.AllowPaging = false;
            dg.DataSource = lsObject;
            dg.DataBind();

            
            System.IO.StringWriter stringWriter = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter htmlTextWriter = new System.Web.UI.HtmlTextWriter(stringWriter);
            dg.RenderControl(htmlTextWriter);
            var bytes = System.Text.Encoding.UTF8.GetBytes(stringWriter.ToString());
            return bytes;
        }

    }
}
