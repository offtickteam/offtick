﻿using Offtick.Business.Services.Storage.File;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.Services.Domain.PdfGenerator
{
    public class ServiceOrProductItem
    {
        public int rowNo{get;set;}
        public string itemCode{get;set;}
        public string name{get;set;}
        public int count{get;set;}
        public long itemPrice{get;set;}
        public long totalPrice{get;set;}
        public long discountPrice{get;set;}
        public long totalPriceAfterDiscount{get;set;}
        public long taxes{get;set;}
        public long totalPriceAfterDiscountAndTaxes{get;set;}

    }
    public class PdfService
    {
        //https://www.hesabfa.com/Help/%D9%81%D8%A7%DA%A9%D8%AA%D9%88%D8%B1-%D8%B1%D8%B3%D9%85%DB%8C-%D9%85%D9%88%D8%B1%D8%AF-%D8%AA%D8%A7%DB%8C%DB%8C%D8%AF-%D8%AF%D8%A7%D8%B1%D8%A7%DB%8C%DB%8C
        //http://faktorprint.com/2015/09/%D9%81%D8%A7%DA%A9%D8%AA%D9%88%D8%B1-%D9%81%D8%B1%D9%88%D8%B4-%D8%B1%D8%B3%D9%85%DB%8C-%D9%85%D9%88%D8%B1%D8%AF-%D8%AA%D8%A7%DB%8C%DB%8C%D8%AF-%D8%AF%D8%A7%D8%B1%D8%A7%DB%8C%DB%8C/
        private string rowNoCaption;
        private string itemCodeCaption;
        private string nameCaption;
        private string countCaption;
        private string itemPriceCaption;
        private string totalPriceCaption;
        private string discountPriceCaption;
        private string totalPriceAfterDiscountCaption;
        private string taxesCaption;
        private string totalPriceAfterDiscountAndTaxesCaption;


        public PdfService(
            string rowNoCaption,
            string itemCodeCaption,
            string nameCaption,
            string countCaption,
           string itemPriceCaption,
           string totalPriceCaption,
           string discountPriceCaption,
           string totalPriceAfterDiscountCaption,
           string taxesCaption,
           string totalPriceAfterDiscountAndTaxesCaption
            )
        {
            this.rowNoCaption = rowNoCaption;
            this.itemCodeCaption = itemCodeCaption;
            this.nameCaption = nameCaption;
            this.countCaption = countCaption;
            this.itemPriceCaption = itemPriceCaption;
            this.totalPriceCaption = totalPriceCaption;
            this.discountPriceCaption = discountPriceCaption;
            this.totalPriceAfterDiscountCaption = totalPriceAfterDiscountCaption;
            this.taxesCaption = taxesCaption;
            this.totalPriceAfterDiscountAndTaxesCaption = totalPriceAfterDiscountAndTaxesCaption;
        }                                                                                            


        public string CreatePdf(string userId,string title,string offtickCode,string reciverName,string value,string price,string validityFrom,string validityTo,string address,string phone,string imageFileName,string condition,string property,string saleOrderId,string saleRefrenceId)
        {
            PdfFileManager pdfFileManager = new PdfFileManager(userId);
            string pdfFileName = string.Format("{0}{1}{2}",
               System.IO.Path.GetFileNameWithoutExtension(userId + offtickCode),
               DateTime.Now.ToString("yyyyMMddHHmmss"),
               System.IO.Path.GetExtension(".pdf"));
            string fileName = pdfFileManager.GetFullFileName(pdfFileName);
            
            var doc1 = new Document();
            var pdfWriter=PdfWriter.GetInstance(doc1, new FileStream(fileName, FileMode.Create));
            doc1.Open();

            doc1.AddLanguage("fa-IR");




           // BaseFont basefont = BaseFont.CreateFont(BaseFont.TIMES_ITALIC, BaseFont.IDENTITY_H, true);
            BaseFont basefont = BaseFont.CreateFont(Environment.GetEnvironmentVariable("windir") + @"\fonts\tahoma.ttf", BaseFont.IDENTITY_H, true);
           
            Font myFont = new Font(basefont, 14, iTextSharp.text.Font.NORMAL, iTextSharp.text.BaseColor.BLUE);
            Font myFont2 = new Font(basefont, 12, iTextSharp.text.Font.NORMAL, iTextSharp.text.BaseColor.BLACK);

            PdfPTable t = new PdfPTable(1);
            t.RunDirection = PdfWriter.RUN_DIRECTION_RTL;

            Paragraph p = new Paragraph("آفتیک", myFont);
            p.Alignment = Element.ALIGN_RIGHT;
            t.AddCell(p);



            p = new Paragraph(title, myFont);
            p.Alignment = Element.ALIGN_RIGHT;
            t.AddCell(p);
            t.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
            doc1.Add(t);

            doc1.Add(new Paragraph(" "));
            doc1.Add(new Chunk("\n"));
            


            // Add a horizontal line below the headig text and add it to the paragraph
            //Paragraph paragraph;
            //paragraph = new Paragraph("");
            //iTextSharp.text.pdf.draw.VerticalPositionMark seperator = new iTextSharp.text.pdf.draw.LineSeparator();
            //seperator.Offset = -6f;
            //paragraph.Add(seperator);
            //doc1.Add(paragraph);


            PdfPTable table = new PdfPTable(3);
            table.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
            
            PdfPCell cellLeft = File.Exists(imageFileName)? new PdfPCell(Image.GetInstance(imageFileName)): new PdfPCell();
            cellLeft.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            cellLeft.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
            cellLeft.Colspan = 1;
            table.AddCell(cellLeft);

            PdfPTable tInfo = new PdfPTable(1);
            tInfo.RunDirection = PdfWriter.RUN_DIRECTION_RTL;

            p = new Paragraph(new Phrase(string.Format("دریافت کننده:{0}", reciverName), myFont2));
            p.Alignment = Element.ALIGN_RIGHT;
            tInfo.AddCell(p);

           // tInfo.AddCell(new Phrase(string.Format(" نام:{0}", firstName), myFont2));
           // tInfo.AddCell(new Phrase(string.Format(" فامیلی:{0}", lastName), myFont2));

            tInfo.AddCell(new Phrase(string.Format("شناسه خرید:{0}", saleOrderId), myFont2));
            tInfo.AddCell(new Phrase(string.Format(" شناسه پرداخت:{0}", saleRefrenceId), myFont2));


            tInfo.AddCell(new Phrase(string.Format("ارزش کوپن:{0}", value), myFont2));
            tInfo.AddCell(new Phrase(string.Format("قیمت با تخفیف:{0}", price), myFont2));
            tInfo.AddCell(new Phrase(string.Format(" مدت اعتبار:{0}-{1}", validityFrom, validityTo), myFont2));
            tInfo.AddCell(new Phrase(string.Format("آدرس:{0}", address), myFont2));
            tInfo.AddCell(new Phrase(string.Format("تلفن:{0}", phone), myFont2));
            PdfPCell cellRight = new PdfPCell(tInfo);
            cellRight.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            cellRight.Colspan = 2;
            table.AddCell(cellRight);
            
            doc1.Add(table);
            doc1.Add(new Chunk("\n"));
            doc1.Add(new Chunk("\n"));


            PdfPTable tSecond = new PdfPTable(2);
            tSecond.RunDirection = PdfWriter.RUN_DIRECTION_RTL;

            tSecond.AddCell(new Phrase(condition, myFont2));
            tSecond.AddCell(new Phrase(property, myFont2));
            doc1.Add(tSecond);

            doc1.Add(new Paragraph("هیچ هزینه دیگری غیر از هزینه پرداختی در این فاکتور اعم از هزینه حمل و تحویل کالا در محدود مشخص شده در جزییات اف تیک انتخاب شده، از خریدار اخذ نخواهد شد"));
            doc1.Add(new Chunk("\n"));
            doc1.Close();
            return pdfFileName;

        }

        public string CreatePdf(string userId, string factorCode,DateTime factorDate,
            string sellerEchonomicCode, string sellerNationalCode, string sellerPostalCode, string sellerRegisterCode,
            string sellerName,string sellerStae,string sellerCity,string sellerAddress,string sellerPhoneNumber,string sellerFax,
            string buyerName, string buyerStae, string buyerCity, string buyerAddress, string buyerPhoneNumber, string buyerFax,
            string title, string offtickCode, string reciverName, string value, string price, string validityFrom, string validityTo, string address, string phone, string imageFileName, string condition, string property)
        {
            PdfFileManager pdfFileManager = new PdfFileManager(userId);
            string pdfFileName = string.Format("{0}{1}{2}",
               System.IO.Path.GetFileNameWithoutExtension(userId + offtickCode),
               DateTime.Now.ToString("yyyyMMddHHmmss"),
               System.IO.Path.GetExtension(".pdf"));
            string fileName = pdfFileManager.GetFullFileName(pdfFileName);

            var doc1 = new Document();
            var pdfWriter = PdfWriter.GetInstance(doc1, new FileStream(fileName, FileMode.Create));
            doc1.Open();

            doc1.AddLanguage("fa-IR");




            // BaseFont basefont = BaseFont.CreateFont(BaseFont.TIMES_ITALIC, BaseFont.IDENTITY_H, true);
            BaseFont basefont = BaseFont.CreateFont(Environment.GetEnvironmentVariable("windir") + @"\fonts\tahoma.ttf", BaseFont.IDENTITY_H, true);

            Font myFont = new Font(basefont, 14, iTextSharp.text.Font.NORMAL, iTextSharp.text.BaseColor.BLUE);
            Font myFont2 = new Font(basefont, 12, iTextSharp.text.Font.NORMAL, iTextSharp.text.BaseColor.BLACK);

            PdfPTable t = new PdfPTable(1);
            t.RunDirection = PdfWriter.RUN_DIRECTION_RTL;

            Paragraph p = new Paragraph("آفتیک", myFont);
            p.Alignment = Element.ALIGN_RIGHT;
            t.AddCell(p);



            p = new Paragraph(title, myFont);
            p.Alignment = Element.ALIGN_RIGHT;
            t.AddCell(p);
            t.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
            doc1.Add(t);

            doc1.Add(new Paragraph(" "));
            doc1.Add(new Chunk("\n"));



            // Add a horizontal line below the headig text and add it to the paragraph
            //Paragraph paragraph;
            //paragraph = new Paragraph("");
            //iTextSharp.text.pdf.draw.VerticalPositionMark seperator = new iTextSharp.text.pdf.draw.LineSeparator();
            //seperator.Offset = -6f;
            //paragraph.Add(seperator);
            //doc1.Add(paragraph);


            PdfPTable table = new PdfPTable(3);
            table.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
            PdfPCell cellLeft = new PdfPCell(Image.GetInstance(imageFileName));
            cellLeft.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            cellLeft.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
            cellLeft.Colspan = 1;
            table.AddCell(cellLeft);

            PdfPTable tInfo = new PdfPTable(1);
            tInfo.RunDirection = PdfWriter.RUN_DIRECTION_RTL;

            p = new Paragraph(new Phrase(string.Format("دریافت کننده:{0}", reciverName), myFont2));
            p.Alignment = Element.ALIGN_RIGHT;
            tInfo.AddCell(p);

            tInfo.AddCell(new Phrase(string.Format("ارزش کوپن:{0}", value), myFont2));
            tInfo.AddCell(new Phrase(string.Format("قیمت با تخفیف:{0}", price), myFont2));
            tInfo.AddCell(new Phrase(string.Format(" مدت اعتبار:{0}-{1}", validityFrom, validityTo), myFont2));
            tInfo.AddCell(new Phrase(string.Format("آدرس:{0}", address), myFont2));
            tInfo.AddCell(new Phrase(string.Format("تلفن:{0}", phone), myFont2));
            PdfPCell cellRight = new PdfPCell(tInfo);
            cellRight.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            cellRight.Colspan = 2;
            table.AddCell(cellRight);

            doc1.Add(table);
            doc1.Add(new Chunk("\n"));
            doc1.Add(new Chunk("\n"));


            PdfPTable tSecond = new PdfPTable(2);
            tSecond.RunDirection = PdfWriter.RUN_DIRECTION_RTL;

            tSecond.AddCell(new Phrase(condition, myFont2));
            tSecond.AddCell(new Phrase(property, myFont2));
            doc1.Add(tSecond);

            doc1.Add(new Paragraph("هیچ هزینه دیگری غیر از هزینه پرداختی در این فاکتور اعم از هزینه حمل و تحویل کالا در محدود مشخص شده در جزییات اف تیک انتخاب شده، از خریدار اخذ نخواهد شد"));
            doc1.Add(new Chunk("\n"));
            doc1.Close();
            return pdfFileName;

        }

    }
}

