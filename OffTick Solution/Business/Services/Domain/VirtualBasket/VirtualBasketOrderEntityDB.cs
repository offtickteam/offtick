﻿using Offtick.Data.Context.ExpertOnlinerContexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.Services.Domain.VirtualBasket
{
    public class VirtualBasketOrderEntityDB
    {

        public int Count { get; set; }
        public long EntityId { get; set; }
        public long EntityPrice { get; set; }
        public bool IsBuyingVisible { get; set; }

        //Start of presentation Fields,this field filled when user request fetch basket
        public string EntityName { get; set; }
        public int? DiscountPercent { get; set; }
        public long? TotalDiscount { get; set; }
        //end of presentation fields




        public long TotalPrice
        {
            get
            {
                return EntityPrice * Count;
            }
        }
    }
}
