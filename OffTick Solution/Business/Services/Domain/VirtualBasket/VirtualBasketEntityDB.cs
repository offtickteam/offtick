﻿using Offtick.Data.Context.ExpertOnlinerContexts;
using Newtonsoft.Json;
using PaymentGatewayService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.Services.Domain.VirtualBasket
{
    public class VirtualBasketEntityDB
    {
       
        public Guid UserId { get; set; }
        public string OrderId { get; set; }
        public IList<VirtualBasketOrderEntityDB> Entities;
        public PaymentGatewayResponseModel GatewayResponse { get; set; }


        public void SetGatewayResponse(string refId, short resCode, long? saleReferenceId, long? saleOrderId)
        {
            if (this.GatewayResponse == null)
                this.GatewayResponse = new PaymentGatewayResponseModel();
            this.GatewayResponse.RefId = refId;
            this.GatewayResponse.ResCode = resCode;
            this.GatewayResponse.SaleOrderId = saleOrderId;
            this.GatewayResponse.SaleReferenceId = saleReferenceId;
        }

        public void AddOrUpdateBasketOrder(long entityId, int count,bool? isBuyingVisible)
        {
            if (Entities == null)
                Entities = new List<VirtualBasketOrderEntityDB>();
            var existed = Entities.FirstOrDefault(e => e.EntityId.Equals(entityId));
            if (existed != null)
            {
                existed.Count = count;
                existed.IsBuyingVisible = isBuyingVisible.HasValue ? isBuyingVisible.Value : existed.IsBuyingVisible;
            }
            else
            {
                this.Entities.Add(new VirtualBasketOrderEntityDB()
                {
                    Count = count,
                    EntityId = entityId,
                    IsBuyingVisible=isBuyingVisible.HasValue ? isBuyingVisible.Value : true,
                });
            }
            
        }

        public void RemoveBasketOrder(OfferFood offer)
        {
            if (offer == null)
                return;
            var existed = Entities.FirstOrDefault(e => e.EntityId.Equals(offer.OfferFoodId));
            if (existed != null)
                this.Entities.Remove(existed);
        }
        public void RemoveBasketOrder(VirtualBasketOrderEntityDB basketEntity)
        {
            if (basketEntity == null)
                return;
            this.Entities.Remove(basketEntity);
        }

        public long BasketPrice
        {
            get
            {
                string s;
               
                long sum=0;
                if (Entities != null)
                    foreach (var entity in this.Entities)
                    {
                        sum += entity.TotalPrice;
                        var offtickOfferEntity =Offtick.Business.Services.Managers.OfferManager.Instance.GetOfferFood(entity.EntityId);
                        if (offtickOfferEntity != null && offtickOfferEntity.AddedValueTax.HasValue && offtickOfferEntity.AddedValueTax.Value > 0)
                        {
                            sum += (entity.TotalPrice * offtickOfferEntity.AddedValueTax.Value) / 100;
                        }
                    }
                return sum;
            }
        }


        public void JsonDeserialize( string toDeserialize)
        {
            this.Entities= JsonConvert.DeserializeObject<IList<VirtualBasketOrderEntityDB>>(toDeserialize);
        }

        public  string JsonSerialize( )
        {
            return JsonConvert.SerializeObject(Entities);
        }
    }
}
