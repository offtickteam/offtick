﻿using Offtick.Data.Context.ExpertOnlinerContexts;
using PaymentGatewayService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.Services.Domain.VirtualBasket
{
    public class VirtualBasketEntity
    {
       // public string SessionId { get; set; }
        public MembershipUser User { get; set; }
        public string OrderId { get; set; }
        public IList<VirtualBasketOrderEntity> Entities;
        public PaymentGatewayResponseModel GatewayResponse { get; set; }


        public void SetGatewayResponse(string refId, short resCode, long? saleReferenceId, long? saleOrderId)
        {
            if (this.GatewayResponse == null)
                this.GatewayResponse = new PaymentGatewayResponseModel();
            this.GatewayResponse.RefId = refId;
            this.GatewayResponse.ResCode = resCode;
            this.GatewayResponse.SaleOrderId = saleOrderId;
            this.GatewayResponse.SaleReferenceId = saleReferenceId;
        }

        public void AddOrUpdateBasketOrder(OfferFood offer, int count,bool? isBuyingVisible)
        {
            if (offer == null)
                return;

            if (Entities == null)
                Entities = new List<VirtualBasketOrderEntity>();
            var existed = Entities.FirstOrDefault(e => e.Entity.OfferFoodId.Equals(offer.OfferFoodId));
            if (existed != null)
            {
                existed.Count = count;
                existed.IsBuyingVisible = isBuyingVisible.HasValue ? isBuyingVisible.Value : existed.IsBuyingVisible;
            }
            else
            {
                this.Entities.Add(new VirtualBasketOrderEntity()
                {
                    Count = count,
                    Entity = offer,
                    IsBuyingVisible=isBuyingVisible.HasValue ? isBuyingVisible.Value : true,
                });
            }
            
        }

        public void RemoveBasketOrder(OfferFood offer)
        {
            if (offer == null)
                return;
            var existed = Entities.FirstOrDefault(e => e.Entity.OfferFoodId.Equals(offer.OfferFoodId));
            if (existed != null)
                this.Entities.Remove(existed);
        }
        public void RemoveBasketOrder(VirtualBasketOrderEntity basketEntity)
        {
            if (basketEntity == null)
                return;
            this.Entities.Remove(basketEntity);
        }

        public long BasketPrice
        {
            get
            {
                long sum=0;
                if (Entities != null)
                    foreach (var entity in this.Entities)
                    {
                        sum += entity.TotalPrice;
                        var offerEntity =Offtick.Business.Services.Managers.OfferManager.Instance.GetOffer(entity.Entity.OfferId);
                        var offtickOfferEntity = offerEntity.OfferFoods.FirstOrDefault();
                        if (offtickOfferEntity != null && offtickOfferEntity.AddedValueTax.HasValue && offtickOfferEntity.AddedValueTax.Value > 0)
                        {
                            sum += (entity.TotalPrice * offtickOfferEntity.AddedValueTax.Value) / 100;
                        }
                    }
                return sum;
            }
        }

       
    }
}
