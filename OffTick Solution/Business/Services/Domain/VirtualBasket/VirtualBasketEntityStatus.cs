﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.Services.Domain.VirtualBasket
{
    public enum VirtualBasketEntityStatus
    {
        Selecting,
        
        SentPayRequest,
        PayRequestSuccessed,
        PayRequestFailed,

        SendReversalRequest,
        ReversalRequestSuccessed,
        ReversalRequestFailed,

        SentVerifyRequest,
        VerifyRequestSuccessed,
        VerifyRequestFailed,
    }
}
