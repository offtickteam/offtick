﻿using Offtick.Data.Context.ExpertOnlinerContexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.Services.Domain.VirtualBasket
{
    public class VirtualBasketOrderEntity
    {

        public int Count { get; set; }
        public OfferFood Entity { get; set; }
        public bool IsBuyingVisible { get; set; }
        public long TotalPrice
        {
            get
            {
                if (Entity != null)
                {
                    return Entity.DiscountPrice * Count;
                }
                return 0;
            }
        }
    }
}
