﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.Services.Domain.VirtualBasket
{
    public class VirtualBasketManager
    {
        private static readonly string _sessionKey = "VirtualBasketEntity";
            private static VirtualBasketManager instance;

        public static VirtualBasketManager Instance
        {
            get
            {
                if (instance != null)
                    return instance;
                lock (typeof(VirtualBasketManager))
                {
                    if (instance == null)
                        instance = new VirtualBasketManager();
                    return instance;
                }
            }
        }


        private VirtualBasketManager()
        {
            
        }


        public void AddVirtualBasketEntity(VirtualBasketEntity entity)
        {
            if (entity == null)
                return;
            VirtualBasketEntity _exitesdEntity = (VirtualBasketEntity)System.Web.HttpContext.Current.Session[_sessionKey];
            if (_exitesdEntity == null)
            {
                System.Web.HttpContext.Current.Session[_sessionKey] = entity;
            }
            else
            {
                foreach (var item in entity.Entities)
                {
                    _exitesdEntity.AddOrUpdateBasketOrder(item.Entity, item.Count,item.IsBuyingVisible);
                }
                _exitesdEntity.OrderId = entity.OrderId;
            }
        }

        public void RemoveVirtualBasketEntity(string entityId)
        {
            if (string.IsNullOrEmpty(entityId))
                return;
            VirtualBasketEntity entity = (VirtualBasketEntity)System.Web.HttpContext.Current.Session[_sessionKey];
            Guid entityGuidId;
            if (Guid.TryParse(entityId,out entityGuidId))
            {
                var entityToRemove=entity.Entities.FirstOrDefault(e => e.Entity.OfferId.Equals(entityGuidId));
                entity.RemoveBasketOrder(entityToRemove);
              
            }
            
        }

        public void ChangeOrderIdOfVirtualBasketEntity(string newOrderId)
        {
            VirtualBasketEntity entity = (VirtualBasketEntity)System.Web.HttpContext.Current.Session[_sessionKey];
            if (entity != null)
            {
                entity.OrderId = newOrderId;
                
            }

        }

        public VirtualBasketEntity GetBasketEntitybyOrderId(string orderId)
        {
            if (string.IsNullOrEmpty(orderId))
                return null;
            IList<VirtualBasketEntity> entities = (IList<VirtualBasketEntity>)System.Web.HttpContext.Current.Session[_sessionKey];
            if (entities == null)
                return null;
            var existed = entities.FirstOrDefault(e => e.OrderId.Equals(orderId));
            return existed;
        }

        public VirtualBasketEntity GetListOfBasketEntitiesForCurrentUser()
        {
            VirtualBasketEntity entity = (VirtualBasketEntity)System.Web.HttpContext.Current.Session[_sessionKey];
            return entity;

        }

        public bool ProcessSaleDoneForCurrentUser()
        {
            VirtualBasketEntity entity = (VirtualBasketEntity)System.Web.HttpContext.Current.Session[_sessionKey];
            if (entity != null)
            {
                //must do another work??
                System.Web.HttpContext.Current.Session[_sessionKey] = null;
            }
            return false;
        }
    }
}
