﻿using Offtick.Business.Membership;
using Offtick.Business.Services.Managers;
using Offtick.Business.Services.Storage;
using Offtick.Data.Context.ExpertOnlinerContexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offtick.Business.Services.Domain.VirtualBasket
{
    public class VirtualBasketManagerDB : BaseManager
    {
        
        private static VirtualBasketManagerDB instance;

        public static VirtualBasketManagerDB Instance
        {
            get
            {
                if (instance != null)
                    return instance;
                lock (typeof(VirtualBasketManagerDB))
                {
                    if (instance == null)
                        instance = new VirtualBasketManagerDB();
                    return instance;
                }
            }
        }


        private VirtualBasketManagerDB()
        {
            
        }


        public void AddVirtualBasketEntity(MembershipUser user, VirtualBasketEntityDB entity)
        {
            if (entity == null)
                return;
            
            if (user == null || !user.UserId.Equals(entity.UserId))
                return;
            VirtualBasketEntityDB _exitesdEntity = entity;
            Offtick.Data.Context.ExpertOnlinerContexts.VirtualBasket basket = user.VirtualBasket;
            if (basket == null)
                user.VirtualBasket = basket=new Offtick.Data.Context.ExpertOnlinerContexts.VirtualBasket();
            else
            {
                //Fetch From DB
                 _exitesdEntity = fetchFromDB(basket);

                //Change in memory
                _exitesdEntity.OrderId = entity.OrderId;
                foreach (var item in entity.Entities)
                    _exitesdEntity.AddOrUpdateBasketOrder(item.EntityId, item.Count, item.IsBuyingVisible);
            }
            //persist
            persistVirtualBasket(_exitesdEntity, basket);
        }
        
        private int persistVirtualBasket(VirtualBasketEntityDB basketMemory, Offtick.Data.Context.ExpertOnlinerContexts.VirtualBasket basketDB)
        {
            basketDB.OrderId = basketMemory.OrderId;
            basketDB.LastDateTime = DateTime.Now;
            basketDB.ExpireDateTime = basketDB.LastDateTime.AddMinutes(20);
            basketDB.Entities = basketMemory.JsonSerialize();
            int effected = this.AcceptChanges();
            return effected;
        }

        private VirtualBasketEntityDB fetchFromDB(Offtick.Data.Context.ExpertOnlinerContexts.VirtualBasket basket)
        {
            if (basket == null)
                return null;
            VirtualBasketEntityDB _exitesdEntity = new VirtualBasketEntityDB();
            _exitesdEntity.UserId = basket.BasketId;
            _exitesdEntity.OrderId = basket.OrderId;
            _exitesdEntity.JsonDeserialize(basket.Entities);
            return _exitesdEntity;
        }
        public VirtualBasketEntityDB fetchFromDB(Offtick.Data.Context.ExpertOnlinerContexts.MembershipUser user)
        {
            if (user == null || user.VirtualBasket==null)
                return null;
            var basket= fetchFromDB(user.VirtualBasket);
            foreach (var entity in basket.Entities)
            {
                var foodOffer = OfferManager.Instance.GetOfferFood(entity.EntityId);
                if (foodOffer != null)
                {
                    entity.EntityPrice = foodOffer.DiscountPrice;
                    entity.EntityName = foodOffer.Offer.Title;
                    entity.DiscountPercent = foodOffer.DiscountPercent;
                    entity.TotalDiscount = (foodOffer.OriginalPrice - foodOffer.DiscountPrice) * entity.Count;
                }
            }
            return basket;
        }

        public VirtualBasketEntityDB FetchBasketEntitybyOrderId(string orderId)
        {
            if (string.IsNullOrEmpty(orderId))
                return null;
            var entities = this.GetPersitantStorage();
            var basket= entities.VirtualBaskets.FirstOrDefault(e => e.OrderId.Equals(orderId));
            return fetchFromDB(basket);
        }

        public void RemoveVirtualBasketEntity(long entityId, MembershipUser user)
        {
            if (user == null)
                return;
            VirtualBasketEntityDB _exitesdEntity = fetchFromDB(user.VirtualBasket);
            if (_exitesdEntity == null)
                return;
            var entityToRemove = _exitesdEntity.Entities.FirstOrDefault(e => e.EntityId.Equals(entityId));
            _exitesdEntity.RemoveBasketOrder(entityToRemove);
            persistVirtualBasket(_exitesdEntity, user.VirtualBasket);
        }

        public void ChangeOrderIdOfVirtualBasketEntity(MembershipUser user, string newOrderId)
        {
            if (user == null)
                return;
            VirtualBasketEntityDB _exitesdEntity = fetchFromDB(user.VirtualBasket);
            if (_exitesdEntity == null)
                return;
            _exitesdEntity.OrderId = newOrderId;
            persistVirtualBasket(_exitesdEntity, user.VirtualBasket);
        }

       

       
        public void ProcessSaleDoneForUser(MembershipUser user)
        {
            if (user == null)
                return;
            user.VirtualBasket = null;
            this.AcceptChanges();
        }
    }
}
