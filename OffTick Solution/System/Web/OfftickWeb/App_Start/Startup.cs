﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Owin;
using Microsoft.Owin;
using Ninject;
using Microsoft.AspNet.SignalR;
[assembly: OwinStartup(typeof(NearbyVisitWebView.App_Start.Startup))]
namespace NearbyVisitWebView.App_Start
{

    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {


            app.MapSignalR();

        }

    }
}