﻿
$(function () {
    $(document).on('click', '#btnSendSms', function (e) {
        var currentUserName = loginUserName;

        var endPoint = $("#txtSendSmsEndPoint").val();
        var domainName = $("#txtSendSmsDomainName").val();
        var userName = $("#txtSendSmsUserName").val();
        var password = $("#txtSendSmsPassword").val();
        var sender = $("#txtSendSmsSender").val();
        var receipt = $("#txtSendSmsReceipt").val();
        var text = $("#txtSendSmsText").val();


        var urlOfAction = adminPanelSmsSend;
        


        $.ajax({
            type: 'POST',
            url: urlOfAction,
            data: JSON.stringify({endPoint:endPoint, domainName: domainName, userName: userName, password: password, sender: sender, receipt: receipt, text: text }),
            dataType: 'json',
            processData: false,
            contentType: 'application/json; charset:utf-8',
            success: function (data, status) {
                if (status == "success") {
                    if (data.ResultStatus == "Success")
                        $("#lblSmsSendResult").text(data.Message);
                    else if (data.ResultStatus == "Failed")
                        $("#lblSmsSendResult").text(data.Error);
                }
            },
            error: function (xhr, textStatus, error) {
                var errorMessage = error || xhr.statusText;
                $("#lblSmsSendResult").val(errorMessage);

                //showMessageModal(errorMessage + xhr.toString() + textStatus);
            }
        });
    });
});