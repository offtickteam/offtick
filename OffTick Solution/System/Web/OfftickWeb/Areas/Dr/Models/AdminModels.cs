﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Offtick.Web.OfftickWeb.Areas.Dr.Models
{
    public class ControlItemModel
    {
        public System.Web.Mvc.MvcHtmlString ControlAddress { get; set; }
        public int EventCount { get; set; }
        public string EventLabel { get; set; }
        public string ControlLabel { get; set; }
        public System.Web.Mvc.MvcHtmlString EventAddress { get; set; }
        public string Title { get; set; }
        //public System.Drawing.Image Thumnail { get; set; }
        public string ImageUrl { get; set; }
        public double RouteValue { get; set; }


        

    }
}