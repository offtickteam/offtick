﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Offtick.Web.OfftickWeb.Areas.Dr.Models
{

    public enum AdaptGalleryMode
    {
        Add,
        Edit
    }
    public class AdaptGalleryModel
    {
        public Nullable<Guid> Id { get; set; }
        public Nullable<Guid> ParentId { get; set; }
        public String Title { get; set; }
        public string Description { get; set; }
        public bool IsDefault { get; set; }
        public AdaptGalleryMode AdaptMode { get; set; }
    }

    public class GalleryImageWrapperModel
    {
        public Guid GalleryId { get; set; }
        public Guid ImageId { get; set; }
    }


    public class GalleryImageModel
    {
        public System.Guid ImageId { get; set; }
        public Nullable<System.Guid> GalleryId { get; set; }
        public System.DateTime DateTime { get; set; }
        public string ImageUrl { get; set; }
        public short LanguageId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

    }

  


}