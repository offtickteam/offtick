﻿using Offtick.Core.EnumTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Offtick.Web.OfftickWeb.Areas.Dr.Models
{
    public enum MapModelMode {
        Edit=0,
        View=1,
    }

    [Serializable]
    public class MapMarker
    {
        public string latitude { get; set; }
        public string longitude { get; set; }
        public bool IsCenterMarker { get; set; }
    }

    [Serializable]
    public class MapModel
    {

        public bool Enable
        {
            get
            {
                return Mode == MapModelMode.Edit;
            }
        }

       
        public MapMarker[] Markers { get; set; }
        public string MapWidth { get; set; }
        public string mapHeight { get; set; }
        public string Title { get; set; }

        public int ZoomLevel { get; set; }
        public MapModelMode Mode { get; set; }
        public MapMarker Center
        {
            get
            {
                if (Markers != null && Markers.Count() != 0)
                    return Markers[0];
                return new MapMarker() { latitude = "35.749873", longitude = "51.387241", };
            }
        }
        public int DefaultZoom
        {
            get
            {
                return ZoomLevel !=0? ZoomLevel: 10;
            }
        }
        
    }

    public class TextAreaModel
    {
        public string EntityId { get; set; }
        public string Value { get; set; }
        public string ActionUrl { get; set; }
        public string Title { get; set; }
        public CommandType CommandType { get; set; }
    }
}