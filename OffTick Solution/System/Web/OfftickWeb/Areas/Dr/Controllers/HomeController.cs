﻿using Offtick.Web.OfftickWeb.Areas.Dr.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Offtick.Web.OfftickWeb.Areas.Dr.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Dr/Home/
        
        public ActionResult Index()
        {
           
            IList<ControlItemModel> items = new List<ControlItemModel>();
            items.Add(new ControlItemModel()
            {
                EventCount = 0,
                EventLabel="Fallow",
                Title = Resources.UILabels.Profile,
                ControlAddress = System.Web.Mvc.MvcHtmlString.Create("/Dr/Profile/Index"),
                EventAddress = System.Web.Mvc.MvcHtmlString.Create("Dr/Event/0"),
                ImageUrl = "~/Content/Images/profile1.jpg",
                RouteValue = 3.4,
            });

            items.Add(new ControlItemModel()
            {
                EventCount = 0,
                ControlLabel="Go to",
                Title = Resources.UILabels.Galary,
                ControlAddress = System.Web.Mvc.MvcHtmlString.Create("/Dr/Gallery/Index"),
                EventAddress = System.Web.Mvc.MvcHtmlString.Create("Dr/Event/1"),
                ImageUrl = "~/Content/Images/gallery_icon.png",
                RouteValue = 3.4,
            });

            items.Add(new ControlItemModel()
            {
                EventCount = 0,
                Title = Resources.UILabels.News,
                ControlAddress = System.Web.Mvc.MvcHtmlString.Create("/Dr/News/Index"),
                EventAddress = System.Web.Mvc.MvcHtmlString.Create("/Dr/Event/2"),
                ImageUrl = "~/Content/Images/news3.JPG",
                RouteValue = 3.4,
            });

            items.Add(new ControlItemModel()
            {
                EventCount = 0,
                Title = Resources.UILabels.Paper,
                ControlAddress = System.Web.Mvc.MvcHtmlString.Create("Dr/Paper/Index"),
                EventAddress = System.Web.Mvc.MvcHtmlString.Create("Dr/Event/3"),
                ImageUrl = "~/Content/Images/paper.jpg",
                RouteValue = 3.4,
            });
            items.Add(new ControlItemModel()
            {
                 EventCount=0,
                 Title=Resources.UILabels.FAQ,
                 ControlAddress = System.Web.Mvc.MvcHtmlString.Create("Dr/FAQ/Index"),
                 EventAddress = System.Web.Mvc.MvcHtmlString.Create("Dr/Event/4"),
                 ImageUrl = "~/Content/Images/faq3.jpg",
                 RouteValue = 3.4,
            });
            items.Add(new ControlItemModel()
            {
                EventCount = 0,
                Title = Resources.UILabels.Services,
                ControlAddress = System.Web.Mvc.MvcHtmlString.Create("Dr/Service/Index"),
                EventAddress = System.Web.Mvc.MvcHtmlString.Create("Dr/Event/5"),
                ImageUrl = "~/Content/Images/medicalService1.jpg",
                 RouteValue=3.2,
            });

            items.Add(new ControlItemModel()
            {
                EventCount = 0,
                Title = Resources.UILabels.UseFullLink,
                ControlAddress = System.Web.Mvc.MvcHtmlString.Create("Dr/UseFullLink/Index"),
                EventAddress = System.Web.Mvc.MvcHtmlString.Create("Dr/Event/6"),
                ImageUrl = "~/Content/Images/usefullLink2.jpg",
                RouteValue = 3.2,
            });

            items.Add(new ControlItemModel()
            {
                EventCount = 0,
                Title = Resources.UILabels.TimePlan,
                ControlAddress = System.Web.Mvc.MvcHtmlString.Create("Dr/TimePlan/Index"),
                EventAddress = System.Web.Mvc.MvcHtmlString.Create("Dr/Event/7"),
                ImageUrl = "~/Content/Images/timeplan4.png",
                RouteValue = 3.2,
            });

            items.Add(new ControlItemModel()
            {
                EventCount = 0,
                Title = Resources.UILabels.Refferal,
                ControlAddress = System.Web.Mvc.MvcHtmlString.Create("Dr/Refferal/Index"),
                EventAddress = System.Web.Mvc.MvcHtmlString.Create("Dr/Event/8"),
                ImageUrl = "~/Content/Images/Refferal1.png",
                RouteValue = 3.2,
            });

            items.Add(new ControlItemModel()
            {
                EventCount = 0,
                Title = Resources.UILabels.Configuration,
                ControlAddress = System.Web.Mvc.MvcHtmlString.Create("Dr/Config/Index"),
                EventAddress = System.Web.Mvc.MvcHtmlString.Create("Dr/Event/9"),
                ImageUrl = "~/Content/Images/security3.jpg",
                RouteValue = 3.2,
            });

            items.Add(new ControlItemModel()
            {
                EventCount = 0,
                Title = Resources.UILabels.MenuBuilder,
                ControlAddress = System.Web.Mvc.MvcHtmlString.Create("/Dr/MenuBuilder/Index"),
                EventAddress = System.Web.Mvc.MvcHtmlString.Create("Dr/Event/10"),
                ImageUrl = "~/Content/Images/menu48.png",
                RouteValue = 3.2,
            });

            items.Add(new ControlItemModel()
            {
                EventCount = 0,
                Title = Resources.UILabels.Content,
                ControlAddress = System.Web.Mvc.MvcHtmlString.Create("/Dr/CPContent/Index"),
                EventAddress = System.Web.Mvc.MvcHtmlString.Create("Dr/Event/11"),
                ImageUrl = "~/Content/Images/Document-icon48.png",
                RouteValue = 3.2,
            });

            ViewData["controlItems"] = items;
            return View("Index");
        }

    }
}
