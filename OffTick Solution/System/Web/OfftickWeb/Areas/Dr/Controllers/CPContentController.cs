﻿using Offtick.Business.Membership;
using Offtick.Business.Services.Managers;
using Offtick.Business.Services.Storage;
using Offtick.Business.Services.Storage.File;
using Offtick.Business.Web;
using Offtick.Core.EnumTypes;
using Offtick.Web.OfftickWeb.Infrustracture;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Offtick.Web.OfftickWeb.Areas.Dr.Controllers
{
    public class CPContentController : BaseController
    {
        public CPContentController(DataContextContainer obj)
            : base(obj )
        {
            
        }

        public ActionResult Index()
        {
            return View();
        }


        [HttpPost]
        [Authorize]
        [ValidateInput(false)]
        public ActionResult UploadFileLoader()
        {
            var length = Request.ContentLength;
            string title = Request.Form["Title"];
            string description = Request.Form["Description"];

            if (Request.Files != null && Request.Files.Count != 0)
            {
                ContentManager.Instance.AddFile(1,title, description,  MembershipManager.Instance.GetCurrentUser().UserId.ToString(), Request.Files);
            }
            return null;
        }

        public ActionResult ContentPresentationItemCollection()
        {
            IList<Offtick.Data.Context.ExpertOnlinerContexts.File> files = MembershipManager.Instance.GetCurrentUser().Files.ToList();
            return View(files);
        }

        public ActionResult DeleteFile(Guid fileId)
        {
            DeleteStatus status = ContentManager.Instance.DeleteFile(fileId);
            string msg = UIActionStatusToUiMessage.ActionStatusToMessage(status);
            return Json(msg); 
        }
    }
}
