﻿using Offtick.Business.Membership;
using Offtick.Business.Services.Managers;
using Offtick.Business.Services.Storage;
using Offtick.Business.Services.Storage.File;
using Offtick.Business.Web;
using Offtick.Core.EnumTypes;
using Offtick.Web.OfftickWeb.Infrustracture;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Offtick.Web.OfftickWeb.Areas.Dr.Controllers
{
    public class GalleryController : BaseController
    {
        //
        // GET: /Dr/Gallery/

        public ActionResult Index()
        {
            var currentUser=MembershipManager.Instance.GetCurrentUser();
            return View(currentUser!=null?currentUser.Galleries.Where(e=>e.ParentGaleryId==null):null);
        }



        public GalleryController(DataContextContainer obj)
            : base(obj )
        {
            
        }



        [HttpPost]
        public ActionResult UploadFileLoader()
        {
            var length = Request.ContentLength;
            string title = Request.Form["Title"];
            string description = Request.Form["Description"];
            string galleryId = Request.Form["GalleryId"];

            if (Request.Files != null && Request.Files.Count != 0 && !String.IsNullOrEmpty(galleryId))
            {
                GalleryManager.Instance.AddImgageGallery(title, description, Guid.Parse(galleryId), MembershipManager.Instance.GetCurrentUser().UserId, Request.Files);
            }
            return null;
        }
       


       [HttpPost]
       public ActionResult AdaptGallery(Offtick.Web.OfftickWeb.Areas.Dr.Models.AdaptGalleryModel gallery)
       {
           string msg = string.Empty; 
           if (gallery != null && gallery.AdaptMode == Models.AdaptGalleryMode.Add)
           {
               AddStatus status = GalleryManager.Instance.AddGallery(gallery.Title, gallery.Description, gallery.ParentId, gallery.IsDefault);
              msg=UIActionStatusToUiMessage.ActionStatusToMessage(status);
           }
           else if (gallery.Id.HasValue)
           {
               EditStatus status= GalleryManager.Instance.EditGallery(gallery.Title, gallery.Description, gallery.Id.Value, gallery.IsDefault);
               msg = UIActionStatusToUiMessage.ActionStatusToMessage(status);
           }
           return Json(msg);
       }

        /// <summary>
        /// Return Gallery Menu for Current User
        /// </summary>
        /// <param name="imageGalleryMenuID"></param>
        /// <param name="updateTargetId"></param>
        /// <returns></returns>
       public ActionResult GalleryMenu(string imageGalleryMenuID,string updateTargetId)
       { 
            var currentUser=MembershipManager.Instance.GetCurrentUser();
            var gallerisOfCurrentUser=currentUser!=null?currentUser.Galleries.Where(e=>e.ParentGaleryId==null):null;
            var strMenu= Offtick.Business.Web.GalleryImageHelper.CreateGalleryMenu(gallerisOfCurrentUser, 
                LangaugeManager.Instance.GetDefaultLangaugeId()
                ,imageGalleryMenuID, string.Empty, updateTargetId);
            return Content( strMenu.ToString());
           
            
       }
       public ActionResult GalleryHeaderInfo(string galleryObjectId)
       {

           if (!string.IsNullOrEmpty(galleryObjectId))
           {

               return View(GalleryManager.Instance.GetGallery(Guid.Parse(galleryObjectId)));
           }
           else
               return View();
       }

        
       public ActionResult GalleryBodyManagement(string galleryId)
       {
           if (!string.IsNullOrEmpty(galleryId))
           {
               var obj = GalleryManager.Instance.GetGallery(Guid.Parse(galleryId));
               return View(obj);
           }
           else
               return View(); 
       }

       public ActionResult GalleryImageWrapperCollection(string galleryId)
       {
           if (!string.IsNullOrEmpty(galleryId))
           {
               var obj = GalleryManager.Instance.GetGallery(Guid.Parse(galleryId));
               return View(obj.GalleryImages);
           }
           else
               return View();
           
           
       }
       public ActionResult GalleryImageWrapper(string galleryImageId)
       {
           if (!string.IsNullOrEmpty(galleryImageId))
           {
               var obj = GalleryManager.Instance.GetGalleryImage(Guid.Parse(galleryImageId));
               return View(obj);
           }
           else
               return View();


       }

       public ActionResult DeleteGalleryImage(string galleryImageId)
       {
           if (!string.IsNullOrEmpty(galleryImageId))
           {
               bool deleted= GalleryManager.Instance.DeleteGalleryImage(Guid.Parse(galleryImageId));
               return Content(deleted?"Deleted":"Error in deleting entity");
           }
           else
               return Content("Error in deleting entity"); 
       }

        [HttpGet]
       public ActionResult EditGalleryImageInfo(string galleryImageId)
       {
           if (!string.IsNullOrEmpty(galleryImageId))
           {
               var gal = GalleryManager.Instance.GetGalleryImage(Guid.Parse(galleryImageId));
               if (gal != null)
               {
                   Offtick.Web.OfftickWeb.Areas.Dr.Models.GalleryImageModel obj = new Models.GalleryImageModel()
                   {
                        GalleryId=gal.GalleryId,
                        ImageId=gal.ImageId,
                         Description=gal.Description,
                            Title=gal.Title,
                            
                   };
                   return View(obj);
               }
           }
               throw new HttpException("No Gallery Image Specified");
       }

        [HttpPost]
       public ActionResult EditGalleryImageInfo(Offtick.Web.OfftickWeb.Areas.Dr.Models.GalleryImageModel galleryImage)
       {
           if (galleryImage != null)
           {
               bool effected = GalleryManager.Instance.EditGalleryImageInfo(galleryImage.ImageId, galleryImage.Title, galleryImage.Description);
               return Json(effected ? "Saved" : "Error in editing entity");
           }
           else
               throw new HttpException("No Gallery Image Specified");
       }


        

    }

    
}
