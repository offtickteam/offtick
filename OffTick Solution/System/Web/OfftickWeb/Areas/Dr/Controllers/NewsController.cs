﻿using Offtick.Business.Membership;
using Offtick.Business.Services.Managers;
using Offtick.Business.Services.Storage;
using Offtick.Business.Web;
using Offtick.Core.EnumTypes;
using Offtick.Web.OfftickWeb.Infrustracture;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Offtick.Web.OfftickWeb.Areas.Dr.Controllers
{
    public class NewsController : BaseController
    {

        public NewsController(DataContextContainer obj)
            : base(obj )
        {
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult NewsArchive(string userId)
        {
            if (string.IsNullOrEmpty(userId))
                userId = MembershipManager.Instance.GetCurrentUserName();
            if (MembershipManager.Instance.ExistUser(userId))
            {
                return View(MembershipManager.Instance.GetCurrentUser().News);
            }
            else
                return null;
        }

        [HttpPost]
        [ValidateInput(false)]
        
        public ActionResult UploadNews()
        {
            var length = Request.ContentLength;
            string title = Request.Form["Title"];
            string body = Request.Form["Body"];
            string KeywordsJsonString = Request.Form["Keywords"];
            //JavaScriptSerializer JSS = new JavaScriptSerializer();

            //object obj = JSS.Deserialize<object>(Keywords);
            IList<string> keywordsList = new List<string>();
            var objects = Newtonsoft.Json.Linq.JArray.Parse(KeywordsJsonString); // parse as array  
            foreach (Newtonsoft.Json.Linq.JObject root in objects)
            {
                foreach (KeyValuePair<String, Newtonsoft.Json.Linq.JToken> app in root)
                {
                    keywordsList.Add((String)app.Value);
                }
            }



            if (Request.Files != null && Request.Files.Count != 0)
            {
                NewsManager.Instance.AddFile(1, title, body, MembershipManager.Instance.GetCurrentUserName(), keywordsList, Request.Files);
            }
            return null;
        }


        public ActionResult DeleteNews(Guid newsId)
        {
            DeleteStatus status = NewsManager.Instance.DeleteNews(newsId);
            string msg = UIActionStatusToUiMessage.ActionStatusToMessage(status);
            return Json(msg);
        }

        public ActionResult EditNews(Guid newsId)
        {
            var news = NewsManager.Instance.GetNews(newsId);
            return View(news);
        }

    }
}
