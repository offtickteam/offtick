﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Offtick.Business.Web;
using Offtick.Data.Context.ExpertOnlinerContexts;
using Offtick.Web.OfftickWeb.Areas.Dr.Models;
using Offtick.Business.Services.Storage;
using Offtick.Business.Membership;
using Offtick.Business.Services.RealtimeSearvices;

namespace Offtick.Web.OfftickWeb.Areas.Dr.Controllers
{
    public class ProfileController : BaseController
    {
        //
        // GET: /Admin/Profile/

        public ProfileController(DataContextContainer obj)
            : base(obj )
        {
        }
        

        public ActionResult Index()
        {
            return View();
        }


        public ViewResult ProfileControlPanel()
        {
            return View();
        }




        public ViewResult ManageAccount(string manageAccountViewName)
        {
            object model=null;
            if (manageAccountViewName == "ContactUs" || manageAccountViewName == "AboutUs")
            {
                var introduceInfo=MembershipManager.Instance.GetIntroduceInfo(MembershipManager.Instance.GetCurrentUser().UserId);
                string value=introduceInfo!=null? ( manageAccountViewName == "ContactUs"?introduceInfo.ContactUs:introduceInfo.AboutUs):string.Empty;
                model = new TextAreaModel()
                {
                    Value =value
                };
            }
            return View(manageAccountViewName, model);
        }


        public ViewResult Information()
        {
            return View();
        }

        [HttpPost]
        public ViewResult Information(Offtick.Web.OfftickWeb.Models.RegisterModel model)
        {
            return View();
        }

        public ViewResult Map()
        {
            MapModel mapmodel = new MapModel()
            {
                mapHeight = "800px",
                MapWidth = "600px",
                Mode = MapModelMode.Edit
            };
            return View(mapmodel);
        }

        [HttpPost]
        public ActionResult Map(MapModel tt)
        {
            if (Request.IsAjaxRequest())
            {
                return Json(null);
            }
            else

            return View();
        }

        public ActionResult Address()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Address(int page, int rows, string search, string sidx, string sord)
        {
            var locations=MembershipManager.Instance.GetLocationsOfUser(MembershipManager.Instance.GetCurrentUser().UserId);
            var jsonLocations=(from l in locations select new {id=l.LocationId.ToString(),cell=new string[]{l.Name,l.Address}}).ToArray();
            var jsonData= new {total = 1, page = 1, records = 1, rows=jsonLocations  };

            return Json(jsonData);
            
        }

        [HttpPost]
        public ActionResult ContactUs(Offtick.Web.OfftickWeb.Areas.Dr.Models.TextAreaModel info)
        {
            var userId=MembershipManager.Instance.GetCurrentUser().UserId;
            MembershipManager.Instance.SaveIntroduceInfo(userId, info.Value, false);
            return Json("saved");
        }

        [HttpPost]
        public ActionResult AboutUs(Offtick.Web.OfftickWeb.Areas.Dr.Models.TextAreaModel info)
        {
            var userId = MembershipManager.Instance.GetCurrentUser().UserId;
            MembershipManager.Instance.SaveIntroduceInfo(userId, info.Value, true);
            return Json("saved");
        }

        [HttpPost]
        public ActionResult ChangePassword(Offtick.Web.OfftickWeb.Models.ChangePasswordModel model)
        {
            if (ModelState.IsValid)
            {

                // ChangePassword will throw an exception rather
                // than return false in certain failure scenarios.
                Offtick.Data.Entities.Common.LoginStatus status = Data.Entities.Common.LoginStatus.InvalidPassword;
                try
                {
                     status=MembershipManager.Instance.ChangePassword(model.OldPassword, model.NewPassword);
                     
                }
                catch (Exception)
                {
                }

                if (status== Data.Entities.Common.LoginStatus.Success)
                {
                    return Json("Success");
                }
                else
                {
                    return Json("Invalid Password");
                }
            }
            return Json("Invalid Parameter");
        }

        public ActionResult PorfileItem(int orderNumber)
        {
            return View(orderNumber);
        }

        public ActionResult ProfileItemCollection()
        {
           
            return View();
        }
    }
}
