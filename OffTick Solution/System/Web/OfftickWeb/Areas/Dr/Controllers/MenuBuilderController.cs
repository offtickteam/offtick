﻿using Offtick.Business.Membership;
using Offtick.Business.Services.Managers;
using Offtick.Core.EnumTypes;
using Offtick.Data.Context.ExpertOnlinerContexts;
using Offtick.Web.OfftickWeb.Areas.Dr.Models;
using Offtick.Web.OfftickWeb.Infrustracture;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Offtick.Web.OfftickWeb.Areas.Dr.Controllers
{
    public class MenuBuilderController : Offtick.Business.Web.BaseController
    {


        public MenuBuilderController(Offtick.Business.Services.Storage.DataContextContainer obj)
            : base(obj )
        {
            
        }


        //
        // GET: /Dr/MenuBuilder/

        public ActionResult Index()
        {
            var currentUser = MembershipManager.Instance.GetCurrentUser();
            return View(currentUser != null ? currentUser.Menus.Where(e => e.ParentId == null) : null);
        }

        public ActionResult DeleteMenu(MenuBuilderGeneralModel model )
        {
            DeleteStatus status = MenuBuilder.Instance.DeleteMenu(model.MenuId.Value);
            string msg = UIActionStatusToUiMessage.ActionStatusToMessage(status);
            return Json(msg);
        }

        public ActionResult EditMenuInfo(MenuBuilderGeneralModel model)
        {
            EditStatus status= MenuBuilder.Instance.EditMenuTitle(model.MenuId.Value, model.Title);
            string msg = UIActionStatusToUiMessage.ActionStatusToMessage(status);
            return Json(msg);
        }

        public ActionResult AddMenu(Offtick.Data.Entities.Common.Models.MenuModel menu)
        {
            AddStatus status = MenuBuilder.Instance.AddMenu(menu.ParentId,
                LangaugeManager.Instance.GetDefaultLangaugeId(),
                menu.Title, (MenuType)menu.MenuType, menu.Url,menu.IconUrl, menu.IsSystemMenu);
            string msg = UIActionStatusToUiMessage.ActionStatusToMessage(status);
            return Json(msg);
        }

        public ActionResult MenuBuilderParser(Nullable<Guid> menuId)
        {
            return View("MenuBuilderBody", menuId.HasValue? MenuBuilder.Instance.GetMenu(menuId.Value):
                 new Menu() { LastDateTime = DateTime.Now, MenuType = (short)Offtick.Core.EnumTypes.MenuType.ParentMenu }
                );
        }

        public ActionResult AdaptMenuContent(Offtick.Web.OfftickWeb.Areas.Dr.Models.TextAreaModel model)
        {
            if (model.CommandType == CommandType.Add)
            {
                AddStatus status = MenuBuilder.Instance.AddMenuContent(Guid.Parse( model.EntityId),LangaugeManager.Instance.GetDefaultLangaugeId(), model.Value);
                string msg = UIActionStatusToUiMessage.ActionStatusToMessage(status);
                return Json(msg);
            }
            else
            {
                EditStatus status = MenuBuilder.Instance.EditMenuContentByMenuId(Guid.Parse(model.EntityId), model.Value);
                string msg = UIActionStatusToUiMessage.ActionStatusToMessage(status);
                return Json(msg);
            }
        }
    }
}
