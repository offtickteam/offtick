﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Offtick.Web.OfftickWeb.Areas.User.Models
{
    
    public enum ApplicationStatus{
        PreStart,
        Running,
        Finished,
        FinishedNotPersisted,
        Next,
        Previous,
        Reset,
    }
        public class MBTIModel
        {
            public Int16 QuestionNo { get; set; }
            public string QuestionText { get; set; }
            public string AnswerAText { get; set; }
            public string AnswerBText { get; set; }
            public Int16 AnswerNo { get; set; }
            public ApplicationStatus MBTIStatus { get; set; }
            
        }

        public class HollandModel
        {
            public Int16 PageNo { get; set; }
            public Int16 PageTake { get; set; }
            public int Level { get; set; }
            public string Answers { get; set; }
            public ApplicationStatus ApplicationStatus { get; set; }
            

        }
        public class SelectMajorModel
        {
            public int ApplicationMajorChoiceHighSchoolId { get; set; }
            public string StringChoices { get; set; } // only set from cient
           // public IList<MajorCityUniversity> Choices { get; set; }
            public int Step { get; set; }// 1: select highschool major, 2: select Choice; 3: assign wieghts; 4: assign grade to each choice
           // public Dictionary<int, int> CityGrade { get; set; }
           // public Dictionary<int, int> UniversityGrade { get; set; }
           // public Dictionary<int, int> MajorGrade { get; set; }


            public int CityWeight { get; set; }
            public int UniversityWeight { get; set; }
            public int MajorWeight { get; set; }
          
            

            public ApplicationStatus ApplicationStatus { get; set; }

        }
        public class MajorCityUniversity
        {
            public int ApplicationMajorChoiceCityId { get; set; }
            public int ApplicationMajorChoiceUniversityMajorId { get; set; }
            public int ApplicationMajorChoiceUniversityId { get; set; }
        }
    
}