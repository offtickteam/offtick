﻿using Offtick.Core.EnumTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Offtick.Web.OfftickWeb.Areas.User.Models
{
    public class TourModel
    {
        public HttpPostedFileBase mainImage { get; set; }
        public HttpPostedFileBase[] tourImages { get; set; }
        public HttpPostedFileBase[] menuImages { get; set; }

        public string tourTitle { get; set; }
        public string tourTitleLong { get; set; }
        public string url { get; set; }


        public long tourOriginalPrice { get; set; }
        public string tourOriginalTitle { get; set; }
        public string tourUnit { get; set; }
        public long tourDiscountPrice { get; set; }
        public string tourDiscountTitle { get; set; }
        public short tourDiscountPercent { get; set; }
        public int tourExpireDate { get; set; }
        public short tourQuantity { get; set; }
        
        public string tourLocationAddress { get; set; }
        public HttpPostedFileBase tourSpecification { get; set; }
        public HttpPostedFileBase tourConditions { get; set; }
        public HttpPostedFileBase tourDescriptions { get; set; }
        
        public bool showIncomeInPercentage { get; set; }
        public bool  tourHasAddedValueTax { get; set; }
        public bool  tourShowBuyButton        { get; set; }
        public bool  tourShowOffButton        { get; set; }
        public bool   tourShowInstantButton   { get; set; }
        public string tourSourceCityId      { get; set; }
        public string tourDestinationCityId { get; set; }
        public string tourWentDate           { get; set; }
        public string tourReturnDate        { get; set; }
        public string tourAgencyName        { get; set; }
        public TravelTypeEnum tourTravelType { get; set; }


    }

    public class FoodModel
    {
        public HttpPostedFileBase mainImage { get; set; }
        public HttpPostedFileBase[] foodImages { get; set; }
        public HttpPostedFileBase[] menuImages { get; set; }

        public string foodTitle { get; set; }
        public string foodTitleLong { get; set; }
        public string url { get; set; }


        public long foodOriginalPrice       { get; set; }
        public string foodOriginalTitle     { get; set; }
        public string foodUnit              { get; set; }
        public long foodDiscountPrice       { get; set; }
        public string foodDiscountTitle     { get; set; }
        public short foodDiscountPercent     { get; set; }
        public int foodExpireDate { get; set; }
        public short foodQuantity            { get; set; }
        public string foodAudeiences        { get; set; }
        public string foodLocationCityId { get; set; }
        public string foodLocationAddress { get; set; }
        public HttpPostedFileBase foodSpecification { get; set; }
        public HttpPostedFileBase foodConditions { get; set; }
        public HttpPostedFileBase foodDescriptions { get; set; }
        public short offtickOfferTypeId { get; set; }
        public bool showIncomeInPercentage { get; set; }
        public bool foodHasAddedValueTax { get; set; }
        

    }

    public class UpgradeOfftickModel
    {

        public Guid offerId { get; set; }

        public string offerTitle { get; set; }
        public string offerTitleLong { get; set; }
        public string url { get; set; }


        public long offerOriginalPrice { get; set; }
        public string offerOriginalTitle { get; set; }
        public string offerUnit { get; set; }
        public long offerDiscountPrice { get; set; }
        public string offerDiscountTitle { get; set; }
        public short offerDiscountPercent { get; set; }
        public int offerExpireDate { get; set; }
        public short offerQuantity { get; set; }
        public string offerAudeiences { get; set; }
        public string offerLocationCityId { get; set; }
        public string offerLocationAddress { get; set; }
        public HttpPostedFileBase offerSpecification { get; set; }
        public HttpPostedFileBase offerConditions { get; set; }
        public HttpPostedFileBase offerDescriptions { get; set; }
        
        public bool showIncomeInPercentage { get; set; }
        public bool offerHasAddedValueTax { get; set; }


    }
}