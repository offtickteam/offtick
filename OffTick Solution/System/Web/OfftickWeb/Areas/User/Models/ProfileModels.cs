﻿using Offtick.Core.EnumTypes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Offtick.Web.OfftickWeb.Areas.User.Models
{
    public class PersonalModel
    {
        public string firstName{get;set;}
        public string lastName { get; set; }
        public string userName { get; set; }
        public string imageThumbnail { get; set; }
        public FollowStatus fallowStatus { get; set; }

    }

    public class ProfileHeaderInfo
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string userName { get; set; }
        public string imageThumbnail { get; set; }

        public int countOfFollower { get; set; }
        public int countOfFollowing { get; set; }
        public int countOfPost { get; set; }
        public int age { get; set; }
        public string gender { get; set; }

        public FollowStatus fallowStatus { get; set; }
        public short isBlocked { get; set; }
      
    }


    public class ControlItemModel
    {
        public System.Web.Mvc.MvcHtmlString ControlAddress { get; set; }
        public int EventCount { get; set; }
        public string EventLabel { get; set; }
        public string ControlLabel { get; set; }
        public System.Web.Mvc.MvcHtmlString EventAddress { get; set; }
        public string Title { get; set; }
        //public System.Drawing.Image Thumnail { get; set; }
        public string ImageUrl { get; set; }
        public double RouteValue { get; set; }




    }

    public class OwnerSpecificationModel
    {
        public string UserName { get; set; }
        public string Specification { get; set; }
        public string ResponsibleName{ get; set; }
        public string ResponsiblePhone{ get; set; }
        public string ResponsibleEmail{ get; set; }
    }

    public class ChangeUserNameModel
    {
        [Required(ErrorMessageResourceName = "ReguieredField", ErrorMessageResourceType = typeof(Resources.UIMessages))]
        [Display(Name = "UserName", ResourceType = typeof(Resources.UICaptions))]
        public string UserName { get; set; }

        
        [Required(ErrorMessageResourceName = "ReguieredField", ErrorMessageResourceType = typeof(Resources.UIMessages))]
        [Display(Name = "NewUserName", ResourceType = typeof(Resources.UICaptions))]
        [StringLength(30, MinimumLength = 6, ErrorMessageResourceName = "StringRangeLength", ErrorMessageResourceType = typeof(Resources.UIMessages))]
        [RegularExpression("[a-zA-Z0-9_]{6,40}", ErrorMessageResourceName = "InvalidUserName", ErrorMessageResourceType = typeof(Resources.UIMessages))]
        public string NewUserName { get; set; }

        [Required(ErrorMessageResourceName = "ReguieredField", ErrorMessageResourceType = typeof(Resources.UIMessages))]
        [Display(Name = "ReNewUserName", ResourceType = typeof(Resources.UICaptions))]
        [System.Web.Mvc.Compare("NewUserName", ErrorMessageResourceName = "NotEqualInCompare", ErrorMessageResourceType = typeof(Resources.UIMessages))]
        public string ReNewUserName { get; set; }
    }
}