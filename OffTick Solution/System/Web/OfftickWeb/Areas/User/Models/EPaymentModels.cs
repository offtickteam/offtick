﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Offtick.Web.OfftickWeb.Areas.User.Models
{
    public class PayRequestModel
    {
        public long orderId{get;set;}
        public long amount{get;set;}
        public string date{get;set;}
        public string time{get;set;}
        public string additionalData{get;set;}
        public long payerId{get;set;}
    }

     
    public class InquiryVerifyReversalSettleRequestModel
    {
        public int actionType { get; set; }//Verify=1,Inquery=2,Reversal=3,Settle=4
        public long OrderId { get; set; }
        public long SaleOrderId { get; set; }
        public long SaleReferenceId { get; set; }
        
    }


    public class PaymentResult
    {
        public string refId{get;set;}
        public short resCode{get;set;}
        public long? saleReferenceId{get;set;}
        public long? saleOrderId { get; set; }
        public bool isSucess { get; set; }
    }

    
}