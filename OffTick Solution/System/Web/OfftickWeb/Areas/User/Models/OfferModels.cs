﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Offtick.Web.OfftickWeb.Areas.User.Models
{
    public class OfferVisitedPresenterModel
    {
        public Guid OfferId { get; set; }
        public string ControlId { get; set; }
        public int PictureSize { get; set; }
        public string HeaderTitle { get; set; }
        public ModalMode ModalMode { get; set; }
    }

        public enum ModalMode{
            Small=0,
            Medium=1,
            large=2,
        }
}