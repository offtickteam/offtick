﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Offtick.Web.OfftickWeb.Areas.User.Models
{
    public class MenuBuilderGeneralModel
    {
        public string Title { get; set; }
        public Nullable<Guid> MenuId { get; set; }
        public Nullable<Guid> ParentId { get; set; }
        public string UrlLink { get; set; }
        public Offtick.Core.EnumTypes.MenuType MenuType { get; set; }
    }

    public class MenuBuilderAddModel
    {
        public string Title { get; set; }
        public Guid MenuId { get; set; }
        public Nullable<Guid> ParentId { get; set; }
        public string UrlLink { get; set; }
        public Offtick.Core.EnumTypes.MenuType MenuType { get; set; }


    }

    
}