﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Offtick.Web.OfftickWeb.Areas.User.Models
{
    public class BasketUpdateCountModel
    {
        public Guid OfferId { get; set; }
        public int OfferCount { get; set; }
    }
}