﻿using Offtick.Business.Membership;
using Offtick.Business.Services.Managers;
using Offtick.Business.Services.Storage;
using Offtick.Business.Services.Storage.File;
using Offtick.Business.Web;
using Offtick.Core.Utility.PersianTools;
using Offtick.Data.Entities.Common;
using Offtick.Web.OfftickWeb.Areas.User.Models;
using Offtick.Web.OfftickWeb.Models;
using Offtick.Web.OfftickWeb.Resources;
using Offtick.Data.Context.ExpertOnlinerContexts;
using iTextSharp.text.pdf.qrcode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Offtick.Web.OfftickWeb.Areas.User.Controllers
{
    public class ProfileController : BaseController
    {
        public ProfileController(DataContextContainer obj)
            : base(obj)
        {
            
        }



        //
        // GET: /User/Profile/

        
        public ActionResult Index(string userName)
        {
            string currentUserName = Offtick.Business.Membership.MembershipManager.Instance.GetCurrentUserName();
            if (!string.IsNullOrEmpty(userName))
            {
                var owner=MembershipManager.Instance.GetUser(userName);
                var currentUser = MembershipManager.Instance.GetCurrentUser();
                if (owner != null && !userName.Equals(currentUserName))
                {
                    MembershipManager.Instance.AddVisitor(owner,currentUser);
                }
            }
            if(string.IsNullOrEmpty(userName))
                userName=currentUserName;
            return View((object)userName);
        }


     

        public ActionResult OtherProfile(string userName)
        {
            return RedirectToAction("Index", "Profile", new { userName=userName});
        }
        public ActionResult HeaderProfile(string userId)
        {
            return View((object)userId);
        }
        public ActionResult RandomUsersForMainPage()
        {
            {

                var users = MembershipManager.Instance.GetAllUsers(7, 0);

                if (users != null && users.Count() > 0)
                {
                    IList<PersonalModel> lsResultToClient = new List<PersonalModel>();
                    int counter = 0;
                    foreach (var user in users)
                    {

                        ImageFileManager imgFileManager = new ImageFileManager(user.UserId.ToString());
                        string imgAddress = !string.IsNullOrEmpty(user.Picture)?imgFileManager.GetRelativeFullFileName(user.UserId.ToString(), user.Picture, true):string.Empty;

                        var userClientObj = new Offtick.Web.OfftickWeb.Areas.User.Models.PersonalModel
                        {
                            firstName = user.FirstName,
                            lastName = user.LastName,
                            userName = user.UserName,
                            imageThumbnail = imgAddress,
                            fallowStatus = FollowManager.Instance.GetFollowStatusFor(MembershipManager.Instance.GetCurrentUser(), user),
                        };
                        //lsResultToClient[counter] = userClientObj;
                        lsResultToClient.Add(userClientObj);
                        counter++;




                    }
                    //return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "found items", lsResultToClient.ToArray(), ActionResponseFromServerToClient.FollowSearchResultServerToClientKey));
                    return View(lsResultToClient);
                }
                else
                    return View();
                // return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "No item found", string.Empty, string.Empty, string.Empty));

            }
            return View();
        }

        public ActionResult UsersByCriteria(string[] searchCriterias,int take,int skip)
        {
            {

                var users = MembershipManager.Instance.GetUsersByCriteria(searchCriterias,take, skip);

                if (users != null && users.Count() > 0)
                {
                    IList<PersonalModel> lsResultToClient = new List<PersonalModel>();
                    int counter = 0;
                    foreach (var user in users)
                    {

                        ImageFileManager imgFileManager = new ImageFileManager(user.UserId.ToString());
                        string imgAddress = !string.IsNullOrEmpty(user.Picture) ? imgFileManager.GetRelativeFullFileName(user.UserId.ToString(), user.Picture, true) : string.Empty;

                        var userClientObj = new Offtick.Web.OfftickWeb.Areas.User.Models.PersonalModel
                        {
                            firstName = user.FirstName,
                            lastName = user.LastName,
                            userName = user.UserName,
                            imageThumbnail = imgAddress,
                            fallowStatus = FollowManager.Instance.GetFollowStatusFor(MembershipManager.Instance.GetCurrentUser(), user),
                        };
                        //lsResultToClient[counter] = userClientObj;
                        lsResultToClient.Add(userClientObj);
                        counter++;




                    }
                    //return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "found items", lsResultToClient.ToArray(), ActionResponseFromServerToClient.FollowSearchResultServerToClientKey));
                    return View("RandomUsersForMainPage", lsResultToClient);
                }
                else
                    return View("RandomUsersForMainPage");
                // return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "No item found", string.Empty, string.Empty, string.Empty));

            }

        }
     

        [Authorize]
        public ActionResult Wall()
        {
            return View();
        }


        #region Follow

        
        public ActionResult FollowersForUserName(string userName)
        {
            if (!string.IsNullOrEmpty(userName))
            {
                var userEntity = Offtick.Business.Membership.MembershipManager.Instance.GetUser(userName);

                IList<Follow> lsSearchResult = userEntity != null ? userEntity.Follows.ToList() : null;

                if (lsSearchResult != null && lsSearchResult.Count() > 0)
                {
                    IList<PersonalModel> lsResultToClient = new List<PersonalModel>();
                    int counter = 0;
                    foreach (var follow in lsSearchResult)
                    {
                        var follower = follow.MembershipUser1;
                        ImageFileManager imgFileManager = new ImageFileManager(follower.UserId.ToString());

                        //byte[] img32 = imgFileManager.GetBytesOfImage(imgFileManager.GetFullFileName(follow.MembershipUser1.Picture, ImageState.Thumbnail32));
                       

                        string imgAddress = imgFileManager.GetRelativeFullFileName(follow.MembershipUser1.UserId.ToString(), follow.MembershipUser1.Picture, true);



                        var userClientObj = new Offtick.Web.OfftickWeb.Areas.User.Models.PersonalModel
                        {
                            firstName = follower.FirstName,
                            lastName = follower.LastName,
                            userName = follower.UserName,
                            imageThumbnail = imgAddress,
                            fallowStatus = FollowManager.Instance.GetFollowStatusFor(MembershipManager.Instance.GetCurrentUser(), follower),
                        };
                        //lsResultToClient[counter] = userClientObj;
                        lsResultToClient.Add(userClientObj);
                        counter++;




                    }
                    //return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "found items", lsResultToClient.ToArray(), ActionResponseFromServerToClient.FollowSearchResultServerToClientKey));
                    return View(lsResultToClient);
                }
                else
                    return View();
                   // return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "No item found", string.Empty, string.Empty, string.Empty));

            }
            return View();
        }

        
        public ActionResult FollowingsForUserName(string userName)
        {
            if (!string.IsNullOrEmpty(userName))
            {
                var userEntity = MembershipManager.Instance.GetUser(userName);

                IList<Follow> lsSearchResult = userEntity != null ? userEntity.Follows1.ToList() : null;

                if (lsSearchResult != null && lsSearchResult.Count() > 0)
                {
                    IList<PersonalModel> lsResultToClient = new List<PersonalModel>();
                    int counter = 0;
                    foreach (var follow in lsSearchResult)
                    {
                        var following = follow.MembershipUser;
                        ImageFileManager imgFileManager = new ImageFileManager(following.UserId.ToString());

                        //byte[] img32 = imgFileManager.GetBytesOfImage(imgFileManager.GetFullFileName(following.Picture, ImageState.Thumbnail32));

                        string imgAddress = imgFileManager.GetRelativeFullFileName(following.UserId.ToString(), following.Picture, true);

                        var userClientObj = new PersonalModel
                        {
                            firstName = following.FirstName,
                            lastName = following.LastName,
                            userName = following.UserName,
                            // imageThumbnail = img32 != null ? Convert.ToBase64String(img32) : string.Empty,
                            imageThumbnail = imgAddress,
                            fallowStatus = FollowManager.Instance.GetFollowStatusFor(MembershipManager.Instance.GetCurrentUser(), following),
                        };
                        //lsResultToClient[counter] = userClientObj;
                        lsResultToClient.Add(userClientObj);
                        counter++;




                    }
                    return View(lsResultToClient);
                }
                else
                    return View();

            }
            return null;
        }


        [HttpPost]
        public JsonResult FollowingsForUserName_Json(string userName)
        {
            if (!string.IsNullOrEmpty(userName))
            {
                var userEntity = MembershipManager.Instance.GetUser(userName);

                IList<Follow> lsSearchResult = userEntity != null ? userEntity.Follows1.ToList() : null;

                if (lsSearchResult != null && lsSearchResult.Count() > 0)
                {
                    IList<PersonalModel> lsResultToClient = new List<PersonalModel>();
                    int counter = 0;
                    foreach (var follow in lsSearchResult)
                    {
                        var following = follow.MembershipUser;
                        ImageFileManager imgFileManager = new ImageFileManager(following.UserId.ToString());

                        //byte[] img32 = imgFileManager.GetBytesOfImage(imgFileManager.GetFullFileName(following.Picture, ImageState.Thumbnail32));

                        string imgAddress = imgFileManager.GetRelativeFullFileName(following.UserId.ToString(), following.Picture, true);

                        var userClientObj = new PersonalModel
                        {
                            firstName = following.FirstName,
                            lastName = following.LastName,
                            userName = following.UserName,
                            // imageThumbnail = img32 != null ? Convert.ToBase64String(img32) : string.Empty,
                            imageThumbnail = imgAddress,
                            fallowStatus = FollowManager.Instance.GetFollowStatusFor(MembershipManager.Instance.GetCurrentUser(), following),
                        };
                        //lsResultToClient[counter] = userClientObj;
                        lsResultToClient.Add(userClientObj);
                        counter++;




                    }
                    return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "found items", lsResultToClient.ToArray(), ActionResponseFromServerToClient.FollowSearchResultServerToClientKey));
                }
                else
                    return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "No item found", string.Empty, string.Empty, string.Empty));

            }
            return null;
        }



        [HttpPost]
        public JsonResult FollowRequestedsForUserName(string userName)
        {
            if (!string.IsNullOrEmpty(userName))
            {
                var userEntity = MembershipManager.Instance.GetUser(userName);

                IList<FollowRequested> lsSearchResult = userEntity != null ? userEntity.FollowRequesteds.ToList() : null;

                if (lsSearchResult != null && lsSearchResult.Count() > 0)
                {
                    IList<object> lsResultToClient = new List<object>();
                    int counter = 0;
                    foreach (var followRequest in lsSearchResult)
                    {
                        var follower = followRequest.MembershipUser1;
                        ImageFileManager imgFileManager = new ImageFileManager(follower.UserId.ToString());

                        //  byte[] img32 = imgFileManager.GetBytesOfImage(imgFileManager.GetFullFileName(follower.Picture, ImageState.Thumbnail32));
                        string imgAddress = imgFileManager.GetRelativeFullFileName(follower.UserId.ToString(), follower.Picture, true);



                        var userClientObj = new PersonalModel
                        {
                            firstName = follower.FirstName,
                            lastName = follower.LastName,
                            userName = follower.UserName,
                            imageThumbnail = imgAddress,
                        };
                        //lsResultToClient[counter] = userClientObj;
                        lsResultToClient.Add(userClientObj);
                        counter++;




                    }
                    return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "found items", lsResultToClient.ToArray(), ActionResponseFromServerToClient.FollowSearchResultServerToClientKey));
                }
                else
                    return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "No item found", string.Empty, string.Empty, string.Empty));

            }
            return null;
        }

        [HttpPost]
        public JsonResult FollowRejectedsForUserName(string userName)
        {
            if (!string.IsNullOrEmpty(userName))
            {
                var userEntity = MembershipManager.Instance.GetUser(userName);

                IList<FollowRejected> lsSearchResult = userEntity != null ? userEntity.FollowRejecteds1.ToList() : null;

                if (lsSearchResult != null && lsSearchResult.Count() > 0)
                {
                    IList<object> lsResultToClient = new List<object>();
                    int counter = 0;
                    foreach (var followRequest in lsSearchResult)
                    {
                        var follower = followRequest.MembershipUser;
                        ImageFileManager imgFileManager = new ImageFileManager(follower.UserId.ToString());

                        //byte[] img32 = imgFileManager.GetBytesOfImage(imgFileManager.GetFullFileName(follower.Picture, ImageState.Thumbnail32));
                        string imgAddress = imgFileManager.GetRelativeFullFileName(follower.UserId.ToString(), follower.Picture, true);



                        var userClientObj = new PersonalModel
                        {
                            firstName = follower.FirstName,
                            lastName = follower.LastName,
                            userName = follower.UserName,
                            //imageThumbnail = img32 != null ? Convert.ToBase64String(img32) : string.Empty,
                            imageThumbnail = imgAddress,
                        };
                        //lsResultToClient[counter] = userClientObj;
                        lsResultToClient.Add(userClientObj);
                        counter++;




                    }
                    return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "found items", lsResultToClient.ToArray(), ActionResponseFromServerToClient.FollowSearchResultServerToClientKey));
                }
                else
                    return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "No item found", string.Empty, string.Empty, string.Empty));

            }
            return null;
        }

        public JsonResult AcceptFollowRequestedFor(string userName, string followerUserName)
        {
            if (!string.IsNullOrEmpty(userName) && !string.IsNullOrEmpty(followerUserName))
            {
                var userEntity = MembershipManager.Instance.GetUser(userName);

                var followRequested = userEntity != null ? userEntity.FollowRequesteds.FirstOrDefault(e => e.MembershipUser1.UserName.Equals(followerUserName)) : null;

                if (followRequested != null)
                {
                    Follow follow = FollowManager.Instance.AcceptAFollow(followRequested, string.Empty);
                    if (follow != null)
                        return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "Follow Accepted", string.Empty, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
                }
                else
                    return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "No item found", string.Empty, string.Empty, string.Empty));

            }
            return null;
        }

        public JsonResult RejectFollowRequestedFor(string userName, string followerUserName)
        {
            if (!string.IsNullOrEmpty(userName) && !string.IsNullOrEmpty(followerUserName))
            {
                var userEntity = MembershipManager.Instance.GetUser(userName);

                var followRequested = userEntity != null ? userEntity.FollowRequesteds.FirstOrDefault(e => e.MembershipUser1.UserName.Equals(followerUserName)) : null;

                if (followRequested != null)
                {
                    FollowRejected followRejected = FollowManager.Instance.RejectAFollow(followRequested, string.Empty);
                    if (followRejected != null)
                        return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "Follow Rejected", string.Empty, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
                }
                else
                    return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "No item found", string.Empty, string.Empty, string.Empty));

            }
            return null;
        }

        [HttpPost]
        public JsonResult RequestAFollowFor(string userName, string followingUserName)
        {
            if (!string.IsNullOrEmpty(userName) && !string.IsNullOrEmpty(followingUserName))
            {
                var userEntity = MembershipManager.Instance.GetUser(userName);

                var followRequested = userEntity != null ? userEntity.FollowRequesteds1.FirstOrDefault(e => e.MembershipUser.UserName.Equals(followingUserName)) : null;

                if (followRequested == null)
                {
                    followRequested = FollowManager.Instance.RequestAFallow(userEntity, MembershipManager.Instance.GetUser(followingUserName));
                    if (followRequested != null)
                        return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "Follow Requested", ProfileInfoForUserName_Json(followingUserName, userName), ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
                }
                else
                    return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, "Exist Before", string.Empty, string.Empty, string.Empty));

            }
            return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.ExpiredSession, "Please login to request a follow", string.Empty, string.Empty, string.Empty));
        }

        public JsonResult RemoveAFollowFor(string userName, string followingUserName)
        {
            if (!string.IsNullOrEmpty(userName) && !string.IsNullOrEmpty(followingUserName))
            {
                var userEntity = MembershipManager.Instance.GetUser(userName);

                var follow = userEntity != null ? userEntity.Follows1.SingleOrDefault(e => e.MembershipUser.UserName.Equals(followingUserName)) : null;

                if (follow != null)
                {
                    var removed = FollowManager.Instance.RemoveAFollow(follow);
                    if (removed)
                        return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "Follow Removed", ProfileInfoForUserName_Json(followingUserName, userName), ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
 
                        //return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "Follow Removed", string.Empty, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
                }
                else
                    return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, "Not Existed", string.Empty, string.Empty, string.Empty));

            }
            return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.ExpiredSession, "Please login to request a follow", string.Empty, string.Empty, string.Empty));
        }

        [HttpPost]
        public JsonResult FollowCountsForUserName(string userName)
        {
            if (!string.IsNullOrEmpty(userName))
            {

                var userEntity = MembershipManager.Instance.GetUser(userName);

                if (userEntity != null)
                {
                    int countOfFollower = userEntity.Follows.Count();
                    int countOfFollowing = userEntity.Follows1.Count();
                    int countOfRejected = userEntity.FollowRejecteds1.Count();
                    int countOfRequested = userEntity.FollowRequesteds1.Count();
                    int countOfPost = userEntity.Offers.Where(e => e.OfferType == (int)0).Count();

                    var userClientObj = new
                    {
                        countOfFollower = countOfFollower,
                        countOfFollowing = countOfFollowing,
                        countOfRejected = countOfRejected,
                        countOfRequested = countOfRequested,
                        countOfPost = countOfPost,

                    };
                    return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "found items", userClientObj, ActionResponseFromServerToClient.FollowCountsServerToClientKey));
                }
                else
                    return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "No item found", string.Empty, string.Empty, string.Empty));

            }
            return null;
        }


        public ActionResult ProfileInfoForUserName(string userName, string followerUserName)
        {
            if (!string.IsNullOrEmpty(userName))
            {

                var followerUserEntity = MembershipManager.Instance.GetUser(followerUserName);
                var userEntity =  MembershipManager.Instance.GetUser(userName) ;
                if (userEntity != null)
                {
                    int countOfFollower = userEntity.Follows.Count();
                    int countOfFollowing = userEntity.Follows1.Count();
                    int countOfPost = userEntity.Offers.Where(e => e.OfferType != (int)0).Count();

                    ImageFileManager imgFileManager = new ImageFileManager(userEntity.UserId.ToString());
                    string img32Address = string.Empty;
                    if (!string.IsNullOrEmpty(userEntity.Picture))
                    {
                        img32Address = imgFileManager.GetRelativeFullFileName(userEntity.UserId.ToString(), userEntity.Picture, true);
                    }
                    var followStatus = FollowManager.Instance.GetFollowStatusFor(followerUserEntity, userEntity);// userEntity.Follows1.FirstOrDefault(e => e.UserId == followerUserEntity.UserId) != null ? 1 : 0;
                    bool isBlocked = false;

                    var userClientObj = new ProfileHeaderInfo
                    {
                        firstName = userEntity.FirstName,
                        lastName = userEntity.LastName,
                        userName = userEntity.UserName,
                        imageThumbnail = img32Address,
                        countOfFollower = countOfFollower,
                        countOfFollowing = countOfFollowing,
                        countOfPost = countOfPost,
                        age = userEntity.BirthDate.HasValue ? (int)((DateTime.Now - userEntity.BirthDate.Value).TotalDays / 365) : 0,
                        fallowStatus = followStatus,
                        isBlocked = isBlocked ? (short)1 : (short)0
                    };
                    return View(userClientObj);
                    //return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "found items", userClientObj, ActionResponseFromServerToClient.ProfileInfoServerToClientKey));
                }
                else
                    return View();
                    //return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "No item found", string.Empty, string.Empty, string.Empty));

            }
            return View();
        }
        public ActionResult ProfileInfoForUserName_deprecated(string userName, string followerUserName)
        {
            if (!string.IsNullOrEmpty(followerUserName))
            {

                var followerUserEntity = MembershipManager.Instance.GetUser(followerUserName);
                var userEntity = !string.IsNullOrEmpty(followerUserName) ? MembershipManager.Instance.GetUser(userName) : null;
                if (followerUserEntity != null)
                {
                    int countOfFollower = followerUserEntity.Follows.Count();
                    int countOfFollowing = followerUserEntity.Follows1.Count();
                    int countOfPost = followerUserEntity.Offers.Where(e => e.OfferType != (int)0).Count();

                    ImageFileManager imgFileManager = new ImageFileManager(followerUserEntity.UserId.ToString());
                    string img32Address = string.Empty;
                    if (!string.IsNullOrEmpty(followerUserEntity.Picture))
                    {
                        img32Address = imgFileManager.GetRelativeFullFileName(followerUserEntity.UserId.ToString(), followerUserEntity.Picture, true);
                    }
                    var followStatus = FollowManager.Instance.GetFollowStatusFor(userEntity, followerUserEntity);// userEntity.Follows1.FirstOrDefault(e => e.UserId == followerUserEntity.UserId) != null ? 1 : 0;
                    bool isBlocked = false;

                    var userClientObj = new ProfileHeaderInfo
                    {
                        firstName = followerUserEntity.FirstName,
                        lastName = followerUserEntity.LastName,
                        userName = followerUserEntity.UserName,
                        imageThumbnail = img32Address,
                        countOfFollower = countOfFollower,
                        countOfFollowing = countOfFollowing,
                        countOfPost = countOfPost,
                        age = followerUserEntity.BirthDate.HasValue ? (int)((DateTime.Now - followerUserEntity.BirthDate.Value).TotalDays / 365) : 0,
                        fallowStatus = followStatus,
                        isBlocked = isBlocked ? (short)1 : (short)0
                    };
                    return View(userClientObj);
                    //return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "found items", userClientObj, ActionResponseFromServerToClient.ProfileInfoServerToClientKey));
                }
                else
                    return View();
                //return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "No item found", string.Empty, string.Empty, string.Empty));

            }
            return View();
        }


        public JsonResult ProfileInfoForUserName_Json(string userName, string followerUserName)
        {
            if (!string.IsNullOrEmpty(userName))
            {

                var followerUserEntity = MembershipManager.Instance.GetUser(followerUserName);
                var userEntity = MembershipManager.Instance.GetUser(userName);
                if (userEntity != null)
                {
                    int countOfFollower = userEntity.Follows.Count();
                    int countOfFollowing = userEntity.Follows1.Count();
                    int countOfPost = userEntity.Offers.Where(e => e.OfferType != (int)0).Count();

                    ImageFileManager imgFileManager = new ImageFileManager(userEntity.UserId.ToString());
                    byte[] img32 = imgFileManager.GetBytesOfImage(imgFileManager.GetFullFileName(userEntity.Picture, true));
                    var followStatus = FollowManager.Instance.GetFollowStatusFor(followerUserEntity, userEntity); 
                    bool isBlocked = false;

                    var userClientObj = new ProfileHeaderInfo
                    {
                        firstName = userEntity.FirstName,
                        lastName = userEntity.LastName,
                        userName = userEntity.UserName,
                        imageThumbnail = img32 != null ? Convert.ToBase64String(img32) : string.Empty,
                        countOfFollower = countOfFollower,
                        countOfFollowing = countOfFollowing,
                        countOfPost = countOfPost,
                        age = userEntity.BirthDate.HasValue ? (int)((DateTime.Now - userEntity.BirthDate.Value).TotalDays / 365) : 0,
                        fallowStatus = followStatus,
                        isBlocked = isBlocked ? (short)1 : (short)0
                    };
                    return Json(userClientObj);
                   //return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "found items", userClientObj, ActionResponseFromServerToClient.ProfileInfoServerToClientKey));
                }
                else
                    return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "No item found", string.Empty, string.Empty, string.Empty));

            }
            return null;
        }
        public JsonResult ProfileInfoForUserName_Json_Deprecatd(string userName, string followerUserName)
        {
            if (!string.IsNullOrEmpty(followerUserName))
            {

                var followerUserEntity = MembershipManager.Instance.GetUser(followerUserName);
                var userEntity = !string.IsNullOrEmpty(followerUserName) ? MembershipManager.Instance.GetUser(userName) : null;
                if (followerUserEntity != null)
                {
                    int countOfFollower = followerUserEntity.Follows.Count();
                    int countOfFollowing = followerUserEntity.Follows1.Count();
                    int countOfPost = followerUserEntity.Offers.Where(e => e.OfferType != (int)0).Count();

                    ImageFileManager imgFileManager = new ImageFileManager(followerUserEntity.UserId.ToString());
                    byte[] img32 = imgFileManager.GetBytesOfImage(imgFileManager.GetFullFileName(followerUserEntity.Picture, true));
                    var followStatus = FollowManager.Instance.GetFollowStatusFor(userEntity, followerUserEntity);
                    bool isBlocked = false;

                    var userClientObj = new ProfileHeaderInfo
                    {
                        firstName = followerUserEntity.FirstName,
                        lastName = followerUserEntity.LastName,
                        userName = followerUserEntity.UserName,
                        imageThumbnail = img32 != null ? Convert.ToBase64String(img32) : string.Empty,
                        countOfFollower = countOfFollower,
                        countOfFollowing = countOfFollowing,
                        countOfPost = countOfPost,
                        age = followerUserEntity.BirthDate.HasValue ? (int)((DateTime.Now - followerUserEntity.BirthDate.Value).TotalDays / 365) : 0,
                        fallowStatus = followStatus,
                        isBlocked = isBlocked ? (short)1 : (short)0
                    };
                    return Json(userClientObj);
                    //return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "found items", userClientObj, ActionResponseFromServerToClient.ProfileInfoServerToClientKey));
                }
                else
                    return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "No item found", string.Empty, string.Empty, string.Empty));

            }
            return null;
        }


        public ActionResult FollowsForUserName(string userName)
        {
            if (!string.IsNullOrEmpty(userName))
            {
                var userEntity = MembershipManager.Instance.GetUser(userName);
                return View(userEntity);

            }
            return View();
        }
        #endregion


        #region Content
        [Authorize]
        public ActionResult PlusAction(string selectedAction)
        {
                switch (selectedAction)
                {
                    case "plus_photo":
                        return RedirectToAction("PostPhoto", "Offer", new { area="User"});
                    case "plus_gallery":
                        return RedirectToAction("Index", "Gallery", new { area = "User" });

                    case "plus_video":
                        return RedirectToAction("PostVideo", "Offer", new { area = "User" });
                    case "plus_food":
                        return RedirectToAction("OfftickOffer", "Offer", new { area = "User", offtickOfferType = "resturant" });

                    case "plus_theather":
                        return RedirectToAction("OfftickOffer", "Offer", new { area = "User", offtickOfferType = "theather" });
                    case "plus_intertainment":
                        return RedirectToAction("OfftickOffer", "Offer", new { area = "User", offtickOfferType = "intertainment" });
                    case "plus_tour":
                        return RedirectToAction("TourOffer", "Offer", new { area = "User"});
                    case "plus_health":
                        return RedirectToAction("OfftickOffer", "Offer", new { area = "User", offtickOfferType = "health" });
                    case "plus_stuff":
                        return RedirectToAction("OfftickOffer", "Offer", new { area = "User", offtickOfferType = "stuff" });
                    case "plus_learning":
                        return RedirectToAction("OfftickOffer", "Offer", new { area = "User", offtickOfferType = "learning" });
                    case "plus_beauty":
                        return RedirectToAction("OfftickOffer", "Offer", new { area = "User", offtickOfferType = "beauty" });
                    case "plus_service":
                        return RedirectToAction("OfftickOffer", "Offer", new { area = "User", offtickOfferType = "service" });
                    case "plus_builing":
                        return RedirectToAction("OfftickOffer", "Offer", new { area = "User", offtickOfferType = "builing" });
                    case "plus_car":
                        return RedirectToAction("OfftickOffer", "Offer", new { area = "User", offtickOfferType = "car" });
                    case "plus_special":
                        return RedirectToAction("OfftickOffer", "Offer", new { area = "User", offtickOfferType = "special" });

                    default:
                        return View();
                }
        }


        public ActionResult ShowContent(Guid menuId)
        {
            var menu = Offtick.Business.Services.Managers.ContentManager.Instance.GetMenu(menuId);
            if (!menu.IsSystemMenu)
                return View(menu);
            else
                return View(menu.MenuSystemic.SectionName, menu);


        }
        #endregion


        public ActionResult ShowUserStatus(MembershipUser user,int maxWidth,bool showNameAndFamily=false)
        {
            ViewBag.maxWidth = maxWidth;
            ViewBag.showNameAndFamily = showNameAndFamily;
            return View(user);
        }
        public ActionResult ShowUserStatusFull(MembershipUser user)
        {
            return View(user);
        }
        public ActionResult ShowUserStatusLinear(MembershipUser user)
        {
            return View(user);
        }
        public ActionResult ShowUserStatusMin(MembershipUser user)
        {
            return View(user);
        }

        

        #region subscription 
        public ActionResult SubscribeNewUser(MembershipUser user, MembershipCity city){
            return View();
        }
        #endregion



        public ActionResult Scripts()
        {
            return View();
        }


        #region EditProfile
        public ActionResult EditProfileCP()
        {
            return View();
        }

        [HttpGet]
        public ActionResult EditProfileChangePhoto()
        {
            return View();
        }

        [HttpPost, Authorize]
        public ActionResult EditProfileChangePhotoUploader()
        {
            var currentUser = MembershipManager.Instance.GetCurrentUser();
            if (Request.Files != null && Request.Files.Count != 0)
            {
                var responseEdit = MembershipManager.Instance.ChangePhoto(currentUser, Request.Files[0]);
                if (responseEdit == Core.EnumTypes.EditStatus.Edited)
                        return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, "", "", "", ""));
            
            }
            return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "", "", "", "")); 
        }

        [HttpGet,Authorize]
        public ActionResult EditProfileChangeInfo()
        {
            var cu = MembershipManager.Instance.GetCurrentUser();
            var userCity = cu.MembershipLocations.FirstOrDefault();

            var model = new EditProfileBaseInfoModel()
            {
                FirstName = cu.FirstName,
                LastName = cu.LastName,
                Address = cu.Address,
                BirthDate = cu.BirthDate.HasValue? Offtick.Core.Utility.PersianTools.PersianDateConvertor.ToKhorshidiDate( cu.BirthDate.Value):string.Empty,
                Domain = cu.DomainUrl,
                Gender = cu.Gender,
                PhoneNumber = cu.PhoneNumber,
                CityId = userCity!=null?userCity.MembershipCity.CityId:default(Guid),
                Email=cu.Email,
            };
            return View(model);
        }
        [HttpPost, Authorize]
        public ActionResult EditProfileChangeInfo(EditProfileBaseInfoModel model)
        {
            var cu = MembershipManager.Instance.GetCurrentUser();
            DateTime? dt = null;
            if (!string.IsNullOrEmpty(model.BirthDate))
                dt = PersianDateConvertor.ToGregorian(model.BirthDate);

            if(cu.Email!=model.Email && !string.IsNullOrEmpty( model.Email) && MembershipManager.Instance.ExistEmail(model.Email))
                 return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, UIMessages.ExistedEmail, "", "", ""));
            if (cu.PhoneNumber != model.PhoneNumber && !string.IsNullOrEmpty(model.PhoneNumber) && MembershipManager.Instance.ExistPhoneNumber(model.PhoneNumber))
                return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, UIMessages.ExistedEmail, "", "", ""));

            var city = MembershipManager.Instance.getCityById(model.CityId);
            if(city==null)
                return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, UIMessages.ExistedEmail, "", "", ""));

            var editStatus = MembershipManager.Instance.UpdateInfo(cu, model.FirstName, model.LastName, model.Gender, dt, model.Domain, model.Address, model.PhoneNumber,model.CityId,model.Email);
            if (editStatus == Core.EnumTypes.EditStatus.Edited || editStatus == Core.EnumTypes.EditStatus.RejectedByNonEffectiveQuery)
                return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, "", "", "", ""));
            else
            return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, UIMessages.RejectedByInternalSystemError, "", "", "")); 
        }

        [Authorize]
        public ActionResult EditProfileChangeSpecification()
        {
            var cu = MembershipManager.Instance.GetCurrentUser();
            var infoObj = cu.MembershipIntroduceInfo;
            
            OwnerSpecificationModel model=new OwnerSpecificationModel();
            model.UserName = cu.UserName;

            if(infoObj!=null)
            {
                model.Specification=infoObj.Specification;
                model.ResponsibleName = infoObj.ResponsibleName;
                model.ResponsiblePhone = infoObj.ResponsiblePhone;
                model.ResponsibleEmail = infoObj.ResponsibleEmail;
            };
            return View("EditProfileChangeSpecification", model);
        }


        [HttpPost, Authorize]
        public ActionResult EditProfileChangeSpecification(OwnerSpecificationModel model)
        {
            var cu = MembershipManager.Instance.GetCurrentUser();
            if (model == null)
                return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "", "", "", ""));

            var editStatus = MembershipManager.Instance.UpdateSpecification(cu, model.Specification,model.ResponsibleName,model.ResponsiblePhone,model.ResponsibleEmail);
            if (editStatus == Core.EnumTypes.EditStatus.Edited)
                return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, "", "", "", ""));
            return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "", "", "", ""));
        }


        [HttpGet]
        public ActionResult ChangeUserName()
        {
            ChangeUserNameModel model = new ChangeUserNameModel()
            {
                UserName = MembershipManager.Instance.GetCurrentUserName(),
            };
            return View(model);
        }
        [HttpPost]
        public ActionResult ChangeUserName(ChangeUserNameModel model)
        {
            if (ModelState.IsValid)
            {
                var cu = MembershipManager.Instance.GetCurrentUser();
                if (cu == null || model == null || cu.UserName != model.UserName)
                    return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "خطای داخلی سیستم، لطفا ابتدا از سیستم خارج شده و مجددا سعی نمایید", "", "", ""));
                var editStatus = MembershipManager.Instance.ChangeUserName(model.UserName, model.NewUserName);
                if (editStatus == Core.EnumTypes.EditStatus.Edited)
                    return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, "", "", "", ""));
            }
            return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "خطای داخلی سیستم، لطفا ابتدا از سیستم خارج شده و مجددا سعی نمایید", "", "", ""));

        }


        #endregion

        #region delete-disable account
        [Authorize]
        public ActionResult DisableAccount()
        {
            return View();
        }

        [HttpPost,Authorize]
        public ActionResult DisableAccount(string userName)
        {
           var user= Offtick.Business.Membership.MembershipManager.Instance.GetUser(userName);
           var currentUser = Offtick.Business.Membership.MembershipManager.Instance.GetCurrentUser();
           if (user != null && currentUser != null && user.UserName == currentUser.UserName)
           {
              var editStatus= MembershipManager.Instance.ChangeStatus(user, MembershipStatus.Disabled);
              if (editStatus == Core.EnumTypes.EditStatus.Edited)
              {
                  System.Web.Security.FormsAuthentication.SignOut();
                  return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, "", "", "", ""));
              }
           }
          // ModelState.AddModelError("خطا در غیر فعال سازی");
           return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "", "", "", ""));
        }
        #endregion


        #region profile header
        public ActionResult ProfileMenuBar(string userName)
        {
            return View("ProfileMenuBar", (object)userName);
        }
        public ActionResult ProfileSocialHeader(string userName)
        {
            return View("ProfileSocialHeader", (object)userName);

        }
        public ActionResult ProfileHeader(string userName)
        {
            return View("ProfileHeader", (object)userName);

        }
        #endregion



        #region show Viewers, Visitors, Followers ...
        public ActionResult ShowUsers(IList<MembershipUser> users)
        {
            return View("ShowUsers", users);
        }
        public ActionResult ShowUsersLinear(IList<MembershipUser> users)
        {
            return View("ShowUsers", users);
        }
        #endregion



    }

}
