﻿using Offtick.Business.Services.Domain.VirtualBasket;
using Offtick.Business.Services.Storage;
using Offtick.Business.Web;
using Offtick.Web.OfftickWeb.Areas.User.Models;
using Offtick.Web.OfftickWeb.Infrustracture;
using PaymentGatewayService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Offtick.Web.OfftickWeb.Areas.User.Controllers
{
    public class EPaymentController : BaseController
    {
        //
        // GET: /User/EPayment/

        public EPaymentController(DataContextContainer obj)
            : base(obj)
        {
        }

        public ActionResult Index()
        {
            return View();
        }


        public ActionResult TestPage()
        {
            return View();
        }


        [HttpPost]
        public ActionResult PayRequest(PayRequestModel model)
        {
            int step = 0;
            try
            {
                string result = "";
                
                PaymentGatewayEndpoint endPoint = new PaymentGatewayEndpoint();
                bool isSuccess = false;
                var currentUser = Offtick.Business.Membership.MembershipManager.Instance.GetCurrentUser();
                Guid? userId = currentUser != null ? (Guid?)currentUser.UserId : null;

                string _date = DateTime.Now.ToString("yyyyMMdd");
                string _time = DateTime.Now.ToString("HHmmss");
                string _additionalData = "data";
                string _exceptionMessage = string.Empty;
                endPoint.PayRequest(userId, model.orderId, model.amount*10, _date, _time, _additionalData, model.payerId, (successOrderId, refId) =>
                {
                    isSuccess = true;
                    result = refId;
                    step = 1;
                }, (failOrderId, resCode,exceptionMessage) => {
                    isSuccess = false;
                    result = resCode.ToString();
                    _exceptionMessage = exceptionMessage;
                    step = 2;
                });


                return Json(new { isSuccess = isSuccess, Data = result, exceptionMessage = _exceptionMessage,step=step });
            }
            catch (Exception ex)
            {
                step = 3;
                return Json(new { isSuccess = false, Data = string.Empty, exceptionMessage = ex.Message, step = step });
            }
        }


        [HttpPost]
        public ActionResult PayRequestFake(PayRequestModel model)
        {
            int step = 0;
            try
            {
                string result = "";

                PaymentGatewayEndpoint endPoint = new PaymentGatewayEndpoint();
                bool isSuccess = false;
                var currentUser = Offtick.Business.Membership.MembershipManager.Instance.GetCurrentUser();
                Guid? userId = currentUser != null ? (Guid?)currentUser.UserId : null;

                string _date = DateTime.Now.ToString("yyyyMMdd");
                string _time = DateTime.Now.ToString("HHmmss");
                string _exceptionMessage = string.Empty;
               
                    isSuccess = true;
                    result = "FakeRef_"+GeneralHelper.GetDateTimeToString(DateTime.Now);
                    step = 1;
               

                return Json(new { isSuccess = isSuccess, Data = result, exceptionMessage = _exceptionMessage, step = step });
            }
            catch (Exception ex)
            {
                step = 3;
                return Json(new { isSuccess = false, Data = string.Empty, exceptionMessage = ex.Message, step = step });
            }
        }




        
        public ActionResult ComebackPgw(bool passAuthority=false)
        {
            var currentUser = Offtick.Business.Membership.MembershipManager.Instance.GetCurrentUser();
            string _refId = string.Empty;
            short _resCode = -1;
            long? _saleReferenceId = -1;
            long? _saleOrderId = -1;
            Guid? userId = null;
            if (currentUser != null) userId = currentUser.UserId;

            if (passAuthority)
            {
                var basketOrder = Offtick.Business.Services.Domain.VirtualBasket.VirtualBasketManager.Instance.GetListOfBasketEntitiesForCurrentUser();
                if (basketOrder != null)
                {
                    _refId = basketOrder.GatewayResponse.RefId;
                    _resCode = basketOrder.GatewayResponse.ResCode;
                    _saleOrderId =  basketOrder.GatewayResponse.SaleOrderId;
                    _saleReferenceId =  basketOrder.GatewayResponse.SaleReferenceId;
                }

            }
            else
            {
                if (!GeneralHelper.IsFakePayment())
                {
                    PaymentGatewayEndpoint endPoint = new PaymentGatewayEndpoint();
                    endPoint.ComebackFromPaymentGateway(new PaymentGatewayComebackAction(
                        (refId, resCode, saleOrderId, saleReferenceId) =>
                        {
                            _refId = refId;
                            _resCode = resCode;
                            _saleOrderId = saleOrderId;
                            _saleReferenceId = saleReferenceId;
                        }
                    ), userId, false, false);
                }
                else
                {
                    _refId = "FakeRefId_" + GeneralHelper.GetDateTimeToString(DateTime.Now);
                    _resCode = 0;
                    var basketOrder = Offtick.Business.Services.Domain.VirtualBasket.VirtualBasketManager.Instance.GetListOfBasketEntitiesForCurrentUser();
                    _saleOrderId =long.Parse( basketOrder.OrderId);
                    _saleReferenceId = long.Parse(GeneralHelper.GetDateTimeToString(DateTime.Now));
                }
            }

            
            return InterpretResult(_refId, _resCode, _saleReferenceId, _saleOrderId);

        }

        public ActionResult ExpiredSession()
        {
            return View("ExpiredSession");
        }
        public ActionResult BasketChanged()
        {
            return View("BasketChanged");
        }
        public ActionResult ShowResult(PaymentResult result)
        {
            return View("ShowResult", result);
        }
        private ActionResult InterpretResult(string refId, short resCode, long? saleReferenceId, long? saleOrderId)
        {
             var currentUser = Offtick.Business.Membership.MembershipManager.Instance.GetCurrentUser();
         //    if (currentUser == null)
         //        return ExpiredSession();
             Guid? userId = currentUser != null ? (Guid?)currentUser.UserId : null;
             string userName = currentUser != null ? currentUser.UserName : string.Empty;

            var basketOrder = Offtick.Business.Services.Domain.VirtualBasket.VirtualBasketManager.Instance.GetListOfBasketEntitiesForCurrentUser();
            if (basketOrder == null)
                return View("BasketExpired");

            if (currentUser == null)
            {
                basketOrder.SetGatewayResponse(refId, resCode, saleReferenceId, saleOrderId);
                return RedirectToAction("VerifyAuthority");
            }


            long existedOrderId = -1;
             long.TryParse(basketOrder.OrderId, out existedOrderId);
             PaymentGatewayEndpoint endPoint = new PaymentGatewayEndpoint();
            
            //Response.Write(string.Format("{0} ,{1} ,{2} ,{3} ,{4} ,{5}", refId, resCode, saleReferenceId, saleOrderId,existedOrderId, basketOrder!=null?"hasBasket":"NoBasket"));

            if (existedOrderId != -1 && existedOrderId == saleOrderId)
            {

                bool isSuccessVerify = false;
                if (resCode == 0)
                {
                    if (!GeneralHelper.IsFakePayment())
                    {
                        endPoint.VerifyRequest(userId, existedOrderId, existedOrderId, saleReferenceId.Value, new PaymentGatewaySuccessAction((l1) =>
                        {
                            isSuccessVerify = true;
                        }), new PaymentGatewayFailedAction((l2, l3, exeptionMessage) =>
                        {
                            isSuccessVerify = false;
                        }));
                    }
                    else
                    {
                        isSuccessVerify = true;
                    }
                }
                PaymentResult model;
                if (isSuccessVerify)
                {
                     model = new PaymentResult()
                    {
                        refId = refId,
                        resCode = resCode,
                        saleOrderId = saleOrderId,
                        saleReferenceId = saleReferenceId,
                        isSucess=true,
                    };
                   
                    // register offer order sale
                    OfferController offerController = new OfferController(DataContextManager.Container);
                    foreach(var entity in  basketOrder.Entities)
                    {
                        offerController.SaleFoodOffer(userName, entity.Entity.OfferId.ToString(),saleReferenceId.Value.ToString(), (short)entity.Count, saleOrderId.Value,entity.IsBuyingVisible);
                    }

                    //remove from basket
                    Offtick.Business.Services.Domain.VirtualBasket.VirtualBasketManager.Instance.ProcessSaleDoneForCurrentUser();

                    //present the result to user

                   
                }
                else
                {
                     model = new PaymentResult()
                    {
                        refId = refId,
                        resCode = resCode,
                        saleOrderId = saleOrderId,
                        saleReferenceId = saleReferenceId,
                        isSucess = false,
                    };
                }
                return RedirectToAction("ShowResult",model);

            }
            else
            { //basket entity has been removed or its order id has been changed: means add or remove item from basket
                if (saleOrderId.HasValue && saleReferenceId.HasValue)
                    endPoint.ReversalRequest(userId, saleOrderId.Value, saleOrderId.Value, saleReferenceId.Value, null, null);
                return BasketChanged();

            }
        }

        [HttpPost]
         public ActionResult OtherRequest(InquiryVerifyReversalSettleRequestModel model)
         {
             string result = "";
             PaymentGatewayEndpoint endPoint = new PaymentGatewayEndpoint();
             var userId = Offtick.Business.Membership.MembershipManager.Instance.GetCurrentUser().UserId;
             if (model != null)
             {
                 switch (model.actionType)
                 {
                     case 1:
                         endPoint.VerifyRequest(userId,model.OrderId, model.SaleOrderId, model.SaleReferenceId, (successOrderId) =>
                         {
                             result = "Success:"+successOrderId;
                         }, (failOrderId, resCode,exeptionMessage) => {  result = "failed:"+failOrderId+ " Code:"+ resCode.ToString(); });
                         break;
                     case 2:
                         endPoint.InquiryRequest(userId, model.OrderId, model.SaleOrderId, model.SaleReferenceId, (successOrderId) =>
                         {
                             result = "Success:" + successOrderId;
                         }, (failOrderId, resCode, exeptionMessage) => { result = "failed:" + failOrderId + " Code:" + resCode.ToString(); });
                         break;
                     case 3:
                         endPoint.ReversalRequest(userId, model.OrderId, model.SaleOrderId, model.SaleReferenceId, (successOrderId) =>
                         {
                             result = "Success:" + successOrderId;
                         }, (failOrderId, resCode, exeptionMessage) => { result = "failed:" + failOrderId + " Code:" + resCode.ToString(); });
                         break;
                     case 4:
                         endPoint.SettleRequest(userId, model.OrderId, model.SaleOrderId, model.SaleReferenceId, (successOrderId) =>
                         {
                             result = "Success:" + successOrderId;
                         }, (failOrderId, resCode, exeptionMessage) => { result = "failed:" + failOrderId + " Code:" + resCode.ToString(); });
                         break;
                 }
             }
             return Json(new {Data=result});
         }


        public ActionResult VerifyAuthority()
        {
            var currentUser = Offtick.Business.Membership.MembershipManager.Instance.GetCurrentUser();
            if (currentUser != null)
            {
                return ComebackPgw(true);
            }
            else
                return View("VerifyAuthority");
        }

    }
}
