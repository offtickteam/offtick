﻿using Offtick.Business.Membership;
using Offtick.Business.Services.Managers;
using Offtick.Business.Services.Storage;
using Offtick.Business.Services.Storage.File;
using Offtick.Business.Web;
using Offtick.Web.OfftickWeb.Areas.User.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Offtick.Web.OfftickWeb.Areas.User.Controllers
{
    public class SearchController :  BaseController
    {

        public SearchController(DataContextContainer obj)
            : base(obj)
        {
        }
        //
        // GET: /User/Search/

        public ActionResult Index(string searchValue)
        {
            return View((object)searchValue);
        }

        public ActionResult Resturants()
        {
            
            return View();
        }

        public ActionResult OfftickOffer(string offerName)
        {
            ViewBag.offerName = offerName;
            return View();
        }




      

    }
}
