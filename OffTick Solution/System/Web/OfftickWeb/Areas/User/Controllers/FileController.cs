﻿using Offtick.Business.Membership;
using Offtick.Business.Services.Domain;
using Offtick.Business.Services.Domain.PdfGenerator;
using Offtick.Business.Services.Storage;
using Offtick.Business.Services.Storage.File;
using Offtick.Business.Web;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Offtick.Web.OfftickWeb.Areas.User.Controllers
{
    public class FileController :  BaseController
    {
        public FileController(DataContextContainer obj)
            : base(obj)
        {

        }
    
        //
        // GET: /User/File/

        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        public FileContentResult Pdf(string userName, string fileName)
        {
            var user=MembershipManager.Instance.GetUser(userName);
            if (user == null || string.IsNullOrEmpty(fileName))
                return null;

            PdfFileManager pdfFileManager = new PdfFileManager(user.UserId.ToString());
            string filePath = pdfFileManager.GetFullFileName(fileName);
            if (pdfFileManager.IsExit(filePath))
            {
                byte[] filedata = System.IO.File.ReadAllBytes(filePath);

                //var cd = new System.Net.Mime.ContentDisposition
                //{
                //    FileName = filename,
                //    Inline = true,
                //};

                //Response.AppendHeader("Content-Disposition", cd.ToString());


                Response.AppendHeader("content-disposition", "inline; filename=file.pdf");

                // Return the output stream
                return new FileContentResult(filedata, "application/pdf");
            }
            else
                return null;

        }

       


        public FileContentResult testCreatePDF(string userName, string fileName)
        {
            var user = MembershipManager.Instance.GetUser(userName);
            if (user == null || string.IsNullOrEmpty(fileName))
                return null;

            PdfFileManager pdfFileManager = new PdfFileManager(user.UserId.ToString());
            string filePath = pdfFileManager.GetFullFileName(fileName);
            if (pdfFileManager.IsExit(filePath))
            {
                byte[] filedata = System.IO.File.ReadAllBytes(filePath);

                //var cd = new System.Net.Mime.ContentDisposition
                //{
                //    FileName = filename,
                //    Inline = true,
                //};

                //Response.AppendHeader("Content-Disposition", cd.ToString());


                Response.AppendHeader("content-disposition", "inline; filename=file.pdf");

                // Return the output stream
                return new FileContentResult(filedata, "application/pdf");
            }
            else
                return null;
        }

    }
}
