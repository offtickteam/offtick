﻿using Offtick.Business.Membership;
using Offtick.Business.Services.Managers;
using Offtick.Business.Services.Storage;
using Offtick.Business.Web;
using Offtick.Data.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Offtick.Web.OfftickWeb.Areas.User.Controllers
{
    public class SecurityController : BaseController
    {
        public SecurityController(DataContextContainer obj)
            : base(obj)
        {

        }
        //
        // GET: /User/Security/

        [Authorize]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult UpdateGeneralUserPrivacy(string userName, bool isViewingVisible, bool isBuyingVisible, bool IsVisitingVisible, bool IsVisitorVisible)
        {
            var user = MembershipManager.Instance.GetUser(userName);
            var editStatus = SecurityManager.Instance.UpdateGeneralSecurityForUser(user, isViewingVisible, isBuyingVisible, IsVisitingVisible, IsVisitorVisible);
            if(editStatus == Core.EnumTypes.EditStatus.Edited || editStatus== Core.EnumTypes.EditStatus.RejectedByNonEffectiveQuery)
                return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "تغییرات ذخیره گردید", "", ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
            else
                return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "خطای داخلی رخ داده است.بعدا سعی نمایید", string.Empty, string.Empty, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));

        }

        [Authorize]
        public ActionResult SetOfferWall(string offerId)
        {
            var user = MembershipManager.Instance.GetCurrentUser();
            Guid guidOfferId;
            if(Guid.TryParse(offerId,out guidOfferId)){
                var offer=OfferManager.Instance.GetOffer(guidOfferId);
                var editStatus = OfferManager.Instance.SetOfferWall(offer);
                if (editStatus == Core.EnumTypes.EditStatus.Edited || editStatus == Core.EnumTypes.EditStatus.RejectedByNonEffectiveQuery)
                    return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "تغییرات ذخیره گردید", "", ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
            }
            return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "خطای داخلی رخ داده است.بعدا سعی نمایید", string.Empty, string.Empty, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
        }


    }
}
