﻿using Offtick.Business.Membership;
using Offtick.Business.Services.Managers;
using Offtick.Business.Services.Storage;
using Offtick.Business.Services.Storage.File;
using Offtick.Business.Web;
using Offtick.Core.EnumTypes;
using Offtick.Data.Entities.Common;
using Offtick.Web.OfftickWeb.Infrustracture;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Offtick.Web.OfftickWeb.Areas.User.Controllers
{
    public class GalleryController : BaseController
    {
        //
        // GET: /Dr/Gallery/

        public ActionResult Index()
        {
            var currentUser = MembershipManager.Instance.GetCurrentUser();
            return View(currentUser != null ? currentUser.Galleries.Where(e => e.ParentGaleryId == null) : null);
        }



        public GalleryController(DataContextContainer obj)
            : base(obj)
        {

        }



        [HttpPost]
        public ActionResult UploadFileLoader()
        {
            var length = Request.ContentLength;
            string title = Request.Form["Title"];
            string description = Request.Form["Description"];
            string galleryId = Request.Form["GalleryId"];

            if (Request.Files != null && Request.Files.Count != 0 && !String.IsNullOrEmpty(galleryId))
            {
                GalleryManager.Instance.AddImgageGallery(title, description, Guid.Parse(galleryId), MembershipManager.Instance.GetCurrentUser().UserId, Request.Files);
            }
            return null;
        }



        [HttpPost]
        public ActionResult AdaptGallery(Offtick.Web.OfftickWeb.Areas.User.Models.AdaptGalleryModel gallery)
        {
            string msg = string.Empty;
            if (gallery != null && gallery.AdaptMode == Models.AdaptGalleryMode.Add)
            {
                AddStatus status = GalleryManager.Instance.AddGallery(gallery.Title, gallery.Description, gallery.ParentId, gallery.IsDefault);
                msg = UIActionStatusToUiMessage.ActionStatusToMessage(status);
            }
            else if (gallery.Id.HasValue)
            {
                EditStatus status = GalleryManager.Instance.EditGallery(gallery.Title, gallery.Description, gallery.Id.Value, gallery.IsDefault);
                msg = UIActionStatusToUiMessage.ActionStatusToMessage(status);
            }
            return Json(msg);
        }

        /// <summary>
        /// Return Gallery Menu for Current User
        /// </summary>
        /// <param name="imageGalleryMenuID"></param>
        /// <param name="updateTargetId"></param>
        /// <returns></returns>
        public ActionResult GalleryMenu(string imageGalleryMenuID, string updateTargetId)
        {
            var currentUser = MembershipManager.Instance.GetCurrentUser();
            var gallerisOfCurrentUser = currentUser != null ? currentUser.Galleries.Where(e => e.ParentGaleryId == null) : null;
            var strMenu = Offtick.Business.Web.GalleryImageHelper.CreateGalleryMenu(gallerisOfCurrentUser,
                LangaugeManager.Instance.GetDefaultLangaugeId()
                , imageGalleryMenuID, string.Empty, updateTargetId);
            return Content(strMenu.ToString());


        }
        public ActionResult GalleryHeaderInfo(string galleryObjectId)
        {

            if (!string.IsNullOrEmpty(galleryObjectId))
            {

                return View(GalleryManager.Instance.GetGallery(Guid.Parse(galleryObjectId)));
            }
            else
                return View();
        }


        public ActionResult GalleryBodyManagement(string galleryId)
        {
            if (!string.IsNullOrEmpty(galleryId))
            {
                var obj = GalleryManager.Instance.GetGallery(Guid.Parse(galleryId));
                return View(obj);
            }
            else
                return View();
        }

        public ActionResult GalleryImageWrapperCollection(string galleryId)
        {
            if (!string.IsNullOrEmpty(galleryId))
            {
                var obj = GalleryManager.Instance.GetGallery(Guid.Parse(galleryId));
                return View(obj.GalleryImages);
            }
            else
                return View();


        }

        public ActionResult GalleryImageWrapperCollectionOtherProfile(string galleryId)
        {
            if (!string.IsNullOrEmpty(galleryId))
            {
                var obj = GalleryManager.Instance.GetGallery(Guid.Parse(galleryId));
                return View(obj.GalleryImages);
            }
            else
                return View();


        }
        public ActionResult GalleryImageWrapper(string galleryImageId)
        {
            if (!string.IsNullOrEmpty(galleryImageId))
            {
                var obj = GalleryManager.Instance.GetGalleryImage(Guid.Parse(galleryImageId));
                return View(obj);
            }
            else
                return View();


        }
        public ActionResult GalleryImageWrapperOtherProfile(string galleryImageId)
        {
            if (!string.IsNullOrEmpty(galleryImageId))
            {
                var obj = GalleryManager.Instance.GetGalleryImage(Guid.Parse(galleryImageId));
                return View(obj);
            }
            else
                return View();


        }

        public ActionResult DeleteGalleryImage(string galleryImageId)
        {
            if (!string.IsNullOrEmpty(galleryImageId))
            {
                bool deleted = GalleryManager.Instance.DeleteGalleryImage(Guid.Parse(galleryImageId));
                return Content(deleted ? "Deleted" : "Error in deleting entity");
            }
            else
                return Content("Error in deleting entity");
        }

        [HttpGet]
        public ActionResult EditGalleryImageInfo(string galleryImageId)
        {
            if (!string.IsNullOrEmpty(galleryImageId))
            {
                var gal = GalleryManager.Instance.GetGalleryImage(Guid.Parse(galleryImageId));
                if (gal != null)
                {
                    var obj = new Models.GalleryImageModel()
                    {
                        GalleryId = gal.GalleryId,
                        ImageId = gal.ImageId,
                        Description = gal.Description,
                        Title = gal.Title,

                    };
                    return View(obj);
                }
            }
            throw new HttpException("No Gallery Image Specified");
        }

        [HttpPost]
        public ActionResult EditGalleryImageInfo(Offtick.Web.OfftickWeb.Areas.Dr.Models.GalleryImageModel galleryImage)
        {
            if (galleryImage != null)
            {
                bool effected = GalleryManager.Instance.EditGalleryImageInfo(galleryImage.ImageId, galleryImage.Title, galleryImage.Description);
                return Json(effected ? "Saved" : "Error in editing entity");
            }
            else
                throw new HttpException("No Gallery Image Specified");
        }

        public ActionResult ImageViewer(string galleryImageId)
        {
            Guid galleryImageGuid;
            if (Guid.TryParse(galleryImageId, out galleryImageGuid))
            {
                var galleryImage=Offtick.Business.Services.Managers.GalleryManager.Instance.GetGalleryImage(galleryImageGuid);

                return View(galleryImage);
            }
            return View();
        }


        public ActionResult ShowGallery(string galleryId)
        {
            if (!string.IsNullOrEmpty(galleryId))
            {
                var obj = GalleryManager.Instance.GetGallery(Guid.Parse(galleryId));
                return View(obj);
            }
            else
                return View();
            
        }

        public ActionResult GetGalleryOffer(string userName, int take, int skip)
        {
            var defaultGallery = Offtick.Business.Services.Managers.GalleryManager.Instance.GetDefaultGallery(userName);
            var galleryOffers=Offtick.Business.Services.Managers.GalleryManager.Instance.GetGalleryOffers(defaultGallery, take, skip);
            return View(galleryOffers);
        }

        public ActionResult ShowOffer(string offerId)
        {
            Guid guidOfferId;
            if (Guid.TryParse(offerId, out guidOfferId))
            {
                var offer = Offtick.Business.Services.Managers.OfferManager.Instance.GetOffer(guidOfferId);

                return View(offer);
            }
            return View();
        }

        



        #region like & comment
        [HttpPost]
        public JsonResult LikeToggleForGalleryImage(string galleryImageId, string likerUserName)
        {
            if (!string.IsNullOrEmpty(galleryImageId) && !string.IsNullOrEmpty(likerUserName))
            {
                Guid galleryImageGuid;
                if (Guid.TryParse(galleryImageId, out galleryImageGuid))
                {
                    var userEntity = MembershipManager.Instance.GetUser(likerUserName);
                    var galleryImageEntity = GalleryManager.Instance.GetGalleryImage(galleryImageGuid);
                    long countOfLikes = GalleryManager.Instance.LikeOrDislike(userEntity, galleryImageEntity);
                    if (countOfLikes > -1)
                        return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, string.Empty, countOfLikes, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
                }
            }
            return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "No item found", string.Empty, string.Empty, string.Empty));
        }

        public JsonResult LikesForGalleryImage(string galleryImageId, int take, int skip)
        {
            if (!string.IsNullOrEmpty(galleryImageId))
            {
                Guid galleryImageGuid;
                if (Guid.TryParse(galleryImageId, out galleryImageGuid))
                {
                    var galleryImageEntity = GalleryManager.Instance.GetGalleryImage(galleryImageGuid);

                    if (galleryImageEntity != null && galleryImageEntity.GalleryImageVotes.Count > 0)
                    {
                        IList<object> lsResultToClient = new List<Object>();

                        var searchedLikes = galleryImageEntity.GalleryImageVotes.Skip(skip).Take(take);
                        if (searchedLikes.Count() > 0)
                        {
                            foreach (var likeEntity in searchedLikes)
                            {
                                ImageFileManager imgFileManager = new ImageFileManager(likeEntity.VoterUserId.ToString());
                                Offtick.Data.Context.ExpertOnlinerContexts.MembershipUser liker = likeEntity.MembershipUser;
                                byte[] img32 = imgFileManager.GetBytesOfImage(imgFileManager.GetFullFileName(liker.Picture,true));

                                var objectToClient = new
                                {
                                    firstName = liker.FirstName,
                                    lastName = liker.LastName,
                                    userName = liker.UserName,
                                    dateTime = likeEntity.DateTime.ToString("yyyy-MM-dd HH:mm"),
                                    imageThumbnail = img32 != null ? Convert.ToBase64String(img32) : string.Empty,

                                };
                                lsResultToClient.Add(objectToClient);
                            }
                        }
                        return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "Like success", lsResultToClient.ToArray(), ActionResponseFromServerToClient.OfferLikesServerToClientKey));
                    }

                }
            }
            return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "No item found", string.Empty, string.Empty, string.Empty));
        }


        [HttpPost]
        public JsonResult PostACommentForGalleryImage(string galleryImageId, string commenterUserName, string commentBody)
        {
            if (!string.IsNullOrEmpty(galleryImageId) && !string.IsNullOrEmpty(commenterUserName) && !string.IsNullOrEmpty(commentBody))
            {
                Guid galleryImageGuid;
                if (Guid.TryParse(galleryImageId, out galleryImageGuid))
                {
                    var galleryImageEntity = GalleryManager.Instance.GetGalleryImage(galleryImageGuid);
                    var userEntity = MembershipManager.Instance.GetUser(commenterUserName);

                    var addStatus = GalleryManager.Instance.AddCommentForGalleryImage(galleryImageEntity, userEntity, commentBody);
                    if (addStatus == AddStatus.Added)
                        return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "comment success", string.Empty, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
                    else
                        return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Empty, "comment fail", string.Empty, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));

                }
            }
            return null;
        }
        public JsonResult CommentsForGalleryImage(string galleryImageId, int take, int skip)
        {
            if (!string.IsNullOrEmpty(galleryImageId))
            {
                Guid galleryImageGuid;
                if (Guid.TryParse(galleryImageId, out galleryImageGuid))
                {
                    var galleryImageEntity = GalleryManager.Instance.GetGalleryImage(galleryImageGuid);

                    if (galleryImageEntity != null && galleryImageEntity.GalleryImageComments.Count > 0)
                    {
                        IList<object> lsResultToClient = new List<Object>();

                        var searchedComments = galleryImageEntity.GalleryImageComments.Skip(skip).Take(take);
                        if (searchedComments.Count() > 0)
                        {
                            foreach (var commentEntity in searchedComments)
                            {
                                ImageFileManager imgFileManager = new ImageFileManager(commentEntity.SubscriberUserId.ToString());
                                var liker = commentEntity.MembershipUser;
                                byte[] img32 = imgFileManager.GetBytesOfImage(imgFileManager.GetFullFileName(liker.Picture, true));

                                var objectToClient = new
                                {
                                    commentText = commentEntity.Text,
                                    commenter = commentEntity.MembershipUser.UserName,
                                    commentDateTime = commentEntity.DateTime.HasValue ? commentEntity.DateTime.Value.ToString("yyyy-MM-dd HH:mm") : "",
                                    imageThumbnail = img32 != null ? Convert.ToBase64String(img32) : string.Empty,

                                };
                                lsResultToClient.Add(objectToClient);
                            }
                        }
                        return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "comment success", lsResultToClient.ToArray(), ActionResponseFromServerToClient.OfferCommentsServerToClientKey));
                    }

                }
            }
            return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "No item found", string.Empty, string.Empty, string.Empty));
        }
        #endregion


    }

}
