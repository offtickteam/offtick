﻿using Offtick.Business.Membership;
using Offtick.Business.Services.Domain.VirtualBasket;
using Offtick.Business.Services.Managers;
using Offtick.Business.Services.RealtimeSearvices;
using Offtick.Business.Services.RealtimeSearvices.EventDrivenDispatcher.Notification;
using Offtick.Business.Services.Storage;
using Offtick.Business.Web;
using Offtick.Core.EnumTypes;
using Offtick.Data.Entities.Common;
using Offtick.Web.OfftickWeb.Resources;
using Offtick.Data.Context.ExpertOnlinerContexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using  Offtick.Web.OfftickWeb.Areas.User.Models;

namespace Offtick.Web.OfftickWeb.Areas.User.Controllers
{
    public class BasketController : BaseController
    {
        //
        // GET: /User/Basket/


        public BasketController(DataContextContainer obj)
            : base(obj)
        {
        }

        public ActionResult Index()
        {
            
            return View();
        }

        public ActionResult AddToBasket(string userName, string offerId, short count,bool isBuyingVisible)
        {
             Guid entityId;
             if (Guid.TryParse(offerId, out entityId))
             {
                 var offer = OfferManager.Instance.GetOffer(entityId);
                 
                 if(offer.OfferType==(short)EnumOfferType.Food)
                 {
                     var foodOffer = offer.OfferFoods.FirstOrDefault();
                     var countOfOffer = foodOffer.Quantity;
                     var countOfSoled = foodOffer.OfferFoodOrders.Sum(e => e.Quantity);
                     var remain = countOfOffer - countOfSoled;
                     if (count > remain)
                         return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed,UIMessages.RequestMoreThanRemained , string.Empty, string.Empty, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
                     if(foodOffer.OfferStatusId==(int) OfferExecutionStatus.Suspended)
                         return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, UIMessages.SuspendedOffer, string.Empty, string.Empty, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
                 }

                 if (offer.OfferType == (short)EnumOfferType.Tour)
                 {
                     var tourOffer = offer.OfferTours.FirstOrDefault();
                     var countOfOffer = tourOffer.Quantity;
                     /*to do :
                      * var countOfSoled = tourOffer.offer.Sum(e => e.Quantity);
                     var remain = countOfOffer - countOfSoled;
                     if (count > remain)
                         return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, UIMessages.RequestMoreThanRemained, string.Empty, string.Empty, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
                     if (tourOffer.OfferStatusId == (int)OfferExecutionStatus.Suspended)
                         return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, UIMessages.SuspendedOffer, string.Empty, string.Empty, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
                      */
                 }
                 

                 var user = MembershipManager.Instance.GetUser(userName);
                 if (OfferManager.Instance.isExpiredFoodOffer(offer))
                     return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, UIMessages.ExpiredOffer, string.Empty, string.Empty, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));

                 var basketEntity = new VirtualBasketEntity();
                 basketEntity.AddOrUpdateBasketOrder(offer.OfferFoods.FirstOrDefault(), count,isBuyingVisible);
                 PaymentGatewayService.PaymentGatewayEndpoint endPoint = new PaymentGatewayService.PaymentGatewayEndpoint();
                 basketEntity.OrderId = endPoint.CreateNewOrderId().ToString();

                 VirtualBasketManager.Instance.AddVirtualBasketEntity(basketEntity);

                 return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "Your order has been received", offerId, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));

             }
            return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "Can not process your order, check remaining orders", string.Empty, string.Empty, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));

        }

        [HttpPost]
        public ActionResult UpdateBasket(IList<BasketUpdateCountModel> orders)
        {
            
            if (orders!=null)
            {
                foreach (var order in orders)
                {
                    var offer = OfferManager.Instance.GetOffer(order.OfferId);

                    if (offer.OfferType == (short)EnumOfferType.Food)
                    {
                        var foodOffer = offer.OfferFoods.FirstOrDefault();
                        var countOfOffer = foodOffer.Quantity;
                        var countOfSoled = foodOffer.OfferFoodOrders.Sum(e => e.Quantity);
                        var remain = countOfOffer - countOfSoled;
                        if (order.OfferCount > remain)
                        {
                            return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Format("{0}:{1}",offer.Title, UIMessages.RequestMoreThanRemained), string.Empty, string.Empty, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
                        }
                    }

                    
                    if (OfferManager.Instance.isExpiredFoodOffer(offer))
                        return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "Can not process your order, this offer has epired", string.Empty, string.Empty, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));

                    var basketEntity = new VirtualBasketEntity();
                    basketEntity.AddOrUpdateBasketOrder(offer.OfferFoods.FirstOrDefault(), order.OfferCount, null);
                    PaymentGatewayService.PaymentGatewayEndpoint endPoint = new PaymentGatewayService.PaymentGatewayEndpoint();
                    basketEntity.OrderId = endPoint.CreateNewOrderId().ToString();

                    VirtualBasketManager.Instance.AddVirtualBasketEntity(basketEntity);
                }
                return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "Successfully update basket", "", ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
                
            }
            return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "Error, please try again", string.Empty, string.Empty, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));

        }

        public ActionResult RemoveFromBasket(string userName, string offerId)
        {
             Guid entityId;
             if (Guid.TryParse(offerId, out entityId))
             {
                 var offer = OfferManager.Instance.GetOffer(entityId);
                 var user = MembershipManager.Instance.GetUser(userName);
                 


                 VirtualBasketManager.Instance.RemoveVirtualBasketEntity(offerId);
                 PaymentGatewayService.PaymentGatewayEndpoint endPoint = new PaymentGatewayService.PaymentGatewayEndpoint();
                 VirtualBasketManager.Instance.ChangeOrderIdOfVirtualBasketEntity( endPoint.CreateNewOrderId().ToString());
                 return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "Your order has been received", offerId, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));

             }
            return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "Can not process your order, check remaining orders", string.Empty, string.Empty, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));

        }

        public ActionResult BasketSummery()
        {
            return View();
        }
        

    }
}
