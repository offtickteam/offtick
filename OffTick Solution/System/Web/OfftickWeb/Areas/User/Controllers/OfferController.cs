﻿using Offtick.Business.Membership;
using Offtick.Business.Services;
using Offtick.Business.Services.Managers;
using Offtick.Business.Services.RealtimeSearvices;
using Offtick.Business.Services.RealtimeSearvices.EventDrivenDispatcher.Notification;
using Offtick.Business.Services.Storage;
using Offtick.Business.Web;
using Offtick.Core.EnumTypes;
using Offtick.Core.Utility.PersianTools;
using Offtick.Data.Entities.Common;
using Offtick.Web.OfftickWeb.Areas.User.Models;
using Offtick.Web.OfftickWeb.Infrustracture;
using Offtick.Data.Context.ExpertOnlinerContexts;
using PaymentGatewayService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace Offtick.Web.OfftickWeb.Areas.User.Controllers
{
    public class OfferController : BaseController
    {
        public OfferController(DataContextContainer obj)
            : base(obj)
        {

        }


        //
        // GET: /User/Offers/

        public ActionResult Index()
        {
            return View();
        }

     

        #region Offtick_Offer
        [Authorize]
        public ActionResult OfftickOffer(string offtickOfferType)
        {
            ViewData["offtickOfferType"] = offtickOfferType;
            return View();
        }
        [Authorize,HttpPost]
        public ActionResult OfftickOffer(FoodModel model)
        {
            var currentUser = MembershipManager.Instance.GetCurrentUser();



            
            if (model!=null && ModelState.IsValid)
            {
                ChatService chatService = new ChatService();
                var response = chatService.SRpcSendOffer(new Business.Services.RealtimeSearvices.EventDrivenDispatcher.Notification.OfferNotification()
                {
                    Title = model.foodTitle,
                    Body = model.foodTitleLong,
                    Datetime = DateTime.Now.ToString(),
                    FromUserName = currentUser.UserName,
                    ToUserName = currentUser.UserName,
                    OfferType = (short)EnumOfferType.Food,
                });

                if (response.ResultStatus == "Success")
                {

                    string newFileName = string.Empty;
                    var responseEdit = OfferManager.Instance.UploadOfferFile(currentUser.UserName, response.DataObject.ToString(),model.mainImage, out newFileName);
                    if (responseEdit == Core.EnumTypes.EditStatus.Edited)
                    {
                        Guid offerId = Guid.Parse(response.DataObject.ToString());
                        var offer = OfferManager.Instance.GetOffer(offerId);
                        string foodSpecification = string.Empty;
                        string foodConditions = string.Empty;
                        string foodDescriptions = string.Empty;
                        Guid foodLocationCityId;
                        bool hasCityId=Guid.TryParse(model.foodLocationCityId,out foodLocationCityId);
                        if (model.foodSpecification != null && model.foodSpecification.InputStream != null)
                        {
                            System.IO.TextReader fileReader = new System.IO.StreamReader(model.foodSpecification.InputStream);
                            foodSpecification = fileReader.ReadToEnd();
                        }
                        if (model.foodConditions != null && model.foodConditions.InputStream != null)
                        {
                            System.IO.TextReader fileReader = new System.IO.StreamReader(model.foodConditions.InputStream);
                            foodConditions = fileReader.ReadToEnd();
                        }
                        if (model.foodDescriptions != null && model.foodDescriptions.InputStream != null)
                        {
                            System.IO.TextReader fileReader = new System.IO.StreamReader(model.foodDescriptions.InputStream);
                            foodDescriptions = fileReader.ReadToEnd();
                        }
                        var addStatus= OfferManager.Instance.AddFoodOffer(offer
                            , model.foodImages
                            , model.menuImages
                            , model.foodOriginalPrice
                            , model.foodOriginalTitle
                            , model.url
                            , model.foodUnit
                            , model.foodDiscountPrice
                            , model.foodDiscountTitle
                            , model.foodDiscountPercent
                            , model.foodExpireDate
                            , model.foodQuantity
                            , model.foodAudeiences
                            , foodSpecification
                            , foodConditions
                            , foodDescriptions
                            , hasCityId? (Guid?) foodLocationCityId:null
                            ,model.foodLocationAddress
                            ,model.offtickOfferTypeId
                            ,model.showIncomeInPercentage
                            ,model.foodHasAddedValueTax?(Nullable<short>) GeneralHelper.GetTaxValueForCurrentYear():null,
                            GeneralHelper.isConfirmedCompanyOfferOnInsert()?ConfirmStatus.Confirmed:ConfirmStatus.NotConfirmed
                            );

                       if(addStatus==AddStatus.Added)
                           return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, "", "", newFileName, ""));
                    }
                }
            }
            return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "Failed , try it again", "", "", "")); 
 
            
        }

        //[Authorize]
        public ActionResult OfftickOfferViewer(Offer offer)
        {
            if (offer!=null)
            {
                return View(offer);
            }
            return View();
        }

        public ActionResult MainPageBigOfferViewer()
        {
            return View(OfferManager.Instance.GetMaingPageBigOffer());
        }
        
        public ActionResult SaleFoodOffer(string userName, string offerId,string saleReferenceId, short count,long saleOrderId,bool isBuyingVisible=true)
        {
            Guid entityId;
            if (Guid.TryParse(offerId, out entityId))
            {
                var offer = OfferManager.Instance.GetOffer(entityId);
                var user=MembershipManager.Instance.GetUser(userName);
                if(OfferManager.Instance.isExpiredFoodOffer(offer))
                    return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "Can not process your order, this offer has epired", string.Empty, string.Empty, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));

                var actionStatus = OfferManager.Instance.OrderFoodOffer(offer, user, count, saleOrderId,saleReferenceId ,isBuyingVisible, Offtick.Web.OfftickWeb.Infrustracture.PdfServiceGenerator.CreateDefaultPdfService());
                if (actionStatus == AddStatus.Added)
                {
                    if (!string.IsNullOrEmpty(userName))
                    {
                        string messageBody = string.Format("user:{0} requests {1} units of {2}, mobile: {3}, email: {4}", user.FirstName + " " + user.LastName, count, offer.Title,user.PhoneNumber,user.Email);
                        var notification = new SendMessageNotification()
                        {
                            FromUserName = userName,
                            Message = messageBody,
                            ToUserName = offer.MembershipUser.UserName,
                            Flags = 1,
                            MessageType = "0"

                        };
                        ChatService chService = new ChatService();
                        chService.SRpcSendMessage(notification);


                        //start send sms one for saler and one for bayer
                        string baySaleDate = Offtick.Core.Utility.PersianTools.PersianDateConvertor.ToKhorshidiDate(DateTime.Now);

                        string salerName = offer.MembershipUser.FirstName;
                        var saleOfferNumber = saleOrderId;
                        var saleOfferPrice=offer.OfferFoods.FirstOrDefault().DiscountPrice * count;
                        string salerSms = string.Format(Resources.UIMessages.SmsSaleCompanyReportSuccess, salerName, saleOfferNumber, baySaleDate, saleOfferPrice, count);
                        string salerMobile = offer.MembershipUser.PhoneNumber;
                        
                        string bayerName = user.FirstName + " " + user.LastName;
                        var tracingNo = saleOrderId;
                        string bayerSms = string.Format(Resources.UIMessages.SmsSaleCustomerReportSuccess, bayerName, tracingNo, baySaleDate);
                        string bayerMobile = user.PhoneNumber;

                        Action actionSendSms = () =>
                        {
                            if(!string.IsNullOrEmpty( salerMobile))
                                Offtick.Business.Services.Domain.SmsService.Instance.SendMagfaSms(salerMobile,salerSms);
                            if (!string.IsNullOrEmpty(bayerMobile))
                                 Offtick.Business.Services.Domain.SmsService.Instance.SendMagfaSms( bayerMobile,bayerSms);
                        };
                        actionSendSms.BeginInvoke(null, null);
                        // end of send sms for saler and buyer
                        
                    }
                    return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "Your order has been received", offerId, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
                }
                else
                    return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "Can not process your order, check remaining orders", string.Empty, string.Empty, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
            
            }
            return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "Can not process your order, check remaining orders", string.Empty, string.Empty, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));

        }


        public ActionResult OrderedOffers()
        {
               return View();
        }

        [Authorize]
        public ActionResult GetOrderedOffersFor( int take, int skip)
        {
            var userId=MembershipManager.Instance.GetCurrentUser().UserId;
           var orderedOffers= Offtick.Business.Services.Managers.OfferManager.Instance.getAllOfOrderedOfferForUserName(userId, take, skip);
           return View(orderedOffers);
        }

        public ActionResult BuyedOffers(string userName)
        {
            return View((object)userName);
        }

        public JsonResult BuyedOffersForUser(int? page, int? limit, string sortBy, string direction, string userName)
        {

            int take = limit.HasValue ? limit.Value : 20;
            int skip = 0;
            if (page.HasValue)
                skip = (page.Value - 1) * limit.Value;

            var user = MembershipManager.Instance.GetUser(userName);
            var userId = user.UserId;
            var orderedOffers = Offtick.Business.Services.Managers.OfferManager.Instance.getAllOfBuyedOffersForUserName(userId);


            var buyOffers = orderedOffers.Select(o => new
            {
                Title = o.OfferFood.Offer.Title,
                LongTitle = o.OfferFood.Offer.Body,
                Offerid=o.OfferFood.Offer.OfferId,
                SallerUserId=o.OfferFood.Offer.MembershipUser.UserId,
                SallerUserName = o.OfferFood.Offer.MembershipUser.UserName,
                FileName = o.OfferFood.Offer.FileName,
                DateTime = o.DateTime,
                Quantity = o.Quantity,
                PaymentAmount = o.PaymentAmount,
                DiscountAmount=(o.OfferFood.OriginalPrice- o.OfferFood.DiscountPrice) * o.Quantity
            }).OrderByDescending(e => e.DateTime);

            int total = buyOffers.Count();
            if (!string.IsNullOrEmpty(sortBy) && !string.IsNullOrEmpty(direction))
            {
                if (direction.Trim() == "asc")
                {
                    switch (sortBy)
                    {
                        case "Title":
                            buyOffers = buyOffers.OrderBy(q => q.Title);
                            break;
                        case "LongTitle":
                            buyOffers = buyOffers.OrderBy(q => q.LongTitle);
                            break;
                        case "SallerUserName":
                            buyOffers = buyOffers.OrderBy(q => q.SallerUserName);
                            break;
                        case "DateTime":
                            buyOffers = buyOffers.OrderBy(q => q.DateTime);
                            break;
                        case "Quantity":
                            buyOffers = buyOffers.OrderBy(q => q.Quantity);
                            break;
                        case "PaymentAmount":
                            buyOffers = buyOffers.OrderBy(q => q.PaymentAmount);
                            break;
                        case "DiscountAmount":
                            buyOffers = buyOffers.OrderBy(q => q.DiscountAmount);
                            break;
                    }
                }
                else
                {
                    switch (sortBy)
                    {
                        case "Title":
                            buyOffers = buyOffers.OrderByDescending(q => q.Title);
                            break;
                        case "LongTitle":
                            buyOffers = buyOffers.OrderByDescending(q => q.LongTitle);
                            break;
                        case "SallerUserName":
                            buyOffers = buyOffers.OrderByDescending(q => q.SallerUserName);
                            break;
                        case "DateTime":
                            buyOffers = buyOffers.OrderByDescending(q => q.DateTime);
                            break;
                        case "Quantity":
                            buyOffers = buyOffers.OrderByDescending(q => q.Quantity);
                            break;
                        case "PaymentAmount":
                            buyOffers = buyOffers.OrderByDescending(q => q.PaymentAmount);
                            break;
                        case "DiscountAmount":
                            buyOffers = buyOffers.OrderByDescending(q => q.DiscountAmount);
                            break;
                    }
                }
            }

            var records = buyOffers.Skip(skip).Take(take).ToArray();
            IList<BuyedOfferByUserDTO> lsResult = new List<BuyedOfferByUserDTO>();
            foreach (var record in records)
            {
                lsResult.Add(new BuyedOfferByUserDTO()
                {
                    Title = record.Title,
                    LongTitle = record.LongTitle,
                    DateTime = Offtick.Core.Utility.PersianTools.PersianDateConvertor.ToKhorshidiDate(record.DateTime),
                    OfferId=record.Offerid,
                    SallerUserId=record.SallerUserId,
                    SallerUserName=record.SallerUserName,
                    FileName = GeneralService.GetThumbnailOf(record.SallerUserId, record.FileName),
                    Quantity = record.Quantity,
                    PaymentAmount = record.PaymentAmount.HasValue? record.PaymentAmount.Value:0,
                    DiscountAmount = record.DiscountAmount,
                  
                });
            }
            return Json(new { records = lsResult.ToArray(), total }, JsonRequestBehavior.AllowGet);
        }

      



        public ActionResult SoldOffers()
        {
            return View();
        }

        [Authorize]
        public ActionResult SoldOffersFor(int take, int skip)
        {
            var userId = MembershipManager.Instance.GetCurrentUser().UserId;
            var orderedOffers = Offtick.Business.Services.Managers.OfferManager.Instance.getAllOfSoldOffersForUserName(userId, take, skip);
            return View(orderedOffers);
        }


        public ActionResult SoldOfferForUser(string userName)
        {
            return View((object)userName);
        }

        public JsonResult SoldOfferForUserGroupByOffer(int? page, int? limit, string sortBy, string direction, string userName,string offerName)
        {

            int take = limit.HasValue ? limit.Value : 20;
            int skip = 0;
            if (page.HasValue)
                skip = (page.Value - 1) * limit.Value;

            var user=MembershipManager.Instance.GetUser(userName);
            var userId = user.UserId;
            var orderedOffers = Offtick.Business.Services.Managers.OfferManager.Instance.getAllOfSoldOffersForUserName(userId,offerName);


            var orderedOffersGroupByOffer = orderedOffers.GroupBy(e => new { e.OfferFoodId,e.OfferFood.OriginalPrice,e.OfferFood.DiscountPrice, e.OfferFood.DiscountPercent 
                ,e.OfferFood.Offer.Title,e.OfferFood.Offer.OfferId,e.OfferFood.Offer.FileName}).Select(o => new
            {
                Title = o.Key.Title,
                Body = o.Key.Title,
                OfferId = o.Key.OfferId,
                // FileName =GeneralService.GetThumbnailOf(o.Key.Offer.MembershipUser.UserId, o.Key.Offer.FileName),
                FileName = o.Key.FileName,
                OriginalPrice = o.Key.OriginalPrice,
                DiscountPrice = o.Key.DiscountPrice,
                DiscountPercent = o.Key.DiscountPercent,
                SoldCount = o.Sum(e => e.Quantity),
                TotalSoldValue = o.Sum(e => e.PaymentAmount),
                DateTime = o.Max(e => e.DateTime),
                UserCount = o.Select(e => e.MembershipUser.UserId).Distinct().Count()
            }).OrderByDescending(e => e.DateTime);

            int total = orderedOffersGroupByOffer.Select(e=>e.OfferId).Count();
            if (!string.IsNullOrEmpty(sortBy) && !string.IsNullOrEmpty(direction))
            {
                if (direction.Trim() == "asc")
                {
                    switch (sortBy)
                    {
                        case "Title":
                            orderedOffersGroupByOffer = orderedOffersGroupByOffer.OrderBy(q => q.Title);
                            break;
                        case "Body":
                            orderedOffersGroupByOffer = orderedOffersGroupByOffer.OrderBy(q => q.Body);
                            break;
                        case "FileName":
                            orderedOffersGroupByOffer = orderedOffersGroupByOffer.OrderBy(q => q.FileName);
                            break;
                        case "OriginalPrice":
                            orderedOffersGroupByOffer = orderedOffersGroupByOffer.OrderBy(q => q.OriginalPrice);
                            break;
                        case "DiscountPrice":
                            orderedOffersGroupByOffer = orderedOffersGroupByOffer.OrderBy(q => q.DiscountPrice);
                            break;
                        case "DiscountPercent":
                            orderedOffersGroupByOffer = orderedOffersGroupByOffer.OrderBy(q => q.DiscountPercent);
                            break;
                        case "SoldCount":
                            orderedOffersGroupByOffer = orderedOffersGroupByOffer.OrderBy(q => q.SoldCount);
                            break;
                        case "TotalSoldValue":
                            orderedOffersGroupByOffer = orderedOffersGroupByOffer.OrderBy(q => q.TotalSoldValue);
                            break;
                        case "DateTime":
                            orderedOffersGroupByOffer = orderedOffersGroupByOffer.OrderBy(q => q.DateTime);
                            break;
                        case "UserCount":
                            orderedOffersGroupByOffer = orderedOffersGroupByOffer.OrderBy(q => q.UserCount);
                            break;
                    }
                }
                else
                {
                    switch (sortBy)
                    {
                        case "Title":
                            orderedOffersGroupByOffer = orderedOffersGroupByOffer.OrderByDescending(q => q.Title);
                            break;
                        case "Body":
                            orderedOffersGroupByOffer = orderedOffersGroupByOffer.OrderByDescending(q => q.Body);
                            break;
                        case "FileName":
                            orderedOffersGroupByOffer = orderedOffersGroupByOffer.OrderByDescending(q => q.FileName);
                            break;
                        case "OriginalPrice":
                            orderedOffersGroupByOffer = orderedOffersGroupByOffer.OrderByDescending(q => q.OriginalPrice);
                            break;
                        case "DiscountPrice":
                            orderedOffersGroupByOffer = orderedOffersGroupByOffer.OrderByDescending(q => q.DiscountPrice);
                            break;
                        case "DiscountPercent":
                            orderedOffersGroupByOffer = orderedOffersGroupByOffer.OrderByDescending(q => q.DiscountPercent);
                            break;
                        case "SoldCount":
                            orderedOffersGroupByOffer = orderedOffersGroupByOffer.OrderByDescending(q => q.SoldCount);
                            break;
                        case "TotalSoldValue":
                            orderedOffersGroupByOffer = orderedOffersGroupByOffer.OrderByDescending(q => q.TotalSoldValue);
                            break;
                        case "DateTime":
                            orderedOffersGroupByOffer = orderedOffersGroupByOffer.OrderByDescending(q => q.DateTime);
                            break;
                        case "UserCount":
                            orderedOffersGroupByOffer = orderedOffersGroupByOffer.OrderByDescending(q => q.UserCount);
                            break;
                    }
                }
            }

            var records = orderedOffersGroupByOffer.Skip(skip).Take(take).ToArray();
            IList<SoldOfferGroupByDTO> lsResult=new List<SoldOfferGroupByDTO>();
            foreach (var record in records)
            {
                lsResult.Add(new SoldOfferGroupByDTO()
                {
                    Body = record.Body,
                    DateTime = Offtick.Core.Utility.PersianTools.PersianDateConvertor.ToKhorshidiDate(record.DateTime),
                    DiscountPercent = record.DiscountPercent,
                    DiscountPrice = record.DiscountPrice,
                    FileName = GeneralService.GetThumbnailOf(userId, record.FileName),
                    OfferId = record.OfferId,
                    OriginalPrice = record.OriginalPrice,
                    SoldCount = record.SoldCount,
                    Title = record.Title,
                    TotalSoldValue =record.TotalSoldValue.HasValue? record.TotalSoldValue.Value:0,
                    UserCount = record.UserCount,
                });
            }
            return Json(new { records=lsResult.ToArray(), total }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SoldOfferForUserOffer(Guid offerId)
        {
            return View((Object)offerId);
        }
        public JsonResult SoldOfferForUserByOfferId(int? page, int? limit, string sortBy, string direction, Guid offerId)
        {

            int take = limit.HasValue ? limit.Value : 20;
            int skip = 0;
            if (page.HasValue)
                skip = (page.Value - 1) * limit.Value;


            var user = Offtick.Business.Services.Managers.OfferManager.Instance.GetOffer(offerId).MembershipUser;
            var userId = user.UserId;
            var orderedOffers = Offtick.Business.Services.Managers.OfferManager.Instance.getAllOfSoldOffersForUserName(userId, offerId);


            var orderedOffersObjects = orderedOffers.Select(o => new
            {
                DateTime = o.DateTime,
                DiscountPrice = o.OfferFood.DiscountPrice,
                Quantity = o.Quantity,
                PaymentAmount = o.PaymentAmount,
                DisplayName = o.MembershipUser.FirstName+ " "+ o.MembershipUser.LastName,
                UserName = o.MembershipUser.UserName,
                UserId = o.MembershipUser.UserId,
                PhoneNumber = o.MembershipUser.PhoneNumber,
            }).OrderByDescending(e => e.DateTime);

            int total = orderedOffersObjects.Count();
            if (!string.IsNullOrEmpty(sortBy) && !string.IsNullOrEmpty(direction))
            {
                if (direction.Trim() == "asc")
                {
                    switch (sortBy)
                    {
                        case "DateTime":
                            orderedOffersObjects = orderedOffersObjects.OrderBy(q => q.DateTime);
                            break;
                        case "DiscountPrice":
                            orderedOffersObjects = orderedOffersObjects.OrderBy(q => q.DiscountPrice);
                            break;
                        case "Quantity":
                            orderedOffersObjects = orderedOffersObjects.OrderBy(q => q.Quantity);
                            break;
                        case "PaymentAmount":
                            orderedOffersObjects = orderedOffersObjects.OrderBy(q => q.PaymentAmount);
                            break;
                        case "DisplayName":
                            orderedOffersObjects = orderedOffersObjects.OrderBy(q => q.DisplayName);
                            break;
                        case "UserName":
                            orderedOffersObjects = orderedOffersObjects.OrderBy(q => q.UserName);
                            break;
                        case "PhoneNumber":
                            orderedOffersObjects = orderedOffersObjects.OrderBy(q => q.PhoneNumber);
                            break;
                    }
                }
                else
                {
                    switch (sortBy)
                    {
                        case "DateTime":
                            orderedOffersObjects = orderedOffersObjects.OrderByDescending(q => q.DateTime);
                            break;
                        case "DiscountPrice":
                            orderedOffersObjects = orderedOffersObjects.OrderByDescending(q => q.DiscountPrice);
                            break;
                        case "Quantity":
                            orderedOffersObjects = orderedOffersObjects.OrderByDescending(q => q.Quantity);
                            break;
                        case "PaymentAmount":
                            orderedOffersObjects = orderedOffersObjects.OrderByDescending(q => q.PaymentAmount);
                            break;
                        case "DisplayName":
                            orderedOffersObjects = orderedOffersObjects.OrderByDescending(q => q.DisplayName);
                            break;
                        case "UserName":
                            orderedOffersObjects = orderedOffersObjects.OrderByDescending(q => q.UserName);
                            break;
                        case "PhoneNumber":
                            orderedOffersObjects = orderedOffersObjects.OrderByDescending(q => q.PhoneNumber);
                            break;
                    }
                }
            }

            var records = orderedOffersObjects.Skip(skip).Take(take).ToArray();
            IList<SoldOfferByOfferDTO> lsResult = new List<SoldOfferByOfferDTO>();
            foreach (var record in records)
            {
                lsResult.Add(new SoldOfferByOfferDTO()
                {
                    DateTime = Offtick.Core.Utility.PersianTools.PersianDateConvertor.ToKhorshidiDate(record.DateTime),
                    DisplayName = record.DisplayName,
                    PaymentAmount = record.PaymentAmount.HasValue ? record.PaymentAmount.Value : 0,
                    PhoneNumber = record.PhoneNumber,
                    Quantity = record.Quantity,
                    DiscountPrice = record.DiscountPrice,
                    UserId = record.UserId,
                    UserName = record.UserName,
                });
            }
            return Json(new { records = lsResult.ToArray(), total }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult OfferVisitedPresenter(Guid offerId)
        {
            var offer = Offtick.Business.Services.Managers.OfferManager.Instance.GetOffer(offerId);
            var controller = DependencyResolver.Current.GetService<ProfileController>();
            if (offer != null)
            {

                bool showViewers = offer.OfferPrivacy != null ? offer.OfferPrivacy.IsViewerVisible : true;
                if (showViewers)
                {
                    var visitors = (from v in offer.OfferVisiteds
                                   where (v.MembershipUser.MembershipPrivacy != null && v.MembershipUser.MembershipPrivacy.IsViewingVisible) || v.MembershipUser.MembershipPrivacy == null
                                   select v.MembershipUser).ToList();
                    controller.ViewBag.isLinear = true;
                    return controller.ShowUsers(visitors);
                }
            }
            
            return controller.ShowUsers(null);
        }
        public ActionResult OfferSoldPresenter(Guid offerId)
        {
            var offer = Offtick.Business.Services.Managers.OfferManager.Instance.GetOffer(offerId);
            var controller = DependencyResolver.Current.GetService<ProfileController>();
            if (offer != null)
            {

                bool showViewers = offer.OfferPrivacy != null ? offer.OfferPrivacy.IsBuyerVisible : true;
                if (showViewers)
                {
                    var visitors = (from o in offer.OfferFoods.FirstOrDefault().OfferFoodOrders
                                    where (o.MembershipUser.MembershipPrivacy != null && o.MembershipUser.MembershipPrivacy.IsBuyingVisible) || o.MembershipUser.MembershipPrivacy == null
                                    select o.MembershipUser).ToList();
                    controller.ViewBag.isLinear = true;
                    return controller.ShowUsers(visitors);
                }
            }

            return controller.ShowUsers(null);
        }
        public ActionResult OfferCommentorPresenter(Guid offerId)
        {
            var offer = Offtick.Business.Services.Managers.OfferManager.Instance.GetOffer(offerId);
            var controller = DependencyResolver.Current.GetService<ProfileController>();
            if (offer != null)
            {

                bool showViewers = offer.OfferPrivacy != null ? offer.OfferPrivacy.IsBuyerVisible : true;
                if (showViewers)
                {
                    var users = (from c in offer.OfferComments
                                    where (c.MembershipUser.MembershipPrivacy != null && c.MembershipUser.MembershipPrivacy.IsBuyingVisible) || c.MembershipUser.MembershipPrivacy == null
                                    select c.MembershipUser).Distinct().ToList();
                    controller.ViewBag.isLinear = true;
                    return controller.ShowUsers(users);
                }
            }

            return controller.ShowUsers(null);
        }
        public ActionResult OfferLikersPresenter(Guid offerId)
        {
            var offer = Offtick.Business.Services.Managers.OfferManager.Instance.GetOffer(offerId);
            var controller = DependencyResolver.Current.GetService<ProfileController>();
            if (offer != null)
            {

                bool showViewers = offer.OfferPrivacy != null ? offer.OfferPrivacy.IsBuyerVisible : true;
                if (showViewers)
                {
                    var users = (from l in offer.OfferLikes
                                 where (l.MembershipUser.MembershipPrivacy != null && l.MembershipUser.MembershipPrivacy.IsBuyingVisible) || l.MembershipUser.MembershipPrivacy == null
                                 select l.MembershipUser).ToList();
                    controller.ViewBag.isLinear = true;
                    return controller.ShowUsers(users);
                }
            }

            return controller.ShowUsers(null);
        }


        public ActionResult UpgradeOffer(Guid offerId)
        {
            var offer = Offtick.Business.Services.Managers.OfferManager.Instance.GetOffer(offerId);
            if (offer != null)
            {
                switch ((EnumOfferType)offer.OfferType)
                {
                    case EnumOfferType.Post:
                        return View("UpgradeImageOffer", offer);
                    case EnumOfferType.Food:
                        return View("UpgradeOfftickOffer", offer);
                    case EnumOfferType.Tour:
                        return View("UpgradeTourOffer", offer);
                }
            }
            return View(offer);
        }

        [HttpPost]
        public ActionResult UpgradeOffer(UpgradeOfftickModel model)
        {
            var currentUser = MembershipManager.Instance.GetCurrentUser();




            if (model != null && ModelState.IsValid)
            {
                var specification=string.Empty;
                var condition=string.Empty;
                var description=string.Empty;

                 if (model.offerSpecification != null && model.offerSpecification.InputStream != null)
                        {
                            System.IO.TextReader fileReader = new System.IO.StreamReader(model.offerSpecification.InputStream);
                            specification = fileReader.ReadToEnd();
                        }
                  if (model.offerConditions != null && model.offerConditions.InputStream != null)
                        {
                            System.IO.TextReader fileReader = new System.IO.StreamReader(model.offerConditions.InputStream);
                            condition = fileReader.ReadToEnd();
                        }
                 if (model.offerDescriptions != null && model.offerDescriptions.InputStream != null)
                        {
                            System.IO.TextReader fileReader = new System.IO.StreamReader(model.offerDescriptions.InputStream);
                            description = fileReader.ReadToEnd();
                        }

                 var editStatus = OfferManager.Instance.UpdateOffer(model.offerId, model.offerTitle, model.offerTitleLong,model.url, model.offerOriginalPrice, model.offerOriginalTitle
                     , model.offerUnit, model.offerDiscountPrice, model.offerDiscountTitle, model.offerDiscountPercent, model.offerExpireDate, model.offerQuantity, model.offerAudeiences
                     , model.offerLocationCityId, model.offerLocationAddress, specification, condition, description, model.showIncomeInPercentage, model.offerHasAddedValueTax);

                 if (editStatus == EditStatus.Edited)
                            return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, "", "", "", ""));
                   
                
            }
            return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "Failed , try it again", "", "", "")); 
 
        }



         public ActionResult UpgradeOfferMainImage(){
             var currentUser = MembershipManager.Instance.GetCurrentUser();

             var length = Request.ContentLength;
             string offerId =Request.Form["OfferId"];
             

             if (Request.Files != null && Request.Files.Count != 0)
             {
                 ChatService chatService = new ChatService();
                 //var response = chatService.SRpcSendOffer(new Business.Services.RealtimeSearvices.EventDrivenDispatcher.Notification.OfferNotification()
                 //{
                 //    Title = title,
                 //    Body = description,
                 //    Datetime = DateTime.Now.ToString(),
                 //    FromUserName = currentUser.UserName,
                 //    ToUserName = currentUser.UserName,
                 //    OfferType = (short)EnumOfferType.Post,
                 //});

                 //if (response.ResultStatus == "Success")
                 //{
                 //    string newFileName = string.Empty;
                 //    var responseEdit = OfferManager.Instance.UploadOfferFile(currentUser.UserName, response.DataObject.ToString(), Request.Files, out newFileName);
                 //    if (responseEdit == Core.EnumTypes.EditStatus.Edited)
                 //        return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, "", "", newFileName, ""));
                 //}

                 string newFileName = string.Empty;
                 var responseEdit = OfferManager.Instance.UploadOfferFile(currentUser.UserName, offerId, Request.Files, out newFileName);
                 if (responseEdit == Core.EnumTypes.EditStatus.Edited)
                     return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, "", "", newFileName, ""));
             }
             return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, "", "", "", "")); 
        
         }
        


     
        #endregion


         #region Tour_Offer
         [ Authorize]
         public ActionResult TourOffer()
         {
             return View();
         }

         [Authorize, HttpPost]
         public ActionResult TourOffer(TourModel model)
         {
             var currentUser = MembershipManager.Instance.GetCurrentUser();




             if (model != null && ModelState.IsValid)
             {
                 ChatService chatService = new ChatService();
                 var response = chatService.SRpcSendOffer(new Business.Services.RealtimeSearvices.EventDrivenDispatcher.Notification.OfferNotification()
                 {
                     Title = model.tourTitle,
                     Body = model.tourTitleLong,
                     Datetime = DateTime.Now.ToString(),
                     FromUserName = currentUser.UserName,
                     ToUserName = currentUser.UserName,
                     OfferType = (short)EnumOfferType.Tour,
                 });

                 if (response.ResultStatus == "Success")
                 {

                     string newFileName = string.Empty;
                     var responseEdit = OfferManager.Instance.UploadOfferFile(currentUser.UserName, response.DataObject.ToString(), model.mainImage, out newFileName);
                     if (responseEdit == Core.EnumTypes.EditStatus.Edited)
                     {
                         Guid offerId = Guid.Parse(response.DataObject.ToString());
                         var offer = OfferManager.Instance.GetOffer(offerId);
                         string tourSpecification = string.Empty;
                         string tourConditions = string.Empty;
                         string tourDescriptions = string.Empty;
                         Guid tourSourceCityId;
                         Guid tourDestinationCityId;
                         bool hasSourceCityId = Guid.TryParse(model.tourSourceCityId, out tourSourceCityId);
                         bool hasrDestinationCityId = Guid.TryParse(model.tourDestinationCityId, out tourDestinationCityId);
                         
                         DateTime tourWentDate;
                         DateTime tourReturnDate;
                         tourWentDate= PersianDateConvertor.ToGregorian(model.tourWentDate);
                         bool hasWentDate = tourWentDate != DateTime.MinValue;
                         tourReturnDate= PersianDateConvertor.ToGregorian(model.tourReturnDate) ;
                         bool hasReturnDate = tourReturnDate != DateTime.MinValue;

                         if (model.tourSpecification != null && model.tourSpecification.InputStream != null)
                         {
                             System.IO.TextReader fileReader = new System.IO.StreamReader(model.tourSpecification.InputStream);
                             tourSpecification = fileReader.ReadToEnd();
                         }
                         if (model.tourConditions != null && model.tourConditions.InputStream != null)
                         {
                             System.IO.TextReader fileReader = new System.IO.StreamReader(model.tourConditions.InputStream);
                             tourConditions = fileReader.ReadToEnd();
                         }
                         if (model.tourDescriptions != null && model.tourDescriptions.InputStream != null)
                         {
                             System.IO.TextReader fileReader = new System.IO.StreamReader(model.tourDescriptions.InputStream);
                             tourDescriptions = fileReader.ReadToEnd();
                         }
                         var addStatus = OfferManager.Instance.AddtourOffer(offer
                             , model.tourImages
                             , model.menuImages
                             , model.tourOriginalPrice
                             , model.tourOriginalTitle
                             , model.url
                             , model.tourUnit
                             , model.tourDiscountPrice
                             , model.tourDiscountTitle
                             , model.tourDiscountPercent
                             , model.tourExpireDate
                             , model.tourQuantity
                             , tourSpecification
                             , tourConditions
                             , tourDescriptions
                             , model.tourLocationAddress
                             , model.showIncomeInPercentage
                             , model.tourHasAddedValueTax ? (Nullable<short>)GeneralHelper.GetTaxValueForCurrentYear() : null
                             ,model.tourShowBuyButton
                             ,model.tourShowOffButton
                             , model.tourShowInstantButton
                             , hasSourceCityId? (Guid?) tourSourceCityId:null
                             , hasrDestinationCityId? (Guid?) tourDestinationCityId:null
                             , hasWentDate? (DateTime?)tourWentDate:null
                             , hasReturnDate ? (DateTime?)tourReturnDate : null
                             ,model.tourAgencyName
                             ,model.tourTravelType
                             ,GeneralHelper.isConfirmedCompanyOfferOnInsert() ? ConfirmStatus.Confirmed : ConfirmStatus.NotConfirmed
                             );

                         if (addStatus == AddStatus.Added)
                             return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, "", "", newFileName, ""));
                     }
                 }
             }
             return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "Failed , try it again", "", "", ""));


         }


         #endregion


         #region ImageOffer && MovieOffer
         public ActionResult PostPhoto()
        {
            return View();
        }

        [HttpPost,Authorize]
        public ActionResult PostPhotoLoader()
        {
            var currentUser=MembershipManager.Instance.GetCurrentUser();
            
            var length = Request.ContentLength;
            string title = Request.Form["Title"];
            string description = Request.Form["Description"];
            string galleryId = Request.Form["GalleryId"];

            if (Request.Files != null && Request.Files.Count != 0 )
            {
                ChatService chatService = new ChatService();
                var response = chatService.SRpcSendOffer(new Business.Services.RealtimeSearvices.EventDrivenDispatcher.Notification.OfferNotification()
                {
                    Title = title,
                    Body = description,
                    Datetime = DateTime.Now.ToString(),
                    FromUserName = currentUser.UserName,
                    ToUserName = currentUser.UserName,
                    OfferType = (short)EnumOfferType.Post,
                });

                if (response.ResultStatus == "Success")
                {
                    string newFileName=string.Empty;
                    var responseEdit=OfferManager.Instance.UploadOfferFile(currentUser.UserName, response.DataObject.ToString(), Request.Files, out newFileName);
                    if (responseEdit == Core.EnumTypes.EditStatus.Edited)
                        return Json( new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, "", "", newFileName, ""));
                }
            }
            return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "خطا در دریافت فایل", "", "", "")); 
        }
        [HttpPost, Authorize]
        public ActionResult UpgradeImageOffer_JSON(Guid offerId, string userName, string title, string description)
        {
            var currentUser = MembershipManager.Instance.GetCurrentUser();
            if (string.IsNullOrEmpty(userName) || offerId == default(Guid))
                return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "Invalid Parameter, Try later", "", "", ""));

            var responseEdit = OfferManager.Instance.UpdateOffer(offerId, title, description);
            if (responseEdit == Core.EnumTypes.EditStatus.Edited)
                return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, "", "", "", ""));


            return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, "", "", "", ""));
        }


        public ActionResult ImageOfferViewer(Offer offer)
        {
            if (offer!=null)
            {
                return View(offer);
            }
            return View();
        }


        [HttpGet]
        public ActionResult PostVideo()
        {
            return View();
        }

        [HttpPost, Authorize]
        public ActionResult PostVideo_JSON()
        {
            var currentUser = MembershipManager.Instance.GetCurrentUser();

            var length = Request.ContentLength;
            string title = Request.Form["Title"];
            string description = Request.Form["Description"];
            string galleryId = Request.Form["GalleryId"];

            if (Request.Files != null && Request.Files.Count != 0)
            {
                ChatService chatService = new ChatService();
                var response = chatService.SRpcSendOffer(new Business.Services.RealtimeSearvices.EventDrivenDispatcher.Notification.OfferNotification()
                {
                    Title = title,
                    Body = description,
                    Datetime = DateTime.Now.ToString(),
                    FromUserName = currentUser.UserName,
                    ToUserName = currentUser.UserName,
                    OfferType = (short)EnumOfferType.Video,
                });

                if (response.ResultStatus == "Success")
                {
                    string newFileName = string.Empty;
                    var responseEdit = OfferManager.Instance.UploadOfferFile(currentUser.UserName, response.DataObject.ToString(), Request.Files, out newFileName);
                    if (responseEdit == Core.EnumTypes.EditStatus.Edited)
                        return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, "", "", newFileName, ""));
                }
            }
            return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, "", "", "", ""));
        }


        public ActionResult VideofferViewer(Offer offer)
        {
            if (offer != null)
            {
                return View(offer);
            }
            return View();
        }

        #endregion

        [HttpPost, Authorize]
        public ActionResult DeleteOffer_JSON(string userName,string offerId)
        {
            if (!string.IsNullOrEmpty(offerId))
            {
                Guid entityId;
                if (Guid.TryParse(offerId, out entityId))
                {
                    var offerEntity = OfferManager.Instance.GetOffer(entityId);
                    var currentUser = MembershipManager.Instance.GetCurrentUser();
                    string roleName = currentUser != null ? currentUser.MembershipUsersInRoles.First().MembershipRole.RoleName : string.Empty;
                    if (offerEntity != null)
                    {
                        if (userName.Equals(currentUser.UserName) && (offerEntity.MembershipUser.UserName.Equals(currentUser.UserName) || roleName == "Administrator"))
                        {
                            var actionStatus = OfferManager.Instance.DeleteOffer(userName, entityId);
                            if (actionStatus == DeleteStatus.Deleted)
                            {
                                return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, "", "", "", ""));
                            }
                        }
                        else
                        {
                            //security issue, must to do

                        }
                    }
                    else
                    {
                        return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, "", "", "", ""));

                    }
                }
            }

            return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "Can not delete this offer", "", "", ""));
        }

        [HttpPost, Authorize]
        public ActionResult UpdateOffer(string offerId, string userName)
        {
            if (!string.IsNullOrEmpty(offerId))
            {
                Guid entityId;
                if (Guid.TryParse(offerId, out entityId))
                {
                    var offerComment = OfferManager.Instance.GetOffer(entityId);
                    var currentUser = MembershipManager.Instance.GetCurrentUser();
                    string roleName = currentUser != null ? currentUser.MembershipUsersInRoles.First().MembershipRole.RoleName : string.Empty;

                    if (userName.Equals(currentUser.UserName) && offerComment.MembershipUser.UserName.Equals(currentUser.UserName) || roleName == "Administrator")
                    {
                        return View(offerComment);
                    }
                    else
                    {
                        //security issue, must to do

                    }
                }
            }
            return View();
        }
        [HttpPost, Authorize]
        public ActionResult ReportOffer(string offerId)
        {
            if (!string.IsNullOrEmpty(offerId))
            {
                Guid entityId;
                if (Guid.TryParse(offerId, out entityId))
                {
                    var offer = OfferManager.Instance.GetOffer(entityId);
                    if (offer!=null)
                        return View(offer);
                }
            }
            return View();
        }

        [HttpPost, Authorize]
        public ActionResult UpdateOffer_JSON(string offerId, string userName, string title, string description,short quantity, bool isBuyerVisible, bool isViewerVisible, bool IsOnlyVisibleForFollowers)
        {
            if (!string.IsNullOrEmpty(offerId) && !string.IsNullOrEmpty(userName))
            {
                Guid entityId;
                if (Guid.TryParse(offerId, out entityId))
                {
                    var offerEntity = OfferManager.Instance.GetOffer(entityId);
                    var currentUser = MembershipManager.Instance.GetCurrentUser();

                    string roleName = currentUser != null ? currentUser.MembershipUsersInRoles.First().MembershipRole.RoleName : string.Empty;
                    if (offerEntity.MembershipUser.UserName.Equals(userName) || roleName == "Administrator")
                    {
                        var actionStatus = OfferManager.Instance.UpdateOffer(entityId, title, description, quantity,isBuyerVisible, isViewerVisible, IsOnlyVisibleForFollowers);

                        if (actionStatus == EditStatus.Edited || actionStatus== EditStatus.RejectedByNonEffectiveQuery)
                            return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "edit comment was successful", offerId, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
                        else
                            return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Empty, "fail to edit comment", string.Empty, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
                    }
                    else
                    {
                        //security issue, must to do
                        return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Empty, "fail to edit comment", string.Empty, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
                    }
                }
            }
            return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Empty, "Invalid Parameters", string.Empty, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
        }

        [HttpPost, Authorize]
        public ActionResult ReportOffer_JSON(string offerId, string userName, string reportBody)
        {
            if (!string.IsNullOrEmpty(offerId) && !string.IsNullOrEmpty(userName) && !string.IsNullOrEmpty(reportBody))
            {
                Guid entityId;
                if (Guid.TryParse(offerId, out entityId))
                {
                    var offerEntity = OfferManager.Instance.GetOffer(entityId);
                    var userEntity = MembershipManager.Instance.GetUser(userName);

                    var actionStatus = OfferManager.Instance.ReportOffer(offerEntity, userEntity, reportBody);

                    if (actionStatus == AddStatus.Added)
                        return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "Report offer was successful", offerId, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
                    else
                        return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Empty, "fail to report offer", string.Empty, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));

                }
            }
            return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Empty, "Invalid Parameters", string.Empty, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
        }


         [HttpPost, Authorize]
        public ActionResult AddCommentForOffer(string  offerId, string userName, string comment)
        {
            if (!string.IsNullOrEmpty(offerId) && !string.IsNullOrEmpty(userName) && !string.IsNullOrEmpty(comment))
            {
                Guid entityGuid;
                if (Guid.TryParse(offerId, out entityGuid))
                {
                    var offerEntity = OfferManager.Instance.GetOffer(entityGuid); ;
                    var userEntity = MembershipManager.Instance.GetUser(userName);
                    OfferComment reference=null;
                    var addStatus = OfferManager.Instance.AddCommentForOffer(offerEntity, userEntity, comment,out reference);
                    if (addStatus == AddStatus.Added)
                    {
                        int likeCount = offerEntity.OfferLikes.Count;
                        int commentCount = offerEntity.OfferComments.Count;
                        return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, string.Empty, new { likeCount = likeCount, commentCount = commentCount, OfferCommentId=reference.OfferCommentId }, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
                       // return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "comment success", reference.OfferCommentId, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
                    }
                    else
                        return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Empty, "comment fail", string.Empty, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));

                }
            }
            return null;
        }

         [HttpPost, Authorize]
         public ActionResult RemoveCommentForOffer(string offerCommentId, string userName)
         {
             if (!string.IsNullOrEmpty(offerCommentId) && !string.IsNullOrEmpty(userName))
             {
                 long entityId;
                 if (long.TryParse(offerCommentId, out entityId))
                 {
                     var offerEntity = OfferManager.Instance.GetOfferCommentById(entityId).Offer;
                     var actionStatus = OfferManager.Instance.RemoveCommentForOffer(entityId, userName);



                     if (actionStatus == DeleteStatus.Deleted)
                     {
                   

                         int likeCount = offerEntity.OfferLikes.Count;
                         int commentCount = offerEntity.OfferComments.Count;
                         return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "delete comment was successful",  new { likeCount = likeCount, commentCount = commentCount,offerId=offerEntity.OfferId }, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
                   
                     }
                     else
                         return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Empty, "fail to delete comment", string.Empty, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));

                 }
             }
             return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Empty, "Invalid Parameters", string.Empty, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
         }


         [HttpPost,Authorize]
         public ActionResult UpdateCommentForOffer(string offerCommentId,string userName)
         {
             if (!string.IsNullOrEmpty(offerCommentId))
             {
                 long entityId;
                 if (long.TryParse(offerCommentId, out entityId))
                 {
                     var user = MembershipManager.Instance.GetUser(userName);

                     string roleName = user != null ? user.MembershipUsersInRoles.First().MembershipRole.RoleName : string.Empty;

                     var offerComment = OfferManager.Instance.GetOfferCommentById(entityId);
                     var currentUser = MembershipManager.Instance.GetCurrentUser();
                     if (userName.Equals(currentUser.UserName) && offerComment.MembershipUser.UserName.Equals(currentUser.UserName) || roleName == "Administrator")
                     {
                         return View(offerComment);
                     }
                     else
                     {
                         //security issue, must to do
                         
                     }
                 }
             }
             return View();
         }

         [HttpPost, Authorize]
         public ActionResult UpdateCommentForOffer_JSON(string offerCommentId, string userName,string comment)
         {
             if (!string.IsNullOrEmpty(offerCommentId) && !string.IsNullOrEmpty(userName) && !string.IsNullOrEmpty(comment))
             {
                 long entityId;
                 if (long.TryParse(offerCommentId, out entityId))
                 {
                     var offerComment=OfferManager.Instance.GetOfferCommentById(entityId);
                     var user = MembershipManager.Instance.GetUser(userName);
                     string roleName = user != null ? user.MembershipUsersInRoles.First().MembershipRole.RoleName : string.Empty;
                     if (offerComment.MembershipUser.UserName.Equals(userName) || roleName == "Administrator")
                     {
                         var actionStatus = OfferManager.Instance.UpdateCommentForOffer(entityId, comment);
                         if (actionStatus == EditStatus.Edited)
                             return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "edit comment was successful", offerCommentId, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
                         else
                             return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Empty, "fail to edit comment", string.Empty, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
                     }
                     else
                     {
                         //security issue, must to do
                         return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Empty, "fail to edit comment", string.Empty, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
                     }
                 }
             }
             return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Empty, "Invalid Parameters", string.Empty, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
         }

       
         public ActionResult CommentsForOffer(Offer offer)
         {
             return View(offer);
         }

         public ActionResult SliceCommentsForOffer(string offerId, int take, int skip)
         {
             Guid guidOfferId=Guid.Empty;
             if(Guid.TryParse(offerId,out guidOfferId)){
                 var comments = OfferManager.Instance.GetOffer(guidOfferId).OfferComments.OrderByDescending(e=>e.Datetime).Skip(skip).Take(take).ToList();
                 return View(comments);
             }

             return View();
         }

         public ActionResult SliceCommentForOffer(long offerCommentId)
         {

             var comment = OfferManager.Instance.GetOfferCommentById(offerCommentId);
                 return View(comment);
         
         }




         [HttpPost, Authorize]
         public JsonResult LikeToggleForOffer(string offerId, string likerUserName)
         {
             if (!string.IsNullOrEmpty(offerId) && !string.IsNullOrEmpty(likerUserName))
             {
                 Guid entityGuid;
                 if (Guid.TryParse(offerId, out entityGuid))
                 {
                     var userEntity = MembershipManager.Instance.GetUser(likerUserName);
                     var offerEntity = OfferManager.Instance.GetOffer(entityGuid);
                     long countOfLikes = OfferManager.Instance.LikeOrDislike(userEntity, offerEntity);
                     if (countOfLikes > -1)
                     {
                         int likeCount = offerEntity.OfferLikes.Count;
                         int commentCount = offerEntity.OfferComments.Count;
                         return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, string.Empty, new { likeCount=likeCount,commentCount=commentCount }, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
                     }
                 }
             }
             return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "No item found", string.Empty, string.Empty, string.Empty));
         }

         public ActionResult LikesForOfferSlice(string offerId, int take, int skip)
         {
             Guid guidOfferId = Guid.Empty;
             if (Guid.TryParse(offerId, out guidOfferId))
             {
                 var likes = OfferManager.Instance.GetOffer(guidOfferId).OfferLikes.OrderByDescending(e => e.Datetime).Skip(skip).Take(take).ToList();
                 return View(likes);
             }

             return View();
         }


        public ActionResult OfferViewer(string offerId)
         {
            
             Guid entityId;
             if (Guid.TryParse(offerId, out entityId))
             {
                 
                 var offer = OfferManager.Instance.GetOffer(entityId);
                 if (offer != null)
                 {
                     if (offer.ConfirmStatusId == (int)ConfirmStatus.NotConfirmed)
                         return View("SuspendedOffer");


                     OfferManager.Instance.IncreaseOfferViewCount(offer, MembershipManager.Instance.GetCurrentUser());

                     if ((EnumOfferType)offer.OfferType == EnumOfferType.Tour)
                     {
                         return View("TourOfferViewer", offer);
                     }
                     if ((EnumOfferType)offer.OfferType == EnumOfferType.Food)
                     {
                         return View("OfftickOfferViewer", offer);
                     }
                     if ((EnumOfferType)offer.OfferType == EnumOfferType.Post)
                     {
                         return View("ImageOfferViewer", offer);
                     }
                     if ((EnumOfferType)offer.OfferType == EnumOfferType.Video)
                     {
                         return View("VideoOfferViewer", offer);
                     }
                 }
             }

             return View("NotExistOffer");
        }
        
        [HttpGet]
        public ActionResult SendFeedback(string layout)
        {
            if (!string.IsNullOrEmpty(layout))
            {
                ViewBag.customLayout = "~/Views/shared/_MSVILayout.cshtml";
            }
            
            return View();
        }


        [HttpPost]
        public ActionResult SendFeedback()
        {
            try
            {
                var currentUser = MembershipManager.Instance.GetCurrentUser();

                var length = Request.ContentLength;
                string title = Request.Form["Title"];
                string body = Request.Form["Feedback"];
                string name = currentUser != null ? string.Format("{0} {1}", currentUser.FirstName, currentUser.LastName) : Request.Form["Name"];
                string emailAddress = currentUser != null ? currentUser.Email : Request.Form["EmailAddress"];

                body = string.Format("<div>name:{0} , Email:{1}</div> <div> {2}</div>", name, emailAddress,body);
                SMTPService mailService = new SMTPService();
                System.IO.Stream stream=null;
                string fileName = string.Empty;
                if (Request.Files != null && Request.Files.Count != 0)
                {
                    stream = Request.Files[0].InputStream;
                    fileName=Request.Files[0].FileName;
                }

                mailService.SendMail("info@offTick.com", "info@offTick.com", title, body, stream, fileName);
                // mailService.SendMail("info@fypad.com", "amin_asr603@yahoo.com", title, body, stream, fileName);
                return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, "", "", "", ""));
            }
            catch
            {
                return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "", "", "", ""));

            }
        }


        #region Get_Offers
        public ActionResult GetMainPageOffers(int take, int skip,Guid? cityId=null)
        {
            var foodOffers = Offtick.Business.Services.Managers.OfferManager.Instance.getMainPageFoodOffers(skip, take,cityId,MembershipManager.Instance.GetCurrentUser());
            return View(foodOffers);
        }


        public ActionResult OffersByCriteria(int take, int skip, string[] searchCriterias)
        {
            var offers = Offtick.Business.Services.Managers.OfferManager.Instance.getOffersByCriteria(searchCriterias, take, skip, MembershipManager.Instance.GetCurrentUser());
            return View("GetMainPageOffers", offers);
        }

        public ActionResult ResturantOffersByCriteriaGeneral(int take, int skip, string[] searchCriterias)
        {
            var offers = Offtick.Business.Services.Managers.OfferManager.Instance.getOffersByCriteria(searchCriterias, take, skip, EnumOfferType.Food, MembershipManager.Instance.GetCurrentUser());
            return View("GetMainPageOffers", offers);
        }

        public ActionResult ResturantOffersByCriteria(int take, int skip, string[] resturanName,string[] foodTitle,string[] address,string cityId,short? offtickType=null)
        {
            Guid guidCityId=Guid.Empty;
            bool cityIdHasValue=false;
            if(!string.IsNullOrEmpty(cityId)){
                cityIdHasValue= Guid.TryParse(cityId,out guidCityId);
            }
            var offers = Offtick.Business.Services.Managers.OfferManager.Instance.getFoodOffersByCriteria(take, skip, resturanName, foodTitle, address, cityIdHasValue ? (Guid?)guidCityId : null, MembershipManager.Instance.GetCurrentUser(), offtickType);
            return View("GetMainPageOffers", offers);
        }


        /// <summary>
        /// this method return offers of current  user + offers of users that current user follow them
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="take"></param>
        /// <param name="skip"></param>
        /// <returns></returns>
        public ActionResult WallOffers(string userName, int take, int skip)
        {
            IList<Offer> offers = null;
            

            var user = MembershipManager.Instance.GetUser(userName);
            if(user!=null){
                offers = Offtick.Business.Services.Managers.OfferManager.Instance.getWallOffersForUser(user, skip, take);
            }
            if ((offers == null || offers.Count() == 0) && skip == 0)
                return View("WallEmpty");
            else
                return View(offers);

        }

        public ActionResult ProfileOffers(string userName, int take, int skip)
        {
            IList<Offer> offers = null;
            if (!string.IsNullOrEmpty(userName))
            {
                var user = MembershipManager.Instance.GetUser(userName);
                 offers = Offtick.Business.Services.Managers.OfferManager.Instance.GetPublicOffersFor(user, skip, take, MembershipManager.Instance.GetCurrentUser());
            }

            return View("WallOffers", offers);

        }

        

        #endregion

        public ActionResult OfferManagementCombo(string currentUserName, string userNameOfOfferOwner,Guid offerId)
        {
            ViewBag.userNameOfOfferOwner = userNameOfOfferOwner;
             ViewBag.currentUserName=currentUserName;
             ViewBag.offerId = offerId;
             return View();
        }


        public ActionResult SimularOffersContainer(string offerId)
        {
            return View((object) offerId);
        }
        public ActionResult SimularOffers(string offerId, int take, int skip)
        {
            Guid guidOfferId;
            IList<Offer> offers = null;
            if (Guid.TryParse(offerId, out guidOfferId))
            {
                var offer = Offtick.Business.Services.Managers.OfferManager.Instance.GetOffer(guidOfferId);
                if (offer != null)
                    offers = Offtick.Business.Services.Managers.OfferManager.Instance.getSimularOffers(offer, skip, take);
            }
            return View(offers);
        }


        #region TextMode
        public JsonResult LoadTourOffersForMainPage(int? page, int? limit, string sortBy, string direction)
        {

            int take = limit.HasValue ? limit.Value : 20;
            int skip = 0;
            if (page.HasValue)
                skip = (page.Value - 1) * limit.Value;


            var offers = OfferManager.Instance.getMainPageTourOffers(null, MembershipManager.Instance.GetCurrentUser());
           // IQueryable<OfferTour> offers=null;
            int total = offers.Count();
            if (!string.IsNullOrEmpty(sortBy) && !string.IsNullOrEmpty(direction))
            {
                if (direction.Trim() == "asc")
                {
                    switch (sortBy)
                    {
                        case "Title":
                            offers = offers.OrderBy(q => q.Offer.Title);
                            break;
                        case "SourceCity":
                            offers = offers.OrderBy(q => q.MembershipCity.CityName);
                            break;
                        case "DestinationCity":
                            offers = offers.OrderBy(q => q.MembershipCity1.CityName);
                            break;
                        case "AgancyName":
                            offers = offers.OrderBy(q => q.AgancyName);
                            break;
                        case "WentDateTime":
                            offers = offers.OrderBy(q => q.WentDateTime);
                            break;
                        case "ReturnDateTime":
                            offers = offers.OrderBy(q => q.ReturnDateTime);
                            break;
                        case "DiscountPrice":
                            offers = offers.OrderBy(q => q.DiscountPrice);
                            break;
                        case "TravelType":
                            offers = offers.OrderBy(q => q.TravelType.Name);
                            break;
                    }
                }
                else
                {
                    switch (sortBy)
                    {
                        case "Title":
                            offers = offers.OrderByDescending(q => q.Offer.Title);
                            break;
                        case "SourceCity":
                            offers = offers.OrderByDescending(q => q.MembershipCity.CityName);
                            break;
                        case "DestinationCity":
                            offers = offers.OrderByDescending(q => q.MembershipCity1.CityName);
                            break;
                        case "AgancyName":
                            offers = offers.OrderByDescending(q => q.AgancyName);
                            break;
                        case "WentDateTime":
                            offers = offers.OrderByDescending(q => q.WentDateTime);
                            break;
                        case "ReturnDateTime":
                            offers = offers.OrderByDescending(q => q.ReturnDateTime);
                            break;
                        case "DiscountPrice":
                            offers = offers.OrderByDescending(q => q.DiscountPrice);
                            break;
                        case "TravelType":
                            offers = offers.OrderByDescending(q => q.TravelType.Name);
                            break;
                    }
                }
            }

            var records = offers.Skip(skip).Take(take).ToList().Select(e => new
            {
                OfferId=e.OfferId,
                Title=e.Offer.Title,
                SourceCity=e.MembershipCity.CityName,
                DestinationCity=e.MembershipCity1.CityName,
                AgancyName=e.AgancyName,
                WentDateTime= e.WentDateTime.HasValue? PersianDateConvertor.ToKhorshidiDate( e.WentDateTime.Value):string.Empty,
                ReturnDateTime = e.ReturnDateTime.HasValue ? PersianDateConvertor.ToKhorshidiDate(e.ReturnDateTime.Value) : string.Empty,
                Durration=e.ReturnDateTime.HasValue && e.WentDateTime.HasValue? (e.ReturnDateTime.Value-e.WentDateTime.Value).TotalDays.ToString():string.Empty,
                DiscountPrice=e.DiscountPrice,
                TravelType = e.TravelType.Name,
                Icon="",
                Grade="",

            }).ToArray();
            // return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "search offers", offers.ToArray(), ActionResponseFromServerToClient.OffersServerToClientKey));
            return Json(new { records, total }, JsonRequestBehavior.AllowGet);
        }
        #endregion

    }


    public class SoldOfferGroupByDTO
    {
             public string    Title{get;set;}
             public string    Body {get;set;}
             public Guid    OfferId{get;set;}
             public string    FileName {get;set;}
             public long    OriginalPrice {get;set;}
             public long DiscountPrice { get; set; }
             public short    DiscountPercent {get;set;}
             public long SoldCount { get; set; }
             public long TotalSoldValue { get; set; }
             public string    DateTime{get;set;}
             public int UserCount { get; set; }
    }
    public class SoldOfferByOfferDTO
    {
        public string DateTime { get; set; }
        public long DiscountPrice { get; set; }
        public long Quantity { get; set; }
        public long PaymentAmount { get; set; }
        public string DisplayName { get; set; }
        public string UserName { get; set; }
        public Guid UserId { get; set; }
        public string PhoneNumber { get; set; }
      
    }

    public class BuyedOfferByUserDTO
    {
        public string Title { get; set; }
        public string LongTitle { get; set; }
        public Guid OfferId { get; set; }
        public Guid SallerUserId { get; set; }
        public string SallerUserName { get; set; }
        public string FileName { get; set; }
        public string DateTime { get; set; }
        public long Quantity { get; set; }
        public long PaymentAmount { get; set; }
        public long DiscountAmount { get; set; }

    }
}
