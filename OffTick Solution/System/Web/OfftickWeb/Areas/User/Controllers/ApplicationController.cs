﻿using Offtick.Business.Membership;
using Offtick.Business.Services.Managers;
using Offtick.Business.Services.Storage;
using Offtick.Business.Web;
using Offtick.Web.OfftickWeb.Areas.User.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Offtick.Web.OfftickWeb.Areas.User.Controllers
{
    public class ApplicationController : BaseController
    {

        public ApplicationController(DataContextContainer obj)
            : base(obj)
        {

        }

        //
        // GET: /User/Application/

        public ActionResult Index()
        {
            var applications = Offtick.Business.Services.Managers.ApplicationManager.Instance.GetAllApplications();

            return View(applications);
        }
        public ActionResult ShowApplications(string layout)
        {
            if (!string.IsNullOrEmpty(layout))
            {
                ViewBag.customLayout = "~/views/shared/_SimpleLayout.cshtml";
            }
            var applications = Offtick.Business.Services.Managers.ApplicationManager.Instance.GetAllApplications();

            return View(applications);
        }



        public ActionResult ShowParentApplication(Offtick.Data.Context.ExpertOnlinerContexts.Application application,bool hasLayout=true)
        {
            TempData["showParentLayout"] = hasLayout;
            return View(application);
        }

        public ActionResult ShowParentApplicationExept(Offtick.Data.Context.ExpertOnlinerContexts.Application except,Offtick.Data.Context.ExpertOnlinerContexts.Application parent)
        {
            IList<Offtick.Data.Context.ExpertOnlinerContexts.Application> apps = new List<Offtick.Data.Context.ExpertOnlinerContexts.Application>();
            apps.Add(except);
            var newApps = parent.Application1.Except(apps).ToList();
            return View(newApps);
        }

        public ActionResult SelectApplication(long applicationId)
        {
            var application = Offtick.Business.Services.Managers.ApplicationManager.Instance.GetApplicationById(applicationId);

            if (application != null)
            {
                if (application.Application1 != null && application.Application1.Count() > 0)
                {
                    return View("ShowParentApplication", application);
                }
                else
                if (application.Name == "MBTI")
                {
                    Offtick.Web.OfftickWeb.Areas.User.Models.MBTIModel model = new MBTIModel();
                    model.QuestionNo = 0;
                    model.MBTIStatus = ApplicationStatus.PreStart;
                    return RedirectToAction("MBTI", model);
                }
                else if (application.Name == "Holland")
                {
                    //HollandModel model = new HollandModel()
                    //{
                    //    ApplicationStatus = ApplicationStatus.PreStart,
                    //    PageTake = 15,
                    //};
                    object model=null;
                    return RedirectToAction("Holland",model);
                }
                else if (application.Name == "SelectMajor")
                {
                    object model = null;
                    return RedirectToAction("SelectMajor", model);
                }
                else
                {
                    return RedirectToAction("OfftickOffer", "Search", new { area = "User", offerName =application.Name});
                }
            }
            return View(application);
        }


        
        public ActionResult MBTI(MBTIModel model)
        {
            var currentUser=MembershipManager.Instance.GetCurrentUser();
            
            
            if(model==null || model.MBTIStatus!= ApplicationStatus.Reset && ApplicationManager.Instance.isValidAnswersForMBTI(currentUser))
                return View(new MBTIModel() { QuestionNo = -1,MBTIStatus = ApplicationStatus.Finished });


            if (model == null || (model.MBTIStatus == ApplicationStatus.PreStart && model.QuestionNo<=0))
            {
                long lastAnsweredQuestionNo = ApplicationManager.Instance.getLastMBTIQuestion(currentUser);
                if (lastAnsweredQuestionNo > 0)
                {
                    model = new Models.MBTIModel() { QuestionNo = (short)lastAnsweredQuestionNo, MBTIStatus = ApplicationStatus.Next };
                }
                else
                    model = new Models.MBTIModel() { QuestionNo = 0, MBTIStatus = ApplicationStatus.PreStart };
            }
             if (model.MBTIStatus == ApplicationStatus.Next)
            {
                long lastAnsweredQuestionNo = ApplicationManager.Instance.getLastMBTIQuestion(currentUser);
                long nextQuestionNo = ApplicationManager.Instance.getNextMBTIQuestion(currentUser, model.QuestionNo);
                if (nextQuestionNo <= lastAnsweredQuestionNo + 1)
                {
                    model.QuestionNo = (short)nextQuestionNo;
                    model.MBTIStatus = ApplicationStatus.Running;
                }
            }
            else if (model.MBTIStatus == ApplicationStatus.Previous)
            {
                long firstAnsweredQuestionNo = 1;
                long previousQuestionNo = ApplicationManager.Instance.getPreviousMBTIQuestion(currentUser, model.QuestionNo);
                if (previousQuestionNo >= firstAnsweredQuestionNo)
                {
                    model.QuestionNo = (short)previousQuestionNo;
                    model.MBTIStatus = ApplicationStatus.Running;
                }
            }
            else if (model.MBTIStatus == ApplicationStatus.Running)
            {
                //step 0: check is exist previus records and not exit current question
                if (ApplicationManager.Instance.isValidPreviousAnswersForMBTI(currentUser, model.QuestionNo))
                // && !ApplicationManager.Instance.isExistedAnswerForMBTI(currentUser, model.QuestionNo))// no need this criteria, because we want to allo update answered question
                {
                    //step 1: save model
                    var addStatus = ApplicationManager.Instance.AddOrUpdateAnswerForMBTI(currentUser, model.QuestionNo, model.AnswerNo);
                    //step 2: check criteria
                    if (addStatus == Core.EnumTypes.AddStatus.Added)
                    {
                        if (ApplicationManager.Instance.isValidAnswersForMBTI(currentUser))
                        {
                            model.QuestionNo = -1;
                            model.MBTIStatus = ApplicationStatus.Finished;
                        }
                        else
                        {
                            model.QuestionNo = (short)ApplicationManager.Instance.getNextMBTIQuestion(currentUser, model.QuestionNo);
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("", "Not valid state");
                    }

                }
                else
                {
                    //go to valid question and continue running
                    long questionNo = ApplicationManager.Instance.getLastMBTIQuestion(currentUser);
                    long nextQuestionNo = ApplicationManager.Instance.getNextMBTIQuestion(currentUser, questionNo);
                    return View(new MBTIModel() { QuestionNo = (short)nextQuestionNo, MBTIStatus = ApplicationStatus.Running });

                }

            }
            else if (model.MBTIStatus == ApplicationStatus.Reset)
            {
                bool result = ApplicationManager.Instance.ResetMBTIFor(currentUser);
                if (result == true)
                {
                    model.MBTIStatus = ApplicationStatus.PreStart;
                    model.QuestionNo = 1;
                }
            }
             else if (model.MBTIStatus == ApplicationStatus.FinishedNotPersisted)
             {
                var mbtiResult=ApplicationManager.Instance.GetMBTIResultForUser(null);
                if (currentUser != null && mbtiResult != null && ApplicationManager.Instance.isValidAnswersForMBTI(mbtiResult))
                {
                    ApplicationManager.Instance.persistMBTIResultForCurrentUser();
                    model.QuestionNo = -1;
                    model.MBTIStatus = ApplicationStatus.Finished;
                }
                else
                {
                    model.MBTIStatus = ApplicationStatus.Reset;
                }

             }

            return View(model);
        }

        [Authorize]
        public ActionResult MBTIAdmin()
        {
            return View();
        }


       

        
        public ActionResult Holland(HollandModel model)
        {
                                int pageTake = 15;

            var currentUser = MembershipManager.Instance.GetCurrentUser();
            if (model == null || model.ApplicationStatus != ApplicationStatus.Reset && ApplicationManager.Instance.isValidAnswersForHolland(currentUser))
                return View(new HollandModel() { ApplicationStatus = ApplicationStatus.Finished });


            if (model == null || model.ApplicationStatus == ApplicationStatus.PreStart)
            {
                long lastAnsweredQuestionNo = ApplicationManager.Instance.getLastHollandQuestion(currentUser);
                if (lastAnsweredQuestionNo > 0)
                {
                    int pageNo = (int)lastAnsweredQuestionNo / pageTake + lastAnsweredQuestionNo % pageTake != 0 ? 1 : 0;
                    model = new HollandModel() { PageNo = (short)pageNo, PageTake = (short)pageTake, ApplicationStatus = ApplicationStatus.Next, Level = 1 };
                }
              
            }
            if (model.ApplicationStatus == ApplicationStatus.Next)
            {
                long lastAnsweredQuestionNo = ApplicationManager.Instance.getLastHollandQuestion(currentUser);
                long maxAnsweredPassed = model.PageNo * pageTake;
                long nextQuestionNo = ApplicationManager.Instance.getNextHollandQuestion(currentUser, maxAnsweredPassed);
                if (nextQuestionNo <= lastAnsweredQuestionNo + 1)
                {
                    model.PageNo += 1;
                    model.ApplicationStatus = ApplicationStatus.Running;
                    model.Answers = "";
                }
            }
            else if (model.ApplicationStatus == ApplicationStatus.Previous)
            {
                long firstAnsweredQuestionNo = 1;
                long minAnsweredPassed = (model.PageNo-1) * pageTake+1;

                long previousQuestionNo = ApplicationManager.Instance.getPreviousHollandQuestion(currentUser, minAnsweredPassed);
                if (previousQuestionNo >= firstAnsweredQuestionNo)
                {
                    model.PageNo =(short)Math.Max(1, model.PageNo-1);
                    model.ApplicationStatus = ApplicationStatus.Running;
                }
            }
            else if (model.ApplicationStatus == ApplicationStatus.Running)
            {
                //step 0: check is exist previus records and not exit current question
                long minAnsweredPassed =Math.Min(216, (model.PageNo - 1) * pageTake + 1);
                
                if (ApplicationManager.Instance.isValidPreviousAnswersForHolland(currentUser, minAnsweredPassed))
                {
                    string[] answers = model.Answers.Split('_');
                    for (int k = 0; k < answers.Length; k++)
                    {
                        string answer = answers[k];
                        if (!string.IsNullOrEmpty(answer) && answer.Contains(":"))
                        {
                            string questionNo = answer.Substring(0, answer.IndexOf(":"));
                            string answerFlag = answer.Substring(answer.IndexOf(":")+1);
                            int questionNoInt;
                            int answerFlagInt;
                            if(int.TryParse(questionNo,out questionNoInt) && int.TryParse(answerFlag,out answerFlagInt)){
                                //step 1: save model
                                var addStatus = ApplicationManager.Instance.AddOrUpdateAnswerForHolland(currentUser, (short)questionNoInt, (short)answerFlagInt);
                                //step 2: check criteria
                                if (addStatus == Core.EnumTypes.AddStatus.Added)
                                {
                                    if (ApplicationManager.Instance.isValidAnswersForHolland(currentUser))
                                    {
                                        model.ApplicationStatus = ApplicationStatus.Finished;
                                       
                                    }
                                }
                                else
                                {
                                    ModelState.AddModelError("", "Not valid state");
                                }
                            }
                          
                        }
                    }
                    if (model.ApplicationStatus != ApplicationStatus.Finished)
                    {
                        model.PageNo++;
                    }

                }
                else
                {
                    //go to valid question and continue running
                    long questionNo = ApplicationManager.Instance.getLastHollandQuestion(currentUser);
                    long nextQuestionNo = ApplicationManager.Instance.getNextHollandQuestion(currentUser, questionNo);
                    int pageNo = (int)nextQuestionNo / pageTake + 1;
                    return View(new HollandModel() { PageNo = (short)pageNo, ApplicationStatus = ApplicationStatus.Running });

                }

            }
            else if (model.ApplicationStatus == ApplicationStatus.Reset)
            {
                bool result = ApplicationManager.Instance.ResetHollandFor(currentUser);
                if (result == true)
                {
                    model.ApplicationStatus = ApplicationStatus.PreStart;
                    model.PageNo = 0;
                }
            }
            else if (model.ApplicationStatus == ApplicationStatus.FinishedNotPersisted)
            {
                var hollandResult = ApplicationManager.Instance.GetHollandResultForUser(null);
                if (currentUser != null && hollandResult != null && ApplicationManager.Instance.isValidAnswersForHolland(hollandResult))
                {
                    ApplicationManager.Instance.persistHollandResultForCurrentUser();
                    model.ApplicationStatus = ApplicationStatus.Finished;
                }
                else
                {
                    model.ApplicationStatus = ApplicationStatus.Reset;
                }

            }

            return View(model);
        }

        public ActionResult SelectMajor(SelectMajorModel model)
        {
            

            var currentUser = MembershipManager.Instance.GetCurrentUser();
            var persistedObject = ApplicationManager.Instance.GetMajorChoiceResultForUser(currentUser);
           

         


            if (model.ApplicationStatus == ApplicationStatus.PreStart)
            {
                if (persistedObject != null )
                {
                    model.ApplicationStatus = ApplicationManager.Instance.isCalculatedForMajorChoice(persistedObject) ? ApplicationStatus.Finished : ApplicationStatus.Running;
                    model.ApplicationMajorChoiceHighSchoolId = persistedObject.ApplicationMajorChoiceHighSchoolId;
                    model.Step =model.ApplicationStatus== ApplicationStatus.Finished? 5: 2;
                }
            }
            if (model.ApplicationStatus == ApplicationStatus.Previous)
            {
                model.ApplicationStatus = ApplicationStatus.Running;
                if (persistedObject != null)
                {
                    model.ApplicationMajorChoiceHighSchoolId = persistedObject.ApplicationMajorChoiceHighSchoolId;
                    if (model.Step == 3)
                    {
                        model.CityWeight = persistedObject.CityWeight.HasValue ? persistedObject.CityWeight.Value : 0;
                        model.UniversityWeight = persistedObject.UniversityWeight.HasValue ? persistedObject.UniversityWeight.Value : 0;
                        model.MajorWeight = persistedObject.MajorWeight.HasValue ? persistedObject.MajorWeight.Value : 0;
                    }
                }
               
            }
            else if (model.ApplicationStatus == ApplicationStatus.Running || model.ApplicationStatus == ApplicationStatus.Next)
            {
                if (model.Step == 2)
                {
                    ApplicationManager.Instance.AddOrUpdateMajorChoiceHighSchool(currentUser, model.ApplicationMajorChoiceHighSchoolId);
                }

                if (model.Step ==3 && !string.IsNullOrEmpty(model.StringChoices))
                {
                    ApplicationManager.Instance.ClearMajorChoices(currentUser);// if model.StringChoices is not null means choices are changed and must clean and register new ones

                    var splitor = new[] { "__" };
                    string[] answers = model.StringChoices.Split(splitor, StringSplitOptions.None);
                   
                    for (int k = 0; k < answers.Length; k++)
                    {
                        string choice = answers[k];
                        if (!string.IsNullOrEmpty(choice))
                        {
                            string[] majorCityUniversity = choice.Split('_');
                            int ApplicationMajorChoiceCityId = 0;
                            int ApplicationMajorChoiceUniversityMajorId = 0;
                            int ApplicationMajorChoiceUniversityId = 0;

                            foreach (var fieldValue in majorCityUniversity)
                            {


                                string field = fieldValue.Substring(0, fieldValue.IndexOf(":"));
                                string valueOfField = fieldValue.Substring(fieldValue.IndexOf(":") + 1);
                                int valueOfFieldInt = -1;
                                if (int.TryParse(valueOfField, out valueOfFieldInt))
                                {
                                    if (field == "ApplicationMajorChoiceCityId")
                                        ApplicationMajorChoiceCityId = valueOfFieldInt;
                                    if (field == "ApplicationMajorChoiceUniversityMajorId")
                                        ApplicationMajorChoiceUniversityMajorId = valueOfFieldInt;
                                    if (field == "ApplicationMajorChoiceUniversityId")
                                        ApplicationMajorChoiceUniversityId = valueOfFieldInt;

                                }
                            }

                            ApplicationManager.Instance.AddOrUpdateMajorChoice(currentUser, ApplicationMajorChoiceCityId, ApplicationMajorChoiceUniversityMajorId, ApplicationMajorChoiceUniversityId);
                        }
                    }

                    ApplicationManager.Instance.RefreshMajorChoiceGrades(currentUser);
                    model.CityWeight = persistedObject.CityWeight.HasValue? persistedObject.CityWeight.Value:0;
                    model.UniversityWeight = persistedObject.UniversityWeight.HasValue ? persistedObject.UniversityWeight.Value : 0;
                    model.MajorWeight = persistedObject.MajorWeight.HasValue ? persistedObject.MajorWeight.Value : 0;

                    
                }
                if (model.Step == 4)
                {
                    ApplicationManager.Instance.AddOrUpdateMajorChoiceWeight(currentUser,model.CityWeight,model.MajorWeight,model.UniversityWeight);
                }
                if (model.Step == 5 && !string.IsNullOrEmpty(model.StringChoices))
                {
                    var splitor = new[] { "__" };// 3 parts: part 1: cities, part 2: universities, part 3: majors
                    string[] cityUniversityMajorGrades = model.StringChoices.Split(splitor, StringSplitOptions.None);

                    
                        string cities_gradesVal = cityUniversityMajorGrades[0];
                        if (!string.IsNullOrEmpty(cities_gradesVal))
                        {
                            string[] cities_gradesValArray = cities_gradesVal.Split('_');
                            foreach (var fieldValue in cities_gradesValArray)
                            {
                                string entityId = fieldValue.Substring(0, fieldValue.IndexOf(":"));
                                string valueOfEntityId = fieldValue.Substring(fieldValue.IndexOf(":") + 1);
                                int valueOfFieldInt = -1;
                                int entityIdInt = -1;
                                if (int.TryParse(valueOfEntityId, out valueOfFieldInt) && int.TryParse(entityId, out entityIdInt))
                                {
                                    ApplicationManager.Instance.AddOrUpdateMajorGrade(currentUser, entityIdInt, valueOfFieldInt, null, null, null, null);
                                }
                            }
                        }

                        string universities_gradesVal = cityUniversityMajorGrades[1];
                        if (!string.IsNullOrEmpty(universities_gradesVal))
                        {
                            string[] universities_gradesValArray = universities_gradesVal.Split('_');
                            foreach (var fieldValue in universities_gradesValArray)
                            {
                                string entityId = fieldValue.Substring(0, fieldValue.IndexOf(":"));
                                string valueOfEntityId = fieldValue.Substring(fieldValue.IndexOf(":") + 1);
                                int valueOfFieldInt = -1;
                                int entityIdInt = -1;
                                if (int.TryParse(valueOfEntityId, out valueOfFieldInt) && int.TryParse(entityId, out entityIdInt))
                                {
                                    ApplicationManager.Instance.AddOrUpdateMajorGrade(currentUser, null, null, null, null, entityIdInt, valueOfFieldInt);
                                    
                                }
                            }
                        }

                        string majors_gradesVal = cityUniversityMajorGrades[2];
                        if (!string.IsNullOrEmpty(majors_gradesVal))
                        {
                            string[] majors_gradesValArray = majors_gradesVal.Split('_');
                            foreach (var fieldValue in majors_gradesValArray)
                            {
                                string entityId = fieldValue.Substring(0, fieldValue.IndexOf(":"));
                                string valueOfEntityId = fieldValue.Substring(fieldValue.IndexOf(":") + 1);
                                int valueOfFieldInt = -1;
                                int entityIdInt = -1;
                                if (int.TryParse(valueOfEntityId, out valueOfFieldInt) && int.TryParse(entityId, out entityIdInt))
                                {
                                    ApplicationManager.Instance.AddOrUpdateMajorGrade(currentUser, null, null, entityIdInt, valueOfFieldInt, null, null);
                                }
                            }
                        }
                        
                        model.ApplicationStatus = ApplicationStatus.Finished;
                        ApplicationManager.Instance.CalculteMajorChoiceFor(currentUser);
                }
                if (model.Step >5){
                    model.ApplicationMajorChoiceHighSchoolId = persistedObject.ApplicationMajorChoiceHighSchoolId;
                    model.Step = 2;
                }
            }



            else if (model.ApplicationStatus == ApplicationStatus.Reset)
            {
                bool result = ApplicationManager.Instance.ResetMajorChoiceFor(currentUser);
                if (result == true)
                {
                    model.ApplicationStatus = ApplicationStatus.PreStart;

                }
            }
            else if (model.ApplicationStatus == ApplicationStatus.FinishedNotPersisted)
            {
                //var hollandResult = ApplicationManager.Instance.GetHollandResultForUser(null);
                if (currentUser != null)// && hollandResult != null && ApplicationManager.Instance.isValidAnswersForHolland(hollandResult))
                {
                    persistedObject = ApplicationManager.Instance.GetMajorChoiceResultForUser(null);
                    model.ApplicationMajorChoiceHighSchoolId = persistedObject.ApplicationMajorChoiceHighSchoolId;
                    ApplicationManager.Instance.PersistChoiceMajorResultForCurrentUser();
                    model.ApplicationStatus = ApplicationStatus.Finished;
                    model.Step = 5;
                }
                else
                {
                    model.ApplicationStatus = ApplicationStatus.Reset;
                }

            }

            return View(model);
        }


        [HttpPost]
        public JsonResult CheckRandomNumberIsValid(long randomNumber)
        {
            bool isValid = false;
            var currentUser = MembershipManager.Instance.GetCurrentUser();
            var persistedSelectMajor = ApplicationManager.Instance.GetMajorChoiceResultForUser(currentUser);
            var persistedHolland = ApplicationManager.Instance.GetHollandResultForUser(currentUser);
            var numberSessionobject = System.Web.HttpContext.Current.Session["randomNumberSessionObjectKey"];
            isValid = (currentUser != null &&
                    ((persistedSelectMajor != null && persistedSelectMajor.PasswordNumber.HasValue && persistedSelectMajor.PasswordNumber.Value.Equals(randomNumber) )
                        ||
                    (persistedHolland != null && persistedHolland.PasswordNumber.HasValue && persistedHolland.PasswordNumber.Value.Equals(randomNumber))))
                    ||
                    numberSessionobject != null &&  randomNumber.Equals((long) numberSessionobject)
                    ;
            if (!isValid)
            {
                var numberEntity=ApplicationManager.Instance.GetApplicationRandomNumberByNumber(randomNumber);
                isValid = numberEntity != null && !numberEntity.IsUsed;
            }
            if (isValid)
            {
                ApplicationManager.Instance.AddOrUpdateRandomNumber(randomNumber);
            }
            return Json(new { IsValid = isValid });
        }
    }
}
