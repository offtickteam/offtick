﻿using Offtick.Business.Membership;
using Offtick.Business.Services.Managers;
using Offtick.Business.Services.RealtimeSearvices;
using Offtick.Business.Services.Storage;
using Offtick.Business.Services.Storage.File;
using Offtick.Business.Web;
using Offtick.Data.Entities.Common;
using Offtick.Data.Entities.Common.Models.Chat;
using Offtick.Web.OfftickWeb.Infrustracture;
using Offtick.Data.Context.ExpertOnlinerContexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Web.Mvc;
using Offtick.Core.Utility;


namespace Offtick.Web.OfftickWeb.Areas.User.Controllers
{
    [Authorize]
    public class ConversationController : BaseController
    {

        public ConversationController(DataContextContainer obj)
            : base(obj)
        {

        }

        //
        // GET: /User/Conversation/

        public ActionResult Index(string otherUserName)
        {
            ViewBag.otherUserName = otherUserName;
            var currentUser = MembershipManager.Instance.GetCurrentUser();
            var query = from m in currentUser.Messages.Union(currentUser.Messages1)
                        group m by new { m.RecieverUserId, m.SenderUserId } into groupMessage
                        select groupMessage;

            IList<Offtick.Data.Context.ExpertOnlinerContexts.Message> lsResult = new List<Offtick.Data.Context.ExpertOnlinerContexts.Message>();
            foreach (var item in query)
            {
                var condidateItem = item.OrderBy(e => e.DateTime).Last();
                var existBefore = lsResult.FirstOrDefault(e => (e.SenderUserId.Equals(condidateItem.SenderUserId)
                                            && e.RecieverUserId.Equals(condidateItem.RecieverUserId)
                                            ||
                                            (e.SenderUserId.Equals(condidateItem.RecieverUserId)
                                            && e.RecieverUserId.Equals(condidateItem.SenderUserId))));
                if (existBefore == null)
                    lsResult.Add(condidateItem);
            }


            return View(lsResult);
        }

        public static int countOfUnRead()
        {
            var currentUser = MembershipManager.Instance.GetCurrentUser();
            return currentUser.Messages1.Where(
                      m => !m.IsRead.HasValue || !m.IsRead.Value).Count();

        }

        [Authorize]
        public ActionResult ConversationFor(string otherUserName)
        {
            ViewBag.otherUserName = otherUserName;
            var currentUser = MembershipManager.Instance.GetCurrentUser();
            ChatService chatService = new ChatService();
            chatService.SRpcReadMessages(new Business.Services.RealtimeSearvices.EventDrivenDispatcher.Notification.BidirectionalInteractionNotification()
            {
                FirstUserName = otherUserName,
                SecondUserName = currentUser.UserName,
            });
            IEnumerable<Message> query = currentUser.UserName.Equals(otherUserName) ?
                from m in currentUser.Messages
                where m.MembershipUser1.UserName.Equals(otherUserName)
                select m
                : from m in currentUser.Messages.Union(currentUser.Messages1)
                  where m.MembershipUser.UserName.Equals(otherUserName) || m.MembershipUser1.UserName.Equals(otherUserName)
                  select m;

            return View(query.OrderBy(e => e.DateTime).ToList());
        }

        [Authorize]
        [AllowJsonGet]
        public JsonResult ListOfChatMemberFor(string userName)
        {
            if (!string.IsNullOrEmpty(userName))
            {
                var userEntity = MembershipManager.Instance.GetUser(userName);
                var userEntityId = userEntity.UserId;
                // IList<MembershipUser> lsSearchResult = userEntity != null ? (from e in userEntity.Follows where e.MembershipUser.UserId.Equals(userEntityId)||e.MembershipUser1.UserId.Equals(userEntityId) select e.MembershipUser).Distinct().ToList() : null;

                IList<MembershipUser> lsSearchResult = userEntity != null ? (from e in userEntity.Follows where e.MembershipUser.UserId.Equals(userEntityId) || e.MembershipUser1.UserId.Equals(userEntityId) select e.MembershipUser).Distinct().ToList() : null;

                if (lsSearchResult != null && lsSearchResult.Count() > 0)
                {
                    IList<ChatUserModel> lsResultToClient = new List<ChatUserModel>();
                    int counter = 0;
                    foreach (var user in lsSearchResult)
                    {

                        ImageFileManager imgFileManager = new ImageFileManager(user.UserId.ToString());

                        //byte[] img32 = imgFileManager.GetBytesOfImage(imgFileManager.GetFullFileName(following.Picture, ImageState.Thumbnail32));

                        string imgAddress = imgFileManager.GetRelativeFullFileName(user.UserId.ToString(), user.Picture, true);

                        var userClientObj = new ChatUserModel
                        {
                            firstName = user.FirstName,
                            lastName = user.LastName,
                            userName = user.UserName,
                            // imageThumbnail = img32 != null ? Convert.ToBase64String(img32) : string.Empty,
                            imageThumbnail = imgAddress,
                            fallowStatus = FollowManager.Instance.GetFollowStatusFor(MembershipManager.Instance.GetCurrentUser(), user),
                        };
                        //lsResultToClient[counter] = userClientObj;
                        lsResultToClient.Add(userClientObj);
                        counter++;
                    }
                    return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "found items", lsResultToClient.ToArray(), ActionResponseFromServerToClient.ChatMembersClientKey));
                }
                else
                    return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "No item found", string.Empty, string.Empty, string.Empty));

            }
            return null;
        }

        [Authorize]
        [AllowJsonGet]
        public JsonResult ConversationListForUser(string userName)
        {
            if (!string.IsNullOrEmpty(userName))
            {
                var userEntity = MembershipManager.Instance.GetUser(userName);
                var userEntityId = userEntity.UserId;


                var currentUser = MembershipManager.Instance.GetCurrentUser();
                var query = from m in currentUser.Messages.Union(currentUser.Messages1)
                            orderby m.DateTime
                            group m by new { m.MembershipUser, m.MembershipUser1 } into groupMessage
                            select new
                            {
                                SenderUser = groupMessage.Key.MembershipUser,
                                RecieverUser = groupMessage.Key.MembershipUser1,
                                LastMessageBody = groupMessage.Last().Body,
                                LastMessageDateTime = groupMessage.Last().DateTime,
                                CountOfUnRead = groupMessage.Count(m => !m.IsRead.HasValue || !m.IsRead.Value)
                            };


                List<ConversationListRowDTO> lsResultToClient = new List<ConversationListRowDTO>();
                foreach (var item in query)
                {

                    var existBefore = lsResultToClient.FirstOrDefault(e => (e.SelfUserId.Equals(item.SenderUser.UserId)
                                           && e.OtherUserId.Equals(item.RecieverUser.UserId)
                                           ||
                                           (e.SelfUserId.Equals(item.RecieverUser.UserId)
                                           && e.OtherUserId.Equals(item.SenderUser.UserId))));
                    if (existBefore != null)
                    {
                        if (item.RecieverUser.UserId.Equals(userEntityId))
                        {
                            existBefore.CountOfUnread = item.CountOfUnRead;
                        }
                        if (existBefore.LastMessageDateTime < item.LastMessageDateTime)
                        {
                            existBefore.LastMessageBody = item.LastMessageBody.TruncateAtWord(50);

                            existBefore.LastMessageDateTime = item.LastMessageDateTime;
                            existBefore.LastMessageDateTimeFa = Offtick.Web.OfftickWeb.Infrustracture.GeneralHelper.GetPersianDate(item.LastMessageDateTime);

                        }

                    }
                    else
                    {
                        ConversationListRowDTO conversationListRowDTO = new Controllers.ConversationListRowDTO();
                        var selfUser = item.SenderUser.UserId.Equals(userEntityId) ? item.SenderUser : item.RecieverUser;
                        var otherUser = !item.SenderUser.UserId.Equals(userEntityId) ? item.SenderUser : item.RecieverUser;
                        ImageFileManager selfImgFileManager = new ImageFileManager(selfUser.UserId.ToString());
                        ImageFileManager otherImgFileManager = new ImageFileManager(otherUser.UserId.ToString());

                        conversationListRowDTO.SelfUserId = selfUser.UserId;
                        conversationListRowDTO.SelfUserName = selfUser.UserName;

                        var userImgUrlDefault = "/Content/Images/personal32.png";
                        var otherImgUrl = otherImgFileManager.GetFullFileName(otherUser.Picture, true);
                        otherImgUrl = otherImgFileManager.IsExit(otherImgUrl) ?
                            otherImgUrl = otherImgFileManager.GetRelativeFullFileName(otherUser.UserId.ToString(), otherUser.Picture, true) : userImgUrlDefault;

                        var selfImgUrl = selfImgFileManager.GetFullFileName(selfUser.Picture, true);
                        selfImgUrl = selfImgFileManager.IsExit(selfImgUrl) ?
                            selfImgFileManager.GetRelativeFullFileName(selfUser.UserId.ToString(), selfUser.Picture, true) : userImgUrlDefault;

                        conversationListRowDTO.SelfUserPicture = selfImgUrl;
                        conversationListRowDTO.SelfFirstName = selfUser.FirstName;
                        conversationListRowDTO.SelfLastName = selfUser.LastName;
                        conversationListRowDTO.OtherFirstName = otherUser.FirstName;
                        conversationListRowDTO.OtherLastName = otherUser.LastName;
                        conversationListRowDTO.OtherUserId = otherUser.UserId;
                        conversationListRowDTO.OtherUserName = otherUser.UserName;
                        conversationListRowDTO.OtherUserPicture = otherImgUrl;
                        conversationListRowDTO.CountOfUnread = item.RecieverUser.UserId.Equals(userEntityId) ? item.CountOfUnRead : 0;
                        conversationListRowDTO.LastMessageBody = item.LastMessageBody.TruncateAtWord(50);
                        conversationListRowDTO.LastMessageDateTime = item.LastMessageDateTime;
                        conversationListRowDTO.LastMessageDateTimeFa = Offtick.Web.OfftickWeb.Infrustracture.GeneralHelper.GetPersianDate(item.LastMessageDateTime);

                        lsResultToClient.Add(conversationListRowDTO);
                    }

                }
                return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "found items", lsResultToClient.ToArray(), ActionResponseFromServerToClient.ChatMembersClientKey));
                //    return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "No item found", string.Empty, string.Empty, string.Empty));

            }
            return null;
        }


        public static int CountOfUnreadFor(Offtick.Data.Context.ExpertOnlinerContexts.MembershipUser other)
        {

            var currentUser = MembershipManager.Instance.GetCurrentUser();
            if (currentUser == null || other == null)
                return 0;
            return currentUser.Messages1.Where(
                   m => (!m.IsRead.HasValue || !m.IsRead.Value) && m.SenderUserId.Equals(other.UserId)).Count();
        }

        [Authorize]
        [AllowJsonGet]
        public JsonResult SearchUser(string search)
        {
            if (string.IsNullOrEmpty(search))
                return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Empty, "Not found", null, ActionResponseFromServerToClient.SearchUserDTOServerToClientKey));

            var users=MembershipManager.Instance.GetUsersByCriteria(new[] { search }, 10, 0);
            if(users==null || users.Count==0)
                return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Empty, "Not found", null, ActionResponseFromServerToClient.SearchUserDTOServerToClientKey));
            IList<SearchUserDTO> lsResult = new List<SearchUserDTO>();
            foreach (var user in users)
            {
                ImageFileManager otherImgFileManager = new ImageFileManager(user.UserId.ToString());
                var userImgUrlDefault = "/Content/Images/personal32.png";
                var otherImgUrl = otherImgFileManager.GetFullFileName(user.Picture, true);
                otherImgUrl = otherImgFileManager.IsExit(otherImgUrl) ?
                    otherImgUrl = otherImgFileManager.GetRelativeFullFileName(user.UserId.ToString(), user.Picture, true) : userImgUrlDefault;

                lsResult.Add(new SearchUserDTO()
                {
                     FirstName=user.FirstName,
                     LastName=user.LastName,
                     UserName=user.UserName,
                     ImageUrl=otherImgUrl,
                });
            }
            return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "found items", lsResult.ToArray(), ActionResponseFromServerToClient.SearchUserDTOServerToClientKey));

        }


    }


     public class ConversationListRowDTO
     {
         public string OtherFirstName { get; set; }
         public string OtherLastName { get; set; }
         public Guid OtherUserId { get; set; }
         public string OtherUserName { get; set; }
         public string OtherUserPicture { get; set; }

         public string SelfFirstName { get; set; }
         public string SelfLastName { get; set; }
         public Guid SelfUserId { get; set; }
         public string SelfUserName { get; set; }
         public string SelfUserPicture { get; set; }

         public string LastMessageBody { get; set; }
         public DateTime LastMessageDateTime { get; set; }
         public string LastMessageDateTimeFa { get; set; }
         public int CountOfUnread { get; set; }

     }

     public class SearchUserDTO
     {
         public string FirstName { get; set; }
         public string LastName { get; set; }
         public string LastDateTimeEn { get; set; }
         public string LastDateTimeFa { get; set; }
         public string UserName { get; set; }
         public string ImageUrl { get; set; }
     }
}
