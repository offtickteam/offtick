﻿using Offtick.Business.Membership;
using Offtick.Business.Services.Domain;
using Offtick.Business.Services.Managers;
using Offtick.Business.Services.RealtimeSearvices;
using Offtick.Business.Services.RealtimeSearvices.EventDrivenDispatcher.Notification;
using Offtick.Business.Services.Storage;
using Offtick.Business.Web;
using Offtick.Core.EnumTypes;
using Offtick.Core.Utility.PersianTools;
using Offtick.Data.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Offtick.Web.OfftickWeb.Areas.Admin.Controllers
{
    public class HomeController : BaseController
    {
        public HomeController(DataContextContainer obj)
            : base(obj)
        {

        }

        //
        // GET: /Admin/Home/


        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoadPanel(string panel, string userName)
        {
            return View(panel);
        }

        [HttpPost]
        public ActionResult UserManagementPanel()
        {
            return View();
        }

        [HttpPost]
        public ActionResult MagfaSmsPanel()
        {
            return View();
        }

        [HttpPost]
        public ActionResult OfferPanel()
        {
            return View();
        }
        [HttpPost]
        public ActionResult UserProfilePanel()
        {
            return View();
        }


        [HttpPost]
        public ActionResult MagfaSmsSendTextMessage(string endPoint ,string domainName, string userName, string password, string sender, string receipt, string text)
        {
            long result = SmsService.Instance.SendMagfaSms(endPoint, domainName, userName, password, sender, receipt, text);
            if (result> 120)
                return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, "", result.ToString(), "", ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
            else
            {
                string errorMessage = Offtick.Business.SmsService.SmsErrorCodes.GetErrorCode(result);
                string messageToClient=string.Format("ErrorCode:{0} means: {1}:",result, errorMessage);
                return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed,messageToClient, "", "", ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
            }
            
        }

     

        #region UserManagement

        [HttpPost]
        public ActionResult ResetPasswordByAdmin(Guid userIdToResetPass)
        {
            string adminUserName=MembershipManager.Instance.GetCurrentUserName();
            var checkUser = MembershipManager.Instance.GetUser(userIdToResetPass);
            if (checkUser == null)
                return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "چنین کاربری وجود ندارد", "", "", ActionResponseFromServerToClient.EmptyObjectServerToClientKey));

            PasswordGenerator pg = new PasswordGenerator(true, true, true, false, 8, 3);
            string password =pg.Next(); 
            var editStatus = MembershipManager.Instance.ResetPassword(checkUser, password);
            if (editStatus == Core.EnumTypes.EditStatus.Edited)
            {
                string msg = string.Format("پسورد کاربر {0} به مقدار {1} تغییر پیدا کرد", checkUser.UserName, password);

                ChatService chatService = new ChatService();
                var chatServiceResponse = chatService.SRpcSendMessage(new SendMessageNotification()
                {
                    FromUserName = adminUserName,
                    Message = msg,
                    ToUserName = adminUserName,
                    Flags = 1,
                    MessageType = "0",
                    Datetime = DateTime.Now.ToString(),
                    DatetimeFa = PersianDateConvertor.ToKhorshidiDateTime(DateTime.Now, false),
                });
                return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, "", msg, "", ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
            }
            return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "خطای داخلی سیستم، بعدا سعی نمایید", "", "", ActionResponseFromServerToClient.EmptyObjectServerToClientKey));

        }


        [HttpPost]
        public ActionResult CheckUserNameStatus(string checkUserName, string userName)
        {
            var checkUser = MembershipManager.Instance.GetUser(checkUserName);
            if (checkUser == null)
                return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "چنین کاربری وجود ندارد", "", "", ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
            var userStatus = checkUser.MembershipUserStatus.FirstOrDefault();
            return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, "", userStatus.MembershipStatu.StatusModel, "", ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
        }




        [HttpPost]
        public ActionResult SuspendUserNameStatus(string checkUserName, string userName)
        {
            var checkUser = MembershipManager.Instance.GetUser(checkUserName);
            if (checkUser == null)
                return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "چنین کاربری وجود ندارد", "", "", ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
            var editStatus = MembershipManager.Instance.ChangeStatus(checkUser, MembershipStatus.InActive);
            if (editStatus == Core.EnumTypes.EditStatus.Edited || editStatus == Core.EnumTypes.EditStatus.RejectedByNonEffectiveQuery)
                return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, "", "غیر فعال گردید", "", ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
            return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "خطای داخلی سیستم، بعدا سعی نمایید", "", "", ActionResponseFromServerToClient.EmptyObjectServerToClientKey));

        }

        [HttpPost]
        public ActionResult ToggleUserStatus(Guid userId, string userName)
        {
            var checkUser = MembershipManager.Instance.GetUser(userId);
            if (checkUser == null)
                return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "چنین کاربری وجود ندارد", "", "", ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
            EditStatus editStatus;
            if(checkUser.MembershipUserStatus.FirstOrDefault().StatusId==(short)MembershipStatus.Active)
                editStatus = MembershipManager.Instance.ChangeStatus(checkUser, MembershipStatus.InActive);
            else
                editStatus = MembershipManager.Instance.ChangeStatus(checkUser, MembershipStatus.Active);
            if (editStatus == Core.EnumTypes.EditStatus.Edited || editStatus == Core.EnumTypes.EditStatus.RejectedByNonEffectiveQuery)
                return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, "", "تغییر وضعیت فعال گردید", "", ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
            return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "خطای داخلی سیستم، بعدا سعی نمایید", "", "", ActionResponseFromServerToClient.EmptyObjectServerToClientKey));

        }

        [HttpPost]
        public ActionResult ActiveUserNameStatus(string checkUserName, string userName)
        {
            var checkUser = MembershipManager.Instance.GetUser(checkUserName);
            if (checkUser == null)
                return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "چنین کاربری وجود ندارد", "", "", ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
            var editStatus = MembershipManager.Instance.ChangeStatus(checkUser, MembershipStatus.Active);
            if (editStatus == Core.EnumTypes.EditStatus.Edited || editStatus == Core.EnumTypes.EditStatus.RejectedByNonEffectiveQuery)
                return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, "", "فعال گردید", "", ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
            return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "خطای داخلی سیستم، بعدا سعی نمایید", "", "", ActionResponseFromServerToClient.EmptyObjectServerToClientKey));

        }

        public JsonResult LoadUsers(int? page, int? limit, string sortBy, string direction, string umpName, string umpUserName, string umpEmail, string umpMBTI, string umpDateFrom, string umpDateTo, string umpPhoneNumber, bool? umpSuspendedUser)
        {

            int take = limit.HasValue ? limit.Value : 20;
            int skip = 0;
            if (page.HasValue)
            {
                skip = (page.Value - 1) * limit.Value;
                //records = query.Skip(start).Take(limit.Value).ToList();
            }


            var users = MembershipManager.Instance.GetUsersByCriteria(umpName, umpUserName, umpEmail, umpMBTI, umpDateFrom, umpDateTo, umpPhoneNumber, umpSuspendedUser);
            int total = users.Count();
            if (!string.IsNullOrEmpty(sortBy) && !string.IsNullOrEmpty(direction))
            {
                if (direction.Trim() == "asc")
                {
                    switch (sortBy)
                    {
                        case "FirstName":
                            users = users.OrderBy(q => q.FirstName);
                            break;
                        case "LastName":
                            users = users.OrderBy(q => q.LastName);
                            break;
                        case "UserName":
                            users = users.OrderBy(q => q.UserName);
                            break;
                        case "RegisterDateTime":
                            users = users.OrderBy(q => q.RegisterDateTime);
                            break;
                        case "Email":
                            users = users.OrderBy(q => q.Email);
                            break;
                        case "MBTI":
                            users = users.OrderBy(q => q.ApplicationMBTIResults.Select(e => e.Result));
                            break;
                        case "PhoneNumber":
                            users = users.OrderBy(q => q.PhoneNumber);
                            break;
                    }
                }
                else
                {
                    switch (sortBy)
                    {
                        case "FirstName":
                            users = users.OrderByDescending(q => q.FirstName);
                            break;
                        case "LastName":
                            users = users.OrderByDescending(q => q.LastName);
                            break;
                        case "UserName":
                            users = users.OrderByDescending(q => q.UserName);
                            break;
                        case "RegisterDateTime":
                            users = users.OrderByDescending(q => q.RegisterDateTime);
                            break;
                        case "Email":
                            users = users.OrderByDescending(q => q.Email);
                            break;
                        case "MBTI":
                            users = users.OrderByDescending(q => q.ApplicationMBTIResults.Select(e => e.Result));
                            break;
                        case "PhoneNumber":
                            users = users.OrderByDescending(q => q.PhoneNumber);
                            break;
                    }
                }
            }

            var records = users.Skip(skip).Take(take).ToList().Select(e => new
            {
                UserId=e.UserId,
                FirstName = e.FirstName,
                LastName = e.LastName,
                UserName = e.UserName,
                RegisterDateTime = e.RegisterDateTime.HasValue ? e.RegisterDateTime.Value.ToString("yyyy/MM/dd HH:mm:ss") : string.Empty,
                Email = e.Email,
                MBTI = e.ApplicationMBTIResults.Select(m => m.Result),
                PhoneNumber = e.PhoneNumber,
                Status=e.MembershipUserStatus.FirstOrDefault().MembershipStatu.StatusModel,



            }).ToArray();
            // return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "search offers", offers.ToArray(), ActionResponseFromServerToClient.OffersServerToClientKey));
            return Json(new { records, total }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult SendMessageToUser(Guid receiverUserId)
        {
            return View(MembershipManager.Instance.GetUser(receiverUserId));
        }

        [HttpPost, Authorize]
        public ActionResult SendMessageToUser_JSON(string senderUserName, string receiverUserName, string messageBody, string methodType)
        {
            if (!string.IsNullOrEmpty(messageBody) && !string.IsNullOrEmpty(methodType) && !string.IsNullOrEmpty(senderUserName) && !string.IsNullOrEmpty(receiverUserName))
            {
                var receiverUser = MembershipManager.Instance.GetUser(receiverUserName);
                if(receiverUser==null)
                    return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Empty, "receiver is not valid", receiverUserName, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
                switch(methodType.ToUpper())
                {
                    case "SMS":
                        if (string.IsNullOrEmpty(receiverUser.PhoneNumber))
                            return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Empty, "receiver phone is not valid", receiverUserName, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
                        else
                        {
                            var response = SmsService.Instance.SendMagfaSms(receiverUser.PhoneNumber, messageBody);
                            return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "Successfully send SMS message", receiverUserName, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
                        }
                    case "EMAIL":
                        if (string.IsNullOrEmpty(receiverUser.Email))
                            return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Empty, "receiver email is not valid", receiverUserName, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
                        else
                        {
                            SMTPService mailService=new SMTPService();

                             mailService.SendMail(receiverUser.Email, "Offtick", messageBody);
                             return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "Successfully send Email message", receiverUserName, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
                        }
                    case "DOMAINMESSAGE":
                         ChatService chatService = new ChatService();
                         var chatServiceResponse = chatService.SRpcSendMessage(new SendMessageNotification()
                         {
                             FromUserName = senderUserName,
                             Message = messageBody,
                             ToUserName =receiverUserName,
                             Flags = 1,
                             MessageType = "0",
                             Datetime=DateTime.Now.ToString(),
                             DatetimeFa = PersianDateConvertor.ToKhorshidiDateTime(DateTime.Now, false),
                         });
                         return Json(chatServiceResponse);
                }
                    
                    AddStatus actionStatus = AddStatus.Added ;//= OfferManager.Instance.ReportOffer(offerEntity, userEntity, reportBody);

                    if (actionStatus == AddStatus.Added)
                        return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "Report offer was successful", receiverUserName, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
                    else
                        return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Empty, "fail to report offer", string.Empty, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));

                
            }
            return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Empty, "Invalid Parameters", string.Empty, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
        }

        #endregion

        #region SiteManagement

        [HttpPost]
        public ActionResult ToggleProfileStatus(Guid userId)
        {
            var checkUser = MembershipManager.Instance.GetUser(userId);
            if (checkUser == null)
                return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "چنین کاربری وجود ندارد", "", "", ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
            EditStatus editStatus;
            if (checkUser.ProfileSetting != null && checkUser.ProfileSetting.ProfileConfirmStatusId == (int)ConfirmStatus.Confirmed)
                editStatus = MembershipManager.Instance.ChangeProfileStatus(checkUser, ConfirmStatus.NotConfirmed);
            else
                editStatus = MembershipManager.Instance.ChangeProfileStatus(checkUser, ConfirmStatus.Confirmed);
            if (editStatus == Core.EnumTypes.EditStatus.Edited || editStatus == Core.EnumTypes.EditStatus.RejectedByNonEffectiveQuery)
                return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, "", "تغییر وضعیت فعال گردید", "", ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
            return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "خطای داخلی سیستم، بعدا سعی نمایید", "", "", ActionResponseFromServerToClient.EmptyObjectServerToClientKey));

        }
        public ActionResult TogglePublishOnMainPageStatus(Guid userId)
        {
            var checkUser = MembershipManager.Instance.GetUser(userId);
            if (checkUser == null)
                return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "چنین کاربری وجود ندارد", "", "", ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
            EditStatus editStatus;
            if (checkUser.ProfileSetting != null && checkUser.ProfileSetting.PublishOnMainPageConfirmStatusId == (int)ConfirmStatus.Confirmed)
                editStatus = MembershipManager.Instance.ChangePublishOnMainPageStatus(checkUser, ConfirmStatus.NotConfirmed);
            else
                editStatus = MembershipManager.Instance.ChangePublishOnMainPageStatus(checkUser, ConfirmStatus.Confirmed);
            if (editStatus == Core.EnumTypes.EditStatus.Edited || editStatus == Core.EnumTypes.EditStatus.RejectedByNonEffectiveQuery)
                return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, "", "تغییر وضعیت فعال گردید", "", ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
            return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "خطای داخلی سیستم، بعدا سعی نمایید", "", "", ActionResponseFromServerToClient.EmptyObjectServerToClientKey));

        }


        public JsonResult LoadSites(int? page, int? limit, string sortBy, string direction, string uppName, string uppUserName, string uppEmail, string uppDateFrom, string uppDateTo, string uppPhoneNumber, bool? uppSuspendedSite, bool? uppPermitToDistributeOnMainSite)
        {

            int take = limit.HasValue ? limit.Value : 20;
            int skip = 0;
            if (page.HasValue)
                skip = (page.Value - 1) * limit.Value;


            var users = MembershipManager.Instance.GetUsersProfileByCriteria(uppName, uppUserName, uppEmail, uppDateFrom, uppDateTo, uppPhoneNumber, uppSuspendedSite, uppPermitToDistributeOnMainSite);
            int total = users.Count();
            if (!string.IsNullOrEmpty(sortBy) && !string.IsNullOrEmpty(direction))
            {
                if (direction.Trim() == "asc")
                {
                    switch (sortBy)
                    {
                        case "FirstName":
                            users = users.OrderBy(q => q.FirstName);
                            break;
                        case "LastName":
                            users = users.OrderBy(q => q.LastName);
                            break;
                        case "UserName":
                            users = users.OrderBy(q => q.UserName);
                            break;
                        case "RegisterDateTime":
                            users = users.OrderBy(q => q.RegisterDateTime);
                            break;
                        case "Email":
                            users = users.OrderBy(q => q.Email);
                            break;
                        case "MBTI":
                            users = users.OrderBy(q => q.ApplicationMBTIResults.Select(e => e.Result));
                            break;
                        case "PhoneNumber":
                            users = users.OrderBy(q => q.PhoneNumber);
                            break;
                    }
                }
                else
                {
                    switch (sortBy)
                    {
                        case "FirstName":
                            users = users.OrderByDescending(q => q.FirstName);
                            break;
                        case "LastName":
                            users = users.OrderByDescending(q => q.LastName);
                            break;
                        case "UserName":
                            users = users.OrderByDescending(q => q.UserName);
                            break;
                        case "RegisterDateTime":
                            users = users.OrderByDescending(q => q.RegisterDateTime);
                            break;
                        case "Email":
                            users = users.OrderByDescending(q => q.Email);
                            break;
                        case "MBTI":
                            users = users.OrderByDescending(q => q.ApplicationMBTIResults.Select(e => e.Result));
                            break;
                        case "PhoneNumber":
                            users = users.OrderByDescending(q => q.PhoneNumber);
                            break;
                    }
                }
            }

            var records = users.Skip(skip).Take(take).ToList().Select(e => new
            {
                UserId = e.UserId,
                FirstName = e.FirstName,
                LastName = e.LastName,
                UserName = e.UserName,
                RegisterDateTime = e.RegisterDateTime.HasValue ? e.RegisterDateTime.Value.ToString("yyyy/MM/dd HH:mm:ss") : string.Empty,
                Email = e.Email,
                MBTI = e.ApplicationMBTIResults.Select(m => m.Result),
                PhoneNumber = e.PhoneNumber,
                Status = e.MembershipUserStatus.FirstOrDefault().MembershipStatu.StatusModel,
                ProfileStatus = e.ProfileSetting!=null ? e.ProfileSetting.ConfirmStatu.Name: ConfirmStatus.NotDecide.ToString(),
                MainPagePublishStatus = e.ProfileSetting != null ? e.ProfileSetting.ConfirmStatu2.Name : ConfirmStatus.NotDecide.ToString(),



            }).ToArray();
            // return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "search offers", offers.ToArray(), ActionResponseFromServerToClient.OffersServerToClientKey));
            return Json(new { records, total }, JsonRequestBehavior.AllowGet);
        }


        #endregion

        #region OfferManagement
        [HttpPost]
        public ActionResult ToggleConfirmOffer(Guid offerId)
        {
            var offer = OfferManager.Instance.GetOffer(offerId);
            if (offer == null)
                return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "چنین سفارشی وجود ندارد", "", "", ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
            EditStatus editStatus;
            if (offer.ConfirmStatu.ConfirmStatusId != (int)ConfirmStatus.Confirmed)
                editStatus = OfferManager.Instance.ConfirmOffer(offer);
            else
                editStatus = OfferManager.Instance.NotConfirmOffer(offer);
            if (editStatus == Core.EnumTypes.EditStatus.Edited || editStatus == Core.EnumTypes.EditStatus.RejectedByNonEffectiveQuery)
                return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, "", "انجام گردید", "", ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
            return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "خطای داخلی سیستم، بعدا سعی نمایید", "", "", ActionResponseFromServerToClient.EmptyObjectServerToClientKey));

        }
        [HttpPost]
        public ActionResult ToggleSuspendOffer(Guid offerId)
        {
            var offer = OfferManager.Instance.GetOffer(offerId);
            if (offer == null )
                return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "چنین سفارشی وجود ندارد", "", "", ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
            var foodOffer = offer.OfferFoods.FirstOrDefault();
            if(foodOffer==null)
                return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "این آفر را نمی توان معلق/ازاد نمود", "", "", ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
             EditStatus editStatus ;
            if(foodOffer.OfferStatusId==(short)OfferExecutionStatus.Running)
                 editStatus = OfferManager.Instance.ChangeStatusForOffer(foodOffer, OfferExecutionStatus.Suspended);
            else if(foodOffer.OfferStatusId==(short) OfferExecutionStatus.Suspended)
                editStatus = OfferManager.Instance.ChangeStatusForOffer(foodOffer, OfferExecutionStatus.Running);
            else
                return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "این آفر وضعیت آن به اتمام رسیده است نمیتوان وضعیت آن را عوض کرد", "", "", ActionResponseFromServerToClient.EmptyObjectServerToClientKey));

            if (editStatus == Core.EnumTypes.EditStatus.Edited || editStatus == Core.EnumTypes.EditStatus.RejectedByNonEffectiveQuery)
                return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, "", "تایید گردید", "", ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
            return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "خطای داخلی سیستم، بعدا سعی نمایید", "", "", ActionResponseFromServerToClient.EmptyObjectServerToClientKey));

        }


        [HttpGet]
        public JsonResult LoadOffersForConfirm(int? page, int? limit, string sortBy, string direction, string searchStr, int offerType, int roleType, int confirmStatus, bool isOnlyReported)
        {
            Console.Write(offerType + ", " + roleType + ", " + confirmStatus + ", " + isOnlyReported);
            int take =limit.HasValue?limit.Value:20;
            int skip = 0;
             if (page.HasValue)
                {
                     skip = (page.Value - 1) * limit.Value;
                    //records = query.Skip(start).Take(limit.Value).ToList();
                }
               
             Nullable<EnumOfferType> offerTypeEnum=offerType==0? (EnumOfferType?)null: (EnumOfferType)offerType;
             Nullable<RoleType> roleTypeEnum=roleType==0? (RoleType?)null: (RoleType)roleType;
             Nullable<ConfirmStatus> offerStatusEnum = confirmStatus == 0 ? (ConfirmStatus?)null : (ConfirmStatus)confirmStatus;
             var offers=OfferManager.Instance.getOffersByCriteria(!string.IsNullOrEmpty(searchStr) ? searchStr.Split(' ') : null,  offerTypeEnum, roleTypeEnum, offerStatusEnum, isOnlyReported);
             int total = offers.Count();
             if (!string.IsNullOrEmpty(sortBy) && !string.IsNullOrEmpty(direction))
             {
                 if (direction.Trim() == "asc")
                 {
                     switch (sortBy)
                     {
                         case "Title":
                             offers = offers.OrderBy(q => q.Title);
                             break;
                         case "Owner":
                             offers = offers.OrderBy(q => q.MembershipUser.UserName);
                             break;
                         case "DateTime":
                             offers = offers.OrderBy(q => q.DateTime);
                             break;
                         case "ConfrimDateTime":
                             offers = offers.OrderBy(q => q.ConfirmDateTime);
                             break;
                         case "IsConfirm":
                             offers = offers.OrderBy(q => q.ConfirmStatusId);
                             break;
                         case "Status":
                             offers = offers.OrderBy(q => q.OfferFoods.Select(e=>e.OfferStatusId));
                             break;
                     }
                 }
                 else
                 {
                     switch (sortBy)
                     {
                         case "Title":
                             offers = offers.OrderByDescending(q => q.Title);
                             break;
                         case "Owner":
                             offers = offers.OrderByDescending(q => q.MembershipUser.UserName);
                             break;
                         case "DateTime":
                             offers = offers.OrderByDescending(q => q.DateTime);
                             break;
                         case "ConfrimDateTime":
                             offers = offers.OrderByDescending(q => q.ConfirmDateTime);
                             break;
                         case "IsConfirm":
                             offers = offers.OrderByDescending(q => q.ConfirmStatusId);
                             break;
                         case "Status":
                             offers = offers.OrderByDescending(q => q.OfferFoods.Select(e => e.OfferStatusId));
                             break;
                     }
                 }
             }

             var records = offers.Skip(skip).Take(take).ToList().Select(e => new { Body = e.Body,
                 OfferId = e.OfferId, 
                 Title = e.Title,
                 UserId=e.SenderUserId,
                 UserName=e.MembershipUser.UserName,
                 UserDisplayName=e.MembershipUser.FirstName,
                 DateTime =  e.DateTime.ToString("yyyy/MM/dd HH:mm:ss"),
                 ConfirmDateTime = e.ConfirmDateTime.HasValue? e.ConfirmDateTime.Value.ToString("yyyy/MM/dd HH:mm:ss"):string.Empty,
                 HasReport=e.OfferReports.FirstOrDefault()!=null,
                 IsConfirm=e.ConfirmStatu.Name,
                 Status=e.OfferFoods.FirstOrDefault()!=null?e.OfferFoods.FirstOrDefault().OfferStatu.Name:string.Empty,
             }).ToArray();
            // return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "search offers", offers.ToArray(), ActionResponseFromServerToClient.OffersServerToClientKey));
             return Json(new { records, total }, JsonRequestBehavior.AllowGet);
        }

        #endregion

    }
}
