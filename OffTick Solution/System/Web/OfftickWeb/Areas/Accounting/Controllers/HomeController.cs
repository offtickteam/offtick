﻿using Offtick.Business.Membership;
using Offtick.Business.Services;
using Offtick.Business.Services.Domain;
using Offtick.Business.Services.Managers;
using Offtick.Business.Services.RealtimeSearvices;
using Offtick.Business.Services.RealtimeSearvices.EventDrivenDispatcher.Notification;
using Offtick.Business.Services.Storage;
using Offtick.Business.Web;
using Offtick.Core.EnumTypes;
using Offtick.Core.Utility.PersianTools;
using Offtick.Data.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Offtick.Web.OfftickWeb.Areas.Accounting.Controllers
{
    public class HomeController :  BaseController
    {

        public HomeController(DataContextContainer obj)
            : base(obj)
        {

        }

        //
        // GET: /Accounting/Home/

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoadPanel(string panel, string userName)
        {
            return View(panel);
        }

        [HttpPost]
        public ActionResult CountryDetailedPanel()
        {
            return View();
        }
        [HttpPost]
        public ActionResult CityDetailedPanel()
        {
            return View();
        }
        [HttpPost]
        public ActionResult StateDetailedPanel()
        {
            return View();
        }
        [HttpPost]
        public ActionResult UserDetailedPanel()
        {
            return View();
        }
        [HttpPost]
        public ActionResult OfferPanel()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SalesPanel()
        {
            return View();
        }
        
        [HttpPost]
        public ActionResult BuysPanel()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AccountsPanel()
        {
            return View();
        }
        [HttpPost]
        public ActionResult VoucherPanel()
        {
            return View();
        }

     

        #region Detailed

        public JsonResult LoadCountryForDetailedCode(int? page, int? limit, string sortBy, string direction, string cdpCountryName,bool cdpOnlyEmptydetailedCode)
        {

            int take = limit.HasValue ? limit.Value : 20;
            int skip = 0;
            if (page.HasValue)
            {
                skip = (page.Value - 1) * limit.Value;
                //records = query.Skip(start).Take(limit.Value).ToList();
            }


            var coutries = MembershipManager.Instance.GetCountries(cdpCountryName, cdpOnlyEmptydetailedCode);
            int total = coutries.Count();
            if (!string.IsNullOrEmpty(sortBy) && !string.IsNullOrEmpty(direction))
            {
                if (direction.Trim() == "asc")
                {
                    switch (sortBy)
                    {
                        case "CountryName":
                            coutries = coutries.OrderBy(q => q.CountryName);
                            break;
                        case "TellCode":
                            coutries = coutries.OrderBy(q => q.TellCode);
                            break;
                        case "DetailedCode":
                            coutries = coutries.OrderBy(q => q.DetailedCode);
                            break;
                      
                    }
                }
                else
                {
                    switch (sortBy)
                    {
                        case "CountryName":
                            coutries = coutries.OrderByDescending(q => q.CountryName);
                            break;
                        case "TellCode":
                            coutries = coutries.OrderByDescending(q => q.TellCode);
                            break;
                        case "DetailedCode":
                            coutries = coutries.OrderByDescending(q => q.DetailedCode);
                            break;
                    }
                }
            }

            var records = coutries.Skip(skip).Take(take).ToList().Select(e => new
            {
                CountryId = e.CountryId,
                CountryName = e.CountryName,
                TellCode = e.TellCode,
                DetailedCode = e.DetailedCode,

            }).ToArray();
            // return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "search offers", offers.ToArray(), ActionResponseFromServerToClient.OffersServerToClientKey));
            return Json(new { records, total }, JsonRequestBehavior.AllowGet);
        }
        public FileContentResult ExportToExcellCountryForDetailedCode( string cdpCountryName, bool cdpOnlyEmptydetailedCode)
        {
            var countries = MembershipManager.Instance.GetCountries(cdpCountryName, cdpOnlyEmptydetailedCode);
            List<CountryDTO> ls = new List<CountryDTO>();
            foreach (var country in countries)
            {
                ls.Add(new CountryDTO()
                {
                    DetailedCode = country.DetailedCode.HasValue ? country.DetailedCode.Value.ToString() : "",
                    Name = country.CountryName,
                });
            }
            return ExcelService.ExportToExcel<CountryDTO>(ls.ToList(), "countries.xls");
        }
         
        public JsonResult LoadCityForDetailedCode(int? page, int? limit, string sortBy, string direction, string cityName, bool onlyEmptydetailedCode)
        {

            int take = limit.HasValue ? limit.Value : 20;
            int skip = 0;
            if (page.HasValue)
            {
                skip = (page.Value - 1) * limit.Value;
                //records = query.Skip(start).Take(limit.Value).ToList();
            }


            var cities = MembershipManager.Instance.GetCities(cityName, onlyEmptydetailedCode);
            int total = cities.Count();
            if (!string.IsNullOrEmpty(sortBy) && !string.IsNullOrEmpty(direction))
            {
                if (direction.Trim() == "asc")
                {
                    switch (sortBy)
                    {
                        case "CityName":
                            cities = cities.OrderBy(q => q.CityName);
                            break;
                        case "StateName":
                            cities = cities.OrderBy(q => q.MembershipState.StateName);
                            break;
                        case "DetailedCode":
                            cities = cities.OrderBy(q => q.DetailedCode);
                            break;
                      
                    }
                }
                else
                {
                    switch (sortBy)
                    {
                        case "CityName":
                            cities = cities.OrderByDescending(q => q.CityName);
                            break;
                        case "StateName":
                            cities = cities.OrderByDescending(q => q.MembershipState.StateName);
                            break;
                        case "DetailedCode":
                            cities = cities.OrderByDescending(q => q.DetailedCode);
                            break;
                    }
                }
            }

            var records = cities.Skip(skip).Take(take).ToList().Select(e => new
            {
                CityId = e.CityId,
                CityName = e.CityName,
                StateName = e.MembershipState.StateName,
                DetailedCode = e.DetailedCode,

            }).ToArray();
            // return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "search offers", offers.ToArray(), ActionResponseFromServerToClient.OffersServerToClientKey));
            return Json(new { records, total }, JsonRequestBehavior.AllowGet);
        }
        public FileContentResult ExportToExcellCityForDetailedCode(string cityName, bool onlyEmptydetailedCode)
        {
            var cities = MembershipManager.Instance.GetCities(cityName, onlyEmptydetailedCode);
            List<CityDTO> ls = new List<CityDTO>();
            foreach (var city in cities)
            {
                ls.Add(new CityDTO()
                {
                    DetailedCode = city.DetailedCode.HasValue ? city.DetailedCode.Value.ToString() : "",
                    Name = city.CityName,
                    StateDetailedCode=city.MembershipState.DetailedCode.HasValue ? city.MembershipState.DetailedCode.Value.ToString() : "",
                });
            }
            return ExcelService.ExportToExcel<CityDTO>(ls, "cities.xls");
          
        }
        
        public JsonResult LoadStateForDetailedCode(int? page, int? limit, string sortBy, string direction, string stateName, bool onlyEmptydetailedCode)
        {

            int take = limit.HasValue ? limit.Value : 20;
            int skip = 0;
            if (page.HasValue)
            {
                skip = (page.Value - 1) * limit.Value;
                //records = query.Skip(start).Take(limit.Value).ToList();
            }


            var cities = MembershipManager.Instance.GetStates(stateName, onlyEmptydetailedCode);
            int total = cities.Count();
            if (!string.IsNullOrEmpty(sortBy) && !string.IsNullOrEmpty(direction))
            {
                if (direction.Trim() == "asc")
                {
                    switch (sortBy)
                    {
                        case "CountryName":
                            cities = cities.OrderBy(q => q.MembershipCountry.CountryName);
                            break;
                        case "StateName":
                            cities = cities.OrderBy(q => q.StateName);
                            break;
                        case "DetailedCode":
                            cities = cities.OrderBy(q => q.DetailedCode);
                            break;

                    }
                }
                else
                {
                    switch (sortBy)
                    {
                        case "CountryName":
                            cities = cities.OrderByDescending(q => q.MembershipCountry.CountryName);
                            break;
                        case "StateName":
                            cities = cities.OrderByDescending(q => q.StateName);
                            break;
                        case "DetailedCode":
                            cities = cities.OrderByDescending(q => q.DetailedCode);
                            break;
                    }
                }
            }

            var records = cities.Skip(skip).Take(take).ToList().Select(e => new
            {
                StateId = e.StateId,
                StateName = e.StateName,
                CountryName = e.MembershipCountry.CountryName,
                DetailedCode = e.DetailedCode,

            }).ToArray();
            // return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "search offers", offers.ToArray(), ActionResponseFromServerToClient.OffersServerToClientKey));
            return Json(new { records, total }, JsonRequestBehavior.AllowGet);
        }

        public FileContentResult ExportToExcellStateForDetailedCode( string stateName, bool onlyEmptydetailedCode)
        {
            var states = MembershipManager.Instance.GetStates(stateName, onlyEmptydetailedCode);
            List<StateDTO> ls = new List<StateDTO>();
            foreach (var state in states)
            {
                ls.Add(new StateDTO()
                {
                    DetailedCode = state.DetailedCode.HasValue ? state.DetailedCode.Value.ToString() : "",
                    Name = state.StateName,
                    CountryDetailedCode = state.MembershipCountry.DetailedCode.HasValue ? state.MembershipCountry.DetailedCode.Value.ToString() : "",
                });
            }
            return ExcelService.ExportToExcel<StateDTO>(ls.ToList(), "states.xls");
        }

        public JsonResult LoadUserForDetailedCode(int? page, int? limit, string sortBy, string direction, string udpName, string udpUserName, string udpEmail, string udpMBTI, string udpPhoneNumber, bool? onlyEmptyDetailCode)
        {

            int take = limit.HasValue ? limit.Value : 20;
            int skip = 0;
            if (page.HasValue)
            {
                skip = (page.Value - 1) * limit.Value;
                //records = query.Skip(start).Take(limit.Value).ToList();
            }


            var users = MembershipManager.Instance.GetUsersByCriteria(udpName, udpUserName, udpEmail, udpMBTI,  udpPhoneNumber, onlyEmptyDetailCode);
            int total = users.Count();
            if (!string.IsNullOrEmpty(sortBy) && !string.IsNullOrEmpty(direction))
            {
                if (direction.Trim() == "asc")
                {
                    switch (sortBy)
                    {
                        case "FirstName":
                            users = users.OrderBy(q => q.FirstName);
                            break;
                        case "LastName":
                            users = users.OrderBy(q => q.LastName);
                            break;
                        case "UserName":
                            users = users.OrderBy(q => q.UserName);
                            break;
                        case "RegisterDateTime":
                            users = users.OrderBy(q => q.RegisterDateTime);
                            break;
                        case "Email":
                            users = users.OrderBy(q => q.Email);
                            break;
                        case "MBTI":
                            users = users.OrderBy(q => q.ApplicationMBTIResults.Select(e => e.Result));
                            break;
                        case "PhoneNumber":
                            users = users.OrderBy(q => q.PhoneNumber);
                            break;
                        case "DetailedCode":
                            users = users.OrderBy(q => q.DetailedCode);
                            break;
                    }
                }
                else
                {
                    switch (sortBy)
                    {
                        case "FirstName":
                            users = users.OrderByDescending(q => q.FirstName);
                            break;
                        case "LastName":
                            users = users.OrderByDescending(q => q.LastName);
                            break;
                        case "UserName":
                            users = users.OrderByDescending(q => q.UserName);
                            break;
                        case "RegisterDateTime":
                            users = users.OrderByDescending(q => q.RegisterDateTime);
                            break;
                        case "Email":
                            users = users.OrderByDescending(q => q.Email);
                            break;
                        case "MBTI":
                            users = users.OrderByDescending(q => q.ApplicationMBTIResults.Select(e => e.Result));
                            break;
                        case "PhoneNumber":
                            users = users.OrderByDescending(q => q.PhoneNumber);
                            break;
                        case "DetailedCode":
                            users = users.OrderByDescending(q => q.DetailedCode);
                            break;
                    }
                }
            }

            var records = users.Skip(skip).Take(take).ToList().Select(e => new
            {
                UserId = e.UserId,
                FirstName = e.FirstName,
                LastName = e.LastName,
                UserName = e.UserName,
                RegisterDateTime = e.RegisterDateTime.HasValue ? e.RegisterDateTime.Value.ToString("yyyy/MM/dd HH:mm:ss") : string.Empty,
                Email = e.Email,
                MBTI = e.ApplicationMBTIResults.Select(m => m.Result),
                PhoneNumber = e.PhoneNumber,
                Status = e.MembershipUserStatus.FirstOrDefault().MembershipStatu.StatusModel,
                DetailedCode = e.DetailedCode

            }).ToArray();
            // return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "search offers", offers.ToArray(), ActionResponseFromServerToClient.OffersServerToClientKey));
            return Json(new { records, total }, JsonRequestBehavior.AllowGet);
        }

        public FileContentResult ExportToExcellUserForDetailedCode(string udpName, string udpUserName, string udpEmail, string udpMBTI, string udpPhoneNumber, bool? onlyEmptyDetailCode)
        {
            var users = MembershipManager.Instance.GetUsersByCriteria(udpName, udpUserName, udpEmail, udpMBTI, udpPhoneNumber, onlyEmptyDetailCode);
            List<UserExportDTO> ls = new List<UserExportDTO>();
            foreach (var user in users)
            {
                ls.Add(new UserExportDTO()
                {
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    UserName = user.UserName,
                    RegisterDateTime = user.RegisterDateTime.HasValue ? user.RegisterDateTime.Value.ToString("yyyy/MM/dd HH:mm:ss") : string.Empty,
                    Email = user.Email,
                    PhoneNumber = user.PhoneNumber,
                    Status = user.MembershipUserStatus.FirstOrDefault().MembershipStatu.StatusModel,
                    DetailedCode = user.DetailedCode.HasValue?user.DetailedCode.Value.ToString():"",
                });
            }

            //var records = users.Skip(skip).Take(take).ToList().Select(e => new
            //{
            //    UserId = e.UserId,
            //    FirstName = e.FirstName,
            //    LastName = e.LastName,
            //    UserName = e.UserName,
            //    RegisterDateTime = e.RegisterDateTime.HasValue ? e.RegisterDateTime.Value.ToString("yyyy/MM/dd HH:mm:ss") : string.Empty,
            //    Email = e.Email,
            //    MBTI = e.ApplicationMBTIResults.Select(m => m.Result),
            //    PhoneNumber = e.PhoneNumber,
            //    Status = e.MembershipUserStatus.FirstOrDefault().MembershipStatu.StatusModel,
            //    DetailedCode = e.DetailedCode

            //}).ToArray();
            return ExcelService.ExportToExcel<UserExportDTO>(ls.ToList(), "users.xls");

            
        }
       
        [HttpGet]
        public JsonResult LoadOffersForDetailedCode(int? page, int? limit, string sortBy, string direction, string searchStr, int offerType, int roleType, int confirmStatus,bool chkOnlyReported, bool onlyEmptyDetailCode)
        {
            Console.Write(offerType + ", " + roleType + ", " + confirmStatus + ", " + onlyEmptyDetailCode);
            int take =limit.HasValue?limit.Value:20;
            int skip = 0;
             if (page.HasValue)
                {
                     skip = (page.Value - 1) * limit.Value;
                    //records = query.Skip(start).Take(limit.Value).ToList();
                }
               
             Nullable<EnumOfferType> offerTypeEnum=offerType==0? (EnumOfferType?)null: (EnumOfferType)offerType;
             Nullable<RoleType> roleTypeEnum=roleType==0? (RoleType?)null: (RoleType)roleType;
             Nullable<ConfirmStatus> offerStatusEnum = confirmStatus == 0 ? (ConfirmStatus?)null : (ConfirmStatus)confirmStatus;
             var offers = OfferManager.Instance.getOffersByCriteria(!string.IsNullOrEmpty(searchStr) ? searchStr.Split(' ') : null, offerTypeEnum, roleTypeEnum, offerStatusEnum, onlyEmptyDetailCode);
             int total = offers.Count();
             if (!string.IsNullOrEmpty(sortBy) && !string.IsNullOrEmpty(direction))
             {
                 if (direction.Trim() == "asc")
                 {
                     switch (sortBy)
                     {
                         case "Title":
                             offers = offers.OrderBy(q => q.Title);
                             break;
                         case "Owner":
                             offers = offers.OrderBy(q => q.MembershipUser.UserName);
                             break;
                         case "DateTime":
                             offers = offers.OrderBy(q => q.DateTime);
                             break;
                         case "ConfrimDateTime":
                             offers = offers.OrderBy(q => q.ConfirmDateTime);
                             break;
                         case "IsConfirm":
                             offers = offers.OrderBy(q => q.ConfirmStatusId);
                             break;
                         case "Status":
                             offers = offers.OrderBy(q => q.OfferFoods.Select(e=>e.OfferStatusId));
                             break;
                         case "DetailedCode":
                             offers = offers.OrderBy(q => q.DetailedCode);
                             break;
                     }
                 }
                 else
                 {
                     switch (sortBy)
                     {
                         case "Title":
                             offers = offers.OrderByDescending(q => q.Title);
                             break;
                         case "Owner":
                             offers = offers.OrderByDescending(q => q.MembershipUser.UserName);
                             break;
                         case "DateTime":
                             offers = offers.OrderByDescending(q => q.DateTime);
                             break;
                         case "ConfrimDateTime":
                             offers = offers.OrderByDescending(q => q.ConfirmDateTime);
                             break;
                         case "IsConfirm":
                             offers = offers.OrderByDescending(q => q.ConfirmStatusId);
                             break;
                         case "Status":
                             offers = offers.OrderByDescending(q => q.OfferFoods.Select(e => e.OfferStatusId));
                             break;
                         case "DetailedCode":
                             offers = offers.OrderByDescending(q => q.DetailedCode);
                             break;
                     }
                 }
             }

             var records = offers.Skip(skip).Take(take).ToList().Select(e => new { Body = e.Body,
                 OfferId = e.OfferId, 
                 Title = e.Title,
                 UserId=e.SenderUserId,
                 UserName=e.MembershipUser.UserName,
                 UserDisplayName=e.MembershipUser.FirstName,
                 DateTime =  e.DateTime.ToString("yyyy/MM/dd HH:mm:ss"),
                 ConfirmDateTime = e.ConfirmDateTime.HasValue? e.ConfirmDateTime.Value.ToString("yyyy/MM/dd HH:mm:ss"):string.Empty,
                 HasReport=e.OfferReports.FirstOrDefault()!=null,
                 IsConfirm=e.ConfirmStatu.Name,
                 Status=e.OfferFoods.FirstOrDefault()!=null?e.OfferFoods.FirstOrDefault().OfferStatu.Name:string.Empty,
                 DetailedCode = e.DetailedCode
             }).ToArray();
            // return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "search offers", offers.ToArray(), ActionResponseFromServerToClient.OffersServerToClientKey));
             return Json(new { records, total }, JsonRequestBehavior.AllowGet);
        }
        public FileContentResult ExportToExcellOffersForDetailedCode( string searchStr, int offerType, int roleType, int confirmStatus, bool chkOnlyReported, bool onlyEmptyDetailCode)
        {
            
            Nullable<EnumOfferType> offerTypeEnum = offerType == 0 ? (EnumOfferType?)null : (EnumOfferType)offerType;
            Nullable<RoleType> roleTypeEnum = roleType == 0 ? (RoleType?)null : (RoleType)roleType;
            Nullable<ConfirmStatus> offerStatusEnum = confirmStatus == 0 ? (ConfirmStatus?)null : (ConfirmStatus)confirmStatus;
            var offers = OfferManager.Instance.getOffersByCriteria(!string.IsNullOrEmpty(searchStr) ? searchStr.Split(' ') : null, offerTypeEnum, roleTypeEnum, offerStatusEnum, onlyEmptyDetailCode);
            List<OfferExportDTO> ls = new List<OfferExportDTO>();

            foreach (var offer in offers)
            {
                ls.Add(new OfferExportDTO()
                {
                    Body = offer.Body,
                     UserDetailedCode=offer.MembershipUser.DetailedCode.HasValue?offer.MembershipUser.DetailedCode.Value.ToString():"",
                    Title = offer.Title,
                    UserName = offer.MembershipUser.UserName,
                    UserDisplayName = offer.MembershipUser.FirstName,
                    DateTime = offer.DateTime.ToString("yyyy/MM/dd HH:mm:ss"),
                    ConfirmDateTime = offer.ConfirmDateTime.HasValue ? offer.ConfirmDateTime.Value.ToString("yyyy/MM/dd HH:mm:ss") : string.Empty,
                    IsConfirm = offer.ConfirmStatu.Name,
                    Status = offer.OfferFoods.FirstOrDefault() != null ? offer.OfferFoods.FirstOrDefault().OfferStatu.Name : string.Empty,
                    DetailedCode = offer.DetailedCode.HasValue?offer.DetailedCode.Value.ToString():"",
                });
            }

            //var records = offers.Skip(skip).Take(take).ToList().Select(e => new
            //{
            //    Body = e.Body,
            //    OfferId = e.OfferId,
            //    Title = e.Title,
            //    UserId = e.SenderUserId,
            //    UserName = e.MembershipUser.UserName,
            //    UserDisplayName = e.MembershipUser.FirstName,
            //    DateTime = e.DateTime.ToString("yyyy/MM/dd HH:mm:ss"),
            //    ConfirmDateTime = e.ConfirmDateTime.HasValue ? e.ConfirmDateTime.Value.ToString("yyyy/MM/dd HH:mm:ss") : string.Empty,
            //    HasReport = e.OfferReports.FirstOrDefault() != null,
            //    IsConfirm = e.ConfirmStatu.Name,
            //    Status = e.OfferFoods.FirstOrDefault() != null ? e.OfferFoods.FirstOrDefault().OfferStatu.Name : string.Empty,
            //    DetailedCode = e.DetailedCode
            //}).ToArray();
            // return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "search offers", offers.ToArray(), ActionResponseFromServerToClient.OffersServerToClientKey));
            return ExcelService.ExportToExcel<OfferExportDTO>(ls.ToList(), "offers.xls");
        }

        
        public ActionResult ChangeDetailedCode(Guid entityId, string entityType)
        {
            object obj = null;
            switch (entityType)
            {
                case "MembershipCountry":
                    obj=MembershipManager.Instance.getCountryById(entityId);
                    break;
                case "MembershipCity":
                    obj = MembershipManager.Instance.getCityById(entityId);
                    break;
                case "MembershipState":
                    obj = MembershipManager.Instance.getStateById(entityId);
                    break;
                case "MembershipUser":
                    obj = MembershipManager.Instance.GetUser(entityId);
                    break;
                case "Offer":
                    obj = OfferManager.Instance.GetOffer(entityId);
                    break;
            }
            return View(obj);
        }

        [HttpPost, Authorize]
        public ActionResult ChangeDetailedCode_JSON(Guid entityId, string entityType, string entityDetailedCode)
        {
            if (entityId != null && !string.IsNullOrEmpty(entityType) && !string.IsNullOrEmpty(entityDetailedCode))
            {
                int intEntityDetailedCode;
                EditStatus actionStatus=EditStatus.RejectedByAccessDenied;
                if (int.TryParse(entityDetailedCode, out intEntityDetailedCode))
                {
                    switch (entityType)
                    {
                        case "MembershipCountry":
                            actionStatus = MembershipManager.Instance.updateCountryDetailedCode(entityId, intEntityDetailedCode);
                            break;
                        case "MembershipCity":
                            actionStatus = MembershipManager.Instance.updatecityDetailedCode(entityId, intEntityDetailedCode);
                            break;
                        case "MembershipState":
                            actionStatus = MembershipManager.Instance.updateStateDetailedCode(entityId, intEntityDetailedCode);
                            break;
                        case "MembershipUser":
                            actionStatus = MembershipManager.Instance.updateUserDetailedCode(entityId, intEntityDetailedCode);
                            break;
                        case "Offer":
                            actionStatus = OfferManager.Instance.UpdateOfferDetailedCode(entityId, intEntityDetailedCode);
                            break;
                    }


                    if (actionStatus == EditStatus.Edited)
                        return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "Edit DetailedCode  was successful", string.Empty, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
                    else
                        return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "fail to Edit DetailedCode", "fail to Edit DetailedCode", string.Empty, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
                }
            }

            return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "Invalid Parameters", "Invalid Parameters", string.Empty, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
        }

        #endregion

        #region UserManagement


        [HttpPost]
        public ActionResult CheckUserNameStatus(string checkUserName, string userName)
        {
            var checkUser = MembershipManager.Instance.GetUser(checkUserName);
            if (checkUser == null)
                return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "چنین کاربری وجود ندارد", "", "", ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
            var userStatus = checkUser.MembershipUserStatus.FirstOrDefault();
            return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, "", userStatus.MembershipStatu.StatusModel, "", ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
        }




        [HttpPost]
        public ActionResult SuspendUserNameStatus(string checkUserName, string userName)
        {
            var checkUser = MembershipManager.Instance.GetUser(checkUserName);
            if (checkUser == null)
                return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "چنین کاربری وجود ندارد", "", "", ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
            var editStatus = MembershipManager.Instance.ChangeStatus(checkUser, MembershipStatus.InActive);
            if (editStatus == Core.EnumTypes.EditStatus.Edited || editStatus == Core.EnumTypes.EditStatus.RejectedByNonEffectiveQuery)
                return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, "", "غیر فعال گردید", "", ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
            return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "خطای داخلی سیستم، بعدا سعی نمایید", "", "", ActionResponseFromServerToClient.EmptyObjectServerToClientKey));

        }

        [HttpPost]
        public ActionResult ToggleUserStatus(Guid userId, string userName)
        {
            var checkUser = MembershipManager.Instance.GetUser(userId);
            if (checkUser == null)
                return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "چنین کاربری وجود ندارد", "", "", ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
            EditStatus editStatus;
            if (checkUser.MembershipUserStatus.FirstOrDefault().StatusId == (short)MembershipStatus.Active)
                editStatus = MembershipManager.Instance.ChangeStatus(checkUser, MembershipStatus.InActive);
            else
                editStatus = MembershipManager.Instance.ChangeStatus(checkUser, MembershipStatus.Active);
            if (editStatus == Core.EnumTypes.EditStatus.Edited || editStatus == Core.EnumTypes.EditStatus.RejectedByNonEffectiveQuery)
                return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, "", "تغییر وضعیت فعال گردید", "", ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
            return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "خطای داخلی سیستم، بعدا سعی نمایید", "", "", ActionResponseFromServerToClient.EmptyObjectServerToClientKey));

        }

        [HttpPost]
        public ActionResult ActiveUserNameStatus(string checkUserName, string userName)
        {
            var checkUser = MembershipManager.Instance.GetUser(checkUserName);
            if (checkUser == null)
                return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "چنین کاربری وجود ندارد", "", "", ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
            var editStatus = MembershipManager.Instance.ChangeStatus(checkUser, MembershipStatus.Active);
            if (editStatus == Core.EnumTypes.EditStatus.Edited || editStatus == Core.EnumTypes.EditStatus.RejectedByNonEffectiveQuery)
                return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, "", "فعال گردید", "", ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
            return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "خطای داخلی سیستم، بعدا سعی نمایید", "", "", ActionResponseFromServerToClient.EmptyObjectServerToClientKey));

        }

        public JsonResult LoadUsers(int? page, int? limit, string sortBy, string direction, string umpName, string umpUserName, string umpEmail, string umpMBTI, string umpDateFrom, string umpDateTo, string umpPhoneNumber, bool? umpSuspendedUser)
        {

            int take = limit.HasValue ? limit.Value : 20;
            int skip = 0;
            if (page.HasValue)
            {
                skip = (page.Value - 1) * limit.Value;
                //records = query.Skip(start).Take(limit.Value).ToList();
            }


            var users = MembershipManager.Instance.GetUsersByCriteria(umpName, umpUserName, umpEmail, umpMBTI, umpDateFrom, umpDateTo, umpPhoneNumber, umpSuspendedUser);
            int total = users.Count();
            if (!string.IsNullOrEmpty(sortBy) && !string.IsNullOrEmpty(direction))
            {
                if (direction.Trim() == "asc")
                {
                    switch (sortBy)
                    {
                        case "FirstName":
                            users = users.OrderBy(q => q.FirstName);
                            break;
                        case "LastName":
                            users = users.OrderBy(q => q.LastName);
                            break;
                        case "UserName":
                            users = users.OrderBy(q => q.UserName);
                            break;
                        case "RegisterDateTime":
                            users = users.OrderBy(q => q.RegisterDateTime);
                            break;
                        case "Email":
                            users = users.OrderBy(q => q.Email);
                            break;
                        case "MBTI":
                            users = users.OrderBy(q => q.ApplicationMBTIResults.Select(e => e.Result));
                            break;
                        case "PhoneNumber":
                            users = users.OrderBy(q => q.PhoneNumber);
                            break;
                    }
                }
                else
                {
                    switch (sortBy)
                    {
                        case "FirstName":
                            users = users.OrderByDescending(q => q.FirstName);
                            break;
                        case "LastName":
                            users = users.OrderByDescending(q => q.LastName);
                            break;
                        case "UserName":
                            users = users.OrderByDescending(q => q.UserName);
                            break;
                        case "RegisterDateTime":
                            users = users.OrderByDescending(q => q.RegisterDateTime);
                            break;
                        case "Email":
                            users = users.OrderByDescending(q => q.Email);
                            break;
                        case "MBTI":
                            users = users.OrderByDescending(q => q.ApplicationMBTIResults.Select(e => e.Result));
                            break;
                        case "PhoneNumber":
                            users = users.OrderByDescending(q => q.PhoneNumber);
                            break;
                    }
                }
            }

            var records = users.Skip(skip).Take(take).ToList().Select(e => new
            {
                UserId = e.UserId,
                FirstName = e.FirstName,
                LastName = e.LastName,
                UserName = e.UserName,
                RegisterDateTime = e.RegisterDateTime.HasValue ? e.RegisterDateTime.Value.ToString("yyyy/MM/dd HH:mm:ss") : string.Empty,
                Email = e.Email,
                MBTI = e.ApplicationMBTIResults.Select(m => m.Result),
                PhoneNumber = e.PhoneNumber,
                Status = e.MembershipUserStatus.FirstOrDefault().MembershipStatu.StatusModel,



            }).ToArray();
            // return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "search offers", offers.ToArray(), ActionResponseFromServerToClient.OffersServerToClientKey));
            return Json(new { records, total }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult SendMessageToUser(Guid receiverUserId)
        {
            return View(MembershipManager.Instance.GetUser(receiverUserId));
        }

        [HttpPost, Authorize]
        public ActionResult SendMessageToUser_JSON(string senderUserName, string receiverUserName, string messageBody, string methodType)
        {
            if (!string.IsNullOrEmpty(messageBody) && !string.IsNullOrEmpty(methodType) && !string.IsNullOrEmpty(senderUserName) && !string.IsNullOrEmpty(receiverUserName))
            {
                var receiverUser = MembershipManager.Instance.GetUser(receiverUserName);
                if (receiverUser == null)
                    return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Empty, "receiver is not valid", receiverUserName, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
                switch (methodType.ToUpper())
                {
                    case "SMS":
                        if (string.IsNullOrEmpty(receiverUser.PhoneNumber))
                            return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Empty, "receiver phone is not valid", receiverUserName, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
                        else
                        {
                            var response = SmsService.Instance.SendMagfaSms(receiverUser.PhoneNumber, messageBody);
                            return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "Successfully send SMS message", receiverUserName, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
                        }
                    case "EMAIL":
                        if (string.IsNullOrEmpty(receiverUser.Email))
                            return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Empty, "receiver email is not valid", receiverUserName, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
                        else
                        {
                            SMTPService mailService = new SMTPService();

                            mailService.SendMail(receiverUser.Email, "Offtick", messageBody);
                            return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "Successfully send Email message", receiverUserName, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
                        }
                    case "DOMAINMESSAGE":
                        ChatService chatService = new ChatService();
                        var chatServiceResponse = chatService.SRpcSendMessage(new SendMessageNotification()
                        {
                            FromUserName = senderUserName,
                            Message = messageBody,
                            ToUserName = receiverUserName,
                            Flags = 1,
                            MessageType = "0",
                            Datetime = DateTime.Now.ToString(),
                            DatetimeFa = PersianDateConvertor.ToKhorshidiDateTime(DateTime.Now,false),
                        });
                        return Json(chatServiceResponse);
                }

                AddStatus actionStatus = AddStatus.Added;//= OfferManager.Instance.ReportOffer(offerEntity, userEntity, reportBody);

                if (actionStatus == AddStatus.Added)
                    return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "Report offer was successful", receiverUserName, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
                else
                    return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Empty, "fail to report offer", string.Empty, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));


            }
            return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Empty, "Invalid Parameters", string.Empty, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
        }

        #endregion

        #region SiteManagement

        [HttpPost]
        public ActionResult ToggleProfileStatus(Guid userId)
        {
            var checkUser = MembershipManager.Instance.GetUser(userId);
            if (checkUser == null)
                return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "چنین کاربری وجود ندارد", "", "", ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
            EditStatus editStatus;
            if (checkUser.ProfileSetting != null && checkUser.ProfileSetting.ProfileConfirmStatusId == (int)ConfirmStatus.Confirmed)
                editStatus = MembershipManager.Instance.ChangeProfileStatus(checkUser, ConfirmStatus.NotConfirmed);
            else
                editStatus = MembershipManager.Instance.ChangeProfileStatus(checkUser, ConfirmStatus.Confirmed);
            if (editStatus == Core.EnumTypes.EditStatus.Edited || editStatus == Core.EnumTypes.EditStatus.RejectedByNonEffectiveQuery)
                return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, "", "تغییر وضعیت فعال گردید", "", ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
            return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "خطای داخلی سیستم، بعدا سعی نمایید", "", "", ActionResponseFromServerToClient.EmptyObjectServerToClientKey));

        }
        public ActionResult TogglePublishOnMainPageStatus(Guid userId)
        {
            var checkUser = MembershipManager.Instance.GetUser(userId);
            if (checkUser == null)
                return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "چنین کاربری وجود ندارد", "", "", ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
            EditStatus editStatus;
            if (checkUser.ProfileSetting != null && checkUser.ProfileSetting.PublishOnMainPageConfirmStatusId == (int)ConfirmStatus.Confirmed)
                editStatus = MembershipManager.Instance.ChangePublishOnMainPageStatus(checkUser, ConfirmStatus.NotConfirmed);
            else
                editStatus = MembershipManager.Instance.ChangePublishOnMainPageStatus(checkUser, ConfirmStatus.Confirmed);
            if (editStatus == Core.EnumTypes.EditStatus.Edited || editStatus == Core.EnumTypes.EditStatus.RejectedByNonEffectiveQuery)
                return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, "", "تغییر وضعیت فعال گردید", "", ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
            return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "خطای داخلی سیستم، بعدا سعی نمایید", "", "", ActionResponseFromServerToClient.EmptyObjectServerToClientKey));

        }


        public JsonResult LoadSites(int? page, int? limit, string sortBy, string direction, string uppName, string uppUserName, string uppEmail, string uppDateFrom, string uppDateTo, string uppPhoneNumber, bool? uppSuspendedSite, bool? uppPermitToDistributeOnMainSite)
        {

            int take = limit.HasValue ? limit.Value : 20;
            int skip = 0;
            if (page.HasValue)
                skip = (page.Value - 1) * limit.Value;


            var users = MembershipManager.Instance.GetUsersProfileByCriteria(uppName, uppUserName, uppEmail, uppDateFrom, uppDateTo, uppPhoneNumber, uppSuspendedSite, uppPermitToDistributeOnMainSite);
            int total = users.Count();
            if (!string.IsNullOrEmpty(sortBy) && !string.IsNullOrEmpty(direction))
            {
                if (direction.Trim() == "asc")
                {
                    switch (sortBy)
                    {
                        case "FirstName":
                            users = users.OrderBy(q => q.FirstName);
                            break;
                        case "LastName":
                            users = users.OrderBy(q => q.LastName);
                            break;
                        case "UserName":
                            users = users.OrderBy(q => q.UserName);
                            break;
                        case "RegisterDateTime":
                            users = users.OrderBy(q => q.RegisterDateTime);
                            break;
                        case "Email":
                            users = users.OrderBy(q => q.Email);
                            break;
                        case "MBTI":
                            users = users.OrderBy(q => q.ApplicationMBTIResults.Select(e => e.Result));
                            break;
                        case "PhoneNumber":
                            users = users.OrderBy(q => q.PhoneNumber);
                            break;
                    }
                }
                else
                {
                    switch (sortBy)
                    {
                        case "FirstName":
                            users = users.OrderByDescending(q => q.FirstName);
                            break;
                        case "LastName":
                            users = users.OrderByDescending(q => q.LastName);
                            break;
                        case "UserName":
                            users = users.OrderByDescending(q => q.UserName);
                            break;
                        case "RegisterDateTime":
                            users = users.OrderByDescending(q => q.RegisterDateTime);
                            break;
                        case "Email":
                            users = users.OrderByDescending(q => q.Email);
                            break;
                        case "MBTI":
                            users = users.OrderByDescending(q => q.ApplicationMBTIResults.Select(e => e.Result));
                            break;
                        case "PhoneNumber":
                            users = users.OrderByDescending(q => q.PhoneNumber);
                            break;
                    }
                }
            }

            var records = users.Skip(skip).Take(take).ToList().Select(e => new
            {
                UserId = e.UserId,
                FirstName = e.FirstName,
                LastName = e.LastName,
                UserName = e.UserName,
                RegisterDateTime = e.RegisterDateTime.HasValue ? e.RegisterDateTime.Value.ToString("yyyy/MM/dd HH:mm:ss") : string.Empty,
                Email = e.Email,
                MBTI = e.ApplicationMBTIResults.Select(m => m.Result),
                PhoneNumber = e.PhoneNumber,
                Status = e.MembershipUserStatus.FirstOrDefault().MembershipStatu.StatusModel,
                ProfileStatus = e.ProfileSetting != null ? e.ProfileSetting.ConfirmStatu.Name : ConfirmStatus.NotDecide.ToString(),
                MainPagePublishStatus = e.ProfileSetting != null ? e.ProfileSetting.ConfirmStatu2.Name : ConfirmStatus.NotDecide.ToString(),



            }).ToArray();
            // return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "search offers", offers.ToArray(), ActionResponseFromServerToClient.OffersServerToClientKey));
            return Json(new { records, total }, JsonRequestBehavior.AllowGet);
        }


        #endregion

        #region OfferManagement
        [HttpPost]
        public ActionResult ToggleConfirmOffer(Guid offerId)
        {
            var offer = OfferManager.Instance.GetOffer(offerId);
            if (offer == null)
                return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "چنین سفارشی وجود ندارد", "", "", ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
            EditStatus editStatus;
            if (offer.ConfirmStatu.ConfirmStatusId != (int)ConfirmStatus.Confirmed)
                editStatus = OfferManager.Instance.ConfirmOffer(offer);
            else
                editStatus = OfferManager.Instance.NotConfirmOffer(offer);
            if (editStatus == Core.EnumTypes.EditStatus.Edited || editStatus == Core.EnumTypes.EditStatus.RejectedByNonEffectiveQuery)
                return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, "", "انجام گردید", "", ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
            return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "خطای داخلی سیستم، بعدا سعی نمایید", "", "", ActionResponseFromServerToClient.EmptyObjectServerToClientKey));

        }
        [HttpPost]
        public ActionResult ToggleSuspendOffer(Guid offerId)
        {
            var offer = OfferManager.Instance.GetOffer(offerId);
            if (offer == null)
                return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "چنین سفارشی وجود ندارد", "", "", ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
            var foodOffer = offer.OfferFoods.FirstOrDefault();
            if (foodOffer == null)
                return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "این آفر را نمی توان معلق/ازاد نمود", "", "", ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
            EditStatus editStatus;
            if (foodOffer.OfferStatusId == (short)OfferExecutionStatus.Running)
                editStatus = OfferManager.Instance.ChangeStatusForOffer(foodOffer, OfferExecutionStatus.Suspended);
            else if (foodOffer.OfferStatusId == (short)OfferExecutionStatus.Suspended)
                editStatus = OfferManager.Instance.ChangeStatusForOffer(foodOffer, OfferExecutionStatus.Running);
            else
                return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "این آفر وضعیت آن به اتمام رسیده است نمیتوان وضعیت آن را عوض کرد", "", "", ActionResponseFromServerToClient.EmptyObjectServerToClientKey));

            if (editStatus == Core.EnumTypes.EditStatus.Edited || editStatus == Core.EnumTypes.EditStatus.RejectedByNonEffectiveQuery)
                return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, "", "تایید گردید", "", ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
            return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "خطای داخلی سیستم، بعدا سعی نمایید", "", "", ActionResponseFromServerToClient.EmptyObjectServerToClientKey));

        }


        [HttpGet]
        public JsonResult LoadOffersForConfirm(int? page, int? limit, string sortBy, string direction, string searchStr, int offerType, int roleType, int confirmStatus, bool isOnlyReported)
        {
            Console.Write(offerType + ", " + roleType + ", " + confirmStatus + ", " + isOnlyReported);
            int take = limit.HasValue ? limit.Value : 20;
            int skip = 0;
            if (page.HasValue)
            {
                skip = (page.Value - 1) * limit.Value;
                //records = query.Skip(start).Take(limit.Value).ToList();
            }

            Nullable<EnumOfferType> offerTypeEnum = offerType == 0 ? (EnumOfferType?)null : (EnumOfferType)offerType;
            Nullable<RoleType> roleTypeEnum = roleType == 0 ? (RoleType?)null : (RoleType)roleType;
            Nullable<ConfirmStatus> offerStatusEnum = confirmStatus == 0 ? (ConfirmStatus?)null : (ConfirmStatus)confirmStatus;
            var offers = OfferManager.Instance.getOffersByCriteria(!string.IsNullOrEmpty(searchStr) ? searchStr.Split(' ') : null, offerTypeEnum, roleTypeEnum, offerStatusEnum, isOnlyReported);
            int total = offers.Count();
            if (!string.IsNullOrEmpty(sortBy) && !string.IsNullOrEmpty(direction))
            {
                if (direction.Trim() == "asc")
                {
                    switch (sortBy)
                    {
                        case "Title":
                            offers = offers.OrderBy(q => q.Title);
                            break;
                        case "Owner":
                            offers = offers.OrderBy(q => q.MembershipUser.UserName);
                            break;
                        case "DateTime":
                            offers = offers.OrderBy(q => q.DateTime);
                            break;
                        case "ConfrimDateTime":
                            offers = offers.OrderBy(q => q.ConfirmDateTime);
                            break;
                        case "IsConfirm":
                            offers = offers.OrderBy(q => q.ConfirmStatusId);
                            break;
                        case "Status":
                            offers = offers.OrderBy(q => q.OfferFoods.Select(e => e.OfferStatusId));
                            break;
                    }
                }
                else
                {
                    switch (sortBy)
                    {
                        case "Title":
                            offers = offers.OrderByDescending(q => q.Title);
                            break;
                        case "Owner":
                            offers = offers.OrderByDescending(q => q.MembershipUser.UserName);
                            break;
                        case "DateTime":
                            offers = offers.OrderByDescending(q => q.DateTime);
                            break;
                        case "ConfrimDateTime":
                            offers = offers.OrderByDescending(q => q.ConfirmDateTime);
                            break;
                        case "IsConfirm":
                            offers = offers.OrderByDescending(q => q.ConfirmStatusId);
                            break;
                        case "Status":
                            offers = offers.OrderByDescending(q => q.OfferFoods.Select(e => e.OfferStatusId));
                            break;
                    }
                }
            }

            var records = offers.Skip(skip).Take(take).ToList().Select(e => new
            {
                Body = e.Body,
                OfferId = e.OfferId,
                Title = e.Title,
                UserId = e.SenderUserId,
                UserName = e.MembershipUser.UserName,
                UserDisplayName = e.MembershipUser.FirstName,
                DateTime = e.DateTime.ToString("yyyy/MM/dd HH:mm:ss"),
                ConfirmDateTime = e.ConfirmDateTime.HasValue ? e.ConfirmDateTime.Value.ToString("yyyy/MM/dd HH:mm:ss") : string.Empty,
                HasReport = e.OfferReports.FirstOrDefault() != null,
                IsConfirm = e.ConfirmStatu.Name,
                Status = e.OfferFoods.FirstOrDefault() != null ? e.OfferFoods.FirstOrDefault().OfferStatu.Name : string.Empty,
            }).ToArray();
            // return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "search offers", offers.ToArray(), ActionResponseFromServerToClient.OffersServerToClientKey));
            return Json(new { records, total }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Sales & buys
        public JsonResult LoadSalesGroupByUser(int? page, int? limit, string sortBy, string direction, string sallerDisplayName, string sallerUserName, bool justUsersHaveSale)
        {

            int take = limit.HasValue ? limit.Value : 20;
            int skip = 0;
            if (page.HasValue)
            {
                skip = (page.Value - 1) * limit.Value;
                //records = query.Skip(start).Take(limit.Value).ToList();
            }


            //var coutries = OfferManager.Instance.getAllOfOrderedOffers().GroupBy(e=>e.;
            //var orderedOffers = OfferManager.Instance.foodoffer().Where(e => e.OfferFoods.Where(o => o.OfferFoodOrders.Count() > 0).Count() > 0);
            var offers = OfferManager.Instance.getOffersByCriteria(null, EnumOfferType.Food, null, null, false);
            if (!string.IsNullOrEmpty(sallerDisplayName))
                offers = offers.Where(e => e.MembershipUser.FirstName.Contains(sallerDisplayName) || e.MembershipUser.LastName.Contains(sallerDisplayName));
            if (!string.IsNullOrEmpty(sallerUserName))
                offers=offers.Where(e=>e.MembershipUser.UserName.Contains(sallerUserName));
            if (justUsersHaveSale)
                offers = offers.Where(e => e.OfferFoods.Where(o => o.OfferFoodOrders.Count() > 0).Count() > 0);

            var offersGroupByUser = offers.GroupBy(e => e.MembershipUser).Select(e => new SellerUserInfoDTO()
            {
                SallerDisplayName = e.Key.FirstName,
                SallerFileName = e.Key.Picture,
                SallerUserName = e.Key.UserName,
                SallerUserId=e.Key.UserId,
                TotalOffers = e.Count(),
                TotalSaleAmount = e.Sum(o => o.OfferFoods.FirstOrDefault() != null && o.OfferFoods.FirstOrDefault().OfferFoodOrders.Count>0? o.OfferFoods.FirstOrDefault().OfferFoodOrders.Sum(of => of.PaymentAmount.HasValue ? of.PaymentAmount.Value : 0) : 0),
                TotalSaledOffersCount = e.Sum(o => o.OfferFoods.FirstOrDefault() != null && o.OfferFoods.FirstOrDefault().OfferFoodOrders.Count > 0 ? o.OfferFoods.FirstOrDefault().OfferFoodOrders.Count : 0)
            }).OrderByDescending(e=>e.TotalOffers);

            int total = offersGroupByUser.Count();
            if (!string.IsNullOrEmpty(sortBy) && !string.IsNullOrEmpty(direction))
            {
                if (direction.Trim() == "asc")
                {
                    switch (sortBy)
                    {
                        case "SallerDisplayName":
                            offersGroupByUser = offersGroupByUser.OrderBy(q => q.SallerDisplayName);
                            break;
                        case "SallerFileName":
                            offersGroupByUser = offersGroupByUser.OrderBy(q => q.SallerFileName);
                            break;
                        case "SallerUserName":
                            offersGroupByUser = offersGroupByUser.OrderBy(q => q.SallerUserName);
                            break;
                        case "TotalOffers":
                            offersGroupByUser = offersGroupByUser.OrderBy(q => q.TotalOffers);
                            break;
                        case "TotalSaleAmount":
                            offersGroupByUser = offersGroupByUser.OrderBy(q => q.TotalSaleAmount);
                            break;
                        case "TotalSaledOffersCount":
                            offersGroupByUser = offersGroupByUser.OrderBy(q => q.TotalSaledOffersCount);
                            break;

                    }
                }
                else
                {
                    switch (sortBy)
                    {
                        case "SallerDisplayName":
                            offersGroupByUser = offersGroupByUser.OrderByDescending(q => q.SallerDisplayName);
                            break;
                        case "SallerFileName":
                            offersGroupByUser = offersGroupByUser.OrderByDescending(q => q.SallerFileName);
                            break;
                        case "SallerUserName":
                            offersGroupByUser = offersGroupByUser.OrderByDescending(q => q.SallerUserName);
                            break;
                        case "TotalOffers":
                            offersGroupByUser = offersGroupByUser.OrderByDescending(q => q.TotalOffers);
                            break;
                        case "TotalSaleAmount":
                            offersGroupByUser = offersGroupByUser.OrderByDescending(q => q.TotalSaleAmount);
                            break;
                        case "TotalSaledOffersCount":
                            offersGroupByUser = offersGroupByUser.OrderByDescending(q => q.TotalSaledOffersCount);
                            break;
                    }
                }
            }
            var records = offersGroupByUser.Skip(skip).Take(take).ToList();
            foreach (var entity in records)
            {
                entity.SallerFileName =GeneralService.GetThumbnailOf(entity.SallerUserId , entity.SallerFileName);
            }
            

            return Json(new { records, total }, JsonRequestBehavior.AllowGet);
        }

        public FileContentResult ExportToExcellSaleOffers(string sallerDisplayName, string sallerUserName, bool justUsersHaveSale)
        {
            var offers = OfferManager.Instance.getOffersByCriteria(null, EnumOfferType.Food, null, null, false);
            if (!string.IsNullOrEmpty(sallerDisplayName))
                offers = offers.Where(e => e.MembershipUser.FirstName.Contains(sallerDisplayName) || e.MembershipUser.LastName.Contains(sallerDisplayName));
            if (!string.IsNullOrEmpty(sallerUserName))
                offers = offers.Where(e => e.MembershipUser.UserName.Contains(sallerUserName));
            if (justUsersHaveSale)
                offers = offers.Where(e => e.OfferFoods.Where(o => o.OfferFoodOrders.Count() > 0).Count() > 0);

            var offersGroupByUser = offers.GroupBy(e => e.MembershipUser).Select(e => new SellerUserInfoDTO()
            {
                SallerDisplayName = e.Key.FirstName,
                SallerFileName = e.Key.Picture,
                SallerUserName = e.Key.UserName,
                SallerUserId = e.Key.UserId,
                TotalOffers = e.Count(),
                TotalSaleAmount = e.Sum(o => o.OfferFoods.FirstOrDefault() != null && o.OfferFoods.FirstOrDefault().OfferFoodOrders.Count > 0 ? o.OfferFoods.FirstOrDefault().OfferFoodOrders.Sum(of => of.PaymentAmount.HasValue ? of.PaymentAmount.Value : 0) : 0),
                TotalSaledOffersCount = e.Sum(o => o.OfferFoods.FirstOrDefault() != null && o.OfferFoods.FirstOrDefault().OfferFoodOrders.Count > 0 ? o.OfferFoods.FirstOrDefault().OfferFoodOrders.Count : 0)
            }).OrderByDescending(e => e.TotalOffers);
            return ExcelService.ExportToExcel<SellerUserInfoDTO>(offersGroupByUser.ToList(), "SaleOffers.xls");
        }


        public JsonResult LoadBuysGroupByUser(int? page, int? limit, string sortBy, string direction, string buyerDisplayName, string buyerUserName, bool justUsersHaveBuy)
        {

            int take = limit.HasValue ? limit.Value : 20;
            int skip = 0;
            if (page.HasValue)
            {
                skip = (page.Value - 1) * limit.Value;
                //records = query.Skip(start).Take(limit.Value).ToList();
            }


            //var coutries = OfferManager.Instance.getAllOfOrderedOffers().GroupBy(e=>e.;
            //var orderedOffers = OfferManager.Instance.foodoffer().Where(e => e.OfferFoods.Where(o => o.OfferFoodOrders.Count() > 0).Count() > 0);
            var users = MembershipManager.Instance.GetAllUsers();
            if (!string.IsNullOrEmpty(buyerDisplayName))
                users = users.Where(e => e.FirstName.Contains(buyerDisplayName) || e.LastName.Contains(buyerDisplayName));
            if (!string.IsNullOrEmpty(buyerUserName))
                users = users.Where(e => e.UserName.Contains(buyerUserName));
            if (justUsersHaveBuy)
                users = users.Where(e => e.OfferFoodOrders.Count>0);

            var offersGroupByUser = users.Select(e => new BuyerUserInfoDTO()
            {
                BuyerDisplayName = e.FirstName + " " + e.LastName,
                BuyerEmail = e.Email,
                BuyerFileName = e.Picture,
                BuyerPhoneNumber = e.PhoneNumber,
                BuyerUserName = e.UserName,
                TotalBuyAmount = e.OfferFoodOrders.Count>0?e.OfferFoodOrders.Sum(o => o.PaymentAmount.HasValue ? o.PaymentAmount.Value : 0):0,
                TotalBuyOffersCount = e.OfferFoodOrders.Count > 0? e.OfferFoodOrders.Sum(o => o.Quantity):0,
                BuyerUserId = e.UserId
            }).OrderByDescending(e => e.TotalBuyAmount);

            int total = offersGroupByUser.Count();
            if (!string.IsNullOrEmpty(sortBy) && !string.IsNullOrEmpty(direction))
            {
                if (direction.Trim() == "asc")
                {
                    switch (sortBy)
                    {
                        case "DisplayName":
                            offersGroupByUser = offersGroupByUser.OrderBy(q => q.BuyerDisplayName);
                            break;
                        case "Email":
                            offersGroupByUser = offersGroupByUser.OrderBy(q => q.BuyerEmail);
                            break;
                        case "PhoneNumber":
                            offersGroupByUser = offersGroupByUser.OrderBy(q => q.BuyerPhoneNumber);
                            break;
                        case "UserName":
                            offersGroupByUser = offersGroupByUser.OrderBy(q => q.BuyerUserName);
                            break;
                        case "TotalBuyAmount":
                            offersGroupByUser = offersGroupByUser.OrderBy(q => q.TotalBuyAmount);
                            break;
                        case "TotalBuyOffersCount":
                            offersGroupByUser = offersGroupByUser.OrderBy(q => q.TotalBuyOffersCount);
                            break;

                    }
                }
                else
                {
                    switch (sortBy)
                    {
                        case "DisplayName":
                            offersGroupByUser = offersGroupByUser.OrderByDescending(q => q.BuyerDisplayName);
                            break;
                        case "Email":
                            offersGroupByUser = offersGroupByUser.OrderByDescending(q => q.BuyerEmail);
                            break;
                        case "PhoneNumber":
                            offersGroupByUser = offersGroupByUser.OrderByDescending(q => q.BuyerPhoneNumber);
                            break;
                        case "UserName":
                            offersGroupByUser = offersGroupByUser.OrderByDescending(q => q.BuyerUserName);
                            break;
                        case "TotalBuyAmount":
                            offersGroupByUser = offersGroupByUser.OrderByDescending(q => q.TotalBuyAmount);
                            break;
                        case "TotalBuyOffersCount":
                            offersGroupByUser = offersGroupByUser.OrderByDescending(q => q.TotalBuyOffersCount);
                            break;
                    }
                }
            }
            var records = offersGroupByUser.Skip(skip).Take(take).ToList();
            foreach (var entity in records)
            {
                entity.BuyerFileName = GeneralService.GetThumbnailOf(entity.BuyerUserId, entity.BuyerFileName);
            }


            return Json(new { records, total }, JsonRequestBehavior.AllowGet);
        }

        public FileContentResult ExportToExcellBuysOffers(string buyerDisplayName, string buyerUserName, bool justUsersHaveBuy)
        {

            //var coutries = OfferManager.Instance.getAllOfOrderedOffers().GroupBy(e=>e.;
            //var orderedOffers = OfferManager.Instance.foodoffer().Where(e => e.OfferFoods.Where(o => o.OfferFoodOrders.Count() > 0).Count() > 0);
            var users = MembershipManager.Instance.GetAllUsers();
            if (!string.IsNullOrEmpty(buyerDisplayName))
                users = users.Where(e => e.FirstName.Contains(buyerDisplayName) || e.LastName.Contains(buyerDisplayName));
            if (!string.IsNullOrEmpty(buyerUserName))
                users = users.Where(e => e.UserName.Contains(buyerUserName));
            if (justUsersHaveBuy)
                users = users.Where(e => e.OfferFoodOrders.Count > 0);

            var offersGroupByUser = users.Select(e => new BuyerUserInfoDTO()
            {
                BuyerDisplayName = e.FirstName + " " + e.LastName,
                BuyerEmail = e.Email,
                BuyerFileName = e.Picture,
                BuyerPhoneNumber = e.PhoneNumber,
                BuyerUserName = e.UserName,
                TotalBuyAmount = e.OfferFoodOrders.Count > 0 ? e.OfferFoodOrders.Sum(o => o.PaymentAmount.HasValue ? o.PaymentAmount.Value : 0) : 0,
                TotalBuyOffersCount = e.OfferFoodOrders.Count > 0 ? e.OfferFoodOrders.Sum(o => o.Quantity) : 0,
                BuyerUserId = e.UserId
            }).OrderByDescending(e => e.TotalBuyAmount);

            return ExcelService.ExportToExcel<BuyerUserInfoDTO>(offersGroupByUser.ToList(), "BuyOffers.xls");
        }
     
        #endregion


        #region Voucher
        public JsonResult LoadVouchers(int? page, int? limit, string sortBy, string direction, int? detailedCodeLevel1, int? detailedCodeLevel2, int? detailedCodeLevel3)
        {

            int take = limit.HasValue ? limit.Value : 20;
            int skip = 0;
            if (page.HasValue)
            {
                skip = (page.Value - 1) * limit.Value;
                //records = query.Skip(start).Take(limit.Value).ToList();
            }


            //var coutries = OfferManager.Instance.getAllOfOrderedOffers().GroupBy(e=>e.;
            //var orderedOffers = OfferManager.Instance.foodoffer().Where(e => e.OfferFoods.Where(o => o.OfferFoodOrders.Count() > 0).Count() > 0);
            var orders = VoucherManager.Instance.getAllOfVouchers();
            if (detailedCodeLevel1 .HasValue)
                orders = orders.Where(e => e.OfferFoodOrder.OfferFood.Offer.DetailedCode.Equals(detailedCodeLevel1.Value));
            if (detailedCodeLevel2.HasValue)
                orders = orders.Where(e => e.OfferFoodOrder.MembershipUser.DetailedCode.Equals(detailedCodeLevel2.Value));
            if (detailedCodeLevel3.HasValue)
                orders = orders.Where(e => e.OfferFoodOrder.OfferFood.Offer.MembershipCity != null ? e.OfferFoodOrder.OfferFood.Offer.MembershipCity.DetailedCode.Equals(detailedCodeLevel3.Value) : false);

            var vocherDTO = orders.Select(e => new VoucherDTO()
            {
                AccountCode = e.AccAccount.Code,
                AccountId = e.AccountId,
                AccountName = e.AccAccount.Name,
                Credit = e.Credit.HasValue ? e.Credit.Value : 0,
                Debit = e.Debit.HasValue ? e.Debit.Value : 0,
                Description = e.Description,
                DetailedCodeLevel1 = e.OfferFoodOrder.OfferFood.Offer.DetailedCode.HasValue ? e.OfferFoodOrder.OfferFood.Offer.DetailedCode.Value : 0,
                DetailedCodeLevel2 = e.OfferFoodOrder.MembershipUser.DetailedCode.HasValue ? e.OfferFoodOrder.MembershipUser.DetailedCode.Value : 0,
                DetailedCodeLevel3 = e.OfferFoodOrder.OfferFood.Offer.MembershipCity != null && e.OfferFoodOrder.OfferFood.Offer.MembershipCity.DetailedCode.HasValue ? e.OfferFoodOrder.OfferFood.Offer.MembershipCity.DetailedCode.Value : 0,
                EnDateTime = e.DateTime,
                OrderId = e.OrderId.HasValue ? e.OrderId.Value : 0,
                SaleOrderId = e.OfferFoodOrder.SaleOrderId.ToString(),

            }).OrderByDescending(e => e.EnDateTime);

            int total = vocherDTO.Count();
            if (!string.IsNullOrEmpty(sortBy) && !string.IsNullOrEmpty(direction))
            {
                if (direction.Trim() == "asc")
                {
                    switch (sortBy)
                    {
                        case "AccountCode":
                            vocherDTO = vocherDTO.OrderBy(q => q.AccountCode);
                            break;
                        case "AccountName":
                            vocherDTO = vocherDTO.OrderBy(q => q.AccountName);
                            break;
                        case "Credit":
                            vocherDTO = vocherDTO.OrderBy(q => q.Credit);
                            break;
                        case "Debit":
                            vocherDTO = vocherDTO.OrderBy(q => q.Debit);
                            break;
                        case "Description":
                            vocherDTO = vocherDTO.OrderBy(q => q.Description);
                            break;
                        case "DetailedCodeLevel1":
                            vocherDTO = vocherDTO.OrderBy(q => q.DetailedCodeLevel1);
                            break;
                        case "DetailedCodeLevel2":
                            vocherDTO = vocherDTO.OrderBy(q => q.DetailedCodeLevel2);
                            break;
                        case "DetailedCodeLevel3":
                            vocherDTO = vocherDTO.OrderBy(q => q.DetailedCodeLevel3);
                            break;
                        case "EnDateTime":
                            vocherDTO = vocherDTO.OrderBy(q => q.EnDateTime);
                            break;
                        case "FaDateTime":
                            vocherDTO = vocherDTO.OrderBy(q => q.FaDateTime);
                            break;

                    }
                }
                else
                {
                    switch (sortBy)
                    {
                        case "AccountCode":
                            vocherDTO = vocherDTO.OrderByDescending(q => q.AccountCode);
                            break;
                        case "AccountName":
                            vocherDTO = vocherDTO.OrderByDescending(q => q.AccountName);
                            break;
                        case "Credit":
                            vocherDTO = vocherDTO.OrderByDescending(q => q.Credit);
                            break;
                        case "Debit":
                            vocherDTO = vocherDTO.OrderByDescending(q => q.Debit);
                            break;
                        case "Description":
                            vocherDTO = vocherDTO.OrderByDescending(q => q.Description);
                            break;
                        case "DetailedCodeLevel1":
                            vocherDTO = vocherDTO.OrderByDescending(q => q.DetailedCodeLevel1);
                            break;
                        case "DetailedCodeLevel2":
                            vocherDTO = vocherDTO.OrderByDescending(q => q.DetailedCodeLevel2);
                            break;
                        case "DetailedCodeLevel3":
                            vocherDTO = vocherDTO.OrderByDescending(q => q.DetailedCodeLevel3);
                            break;
                        case "EnDateTime":
                            vocherDTO = vocherDTO.OrderByDescending(q => q.EnDateTime);
                            break;
                        case "FaDateTime":
                            vocherDTO = vocherDTO.OrderByDescending(q => q.FaDateTime);
                            break;
                    }
                }
            }
            var records = vocherDTO.Skip(skip).Take(take).ToList();
            foreach (var entity in records)
            {
                entity.FaDateTime = Offtick.Core.Utility.PersianTools.PersianDateConvertor.ToKhorshidiDate(entity.EnDateTime);
            }


            return Json(new { records, total }, JsonRequestBehavior.AllowGet);
        }

        public FileContentResult ExportToExcellVouchers(int? detailedCodeLevel1, int? detailedCodeLevel2, int? detailedCodeLevel3)
        {
            var orders = VoucherManager.Instance.getAllOfVouchers();
            if (detailedCodeLevel1.HasValue)
                orders = orders.Where(e => e.OfferFoodOrder.OfferFood.Offer.DetailedCode.Equals(detailedCodeLevel1.Value));
            if (detailedCodeLevel2.HasValue)
                orders = orders.Where(e => e.OfferFoodOrder.MembershipUser.DetailedCode.Equals(detailedCodeLevel2.Value));
            if (detailedCodeLevel3.HasValue)
                orders = orders.Where(e => e.OfferFoodOrder.OfferFood.Offer.MembershipCity != null ? e.OfferFoodOrder.OfferFood.Offer.MembershipCity.DetailedCode.Equals(detailedCodeLevel3.Value) : false);

            var vocherDTO = orders.Select(e => new VoucherDTO()
            {
                AccountCode = e.AccAccount.Code,
                AccountId = e.AccountId,
                AccountName = e.AccAccount.Name,
                Credit = e.Credit.HasValue ? e.Credit.Value : 0,
                Debit = e.Debit.HasValue ? e.Debit.Value : 0,
                Description = e.Description,
                DetailedCodeLevel1 = e.OfferFoodOrder.OfferFood.Offer.DetailedCode.HasValue ? e.OfferFoodOrder.OfferFood.Offer.DetailedCode.Value : 0,
                DetailedCodeLevel2 = e.OfferFoodOrder.MembershipUser.DetailedCode.HasValue ? e.OfferFoodOrder.MembershipUser.DetailedCode.Value : 0,
                DetailedCodeLevel3 = e.OfferFoodOrder.OfferFood.Offer.MembershipCity != null && e.OfferFoodOrder.OfferFood.Offer.MembershipCity.DetailedCode.HasValue ? e.OfferFoodOrder.OfferFood.Offer.MembershipCity.DetailedCode.Value : 0,
                EnDateTime = e.DateTime,
                OrderId = e.OrderId.HasValue ? e.OrderId.Value : 0,
                SaleOrderId = e.OfferFoodOrder.SaleOrderId.ToString(),

            }).OrderByDescending(e => e.EnDateTime);

          
            var records = vocherDTO.ToList();
            foreach (var entity in records)
            {
                entity.FaDateTime = Offtick.Core.Utility.PersianTools.PersianDateConvertor.ToKhorshidiDate(entity.EnDateTime);
            }
            return ExcelService.ExportToExcel<VoucherDTO>(records.ToList(), "Vouchers.xls");
        }


        #endregion

    }


    public class CountryDTO
    {
        public string Name { get; set; }
        public string DetailedCode { get; set; }
    }

    public class StateDTO
    {
        public string Name { get; set; }
        public string DetailedCode { get; set; }
        public string CountryDetailedCode { get; set; }
    }


    public class CityDTO
    {
        public string Name { get; set; }
        public string DetailedCode { get; set; }
        public string StateDetailedCode { get; set; }
    }

    public class OfferExportDTO{
        public string Body{get;set;}
        public string Title{get;set;}
        public string UserDetailedCode{get;set;}
        public string UserName{get;set;}
        public string UserDisplayName{get;set;}
        public string DateTime{get;set;}
        public string ConfirmDateTime{get;set;}
        public string IsConfirm{get;set;}
        public string Status{get;set;}
        public string DetailedCode{get;set;}
    }

    public class UserExportDTO
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string RegisterDateTime { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Status { get; set; }
        public string DetailedCode { get; set; }
    }
    public class SellerUserInfoDTO
    {
        public string SallerDisplayName { get; set; }
        public string SallerUserName { get; set; }
        public Guid SallerUserId { get; set; }
        public string SallerFileName { get; set; }
        public long TotalSaleAmount { get; set; }
        public long TotalOffers { get; set; }
        public long TotalSaledOffersCount { get; set; }
    }
    public class BuyerUserInfoDTO
    {
        public string BuyerDisplayName { get; set; }
        public string BuyerUserName { get; set; }
        public Guid BuyerUserId { get; set; }
        public string BuyerFileName { get; set; }
        public long TotalBuyAmount { get; set; }
        public long TotalBuyOffersCount { get; set; }
        public string BuyerPhoneNumber { get; set; }
        public string BuyerEmail { get; set; }
    }


    public class VoucherDTO
    {
        public int AccountId { get; set; }
        public int AccountCode { get; set; }
        public String AccountName { get; set; }
        public long OrderId { get; set; }
        public string SaleOrderId { get; set; }
        public int DetailedCodeLevel1 { get; set; }
        public int DetailedCodeLevel2 { get; set; }
        public int DetailedCodeLevel3 { get; set; }
        public string Description { get; set; }
        public long Debit { get; set; }
        public long Credit { get; set; }

        public string FaDateTime { get; set; }
        public DateTime EnDateTime { get; set; }
    }
       
}
