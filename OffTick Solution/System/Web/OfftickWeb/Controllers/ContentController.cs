﻿using Offtick.Business.Services.Storage;
using Offtick.Business.Web;
using Offtick.Core.EnumTypes;
using Offtick.Web.OfftickWeb.Infrustracture;
using Offtick.Data.Context.ExpertOnlinerContexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Offtick.Web.OfftickWeb.Controllers
{
    public class ContentController : BaseController
    {
        //
        // GET: /Content/

        public ContentController(DataContextContainer obj)
            : base(obj )
        {
            
        }


        public ActionResult Index(Guid menuId)
        {
            var menu = Offtick.Business.Services.Managers.ContentManager.Instance.GetMenu(menuId);
            if (!menu.IsSystemMenu)
                return View(menu);
            else
                return View(menu.MenuSystemic.SectionName, menu);

    
        }

        public ActionResult News(Guid newsId)
        {
            var news = Offtick.Business.Services.Managers.NewsManager.Instance.GetNews(newsId);
            return View(news);
        }

        public ActionResult SubmitContactMessage(ContactMessage message)
        {
            AddStatus status = Offtick.Business.Services.Managers.ContentManager.Instance.AddContactMessage(message);
            string msg = UIActionStatusToUiMessage.ActionStatusToMessage(status);
            return Json(msg);
        }

    }
}
