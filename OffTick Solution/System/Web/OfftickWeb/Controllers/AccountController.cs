﻿using Offtick.Business.Membership;
using Offtick.Business.Web;
using Offtick.Core.Utility;
using Offtick.Core.Utility.PersianTools;
using Offtick.Data.Entities.Common;
using Offtick.Web.OfftickWeb.Models;
using Offtick.Web.OfftickWeb.Resources;
using Offtick.Data.Context.ExpertOnlinerContexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace Offtick.Web.OfftickWeb.Controllers
{
    public class AccountController : BaseController
    {

        public AccountController(Offtick.Business.Services.Storage.DataContextContainer obj)
            : base(obj )
        {
        }
        

        // GET: /Account/LogOn
        public ActionResult SimpleLogin()
        {
            return View();
        }


        public ActionResult LoginRegister()
        {
            return View("LoginRegister");
        }

        public ActionResult LogOn()
        {

            //TempData["HasLoginLayout"] = hasLayout;
            return View();
        }

        //
        // POST: /Account/LogOn

        [HttpPost]
        public ActionResult LogOn(LogOnModel model, string returnUrl, string requestedAction, string offerId,string comment)
        {
            if (ModelState.IsValid)
            {
                LoginStatus LoginResult = MembershipManager.Instance.LoginMember(model.UserName, model.Password);

                string error = string.Empty;
                
                switch (LoginResult)
                {
                    case LoginStatus.DisabledUser:
                        error = string.Format(Resources.UIMessages.IncorrentAccountState, Resources.UICaptions.UserName, Resources.UICaptions.LoginStateDisabled);
                         MembershipManager.Instance.ChangeStatus( MembershipManager.Instance.GetUser(model.UserName), MembershipStatus.Active);
                         goto case LoginStatus.Success;

                    case LoginStatus.Success:
                        if (!string.IsNullOrEmpty(requestedAction))
                        {
                            if (requestedAction == "comment")
                            {
                                var offerController = new Offtick.Web.OfftickWeb.Areas.User.Controllers.OfferController(Offtick.Business.Services.Storage.DataContextManager.Container);
                                offerController.AddCommentForOffer(offerId, model.UserName, comment);
                            }
                            if (requestedAction == "like")
                            {
                                var offerController = new Offtick.Web.OfftickWeb.Areas.User.Controllers.OfferController(Offtick.Business.Services.Storage.DataContextManager.Container);
                                offerController.LikeToggleForOffer(offerId, model.UserName);
                            }
                        }


                        System.Web.Security.FormsAuthentication.SetAuthCookie(MembershipManager.Instance.GetUser(model.UserName).UserName, model.RememberMe);
                       // if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/")& !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
                        if(!string.IsNullOrEmpty(returnUrl))
                        {
                            return Redirect(returnUrl);
                        }
                        else
                        {
                            return RedirectToAction("Wall", "Profile", new { area = "User" });
                        }
                        break;
                    case LoginStatus.BlockUser:
                        error = string.Format(Resources.UIMessages.IncorrentAccountState, Resources.UICaptions.UserName, Resources.UICaptions.LoginStateBlock);
                        break;
                    case LoginStatus.InactiveUser:
                        error = string.Format(Resources.UIMessages.IncorrentAccountState, Resources.UICaptions.UserName, Resources.UICaptions.LoginStateDisabled);
                        break;
                    case LoginStatus.InvalidPassword:
                        error = string.Format(Resources.UIMessages.IncorrentAccountState, Resources.UICaptions.Password, Resources.UICaptions.LoginStateInvalidPassword);
                        break;
                    case LoginStatus.InvalidUserName:
                        error = string.Format(Resources.UIMessages.IncorrentAccountState, Resources.UICaptions.UserName, Resources.UICaptions.LoginStateInvalidUserName);
                        break;
                    case LoginStatus.PreActiveUser:
                        error = string.Format(Resources.UIMessages.IncorrentAccountState, Resources.UICaptions.UserName, Resources.UICaptions.LoginStatePreActive);
                        break;
                  
                }
                ModelState.AddModelError("",error);
                
            }


            // If we got this far, something failed, redisplay form
            return View( model);
        }

        //
        // GET: /Account/LogOff

        public ActionResult LogOff()
        {
            System.Web.Security.FormsAuthentication.SignOut();
            Session.Clear();
            Session.RemoveAll();
            Session.Abandon();

            return RedirectToAction("Index", "Home", new { area = "" });
        }

        ////
        //// GET: /Account/Register

        //public ActionResult Register()
        //{
        //    return View();

        //}

        public ActionResult Register(string registerMode)
        {
            return View();
        }

        //
        // POST: /Account/Register


        public ActionResult SimpleRegister()
        {
            return View("SimpleRegister");
        }

        [HttpPost]
        public ActionResult SimpleRegister(SimpleRegisterModel model)
        {
            var configManager = ConfigFactory.GetConfigManager(ConfigManagerType.WebConfig);
            //if (ModelState.IsValid)
            //{

            
                
                string roleName =  configManager.ReadSetting(Offtick.Web.OfftickWeb.Infrustracture.Constants.CompanyNameKey);

                RegisterStatus result = MembershipManager.Instance.InitialRegisterMember(
                    model.Email,
                    model.Password,
                    model.FirstName,
                    model.LastName,
                    "",
                    DateTime.Now,
                    model.Email,
                    string.Empty,
                    string.Empty,
                    roleName);
                if (result == RegisterStatus.Success)
                {
                    System.Web.Security.FormsAuthentication.SetAuthCookie(model.Email, false /* createPersistentCookie */);
                    
                    return RedirectToAction("Index", "Home", new { area = "DR" });
                    

                }
                else
                {
                    ModelState.AddModelError("", "Failed to register");
                    return View(model);
                }

            //}

            //// If we got this far, something failed, redisplay form
            //return View(model);
        }

        
        public ActionResult RegisterStep1()
        {
            var model = Session["RegisterStep1"];
            return View(model);
        }
        [HttpPost]
        public ActionResult RegisterStep1(RegisterModelStep1 model)
        {
            bool existEmail = MembershipManager.Instance.ExistEmail(model.Email);
            bool existPhoneNumber = MembershipManager.Instance.ExistPhoneNumber(model.PhoneNumber);

            if (existPhoneNumber && !string.IsNullOrEmpty(model.PhoneNumber) && !string.IsNullOrEmpty(model.PhoneNumber.Trim()))
            {
                ModelState.AddModelError("", UIMessages.ExistedMobileNumber);

            }
            if (existEmail && !string.IsNullOrEmpty(model.Email) && !string.IsNullOrEmpty(model.Email.Trim()))
            {
                ModelState.AddModelError("", UIMessages.ExistedEmail);
            }

            if (string.IsNullOrEmpty(model.Password) || string.IsNullOrEmpty(model.Password.Trim()) || model.Password.Length < 6)
            {
                ModelState.AddModelError("", string.Format(UIMessages.MinLength,UICaptions.Password ,6));
            }
            if (ModelState.IsValid)
            {
                //bool existUserName = MembershipManager.Instance.ExistUser(model.UserName);
             
                

                Session["RegisterStep1"] = model;
                
                return RegisterStep2();
            }
            return View();

        }

        public ActionResult RegisterStep2()
        {
            RegisterModelStep1 model1 = (RegisterModelStep1)Session["RegisterStep1"];
            RegisterModelStep2 model2 =(RegisterModelStep2) Session["RegisterStep2"];

            if (model1 == null)
                return View("ExpireSession");

            if(model2==null)
                model2=new RegisterModelStep2();

         //   if (string.IsNullOrEmpty(model2.UserName))
           //     model2.UserName=!string.IsNullOrEmpty(model1.PhoneNumber)?model1.PhoneNumber:model1.Email;
            
            
            model2.FillLater = false;
            return View("RegisterStep2",model2);
        }
        [HttpPost]
        public ActionResult RegisterStep2(RegisterModelStep2 model)
        {
            RegisterModelStep1 modelStep1 = (RegisterModelStep1)Session["RegisterStep1"];
            if (modelStep1 == null)
                return View("ExpireSession");

            if (ModelState.IsValid)
            {
                if (Offtick.Business.Membership.MembershipManager.Instance.ExistUser(model.UserName))
                {
                    ModelState.AddModelError("", UIMessages.ExistedUserName);
                    return View();
                }

                    Guid guidUserId;
                    string userId ;
                    if (model == null || !Guid.TryParse(model.userId, out guidUserId))
                    {
                        guidUserId = Guid.NewGuid();
                        userId = guidUserId.ToString();
                        model.userId = userId;
                    }
                    else
                    {
                        userId = model.userId;
                    }
                    
                    

                    if (model.inputFilePicture != null)
                    {
                        var ifm = new Offtick.Business.Services.Storage.File.ImageFileManager(userId);
                        string fileName = model.inputFilePicture.FileName;
                        string newFileName = ifm.Save(model.inputFilePicture, fileName);
                        model.fileName = newFileName;
                    }
                   
                    string lisenceFileName = string.Empty;
                    if (model.inputFileLisence != null)
                    {
                        var ifm = new Offtick.Business.Services.Storage.File.ImageFileManager(userId);
                        lisenceFileName = ifm.Save(model.inputFileLisence, model.inputFileLisence.FileName);
                        model.lisenceFileName = lisenceFileName;
                    }

                    Session["RegisterStep2"] = model;
                    if (model.FillLater)
                    {
                        return RegisterStep3(null);
                    }
                    else
                    {
                        return View("RegisterStep3");
                    }
                
            }
            return View();

        }

        public ActionResult RegisterStep3()
        {
            RegisterModelStep1 modelStep1 = (RegisterModelStep1)Session["RegisterStep1"];
            RegisterModelStep2 modelStep2 = (RegisterModelStep2)Session["RegisterStep2"];
             if (modelStep1 == null || modelStep2 == null)
                 return View("ExpireSession");

            var model = Session["RegisterStep3"];
            return View(model);
        }

        [HttpPost]
        public ActionResult RegisterStep3(RegisterModelStep3 model)
        {
            RegisterModelStep1 modelStep1 = (RegisterModelStep1)Session["RegisterStep1"];
            RegisterModelStep2 modelStep2 = (RegisterModelStep2)Session["RegisterStep2"];
            if (modelStep1 == null || modelStep2 == null)
                return View("ExpireSession");
           
                if (ModelState.IsValid)
                {
                    RegisterModelConfirm confirmModel = new RegisterModelConfirm()
                    {
                        UserName = modelStep2.UserName,
                        Email = modelStep1.Email,
                        FirstName =model!=null?  model.FirstName:string.Empty,
                        LastName = model!=null?model.LastName:string.Empty,
                        BirthDate = model!=null?model.BirthDate:string.Empty,
                        Gender = model!=null?model.Gender:string.Empty,
                        RegistrationNo = model!=null?model.RegistrationNo:string.Empty,
                        PhoneNumber = modelStep1.PhoneNumber,
                        Domain = model!=null?model.Domain:string.Empty,
                        Address = model!=null?model.Address:string.Empty,
                        OfficeNumber = model != null ? model.OfficeNumber : string.Empty,
                        inputFilePicture = modelStep2.fileName,
                        inputFileLisence = modelStep2.lisenceFileName,
                        AcceptRoles = true,
                    };

                    Session["RegisterConfirm3Steps"] = confirmModel;
                    return RegisterConfirm3StepsFinal(confirmModel, Request.QueryString["returnUrl"], Request.QueryString["requestedAction"], Request.QueryString["offerId"], Request.QueryString["comment"]);
                }
           

            return View();
        }


        public ActionResult RegisterConfirm3Steps()
        {
            var model = (RegisterModelConfirm)Session["RegisterConfirm3Steps"];
            if (model == null)
            {
                return RedirectToAction("Register");
            }
            else
            return View(model);
        }

        [HttpPost]
        public ActionResult RegisterConfirm3StepsFinal(RegisterModelConfirm model, string returnUrl, string requestedAction, string offerId, string comment)
        {
            RegisterModelStep1 modelStep1 = (RegisterModelStep1)Session["RegisterStep1"];
            RegisterModelStep2 modelStep2 = (RegisterModelStep2)Session["RegisterStep2"];

            if (modelStep1 != null)
            {
                var configManager = ConfigFactory.GetConfigManager(ConfigManagerType.WebConfig);


                string registerMode = (string)Request.QueryString[Offtick.Web.OfftickWeb.Infrustracture.Constants.RegistrationMode];

                string roleName = string.Empty;
                switch (registerMode)
                {

                    case Offtick.Web.OfftickWeb.Infrustracture.Constants.RegistrationMode_Company:
                        roleName = configManager.ReadSetting(Offtick.Web.OfftickWeb.Infrustracture.Constants.CompanyNameKey);
                        break;
                    case Offtick.Web.OfftickWeb.Infrustracture.Constants.RegistrationMode_Doctors:
                        roleName = configManager.ReadSetting(Offtick.Web.OfftickWeb.Infrustracture.Constants.DoctorNameKey);
                        break;
                    case Offtick.Web.OfftickWeb.Infrustracture.Constants.RegistrationMode_Resturant:
                        roleName = configManager.ReadSetting(Offtick.Web.OfftickWeb.Infrustracture.Constants.ResturantNameKey);
                        break;
                    case Offtick.Web.OfftickWeb.Infrustracture.Constants.RegistrationMode_Personal:
                    case null:
                    case "":
                        roleName = configManager.ReadSetting(Offtick.Web.OfftickWeb.Infrustracture.Constants.PersonalNameKey);
                        break;
                    default:
                        return View();

                }

                DateTime? dt = null;
                if (modelStep2!=null && !string.IsNullOrEmpty(model.BirthDate))
                    dt = PersianDateConvertor.ToGregorian(model.BirthDate);

                RegisterStatus result = MembershipManager.Instance.InitialRegisterMember(
                    model.UserName,
                    modelStep1.Password,
                    model.FirstName,
                    model.LastName,
                    modelStep2 != null ? model.Gender : string.Empty,
                    dt,
                    modelStep1.Email,
                    modelStep2 != null ? model.Domain : string.Empty,
                    modelStep2 != null ? model.Address : string.Empty,
                    roleName,
                    modelStep2 != null ? modelStep1.PhoneNumber : string.Empty, 
                    model!=null? model.inputFilePicture:null,
                    modelStep2.userId
                    );

                if (result == RegisterStatus.Success)
                {
                    if (modelStep1.CityId != default(Guid))
                    {
                        MembershipManager.Instance.SaveMapData(modelStep1.CityId, MembershipManager.Instance.GetUser(model.UserName));
                    }
                    if (roleName == configManager.ReadSetting(Offtick.Web.OfftickWeb.Infrustracture.Constants.DoctorNameKey) && modelStep2!=null)
                    {
                        MembershipManager.Instance.AddOrUpdateDoctorProfile(Guid.Parse( modelStep2.userId), model.RegistrationNo, model.OfficeNumber, model.Address, model.inputFileLisence);
                    }
                    System.Web.Security.FormsAuthentication.SetAuthCookie(model.UserName, true /* createPersistentCookie */);


                    if (!string.IsNullOrEmpty(requestedAction))
                    {
                        if (requestedAction == "comment")
                        {
                            var offerController = new Offtick.Web.OfftickWeb.Areas.User.Controllers.OfferController(Offtick.Business.Services.Storage.DataContextManager.Container);
                            offerController.AddCommentForOffer(offerId, model.UserName, comment);
                        }
                        if (requestedAction == "like")
                        {
                            var offerController = new Offtick.Web.OfftickWeb.Areas.User.Controllers.OfferController(Offtick.Business.Services.Storage.DataContextManager.Container);
                            offerController.LikeToggleForOffer(offerId, model.UserName);
                        }
                    }

                    //clear register sessions
                    Session.Remove("RegisterStep1");
                    Session.Remove("RegisterStep2");
                    Session.Remove("RegisterStep3");


                    //start of sending success register
                    string baySaleDate = Offtick.Core.Utility.PersianTools.PersianDateConvertor.ToKhorshidiDate(DateTime.Now);
                    string userMobile = model.PhoneNumber;
                    string welcomeSms = "";
                    if(string.IsNullOrEmpty( modelStep1.Email)){
                        welcomeSms = string.Format(Resources.UIMessages.SuccessRegisterWithoutEmailSmsNotification, model.UserName, modelStep1.Password);
                    }
                    else{
                        welcomeSms = string.Format(Resources.UIMessages.SuccessRegisterWithoutEmailSmsNotification, model.UserName, modelStep1.Password,model.Email);
                    }
                    // 
                    

                    Action actionSendSms = () =>
                    {
                        Offtick.Business.Services.Domain.SmsService.Instance.SendMagfaSms(userMobile, welcomeSms);
                    };
                    actionSendSms.BeginInvoke(null, null);
                    //end of sending success register

                    // if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/")& !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
                    if (!string.IsNullOrEmpty(returnUrl))
                    {
                        return Redirect(returnUrl);
                    }
                    else
                    {

                        return RedirectToAction("Wall", "Profile", new { area = "User" });
                    }
                    return RedirectToAction("Wall", "Profile", new { area = "User" });
                }

                // ModelState.AddModelError("", ErrorCodeToString(createStatus));

            }

            // If we got this far, something failed, redisplay form
            return View(Session["RegisterStep3"]);
        }

        [HttpPost]
        public ActionResult RegisterConfirm3Steps(RegisterModelConfirm model,string returnUrl, string requestedAction, string offerId,string comment)
        {
            bool acceptRoles = model != null && model.AcceptRoles;
            model = (RegisterModelConfirm)Session["RegisterConfirm3Steps"];

             if (model == null)
                 return RedirectToAction("Register");

             if (!acceptRoles)
            {
                ModelState.AddModelError("invalidAcceptRules", "به منظور تکمیل ثبت نام باید با قوانین موافقت خود را اعلام نمایید");
                return View(model);
            }


            if (model!=null && ModelState.IsValid && model.AcceptRoles)
            {
                return RegisterConfirm3StepsFinal(model, returnUrl, requestedAction, offerId, comment);

            }

            
            return View();
        }


        [HttpPost]
        public ActionResult Register(RegisterModel model,string returnUrl)
        {
            var configManager=ConfigFactory.GetConfigManager(ConfigManagerType.WebConfig);
            if (ModelState.IsValid)
            {


                string registerMode = (string)Request.QueryString[Offtick.Web.OfftickWeb.Infrustracture.Constants.RegistrationMode];
                
                string roleName = string.Empty;
                switch (registerMode)
                {
                
                    case Offtick.Web.OfftickWeb.Infrustracture.Constants.RegistrationMode_Company:
                        roleName = configManager.ReadSetting(Offtick.Web.OfftickWeb.Infrustracture.Constants.CompanyNameKey);
                        break;
                    case Offtick.Web.OfftickWeb.Infrustracture.Constants.RegistrationMode_Doctors:
                        roleName = configManager.ReadSetting(Offtick.Web.OfftickWeb.Infrustracture.Constants.DoctorNameKey);
                        break;
                    case  Offtick.Web.OfftickWeb.Infrustracture.Constants.RegistrationMode_Resturant:
                        roleName = configManager.ReadSetting(Offtick.Web.OfftickWeb.Infrustracture.Constants.ResturantNameKey);
                        break;
                    case Offtick.Web.OfftickWeb.Infrustracture.Constants.RegistrationMode_Personal:
                    case null:
                    case "":
                        roleName = configManager.ReadSetting(Offtick.Web.OfftickWeb.Infrustracture.Constants.PersonalNameKey);
                        break;
                    default:
                        return View(model);;

                }

                DateTime? dt=null;
                if (!string.IsNullOrEmpty(model.BirthDate))
                    dt = PersianDateConvertor.ToGregorian(model.BirthDate);
                Guid userId = Guid.NewGuid();
                RegisterStatus result = MembershipManager.Instance.InitialRegisterMember(
                    model.UserName,
                    model.Password,
                    model.FirstName,
                    string.Empty,
                    model.Gender,
                    dt,
                    model.Email,
                    model.Domain,
                    model.Address,
                    roleName,
                    model.PhoneNumber,
                    model.inputFilePicture,
                    userId.ToString());

                if (result == RegisterStatus.Success)
                {
                    if( roleName == configManager.ReadSetting(Offtick.Web.OfftickWeb.Infrustracture.Constants.DoctorNameKey))
                    {
                        MembershipManager.Instance.AddOrUpdateDoctorProfile(userId, model.RegistrationNo, model.OfficeNumber, model.Address, model.inputFileLisence);
                    }
                    System.Web.Security.FormsAuthentication.SetAuthCookie(model.UserName, false /* createPersistentCookie */);
                    if(string.IsNullOrEmpty(returnUrl))
                        return RedirectToAction("Wall", "Profile", new { area = "User" });
                    else
                        return Redirect(returnUrl);
                }
              
                else
                    ModelState.AddModelError("", ErrorCodeToString(result));
               
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }


     
        //
        // GET: /Account/ChangePassword

        [Authorize]
        public ActionResult ChangePassword()
        {
            return View();
        }

        //
        // POST: /Account/ChangePassword

        [Authorize]
        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordModel model)
        {
            if (ModelState.IsValid)
            {

                // ChangePassword will throw an exception rather
                // than return false in certain failure scenarios.
                LoginStatus changePasswordStatus;
                try
                {
                    var currentUser = MembershipManager.Instance.GetCurrentUser();
                    changePasswordStatus = MembershipManager.Instance.ChangePassword(model.OldPassword, model.NewPassword);
                }
                catch (Exception)
                {
                    changePasswordStatus =  LoginStatus.InactiveUser;
                }

                if (changePasswordStatus== LoginStatus.Success)
                {
                    return RedirectToAction("ChangePasswordSuccess");
                }
                else
                {
                    ModelState.AddModelError("", "The current password is incorrect or the new password is invalid.");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ChangePasswordSuccess

        public ActionResult ChangePasswordSuccess()
        {
            return View();
        }

        #region Status Codes
        private static string ErrorCodeToString(RegisterStatus createStatus)
        {
            switch (createStatus)
            {
                case RegisterStatus.ExitUserName:
                    return "User user name already exists. Please enter a different user name.";

                case RegisterStatus.ExitEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";
                
                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
        private static string ErrorCodeToString(System.Web.Security.MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case System.Web.Security.MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case System.Web.Security.MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case System.Web.Security.MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case System.Web.Security.MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case System.Web.Security.MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case System.Web.Security.MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case System.Web.Security.MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case System.Web.Security.MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case System.Web.Security.MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
        #endregion


        public ActionResult ForgotPassword()
        {
            return View();
        }
        
        [HttpPost]
        public ActionResult ForgotPassword(string method, string userValue)
        {
            if (string.IsNullOrEmpty(method))
                return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "Invalid Parameter", string.Empty, null, string.Empty));
            if (string.IsNullOrEmpty(userValue))
                return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "Invalid Parameter", string.Empty, null, string.Empty));
            MembershipUser user = null;
            switch (method)
            {
                case "UserName":
                    user=MembershipManager.Instance.GetUser(userValue);
                    if (user == null)
                        return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Format( UIMessages.NotExistUserByUserName,userValue), string.Empty, null, string.Empty));
                    break;
                case "Email":
                    user = MembershipManager.Instance.GetUserByEmail(userValue);
                    if (user == null)
                        return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Format( UIMessages.NotExistUserByEmail,userValue), string.Empty, null, string.Empty));
                    break;
                case "PhoneNumber":
                    user = MembershipManager.Instance.GetUserByPhoneNumber(userValue);
                    if (user == null)
                        return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, string.Format(UIMessages.NotExistUserByPhoneNumber, userValue), string.Empty, null, string.Empty));
                    break;
            }

            if (user != null)
            {
                string password = MembershipManager.Instance.getDecryptPassword(user.UserName);
                string bodyOfRecovery = string.Format("<div style=\"color:blue;\"><b>Password Recovery</b></div><p>your userName is: {0} and your password is: {1}</p>", user.UserName, password);
                method = method.Equals("UserName") ? (!string.IsNullOrEmpty(user.PhoneNumber) ? user.PhoneNumber : user.Email) : method;

                if (method.Equals("Email"))
                {
                    SMTPService smtp = new SMTPService();
                    smtp.SendMail(user.Email, "Password Recovery", bodyOfRecovery);
                    //all criteria has been passed so send an email to user that contain his/her password
                    return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, string.Format(UIMessages.SendRecoveryPasswordByEmail, userValue), null, string.Empty));
                }
                if (method.Equals("PhoneNumber"))
                {
                    /*Action actionSendSms = () =>
                     {
                         Offtick.Business.Services.Domain.SmsService.Instance.SendMagfaSms(user.PhoneNumber, bodyOfRecovery);
                     };
                    actionSendSms.BeginInvoke(null, null);*/
                    Offtick.Business.Services.Domain.SmsService.Instance.SendMagfaSms(user.PhoneNumber, bodyOfRecovery);
                    return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, string.Format(UIMessages.SendRecoveryPasswordByPhoneNumber, userValue), null, string.Empty));
                }
            }
            return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, UIMessages.RejectedByInternalSystemError, string.Empty, null, string.Empty));
        }
    }
}
