﻿using Offtick.Business.Membership;
using Offtick.Business.Web;
using Offtick.Data.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OfftickWeb.Controllers
{
    public class HomeController : BaseController
    {

        public HomeController(Offtick.Business.Services.Storage.DataContextContainer obj)
            : base(obj )
        {
        }


        public ActionResult Manifest()
        {
            
          // Response.ContentType = "text/cache-manifest";
            Response.ContentEncoding = System.Text.Encoding.UTF8;
           Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            return View();
        }
        
        public ActionResult Index2(string userName)
        {
            ViewBag.Message = string.IsNullOrEmpty(userName) ? "User Name" : userName;
            return View();
        }

        public ActionResult Index(string cityName)
        {
            ViewBag.cityName = cityName;
            return View();
        }

        public ActionResult Index_old(string userName)
        {
            ViewBag.Message = string.IsNullOrEmpty(userName)? "User Name":userName;
            return View();
        }


        public ActionResult About()
        {
            return View();
        }

		public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Privacy()
        {
            return View();
        }
        public ActionResult FAQ()
        {
            return View();
        }

        public ActionResult Agency()
        {
            return View();

        }
        public ActionResult Help()
        {
            return View();

        }

        [HttpGet]
        public ActionResult Compliant(string returnUrl)
        {
            return View();

        }


        [HttpPost]
        public ActionResult Compliant()
        {
            try
            {
                var currentUser = MembershipManager.Instance.GetCurrentUser();

                var length = Request.ContentLength;
                string title = string.Format("Compliant -  {0}", Request.Form["Title"]);
                string body = Request.Form["Feedback"];
                string name = currentUser != null ? string.Format("{0} {1}", currentUser.FirstName, currentUser.LastName) : Request.Form["Name"];
                string emailAddress = currentUser != null ? currentUser.Email : Request.Form["EmailAddress"];

                body = string.Format("<div>name:{0} , Email:{1}</div> <div> {2}</div>", name, emailAddress, body);
                SMTPService mailService = new SMTPService();
                System.IO.Stream stream = null;
                string fileName = string.Empty;
                if (Request.Files != null && Request.Files.Count != 0)
                {
                    stream = Request.Files[0].InputStream;
                    fileName = Request.Files[0].FileName;
                }

                mailService.SendMail("info@offTick.com", "info@offTick.com", title, body, stream, fileName);
                // mailService.SendMail("info@fypad.com", "amin_asr603@yahoo.com", title, body, stream, fileName);
                return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, "", "", "", ""));
            }
            catch
            {
                return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, "", "", "", ""));

            }
        }

        [AcceptVerbs("post")]
        public ActionResult Rate(FormCollection form)
        {
            var rate = Convert.ToInt32(form["Score"]);
            var id = Convert.ToInt32(form["ObjectID"]);
            if (Request.Cookies["rating" + id] != null)
                return Content("false");
            Response.Cookies["rating" + id].Value = DateTime.Now.ToString();
            Response.Cookies["rating" + id].Expires = DateTime.Now.AddYears(1);
            Offtick.Web.OfftickWeb.Models.StarRateModel ar = new Offtick.Web.OfftickWeb.Models.StarRateModel()
            {
                 AverageRating=2.5,
                  ObjectId=5,
                   Rating=3,
                    TotalRaters=5,
            };
            return Json(ar);
        }
    }
}
