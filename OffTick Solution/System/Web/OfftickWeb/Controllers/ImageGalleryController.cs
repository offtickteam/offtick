﻿using Offtick.Business.Services.Managers;
using Offtick.Business.Web;
using Offtick.Web.OfftickWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Offtick.Web.OfftickWeb.Controllers
{
    public class ImageGalleryController : BaseController
    {

        public ImageGalleryController(Offtick.Business.Services.Storage.DataContextContainer objectContext):base(objectContext)
        {
        }
        //
        // GET: /ImageGallery/

        public ActionResult Index(Guid garlleryId)
        {
            return View();
        }

        public ActionResult GalleriaViewer(string galleryId)
        {
            return PartialView("GalleriaViewer",(object)galleryId);
        }

        public ActionResult CarouselViewer(string galleryId) {
            Guid guidGalleryId;

                return PartialView("CarouselViewer",(object) galleryId);
            
        }
        public ActionResult CarouselViewer2(string galleryId)
        {
            Guid guidGalleryId;

            return PartialView("CarouselViewer2", (object)galleryId);

        }
        public ActionResult GalleriaImages(string galleryId)
        {
            if (!string.IsNullOrEmpty(galleryId))
            {
                var gallery = Offtick.Business.Services.Managers.GalleryManager.Instance.GetGallery(Guid.Parse(galleryId));
                if (gallery != null && gallery.GalleryImages != null)
                {
                    
                    Guid userId=gallery.MembershipUser.UserId;
                    var galleriaModel = (from img in gallery.GalleryImages
                                         select new GalleriaModel
                                         {
                                             description = img.Description,
                                             title = img.Title,
                                             imageId =img.ImageId,
                                             image = Offtick.Business.Services.Managers.GalleryManager.Instance.GetVirtualPathOfGalleryImage(img.ImageUrl, userId, false),
                                             thumb = Offtick.Business.Services.Managers.GalleryManager.Instance.GetVirtualPathOfGalleryImage(img.ImageUrl, userId, true),

                                         }).ToArray();

                    ImageGalleryViewerModel model = new ImageGalleryViewerModel()
                    {
                        GalleriaModels = galleriaModel,
                        GalleryId = gallery.GalleryId,
                        UserId = gallery.UserId,
                        UserName = gallery.MembershipUser.UserName,
                    };
                    return Json(model, JsonRequestBehavior.AllowGet);
                }
                
            }
            return null;
            
        }


       
    }
}
