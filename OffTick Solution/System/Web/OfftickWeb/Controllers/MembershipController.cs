﻿using Offtick.Business.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Offtick.Web.OfftickWeb.Controllers
{
    public class MembershipController : BaseController
    {
        //
        // GET: /Membership/
        public MembershipController(Offtick.Business.Services.Storage.DataContextContainer objectContext)
            : base(objectContext)
        {
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Show(string username)
        {
            ViewData["MembershipUserName"] = username;
            return View((object)username);
        }

        

        public ActionResult News(string userName)
        {
            return View();
        }
    }
}
