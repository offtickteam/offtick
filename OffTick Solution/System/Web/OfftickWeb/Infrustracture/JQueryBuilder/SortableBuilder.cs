﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Offtick.Web.OfftickWeb.Infrustracture.JQueryBuilder
{
    public class SortableBuilder
    {
        public static HtmlString GenerateSortable(IList<KeyValueItem> dataSource, string ulId, IList<string> liCssClass, IList<string> liSpanContentCssClass)
        {
            Func<IList<string>, string> getCssClassesOfList = (ls) =>
            {
                string allcssClasses = string.Empty;
                if (ls != null)
                    foreach (string css in ls)
                        allcssClasses = string.Format("{0} {1}", allcssClasses, css);
                return allcssClasses;
            };

            
            if (dataSource == null || dataSource.Count == 0)
                return new HtmlString(string.Empty);

            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            sb.AppendFormat("<ul id=\"{0}\">",ulId);
            foreach (var row in dataSource)
            {
                sb.AppendFormat("<li class=\"{0}\" id={1}><span class=\"{2}\"></span>{3}</li>", getCssClassesOfList(liCssClass),"Sort_"+ row.Key, getCssClassesOfList(liSpanContentCssClass), row.Value);
            }
            sb.Append("</ul>");
            return new HtmlString(sb.ToString());

        }
    }
}