﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Offtick.Web.OfftickWeb.Infrustracture
{
    public static class Constants
    {
        public  const string RegistrationMode = "RegistrationMode";
        public  const string RegistrationMode_Personal = "RegistrationMode_Personal";
        public  const string RegistrationMode_Company = "RegistrationMode_Company";
        public const string RegistrationMode_Resturant = "RegistrationMode_Resturant";

        public const string RegistrationMode_Doctors = "RegistrationMode_Doctor";


        public const string CompanyNameKey = "CompanyRoleKey";
        public const string PersonalNameKey = "PersonalRoleKey";
        public const string DoctorNameKey = "DoctorRoleKey";
        public const string ResturantNameKey = "ResturantRoleKey";
    }
}