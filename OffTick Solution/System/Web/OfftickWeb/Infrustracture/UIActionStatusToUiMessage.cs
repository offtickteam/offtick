﻿using Offtick.Core.EnumTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Offtick.Web.OfftickWeb.Infrustracture
{
    public class UIActionStatusToUiMessage
    {
        public static string ActionStatusToMessage(Enum status)
        {
            string msg = string.Empty;
            if (status.GetType() == typeof(EditStatus))
            {
                EditStatus editStatus =(EditStatus) status;
                switch (editStatus)
                {
                    case EditStatus.Edited:
                        msg = Offtick.Web.OfftickWeb.Resources.UIMessages.EditResultEdited;
                        break;
                    case EditStatus.RejectedByNotExit:
                        msg = Offtick.Web.OfftickWeb.Resources.UIMessages.RejectedByNotExit;
                        break;
                    case EditStatus.RejectedByAccessDenied:
                        msg = msg = Offtick.Web.OfftickWeb.Resources.UIMessages.RejectedByAccessDenied;
                        break;
                    case EditStatus.RejectedByInternalSystemError:
                        msg = msg = Offtick.Web.OfftickWeb.Resources.UIMessages.RejectedByInternalSystemError;
                        break;
                    case EditStatus.RejectedByNonEffectiveQuery:
                        msg = msg = Offtick.Web.OfftickWeb.Resources.UIMessages.RejectedByNonEffectiveQuery;
                        break;
                }
            }


            if (status.GetType() == typeof(DeleteStatus))
            {
                DeleteStatus editStatus = (DeleteStatus)status;
                switch (editStatus)
                {
                    case DeleteStatus.Deleted:
                        msg = Offtick.Web.OfftickWeb.Resources.UIMessages.DeleteResultDeleted;
                        break;
                    case DeleteStatus.RejectedByExistSubItems:
                        msg = Offtick.Web.OfftickWeb.Resources.UIMessages.DeleteResultRejectedByExistSubItems;
                        break;
                        
                    case DeleteStatus.RejectedByNotExit:
                        msg = Offtick.Web.OfftickWeb.Resources.UIMessages.RejectedByNotExit;
                        break;
                    case DeleteStatus.RejectedByAccessDenied:
                        msg = msg = Offtick.Web.OfftickWeb.Resources.UIMessages.RejectedByAccessDenied;
                        break;
                    case DeleteStatus.RejectedByInternalSystemError:
                        msg = msg = Offtick.Web.OfftickWeb.Resources.UIMessages.RejectedByInternalSystemError;
                        break;
                    case DeleteStatus.RejectedByNonEffectiveQuery:
                        msg = msg = Offtick.Web.OfftickWeb.Resources.UIMessages.RejectedByNonEffectiveQuery;
                        break;
                }
            }


            if (status.GetType() == typeof(MoveStatus))
            {
                MoveStatus editStatus = (MoveStatus)status;
                switch (editStatus)
                {
                    case MoveStatus.Moved:
                        msg = Offtick.Web.OfftickWeb.Resources.UIMessages.MoveResultMoved;
                        break;
                    case MoveStatus.RejectedByNotExitSource:
                        msg = Offtick.Web.OfftickWeb.Resources.UIMessages.MoveResultRejectedByNotExitSource;
                        break;
                    case MoveStatus.RejectedByNotExitDestination:
                        msg = Offtick.Web.OfftickWeb.Resources.UIMessages.MoveResultRejectedByNotExitDestination;
                        break;
                    case MoveStatus.RejectedByIsNotParentDestination:
                        msg = Offtick.Web.OfftickWeb.Resources.UIMessages.MoveResultRejectedByIsNotParentDestination;
                        break;


                    case MoveStatus.RejectedByAccessDenied:
                        msg = msg = Offtick.Web.OfftickWeb.Resources.UIMessages.RejectedByAccessDenied;
                        break;
                    case MoveStatus.RejectedByInternalSystemError:
                        msg = msg = Offtick.Web.OfftickWeb.Resources.UIMessages.RejectedByInternalSystemError;
                        break;
                    case MoveStatus.RejectedByNonEffectiveQuery:
                        msg = msg = Offtick.Web.OfftickWeb.Resources.UIMessages.RejectedByNonEffectiveQuery;
                        break;
                }
            }

            if (status.GetType() == typeof(AddStatus))
            {
                AddStatus editStatus = (AddStatus)status;
                switch (editStatus)
                {
                    case AddStatus.Added:
                        msg = Offtick.Web.OfftickWeb.Resources.UIMessages.AddStatuesAdded;
                        break;
                    case AddStatus.RejectedByExitBefore:
                        msg = Offtick.Web.OfftickWeb.Resources.UIMessages.AddStatusRejectedByExitBefore;
                        break;
                    case AddStatus.RejectedByParentNotExit:
                        msg = Offtick.Web.OfftickWeb.Resources.UIMessages.AddStatusRejectedByParentNotExit;
                        break;


                    case AddStatus.RejectedByAccessDenied:
                        msg = msg = Offtick.Web.OfftickWeb.Resources.UIMessages.RejectedByAccessDenied;
                        break;
                    case AddStatus.RejectedByInternalSystemError:
                        msg = msg = Offtick.Web.OfftickWeb.Resources.UIMessages.RejectedByInternalSystemError;
                        break;
                    case AddStatus.RejectedByNonEffectiveQuery:
                        msg = msg = Offtick.Web.OfftickWeb.Resources.UIMessages.RejectedByNonEffectiveQuery;
                        break;
                }
            }
            return msg;
        }
    }
}