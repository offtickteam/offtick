﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Offtick.Web.OfftickWeb.Infrustracture
{
    public class LangaueUIHelper
    {
        public static IList<System.Web.Mvc.SelectListItem> GetAllLangageList()
        {
            var langaues=Offtick.Business.Services.Managers.LangaugeManager.Instance.GetLangauges();
            IList<System.Web.Mvc.SelectListItem> ls = new List<System.Web.Mvc.SelectListItem>();
            if (langaues != null)
            {
                foreach (var langaue in langaues)
                {
                    ls.Add(new System.Web.Mvc.SelectListItem()
                    {
                         Text=langaue.TitleLocal,
                          Value=langaue.LanguageId.ToString(),
                    });
                }
            }
            return ls;
        }

        public static IList<System.Web.Mvc.SelectListItem> GetMemberAllLangageList(Guid userId)
        {
            var langaues = Offtick.Business.Services.Managers.LangaugeManager.Instance.GetMemberLangauges(userId);
            IList<System.Web.Mvc.SelectListItem> ls = new List<System.Web.Mvc.SelectListItem>();
            if (langaues != null)
            {
                foreach (var langaue in langaues)
                {
                    ls.Add(new System.Web.Mvc.SelectListItem()
                    {
                        Text = langaue.Language.TitleLocal,
                        Value = langaue.LanguageId.ToString(),
                        Selected=langaue.IsDefault,
                    });
                }
            }
            return ls;
        }
    }
}