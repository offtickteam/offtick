﻿using Offtick.Core.EnumTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Offtick.Web.OfftickWeb.Infrustracture
{
    public class GlobalizationManager
    {
        public static string GetMenuTypeCaptions(MenuType menuType)
        {
            string msg = string.Empty;
            switch (menuType)
            {
                case MenuType.ParentMenu:
                    msg= Resources.UICaptions.MenuTypeParent;
                    break;
                case MenuType.DirectLink:
                    msg= Resources.UICaptions.MenuTypeDirectLink;
                    break;
                case MenuType.ChildMenu:
                    msg = Resources.UICaptions.MenuTypeContent;
                    break;
            }

            return msg;
        }
    }
}