﻿using Offtick.Core.EnumTypes;
using Offtick.Core.Utility;
using Offtick.Core.Utility.PersianTools;
using Offtick.Data.Context.ExpertOnlinerContexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Offtick.Web.OfftickWeb.Infrustracture
{
    public class GeneralHelper
    {
        public static string GetThumbnail256Of(Guid userId, string fileName)
        {
            string url = string.Empty;
            if (string.IsNullOrEmpty(fileName))
            {
                url = "/Content/images/user-avatar.png";
            }
            else
            {
                var fileManager = new Offtick.Business.Services.Storage.File.ImageFileManager(userId.ToString());
                url = fileManager.GetRelativeFullFileName(userId.ToString(), fileName,Offtick.Business.Services.Storage.File.ImageFileManager.ImageState.Thumbnail256);
            }
            return url;
        }
        public static string GetThumbnail256Of(MembershipUser user)
        {
            if (user == null)
                return string.Empty;
            else
                return GetThumbnail256Of(user.UserId, user.Picture);
        }

        public static string GetThumbnail64Of(Guid userId, string fileName)
        {
            string url = string.Empty;
            if (string.IsNullOrEmpty(fileName))
            {
                url = "/Content/images/user-avatar.png";
            }
            else
            {
                var fileManager = new Offtick.Business.Services.Storage.File.ImageFileManager(userId.ToString());
                url = fileManager.GetRelativeFullFileName(userId.ToString(), fileName, Offtick.Business.Services.Storage.File.ImageFileManager.ImageState.Thumbnail64);
            }
            return url;
        }
        public static string GetThumbnail32Of(Guid userId, string fileName)
        {
            string url = string.Empty;
            if (string.IsNullOrEmpty(fileName))
            {
                url = "/Content/images/user-avatar32.png";
            }
            else
            {
                var fileManager = new Offtick.Business.Services.Storage.File.ImageFileManager(userId.ToString());
                url = fileManager.GetRelativeFullFileName(userId.ToString(), fileName, Offtick.Business.Services.Storage.File.ImageFileManager.ImageState.Thumbnail32);
            }
            return url;
        }
        public static string GetThumbnail64Of(MembershipUser user)
        {
            if (user == null)
                return string.Empty;
            else
                return GetThumbnail64Of(user.UserId, user.Picture);
        }

        public static string GetThumbnailOf(Guid userId, string fileName)
        {
            string url = string.Empty;
            if (string.IsNullOrEmpty(fileName))
            {
                url = "/Content/images/user-avatar.png";
            }
            else
            {
                var fileManager = new Offtick.Business.Services.Storage.File.ImageFileManager(userId.ToString());
                url = fileManager.GetRelativeFullFileName(userId.ToString(), fileName, true);
            }
            return url;
        }

        public static string GetThumbnailOf(MembershipUser user)
        {
            if (user == null)
                return string.Empty;
            else
                return GetThumbnailOf(user.UserId, user.Picture);
        }

        public static string GetUserNameOfCurrentUser()
        {
            return Offtick.Business.Membership.MembershipManager.Instance.GetCurrentUserName();
        }
        public static MembershipUser GetCurrentUser()
        {
            return Offtick.Business.Membership.MembershipManager.Instance.GetCurrentUser();
        }


        public static string GetProfileUrlOfCurrentUser()
        {
            UrlHelper url = new UrlHelper(System.Web.HttpContext.Current.Request.RequestContext);
            return url.Action("Index", "Profile", new { area = "User" });
            
        }

        public static string GetStringExpireTime(DateTime registerDt, int? hour)
        {
           
            if(!hour.HasValue)
                return @"";
            DateTime dt= registerDt.AddHours(hour.Value);
            var timeSpan = dt.Subtract(DateTime.Now);
            if (timeSpan.TotalDays > 365)
            {
                int years = (int)timeSpan.TotalDays / 365 ;
                int months = (int)(timeSpan.TotalDays - years * 365) / 30;
                //"5 سال و 4 ماه"
                return string.Format("{0} {1} {2} {3}",years, "سال و",months,"ماه"  );
            }
            if (timeSpan.TotalDays >= 31)
            {
                int months = (int)timeSpan.TotalDays / 30;
                int days = (int)(timeSpan.TotalDays % 30);
                return string.Format("{0} {1} {2} {3}",months,  "ماه و",days,"روز");
            }
            if (timeSpan.TotalHours >= 24)
            {
                int days = (int)timeSpan.TotalHours / 24;
                int hours = (int)(timeSpan.TotalHours % 24);
                return string.Format("{0} {1} {2} {3}",days,"روز و",  hours,"ساعت"  );
            }
            if (timeSpan.TotalMinutes >= 60)
            {
                int hours = (int)timeSpan.TotalMinutes / 60;
                int minutes = (int)(timeSpan.TotalMinutes % 60);
                return string.Format("{0} {1} {2} {3}", hours, "ساعت و", minutes,"دقیقه" );
            }

            if (timeSpan.TotalMinutes >= 0)
            {
                int minutes = (int)timeSpan.TotalMinutes ;
                int seconds = (int)(timeSpan.TotalSeconds % 60);
                return string.Format("{3} {2} {1} {0}", minutes, "دقیقه و", seconds, "ثانیه");
            }

            return "منقضی";

            
        }

        public static string GetStringPastFormOfTime(DateTime registerDt)
        {
            var timeSpan = DateTime.Now.Subtract(registerDt);
            if (timeSpan.TotalDays > 365)
            {
                int years = (int)timeSpan.TotalDays / 365;
                int months = (int)(timeSpan.TotalDays - years * 365) / 30;
                //"5 سال و 4 ماه"
                return string.Format("{0} {1}", years, "سال قبل");
            }
            if (timeSpan.TotalDays >= 31)
            {
                int months = (int)timeSpan.TotalDays / 30;
                int days = (int)(timeSpan.TotalDays % 30);
                return string.Format("{0} {1}", months, "ماه قبل");
            }
            if (timeSpan.TotalHours >= 24)
            {
                int days = (int)timeSpan.TotalHours / 24;
                int hours = (int)(timeSpan.TotalHours % 24);
                return string.Format("{0} {1}", days, "روز قبل");
            }
            if (timeSpan.TotalMinutes >= 60)
            {
                int hours = (int)timeSpan.TotalMinutes / 60;
                int minutes = (int)(timeSpan.TotalMinutes % 60);
                return string.Format("{0} {1}", hours, "ساعت قبل");
            }

            if (timeSpan.TotalMinutes >= 0)
            {
                int minutes = (int)timeSpan.TotalMinutes;
                int seconds = (int)(timeSpan.TotalSeconds % 60);
                return string.Format("{0} {1}", minutes, "دقیقه قبل");
            }

            return "تاریخ نامشخص";


        }

        public static string GetPersianDate(DateTime dateTime)
        {
            return PersianDateConvertor.ToKhorshidiDate(dateTime);
        }
        public static string GetPersianDateTime(DateTime dateTime)
        {
            return PersianDateConvertor.ToKhorshidiDateTime(dateTime);
        }
        public static string GetPersianDateTimeWithOutSecond(DateTime dateTime)
        {
            return PersianDateConvertor.ToKhorshidiDateTime(dateTime,false);
        }

        public static string GetOfferIncomeValue(Offer offer)
        {
            var foodOffer = offer.OfferFoods.FirstOrDefault();
            if (foodOffer == null)
                return string.Empty;
            if (foodOffer.ShowIncomeInPercentage)
                return string.Format("{0} %",foodOffer.DiscountPercent);
            else return (foodOffer.OriginalPrice - foodOffer.DiscountPrice).ToString();
        }
        public static string GetOfferIncomeTitle(Offer offer)
        {
            var foodOffer = offer.OfferFoods.FirstOrDefault();
            if (foodOffer == null || foodOffer.ShowIncomeInPercentage)
                return string.Empty;
             return foodOffer.DiscountPriceUnit;
        }


        public static string GetRoleName(MembershipUser user)
        {
            if (user == null)
                return string.Empty;
            var role = user.MembershipUsersInRoles.FirstOrDefault();
            string roleName = role != null ? role.MembershipRole.RoleName : string.Empty;
            return roleName;
        }

        public static bool isAuthorizeForPlusAction(MembershipUser user)
        {
            string roleName = GetRoleName(user);
            if (roleName == "Resturant" || roleName == "Company")
                return true;
            else
                return false;
        }

        public static string GetPresentationNameOfUser(MembershipUser user)
        {
            if (user == null)
                return string.Empty;

             string roleName = GetRoleName(user);
             if (roleName == "Resturant" || roleName == "Company")
                 return user.FirstName;
             else

                 return string.Format("{0} {1}", user.LastName, user.FirstName);
        }


        public static IList<SelectListItem> getCityDropdownDataSource(string selectedCityName="",bool allCities=true)
        {
            IList<SelectListItem> results = new List<SelectListItem>();
            var cities= Offtick.Business.Membership.MembershipManager.Instance.GetCities().OrderBy(e=>e.CityName).ToList();
            if (allCities)
            {
                results.Add(new SelectListItem()
                {
                    Text = "همه شهرها",
                    Value = "all"
                });
            }
            foreach (var city in cities)
            {
                results.Add(new SelectListItem()
                {
                     Text=city.CityName,
                     Value=city.CityId.ToString(),
                     Selected = string.IsNullOrWhiteSpace(selectedCityName)? false: city.CityName.Equals(selectedCityName.Trim())
                });
            }
            return results;
        }

        public static IList<SelectListItem> getTravelTypeDropdownDataSource( bool all = true)
        {
            IList<SelectListItem> results = new List<SelectListItem>();

            if (all)
            {
                results.Add(new SelectListItem()
                {
                    Text = "همه موارد",
                    Value = "10"
                });
            }
            
                results.Add(new SelectListItem()
                {
                    Text = "هوایی",
                    Value = ((int)TravelTypeEnum.Air).ToString(),
                    Selected =true
                });
                results.Add(new SelectListItem()
                {
                    Text = "زمینی",
                    Value = ((int)TravelTypeEnum.Land).ToString(),
                });
                results.Add(new SelectListItem()
                {
                    Text = "قطار",
                    Value = ((int)TravelTypeEnum.Train).ToString(),
                });
                results.Add(new SelectListItem()
                {
                    Text = "دریایی",
                    Value = ((int)TravelTypeEnum.Sea).ToString(),
                });
            
            return results;
        }



        #region ApplicationMajarSelect
        public static IList<SelectListItem> getApplicationSelectMajorHighSchoolMajorDropdownDataSource(string selectedItem = "")
        {
            IList<SelectListItem> results = new List<SelectListItem>();
            var entities = Offtick.Business.Services.Managers.ApplicationManager.Instance.GetHighSchoolMajors();

            foreach (var entity in entities)
            {
                results.Add(new SelectListItem()
                {
                    Text = entity.HighSchoolMajarName,
                    Value = entity.ApplicationMajorChoiceHighSchoolId.ToString(),
                    Selected = string.IsNullOrWhiteSpace(selectedItem) ? false : entity.HighSchoolMajarName.Equals(selectedItem.Trim())
                });
            }
            return results;
        }

        public static IList<SelectListItem> getApplicationSelectMajorUniversitiesDropdownDataSource(int highSchoolMajorId, string selectedItem = "")
        {
            IList<SelectListItem> results = new List<SelectListItem>();
            var entities = Offtick.Business.Services.Managers.ApplicationManager.Instance.GetUniversities(highSchoolMajorId);

            foreach (var entity in entities)
            {
                results.Add(new SelectListItem()
                {
                    Text = entity.UniversityName,
                    Value = entity.ApplicationMajorChoiceUniversityId.ToString(),
                    Selected = string.IsNullOrWhiteSpace(selectedItem) ? false : entity.UniversityName.Equals(selectedItem.Trim())
                });
            }
            return results;
        }

        public static IList<SelectListItem> getApplicationSelectMajorUniversityCitiesDropdownDataSource(int highSchoolMajorId, string selectedItem = "")
        {
            IList<SelectListItem> results = new List<SelectListItem>();
            var entities = Offtick.Business.Services.Managers.ApplicationManager.Instance.GetUniversityCities(highSchoolMajorId);

            foreach (var entity in entities)
            {
                results.Add(new SelectListItem()
                {
                    Text = entity.CityName,
                    Value = entity.ApplicationMajorChoiceCityId.ToString(),
                    Selected = string.IsNullOrWhiteSpace(selectedItem) ? false : entity.CityName.Equals(selectedItem.Trim())
                });
            }
            return results;
        }

        public static IList<SelectListItem> getApplicationSelectMajorUniversityMajorsDropdownDataSource(int highSchoolMajorId, string selectedItem = "")
        {
            IList<SelectListItem> results = new List<SelectListItem>();
            var entities = Offtick.Business.Services.Managers.ApplicationManager.Instance.GetUniversityMajors(highSchoolMajorId);

            foreach (var entity in entities)
            {
                results.Add(new SelectListItem()
                {
                    Text = entity.MajorName,
                    Value = entity.ApplicationMajorChoiceUniversityMajorId.ToString(),
                    Selected = string.IsNullOrWhiteSpace(selectedItem) ? false : entity.MajorName.Equals(selectedItem.Trim())
                });
            }
            return results;
        }

        #endregion
        



        public static Guid? getCityIdByName(string cityName)
        {
            var cities = Offtick.Business.Membership.MembershipManager.Instance.GetCities();
            var city= cities.FirstOrDefault(e=>e.CityName.Equals(cityName));
            return city!=null? (Guid?)city.CityId:null;

        }

        public static bool IsFullSize(System.Web.Mvc.WebViewPage page)
        {
            bool? isFullSize= page.ViewBag.fullSize;
            return isFullSize.HasValue ? isFullSize.Value : false;
        }
        public static void setFullSize(System.Web.Mvc.WebViewPage page, bool isFullSize)
        {
             page.ViewBag.fullSize=isFullSize;
        }

        public static string GetPresentationCount(long count)
        {
            if (count < 1000)
                return count.ToString();
            if (count >= 1000 && count < 1000000)
                return ((int)(count / 1000)).ToString() + "K";
            if (count >= 1000000 && count < 1000000000)
                return ((int)(count / 1000000)).ToString() + "M";

            return ((int)(count / 1000000000)).ToString() + "G";//count >= 1000000000
        }

        public static void SetOtherUserName(System.Web.Mvc.WebViewPage page, string otherUserName)
        {
            page.TempData["OtherUserName"] = otherUserName;
        }
        public static string GetOtherUserName(System.Web.Mvc.WebViewPage page)
        {
            return (string)page.TempData["OtherUserName"];
        }

        public static void SetHasMembershipHeader(System.Web.Mvc.WebViewPage page, bool hasMembershipHeader)
        {
            page.TempData["hasMembershipHeader"] = hasMembershipHeader;
        }

        public static bool GetHasMembershipHeader(System.Web.Mvc.WebViewPage page)
        {
            object hasMembershipHeader=page.TempData["hasMembershipHeader"] ;
            return hasMembershipHeader == null ? true : (bool)hasMembershipHeader;
        }

        public static string GetDateTimeToString(DateTime datetime){
            return datetime.ToString("yyyyMMddHHmmss");
        }



        #region config
        public static string ReadSetting(string setting)
        {
            var configManager = ConfigFactory.GetConfigManager(ConfigManagerType.WebConfig);
            return configManager.ReadSetting(setting);
        }

        public static bool IsFakePayment()
        {
            string boolStr = ReadSetting("IsFakePayment");
            bool result=false;
            Boolean.TryParse(boolStr, out result);
            return result;
        }

        public static bool isConfirmedPersonalOfferOnInsert()
        {
            string boolStr = ReadSetting("offerConfirmStatusPersonal");
            bool result = false;
            Boolean.TryParse(boolStr, out result);
            return result;
        }
        public static bool isConfirmedCompanyOfferOnInsert()
        {
            string boolStr = ReadSetting("offerConfirmStatusCompany");
            bool result = false;
            Boolean.TryParse(boolStr, out result);
            return result;
        }
        public static short GetTaxValueForCurrentYear()
        {
            string boolStr = ReadSetting("taxValue");
            short result = 9;
            short.TryParse(boolStr, out result);
            return result;
        }

        #endregion






    }
}