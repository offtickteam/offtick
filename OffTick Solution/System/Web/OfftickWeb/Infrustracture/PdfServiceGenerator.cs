﻿using Offtick.Business.Services.Domain.PdfGenerator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Offtick.Web.OfftickWeb.Infrustracture
{
    public class PdfServiceGenerator
    {
        public static PdfService CreateDefaultPdfService()
        {
            PdfService pdfService = new PdfService(
                 Resources.UICaptions.PDFRowNoCaption
                 , Resources.UICaptions.PDFItemCodeCaption
                 , Resources.UICaptions.PDFNameCaption
                 , Resources.UICaptions.PDFCountCaption
                 , Resources.UICaptions.PDFItemPriceCaption
                 , Resources.UICaptions.PDFTotalPriceCaption
                 , Resources.UICaptions.PDFDiscountPriceCaption
                 , Resources.UICaptions.PDFTotalPriceAfterDiscountCaption
                 , Resources.UICaptions.PDFTaxesCaption
                 , Resources.UICaptions.PDFTotalPriceAfterDiscountAndTaxesCaption
                );
            return pdfService;
        }
    }
}