﻿var maxFileSizeInKiloByte = 1000;

String.prototype.format = function () {
    var s = this,
        i = arguments.length;

    while (i--) {
        s = s.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i]);
    }
    return s;
};



(function ($) {
    $.fn.onEnter = function (func) {
        this.bind('keypress', function (e) {
            if (e.keyCode == 13) func.apply(this, [e]);
        });
        return this;
    };
})(jQuery);

$(function () {
    $(document).on('keypress', '.txtComment,.txtCommentEdit', function (e) {
        if (e.keyCode == 13) {
            $(this).trigger("onEnter");
        }
    });

});








$(function () {
    initializeMoreButtoms();
    $(".submit_link").click(function () {
            $(this).closest("form").submit();
        });
        $(".fillLater_link").click(function () {
            $("#FillLater").val(true);
            $(this).closest("form").submit();
        });


    $("#btnChangePassword").click(function () {

        var _userNameOrEmailOrPhonenumber = $("#txtForgotPassword").val();
        var _selectedMethodForRecoveryPassword = $("#chkForgotPasswordMethod").val();
        if (!_userNameOrEmailOrPhonenumber) {
            $("#txtMsgChangePassword").html("مقدار وارد شده در کادر بازیابی کلمه عبور نمیتواند خالی باشد.");
            return;
        }
        
        var postData = { method: _selectedMethodForRecoveryPassword, userValue: _userNameOrEmailOrPhonenumber };
        $("#txtMsgChangePassword").html("loading...");

        var urlOfAction = $(this).data('url');
        $.ajax({
            type: 'POST',
            url: urlOfAction,
            data: JSON.stringify(postData),
            dataType: 'json',
            processData: false,
            contentType: 'application/json; charset:utf-8',
            success: function (data, status) {
                if (status == "success") {
                    $("#txtMsgChangePassword").removeClass();
                    if (data) {
                        if (data.ResultStatus == "Success") {
                            $("#txtMsgChangePassword").addClass("text-success");
                            $("#txtMsgChangePassword").html(data.Message);
                        }
                        if (data.ResultStatus == "Failed") {
                            $("#txtMsgChangePassword").addClass("text-danger");
                            $("#txtMsgChangePassword").html(data.Error);
                        }
                    }
                }
            },
            error: function (xhr, textStatus, error) {
                var errorMessage = error || xhr.statusText;
                $("#txtMsgChangePassword").removeClass();
                $("#txtMsgChangePassword").addClass("text-danger");
                $("#txtMsgChangePassword").html(errorMessage);
            }
        });
    });


    if (typeof $.fn.sliderPro !== 'undefined') {

        $('.slider-pro').sliderPro({
            width: '100%',
            height: '400',
            arrows: true,
            buttons: false,
            waitForLayers: true,
            fade: true,
            autoplay: true,
            autoScaleLayers: true,
            loop: true,
            fullScreen: true,
            breakpoints: {
                600: {
                    height: '240'
                }

            }
        });
    }
    //if (typeof $.fn.gallery !== 'undefined') {

    //    $('#dg-container').gallery({
    //        current: 0,
    //        autoplay: true,
    //        interval: 2000
    //    });

    //}

});

function initializeMoreButtoms(){
    //this coindition checks if content of offer in wall page bellow that 10, then hide show more buttons
    $(".more-contents").each(function (e) {
        
        if ($("#" + $(this).data("appendcontrol")).children("div").length ==0) {
            if ($("#" + $(this).data("emptycontrol")))
                $("#" + $(this).data("emptycontrol")).show();
        }
        if ($("#" + $(this).data("appendcontrol")).children("div").length < 10) {
            $(this).toggle(false);
        }
    });

    $(".load-more-button").each(function (e) {
        if ($("#" + $(this).data("appendcontrol")).children("article").length ==0) {
            if ($("#" + $(this).data("emptycontrol")))
                $("#" + $(this).data("emptycontrol")).show();
        }
        if ($("#" + $(this).data("appendcontrol")).children("article").length < 9) {
            $(this).toggle(false);
        }
    });
}



function postToPgwSite(refIdValue,pgwSite) {
    var form = document.createElement("form");
    form.setAttribute("method", "POST");
    form.setAttribute("action", pgwSite);
    form.setAttribute("target", "_self");
    var hiddenField = document.createElement("input");
    hiddenField.setAttribute("name", "RefId");
    hiddenField.setAttribute("value", refIdValue);
    form.appendChild(hiddenField);
    document.body.appendChild(form);
    form.submit();
    document.body.removeChild(form);
}