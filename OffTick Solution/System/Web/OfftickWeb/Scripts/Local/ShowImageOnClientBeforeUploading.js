﻿function imageBeforeUploading(fileInputId,imgElementId,resetImages)
{
    $(document).ready(function () {
        var fileInputElement = $("#"+fileInputId);
        fileInputElement.change(function (e) {

            for (var i = 0; i < e.originalEvent.srcElement.files.length; i++) {
                var file = e.originalEvent.srcElement.files[i];
                var img = $("#" + imgElementId)[0];

                if (file.size / 1000 < maxFileSizeInKiloByte) {
                    var reader = new FileReader();
                    reader.onloadend = function () {
                        img.src = reader.result;
                    }
                    reader.readAsDataURL(file);
                    fileInputElement.after(img);
                }
                else {
                    alert("حجم فایل ارسالی باید کمتر از یک مگابایت باشد.");
                    e.preventDefault();
                    if(resetImages==true)
                        img.src = "/Content/Images/user-image-placeholder.png";
                    

                    $(fileInputElement).val("");
                    
                }
            }
        });
    });
}