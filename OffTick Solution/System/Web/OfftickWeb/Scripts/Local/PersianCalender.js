﻿

var currentYear = 1391; var currentMonth = 11; var currentDay = 9; var selectedYear = 1391; var selectedMonth = 11; var selectedDay = 9; var thisYear = 1391; var thisMonth = 11; var thisDay = 9;

 var monthName = ['فروردین', 'اردیبهشت', 'خرداد', 'تیر', 'مرداد', 'شهریور', 'مهر', 'آبان', 'آذر', 'دی', 'بهمن', 'اسفند']; var weekDayName = ['شنبه', 'یکشنبه', 'دوشنبه', 'سه شنبه', 'چهارشنبه', 'پنجشنبه', 'جمعه']; var forbidenDays = []; var forbidenDates = []; var dayWidth = 50; var dayHeight = 18; var minAcceptedYear = 1300; var maxAcceptedYear = 1400; var pickerCSS = 'PickerCSS'; var headerCSS = 'PickerHeaderCSS'; var footerCSS = 'PickerFooterCSS'; var calendarCSS = 'PickerCalendarCSS'; var weekDayCSS = 'PickerWeekDayCSS'; var workDayCSS = 'PickerWorkDayCSS'; var forbidenCSS = 'PickerForbidenCSS'; var selectedCSS = 'PickerSelectedCSS'; var isPickerVisible = false; var dateBox; var datePickerFrame;

 function initializeDate(eCurrentYear, eCurrentMonth, eCurrentDay, eSelectedYear, eSelectedMonth, eSelectedDay, eThisYear, eThisMonth, eThisDay) {
     currentYear = eCurrentYear;
     currentMonth = eCurrentMonth;
     currentDay = eCurrentDay;
     selectedYear = eSelectedYear;
     selectedMonth = eSelectedMonth;
     selectedDay = eSelectedDay;
     thisYear = eThisYear;
     thisMonth = eThisMonth;
     thisDay = eThisDay;
 }
    function ShowDatePicker(dateBoxID, datePickerFrameID)

    {

        if (isPickerVisible)
        {
        
            HideDatePicker();

            return;
            
        }

        dateBox = document.getElementById(dateBoxID);
        
        datePickerFrame = document.getElementById(datePickerFrameID);

        if (dateBox.getAttribute("usedatepicker") == 'false') 

			return;

        if (IsDateValid(dateBox.value))

            ChangeDate(parseInt(dateBox.value.substring(0, 4)), parseInt((dateBox.value.substring(5, 6) != '0' ? dateBox.value.substring(5, 7) : dateBox.value.substring(6, 7)), 10), parseInt((dateBox.value.substring(8, 9) != '0' ? dateBox.value.substring(8, 10) : dateBox.value.substring(9, 10)), 10));

        else

            ChangeDate(thisYear, thisMonth, thisDay);

        isPickerVisible = true;

        datePickerFrame.style.width = dayWidth * 7;

        datePickerFrame.style.visibility = 'visible';

        RenderDatePicker();

    }

    function IsDateValid(currentDate)

    {

        if (currentDate.length != 10)

            return false;

        for (var index = 0; index < currentDate.length; index++)

            if ((currentDate.substring(index, index + 1) != '/' && (index == 4 || index == 7)) || ((index != 4 && index != 7) && isNaN(parseInt(currentDate.substring(index, index + 1)))))

                return false;

        var year = (currentDate.substring(0, 4));

        var month = (currentDate.substring(5, 7));

        var day = (currentDate.substring(8, 10));

        if ((year < 1000) || (year > 9999))

            return false;

        if ((month < 1) || (month > 12))

            return false;

        if ((day < 1) || (day > 31))

            return false;

        if ((month > 6) && (day > 30))

            return false;

        if ((!IsLeap(year)) && (month == 12) && (day == 30))

            return false;

        return true;

    }

    function RenderDatePicker()

    {

        var footerHTML;

        var headerHTML;

        var calendarHTML;

        headerHTML = '<table style="width:100%;" class="' + headerCSS + '"><tr>';

        headerHTML += '<td style="width:10px;"><a class="' + headerCSS + '" title="سال بعد" href="javascript:ChangeYear(+1);">«</a></td>';

        headerHTML += '<td style="width:10px;"><a class="' + headerCSS + '" title="ماه بعد" href="javascript:ChangeMonth(+1);">‹</a></td>';

        headerHTML += '<td><input type="text" title="سال جاری" size="4" maxlength="4" onkeydown="if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105) && (event.keyCode != 8)) {event.returnValue = false; event.cancel = true;} if (event.keyCode == 13) ChangeYear(this.value - currentYear);" onchange="this.value = currentYear;" value="' + currentYear + '"/> <select title="ماه جاری" onchange="currentMonth = this.selectedIndex + 1; RenderDatePicker();">' + LoadMonthOptions() + '</select></td>';

        headerHTML += '<td style="width:10px;"><a class="' + headerCSS + '" title="ماه قبل" href="javascript:ChangeMonth(-1);">›</a></td>';

        headerHTML += '<td style="width:10px;"><a class="' + headerCSS + '" title="سال قبل" href="javascript:ChangeYear(-1);">»</a></td>';

        headerHTML += '</tr></table>';

        footerHTML = '<table style="width:100%;" class="' + footerCSS + '"><tr>';

        footerHTML += '<td><a class="' + footerCSS + '" title="انتخاب روز جاری" href="javascript:ChangeSlectedDate(' + thisYear + ', ' + thisMonth + ', ' + thisDay + ');">امروز ' + thisDay + ' ' + monthName[thisMonth - 1] + ' ' + thisYear + '</a></td>';

        footerHTML += '</tr></table>';

        calendarHTML = '<table cellpadding="0" cellspacing="0">' + RenderCalendar() + '</table>';

        datePickerFrame.innerHTML = '<iframe src="" scrolling="no" frameborder="0" style="position: absolute; width: 100%; height:' + (dateBox.alwaysOnTop == 'true' ? dayHeight * 10 : 0) + 'px; border: none; display: block; z-index: 0;"></iframe><div style="position: absolute; z-index: 0"><table style="direction:ltr; text-align: center; background-color: #000000;" width="' + dayWidth * 7 + '" class="' + pickerCSS + '"><tr><td>' + headerHTML + '</td></tr><tr><td>' + calendarHTML + '</td></tr><tr><td>' + footerHTML + '</td></tr></table></div>';

    }

    function RenderCalendar()

    {

        var result = '<tr>';

        var weekResult = '';

        var monthDayIndex = 0;

        var monthStartDay = GetMonthStartDay(currentYear, currentMonth);

        var monthDays = GetMonthDays(currentYear, currentMonth);

        var isForbiden;

        for (dayIndex = 0; dayIndex < 7; dayIndex++)

            weekResult = '<td class="' + weekDayCSS + '"><span>' + weekDayName[dayIndex] + '</span></td>' + weekResult;

        result += weekResult + '</tr>';        

        for (weekIndex = 0; weekIndex < 6; weekIndex++)

        {

            result += '<tr>';

            weekResult = '';

            for (dayIndex = 0; dayIndex < 7; dayIndex++)

            {

                if (monthDayIndex == 0)

                {

                    if (monthStartDay == dayIndex)

                        monthDayIndex++;

                }

                else

                    monthDayIndex++;

                if (monthDayIndex == 0 || monthDayIndex > monthDays)

                    weekResult = '<td class="' + calendarCSS + '" style="width:' + dayWidth + 'px;height:' + dayHeight + 'px;" >&nbsp;</td>' + weekResult;

                else

                {

                    isForbiden = IsForbiden(currentYear, currentMonth, monthDayIndex, dayIndex);

                    if (isForbiden)

                        weekResult = '<td class="' + forbidenCSS + '" style="width:' + dayWidth + 'px;height:' + dayHeight + 'px;">' + '<span title="' + GetStringDate(currentYear, currentMonth, monthDayIndex) + '" ' + '>' + monthDayIndex + '</span>' + '</td>' + weekResult;

                    else

                        weekResult = '<td class="' + (currentYear == selectedYear && currentMonth == selectedMonth && monthDayIndex == selectedDay ? selectedCSS : workDayCSS) + '" style="width:' + dayWidth + 'px;height:' + dayHeight + 'px;">' + (monthDayIndex <= monthDays && monthDayIndex != 0 ? '<a class="' + (currentYear == selectedYear && currentMonth == selectedMonth && monthDayIndex == selectedDay ? selectedCSS : workDayCSS) + '" title="' + GetStringDate(currentYear, currentMonth, monthDayIndex) + '" href="javascript:ChangeSlectedDate(' + currentYear + ', ' + currentMonth + ', ' + monthDayIndex + ');" >' + monthDayIndex + '</a>' : '') + '</td>' + weekResult;

                }

            }

            result += weekResult + '</tr>';

            if (monthDayIndex >= monthDays)

                weekIndex++;

        }

        return result;

    }

    function IsForbiden(year, month, day, weekDay)

    {

        for (var index = 0; index < forbidenDays.length; index++)

            if (forbidenDays[index] == weekDay)

                return true;

        for (index = 0; index < forbidenDates.length; index++)

            if (forbidenDates[index][0] == year || forbidenDates[index][0] == 0)

                if (forbidenDates[index][1] == month || forbidenDates[index][1] == 0)

                    if (forbidenDates[index][2] == day || forbidenDates[index][2] == 0)

                        return true;

        return false;

    }

    function GetMonthStartDay(year, month) 

    {

        var yearStartDay = 4;

        var yearIndex = year % 231;

        for (index = 1; index <= yearIndex; index++)

            yearStartDay += (IsLeap(index - 1) ? 2 : 1);

        for (index = 1; index < month; index++)

            yearStartDay += GetMonthDays(year, index);

        return yearStartDay % 7;

    }

    function IsLeap(year) {

        var yearIndex = year % 33;

        return ((yearIndex == 1) || (yearIndex == 5) || (yearIndex == 9) || (yearIndex == 13) || (yearIndex == 17) || (yearIndex == 22) || (yearIndex == 26) || (yearIndex == 30));

    }

    function HideDatePicker()

    {

        isPickerVisible = false;

        datePickerFrame.innerHTML = '';

        datePickerFrame.style.visibility = 'hidden';

    }

    function GetMonthDays(year, month)

    {

        if (month >= 1 && month <= 6)

            return 31;

        if (month >= 7 && month <= 11)

            return 30;

        if (!IsLeap(year))

            return 29;

        return 30;

    }

    function ChangeSlectedDate(year, month, day)

    {

        ChangeDate(year, month, day);

        dateBox.value = GetStringDate(year, month, day);

        HideDatePicker();

        if (dateBox.getAttribute("autoPostBack") == 'true')

        {

        document.forms[0].submit(); 

        }

    }

    function ChangeDate(year, month, day)

    {

        currentYear = selectedYear = year;

        currentMonth = selectedMonth = month;

        currentDay = selectedDay = day;

    } 

    function GetStringDate(year, month, day)

    {

        var result = '';

        result += year + '/';

        if (month >= 1 && month <= 9)

            result += '0';

        result += month + '/';

        if (day >= 1 && day <= 9)

            result += '0';

        result += day;

        return result;

    }

    function ChangeYear(count)

    {

        currentYear += count;

        if (currentYear < minAcceptedYear)

            currentYear = minAcceptedYear;

        if (currentYear > maxAcceptedYear)

            currentYear = maxAcceptedYear;

        RenderDatePicker();

    }

    function ChangeMonth(count)

    {

        currentMonth += count;

        if (currentMonth == 13)

        {

            ChangeYear(+1);

            currentMonth = 1;

        }

        if (currentMonth == 0)

        {

            ChangeYear(-1);

            currentMonth = 12;

        }

        RenderDatePicker();

    }

    function LoadMonthOptions()

    {

        var result = '';

        for (index = 0; index < monthName.length; index++)

            result = result + '<option' + (index == currentMonth - 1? ' selected="selected"' : '') + '>' + monthName[index] + '</option>';

        return result;

    }
