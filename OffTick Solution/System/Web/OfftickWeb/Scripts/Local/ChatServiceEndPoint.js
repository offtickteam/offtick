﻿var chatService = $.connection.ChatService;

$(function () {
    $("#txtSendMessage").on("input", function (e) {
        var currentUserName = $("#hdnCurrentUserName").val();
        var otherUserName = $("#hdnOtherUserName").val();
        if ($("#txtSendMessage").val() == "") {
            SRpcCancelTyping(currentUserName, otherUserName);
        }
        else {
            if ($("#txtSendMessage").length == 1) {
                
                SRpcStartTyping(currentUserName, otherUserName);
            }
        }

    });
});


// initialize SignalR functions: this functions need to run conversation page

function initializeSignalR(userName) {

    chatService.client.cRpcSendMessageAck = function (notification) {
        
    };
    chatService.client.cRpcRecieveMessage = function (notification) {
        if (notification) {
            console.log(notification);
            clearMessageBox();

            var currentUserName = $("#hdnCurrentUserName").val();
            var otherUserName = $("#hdnOtherUserName").val();
            var currentImgUrl = $("#hdnCurrentUserImgUrl").val();
            var otherImgUrl = $("#hdnOtherUserImgUrl").val();

            
            if ($('#chatContainer').length) {//means chat page is open
                addMessageToChatBox(currentUserName, notification.FromUserName, currentImgUrl, otherImgUrl, notification.FromUserName, notification.ToUserName, notification.Message, notification.fileName, notification.DatetimeFa);
                if (otherUserName == notification.FromUserName) {//means chat page is open
                    SRpcReadMessage(currentUserName, notification.MessageId);
                    addMessageToPage(notification.MessageId, currentUserName, otherUserName, currentImgUrl, otherImgUrl, notification.FromUserName, notification.ToUserName, notification.Message, notification.fileName,notification.DatetimeFa);
                }
                else  {
                    increaseConversationCounter();
                    SRpcDeliverMessage(currentUserName, notification.MessageId);
                }
            }
            else {
                increaseConversationCounter();
                SRpcDeliverMessage(currentUserName, notification.MessageId);
            }
            
           
        }
        
    };


    
                     
                            
                                
              
    

    chatService.client.cRpcDeliverMessageAck = function (notification) {
        //alert(notification);

    };
    chatService.client.cRpcReadMessageAck = function (notification) {
        $("#" + notification.MessageId + " .message-date").removeClass("sent");
        $("#" + notification.MessageId + " .message-date").addClass("checked");
    };
    chatService.client.cRpcStartTypingAck = function (notification) {
        var message=notification.FirstUserName + " is typing...";
        showMessageBox(message);
    };
    chatService.client.cRpcCancelTypingAck = function (notification) {
        clearMessageBox();
    };
    chatService.client.cRpcDeleteMessageOnReceiver = function (notification) {
        $("li#" + notification.MessageId).fadeOut(300, function () { $(this).remove(); });

    };
    chatService.client.cRpcDeleteMessageBeforeReadAckOnSender = function (notification) {

    };
    chatService.client.cRpcDeleteMessageBeforeReadOnReceiver = function (notification) {

    };
  
    
    


    $.connection.hub.start()
        .done(function () {

        //initialize
            chatService.server.sRpcConnect(userName, "")
                .done(function () { })
                .fail(function
                    (error) { console.log('Could not connect'+ error); });
    })
        .fail(function
        (error) { console.log('Could not connect'+ error); });

   

};


function clearMessageBox() {
    $("#lblConversationMessage").val("");
}
function showMessageBox(message) {
    $("#lblConversationMessage").val(message);
}


function increaseConversationCounter() {
    if ($("#lblConversationCount").length > 0) {
        var counterText = $("#lblConversationCount").text();
        var counter = Number(counterText) + 1;
        $("#lblConversationCount").html(counter);
    } else {
        $("#unreadCounterContainer").append('<span class="icon-qty" id="lblConversationCount">1</span>');
    }
}

function addMessageToPage(messageId,currentUserName, otherUserName, currentImgUrl, otherImgUrl,fromUserName,toUserName,bodyOfMessage,fileName,datetimeFa) {
    

        
        var liImgScr = "";
        var liCssOwner = "";

        if (currentUserName != fromUserName) {
            liCssOwner = "";
            liImgScr = otherImgUrl;
        }
        else {
            liCssOwner = "my-message";
            liImgScr = currentImgUrl;
        }
        var responseHtml = '<li class="' + liCssOwner + '" id="'+messageId+'" >' +
                                '<div class="single-message-content">' +
                                    '<div class="user-avatar-container">' +
                                        '<img src="' + liImgScr + '" alt="">' +
                                    '</div>' +
                                    '<div class="message-container">' +
                                        '<span class="user-name">' + fromUserName + '</span>' +
                                            bodyOfMessage +
                                        '<span class="message-delete btn fa fa-remove" data-messaageId="' + messageId + '"> </span>' +

                                        '<span class="message-date sent">' + datetimeFa + '</span>' +
                                         
                                    '</div>' +
                                '</div>' +
                            '</li>';

        $("#chat-content-list").append(responseHtml);
    
        
};
function addMessageToChatBox(currentUserName, otherUserName, currentImgUrl, otherImgUrl, fromUserName, toUserName, bodyOfMessage, fileName,dateTime) {

    var existedChatBox = $("#chat-box-list").find('li[data-userName="' + otherUserName + '"]');
    existedChatBox.detach().prependTo($("#chat-box-list"));
   if (existedChatBox.length > 0) {
       $(existedChatBox).find('span.chat-excerpt').html(truncateAtWord(bodyOfMessage, 30));
       $(existedChatBox).find('span.last-message-date').html(dateTime);
       $(existedChatBox).find('span.new-messages-count').html();

       if ($(existedChatBox).find('span.new-messages-count').length > 0) {
           var counterText = $(existedChatBox).find('span.new-messages-count').text();
           var counter = Number(counterText) + 1;
           $(existedChatBox).find('span.new-messages-count').html(counter);
       } else {
           $(existedChatBox).append('<span class="new-messages-count">1</span>');
       }
   }
   else {

       var entityHtml = '<li class="not-read chat-box-conversation-for" data-userName="' + otherUserName + '">' +
                                       '<div class="user-avatar-container">' +
                                           '<img class="avatar" src="' + otherImgUrl + '" alt="">' +
                                       '</div>' +
                                       '<span class="user-name">' + otherUserName + '</span>' +
                                       '<span class="chat-excerpt">' + truncateAtWord(bodyOfMessage,30) + '</span>' +
                                       '<span class="new-messages-count">1</span>' +
                                        '<span class="last-message-date">' + dateTime + '</span>' +
                               '</li>';
       $("#chat-box-list").append(entityHtml);
   }
   


};

function truncateAtWord(text,length) {
    if (text == null || text.Length < length || text.indexOf(" ", length) == -1)
        return text;

    return text.substr(0, text.indexOf(" ", length));
}

function deleteMessage(userName,messageId) {
    var notification = {
        MessageId: messageId,
        UserName:userName,
    };

    chatService.server.sRpcDeleteMessage(notification).done(function (result) {
        console.log(result);
        if (result.ResultStatus == "Success") {
            //console.log(result);
            $("li#" + result.DataObject.MessageId).fadeOut(300, function () { $(this).remove(); });
            
        }

    })
        .fail(function (error) {
            console.log('Could not send message' + error);
        });
}
function sendMessage(fromUserName, toUserName, message, latitude, longitude, messageType, flags) {
    // Call the Send method on the hub. 
    var notification =  {
        FromUserName: fromUserName,
        ToUserName: toUserName,
        Message: message,
        Latitude: latitude,
        Longitude: longitude,
        MessageType: messageType,
        Flags: flags,
    };
  
    chatService.server.sRpcSendMessage(notification).done(function (result) {
        console.log(result);
        if (result.ResultStatus == "Success") {
            var currentUserName = $("#hdnCurrentUserName").val();
            var otherUserName = $("#hdnOtherUserName").val();
            var currentImgUrl = $("#hdnCurrentUserImgUrl").val();
            var otherImgUrl = $("#hdnOtherUserImgUrl").val();
            addMessageToPage(result.DataObject.MessageId, currentUserName, otherUserName, currentImgUrl, otherImgUrl, notification.FromUserName, notification.ToUserName, notification.Message, notification.fileName, result.DataObject.DatetimeFa);

            $("#txtSendMessage").val('');
        }
        
    })
        .fail(function (error) {
            console.log('Could not send message' + error);
        });
};


//chatService.server.SRpcDeliverMessage(userName, "").done(function () { });
function SRpcDeliverMessage(userName,messageId) {
    // Call the Send method on the hub. 
    var notification = {
        MessageId: messageId,
        UserName: userName,
    };

    chatService.server.sRpcDeliverMessage(notification).done(function (result) {
        console.log(result);
    })
        .fail(function (error) {
            console.log('Could not send message' + error);
        });
};


//chatService.server.SRpcReadMessage(userName, "").done(function () { });
function SRpcReadMessage(userName, messageId) {
    // Call the Send method on the hub. 
    var notification = {
        MessageId: messageId,
        UserName: userName,
    };

    chatService.server.sRpcReadMessage(notification).done(function (result) {
        console.log(result);
    })
        .fail(function (error) {
            console.log('Could not send message' + error);
        });
};

//chatService.server.SRpcStartTyping(userName, "").done(function () { });
function SRpcStartTyping(sender, receiver) {
    // Call the Send method on the hub. 
    var notification = {
        FirstUserName: sender,
        SecondUserName: receiver,
    };

    chatService.server.sRpcStartTyping(notification).done(function (result) {
        console.log(result);
    })
        .fail(function (error) {
            console.log('Could not send message' + error);
        });
};

//chatService.server.SRpcCancelTyping(userName, "").done(function () { });
function SRpcCancelTyping(sender, receiver) {
    // Call the Send method on the hub. 
    var notification = {
        FirstUserName: sender,
        SecondUserName: receiver,
    };

    chatService.server.sRpcCancelTyping(notification).done(function (result) {
        console.log(result);
    })
        .fail(function (error) {
            console.log('Could not send message' + error);
        });
};



//chatService.server.sRpcRecieveMessageAck(userName, "").done(function () { });


//chatService.server.SRpcDisconnect(userName, "").done(function () { });
//chatService.server.SRpcRequestArchiveMessage(userName, "").done(function () { });
//chatService.server.SRpcEventProcessDone(userName, "").done(function () { });
//chatService.server.SRpcSyncClientToServer(userName, "").done(function () { });


//chatService.server.SRpcChangeRunningState(userName, "").done(function () { });
//chatService.server.SRpcDeleteConversationFor(userName, "").done(function () { });
//chatService.server.SRpcBlockContactFor(userName, "").done(function () { });
//chatService.server.SRpcUnBlockContactFor(userName, "").done(function () { });


//chatService.server.SrpcCheckOnlineStatus(userName, "").done(function () { });
//chatService.server.SrpcDeleteConversationBeforeRead(userName, "").done(function () { });
//chatService.server.SrpcDeleteConversationBeforeReadAck(userName, "").done(function () { });