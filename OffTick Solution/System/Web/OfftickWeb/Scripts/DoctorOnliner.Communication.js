﻿function showCommunicationPanelMessage(message, delayTime) {
	var HeaderCommunicationPanelMessageBoxId = "HeaderCommunicationPanelMessageBox";
	$("#" + HeaderCommunicationPanelMessageBoxId).html(message);
	$("#" + HeaderCommunicationPanelMessageBoxId).show();
	if (delayTime) {
		setTimeout(function () {
			$("#" + HeaderCommunicationPanelMessageBoxId).html("");
			$("#" + HeaderCommunicationPanelMessageBoxId).fadeOut();
		}, delayTime);
	}
}