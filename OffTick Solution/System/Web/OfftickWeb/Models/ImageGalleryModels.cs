﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Offtick.Web.OfftickWeb.Models
{
    public class ImageGalleryViewerModel
    {
        public Guid GalleryId { get; set; }
        public Guid UserId{get;set;}
        public string UserName { get; set; }
        public GalleriaModel[] GalleriaModels { get; set; }
       

    }


    public class GalleriaModel
    {
        public string thumb { get; set; }
        public string image { get; set; }
        public string big { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string link { get; set; }
        public string layer { get; set; }
        public string video { get; set; }
        public string iframe { get; set; }
        public string original { get; set; }
        public Guid imageId { get; set; }
    }
}