﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;
using Offtick.Web.OfftickWeb;
using System.Web;
using Offtick.Core.Validation;

namespace Offtick.Web.OfftickWeb.Models
{

    public class ChangePasswordModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "کلمه عبور فعلی")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "{0} باید حداقل {2} کاراکتر طول داشته باشد", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "کلمه عبور جدید")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "تکرار کلمه عبور جدید")]
        [System.Web.Mvc.Compare("NewPassword", ErrorMessage = "کلمه عبور جدید و تکرار آن با هم یکسان نیستند")]
        public string ConfirmPassword { get; set; }
    }

    public class LogOnModel
    {
         [Required(ErrorMessageResourceName = "ReguieredField", ErrorMessageResourceType = typeof(Resources.UIMessages))]
        [Display(Name = "نام کاربری")]
        public string UserName { get; set; }

         [Required(ErrorMessageResourceName = "ReguieredField", ErrorMessageResourceType = typeof(Resources.UIMessages))]
         [DataType(DataType.Password)]
        [Display(Name = "کلمه عبور")]
        public string Password { get; set; }

        [Display(Name = "مرا بخاطر بسپار")]
        public bool RememberMe { get; set; }
    }

    public class RegisterModel
    {
         [Required(ErrorMessageResourceName = "ReguieredField", ErrorMessageResourceType = typeof(Resources.UIMessages))]
         [Display(Name = "UserName", ResourceType = typeof(Resources.UICaptions))]
        public string UserName { get; set; }

         [DataType(DataType.Password)]
         [Required(ErrorMessageResourceName = "ReguieredField", ErrorMessageResourceType = typeof(Resources.UIMessages))]
         [Display(Name = "Password", ResourceType = typeof(Resources.UICaptions))]
        public string Password { get; set; }


         //[DataType(DataType.Password)]
         //[System.Web.Mvc.Compare("Password", ErrorMessageResourceName = "ComparePassword", ErrorMessageResourceType = typeof(Resources.UIMessages))]
         //[Required(ErrorMessageResourceName = "ReguieredField", ErrorMessageResourceType = typeof(Resources.UIMessages))]
         //[Display(Name = "ConfirmPassword", ResourceType = typeof(Resources.UICaptions))]
         //public string ConfirmPassword { get; set; }


        [Required(ErrorMessageResourceName = "ReguieredField", ErrorMessageResourceType = typeof(Resources.UIMessages))]
         [Display(Name = "FirstName", ResourceType = typeof(Resources.UICaptions))]
         public string FirstName { get; set; }


        //[Required(ErrorMessageResourceName = "ReguieredField", ErrorMessageResourceType = typeof(Resources.UIMessages))]
        [Display(Name = "LastName", ResourceType = typeof(Resources.UICaptions))]
        public string LastName { get; set; }


        [Required(ErrorMessageResourceName = "ReguieredField", ErrorMessageResourceType = typeof(Resources.UIMessages))]
        [Display(Name = "Email", ResourceType = typeof(Resources.UICaptions))]
        public string Email { get; set; }


        //[Display(Name = "ConfirmEmail", ResourceType = typeof(Resources.UICaptions))]
        //public string ConfirmEmail { get; set; }


       // [Required(ErrorMessageResourceName = "ReguieredField", ErrorMessageResourceType = typeof(Resources.UIMessages))]
        [Display(Name = "BirthDate", ResourceType = typeof(Resources.UICaptions))]
        public string BirthDate { get; set; }


        [Required(ErrorMessageResourceName = "ReguieredField", ErrorMessageResourceType = typeof(Resources.UIMessages))]
        [Display(Name = "Gender", ResourceType = typeof(Resources.UICaptions))]
        public string Gender { get; set; }

        [Display(Name = "CityId", ResourceType = typeof(Resources.UICaptions))]
        public Nullable<System.Guid> CityId { get; set; }

      
        [Display(Name = "Picture", ResourceType = typeof(Resources.UICaptions))]
        public string Picture { get; set; }

        //[Required(ErrorMessageResourceName = "ReguieredField", ErrorMessageResourceType = typeof(Resources.UIMessages))]
        [Display(Name = "NezamPezeshkiNumber", ResourceType = typeof(Resources.UICaptions))]
        public string RegistrationNo { get; set; }


        [Display(Name = "PhoneNumber", ResourceType = typeof(Resources.UICaptions))]
        public string PhoneNumber { get; set; }

          [Display(Name = "Domain", ResourceType = typeof(Resources.UICaptions))]
        public string Domain { get; set; }

          [Display(Name = "Address", ResourceType = typeof(Resources.UICaptions))]
          public string Address { get; set; }


          [Display(Name = "OfficeNumber", ResourceType = typeof(Resources.UICaptions))]
          public string OfficeNumber { get; set; }


          //[Required(ErrorMessageResourceName = "ReguieredField", ErrorMessageResourceType = typeof(Resources.UIMessages))]
          public HttpPostedFileBase inputFilePicture { get; set; }

          public HttpPostedFileBase inputFileLisence { get; set; }
       

        
    }


    [AtLeastOneOfTwoAttribute(ErrorMessage="atleast one of 2 required",GroupName="PhoneOrEmail")]
    public class RegisterModelStep1
    {
        

        [DataType(DataType.Password)]
        [Required(ErrorMessageResourceName = "ReguieredField", ErrorMessageResourceType = typeof(Resources.UIMessages))]
        [Display(Name = "Password", ResourceType = typeof(Resources.UICaptions))]
        //[System.ComponentModel.DataAnnotations.MinLength(6, ErrorMessageResourceName = "MinLength", ErrorMessageResourceType = typeof(Resources.UIMessages))]
        [StringLength(100, ErrorMessageResourceName = "StringLengthMessage", ErrorMessageResourceType = typeof(Resources.UIMessages), MinimumLength = 6)]
        public string Password { get; set; }


      //  [Required(ErrorMessageResourceName = "ReguieredField", ErrorMessageResourceType = typeof(Resources.UIMessages))]
        [Display(Name = "Email", ResourceType = typeof(Resources.UICaptions))]
        [EmailAddress(ErrorMessageResourceName = "InvalidEmailAddress", ErrorMessageResourceType = typeof(Resources.UIMessages), ErrorMessage = null)]
        [AtleastOneAttribute(AttributeIndex = 1, AttributeGroup = "PhoneOrEmail")]
        public string Email { get; set; }


        [Display(Name = "PhoneNumber", ResourceType = typeof(Resources.UICaptions))]
        [AtleastOneAttribute(AttributeIndex = 0, AttributeGroup = "PhoneOrEmail")]
        [RegularExpression("^09[0-9]{9}",ErrorMessageResourceName="InvalidPhoneNumber",ErrorMessageResourceType=typeof(Resources.UIMessages))]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessageResourceName = "ReguieredField", ErrorMessageResourceType = typeof(Resources.UIMessages))]
        [Display(Name = "CityId", ResourceType = typeof(Resources.UICaptions))]
        public System.Guid CityId { get; set; }

       

       
    }


    public class RegisterModelStep3
    {


        [Required(ErrorMessageResourceName = "ReguieredField", ErrorMessageResourceType = typeof(Resources.UIMessages))]
        [Display(Name = "FirstName", ResourceType = typeof(Resources.UICaptions))]
        public string FirstName { get; set; }


       // [Required(ErrorMessageResourceName = "ReguieredField", ErrorMessageResourceType = typeof(Resources.UIMessages))]
        [Display(Name = "LastName", ResourceType = typeof(Resources.UICaptions))]
        public string LastName { get; set; }

        [Display(Name = "BirthDate", ResourceType = typeof(Resources.UICaptions))]
        public string BirthDate { get; set; }


        //[Required(ErrorMessageResourceName = "ReguieredField", ErrorMessageResourceType = typeof(Resources.UIMessages))]
        [Display(Name = "Gender", ResourceType = typeof(Resources.UICaptions))]
        public string Gender { get; set; }

        


        

        //[Required(ErrorMessageResourceName = "ReguieredField", ErrorMessageResourceType = typeof(Resources.UIMessages))]
        [Display(Name = "NezamPezeshkiNumber", ResourceType = typeof(Resources.UICaptions))]
        public string RegistrationNo { get; set; }


        //[Display(Name = "PhoneNumber", ResourceType = typeof(Resources.UICaptions))]
        //public string PhoneNumber { get; set; }

        [Display(Name = "Domain", ResourceType = typeof(Resources.UICaptions))]
        public string Domain { get; set; }

        [Display(Name = "Address", ResourceType = typeof(Resources.UICaptions))]
        public string Address { get; set; }


        [Display(Name = "OfficeNumber", ResourceType = typeof(Resources.UICaptions))]
        public string OfficeNumber { get; set; }

    }

    public class RegisterModelStep2
    {
        [Required(ErrorMessageResourceName = "ReguieredField", ErrorMessageResourceType = typeof(Resources.UIMessages))]
        [Display(Name = "UserName", ResourceType = typeof(Resources.UICaptions))]
        [RegularExpression("[a-zA-Z0-9_]{6,40}", ErrorMessageResourceName = "InvalidUserName", ErrorMessageResourceType = typeof(Resources.UIMessages))]
        public string UserName { get; set; }
        public bool FillLater { get; set; }

        //[Required(ErrorMessageResourceName = "ReguieredField", ErrorMessageResourceType = typeof(Resources.UIMessages))]
        public HttpPostedFileBase inputFilePicture { get; set; }
        public HttpPostedFileBase inputFileLisence { get; set; }

        //this bellow 3 field constructed during save images
        public string userId { get; set; }
        public string fileName { get; set; }
        public string lisenceFileName { get; set; }
    }

    public class RegisterModelConfirm
    {
        [Display(Name = "UserName", ResourceType = typeof(Resources.UICaptions))]
        public string UserName { get; set; }
        [Display(Name = "Email", ResourceType = typeof(Resources.UICaptions))]
        public string Email { get; set; }

        [Display(Name = "FirstName", ResourceType = typeof(Resources.UICaptions))]
        public string FirstName { get; set; }
        [Display(Name = "LastName", ResourceType = typeof(Resources.UICaptions))]
        public string LastName { get; set; }
        [Display(Name = "BirthDate", ResourceType = typeof(Resources.UICaptions))]
        public string BirthDate { get; set; }
        [Display(Name = "Gender", ResourceType = typeof(Resources.UICaptions))]
        public string Gender { get; set; }
        [Display(Name = "NezamPezeshkiNumber", ResourceType = typeof(Resources.UICaptions))]
        public string RegistrationNo { get; set; }
        [Display(Name = "PhoneNumber", ResourceType = typeof(Resources.UICaptions))]
        public string PhoneNumber { get; set; }
        [Display(Name = "Domain", ResourceType = typeof(Resources.UICaptions))]
        public string Domain { get; set; }
        [Display(Name = "Address", ResourceType = typeof(Resources.UICaptions))]
        public string Address { get; set; }
        [Display(Name = "OfficeNumber", ResourceType = typeof(Resources.UICaptions))]
        public string OfficeNumber { get; set; }

        //[Display(Name = "AcceptRoles", ResourceType = typeof(Resources.UICaptions))]
        public bool AcceptRoles { get; set; }

        public string inputFilePicture { get; set; }
        public string inputFileLisence { get; set; }
    }


    public class EditProfileBaseInfoModel
    {

        [Required(ErrorMessageResourceName = "ReguieredField", ErrorMessageResourceType = typeof(Resources.UIMessages))]
        [Display(Name = "FirstName", ResourceType = typeof(Resources.UICaptions))]
        public string FirstName { get; set; }


        [Required(ErrorMessageResourceName = "ReguieredField", ErrorMessageResourceType = typeof(Resources.UIMessages))]
        [Display(Name = "LastName", ResourceType = typeof(Resources.UICaptions))]
        public string LastName { get; set; }



        [Display(Name = "BirthDate", ResourceType = typeof(Resources.UICaptions))]
        public string BirthDate { get; set; }


        //[Required(ErrorMessageResourceName = "ReguieredField", ErrorMessageResourceType = typeof(Resources.UIMessages))]
        [Display(Name = "Gender", ResourceType = typeof(Resources.UICaptions))]
        public string Gender { get; set; }






        //[Required(ErrorMessageResourceName = "ReguieredField", ErrorMessageResourceType = typeof(Resources.UIMessages))]
        [Display(Name = "NezamPezeshkiNumber", ResourceType = typeof(Resources.UICaptions))]
        public string RegistrationNo { get; set; }


        [Display(Name = "PhoneNumber", ResourceType = typeof(Resources.UICaptions))]
        public string PhoneNumber { get; set; }

        [Display(Name = "Domain", ResourceType = typeof(Resources.UICaptions))]
        public string Domain { get; set; }

        [Display(Name = "Address", ResourceType = typeof(Resources.UICaptions))]
        public string Address { get; set; }


        [Display(Name = "OfficeNumber", ResourceType = typeof(Resources.UICaptions))]
        public string OfficeNumber { get; set; }


        [Display(Name = "CityId", ResourceType = typeof(Resources.UICaptions))]
        public System.Guid CityId { get; set; }


        [Display(Name = "Email", ResourceType = typeof(Resources.UICaptions))]
        [EmailAddress(ErrorMessageResourceName = "InvalidEmailAddress", ErrorMessageResourceType = typeof(Resources.UIMessages), ErrorMessage = null)]
        public string Email { get; set; }

    }

    //public class RegisterModel
    //{
    //    [Required(ErrorMessageResourceName = "ReguieredField", ErrorMessageResourceType = typeof(Resources.UIMessages))]
    //    [Display(Name = "UserName", ResourceType = typeof(Resources.UICaptions))]
    //    public string UserName { get; set; }

    //    [DataType(DataType.Password)]
    //    [Required(ErrorMessageResourceName = "ReguieredField", ErrorMessageResourceType = typeof(Resources.UIMessages))]
    //    [Display(Name = "Password", ResourceType = typeof(Resources.UICaptions))]
    //    public string Password { get; set; }


    //    [DataType(DataType.Password)]
    //    [System.Web.Mvc.Compare("Password", ErrorMessageResourceName = "ComparePassword", ErrorMessageResourceType = typeof(Resources.UIMessages))]
    //    [Required(ErrorMessageResourceName = "ReguieredField", ErrorMessageResourceType = typeof(Resources.UIMessages))]
    //    [Display(Name = "ConfirmPassword", ResourceType = typeof(Resources.UICaptions))]
    //    public string ConfirmPassword { get; set; }


    //    [Required(ErrorMessageResourceName = "ReguieredField", ErrorMessageResourceType = typeof(Resources.UIMessages))]
    //    [Display(Name = "FirstName", ResourceType = typeof(Resources.UICaptions))]
    //    public string FirstName { get; set; }


    //    [Required(ErrorMessageResourceName = "ReguieredField", ErrorMessageResourceType = typeof(Resources.UIMessages))]
    //    [Display(Name = "LastName", ResourceType = typeof(Resources.UICaptions))]
    //    public string LastName { get; set; }


    //    [Display(Name = "Email", ResourceType = typeof(Resources.UICaptions))]
    //    public string Email { get; set; }


    //    [Display(Name = "ConfirmEmail", ResourceType = typeof(Resources.UICaptions))]
    //    public string ConfirmEmail { get; set; }


    //    [Display(Name = "BirthDate", ResourceType = typeof(Resources.UICaptions))]
    //    public Nullable<System.DateTime> BirthDate { get; set; }


    //    //[Required(ErrorMessageResourceName = "ReguieredField", ErrorMessageResourceType = typeof(Resources.UIMessages))]
    //    [Display(Name = "Gender", ResourceType = typeof(Resources.UICaptions))]
    //    public string Gender { get; set; }

    //    [Display(Name = "CityId", ResourceType = typeof(Resources.UICaptions))]
    //    public Nullable<System.Guid> CityId { get; set; }


    //    [Display(Name = "Picture", ResourceType = typeof(Resources.UICaptions))]
    //    public string Picture { get; set; }

    //    //[Required(ErrorMessageResourceName = "ReguieredField", ErrorMessageResourceType = typeof(Resources.UIMessages))]
    //    [Display(Name = "NezamPezeshkiNumber", ResourceType = typeof(Resources.UICaptions))]
    //    public string RegistrationNo { get; set; }


    //    [Display(Name = "PhoneNumber", ResourceType = typeof(Resources.UICaptions))]
    //    public string PhoneNumber { get; set; }

    //    [Display(Name = "Domain", ResourceType = typeof(Resources.UICaptions))]
    //    public string Domain { get; set; }

    //    [Display(Name = "Address", ResourceType = typeof(Resources.UICaptions))]
    //    public string Address { get; set; }


    //    [Display(Name = "OfficeNumber", ResourceType = typeof(Resources.UICaptions))]
    //    public string OfficeNumber { get; set; }


    //    public HttpPostedFileBase inputFilePicture { get; set; }
    //    public HttpPostedFileBase inputFileLisence { get; set; }



    //}


    public class SimpleRegisterModel
    {
        

        [DataType(DataType.Password)]
        [Required(ErrorMessageResourceName = "ReguieredField", ErrorMessageResourceType = typeof(Resources.UIMessages))]
        [Display(Name = "Password", ResourceType = typeof(Resources.UICaptions))]
        [System.ComponentModel.DataAnnotations.MinLength(6, ErrorMessageResourceName = "MinLegth", ErrorMessageResourceType = typeof(Resources.UIMessages))]
        public string Password { get; set; }
        


        [Required(ErrorMessageResourceName = "ReguieredField", ErrorMessageResourceType = typeof(Resources.UIMessages))]
        [Display(Name = "FirstName", ResourceType = typeof(Resources.UICaptions))]
        public string FirstName { get; set; }


        [Required(ErrorMessageResourceName = "ReguieredField", ErrorMessageResourceType = typeof(Resources.UIMessages))]
        [Display(Name = "LastName", ResourceType = typeof(Resources.UICaptions))]
        public string LastName { get; set; }


        [Display(Name = "Email", ResourceType = typeof(Resources.UICaptions))]
        public string Email { get; set; }



    }

  
}
