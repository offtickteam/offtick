﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Offtick.Web.OfftickWeb
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public override void Init()
        {
            base.Init();
        }
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

         /*   routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );*/

            //routes.MapRoute(
            //    name: "Default",
            //    url: "{controller}/{action}/{id}",
            //    defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
            //    constraints: new[] { "OfftickWeb.Controllers" }
            //);

            routes.MapRoute(
                   "Membership",
                    "{username}",
                    new { controller = "Profile", action = "Index" }
                    , namespaces: new[] { "Offtick.Web.OfftickWeb.Areas.User.Controllers" }
                    ).DataTokens.Add("area", "User");


            routes.MapRoute(
               "Default",
               "{controller}/{action}/{id}",
               new { controller = "Home", action = "Index", id = UrlParameter.Optional },
              namespaces: new[] { "OfftickWeb.Controllers" }
          );

          
            
        }

        protected void Application_Start()
        {

            AreaRegistration.RegisterAllAreas();

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);
            SqlServerTypes.Utilities.LoadNativeAssemblies(Server.MapPath("~/bin"));






        }


        void MvcApplication_BeginRequest(object sender, EventArgs e)
        {
            HttpContext.Current.Items["myContext"] = "amin";
        }

        void MvcApplication_EndRequest(object sender, EventArgs e)
        {
            var entityContext = HttpContext.Current.Items["myContext"];
            HttpContext.Current.Items["myContext"] = string.Empty;
        }

        protected virtual void Application_BeginRequest()
        {
            var routeData = RouteTable.Routes.GetRouteData(new HttpContextWrapper(this.Context));
            if (routeData != null && routeData.RouteHandler is MvcRouteHandler)
            {
                //HttpContext.Current.Items["myContext"] = "amin";
            }
            
        }
        
        protected void Application_EndRequest(object sender, EventArgs e)
        {
            
            
        }

        protected void Session_Start(object sender, EventArgs e)
        {
          //  Console.Write(sender);
        }
        protected void Session_End(object sender, EventArgs e)
        {
            //Console.Write(sender);
        }
        
    }
}