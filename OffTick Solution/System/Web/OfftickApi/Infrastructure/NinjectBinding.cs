﻿using Offtick.Business.Services.Storage;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Offtick.Web.Api.Infrastructure
{
    public class NinjectBinding:NinjectModule
    {
        public override void Load()
        {
            this.Bind<IDataContextContainer>().To<DataContextContainer>();
        }
    }
}