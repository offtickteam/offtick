﻿using Offtick.Business.Membership;
using Offtick.Business.Services.Managers;
using Offtick.Business.Services.Storage;
using Offtick.Data.Context.ExpertOnlinerContexts;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace Offtick.Web.Api.Infrastructure
{
    public class OfftickAuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
       


        /*
         flow after calling token endpoint when grant_type=refresh_token :
        we call this type of request access token when we first request refresh_token and accesstoken by calling grant_type=password and then store refresh_token by client 
        and send reminded request with refresh_token

         1. ValidateClientAuthentication
         2. ReceiveAsync : pass if only refresh_token is not expired else client get invalid_grant response
         3. GrantRefreshToken
         4. CreateAsync


         flow after calling token endpoint when grant_type=password:
         1. ValidateClientAuthentication
         2. GrantResourceOwnerCredentials
         3. TokenEndpoint
         4. CreateAsync



            Bisinuss Roles.
            **  clients are : 1. api , 2. mobile, 3.web. 
            **  for each domain(for example misite.com) only one client (api) is defined and alloworigin is its internet domain or its ip address.
            **  for users of offtick two clients( web, mobile) are defined and alloworigin is *(???). one user can login on many web browser to handle this only one refresh &&
            *   token is defined for pair of (client,user) and when user requests by grant_type=refresh_token then a new access token is generated and pass to browser &&
            *   and when user requests by grant_type=password then algorithm search for exsited RefreshTokenEntity if its fined then return existed in TokenEndpoint method by adding additional parameter and &&
            *   there isn't existed one then algorithm create one RefreshtTokenEntity and return it: by using this mechanisym it is confirm that multiple login for each (client,user,refreshtoken) is allowed and &&
            *   when a RefreshToken expired then the first request for new one do act of replacing expired one and however other web browsers on client (web) do login with grant_type=password (becuase their refreshToke is expired) &&
            *   but their request not ended to create new refreshToken( take for example N webbrowser login with same user,client(web) and theire share one RefreshToke. so they can request resources with grant_type=refresh_token and when &&
            *   refreshToken is expired then the first one webBrowser that find its refresh_token is expired request with grant_type=password then Algorithm detected that refresh_token is expired and replace RefreshTokenEntity in RefshTokenReposity &&
            *   and when other N-1 WebBrowser request with grant_type=password then the new build Refresh_token (in previous request) is returned to client in TokenEndpoint( requests with grand_type=refresh_token with previous one will result to invalid_grant error) &&
            *   
         */
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            DataContextManager.SetDataContextContainer(new DataContextContainer());



            //use BASIC authentication for client authentication(means using clientId,clientSecret), the cridential must be BASE64 encoded value
            // step1=  clientId:clientsecret value convert to base64 string , for ease of use we can use https://www.base64encode.org/ website to create that string and xxxx result
            // step2= set Authorization header in request as BASIC xxxxx
            // step3= set Content-Type header in request as application/x-www-form-urlencoded
            // step4= in body of request add three key/value as follows , username,password, grant_type with value of password
            // step5= in body select x-www-form-urlencoded va type of request body
            //this method try to validate client application not client cridential 
            string clientId = string.Empty;
            string clientSecret = string.Empty;
            // The TryGetBasicCredentials method checks the Authorization header and
            // Return the ClientId and clientSecret
            
            if (!context.TryGetBasicCredentials(out clientId, out clientSecret))
            {
                context.SetError("invalid_client", "Client credentials could not be retrieved through the Authorization header.");
                context.Rejected();
                return;
            }

            //todo validate client
            //ClientEntity clientEntity = new ClientRepository().isValidate(clientId, clientSecret);
            MembershipClient clientEntity = AuthenticationManager.Instance.IsClientValidated(clientId, clientSecret);
            if (clientEntity!=null)
            {
                if (!clientEntity.Active)
                {
                    context.SetError("invalid_client", "Client is inactive.");
                    return;
                }
                // Client has been verified.
                context.OwinContext.Set<MembershipClient>("ta:client", clientEntity);
                context.OwinContext.Set<string>("ta:clientAllowedOrigin", clientEntity.AllowedOrigin);
                context.OwinContext.Set<string>("ta:clientRefreshTokenLifeTime", clientEntity.RefreshTokenLifeTime.ToString());
               


                context.Validated(clientId);
            }
            else
            {
                // Client could not be validated.
                context.SetError("invalid_client", "Client credentials are invalid.");
                context.Rejected();
            }
            //return base.ValidateClientAuthentication(context);
        }

        public override Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            //this method validate client cridential(username,password) and if they are valid then it creates Access Token in which it contains enough information to identify a user
            MembershipClient client = context.OwinContext.Get<MembershipClient>("ta:client");
            var allowedOrigin = context.OwinContext.Get<string>("ta:clientAllowedOrigin");
            if (allowedOrigin == null)
            {
                allowedOrigin = "*";
            }
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { allowedOrigin });


            bool isvalidated = Offtick.Business.Membership.MembershipManager.Instance.LoginMember(context.UserName, context.Password) == Offtick.Data.Entities.Common.LoginStatus.Success;
            if (!isvalidated)
                context.SetError("invalid_grant", "Provided username and password is incorrect");
            else
            {
                //AuthenticationType is bearer token that means Token-Based Authentication, it comes from client request.
                var identity = new ClaimsIdentity(context.Options.AuthenticationType);

                var user=MembershipManager.Instance.GetUser(context.UserName);
                foreach ( var role in user.MembershipUsersInRoles)
                {
                    identity.AddClaim(new Claim(ClaimTypes.Role, role.MembershipRole.RoleName));
                }
                identity.AddClaim(new Claim(ClaimTypes.Name, context.UserName));
                identity.AddClaim(new Claim("Email", user.Email));


                var props = new AuthenticationProperties(new Dictionary<string, string>
                    {
                        {
                            "client_id", (context.ClientId == null) ? string.Empty : context.ClientId
                        },
                        {
                            "userName", context.UserName
                        }
                    });

                context.OwinContext.Set<bool>("ta:requestNewToken", true);
                var ticket = new AuthenticationTicket(identity, props);
                context.Validated(ticket);// by calling this function access token has been set.
            }
            return base.GrantResourceOwnerCredentials(context);

        }


        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            //adding some additional properties to the token response
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }
            return Task.FromResult<object>(null);
        }

        public override Task GrantRefreshToken(OAuthGrantRefreshTokenContext context)
        {
            //This method is called when the actual refresh token request comes in
            
            
            // need to verify that the current client is actually the client that requested the token
            var originalClient = context.Ticket.Properties.Dictionary["client_id"];
            var currentClient = context.ClientId;
            // enforce client binding of refresh token
            if (originalClient != currentClient)
            {
                context.SetError("invalid_clientId", "Refresh token is issued to a different clientId.");
                return Task.FromResult<object>(null);
            }

            // chance to change authentication ticket for refresh token requests
            var newIdentity = new ClaimsIdentity(context.Ticket.Identity);
            newIdentity.AddClaim(new Claim("newClaim", "newValue"));
            var newTicket = new AuthenticationTicket(newIdentity, context.Ticket.Properties);
            context.Validated(newTicket);// by calling this function access token has been set.
            return Task.FromResult<object>(null);
        }
    }
}