﻿using Offtick.Business.Membership;
using Offtick.Business.Services.Managers;
using Offtick.Data.Context.ExpertOnlinerContexts;
using Microsoft.Owin.Security.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Offtick.Web.Api.Infrastructure
{
    public class OfftickRefreshTokenProvider : IAuthenticationTokenProvider
    {

        
        public async Task CreateAsync(AuthenticationTokenCreateContext context)
        {
            //duty: persist refresh token 
            //calling serializeTicket() will result a magic string contains all information about ticket and can be deserialize later to setTicket
            //serializeTicket() also do context.setTicket() in behind, after calling serializeTicket() previous refresh_token() is no valid.

            //Get the client ID from the Ticket properties
            var clientid = context.Ticket.Properties.Dictionary["client_id"];

            if (string.IsNullOrEmpty(clientid))
            {
                return;
            }

            //Generating a Uniqure Refresh Token ID
            var refreshTokenId = Guid.NewGuid().ToString("n");


            
            //clientId from webBrowser is equal to ClientName in database
                bool isNotExisted = AuthenticationManager.Instance.FindRefreshToken(context.Ticket.Identity.Name,clientid) ==null;//to do: check token is changed,untill now only persistence is checked
                bool requestNewToken=context.OwinContext.Get<bool>("ta:requestNewToken");

            if (isNotExisted || requestNewToken)
            {
                // Getting the Refesh Token Life Time From the Owin Context
                var refreshTokenLifeTime = context.OwinContext.Get<string>("ta:clientRefreshTokenLifeTime");
                var userEntity = MembershipManager.Instance.GetUser(context.Ticket.Identity.Name);
                var clientEntity = AuthenticationManager.Instance.GetMembershipClient(clientid);
                //Creating the Refresh Token object
                var token = new MembershipRefreshToken()
                {
                    //storing the RefreshTokenId in hash format
                    RefreshTokenId = Helper.GetHash(refreshTokenId),
                    ClientId = clientEntity.ClientId,
                    UsesrId = userEntity.UserId,
                    IssuedTime = DateTime.UtcNow,
                    ExpiredTime = DateTime.UtcNow.AddMinutes(Convert.ToDouble(refreshTokenLifeTime))
                };

                //Setting the Issued and Expired time of the Refresh Token
                context.Ticket.Properties.IssuedUtc = token.IssuedTime;
                context.Ticket.Properties.ExpiresUtc = token.ExpiredTime;
                //Only do the context.SerializeTicket() after setting the ticket parameters.
                token.ProtectedTicket = context.SerializeTicket();

                var result = await AuthenticationManager.Instance.AddRefreshToken(token);

                if (result)
                {
                    // by calling this function access token has been set.
                    //create refresh_token and send it to client.
                    context.SetToken(refreshTokenId);
                }
            }
            
        }

        public async Task ReceiveAsync(AuthenticationTokenReceiveContext context)
        {
            // generate a new access token when we receive the request from the refresh the token

            var allowedOrigin = context.OwinContext.Get<string>("ta:clientAllowedOrigin");
            context.OwinContext.Set<String>("at:token",context.Token);
            allowedOrigin = allowedOrigin == null ? "*" : allowedOrigin;
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { allowedOrigin });
            string hashedTokenId = Helper.GetHash(context.Token);
            
                var refreshToken = Offtick.Business.Services.Managers.AuthenticationManager.Instance.FindRefreshToken(hashedTokenId);
                if (refreshToken != null)
                {
                    //Get protectedTicket from refreshToken class
                    //context.SetTicket() && context.DeserializeTicket, both set the context's AuthenticationTicket.
                    //The difference is that DeserializeTicket takes a string, which is a serialized ticket, this is usefull when working with persited ticket.
                    //if a refresh_token is expired then DeserializeTicket will result to invalid_grant and user must request a new refresh_token else GrantRefreshToken will be called
                    context.DeserializeTicket(refreshToken.ProtectedTicket);
                    
                    //var result =  _repo.RemoveRefreshTokenByID(hashedTokenId);
                }
            
        }

        public void Create(AuthenticationTokenCreateContext context)
        {
            throw new NotImplementedException();
        }

        public void Receive(AuthenticationTokenReceiveContext context)
        {
            throw new NotImplementedException();
        }
    }
}