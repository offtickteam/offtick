﻿using System;
using System.Threading.Tasks;
using Offtick.Web.Api.Infrastructure;
using System.Web.Http;
using Microsoft.Extensions.DependencyInjection;


using System.Linq;
using System.Web.Http.Dependencies;
using System.Web.Mvc;
using System.Web.Routing;
using Microsoft.Owin;
using Owin;
using Microsoft.Owin.Security.OAuth;
using Offtick.Business.Services.Storage;
using System.Collections.Generic;
using System.Web.Http.Controllers;
using System.Net.Http.Headers;

[assembly: OwinStartup(typeof(Offtick.Web.Api.App_Start.OfftickStartup))]

namespace Offtick.Web.Api.App_Start
{
    public class OfftickStartup
    {
        public void Configuration(IAppBuilder app)
        {
            var services = new ServiceCollection();
            ConfigureServices(services);
            var resolver = new DefaultDependencyResolver(services.BuildServiceProvider());
            // Set MVC Resolver
            DependencyResolver.SetResolver(resolver);

            // MVC Route
          /* RouteTable.Routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional });
          */
            // Set WebAPI Resolver and register
            HttpConfiguration config = new HttpConfiguration();
            //config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("multipart/form-data"));
            config.MapHttpAttributeRoutes();
            config.DependencyResolver = resolver;

            // WebApi Route
            config.Routes.MapHttpRoute(
                name: "MyDefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
           );

           


            // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=316888
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            OAuthAuthorizationServerOptions oauthOption = new OAuthAuthorizationServerOptions()
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/token"),
                //AccessTokenExpireTimeSpan = TimeSpan.FromSeconds(30 * 60),
                AccessTokenExpireTimeSpan = TimeSpan.FromHours(24),
                Provider = new OfftickAuthorizationServerProvider(),
                RefreshTokenProvider = new OfftickRefreshTokenProvider(),
                //AccessTokenProvider = new OfftickAccessTokenProvider(),
                //AuthorizationCodeProvider=new OfftickAccessTokenProvider(),
            };
            app.UseOAuthAuthorizationServer(oauthOption);
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());
           

           // app.Run(context => { return context.Response.WriteAsync(""); });


            app.UseWebApi(config);

            app.MapSignalR();

            //            HttpConfiguration config = new HttpConfiguration();
            // config.MapHttpAttributeRoutes();
            //  WebApiConfig.Register(config);


            //app.UseNinjectMiddleware(CreateKernel).UseNinjectWebApi(config);

        }
        /*private static StandardKernel CreateKernel()
        {
            //var kernel = new StandardKernel(new NinjectBinding());
            var kernel = new StandardKernel();
            kernel.Load(Assembly.GetExecutingAssembly());
           
            return kernel;
        }*/
        public async Task createTask(IOwinContext context)
        {
            context.Set<string>("key1", "value1");
        }

        public void ConfigureServices(IServiceCollection services)
        {
            
            services.AddControllersAsServices(typeof(OfftickStartup).Assembly.GetExportedTypes()
                .Where(t => !t.IsAbstract && !t.IsGenericTypeDefinition)
                // .Where(t => typeof(IController).IsAssignableFrom(t)
                .Where(t => typeof(IHttpController).IsAssignableFrom(t)
                            || t.Name.EndsWith("Controller", StringComparison.OrdinalIgnoreCase)));
            services.AddSingleton<IDataContextContainer, DataContextContainer > ();
        }
    }


    public class DefaultDependencyResolver : System.Web.Mvc.IDependencyResolver, System.Web.Http.Dependencies.IDependencyResolver
    {
        protected IServiceProvider serviceProvider;

        public DefaultDependencyResolver(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
        }

        public IDependencyScope BeginScope()
        {
            return new DefaultDependencyResolver(this.serviceProvider.CreateScope().ServiceProvider);
        }

        public void Dispose()
        {
        }

        public object GetService(Type serviceType)
        {
            return this.serviceProvider.GetService(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return this.serviceProvider.GetServices(serviceType);
        }
    }

    public static class ServiceProviderExtensions
    {
        public static IServiceCollection AddControllersAsServices(this IServiceCollection services,
            IEnumerable<Type> controllerTypes)
        {
            foreach (var type in controllerTypes)
            {
                services.AddTransient(type);
            }

            return services;
        }
    }

}
