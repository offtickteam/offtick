﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Offtick.Web.Api.Models
{
    public class FetchCountriesModel
    {
        public int? Page { get; set; }
        public int? Limit { get; set; }
        public string SortBy { get; set; }
        public string Direction { get; set; }
        public string CountryName { get; set; }
        public bool OnlyEmptydetailedCode { get; set; }
    }


    [Serializable]
    public class CountryDTO
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string TellCode {get;set;}
        public string DetailedCode { get; set; }
    }

    public class StateDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string DetailedCode { get; set; }
        public string CountryDetailedCode { get; set; }
    }


    public class CityDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string DetailedCode { get; set; }
        public string StateDetailedCode { get; set; }
    }
}