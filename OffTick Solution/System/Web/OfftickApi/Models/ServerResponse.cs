﻿using Offtick.Web.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Offtick.Web.Api
{
    public class ServerResponse
    {
        public ServerResponseStatus Status { get; set; }
        public string Message { get; set; }
        public string Error { get; set; }
        public ErrorCode ErrorCode { get; set; }

        public string DataType { get; set; }
        public object Data { get; set; }
    }

    public enum ServerResponseDataType {
        String,
        Boolean,
        City,
        Cities,
        State,
        States,
        Country,
        Counties,
        FetchUserForAdmin,
        FetchSiteForAdmin,
        FetchOfferForAdmin,
        Languages,
        MultiLanguageErrorCodes,
        Dictionary,
        Clients,
        RefreshTokens,
        PersonLightModels,
        ProfileHeaderInfo,
        OwnerSpecificationModel,
        OfferDTO,
        OfferDTOList,
        OfferOrderDTO,
        OfferOrderDTOList,
        MembershipUserLightModelList,
        OfferCommentList,
        MenuList,
        VirtualBasket,
        PayRequestResponse,
        PaymentResult,
        ApplicationMBTIQuestionList,
        ListInteger,
        MBTIResponse,
    }

}