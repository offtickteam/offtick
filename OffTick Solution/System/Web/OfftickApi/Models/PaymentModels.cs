﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Offtick.Web.Api.Models
{
    public class AddToBasketModel
    {
        public Guid OfferId { get; set; }
        public int Quantity { get; set; }
        public bool IsBuyingVisible { get; set; }
    }

    public class BasketUpdateCountModel
    {
        public Guid EntityId { get; set; }
        public int EntityCount { get; set; }
    }

    public class PayRequestResponseModel
    {
        public string OrderId { get; set; }
        public string RefCode { get; set; }
    }

    public class PaymentResult
    {
        public string refId { get; set; }
        public short resCode { get; set; }
        public long? saleReferenceId { get; set; }
        public long? saleOrderId { get; set; }
        public bool isSucess { get; set; }
    }
}