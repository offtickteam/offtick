﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Offtick.Web.Api.Models
{
    public enum ServerResponseStatus
    {
        OK=1,
        InvalidParameters=2,
        AccessDenied=3,
        ServerInternalError=4,
        Timeout=5

    }
}