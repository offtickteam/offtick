﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Offtick.Web.Api.Models
{
    public class ManageContentOfMenu
    {
        public Guid MenuId { get; set; }
        public short? LanguageId { get; set; }
        public string Content { get; set; }
    }
}