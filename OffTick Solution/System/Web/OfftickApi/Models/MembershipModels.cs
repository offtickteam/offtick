﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace Offtick.Web.Api.Models
{
   

    public class RegisterModel
    {
        
        public string UserName { get; set; }
        
        public string Password { get; set; }


        public string ConfirmPassword { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string ConfirmEmail { get; set; }
        public string BirthDate { get; set; }
        public string Gender { get; set; }
        public Guid? CityId { get; set; }
        //public string Picture { get; set; }
        public string RegistrationNo { get; set; }//NezamPezeshkiNumber
        public string PhoneNumber { get; set; }
        public string Domain { get; set; }
        public string Address { get; set; }
        public string OfficeNumber { get; set; }
        public string RegistrationMode { get; set; }
       // public HttpPostedFileBase inputFilePicture { get; set; }
        //public HttpPostedFileBase inputFileLisence { get; set; }


        public bool AcceptRoles { get; set; }
    }


    public class ChangePasswordModel
    {
        public string UserName { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmNewPassword { get; set; }
        public string OldPassword { get; set; }
        
    }

    public class EditProfileBaseInfoModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string BirthDate { get; set; }
        public string Gender { get; set; }
        public string RegistrationNo { get; set; }
        public string PhoneNumber { get; set; }
        public string Domain { get; set; }
        public string Address { get; set; }
        public string OfficeNumber { get; set; }
        public System.Guid CityId { get; set; }
        public string Email { get; set; }
    }

    public class OwnerSpecificationModel
    {
        public string UserName { get; set; }
        public string Specification { get; set; }
        public string ResponsibleName { get; set; }
        public string ResponsiblePhone { get; set; }
        public string ResponsibleEmail { get; set; }
    }

    public class MembershipUserLightModel
    {
        public string Firstname { get; set; }
        public string SecondName { get; set; }
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public string Thumbnail { get; set; }
        public bool isOnline { get; set; }
    }
}