﻿using Offtick.Data.Context.ExpertOnlinerContexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Offtick.Web.Api.Models
{
    public class OfferDTO
    {
        public OfferDTO()
        {
            GalleryImages = new List<string>();
            MenuImages = new List<string>();
        }


      

        public Guid OfferId { get; set; }
        public Guid OwnerId { get; set; }
        public string OwnerUserName { get; set; }
        public string OwnerFirstName { get; set; }
        public string OwnerSecondName { get; set; }

        //images
        public string MainImage { get; set; }
        public IList<string> GalleryImages { get; set; }
        public IList<string> MenuImages { get; set; }

        public string Title { get; set; }
        public string TitleLong { get; set; }
        public string Url { get; set; }


        public long OriginalPrice { get; set; }
        public string OriginalTitle { get; set; }
        public string Unit { get; set; }
        public long DiscountPrice { get; set; }
        public string DiscountTitle { get; set; }
        public short DiscountPercent { get; set; }
        public int ExpireDate { get; set; }
        public short Quantity { get; set; }
        public string Audeiences { get; set; }
        public string LocationCityId { get; set; }
        public string LocationAddress { get; set; }
        
        
        //xmls
        public string Specification { get; set; }
        public string Conditions { get; set; }
        public string Descriptions { get; set; }
        
        
        public short OfftickOfferTypeId { get; set; }
        public string ShowIncomeInPercentage { get; set; }
        public string HasAddedValueTax { get; set; }



        //calculated
        public short Remaining { get; set; }
        public short Sold { get; set; }


    }

    public class TourDTO
    {
        public TourDTO()
        {
            GalleryImages = new List<string>();
            MenuImages = new List<string>();
        }




        public Guid OfferId { get; set; }
        public Guid OwnerId { get; set; }
        public string OwnerUserName { get; set; }
        public string OwnerFirstName { get; set; }
        public string OwnerSecondName { get; set; }

        //images
        public string MainImage { get; set; }
        public IList<string> GalleryImages { get; set; }
        public IList<string> MenuImages { get; set; }

        public string Title { get; set; }
        public string TitleLong { get; set; }
        public string Url { get; set; }


        public long OriginalPrice { get; set; }
        public string OriginalTitle { get; set; }
        public string Unit { get; set; }
        public long? DiscountPrice { get; set; }
        public string DiscountTitle { get; set; }
        public short? DiscountPercent { get; set; }
        public int ExpireDate { get; set; }
        public short? Quantity { get; set; }
        public string Audeiences { get; set; }
        public string LocationCityId { get; set; }
        public string LocationAddress { get; set; }


        //xmls
        public string Specification { get; set; }
        public string Conditions { get; set; }
        public string Descriptions { get; set; }


        public short OfftickOfferTypeId { get; set; }
        public string ShowIncomeInPercentage { get; set; }
        public string HasAddedValueTax { get; set; }



        //calculated
        public short Remaining { get; set; }
        public short Sold { get; set; }

        public bool ShowBuyButton { get; set; }
        public bool ShowOffButton { get; set; }
        public bool ShowInstantButton { get; set; }
        public Guid? SourceCityId { get; set; }
        public string SourceCity { get; set; }
        public Guid? DestinationCityId { get; set; }
        public string DestinationCity { get; set; }
        public DateTime? GoDateTime { get; set; }
        public DateTime? ReturnDateTime { get; set; }
        public string AgancyName { get; set; }
        public int? DurationTime { get; set; }
        public int TravelTypeId { get; set; }
        public string TravelType { get; set; }
    }

    public class ImageVideoDTO
    {
        public Guid OwnerId { get; set; }
        public string OwnerUserName { get; set; }
        public string OwnerFirstName { get; set; }
        public string OwnerSecondName { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string FileName { get; set; }
    }

    public class OfferOrderDTO
    {
        public IList<OfferOrderDetailDTO> Details;
        public OfferOrderDTO()
        {
            Details = new List<OfferOrderDetailDTO>();
        }

        public long OfferOrderId { get; set; }
        public Guid OfferId { get; set; }

        
        public Guid SallerUserId { get; set; }
        public string SallerUserName { get; set; }
        public Guid BuyerUserId { get; set; }
        public string BuyerUserName { get; set; }

        public DateTime DateTime { get; set; }
        public String DateTimePersian { get; set; }
        public short Quantity { get; set; }
        public string Description { get; set; }
        public long PaymentAmount { get; set; }
        public long DiscountAmount { get; set; }
        
        public string SaleRefrenceId { get; set; }
        public string Title { get; set; }
        public string LongTitle { get; set; }

    }
    public class OfferOrderDetailDTO
    {
        public string OfferCode { get; set; }
        public string FileName { get; set; }
        public int UseStatus { get; set; }
    }
    public class MultiPageRequestModel
    {
        public int? Page { get; set; }
        public int? Limit { get; set; }
        public string SortBy { get; set; }
        public string Direction{get;set;}
        public Guid Id { get; set; }
    }


    public class UpdatePostPhotoModel { 
        public Guid OfferId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }


    public class CommentModel
    {
        public string Firstname { get; set; }
        public string SecondName { get; set; }
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public string Thumbnail { get; set; }
        public bool isOnline { get; set; }
        public string Comment { get; set; }
    }


}