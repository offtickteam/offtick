﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Offtick.Web.Api.Models
{
    public enum ApplicationStatus
    {
        PreStart,
        Running,
        Finished,
        FinishedNotPersisted,
        Next,
        Previous,
        Reset,
    }
    public class MBTIModel
    {
        public Int16 QuestionNo { get; set; }
        public string QuestionText { get; set; }
        public string AnswerAText { get; set; }
        public string AnswerBText { get; set; }
        public Int16 AnswerNo { get; set; }
        public ApplicationStatus MBTIStatus { get; set; }

    }


    public class MBTIQuestion
    {
        public long ApplicationMBTIId { get; set; }
        public string Question { get; set; }
        public string AnswerA { get; set; }
        public string AnswerACategory { get; set; }
        public string AnswerB { get; set; }
        public string AnswerBCategory { get; set; }
        public short Level { get; set; }
        public short? CategoryNo { get; set; }
    }

    public class MBTIResponse
    {
        public string UserName { get; set; }
        public string MBTIType { get; set; }
        public string Description { get; set; }
        public bool IsCOmplete { get; set; }
        public IList<MBTIQuestion> AdditionalQuestions { get; set; }
    }
}