﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Offtick.Web.Api.Models
{
    public class LoadUserModel
    {
        public int? Page { get; set; }
        public int? Limit { get; set; }
        public string SortBy { get; set; }
        public string Direction { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string MBTI { get; set; }
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
        public string PhoneNumber { get; set; }
        public bool? SuspendedUser { get; set; }

    }

    public class SendMessageModel
    {
        public string ReceiverUserName { get; set; }
        public string MessageBody { get; set; }
        public string MethodType { get; set; }
    }


    public class FetchSiteModel
    {
        public int? Page { get; set; }
        public int? Limit { get; set; }
        public string SortBy { get; set; }
        public string Direction { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
        public string PhoneNumber { get; set; }
        public bool? SuspendedSite { get; set; }
        public bool? PermitToDistributeOnMainSite { get; set; }
    }


    public class FetchOfferModel
    {
        public int? Page { get; set; }
        public int? Limit { get; set; }
        public string SortBy { get; set; }
        public string Direction { get; set; }
        public string SearchStr { get; set; }
        public int? OfferType { get; set; }
        public int? RoleType { get; set; }
        public int? ConfirmStatus { get; set; }
        public bool IsOnlyReported { get; set; }
    }

    public class LanguageModel
    {
        public short? LanguageId { get; set; }
        public string TitleEnglish { get; set; }
        public string TitleLocal { get; set; }
        public string Caluture { get; set; }
    }

    public class MultiLanguageErrorCodeModel
    {
        
        public Guid? MultiLanguageId { get; set; }
        public short LanguageId { get; set; }
        public string ErrorCode { get; set; }
        public string Meaning { get; set; }
        public string Package { get; set; }
    }


    public class ClientModel
    {
        public Guid? ClientId { get; set; }
        public string ClientName { get; set; }
        public string ClientSecret { get; set; }
        public bool Active { get; set; }
        public int RefreshTokenLifeTime { get; set; }
        public string AllowedOrigin { get; set; }
        //public DateTime CreatedOn { get; set; }
    }

    public class RevokeRefreshTokenModel
    {
        public string ClientName { get; set; }
        public string UserName { get; set; }
    }
}