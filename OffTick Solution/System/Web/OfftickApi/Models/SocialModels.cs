﻿using Offtick.Core.EnumTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Offtick.Web.Api.Models
{
    public class FetchPartiallyModel
    {
        public int? Page { get; set; }
        public int? Limit { get; set; }
        public string UserName { get; set; }
    }

    public class PersonLightModel
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string userName { get; set; }
        public string imageThumbnail { get; set; }
        public FollowStatus fallowStatus { get; set; }
        public long? id { get; set; }
    }

    public class FollowModel
    {
        public string FollowerUserName { get; set; }
        public string FollowingUserName { get; set; }
    }

    public class ProfileHeaderInfo
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string userName { get; set; }
        public string imageThumbnail { get; set; }

        public int countOfFollower { get; set; }
        public int countOfFollowing { get; set; }
        public int countOfPost { get; set; }
        public int age { get; set; }
        public string gender { get; set; }

        public FollowStatus fallowStatus { get; set; }
        public short isBlocked { get; set; }

    }
}