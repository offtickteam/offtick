﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Offtick.Web.Api.Models
{
    public enum ErrorCode
    {
        None=0,

        //Membership (start from 100)
        //userName/Password (01-09)
        UsernameEmpty = 10001,
        UsernameExisted =10002,
        UsernameNotExisted = 10003,
        UsernameInvalidFormat =10004,
        PasswordInvalidFormat=10005,
        
        //email(10-19)
        EmailEmpty=10010,
        EmailExisted =10011,
        EmailNotExisted = 10012,
        EmailInvalidFormat =10013,

        //phonenumber(20-29)
        PhonenumberEmpty = 10020,
        PhonenumberExisted =10021,
        PhonenumberNotExisted=10022,
        PhonenumberInvalidFormat=10023,
        

        //City (30-39)
        CityEmpty=10030,
        CityExisted=10031,
        CityNotExisted=10032,
        CityIdInvalid = 10033,
        StateIdInvalid = 10034,
        CountryIdInvalid = 10035,

        //Info (40-59)
        FirstNameEmpty =10040,
        LastNameEmpty=10041,
        BirthDateEmpty=10042,
        AddressEmpty=10043,
        RegistrationModeInvalid=10044,
        GenderEmpty=10045,
        GenderInvalid=10046,
        PictureEmpty=10047,
        PictureInvalidFileType=10048,
        PictureInvalidFileSize=10049,


        //Security (60-69)
        PasswordConfirmPasswordAreNotMached=10060,
        PasswordInvalid=10061,
        PasswordChangeAccessDenied=10062,
        UserNameChangeAccessDenied=10063,






        //Admin (start from 200)
        MagfaReceptInvalidReceipt=20001,
        MagfaTextMessageInvalidSize=20002,
        SendMessageBodyEmpty=20003,
        SendMessageInvalidType=20004,
        LanguageCaltureEmpty=20005,
        LanguageCaltureDuplicate = 20006,
        LanguageEnglishTitleEmpty = 20007,
        LanguageEnglishTitleDuplicate = 20008,
        LanguageLocalTitleEmpty = 20009,
        LanguageLocalTitleDuplicate = 20010,
        LanguageIdEmpty = 20011,
        LanguageNotExisted=20012,
        LanguageExistedBefore = 20013,
        MultiLanguageMeaningEmpty=20014,
        MultiLanguageErrorCodeEmpty = 20015,
        MultiLanguageErrorCodeDuplicate = 20016,
        MultiLanguagePackeEmpty = 20017,
        MultiLanguageIdEmpty = 20018,
        MultiLanguageNotExist = 20019,
        ClientNameEmpty=20020,
        ClientNameDuplicate=20021,
        RefreshTokeLifeTimeInvalidValue=20022,
        RefreshTokenNotExisted = 20023,
        ClientIdEmpty =20024,
        ClientNotExist=20025,
        ClientSecretEmpty=20026,
        




        //Offer (start from 300)
        OfferNotExist = 30001,
        OfferNotAllowdedToChangeStatus=30002,
        OfferChangeStatusNotAcceptable=30003,
        
        OfferTitleIsEmpty=30010,
        OfferLongTitleIsEmpty=30011,
        OfferOriginalPriceIsEmpty=30012,
        OfferOriginalPriceInvalidFormat = 30013,
        OfferDiscountPriceIsEmpty = 30014,
        OfferDiscountPriceInvalidFormat = 30015,
        OfferExpireDateIsEmpty = 30016,
        OfferExpireDateInvalidFormat = 30017,
        OfferQuantityIsEmpty = 30018,
        OfferQuantityInvalidFormat = 30019,
        OfferTypeIsEmpty = 30020,
        OfferTypeInvalidFormat = 30021,
        OfferTypeNotExist=30022,
        OfferShowIncomeInPercentageIsEmpty = 30023,
        OfferShowIncomeInPercentageInvalidFormat = 30024,
        OfferHasTaxValueIsEmpty = 30025,
        OfferHasTaxValueInvalidFormat = 30026,
        OfferMainImageIsNotProvided=30027,
        OfferMinimumGalleryImagesIsNotProvided = 30028,
        OfferGalleryImagesIsGreaterThanMaximumAllowded = 30029,
        OfferMinimumMenuImagesIsNotProvided = 30030,
        OfferMenuImagesIsGreaterThanMaximumAllowded = 30031,
        OfferGeneralTypeIsNotValid=30032,
        OfferDiscountPercentageIsEmpty = 30033,
        OfferDiscountPercentageInvalidValue = 30034,
        OfferPostImageNotProvided=30035,
        OfferDescriptionIsEmpty=30036,
        OfferReportTextIsEmpty=30037,
        OfferCommentTextIsEmpty=30038,
        OfferCommentNotExist=30039,

        //(tour Start from 50)
        OfferTourShowBuyButtonInvalidValue =30051,
        OfferTourShowOffButtonInvalidValue = 30052,
        OfferTourShowInstantButtonInvalidValue = 30053,
        OfferTourTravelTypeInvalidValue=30054,
        OfferTourSourceCityNotExist=30055,
        OfferTourDestinationCityNotExist = 30056,


        //Social ( strat from 400)
        FollowRequestExistedBefore =40001,
        FollowUserNameToRequestEmpty=40002,
        FollowUserNameToRequestNotExist = 40003,
        FollowRequestedNotExist=40004,
        FollowNotExist=40005,


        //CMS ( start from 500)
        MenuTitleIsEmpty=50001,
        MenuTypeIsInvalid = 50002,
        MenuParentNotExisted=50003,
        MenuNotExisted = 50004,
        CandidateMenuToDeleteHasSubItem=50005,
        MenuContentIsEmpty=50006,


        //EPayment (start from 600)
        OrderQuantityIsNotValid = 60001,
        OrderQuantityIsGreaterRemainingOffer =60002,
        OrderRejectedBySuspendedOffer=60003,
        OrderRejectedByExpiredOffer = 60004,
        OrderResponseFromPaymentGatewaySaleOrderIdIsEmpty=60005,
        OrderBasketChangedSaleOrderIdNotExisted = 60006,


        //Application (start from 700)
        ApplicationMBTIInvalidAsnwers =70001,
        ApplicationMBTIAnswersAreNotComplete = 70002,
        ApplicationMBTIPartOneNotExit=70003,
        ApplicationMBTIIsCompeletedBefore = 70004,
    }
}