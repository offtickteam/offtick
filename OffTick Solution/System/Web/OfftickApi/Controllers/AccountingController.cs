﻿using Offtick.Business.Membership;
using Offtick.Business.Services.Domain;
using Offtick.Business.Services.Storage;
using Offtick.Business.Web;
using Offtick.Web.Api.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization.Formatters.Binary;
using System.Web.Http;

namespace Offtick.Web.Api.Controllers
{
    public class AccountingController : WepApiBaseController
    {
        public AccountingController(IDataContextContainer obj) : base(obj)
        {
        }

        [Authorize(Roles = "Administrator")]
        [HttpPost]
        [Route("api/accounting/FetchCountries")]
        public IHttpActionResult FetchCountries(FetchCountriesModel model)
        {
            if (model == null) return BadRequest();
            
            int take = model.Limit.HasValue ? model.Limit.Value : 20;
            int skip = 0;
            if (model.Page.HasValue)
                skip = (model.Page.Value - 1) * take;



            var coutries = MembershipManager.Instance.GetCountries(model.CountryName, model.OnlyEmptydetailedCode);
            int total = coutries.Count();
            if (!string.IsNullOrEmpty(model.SortBy) && !string.IsNullOrEmpty(model.Direction))
            {
                if (model.Direction.Trim() == "asc")
                {
                    switch (model.SortBy)
                    {
                        case "CountryName":
                            coutries = coutries.OrderBy(q => q.CountryName);
                            break;
                        case "TellCode":
                            coutries = coutries.OrderBy(q => q.TellCode);
                            break;
                        case "DetailedCode":
                            coutries = coutries.OrderBy(q => q.DetailedCode);
                            break;

                    }
                }
                else
                {
                    switch (model.SortBy)
                    {
                        case "CountryName":
                            coutries = coutries.OrderByDescending(q => q.CountryName);
                            break;
                        case "TellCode":
                            coutries = coutries.OrderByDescending(q => q.TellCode);
                            break;
                        case "DetailedCode":
                            coutries = coutries.OrderByDescending(q => q.DetailedCode);
                            break;
                    }
                }
            }

            var records = coutries.Skip(skip).Take(take).ToList().Select(e => new
            {
                CountryId = e.CountryId,
                CountryName = e.CountryName,
                TellCode = e.TellCode,
                DetailedCode = e.DetailedCode,

            }).ToArray();
            return Ok(new ServerResponse() { 
                   ErrorCode= ErrorCode.None,
                   Data=records,
                   DataType=ServerResponseDataType.Cities.ToString()
            });
        }

        [Authorize(Roles = "Administrator")]
        [HttpPost]
        [Route("api/accounting/ExportToExcellCountries")]
        public HttpResponseMessage ExportToExcellCountries(string countryName, bool onlyEmptydetailedCode)
        {
            
            var countries = MembershipManager.Instance.GetCountries(countryName, onlyEmptydetailedCode);
            List<CountryDTO> ls = new List<CountryDTO>();
            foreach (var country in countries)
            {
                ls.Add(new CountryDTO()
                {
                    DetailedCode = country.DetailedCode.HasValue ? country.DetailedCode.Value.ToString() : "",
                    Name = country.CountryName,
                    Id=country.CountryId,
                    TellCode=country.TellCode,
                });
            }
            var result = Request.CreateResponse(HttpStatusCode.OK);
           
           
            result.Content = new StreamContent(new MemoryStream( ExcelService.GetBytes(ls))); // Getting error here
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = "Report.xls"
            };

            return result;
        }
    }
}
