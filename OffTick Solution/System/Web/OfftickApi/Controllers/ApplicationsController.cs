﻿using Offtick.Business.Membership;
using Offtick.Business.Services.Managers;
using Offtick.Business.Services.Storage;
using Offtick.Business.Web;
using Offtick.Data.Context.ExpertOnlinerContexts;
using Offtick.Web.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Offtick.Web.Api.Controllers
{
    public class ApplicationController : WepApiBaseController
    {
        public ApplicationController(IDataContextContainer obj) : base(obj)
        {

        }


        [Authorize()]
        [HttpPost]
        [Route("api/app/doMBTI")]
        public IHttpActionResult DoMBTI(Dictionary<short,short> model)
        {
            var user = MembershipManager.Instance.GetUser(User.Identity.Name);

            if (model == null) return BadRequest();
            var mbtiApp = ApplicationManager.Instance.GetMBTIApplication();
            IList<short> notAnsweredQuestion = new List<short>();
            
            //check all questions has been answered
            foreach(var mbti in mbtiApp.ApplicationMBTIs.Where(e=>e.ApplicationMBTIId<=28))
            {
                if (!model.ContainsKey((short)mbti.ApplicationMBTIId))
                    notAnsweredQuestion.Add((short)mbti.ApplicationMBTIId);
            }
            if(notAnsweredQuestion.Count>0)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.ApplicationMBTIAnswersAreNotComplete ,Data=notAnsweredQuestion,DataType= ServerResponseDataType.ListInteger.ToString()});

            //check all answers are acceptable
            IList<short> invalidAnswers = new List<short>();
            foreach (var key in model.Keys)
            {
                if (!(model[key] == 1 || model[key] == 2))
                    invalidAnswers.Add(key);
            }
            if(invalidAnswers.Count>0)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.ApplicationMBTIInvalidAsnwers,Data=invalidAnswers,DataType=ServerResponseDataType.ListInteger.ToString() });
            ApplicationManager.Instance.ResetMBTIFor(user);
            ApplicationManager.Instance.AddOrUpdateAnswerForMBTI(user, model);
            var mbtiResult = ApplicationManager.Instance.GetMBTIResultForUser(user);
            MBTIResponse resp = new MBTIResponse()
            {
                UserName = user.UserName,
                MBTIType = mbtiResult.Result,
            };
            if (ApplicationManager.Instance.isValidAnswersForMBTI(user))
            {
                resp.IsCOmplete = true;
                return Ok(new ServerResponse() { Status = ServerResponseStatus.OK,  Data = resp, DataType = ServerResponseDataType.MBTIResponse.ToString() });
            }
            if (mbtiResult.Result.Contains("-")) {
                
                var nextQuestions = ApplicationManager.Instance.GetNextMBTIQuestions(mbtiResult.Result);
                IList<MBTIQuestion> questions = new List<MBTIQuestion>();
                foreach (var mbti in nextQuestions)
                {
                    questions.Add(new MBTIQuestion()
                    {
                        AnswerA = mbti.AnswerA,
                        AnswerACategory = mbti.AnswerACategory,
                        AnswerB = mbti.AnswerB,
                        AnswerBCategory = mbti.AnswerBCategory,
                        ApplicationMBTIId = mbti.ApplicationMBTIId,
                        CategoryNo = mbti.CategoryNo,
                        Level = mbti.Level,
                        Question = mbti.Question
                    }); ;
                }
                resp.IsCOmplete = false;
                resp.AdditionalQuestions = questions;
                return Ok(new ServerResponse() { Status = ServerResponseStatus.OK, Data = questions, DataType = ServerResponseDataType.MBTIResponse.ToString() });
            }
            return Ok();
        }

        [Authorize()]
        [HttpPost]
        [Route("api/app/doMBTIPart2")]
        public IHttpActionResult DoMBTIPart2(Dictionary<short, short> model)
        {
            if (model == null) return BadRequest();

            var user = MembershipManager.Instance.GetUser(User.Identity.Name);
            var mbtiResult = user.ApplicationMBTIResults.FirstOrDefault();
            if(mbtiResult==null)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.ApplicationMBTIPartOneNotExit });
            if(ApplicationManager.Instance.isValidAnswersForMBTI(mbtiResult))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.ApplicationMBTIIsCompeletedBefore });

            var addtionalQuestions = ApplicationManager.Instance.GetNextMBTIQuestions(mbtiResult.Result);
            IList<short> notAnsweredQuestion = new List<short>();
            //check all questions has been answered
            foreach (var mbti in addtionalQuestions)
            {
                if (!model.ContainsKey((short)mbti.ApplicationMBTIId))
                    notAnsweredQuestion.Add((short)mbti.ApplicationMBTIId);
            }
            if (notAnsweredQuestion.Count > 0)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.ApplicationMBTIAnswersAreNotComplete, Data = notAnsweredQuestion, DataType = ServerResponseDataType.ListInteger.ToString() });

            
            //check all answers are acceptable
            IList<short> invalidAnswers = new List<short>();
            foreach (var key in model.Keys)
            {
                if (!(model[key] == 1 || model[key] == 2))
                    invalidAnswers.Add(key);
            }
            if (invalidAnswers.Count > 0)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.ApplicationMBTIInvalidAsnwers, Data = invalidAnswers, DataType = ServerResponseDataType.ListInteger.ToString() });

            ApplicationManager.Instance.AddOrUpdateAnswerForMBTI(user, model);
            mbtiResult = ApplicationManager.Instance.GetMBTIResultForUser(user);
            MBTIResponse resp = new MBTIResponse()
            {
                UserName = user.UserName,
                MBTIType = mbtiResult.Result,
                IsCOmplete = ApplicationManager.Instance.isValidAnswersForMBTI(user)
        };
            return Ok(new ServerResponse() { Status = ServerResponseStatus.OK, Data = resp, DataType = ServerResponseDataType.MBTIResponse.ToString() });
        }


        [Authorize()]
        [HttpPost]
        [Route("api/app/fetchMBTIQuestions")]
        public IHttpActionResult FetchMBTIQuestions()
        {
            var mbtis =ApplicationManager.Instance.GetMBTIApplication();
            IList<MBTIQuestion> questions = new List<MBTIQuestion>();
            foreach (var mbti in mbtis.ApplicationMBTIs)
            {
                questions.Add(new MBTIQuestion()
                {
                    AnswerA = mbti.AnswerA,
                    AnswerACategory = mbti.AnswerACategory,
                    AnswerB = mbti.AnswerB,
                    AnswerBCategory=mbti.AnswerBCategory,
                     ApplicationMBTIId=mbti.ApplicationMBTIId,
                     CategoryNo=mbti.CategoryNo,
                     Level=mbti.Level,
                      Question=mbti.Question
                }); ;
            }

            return Ok(new ServerResponse() { Status = ServerResponseStatus.OK, Data = questions, DataType = ServerResponseDataType.ApplicationMBTIQuestionList.ToString() });
        }

        [Authorize()]
        [HttpPost]
        [Route("api/app/fetchMBTIResult")]
        public IHttpActionResult FetchMBTIResult()
        {
            var user = MembershipManager.Instance.GetUser(User.Identity.Name);
            var mbtiResult = user.ApplicationMBTIResults.FirstOrDefault();
            MBTIResponse resp = new MBTIResponse()
            {
                UserName = user.UserName,
                MBTIType = mbtiResult!=null ?mbtiResult.Result:string.Empty,
                IsCOmplete = ApplicationManager.Instance.isValidAnswersForMBTI(user)
            };
            return Ok(new ServerResponse() { Status = ServerResponseStatus.OK, Data = resp, DataType = ServerResponseDataType.MBTIResponse.ToString() });
        }
    }
}
