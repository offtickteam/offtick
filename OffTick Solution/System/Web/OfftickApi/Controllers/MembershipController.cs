﻿using Offtick.Business.Membership;
using Offtick.Business.Services.Storage;
using Offtick.Business.Web;
using Offtick.Core.Utility;
using Offtick.Core.Utility.PersianTools;
using Offtick.Data.Entities.Common;
using Offtick.Data.Context.ExpertOnlinerContexts;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Infrastructure;
using Newtonsoft.Json.Serialization;
using Offtick.Web.Api.Models;
using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
//using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web;
using System.Web.Caching;
using System.Web.Http;

namespace Offtick.Web.Api.Controllers
{
    public class MembershipController : WepApiBaseController
    {
        public MembershipController(IDataContextContainer obj) : base(obj)
        {

        }


        //This resource is For all types of role

        [HttpPost]
        [Route("api/membership/existEmail")]
        public IHttpActionResult ExistEmail(string email)
        {
            return Ok(existEmail(email));
        }
        private ServerResponse existEmail(string email)
        {
            bool existed = false;
            ServerResponseStatus status;
            ErrorCode errorCode = ErrorCode.None;
            if (string.IsNullOrWhiteSpace(email))
            {
                status = ServerResponseStatus.InvalidParameters;
                errorCode = ErrorCode.EmailEmpty;
                //todo : set error message
            }
            else
            {
                existed = MembershipManager.Instance.ExistEmail(email);
                if (existed)
                    errorCode = ErrorCode.EmailExisted;
                status = ServerResponseStatus.OK;
            }
            return new ServerResponse()
            {
                Status = status,
                ErrorCode = errorCode,
                Data = existed,
                DataType = "Boolean"
            };
        }


        [HttpPost]
        [Route("api/membership/existUsername")]
        public IHttpActionResult ExistUsername(string userName)
        {
            return Ok(existUsername(userName));
        }
        private ServerResponse existUsername(string userName)
        {
            bool existed = false;
            ServerResponseStatus status;
            ErrorCode errorCode = ErrorCode.None;
            if (string.IsNullOrWhiteSpace(userName))
            {
                status = ServerResponseStatus.InvalidParameters;
                errorCode = ErrorCode.UsernameEmpty;
                //todo : set error message
            }
            else
            {
                existed = MembershipManager.Instance.ExistUser(userName);
                if (existed)
                    errorCode = ErrorCode.UsernameExisted;
                status = ServerResponseStatus.OK;
            }
            return new ServerResponse()
            {
                Status = status,
                ErrorCode = errorCode,
                Data = existed,
                DataType = "Boolean"
            };
        }



        [HttpPost]
        [Route("api/membership/existPhonenumber")]
        public IHttpActionResult ExistPhonenumber(string phonebumber)
        {
            return Ok(existPhonenumber(phonebumber));
        }
        private ServerResponse existPhonenumber(string phonebumber)
        {
            bool existed = false;
            ServerResponseStatus status;
            ErrorCode errorCode = ErrorCode.None;
            if (string.IsNullOrWhiteSpace(phonebumber))
            {
                status = ServerResponseStatus.InvalidParameters;
                errorCode = ErrorCode.PhonenumberEmpty;
                //todo : set error message
            }
            else
            {
                existed = MembershipManager.Instance.ExistPhoneNumber(phonebumber);
                if (existed)
                    errorCode = ErrorCode.PhonenumberExisted;
                status = ServerResponseStatus.OK;
            }
            return new ServerResponse()
            {
                Status = status,
                ErrorCode = errorCode,
                Data = existed,
                DataType = "Boolean"
            };
        }

        [HttpPost]
        [Route("api/membership/register")]
        public IHttpActionResult Register(RegisterModel model)
        {
            /*if client request use "form-data" then for reading parameters then
             *     System.Web.HttpContext.Current.Request.Params.Get("parameterName")
             *    and for reading files System.Web.HttpContext.Current.Request.Files.Get("fileName") must be used.
             *    but if client use "raw" data then JSON parser convert client json object to a Class Model.
             *    */

            ServerResponseStatus status = ServerResponseStatus.OK;
            if (model == null)
                return BadRequest();

            if (String.IsNullOrWhiteSpace(model.FirstName))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.FirstNameEmpty });
            if (String.IsNullOrWhiteSpace(model.LastName))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.LastNameEmpty });

            var _existPhonenumber = existPhonenumber(model.PhoneNumber);
            if (_existPhonenumber.ErrorCode != ErrorCode.None || _existPhonenumber.Status != ServerResponseStatus.OK)
                return Ok(_existPhonenumber);

            var _existUsername = existUsername(model.UserName);
            if (_existUsername.ErrorCode != ErrorCode.None || _existUsername.Status != ServerResponseStatus.OK)
                return Ok(_existUsername);

            var _existEmail = existEmail(model.Email);
            if (_existEmail.ErrorCode != ErrorCode.None || _existEmail.Status != ServerResponseStatus.OK)
                return Ok(_existEmail);

            MembershipCity existedCity = null;
            if (model.CityId.HasValue)
            {
                existedCity = MembershipManager.Instance.getCityById(model.CityId.Value);
                if (existedCity == null)
                    return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.CityNotExisted });
            }

            if (!String.IsNullOrEmpty(model.Gender) && !(model.Gender.ToUpper().Equals("M") || model.Gender.ToUpper().Equals("F")))
            {
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.GenderInvalid });
            }

            var configManager = ConfigFactory.GetConfigManager(ConfigManagerType.WebConfig);
            string roleName = string.Empty;
            switch (model.RegistrationMode)
            {
                case Infrastructure.Constants.RegistrationMode_Company:
                    roleName = configManager.ReadSetting(Infrastructure.Constants.CompanyNameKey);
                    break;
                case Infrastructure.Constants.RegistrationMode_Doctors:
                    roleName = configManager.ReadSetting(Infrastructure.Constants.DoctorNameKey);
                    break;
                case Infrastructure.Constants.RegistrationMode_Resturant:
                    roleName = configManager.ReadSetting(Infrastructure.Constants.ResturantNameKey);
                    break;
                case Infrastructure.Constants.RegistrationMode_Personal:
                case null:
                case "":
                    roleName = configManager.ReadSetting(Infrastructure.Constants.PersonalNameKey);
                    break;
                default:
                    {
                        return Ok(new ServerResponse()
                        {
                            Status = ServerResponseStatus.InvalidParameters,
                            ErrorCode = ErrorCode.RegistrationModeInvalid,
                            Data = false,
                            DataType = "Boolean"
                        });
                    }

            }

            DateTime? dt = null;
            if (!string.IsNullOrEmpty(model.BirthDate))
                dt = PersianDateConvertor.ToGregorian(model.BirthDate);

            RegisterStatus result = MembershipManager.Instance.InitialRegisterMember(
                model.UserName,
                model.Password,
                model.FirstName,
                model.LastName,
                model != null ? model.Gender : string.Empty,
                dt,
                model.Email,
                model != null ? model.Domain : string.Empty,
                model != null ? model.Address : string.Empty,
                roleName,
                model != null ? model.PhoneNumber : string.Empty,
                string.Empty,
                Guid.NewGuid().ToString()
                );

            if (result == RegisterStatus.Success)
            {
                if (model.CityId.HasValue && model.CityId != default(Guid))

                    MembershipManager.Instance.SaveMapData(model.CityId.Value, MembershipManager.Instance.GetUser(model.UserName));

                //  if (roleName == configManager.ReadSetting(Infrastructure.Constants.DoctorNameKey) && model != null)
                //  {
                //      MembershipManager.Instance.AddOrUpdateDoctorProfile(Guid.Parse(modelStep2.userId), model.RegistrationNo, model.OfficeNumber, model.Address, model.inputFileLisence);
                // }
                // System.Web.Security.FormsAuthentication.SetAuthCookie(model.UserName, true /* createPersistentCookie */);



                //start of sending success register
                string baySaleDate = PersianDateConvertor.ToKhorshidiDate(DateTime.Now);
                string userMobile = model.PhoneNumber;
                string welcomeSms = "";
                if (string.IsNullOrEmpty(model.Email))
                {
                    welcomeSms = string.Format(Resources.UIMessages.SuccessRegisterWithoutEmailSmsNotification, model.UserName, model.Password);
                }
                else
                {
                    welcomeSms = string.Format(Resources.UIMessages.SuccessRegisterWithoutEmailSmsNotification, model.UserName, model.Password, model.Email);
                }
                // 


                Action actionSendSms = () =>
                {
                    Offtick.Business.Services.Domain.SmsService.Instance.SendMagfaSms(userMobile, welcomeSms);
                };
                actionSendSms.BeginInvoke(null, null);
                status = ServerResponseStatus.OK;

            }

            ServerResponse serverResponse = new ServerResponse()
            {
                Status = status,
                Data = true,
                DataType = "Boolean"
            };
            return Ok(serverResponse);
        }

        [HttpPost]
        [Route("api/membership/getCities")]
        public IHttpActionResult GetCities(string stateId)
        {
            Guid guidStateId;
            Guid? guidStateIdNullable = null;
            if (!string.IsNullOrEmpty(stateId))
            {
                if (Guid.TryParse(stateId, out guidStateId))
                    guidStateIdNullable = guidStateId;
                else
                    return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.StateIdInvalid });
            }
            var cities = MembershipManager.Instance.GetCities(guidStateIdNullable);

            return Ok(new ServerResponse()
            {
                Data = cities.Select(e => new { CityId = e.CityId, CityName = e.CityName, StateId = e.StateId }).ToList(),
                DataType = ServerResponseDataType.Cities.ToString(),
                Status = ServerResponseStatus.OK
            });
        }

        [HttpPost]
        [Route("api/membership/getStates")]
        public IHttpActionResult GetStates(string countryId)
        {
            Guid guidCountryId;
            Guid? guidCountryIdNullable = null;
            if (!string.IsNullOrEmpty(countryId))
            {
                if (Guid.TryParse(countryId, out guidCountryId))
                    guidCountryIdNullable = guidCountryId;
                else
                    return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.CountryIdInvalid });
            }
            var cities = MembershipManager.Instance.GetStates(guidCountryIdNullable);

            return Ok(new ServerResponse()
            {
                Data = cities.Select(e => new { StateId = e.StateId, StateName = e.StateName, CountryId = e.CountryId }).ToList(),
                DataType = ServerResponseDataType.States.ToString(),
                Status = ServerResponseStatus.OK
            });
        }

        [HttpPost]
        [Route("api/membership/getCountries")]
        public IHttpActionResult GetCountries(string countryName)
        {
            var countries = MembershipManager.Instance.GetCountries(countryName, null);

            return Ok(new ServerResponse()
            {
                Data = countries.Select(e => new { CountryId = e.CountryId, CountryName = e.CountryName }).ToList(),
                DataType = ServerResponseDataType.Counties.ToString(),
                Status = ServerResponseStatus.OK
            });
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        [Route("api/membership/logout")]
        public IHttpActionResult Logout()
        {
            //AuthenticationManager.SignOut();
            // Request.GetOwinContext().Authentication.SignOut("Bearer");
            //Request.GetOwinContext().Authentication.GetAuthenticationTypes().Select(e => e.AuthenticationType).ToList();
            // Request.GetOwinContext().Authentication.SignOut(Request.GetOwinContext().Authentication.GetAuthenticationTypes().Select(e => e.AuthenticationType).ToArray());


            var authenticationResult = Request.GetOwinContext().Authentication.AuthenticateAsync("Bearer");
            var client_id = authenticationResult.Result.Properties.Dictionary["client_id"];

            //  var item = Request.GetOwinContext().Get<AuthenticationTokenCreateContext>("at:ticket");
            // item.SetToken(null);
            //HttpContext.Current.GetOwinContext()
            var identity = (ClaimsIdentity)User.Identity;
            var rereshToken = Offtick.Business.Services.Managers.AuthenticationManager.Instance.FindRefreshToken(identity.Name, client_id);
            Offtick.Business.Services.Managers.AuthenticationManager.Instance.RemoveRefreshToken(rereshToken);
            // Request.GetOwinContext().Authentication.SignIn(identity);
            return Ok(new ServerResponse() { Status = ServerResponseStatus.OK });
        }


        //(Roles = "Personal, Company, Doctor, ReportUser, Resturant, Administrator, Tester, Developer")
        [Authorize]
        [HttpPost]
        [Route("api/membership/changePassword")]
        public IHttpActionResult ChangePassword(ChangePasswordModel model)
        {

            if (model == null)
                return BadRequest();

            if (String.IsNullOrWhiteSpace(model.OldPassword) || String.IsNullOrWhiteSpace(model.NewPassword) || String.IsNullOrWhiteSpace(model.ConfirmNewPassword))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.PasswordInvalidFormat });
            if (!model.NewPassword.Equals(model.ConfirmNewPassword))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.PasswordConfirmPasswordAreNotMached });

            if (String.IsNullOrWhiteSpace(model.UserName))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.UsernameEmpty });
            var _existedUser = MembershipManager.Instance.GetUser(model.UserName);
            if (_existedUser == null)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.UsernameNotExisted });
            var identity = (ClaimsIdentity)User.Identity;
            //only a user of Administrator || Tester || Developer group can change password of another user else return InvalidAccess
            if (!identity.Name.Equals(model.UserName) &&
                MembershipManager.Instance.GetUser(identity.Name).MembershipUsersInRoles.FirstOrDefault(e => e.MembershipRole.RoleName.Equals("Administrator")
                                                                        || e.MembershipRole.RoleName.Equals("Tester") ||
                                                                         e.MembershipRole.RoleName.Equals("Developer")) == null)
                return Ok(new ServerResponse { Status = ServerResponseStatus.AccessDenied, ErrorCode = ErrorCode.PasswordChangeAccessDenied });


            LoginStatus status1 = MembershipManager.Instance.ChangePassword(_existedUser, model.OldPassword, model.NewPassword);
            if (status1 == LoginStatus.Success)
                return Ok(new ServerResponse { Status = ServerResponseStatus.OK, ErrorCode = ErrorCode.None });
            if (status1 == LoginStatus.InvalidPassword)
                return Ok(new ServerResponse { Status = ServerResponseStatus.AccessDenied, ErrorCode = ErrorCode.PasswordInvalid });

            return Ok(new ServerResponse { Status = ServerResponseStatus.ServerInternalError, ErrorCode = ErrorCode.PasswordInvalid });
        }


        [Authorize]
        [HttpPost]
        [Route("api/membership/forgotPassword")]
        public IHttpActionResult ForgotPassword(string method, string userValue)
        {
            if (string.IsNullOrEmpty(method) || string.IsNullOrEmpty(userValue))
                return Ok(new ServerResponse { Status = ServerResponseStatus.InvalidParameters });

            MembershipUser user = null;
            method = method.ToLower();
            switch (method)
            {
                case "username":
                    user = MembershipManager.Instance.GetUser(userValue);
                    if (user == null)
                        return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.UsernameNotExisted });
                    break;
                case "email":
                    user = MembershipManager.Instance.GetUserByEmail(userValue);
                    if (user == null)
                        return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.EmailNotExisted });
                    break;
                case "phonenumber":
                    user = MembershipManager.Instance.GetUserByPhoneNumber(userValue);
                    if (user == null)
                        return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.PhonenumberNotExisted });
                    break;
            }

            if (user == null)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters });


            string password = MembershipManager.Instance.getDecryptPassword(user.UserName);
            string bodyOfRecovery = string.Format("<div style=\"color:blue;\"><b>Password Recovery</b></div><p>your userName is: {0} and your password is: {1}</p>", user.UserName, password);

            if (method.Equals("username"))
            {
                if (!string.IsNullOrEmpty(user.Email))
                {
                    method = "email";
                    userValue = user.Email;
                }
                else
                if (!string.IsNullOrEmpty(user.PhoneNumber))
                {
                    method = "phonenumber";
                    userValue = user.PhoneNumber;
                }

            }


            if (method.Equals("email"))
            {
                SMTPService smtp = new SMTPService();
                smtp.SendMail(user.Email, "Password Recovery", bodyOfRecovery);
                //all criteria has been passed so send an email to user that contain his/her password
                return Ok(new ServerResponse() { Status = ServerResponseStatus.OK, ErrorCode = ErrorCode.None, Message = string.Format(UIMessages.SendRecoveryPasswordByEmail, userValue) });
            }
            if (method.Equals("phonenumber"))
            {
                /*Action actionSendSms = () =>
                 {
                     Offtick.Business.Services.Domain.SmsService.Instance.SendMagfaSms(user.PhoneNumber, bodyOfRecovery);
                 };
                actionSendSms.BeginInvoke(null, null);*/
                Offtick.Business.Services.Domain.SmsService.Instance.SendMagfaSms(user.PhoneNumber, bodyOfRecovery);
                return Ok(new ServerResponse() { Status = ServerResponseStatus.OK, ErrorCode = ErrorCode.None, Message = string.Format(UIMessages.SendRecoveryPasswordByPhoneNumber, userValue) });
            }
            return Ok(new ServerResponse() { Status = ServerResponseStatus.ServerInternalError });
        }

        [Authorize]
        [HttpPost]
        [Route("api/membership/uploalProfileImage")]
        public IHttpActionResult UploalProfileImage()
        {
            var profilePicture = System.Web.HttpContext.Current.Request.Files.Get("profilePicture");
            if (profilePicture == null)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.PictureEmpty });
            var identity = (ClaimsIdentity)User.Identity;
            var user1 = MembershipManager.Instance.GetCurrentUser();
            var user2 = MembershipManager.Instance.GetUser(identity.Name);
            if (user1.UserName != user2.UserName)
                ModelState.AddModelError("userName", "Invalid");
            HttpPostedFileWrapper imageWrapper = new HttpPostedFileWrapper(profilePicture);

            var editStatus = MembershipManager.Instance.ChangePhoto(user2, imageWrapper);
            if (editStatus == Offtick.Core.EnumTypes.EditStatus.Edited)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.OK, ErrorCode = ErrorCode.None });
            return Ok(new ServerResponse() { Status = ServerResponseStatus.ServerInternalError });
        }


        [Authorize]
        [HttpPost]
        [Route("api/membership/changeUserName")]
        public IHttpActionResult ChangeUserName(string oldUserName, string newUserName)
        {
            if (string.IsNullOrWhiteSpace(oldUserName) || string.IsNullOrWhiteSpace(newUserName))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters });

            var existedUser = MembershipManager.Instance.GetUser(oldUserName);
            if (existedUser == null)
                return Ok(new ServerResponse { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.UsernameNotExisted });

            var checkForexistingUserWithNewuserName = MembershipManager.Instance.GetUser(newUserName);
            if (checkForexistingUserWithNewuserName != null)
                return Ok(new ServerResponse { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.UsernameExisted });
            var identity = (ClaimsIdentity)User.Identity;
            //only a user of Administrator || Tester || Developer group can change password of another user else return InvalidAccess
            if (!identity.Name.Equals(oldUserName) &&
                MembershipManager.Instance.GetUser(identity.Name).MembershipUsersInRoles.FirstOrDefault(e => e.MembershipRole.RoleName.Equals("Administrator")
                                                                        || e.MembershipRole.RoleName.Equals("Tester") ||
                                                                         e.MembershipRole.RoleName.Equals("Developer")) == null)
                return Ok(new ServerResponse { Status = ServerResponseStatus.AccessDenied, ErrorCode = ErrorCode.UserNameChangeAccessDenied });

            var changeStatus = MembershipManager.Instance.ChangeUserName(oldUserName, newUserName);
            if (changeStatus == Offtick.Core.EnumTypes.EditStatus.Edited)
                return Ok(new ServerResponse { Status = ServerResponseStatus.OK, ErrorCode = ErrorCode.None });
            else
                return Ok(new ServerResponse { Status = ServerResponseStatus.ServerInternalError });
        }


        [Authorize]
        [HttpPost]
        [Route("api/membership/updateProfileBaseInfo")]
        public IHttpActionResult UpdateProfileBaseInfo(EditProfileBaseInfoModel model)
        {
            var cu = MembershipManager.Instance.GetUser(User.Identity.Name);
            DateTime? dt = null;
            if (!string.IsNullOrEmpty(model.BirthDate))
                dt = PersianDateConvertor.ToGregorian(model.BirthDate);

            if (cu.Email != model.Email && !string.IsNullOrEmpty(model.Email) && MembershipManager.Instance.ExistEmail(model.Email))
                return Ok(new ServerResponse { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.EmailExisted });
            if (cu.PhoneNumber != model.PhoneNumber && !string.IsNullOrEmpty(model.PhoneNumber) && MembershipManager.Instance.ExistPhoneNumber(model.PhoneNumber))
                return Ok(new ServerResponse { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.PhonenumberExisted });
            var city = MembershipManager.Instance.getCityById(model.CityId);
            if (city == null)
                return Ok(new ServerResponse { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.CityNotExisted });

            var editStatus = MembershipManager.Instance.UpdateInfo(cu, model.FirstName, model.LastName, model.Gender, dt, model.Domain, model.Address, model.PhoneNumber, model.CityId, model.Email);
            if (editStatus == Offtick.Core.EnumTypes.EditStatus.Edited || editStatus == Offtick.Core.EnumTypes.EditStatus.RejectedByNonEffectiveQuery)
                return Ok(new ServerResponse { Status = ServerResponseStatus.OK });
            else
                return Ok(new ServerResponse { Status = ServerResponseStatus.ServerInternalError });
        }

        [Authorize]
        [HttpPost]
        [Route("api/membership/updateProfileSpecification")]
        public IHttpActionResult UpdateProfileSpecification(OwnerSpecificationModel model)
        {
            var cu = MembershipManager.Instance.GetUser(User.Identity.Name);
            if (model == null)
                return BadRequest();

            var editStatus = MembershipManager.Instance.UpdateSpecification(cu, model.Specification, model.ResponsibleName, model.ResponsiblePhone, model.ResponsibleEmail);
            if (editStatus == Offtick.Core.EnumTypes.EditStatus.Edited || editStatus == Offtick.Core.EnumTypes.EditStatus.RejectedByNonEffectiveQuery)
                return Ok(new ServerResponse { Status = ServerResponseStatus.OK });
            else
                return Ok(new ServerResponse { Status = ServerResponseStatus.ServerInternalError });
        }

        [Authorize]
        [HttpPost]
        [Route("api/membership/fetchProfileSpecification")]
        public IHttpActionResult FetchProfileSpecification(string userName)
        {
            if(string.IsNullOrEmpty(userName))
                return Ok(new ServerResponse { Status = ServerResponseStatus.InvalidParameters, ErrorCode= ErrorCode.UsernameEmpty });
            var cu = MembershipManager.Instance.GetUser(userName);
            if(cu==null)
                return Ok(new ServerResponse { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.UsernameNotExisted });
            
            var info = new OwnerSpecificationModel();
            if (cu.MembershipIntroduceInfo != null)
            {
                info.UserName = cu.UserName;
                info.ResponsibleEmail = cu.MembershipIntroduceInfo.ResponsibleEmail;
                info.ResponsibleName = cu.MembershipIntroduceInfo.ResponsibleName;
                info.ResponsiblePhone = cu.MembershipIntroduceInfo.ResponsiblePhone;
                info.Specification = cu.MembershipIntroduceInfo.Specification;
            }
            return Ok(new ServerResponse { Status = ServerResponseStatus.OK, Data = info, DataType = ServerResponseDataType.OwnerSpecificationModel.ToString() });
        }

        [Authorize]
        [HttpPost]
        [Route("api/membership/disableAccount")]
        public IHttpActionResult DisableAccount(string userName)
        {
            if (string.IsNullOrWhiteSpace(userName))
                return BadRequest();
            var user = MembershipManager.Instance.GetUser(userName);
            if (user == null)
                return Ok(new ServerResponse { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.UsernameNotExisted });
            if (!User.Identity.Name.Equals(userName) && !User.IsInRole("Admin"))
                return Ok(new ServerResponse { Status = ServerResponseStatus.AccessDenied });
            var editStatus = MembershipManager.Instance.ChangeStatus(user, MembershipStatus.Disabled);
            if (editStatus == Offtick.Core.EnumTypes.EditStatus.Edited || editStatus == Offtick.Core.EnumTypes.EditStatus.RejectedByNonEffectiveQuery)
                return Ok(new ServerResponse { Status = ServerResponseStatus.OK });
            return Ok(new ServerResponse { Status = ServerResponseStatus.ServerInternalError });
        }
    }
}
