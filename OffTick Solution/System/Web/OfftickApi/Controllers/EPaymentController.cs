﻿using Offtick.Business.Membership;
using Offtick.Business.Services.Domain.VirtualBasket;
using Offtick.Business.Services.Managers;
using Offtick.Business.Services.RealtimeSearvices;
using Offtick.Business.Services.RealtimeSearvices.EventDrivenDispatcher.Notification;
using Offtick.Business.Services.Storage;
using Offtick.Business.Web;
using Offtick.Core.EnumTypes;
using Offtick.Web.DrOnlinerEnterpriseWeb.Infrustracture;
using Offtick.Data.Context.ExpertOnlinerContexts;
using Offtick.Web.Api.Infrastructure;
using Offtick.Web.Api.Models;
using PaymentGatewayService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Offtick.Web.Api.Controllers
{
    public class EPaymentController : WepApiBaseController
    {
        public EPaymentController(IDataContextContainer obj) : base(obj)
        {
        }


        [Authorize()]
        [HttpPost]
        [Route("api/payment/AddToBasket")]
        public IHttpActionResult AddToBasket(AddToBasketModel model)
        {
            if (model == null) return BadRequest();
            var offer = OfferManager.Instance.GetOffer(model.OfferId);
            if (offer == null)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferNotExist });

            if (offer.OfferType == (short)EnumOfferType.Food)
            {
                var foodOffer = offer.OfferFoods.FirstOrDefault();
                var countOfOffer = foodOffer.Quantity;
                var countOfSoled = foodOffer.OfferFoodOrders.Sum(e => e.Quantity);
                var remain = countOfOffer - countOfSoled;
                if (model.Quantity < 1)
                    return Ok(new ServerResponse() { Status = ServerResponseStatus.AccessDenied, ErrorCode = ErrorCode.OrderQuantityIsNotValid });
                if (model.Quantity > remain)
                    return Ok(new ServerResponse() { Status = ServerResponseStatus.AccessDenied, ErrorCode = ErrorCode.OrderQuantityIsGreaterRemainingOffer });
                if (foodOffer.OfferStatusId == (int)OfferExecutionStatus.Suspended)
                    return Ok(new ServerResponse() { Status = ServerResponseStatus.AccessDenied, ErrorCode = ErrorCode.OrderRejectedBySuspendedOffer });
            }

            if (offer.OfferType == (short)EnumOfferType.Tour)
            {
                var tourOffer = offer.OfferTours.FirstOrDefault();
                var countOfOffer = tourOffer.Quantity;
                /*to do :
                 * var countOfSoled = tourOffer.offer.Sum(e => e.Quantity);
                var remain = countOfOffer - countOfSoled;
                if (count > remain)
                    return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, UIMessages.RequestMoreThanRemained, string.Empty, string.Empty, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
                if (tourOffer.OfferStatusId == (int)OfferExecutionStatus.Suspended)
                    return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Failed, UIMessages.SuspendedOffer, string.Empty, string.Empty, ActionResponseFromServerToClient.EmptyObjectServerToClientKey));
                 */
            }


            var user = MembershipManager.Instance.GetUser(User.Identity.Name);
            if (OfferManager.Instance.isExpiredFoodOffer(offer))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.AccessDenied, ErrorCode = ErrorCode.OrderRejectedByExpiredOffer });

            var basketEntity = new VirtualBasketEntityDB();
            basketEntity.AddOrUpdateBasketOrder(offer.OfferFoods.FirstOrDefault().OfferFoodId, model.Quantity, model.IsBuyingVisible);
            PaymentGatewayService.PaymentGatewayEndpoint endPoint = new PaymentGatewayService.PaymentGatewayEndpoint();
            basketEntity.OrderId = endPoint.CreateNewOrderId().ToString();
            basketEntity.UserId = user.UserId;

            VirtualBasketManagerDB.Instance.AddVirtualBasketEntity(user, basketEntity);
            return Ok(new ServerResponse() { Status = ServerResponseStatus.OK });
        }

        [Authorize()]
        [HttpPost]
        [Route("api/payment/updateBasket")]
        public IHttpActionResult UpdateBasket(IList<BasketUpdateCountModel> orders)
        {

            if (orders != null)
            {
                var basketEntity = new VirtualBasketEntityDB();
                foreach (var order in orders)
                {
                    var offer = OfferManager.Instance.GetOffer(order.EntityId);

                    if (offer.OfferType == (short)EnumOfferType.Food)
                    {
                        var foodOffer = offer.OfferFoods.FirstOrDefault();
                        var countOfOffer = foodOffer.Quantity;
                        var countOfSoled = foodOffer.OfferFoodOrders.Sum(e => e.Quantity);
                        var remain = countOfOffer - countOfSoled;
                        if (order.EntityCount < 1)
                            return Ok(new ServerResponse() { Status = ServerResponseStatus.AccessDenied, ErrorCode = ErrorCode.OrderQuantityIsNotValid });
                        if (order.EntityCount > remain)
                            return Ok(new ServerResponse() { Status = ServerResponseStatus.AccessDenied, ErrorCode = ErrorCode.OrderQuantityIsGreaterRemainingOffer });
                        if (foodOffer.OfferStatusId == (int)OfferExecutionStatus.Suspended)
                            return Ok(new ServerResponse() { Status = ServerResponseStatus.AccessDenied, ErrorCode = ErrorCode.OrderRejectedBySuspendedOffer });
                    }
                    if (OfferManager.Instance.isExpiredFoodOffer(offer))
                        return Ok(new ServerResponse() { Status = ServerResponseStatus.AccessDenied, ErrorCode = ErrorCode.OrderRejectedByExpiredOffer });
                    basketEntity.AddOrUpdateBasketOrder(offer.OfferFoods.FirstOrDefault().OfferFoodId, order.EntityCount, null);
                }
                var user = MembershipManager.Instance.GetUser(User.Identity.Name);
                basketEntity.UserId = user.UserId;
                PaymentGatewayService.PaymentGatewayEndpoint endPoint = new PaymentGatewayService.PaymentGatewayEndpoint();
                basketEntity.OrderId = endPoint.CreateNewOrderId().ToString();
                VirtualBasketManagerDB.Instance.AddVirtualBasketEntity(user, basketEntity);
            }
            return Ok(new ServerResponse() { Status = ServerResponseStatus.OK });
        }

        [Authorize()]
        [HttpPost]
        [Route("api/payment/removeFromBasket")]
        public IHttpActionResult RemoveFromBasket(Guid offerId)
        {
            var user = MembershipManager.Instance.GetUser(User.Identity.Name);
            var offer = OfferManager.Instance.GetOffer(offerId);
            if (offer == null)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferNotExist });

            VirtualBasketManagerDB.Instance.RemoveVirtualBasketEntity(offer.OfferFoods.FirstOrDefault().OfferFoodId, user);
            PaymentGatewayService.PaymentGatewayEndpoint endPoint = new PaymentGatewayService.PaymentGatewayEndpoint();
            VirtualBasketManagerDB.Instance.ChangeOrderIdOfVirtualBasketEntity(user, endPoint.CreateNewOrderId().ToString());
            return Ok(new ServerResponse() { Status = ServerResponseStatus.OK });
        }


        [Authorize()]
        [HttpPost]
        [Route("api/payment/fetchBasket")]
        public IHttpActionResult FetchBasket()
        {
            var user = MembershipManager.Instance.GetUser(User.Identity.Name);
            var basketEntity = VirtualBasketManagerDB.Instance.fetchFromDB(user);
            return Ok(new ServerResponse() { Status = ServerResponseStatus.OK, Data = basketEntity, DataType = ServerResponseDataType.VirtualBasket.ToString() });
        }

        [Authorize()]
        [HttpPost]
        [Route("api/payment/payRequest")]
        public IHttpActionResult PayRequest()
        {
            int step = 0;
            try
            {
                string result = "";

                PaymentGatewayEndpoint endPoint = new PaymentGatewayEndpoint();
                bool isSuccess = false;
                var currentUser = Offtick.Business.Membership.MembershipManager.Instance.GetUser(User.Identity.Name);
                Guid userId = currentUser.UserId;

                string _date = DateTime.Now.ToString("yyyyMMdd");
                string _time = DateTime.Now.ToString("HHmmss");
                string _additionalData = "data";
                string _exceptionMessage = string.Empty;
                
                var basket=VirtualBasketManagerDB.Instance.fetchFromDB(currentUser);

                endPoint.PayRequest(userId, long.Parse(basket.OrderId), basket.BasketPrice* 10, _date, _time, _additionalData, 1, (successOrderId, refId) =>
                {
                    isSuccess = true;
                    result = refId;
                    step = 1;
                }, (failOrderId, resCode, exceptionMessage) => {
                    isSuccess = false;
                    result = resCode.ToString();
                    _exceptionMessage = exceptionMessage;
                    step = 2;
                });

                if(isSuccess)
                    return Ok(new ServerResponse() { Status = ServerResponseStatus.OK,Data=new PayRequestResponseModel() { OrderId = basket.OrderId, RefCode = result },DataType= ServerResponseDataType.PayRequestResponse.ToString() });
                return Ok(new ServerResponse() { Status = ServerResponseStatus.ServerInternalError, Data = new PayRequestResponseModel() { OrderId = basket.OrderId, RefCode = result }, DataType = ServerResponseDataType.PayRequestResponse.ToString() });

            }
            catch (Exception ex)
            {
                step = 3;
                return Ok(new ServerResponse() { Status = ServerResponseStatus.ServerInternalError, Error = ex.Message });
            }
        }

        [Authorize()]
        [HttpPost]
        [Route("api/payment/payRequestFake")]
        public IHttpActionResult PayRequestFake()
        {
            var currentUser = Offtick.Business.Membership.MembershipManager.Instance.GetUser(User.Identity.Name);
            var basket = VirtualBasketManagerDB.Instance.fetchFromDB(currentUser);
            
            

            string refCode = "FakeRef_" + GeneralHelper.GetDateTimeToString(DateTime.Now);

            return Ok(new ServerResponse() { Status = ServerResponseStatus.OK, Data = new PayRequestResponseModel() {  OrderId=basket.OrderId, RefCode=refCode}, DataType = ServerResponseDataType.PayRequestResponse.ToString() });
        }


        
        [HttpPost]
        [Route("api/payment/comebackPgw")]
        public IHttpActionResult ComebackPgw()
        {
            
            string refId = string.Empty;
            short resCode = -1;
            long? saleReferenceId = -1;
            long? saleOrderId = -1;
            

            PaymentGatewayEndpoint endPoint = new PaymentGatewayEndpoint();
            if (!GeneralHelper.IsFakePayment())
            {
                endPoint.ComebackFromPaymentGateway(new PaymentGatewayComebackAction(
                    (_refId, _resCode, _saleOrderId, _saleReferenceId) =>
                    {
                        refId = _refId;
                        resCode = _resCode;
                        saleOrderId = _saleOrderId;
                        saleReferenceId = _saleReferenceId;
                    }
                ), null, false, false);
            }
            else
            {//fake request. just get parameters from request context
                var Request = System.Web.HttpContext.Current.Request;
                refId = Request.Params["RefId"];
                resCode = short.Parse(Request.Params["ResCode"]);
                saleOrderId = long.Parse(Request.Params["SaleOrderId"]);
                var _basketOrder = VirtualBasketManagerDB.Instance.FetchBasketEntitybyOrderId(saleOrderId.ToString());
                saleReferenceId = long.Parse(Request.Params["SaleReferenceId"]);
            }

            if (!saleOrderId.HasValue)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OrderResponseFromPaymentGatewaySaleOrderIdIsEmpty });
            
            var basketOrder = VirtualBasketManagerDB.Instance.FetchBasketEntitybyOrderId(saleOrderId.Value.ToString());
            if (basketOrder == null) //basket has been changed during payment. so we cancel payments and call reversal action
            {
                if (saleOrderId.HasValue && saleReferenceId.HasValue)
                    endPoint.ReversalRequest(null, saleOrderId.Value, saleOrderId.Value, saleReferenceId.Value, null, null);
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OrderBasketChangedSaleOrderIdNotExisted });
            }
             
            bool isSuccessVerify = false;
            long longOrderId = long.Parse(basketOrder.OrderId);
            if (resCode == 0)//initial payment is success ,now it's time to verify payment, if successfully verify request then whole process is completed and show results to client
            {
                if (!GeneralHelper.IsFakePayment())
                    endPoint.VerifyRequest(basketOrder.UserId, longOrderId, longOrderId, saleReferenceId.Value, new PaymentGatewaySuccessAction((l1) => { isSuccessVerify = true; })
                        , new PaymentGatewayFailedAction((l2, l3, exeptionMessage) => { isSuccessVerify = false; }));
                else// if fake payment just set result to success
                    isSuccessVerify = true;
            }
            PaymentResult model = new PaymentResult()
            {
                refId = refId,
                resCode = resCode,
                saleOrderId = saleOrderId,
                saleReferenceId = saleReferenceId,
                isSucess = isSuccessVerify,
            }; ;
            if (isSuccessVerify)//whole payment process is success so  do sale action and then send results to client
            {
                // register offer order sale
                OfferController offerController = new OfferController(DataContextManager.Container);
                var user = MembershipManager.Instance.GetUser(basketOrder.UserId);
                foreach (var entity in basketOrder.Entities)
                {
                    var offer = OfferManager.Instance.GetOfferFood(entity.EntityId).Offer;
                    SaleFoodOffer(user, offer, saleReferenceId.Value.ToString(), (short)entity.Count, saleOrderId.Value, entity.IsBuyingVisible);
                }
                //remove from basket
                VirtualBasketManagerDB.Instance.ProcessSaleDoneForUser(user);
            }
            return Ok(new ServerResponse() { Status = ServerResponseStatus.OK, Data = model, DataType = ServerResponseDataType.PaymentResult.ToString() });

        }


        private void SaleFoodOffer(MembershipUser user, Offer offer, string saleReferenceId, short count, long saleOrderId, bool isBuyingVisible = true)
        {

            var actionStatus = OfferManager.Instance.OrderFoodOffer(offer, user, count, saleOrderId, saleReferenceId, isBuyingVisible, PdfServiceGenerator.CreateDefaultPdfService());
            if (actionStatus == AddStatus.Added)
            {
                string messageBody = string.Format("user:{0} requests {1} units of {2}, mobile: {3}, email: {4}", user.FirstName + " " + user.LastName, count, offer.Title, user.PhoneNumber, user.Email);
                var notification = new SendMessageNotification()
                {
                    FromUserName = user.UserName,
                    Message = messageBody,
                    ToUserName = offer.MembershipUser.UserName,
                    Flags = 1,
                    MessageType = "0"
                };
                ChatService chService = new ChatService();
                chService.SRpcSendMessage(notification);
                //start send sms one for saler and one for bayer
                string baySaleDate = Offtick.Core.Utility.PersianTools.PersianDateConvertor.ToKhorshidiDate(DateTime.Now);

                string salerName = offer.MembershipUser.FirstName;
                var saleOfferNumber = saleOrderId;
                var saleOfferPrice = offer.OfferFoods.FirstOrDefault().DiscountPrice * count;
                string salerSms = string.Format(Resources.UIMessages.SmsSaleCompanyReportSuccess, salerName, saleOfferNumber, baySaleDate, saleOfferPrice, count);
                string salerMobile = offer.MembershipUser.PhoneNumber;

                string bayerName = user.FirstName + " " + user.LastName;
                var tracingNo = saleOrderId;
                string bayerSms = string.Format(Resources.UIMessages.SmsSaleCustomerReportSuccess, bayerName, tracingNo, baySaleDate);
                string bayerMobile = user.PhoneNumber;

                Action actionSendSms = () =>
                {
                    if (!string.IsNullOrEmpty(salerMobile))
                        Offtick.Business.Services.Domain.SmsService.Instance.SendMagfaSms(salerMobile, salerSms);
                    if (!string.IsNullOrEmpty(bayerMobile))
                        Offtick.Business.Services.Domain.SmsService.Instance.SendMagfaSms(bayerMobile, bayerSms);
                };
                actionSendSms.BeginInvoke(null, null);
                // end of send sms for saler and buyer
            }
            else
            {
                //to do : an exception has been raised, althow we verify payment but updating sale  information has been faild. we must log this cases and later do settlment
            }
        }
    
    
    }
}
