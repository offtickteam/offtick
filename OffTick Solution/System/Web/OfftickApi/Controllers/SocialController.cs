﻿using Offtick.Business.Membership;
using Offtick.Business.Services.Managers;
using Offtick.Business.Services.Storage;
using Offtick.Business.Services.Storage.File;
using Offtick.Business.Web;
using Offtick.Core.EnumTypes;
using Offtick.Data.Context.ExpertOnlinerContexts;
using Offtick.Web.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Offtick.Web.Api.Controllers
{
    public class SocialController : WepApiBaseController
    {
        public SocialController(IDataContextContainer obj) : base(obj)
        {
        }

        [Authorize]
        [HttpPost]
        [Route("api/social/fetchFollowers")]
        public IHttpActionResult FetchFollowers(FetchPartiallyModel model)
        {
            if (model == null) return BadRequest();
            if (string.IsNullOrEmpty(model.UserName))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.UsernameEmpty });
            var userEntity = Offtick.Business.Membership.MembershipManager.Instance.GetUser(model.UserName);

            var lsSearchResult = userEntity != null ? userEntity.Follows.AsQueryable() : null;
            int total = lsSearchResult.Count();
            IList<PersonLightModel> lsResultToClient = new List<PersonLightModel>();

            if (lsSearchResult != null && total > 0)
            {
                int take = model.Limit.HasValue ? model.Limit.Value : 20;
                int skip = 0;
                if (model.Page.HasValue)
                    skip = (model.Page.Value - 1) * take;

                lsSearchResult = lsSearchResult.Skip(skip).Take(take);
                foreach (var follow in lsSearchResult)
                {
                    var follower = follow.MembershipUser1;
                    ImageFileManager imgFileManager = new ImageFileManager(follower.UserId.ToString());
                    string imgAddress = imgFileManager.GetRelativeFullFileName(follow.MembershipUser1.UserId.ToString(), follow.MembershipUser1.Picture, true);

                    var userClientObj = new PersonLightModel()
                    {
                        firstName = follower.FirstName,
                        lastName = follower.LastName,
                        userName = follower.UserName,
                        imageThumbnail = imgAddress,
                        fallowStatus = FollowManager.Instance.GetFollowStatusFor(MembershipManager.Instance.GetCurrentUser(), follower),
                    };
                    lsResultToClient.Add(userClientObj);
                }
            }
            return Ok(new ServerResponse() { Status = ServerResponseStatus.OK, DataType = ServerResponseDataType.PersonLightModels.ToString(), Data = new { lsResultToClient, total } });
        }


        [Authorize]
        [HttpPost]
        [Route("api/social/fetchFollowing")]
        public IHttpActionResult FetchFollowing(FetchPartiallyModel model)
        {
            if (model == null) return BadRequest();
            if (string.IsNullOrEmpty(model.UserName))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.UsernameEmpty });
            var userEntity = Offtick.Business.Membership.MembershipManager.Instance.GetUser(model.UserName);

            var lsSearchResult = userEntity != null ? userEntity.Follows1.AsQueryable() : null;
            int total = lsSearchResult.Count();
            IList<PersonLightModel> lsResultToClient = new List<PersonLightModel>();

            if (lsSearchResult != null && total > 0)
            {
                int take = model.Limit.HasValue ? model.Limit.Value : 20;
                int skip = 0;
                if (model.Page.HasValue)
                    skip = (model.Page.Value - 1) * take;

                lsSearchResult = lsSearchResult.Skip(skip).Take(take);
                foreach (var follow in lsSearchResult)
                {
                    var following = follow.MembershipUser;
                    ImageFileManager imgFileManager = new ImageFileManager(following.UserId.ToString());
                    string imgAddress = imgFileManager.GetRelativeFullFileName(following.UserId.ToString(), following.Picture, true);

                    var userClientObj = new PersonLightModel
                    {
                        firstName = following.FirstName,
                        lastName = following.LastName,
                        userName = following.UserName,
                        imageThumbnail = imgAddress,
                        fallowStatus = FollowManager.Instance.GetFollowStatusFor(MembershipManager.Instance.GetCurrentUser(), following),
                    };
                    lsResultToClient.Add(userClientObj);
                }
            }
            return Ok(new ServerResponse() { Status = ServerResponseStatus.OK, DataType = ServerResponseDataType.PersonLightModels.ToString(), Data = new { lsResultToClient, total } });
        }


        [Authorize]
        [HttpPost]
        [Route("api/social/requestFollow")]
        public IHttpActionResult RequestFollow(string userNameToRequestFollow)
        {
            {

                if (string.IsNullOrWhiteSpace(userNameToRequestFollow))
                    return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.FollowUserNameToRequestEmpty });

                var mainUser = MembershipManager.Instance.GetUser(userNameToRequestFollow);
                if (mainUser == null)
                    return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.FollowUserNameToRequestNotExist });

                string followerUserName = User.Identity.Name;
                var followerUserEntity = MembershipManager.Instance.GetUser(followerUserName);

                var followRequested = followerUserEntity != null ? followerUserEntity.FollowRequesteds1.FirstOrDefault(e => e.MembershipUser.UserName.Equals(userNameToRequestFollow)) : null;

                if (followRequested == null)
                {
                    followRequested = FollowManager.Instance.RequestAFallow(followerUserEntity, mainUser);
                    if (followRequested != null)
                        return Ok(new ServerResponse() { Status = ServerResponseStatus.OK });
                }
                return Ok(new ServerResponse() { Status = ServerResponseStatus.AccessDenied, ErrorCode = ErrorCode.FollowRequestExistedBefore });
            }
        }

        [Authorize]
        [HttpPost]
        [Route("api/social/fetchFollowRequested")]
        public IHttpActionResult FetchFollowRequested(FetchPartiallyModel model)
        {
            var userEntity = MembershipManager.Instance.GetUser(User.Identity.Name);
            var lsSearchResult = userEntity != null ? userEntity.FollowRequesteds.AsEnumerable() : null;
            int total = lsSearchResult.Count();
            IList<PersonLightModel> lsResultToClient = new List<PersonLightModel>();

            if (lsSearchResult != null && total > 0)
            {
                int take = model.Limit.HasValue ? model.Limit.Value : 20;
                int skip = 0;
                if (model.Page.HasValue)
                    skip = (model.Page.Value - 1) * take;

                lsSearchResult = lsSearchResult.Skip(skip).Take(take);

                foreach (var followRequest in lsSearchResult)
                {
                    var follower = followRequest.MembershipUser1;
                    ImageFileManager imgFileManager = new ImageFileManager(follower.UserId.ToString());
                    string imgAddress = imgFileManager.GetRelativeFullFileName(follower.UserId.ToString(), follower.Picture, true);
                    var userClientObj = new PersonLightModel
                    {
                        firstName = follower.FirstName,
                        lastName = follower.LastName,
                        userName = follower.UserName,
                        imageThumbnail = imgAddress,
                        id = followRequest.FollowRequestedId,
                    };
                    lsResultToClient.Add(userClientObj);
                }
            }
            return Ok(new ServerResponse() { Status = ServerResponseStatus.OK, DataType = ServerResponseDataType.PersonLightModels.ToString(), Data = new { lsResultToClient, total } });
        }

        [Authorize]
        [HttpPost]
        [Route("api/social/fetchFollowRejected")]
        public IHttpActionResult FetchFollowRejected(FetchPartiallyModel model)
        {
            var userEntity = MembershipManager.Instance.GetUser(User.Identity.Name);
            var lsSearchResult = userEntity != null ? userEntity.FollowRejecteds1.AsEnumerable() : null;
            int total = lsSearchResult.Count();
            IList<PersonLightModel> lsResultToClient = new List<PersonLightModel>();

            if (lsSearchResult != null && total > 0)
            {
                int take = model.Limit.HasValue ? model.Limit.Value : 20;
                int skip = 0;
                if (model.Page.HasValue)
                    skip = (model.Page.Value - 1) * take;

                lsSearchResult = lsSearchResult.Skip(skip).Take(take);
                foreach (var followRequest in lsSearchResult)
                {
                    var follower = followRequest.MembershipUser1;
                    ImageFileManager imgFileManager = new ImageFileManager(follower.UserId.ToString());
                    string imgAddress = imgFileManager.GetRelativeFullFileName(follower.UserId.ToString(), follower.Picture, true);
                    var userClientObj = new PersonLightModel
                    {
                        firstName = follower.FirstName,
                        lastName = follower.LastName,
                        userName = follower.UserName,
                        imageThumbnail = imgAddress,
                    };
                    lsResultToClient.Add(userClientObj);
                }
            }
            return Ok(new ServerResponse() { Status = ServerResponseStatus.OK, DataType = ServerResponseDataType.PersonLightModels.ToString(), Data = new { lsResultToClient, total } });

        }


        [Authorize]
        [HttpPost]
        [Route("api/social/acceptFollowRequest")]
        public IHttpActionResult AcceptFollowRequest(long followRequestedId)
        {

            var followRequest = FollowManager.Instance.GetFollowRequestedById(followRequestedId);
            if (followRequest == null)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.FollowRequestedNotExist });
            if (!followRequest.MembershipUser.UserName.Equals(User.Identity.Name))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.AccessDenied });

            Follow follow = FollowManager.Instance.AcceptAFollow(followRequest, string.Empty);
            return Ok(new ServerResponse() { Status = ServerResponseStatus.OK });
        }

        [Authorize]
        [HttpPost]
        [Route("api/social/rejectFollowRequest")]
        public IHttpActionResult RejectFollowRequest(long followRequestedId)
        {
            var followRequest = FollowManager.Instance.GetFollowRequestedById(followRequestedId);
            if (followRequest == null)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.FollowRequestedNotExist });
            if (!followRequest.MembershipUser.UserName.Equals(User.Identity.Name))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.AccessDenied });

            FollowRejected followRejected = FollowManager.Instance.RejectAFollow(followRequest, string.Empty);
            return Ok(new ServerResponse() { Status = ServerResponseStatus.OK });
        }


        [Authorize]
        [HttpPost]
        [Route("api/social/unFollow")]
        public IHttpActionResult UnFollow(string userNameToUnFollow)
        {
            if (string.IsNullOrWhiteSpace(userNameToUnFollow))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.UsernameEmpty });

            var userEntity = MembershipManager.Instance.GetUser(User.Identity.Name);
            var follow = userEntity.Follows1.FirstOrDefault(e => e.MembershipUser.UserName.Equals(userNameToUnFollow));
            if (follow == null)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.AccessDenied, ErrorCode = ErrorCode.FollowNotExist });

            var removed = FollowManager.Instance.RemoveAFollow(follow);
            if (removed)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.OK });
            return Ok(new ServerResponse() { Status = ServerResponseStatus.ServerInternalError });
        }

        [Authorize]
        [HttpPost]
        [Route("api/social/removeFollower")]
        public IHttpActionResult RemoveFollower(string followerUserName)
        {
            if (string.IsNullOrWhiteSpace(followerUserName))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.UsernameEmpty });

            var userEntity = MembershipManager.Instance.GetUser(User.Identity.Name);
            var follow = userEntity.Follows.FirstOrDefault(e => e.MembershipUser1.UserName.Equals(followerUserName));
            if (follow == null)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.AccessDenied, ErrorCode = ErrorCode.FollowNotExist });

            var removed = FollowManager.Instance.RemoveAFollow(follow);
            if (removed)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.OK });
            return Ok(new ServerResponse() { Status = ServerResponseStatus.ServerInternalError });
        }

        [Authorize]
        [HttpPost]
        [Route("api/social/fetchProfileInfo")]
        public IHttpActionResult FetchProfileInfo(string userName)
        {
            var currentUserEntity = MembershipManager.Instance.GetUser(User.Identity.Name);
            var userEntity = MembershipManager.Instance.GetUser(userName);
            if (userEntity == null)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.UsernameNotExisted });

            int countOfFollower = userEntity.Follows.Count();
            int countOfFollowing = userEntity.Follows1.Count();
            int countOfPost = userEntity.Offers.Where(e => e.OfferType != (int)0).Count();

            ImageFileManager imgFileManager = new ImageFileManager(userEntity.UserId.ToString());
            string img32Address = string.Empty;
            if (!string.IsNullOrEmpty(userEntity.Picture))
                img32Address = imgFileManager.GetRelativeFullFileName(userEntity.UserId.ToString(), userEntity.Picture, true);
            
            var followStatus = FollowManager.Instance.GetFollowStatusFor(currentUserEntity, userEntity);// userEntity.Follows1.FirstOrDefault(e => e.UserId == followerUserEntity.UserId) != null ? 1 : 0;
            bool isBlocked = false;
            var userClientObj = new ProfileHeaderInfo
            {
                firstName = userEntity.FirstName,
                lastName = userEntity.LastName,
                userName = userEntity.UserName,
                imageThumbnail = img32Address,
                countOfFollower = countOfFollower,
                countOfFollowing = countOfFollowing,
                countOfPost = countOfPost,
                age = userEntity.BirthDate.HasValue ? (int)((DateTime.Now - userEntity.BirthDate.Value).TotalDays / 365) : 0,
                fallowStatus = followStatus,
                isBlocked = isBlocked ? (short)1 : (short)0
            };
            return Ok(new ServerResponse() { Status = ServerResponseStatus.OK, Data=userClientObj,DataType= ServerResponseDataType.ProfileHeaderInfo.ToString() });
        }

        [Authorize]
        [HttpPost]
        [Route("api/social/fetchFollowStatus")]
        public IHttpActionResult FetchFollowStatus()
        {
            Dictionary<int,string> dic = new Dictionary<int, string>();
            foreach(var followStatus in Enum.GetValues(typeof(FollowStatus))){
                dic.Add((int)followStatus, followStatus.ToString());
            }
            return Ok(new ServerResponse() { Status = ServerResponseStatus.OK,Data=dic,DataType=ServerResponseDataType.Dictionary.ToString() });
        }
    }
}
