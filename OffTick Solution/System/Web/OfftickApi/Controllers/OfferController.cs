﻿using Offtick.Business.Membership;
using Offtick.Business.Services.Managers;
using Offtick.Business.Services.RealtimeSearvices;
using Offtick.Business.Services.Storage;
using Offtick.Business.Web;
using Offtick.Core.EnumTypes;
using Offtick.Core.Utility.PersianTools;
using Offtick.Web.DrOnlinerEnterpriseWeb.Infrustracture;
using Offtick.Data.Context.ExpertOnlinerContexts;
using Offtick.Web.Api.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;

namespace Offtick.Web.Api.Controllers
{
    public class OfferController : WepApiBaseController
    {
        public OfferController(IDataContextContainer obj) : base(obj)
        {
        }

        [Authorize(Roles = "Company, Resturant, Administrator, Tester, Developer")]
        [HttpPost]
        [Route("api/offer/UploadOffer")]
        public IHttpActionResult UploadOffer()
        {
            var currentUser = MembershipManager.Instance.GetUser(User.Identity.Name);

            string generalOfferTypeStr = System.Web.HttpContext.Current.Request.Form.Get("generalOfferType");
            short generalOfferType;
            if (string.IsNullOrWhiteSpace(generalOfferTypeStr) || !short.TryParse(generalOfferTypeStr, out generalOfferType))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferGeneralTypeIsNotValid });
            if (!(generalOfferType == (short)EnumOfferType.Food || generalOfferType == (short)EnumOfferType.Tour))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferGeneralTypeIsNotValid });

            string foodTitle = System.Web.HttpContext.Current.Request.Form.Get("title");
            if (string.IsNullOrWhiteSpace(foodTitle))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferTitleIsEmpty });

            string foodTitleLong = System.Web.HttpContext.Current.Request.Form.Get("titleLong");
            if (string.IsNullOrWhiteSpace(foodTitleLong))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferLongTitleIsEmpty });

            string url = System.Web.HttpContext.Current.Request.Form.Get("url");
            string foodOriginalPriceStr = System.Web.HttpContext.Current.Request.Form.Get("originalPrice");//long
            long originalPrice;
            if (string.IsNullOrWhiteSpace(foodOriginalPriceStr))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferOriginalPriceIsEmpty });
            if (!long.TryParse(foodOriginalPriceStr, out originalPrice))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferOriginalPriceInvalidFormat });

            string originalTitle = System.Web.HttpContext.Current.Request.Form.Get("originalTitle");
            string unit = System.Web.HttpContext.Current.Request.Form.Get("unit");//

            string discountPriceStr = System.Web.HttpContext.Current.Request.Form.Get("discountPrice");
            long discountPrice;
            if (string.IsNullOrWhiteSpace(discountPriceStr))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferDiscountPriceIsEmpty });
            if (!long.TryParse(discountPriceStr, out discountPrice))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferDiscountPriceInvalidFormat });


            string discountTitle = System.Web.HttpContext.Current.Request.Form.Get("discountTitle");
            string discountPercentStr = System.Web.HttpContext.Current.Request.Form.Get("discountPercent");//short
            short discountPercent;
            if (string.IsNullOrWhiteSpace(discountPercentStr))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferDiscountPercentageIsEmpty });
            if (!short.TryParse(discountPercentStr, out discountPercent) || discountPercent > 100 || discountPercent < 0)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferDiscountPercentageInvalidValue });


            string expireDateStr = System.Web.HttpContext.Current.Request.Form.Get("expireDate");//int
            int expireDate;
            if (string.IsNullOrWhiteSpace(expireDateStr))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferExpireDateIsEmpty });
            if (!int.TryParse(expireDateStr, out expireDate))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferExpireDateInvalidFormat });


            string quantityStr = System.Web.HttpContext.Current.Request.Form.Get("quantity");//short
            short quantity;
            if (string.IsNullOrWhiteSpace(quantityStr))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferQuantityIsEmpty });
            if (!short.TryParse(quantityStr, out quantity))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferQuantityInvalidFormat });


            string audeiences = System.Web.HttpContext.Current.Request.Form.Get("audeiences");
            string locationCityIdStr = System.Web.HttpContext.Current.Request.Form.Get("locationCityId");
            Guid locationCityId = default(Guid);
            //if (string.IsNullOrWhiteSpace(foodLocationCityIdStr))
            //  return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.CityEmpty });
            bool hasCityId = Guid.TryParse(locationCityIdStr, out locationCityId);
            if (!string.IsNullOrWhiteSpace(locationCityIdStr) && !hasCityId)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.CityIdInvalid });



            string locationAddress = System.Web.HttpContext.Current.Request.Form.Get("locationAddress");
            string offtickOfferTypeIdStr = System.Web.HttpContext.Current.Request.Form.Get("offtickOfferTypeId");//short
            short offtickOfferTypeId;
            if (string.IsNullOrWhiteSpace(offtickOfferTypeIdStr))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferTypeIsEmpty });
            if (!short.TryParse(offtickOfferTypeIdStr, out offtickOfferTypeId))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferTypeInvalidFormat });
            if (OfferManager.Instance.GetOfferOfftickTypeById(offtickOfferTypeId) == null)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferTypeNotExist });


            string showIncomeInPercentageStr = System.Web.HttpContext.Current.Request.Form.Get("showIncomeInPercentage");//bool
            bool showIncomeInPercentage;
            if (string.IsNullOrWhiteSpace(showIncomeInPercentageStr))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferShowIncomeInPercentageIsEmpty });
            if (!bool.TryParse(showIncomeInPercentageStr, out showIncomeInPercentage))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferShowIncomeInPercentageInvalidFormat });

            string hasAddedValueTaxStr = System.Web.HttpContext.Current.Request.Form.Get("hasAddedValueTax");//bool
            bool hasAddedValueTax;
            if (string.IsNullOrWhiteSpace(hasAddedValueTaxStr))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferHasTaxValueIsEmpty });
            if (!bool.TryParse(hasAddedValueTaxStr, out hasAddedValueTax))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferHasTaxValueInvalidFormat });


            var mainImagePostedFile = System.Web.HttpContext.Current.Request.Files.Get("mainImage");
            if (mainImagePostedFile.ContentLength == 0)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferMainImageIsNotProvided });
            var mainImage = new HttpPostedFileWrapper(mainImagePostedFile);
            var galleryImagesHttpPostedFile = System.Web.HttpContext.Current.Request.Files.GetMultiple("galleryImages");
            System.Web.HttpPostedFileBase[] galleryImages = new System.Web.HttpPostedFileBase[galleryImagesHttpPostedFile.Count];
            for (int i = 0; i < galleryImagesHttpPostedFile.Count; i++)
                if (galleryImagesHttpPostedFile[i].ContentLength > 0)
                    galleryImages[i] = new System.Web.HttpPostedFileWrapper(galleryImagesHttpPostedFile[i]);
            if (galleryImages.Length < 3)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferMinimumGalleryImagesIsNotProvided });
            if (galleryImages.Length > 10)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferGalleryImagesIsGreaterThanMaximumAllowded });


            var menuImagesHttpPostedFile = System.Web.HttpContext.Current.Request.Files.GetMultiple("menuImages");
            System.Web.HttpPostedFileBase[] menuImages = new System.Web.HttpPostedFileBase[menuImagesHttpPostedFile.Count];
            for (int i = 0; i < menuImagesHttpPostedFile.Count; i++)
                if (menuImagesHttpPostedFile[i].ContentLength > 0)
                    menuImages[i] = new System.Web.HttpPostedFileWrapper(menuImagesHttpPostedFile[i]);
            if (menuImages.Length > 10)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferMenuImagesIsGreaterThanMaximumAllowded });

            var specificationImage = System.Web.HttpContext.Current.Request.Files.Get("specification");
            var conditionsImage = System.Web.HttpContext.Current.Request.Files.Get("conditions");
            var descriptionsImage = System.Web.HttpContext.Current.Request.Files.Get("descriptions");





            DateTime tourWentDate = DateTime.MinValue;
            bool hasWentDate = false;
            DateTime tourReturnDate = DateTime.MinValue;
            bool hasReturnDate = false;
            string tourAgencyName = null;
            string tourTravelTypeStr;
            short tourTravelType = 0;
            bool tourShowBuyButton = false;
            bool tourShowOffButton = false;
            bool tourShowInstantButton = false;
            Guid tourSourceCityId = default(Guid);
            Guid tourDestinationCityId = default(Guid);
            bool hasSourceCityId = false;
            bool hasDestinationCityId = false;
            if (generalOfferType == (short)EnumOfferType.Food)
            {
                string tourWentDateStr = System.Web.HttpContext.Current.Request.Form.Get("tourWentDate");
                tourWentDate = PersianDateConvertor.ToGregorian(tourWentDateStr);
                hasWentDate = tourWentDate != DateTime.MinValue;
                string tourReturnDateStr = System.Web.HttpContext.Current.Request.Form.Get("tourReturnDate");
                tourReturnDate = PersianDateConvertor.ToGregorian(tourReturnDateStr);
                hasReturnDate = tourReturnDate != DateTime.MinValue;

                tourAgencyName = System.Web.HttpContext.Current.Request.Form.Get("tourAgencyName");
                tourTravelTypeStr = System.Web.HttpContext.Current.Request.Form.Get("tourTravelType");

                if (!short.TryParse(tourTravelTypeStr, out tourTravelType))
                    return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferTourTravelTypeInvalidValue });

                bool hasTrueTravelTypeValue = false;
                foreach (short travelTypeValue in Enum.GetValues(typeof(TravelTypeEnum)))
                    if (((short)travelTypeValue).Equals(tourTravelType))
                    { hasTrueTravelTypeValue = true; break; }
                if (!hasTrueTravelTypeValue)
                    return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferTourTravelTypeInvalidValue });


                string tourShowBuyButtonStr = System.Web.HttpContext.Current.Request.Form.Get("tourShowBuyButton");
                if (!bool.TryParse(tourShowBuyButtonStr, out tourShowBuyButton))
                    return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferTourShowBuyButtonInvalidValue });

                string tourShowOffButtonStr = System.Web.HttpContext.Current.Request.Form.Get("tourShowOffButton");
                if (!bool.TryParse(tourShowOffButtonStr, out tourShowOffButton))
                    return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferTourShowOffButtonInvalidValue });

                string tourShowInstantButtonStr = System.Web.HttpContext.Current.Request.Form.Get("tourShowInstantButton");
                if (!bool.TryParse(tourShowInstantButtonStr, out tourShowInstantButton))
                    return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferTourShowInstantButtonInvalidValue });


                string tourSourceCityIdStr = System.Web.HttpContext.Current.Request.Form.Get("tourSourceCityId");
                hasSourceCityId = Guid.TryParse(tourSourceCityIdStr, out tourSourceCityId);
                var sourceCity = MembershipManager.Instance.getCityById(tourSourceCityId);
                if (hasSourceCityId && sourceCity == null)
                    return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferTourSourceCityNotExist });


                string tourDestinationCityIdStr = System.Web.HttpContext.Current.Request.Form.Get("tourDestinationCityId");
                hasDestinationCityId = Guid.TryParse(tourDestinationCityIdStr, out tourDestinationCityId);
                var destinationCity = MembershipManager.Instance.getCityById(tourDestinationCityId);
                if (hasDestinationCityId && destinationCity == null)
                    return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferTourDestinationCityNotExist });

            }




            ChatService chatService = new ChatService();
            var response = chatService.SRpcSendOffer(new Offtick.Business.Services.RealtimeSearvices.EventDrivenDispatcher.Notification.OfferNotification()
            {
                Title = foodTitle,
                Body = foodTitleLong,
                Datetime = DateTime.Now.ToString(),
                FromUserName = currentUser.UserName,
                ToUserName = currentUser.UserName,
                OfferType = generalOfferType,//Food or Tour
            });

            if (response.ResultStatus == "Success")
            {

                string newFileName = string.Empty;
                var responseEdit = OfferManager.Instance.UploadOfferFile(currentUser.UserName, response.DataObject.ToString(), mainImage, out newFileName);
                if (responseEdit == Offtick.Core.EnumTypes.EditStatus.Edited)
                {
                    Guid offerId = Guid.Parse(response.DataObject.ToString());
                    var offer = OfferManager.Instance.GetOffer(offerId);
                    string specification = string.Empty;
                    string conditions = string.Empty;
                    string descriptions = string.Empty;
                    //Guid foodLocationCityId;

                    if (specificationImage != null && specificationImage.ContentLength > 0 && specificationImage.InputStream != null)
                    {
                        System.IO.TextReader fileReader = new System.IO.StreamReader(specificationImage.InputStream);
                        specification = fileReader.ReadToEnd();
                    }
                    if (conditionsImage != null && conditionsImage.ContentLength > 0 && conditionsImage.InputStream != null)
                    {
                        System.IO.TextReader fileReader = new System.IO.StreamReader(conditionsImage.InputStream);
                        conditions = fileReader.ReadToEnd();
                    }
                    if (descriptionsImage != null && descriptionsImage.ContentLength > 0 && descriptionsImage.InputStream != null)
                    {
                        System.IO.TextReader fileReader = new System.IO.StreamReader(descriptionsImage.InputStream);
                        descriptions = fileReader.ReadToEnd();
                    }
                    AddStatus addStatus;
                    if (generalOfferType == (short)EnumOfferType.Food)
                    {
                        addStatus = OfferManager.Instance.AddFoodOffer(offer
                           , galleryImages
                           , menuImages
                           , originalPrice
                           , originalTitle
                           , url
                           , unit
                           , discountPrice
                           , discountTitle
                           , discountPercent
                           , expireDate
                           , quantity
                           , audeiences
                           , specification
                           , conditions
                           , descriptions
                           , hasCityId ? (Guid?)locationCityId : null
                           , locationAddress
                           , offtickOfferTypeId
                           , showIncomeInPercentage
                           , hasAddedValueTax ? (Nullable<short>)GeneralHelper.GetTaxValueForCurrentYear() : null,
                           GeneralHelper.isConfirmedCompanyOfferOnInsert() ? ConfirmStatus.Confirmed : ConfirmStatus.NotConfirmed
                           );
                    }
                    else
                    {

                        addStatus = OfferManager.Instance.AddtourOffer(offer
                           , galleryImages
                           , menuImages
                           , originalPrice
                           , originalTitle
                           , url
                           , unit
                           , discountPrice
                           , discountTitle
                           , discountPercent
                           , expireDate
                           , quantity
                           , specification
                           , conditions
                           , descriptions
                           , locationAddress
                           , showIncomeInPercentage
                           , hasAddedValueTax ? (Nullable<short>)GeneralHelper.GetTaxValueForCurrentYear() : null
                           , tourShowBuyButton
                           , tourShowOffButton
                           , tourShowInstantButton
                           , hasSourceCityId ? (Guid?)tourSourceCityId : null
                           , hasDestinationCityId ? (Guid?)tourDestinationCityId : null
                           , hasWentDate ? (DateTime?)tourWentDate : null
                           , hasReturnDate ? (DateTime?)tourReturnDate : null
                           , tourAgencyName
                           , (TravelTypeEnum)tourTravelType
                           , GeneralHelper.isConfirmedCompanyOfferOnInsert() ? ConfirmStatus.Confirmed : ConfirmStatus.NotConfirmed
                           );

                    }
                    if (addStatus == AddStatus.Added)
                        return Ok(new ServerResponse() { Status = ServerResponseStatus.OK });
                }
            }
            return Ok(new ServerResponse() { Status = ServerResponseStatus.ServerInternalError });
        }


        [Authorize(Roles = "Company, Resturant, Administrator, Tester, Developer")]
        [HttpPost]
        [Route("api/offer/fetchMainPageBigOffer")]
        public IHttpActionResult FetchMainPageBigOffer()
        {
            var mainOffer = OfferManager.Instance.GetMaingPageBigOffer();
            OfferDTO dto = null;
            if (mainOffer != null && mainOffer.OfferFoods.FirstOrDefault() != null)
                dto = getDTOof(mainOffer);
            return Ok(new ServerResponse() { Status = ServerResponseStatus.OK, Data = dto, DataType = ServerResponseDataType.OfferDTO.ToString() });
        }




        [Authorize(Roles = "Company, Resturant, Administrator, Tester, Developer")]
        [HttpPost]
        [Route("api/offer/fetchBuyedOffersByUser")]
        public IHttpActionResult FetchBuyedOffersByUser(MultiPageRequestModel model)
        {
            if (model == null)
                return BadRequest();

            //check if currentUser has administration or higher privilage then allow him to do else rejectByAccessDenied;
            var currentUser = MembershipManager.Instance.GetUser(User.Identity.Name);
            if (!model.Id.Equals(currentUser.UserId))
            {
                if (currentUser.MembershipUsersInRoles.FirstOrDefault(e => e.MembershipRole.RoleName.Equals("Administrator") || e.MembershipRole.RoleName.Equals("Tester") || e.MembershipRole.RoleName.Equals("Developer")) == null)
                    return Ok(new ServerResponse() { Status = ServerResponseStatus.AccessDenied });
            }

            //check model.UserIsValid and existed
            var modelUser = MembershipManager.Instance.GetUser(model.Id);
            if (modelUser == null)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.UsernameNotExisted });


            int take = model.Limit.HasValue ? model.Limit.Value : 20;
            int skip = 0;
            if (model.Page.HasValue)
            {
                skip = (model.Page.Value - 1) * take;
            }

            var user = MembershipManager.Instance.GetUser(model.Id);
            var orderedOffers = OfferManager.Instance.getAllOfBuyedOffersForUserName(model.Id);
            var buyOffers = orderedOffers.Select(o => new
            {
                OrderId = o.OfferFoodOrderId,
                Title = o.OfferFood.Offer.Title,
                LongTitle = o.OfferFood.Offer.Body,
                Offerid = o.OfferFood.Offer.OfferId,
                SallerUserId = o.OfferFood.Offer.MembershipUser.UserId,
                SallerUserName = o.OfferFood.Offer.MembershipUser.UserName,
                FileName = o.OfferFood.Offer.FileName,
                DateTime = o.DateTime,
                Quantity = o.Quantity,
                PaymentAmount = o.PaymentAmount,
                DiscountAmount = (o.OfferFood.OriginalPrice - o.OfferFood.DiscountPrice) * o.Quantity,
                SaleRefrenceId = o.SaleRefrenceId,
                Details = o.OfferFoodOrderDetails
            }).OrderByDescending(e => e.DateTime);

            int total = buyOffers.Count();
            if (!string.IsNullOrEmpty(model.SortBy) && !string.IsNullOrEmpty(model.Direction))
            {
                if (model.Direction.Trim() == "asc")
                {
                    switch (model.SortBy)
                    {
                        case "Title":
                            buyOffers = buyOffers.OrderBy(q => q.Title);
                            break;
                        case "LongTitle":
                            buyOffers = buyOffers.OrderBy(q => q.LongTitle);
                            break;
                        case "SallerUserName":
                            buyOffers = buyOffers.OrderBy(q => q.SallerUserName);
                            break;
                        case "DateTime":
                            buyOffers = buyOffers.OrderBy(q => q.DateTime);
                            break;
                        case "Quantity":
                            buyOffers = buyOffers.OrderBy(q => q.Quantity);
                            break;
                        case "PaymentAmount":
                            buyOffers = buyOffers.OrderBy(q => q.PaymentAmount);
                            break;
                        case "DiscountAmount":
                            buyOffers = buyOffers.OrderBy(q => q.DiscountAmount);
                            break;
                    }
                }
                else
                {
                    switch (model.SortBy)
                    {
                        case "Title":
                            buyOffers = buyOffers.OrderByDescending(q => q.Title);
                            break;
                        case "LongTitle":
                            buyOffers = buyOffers.OrderByDescending(q => q.LongTitle);
                            break;
                        case "SallerUserName":
                            buyOffers = buyOffers.OrderByDescending(q => q.SallerUserName);
                            break;
                        case "DateTime":
                            buyOffers = buyOffers.OrderByDescending(q => q.DateTime);
                            break;
                        case "Quantity":
                            buyOffers = buyOffers.OrderByDescending(q => q.Quantity);
                            break;
                        case "PaymentAmount":
                            buyOffers = buyOffers.OrderByDescending(q => q.PaymentAmount);
                            break;
                        case "DiscountAmount":
                            buyOffers = buyOffers.OrderByDescending(q => q.DiscountAmount);
                            break;
                    }
                }
            }

            int count = buyOffers.Count();
            var records = buyOffers.Skip(skip).Take(take).ToArray();
            IList<OfferOrderDTO> offerDTOs = new List<OfferOrderDTO>();
            foreach (var record in records)
            {
                var dto = new OfferOrderDTO()
                {
                    OfferOrderId = record.OrderId,
                    DateTime = record.DateTime,
                    DateTimePersian = Offtick.Core.Utility.PersianTools.PersianDateConvertor.ToKhorshidiDate(record.DateTime),
                    Description = "",
                    Title = record.Title,
                    LongTitle = record.LongTitle,
                    OfferId = record.Offerid,
                    SallerUserId = record.SallerUserId,
                    BuyerUserId = model.Id,
                    BuyerUserName = modelUser.UserName,
                    SallerUserName = record.SallerUserName,
                    Quantity = record.Quantity,
                    PaymentAmount = record.PaymentAmount.HasValue ? record.PaymentAmount.Value : 0,
                    DiscountAmount = record.DiscountAmount,
                    SaleRefrenceId = record.SaleRefrenceId
                };
                foreach (var detail in record.Details)
                    dto.Details.Add(new OfferOrderDetailDTO() { FileName = detail.FileName, OfferCode = detail.OrderCode, UseStatus = detail.Status });
                offerDTOs.Add(dto);
            }
            return Ok(new ServerResponse() { Status = ServerResponseStatus.OK, Data = new { Data = offerDTOs, Count = count }, DataType = ServerResponseDataType.OfferOrderDTOList.ToString() });
        }


        [Authorize(Roles = "Company, Resturant, Administrator, Tester, Developer")]
        [HttpPost]
        [Route("api/offer/fetchSoldOffersByUser")]
        public IHttpActionResult FetchSoldOffersByUser(MultiPageRequestModel model)
        {
            if (model == null)
                return BadRequest();

            //check if currentUser has administration or higher privilage then allow him to do else rejectByAccessDenied;
            var currentUser = MembershipManager.Instance.GetUser(User.Identity.Name);
            if (!model.Id.Equals(currentUser.UserId))
            {
                if (currentUser.MembershipUsersInRoles.FirstOrDefault(e => e.MembershipRole.RoleName.Equals("Administrator") || e.MembershipRole.RoleName.Equals("Tester") || e.MembershipRole.RoleName.Equals("Developer")) == null)
                    return Ok(new ServerResponse() { Status = ServerResponseStatus.AccessDenied });
            }

            //check model.UserIsValid and existed
            var modelUser = MembershipManager.Instance.GetUser(model.Id);
            if (modelUser == null)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.UsernameNotExisted });


            int take = model.Limit.HasValue ? model.Limit.Value : 20;
            int skip = 0;
            if (model.Page.HasValue)
            {
                skip = (model.Page.Value - 1) * take;
            }

            var user = MembershipManager.Instance.GetUser(model.Id);
            var orderedOffers = OfferManager.Instance.getAllOfSoldOffersForUserName(model.Id);
            var orderedOffersQuery = orderedOffers.Select(o => new
            {
                OrderId = o.OfferFoodOrderId,
                Title = o.OfferFood.Offer.Title,
                LongTitle = o.OfferFood.Offer.Body,
                Offerid = o.OfferFood.Offer.OfferId,
                SallerUserId = o.OfferFood.Offer.MembershipUser.UserId,
                SallerUserName = o.OfferFood.Offer.MembershipUser.UserName,
                FileName = o.OfferFood.Offer.FileName,
                DateTime = o.DateTime,
                Quantity = o.Quantity,
                PaymentAmount = o.PaymentAmount,
                DiscountAmount = (o.OfferFood.OriginalPrice - o.OfferFood.DiscountPrice) * o.Quantity,
                SaleRefrenceId = o.SaleRefrenceId,
                Details = o.OfferFoodOrderDetails
            }).OrderByDescending(e => e.DateTime);

            int total = orderedOffers.Count();
            if (!string.IsNullOrEmpty(model.SortBy) && !string.IsNullOrEmpty(model.Direction))
            {
                if (model.Direction.Trim() == "asc")
                {
                    switch (model.SortBy)
                    {
                        case "Title":
                            orderedOffersQuery = orderedOffersQuery.OrderBy(q => q.Title);
                            break;
                        case "LongTitle":
                            orderedOffersQuery = orderedOffersQuery.OrderBy(q => q.LongTitle);
                            break;
                        case "SallerUserName":
                            orderedOffersQuery = orderedOffersQuery.OrderBy(q => q.SallerUserName);
                            break;
                        case "DateTime":
                            orderedOffersQuery = orderedOffersQuery.OrderBy(q => q.DateTime);
                            break;
                        case "Quantity":
                            orderedOffersQuery = orderedOffersQuery.OrderBy(q => q.Quantity);
                            break;
                        case "PaymentAmount":
                            orderedOffersQuery = orderedOffersQuery.OrderBy(q => q.PaymentAmount);
                            break;
                        case "DiscountAmount":
                            orderedOffersQuery = orderedOffersQuery.OrderBy(q => q.DiscountAmount);
                            break;
                    }
                }
                else
                {
                    switch (model.SortBy)
                    {
                        case "Title":
                            orderedOffersQuery = orderedOffersQuery.OrderByDescending(q => q.Title);
                            break;
                        case "LongTitle":
                            orderedOffersQuery = orderedOffersQuery.OrderByDescending(q => q.LongTitle);
                            break;
                        case "SallerUserName":
                            orderedOffersQuery = orderedOffersQuery.OrderByDescending(q => q.SallerUserName);
                            break;
                        case "DateTime":
                            orderedOffersQuery = orderedOffersQuery.OrderByDescending(q => q.DateTime);
                            break;
                        case "Quantity":
                            orderedOffersQuery = orderedOffersQuery.OrderByDescending(q => q.Quantity);
                            break;
                        case "PaymentAmount":
                            orderedOffersQuery = orderedOffersQuery.OrderByDescending(q => q.PaymentAmount);
                            break;
                        case "DiscountAmount":
                            orderedOffersQuery = orderedOffersQuery.OrderByDescending(q => q.DiscountAmount);
                            break;
                    }
                }
            }

            int count = orderedOffers.Count();
            var records = orderedOffersQuery.Skip(skip).Take(take).ToArray();
            IList<OfferOrderDTO> offerDTOs = new List<OfferOrderDTO>();
            foreach (var record in records)
            {
                var dto = new OfferOrderDTO()
                {
                    OfferOrderId = record.OrderId,
                    DateTime = record.DateTime,
                    DateTimePersian = Offtick.Core.Utility.PersianTools.PersianDateConvertor.ToKhorshidiDate(record.DateTime),
                    Description = "",
                    Title = record.Title,
                    LongTitle = record.LongTitle,
                    OfferId = record.Offerid,
                    SallerUserId = record.SallerUserId,
                    BuyerUserId = model.Id,
                    BuyerUserName = modelUser.UserName,
                    SallerUserName = record.SallerUserName,
                    Quantity = record.Quantity,
                    PaymentAmount = record.PaymentAmount.HasValue ? record.PaymentAmount.Value : 0,
                    DiscountAmount = record.DiscountAmount,
                    SaleRefrenceId = record.SaleRefrenceId
                };
                foreach (var detail in record.Details)
                    dto.Details.Add(new OfferOrderDetailDTO() { FileName = detail.FileName, OfferCode = detail.OrderCode, UseStatus = detail.Status });
                offerDTOs.Add(dto);
            }
            return Ok(new ServerResponse() { Status = ServerResponseStatus.OK, Data = new { Data = offerDTOs, Count = count }, DataType = ServerResponseDataType.OfferOrderDTOList.ToString() });

        }


        [Authorize(Roles = "Company, Resturant, Administrator, Tester, Developer")]
        [HttpPost]
        [Route("api/offer/fetchOfferVisitors")]
        public IHttpActionResult FetchOfferVisitors(MultiPageRequestModel model)
        {
            if (model == null)
                return BadRequest();

            //check if currentUser has administration or higher privilage then allow him to do else rejectByAccessDenied;
            var currentUser = MembershipManager.Instance.GetUser(User.Identity.Name);
            /*   if (!model.Id.Equals(currentUser.UserId))
               {
                   if (currentUser.MembershipUsersInRoles.FirstOrDefault(e => e.MembershipRole.RoleName.Equals("Administrator") || e.MembershipRole.RoleName.Equals("Tester") || e.MembershipRole.RoleName.Equals("Developer")) == null)
                       return Ok(new ServerResponse() { Status = ServerResponseStatus.AccessDenied });
               }*/
            int take = model.Limit.HasValue ? model.Limit.Value : 20;
            int skip = 0;
            if (model.Page.HasValue)
            {
                skip = (model.Page.Value - 1) * take;
            }


            //check model.UserIsValid and existed
            var offer = Offtick.Business.Services.Managers.OfferManager.Instance.GetOffer(model.Id);
            if (offer == null)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferNotExist });

            bool showViewers = offer.OfferPrivacy != null ? offer.OfferPrivacy.IsViewerVisible : true;
            MembershipUserLightModel[] visitors = null;
            int count = 0;
            if (showViewers)
            {
                //to do : get Online status from corresponding service
                //to do : get Thumbnail
                var query = from v in offer.OfferVisiteds
                            where (v.MembershipUser.MembershipPrivacy != null && v.MembershipUser.MembershipPrivacy.IsViewingVisible) || v.MembershipUser.MembershipPrivacy == null
                            select new MembershipUserLightModel() { Firstname = v.MembershipUser.FirstName, SecondName = v.MembershipUser.LastName, UserId = v.MembershipUser.UserId, isOnline = false, Thumbnail = v.MembershipUser.Picture, UserName = v.MembershipUser.UserName };
                count = query.Count();
                visitors = query.Skip(skip).Take(take).ToArray();
            }
            foreach (var visitor in visitors)
            {
                string imgSrc = Offtick.Web.DrOnlinerEnterpriseWeb.Infrustracture.GeneralHelper.GetThumbnail32Of(visitor.UserId, visitor.Thumbnail, false);
                if (System.IO.File.Exists(imgSrc))
                {
                    Image img = Image.FromFile(imgSrc);
                    using (MemoryStream mStream = new MemoryStream())
                    {
                        img.Save(mStream, img.RawFormat);
                        string base64 = Convert.ToBase64String(mStream.ToArray());
                        visitor.Thumbnail = base64;
                    }
                }
                else visitor.Thumbnail = "";

            }
            return Ok(new ServerResponse() { Status = ServerResponseStatus.OK, Data = new { Data = visitors, Count = count }, DataType = ServerResponseDataType.MembershipUserLightModelList.ToString() });
        }



        [Authorize(Roles = "Company, Resturant, Administrator, Tester, Developer")]
        [HttpPost]
        [Route("api/offer/fetchOfferBuyers")]
        public IHttpActionResult FetchOfferBuyers(MultiPageRequestModel model)
        {
            if (model == null)
                return BadRequest();

            //check if currentUser has administration or higher privilage then allow him to do else rejectByAccessDenied;
            var currentUser = MembershipManager.Instance.GetUser(User.Identity.Name);
            /* if (!model.Id.Equals(currentUser.UserId))
             {
                 if (currentUser.MembershipUsersInRoles.FirstOrDefault(e => e.MembershipRole.RoleName.Equals("Administrator") || e.MembershipRole.RoleName.Equals("Tester") || e.MembershipRole.RoleName.Equals("Developer")) == null)
                     return Ok(new ServerResponse() { Status = ServerResponseStatus.AccessDenied });
             }*/
            int take = model.Limit.HasValue ? model.Limit.Value : 20;
            int skip = 0;
            if (model.Page.HasValue)
            {
                skip = (model.Page.Value - 1) * take;
            }


            //check model.UserIsValid and existed
            var offer = Offtick.Business.Services.Managers.OfferManager.Instance.GetOffer(model.Id);
            if (offer == null)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferNotExist });

            bool showBuyers = offer.OfferPrivacy != null ? offer.OfferPrivacy.IsBuyerVisible : true;
            MembershipUserLightModel[] buyers = null;
            int count = 0;
            if (showBuyers)
            {
                //to do : get Online status from corresponding service
                var query = (from o in offer.OfferFoods.FirstOrDefault().OfferFoodOrders
                             where (o.MembershipUser.MembershipPrivacy != null && o.MembershipUser.MembershipPrivacy.IsBuyingVisible) || o.MembershipUser.MembershipPrivacy == null
                             select new MembershipUserLightModel() { Firstname = o.MembershipUser.FirstName, SecondName = o.MembershipUser.LastName, UserId = o.MembershipUser.UserId, isOnline = false, Thumbnail = o.MembershipUser.Picture, UserName = o.MembershipUser.UserName });
                //to do : get only distinct users
                count = query.Count();
                buyers = query.Skip(skip).Take(take).ToArray();
            }
            foreach (var buyer in buyers)
            {
                string imgSrc = Offtick.Web.DrOnlinerEnterpriseWeb.Infrustracture.GeneralHelper.GetThumbnail32Of(buyer.UserId, buyer.Thumbnail, false);
                if (System.IO.File.Exists(imgSrc))
                {
                    Image img = Image.FromFile(imgSrc);
                    using (MemoryStream mStream = new MemoryStream())
                    {
                        img.Save(mStream, img.RawFormat);
                        string base64 = Convert.ToBase64String(mStream.ToArray());
                        buyer.Thumbnail = base64;
                    }
                }
                else buyer.Thumbnail = "";

            }
            return Ok(new ServerResponse() { Status = ServerResponseStatus.OK, Data = new { Data = buyers, Count = count }, DataType = ServerResponseDataType.MembershipUserLightModelList.ToString() });
        }


        [Authorize(Roles = "Company, Resturant, Administrator, Tester, Developer")]
        [HttpPost]
        [Route("api/offer/fetchOfferLIkers")]
        public IHttpActionResult FetchOfferLIkers(MultiPageRequestModel model)
        {
            if (model == null)
                return BadRequest();

            //check if currentUser has administration or higher privilage then allow him to do else rejectByAccessDenied;
            var currentUser = MembershipManager.Instance.GetUser(User.Identity.Name);
            /* if (!model.Id.Equals(currentUser.UserId))
             {
                 if (currentUser.MembershipUsersInRoles.FirstOrDefault(e => e.MembershipRole.RoleName.Equals("Administrator") || e.MembershipRole.RoleName.Equals("Tester") || e.MembershipRole.RoleName.Equals("Developer")) == null)
                     return Ok(new ServerResponse() { Status = ServerResponseStatus.AccessDenied });
             }*/
            int take = model.Limit.HasValue ? model.Limit.Value : 20;
            int skip = 0;
            if (model.Page.HasValue)
            {
                skip = (model.Page.Value - 1) * take;
            }


            //check model.UserIsValid and existed
            var offer = Offtick.Business.Services.Managers.OfferManager.Instance.GetOffer(model.Id);
            if (offer == null)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferNotExist });

            //to do : check isShowLikers from OfferPrivacy
            // bool showLikers = offer.OfferPrivacy != null ? offer.OfferPrivacy.IsBuyerVisible : true;
            bool showLikers = true;
            MembershipUserLightModel[] buyers = null;
            int count = 0;
            if (showLikers)
            {
                //to do : get Online status from corresponding service
                var query = (from o in offer.OfferLikes
                             where (o.MembershipUser.MembershipPrivacy != null && o.MembershipUser.MembershipPrivacy.IsBuyingVisible) || o.MembershipUser.MembershipPrivacy == null
                             select new MembershipUserLightModel() { Firstname = o.MembershipUser.FirstName, SecondName = o.MembershipUser.LastName, UserId = o.MembershipUser.UserId, isOnline = false, Thumbnail = o.MembershipUser.Picture, UserName = o.MembershipUser.UserName });
                //to do : get only distinct users
                count = query.Count();
                buyers = query.Skip(skip).Take(take).ToArray();
            }
            foreach (var buyer in buyers)
            {
                string imgSrc = Offtick.Web.DrOnlinerEnterpriseWeb.Infrustracture.GeneralHelper.GetThumbnail32Of(buyer.UserId, buyer.Thumbnail, false);
                if (System.IO.File.Exists(imgSrc))
                {
                    Image img = Image.FromFile(imgSrc);
                    using (MemoryStream mStream = new MemoryStream())
                    {
                        img.Save(mStream, img.RawFormat);
                        string base64 = Convert.ToBase64String(mStream.ToArray());
                        buyer.Thumbnail = base64;
                    }
                }
                else buyer.Thumbnail = "";

            }
            return Ok(new ServerResponse() { Status = ServerResponseStatus.OK, Data = new { Data = buyers, Count = count }, DataType = ServerResponseDataType.MembershipUserLightModelList.ToString() });
        }

        [Authorize(Roles = "Company, Resturant, Administrator, Tester, Developer")]
        [HttpPost]
        [Route("api/offer/fetchOfferCommenters")]
        public IHttpActionResult FetchOfferCommenters(MultiPageRequestModel model)
        {
            if (model == null)
                return BadRequest();

            //check if currentUser has administration or higher privilage then allow him to do else rejectByAccessDenied;
            var currentUser = MembershipManager.Instance.GetUser(User.Identity.Name);
            /* if (!model.Id.Equals(currentUser.UserId))
             {
                 if (currentUser.MembershipUsersInRoles.FirstOrDefault(e => e.MembershipRole.RoleName.Equals("Administrator") || e.MembershipRole.RoleName.Equals("Tester") || e.MembershipRole.RoleName.Equals("Developer")) == null)
                     return Ok(new ServerResponse() { Status = ServerResponseStatus.AccessDenied });
             }*/
            int take = model.Limit.HasValue ? model.Limit.Value : 20;
            int skip = 0;
            if (model.Page.HasValue)
            {
                skip = (model.Page.Value - 1) * take;
            }


            //check model.UserIsValid and existed
            var offer = Offtick.Business.Services.Managers.OfferManager.Instance.GetOffer(model.Id);
            if (offer == null)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferNotExist });

            //to do : check isShowLikers from OfferPrivacy
            // bool showLikers = offer.OfferPrivacy != null ? offer.OfferPrivacy.IsBuyerVisible : true;
            bool showLikers = true;
            MembershipUserLightModel[] buyers = null;
            int count = 0;
            if (showLikers)
            {
                //to do : get Online status from corresponding service
                var query = (from o in offer.OfferComments
                             where (o.MembershipUser.MembershipPrivacy != null && o.MembershipUser.MembershipPrivacy.IsBuyingVisible) || o.MembershipUser.MembershipPrivacy == null
                             select new MembershipUserLightModel() { Firstname = o.MembershipUser.FirstName, SecondName = o.MembershipUser.LastName, UserId = o.MembershipUser.UserId, isOnline = false, Thumbnail = o.MembershipUser.Picture, UserName = o.MembershipUser.UserName });
                //to do : get only distinct users
                count = query.Count();
                buyers = query.Skip(skip).Take(take).ToArray();
            }
            foreach (var buyer in buyers)
            {
                string imgSrc = Offtick.Web.DrOnlinerEnterpriseWeb.Infrustracture.GeneralHelper.GetThumbnail32Of(buyer.UserId, buyer.Thumbnail, false);
                if (System.IO.File.Exists(imgSrc))
                {
                    Image img = Image.FromFile(imgSrc);
                    using (MemoryStream mStream = new MemoryStream())
                    {
                        img.Save(mStream, img.RawFormat);
                        string base64 = Convert.ToBase64String(mStream.ToArray());
                        buyer.Thumbnail = base64;
                    }
                }
                else buyer.Thumbnail = "";

            }
            return Ok(new ServerResponse() { Status = ServerResponseStatus.OK, Data = new { Data = buyers, Count = count }, DataType = ServerResponseDataType.MembershipUserLightModelList.ToString() });
        }


        [Authorize(Roles = "Company, Resturant, Administrator, Tester, Developer")]
        [HttpPost]
        [Route("api/offer/postPhoto")]
        public IHttpActionResult PostPhoto()
        {
            string title = System.Web.HttpContext.Current.Request.Form.Get("title");
            if (string.IsNullOrWhiteSpace(title))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferTitleIsEmpty });

            string description = System.Web.HttpContext.Current.Request.Form.Get("description");
            if (string.IsNullOrWhiteSpace(description))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferDescriptionIsEmpty });

            var postImageFile = System.Web.HttpContext.Current.Request.Files.Get("postImage");
            if (postImageFile.ContentLength == 0)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferPostImageNotProvided });
            var postImage = new HttpPostedFileWrapper(postImageFile);

            ChatService chatService = new ChatService();
            var response = chatService.SRpcSendOffer(new Offtick.Business.Services.RealtimeSearvices.EventDrivenDispatcher.Notification.OfferNotification()
            {
                Title = title,
                Body = description,
                Datetime = DateTime.Now.ToString(),
                FromUserName = User.Identity.Name,
                ToUserName = User.Identity.Name,
                OfferType = (short)EnumOfferType.Post,
            });

            if (response.ResultStatus == "Success")
            {
                string newFileName = string.Empty;
                var responseEdit = OfferManager.Instance.UploadOfferFile(User.Identity.Name, response.DataObject.ToString(), postImage, out newFileName);
                if (responseEdit == Offtick.Core.EnumTypes.EditStatus.Edited)
                    return Ok(new ServerResponse() { Status = ServerResponseStatus.OK });
            }

            return Ok(new ServerResponse() { Status = ServerResponseStatus.ServerInternalError });
        }

        [Authorize(Roles = "Company, Resturant, Administrator, Tester, Developer")]
        [HttpPost]
        [Route("api/offer/updatePostPhoto")]
        public IHttpActionResult UpdatePostPhoto(UpdatePostPhotoModel model)
        {
            if (model == null) return BadRequest();
            if(string.IsNullOrWhiteSpace(model.Title))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferTitleIsEmpty });
            if (string.IsNullOrWhiteSpace(model.Description))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferDescriptionIsEmpty });

            var offer = Offtick.Business.Services.Managers.OfferManager.Instance.GetOffer(model.OfferId);
            if (offer == null)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferNotExist });
            var curentUser = MembershipManager.Instance.GetUser(User.Identity.Name);
            if(!offer.SenderUserId.Equals(curentUser.UserId) && !( User.IsInRole("Administrator") || User.IsInRole("Tester") || User.IsInRole("Developer")))
                    return Ok(new ServerResponse() { Status = ServerResponseStatus.AccessDenied });

            var responseEdit = OfferManager.Instance.UpdateOffer(model.OfferId, model.Title, model.Description);
            if (responseEdit == Offtick.Core.EnumTypes.EditStatus.Edited || responseEdit== EditStatus.RejectedByNonEffectiveQuery)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.OK });
            return Ok(new ServerResponse() { Status = ServerResponseStatus.ServerInternalError});


        }



        [Authorize(Roles = "Company, Resturant, Administrator, Tester, Developer")]
        [HttpPost]
        [Route("api/offer/postVideo")]
        public IHttpActionResult PostVideo()
        {
            string title = System.Web.HttpContext.Current.Request.Form.Get("title");
            if (string.IsNullOrWhiteSpace(title))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferTitleIsEmpty });

            string description = System.Web.HttpContext.Current.Request.Form.Get("description");
            if (string.IsNullOrWhiteSpace(description))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferDescriptionIsEmpty });

            var postImageFile = System.Web.HttpContext.Current.Request.Files.Get("postVideo");
            if (postImageFile.ContentLength == 0)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferPostImageNotProvided });
            var postImage = new HttpPostedFileWrapper(postImageFile);

            ChatService chatService = new ChatService();
            var response = chatService.SRpcSendOffer(new Offtick.Business.Services.RealtimeSearvices.EventDrivenDispatcher.Notification.OfferNotification()
            {
                Title = title,
                Body = description,
                Datetime = DateTime.Now.ToString(),
                FromUserName = User.Identity.Name,
                ToUserName = User.Identity.Name,
                OfferType = (short)EnumOfferType.Video,
            });

            if (response.ResultStatus == "Success")
            {
                string newFileName = string.Empty;
                var responseEdit = OfferManager.Instance.UploadOfferFile(User.Identity.Name, response.DataObject.ToString(), postImage, out newFileName);
                if (responseEdit == Offtick.Core.EnumTypes.EditStatus.Edited)
                    return Ok(new ServerResponse() { Status = ServerResponseStatus.OK });
            }

            return Ok(new ServerResponse() { Status = ServerResponseStatus.ServerInternalError });
        }


        [Authorize(Roles = "Company, Resturant, Administrator, Tester, Developer")]
        [HttpPost]
        [Route("api/offer/updatePostVideo")]
        public IHttpActionResult UpdatePostVideo(UpdatePostPhotoModel model)
        {
            if (model == null) return BadRequest();
            if (string.IsNullOrWhiteSpace(model.Title))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferTitleIsEmpty });
            if (string.IsNullOrWhiteSpace(model.Description))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferDescriptionIsEmpty });

            var offer = Offtick.Business.Services.Managers.OfferManager.Instance.GetOffer(model.OfferId);
            if (offer == null)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferNotExist });
            var curentUser = MembershipManager.Instance.GetUser(User.Identity.Name);
            if (!offer.SenderUserId.Equals(curentUser.UserId) && !(User.IsInRole("Administrator") || User.IsInRole("Tester") || User.IsInRole("Developer")))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.AccessDenied });

            var responseEdit = OfferManager.Instance.UpdateOffer(model.OfferId, model.Title, model.Description);
            if (responseEdit == Offtick.Core.EnumTypes.EditStatus.Edited || responseEdit == EditStatus.RejectedByNonEffectiveQuery)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.OK });
            return Ok(new ServerResponse() { Status = ServerResponseStatus.ServerInternalError });


        }


        [Authorize(Roles = "Company, Resturant, Administrator, Tester, Developer")]
        [HttpPost]
        [Route("api/offer/deleteOffer")]
        public IHttpActionResult DeleteOffer(Guid offerId)
        {
            var offer = OfferManager.Instance.GetOffer(offerId);
            if (offer == null)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferNotExist });

            var curentUser = MembershipManager.Instance.GetUser(User.Identity.Name);
            if (!offer.SenderUserId.Equals(curentUser.UserId) && !(User.IsInRole("Administrator") || User.IsInRole("Tester") || User.IsInRole("Developer")))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.AccessDenied });

            var actionStatus = OfferManager.Instance.DeleteOffer(User.Identity.Name, offerId);
            if (actionStatus == DeleteStatus.Deleted)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.OK });

            return Ok(new ServerResponse() { Status = ServerResponseStatus.AccessDenied });
        }

        [Authorize(Roles = "Company, Resturant, Administrator, Tester, Developer")]
        [HttpPost]
        [Route("api/offer/reportOffer")]
        public IHttpActionResult ReportOffer(Guid offerId,string reportText)
        {
            if(string.IsNullOrWhiteSpace(reportText))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferReportTextIsEmpty });

            var offer = OfferManager.Instance.GetOffer(offerId);
            if (offer == null)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferNotExist });

            var user = MembershipManager.Instance.GetUser(User.Identity.Name);
            var actionStatus = OfferManager.Instance.ReportOffer(offer, user, reportText);

            if (actionStatus == AddStatus.Added)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.OK });

            return Ok(new ServerResponse() { Status = ServerResponseStatus.AccessDenied });
        }


        [Authorize(Roles = "Company, Resturant, Administrator, Tester, Developer")]
        [HttpPost]
        [Route("api/offer/addOfferComment")]
        public IHttpActionResult AddOfferComment(Guid offerId, string comment)
        {
            if (string.IsNullOrWhiteSpace(comment))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferCommentTextIsEmpty });
            var offer = OfferManager.Instance.GetOffer(offerId);
            if (offer == null)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferNotExist });
            var user = MembershipManager.Instance.GetUser(User.Identity.Name);

            OfferComment reference = null;
            var addStatus = OfferManager.Instance.AddCommentForOffer(offer, user, comment, out reference);
            if (addStatus == AddStatus.Added)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.OK });
            return Ok(new ServerResponse() { Status = ServerResponseStatus.ServerInternalError });
        }


        [Authorize(Roles = "Company, Resturant, Administrator, Tester, Developer")]
        [HttpPost]
        [Route("api/offer/removeOfferComment")]
        public IHttpActionResult RemoveOfferComment(long offerCommentId)
        {
            var commentEntity = OfferManager.Instance.GetOfferCommentById(offerCommentId);
            if(commentEntity==null)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferCommentNotExist });
            var actionStatus = OfferManager.Instance.RemoveCommentForOffer(offerCommentId, User.Identity.Name);
            if (actionStatus == DeleteStatus.Deleted)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.OK });
            if (actionStatus == DeleteStatus.RejectedByAccessDenied)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.AccessDenied });
            return Ok(new ServerResponse() { Status = ServerResponseStatus.ServerInternalError });
        }


        [Authorize(Roles = "Company, Resturant, Administrator, Tester, Developer")]
        [HttpPost]
        [Route("api/offer/updateOfferComment")]
        public IHttpActionResult UpdateOfferComment(long offerCommentId,string comment)
        {
            if (string.IsNullOrWhiteSpace(comment))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferCommentTextIsEmpty });

            var commentEntity = OfferManager.Instance.GetOfferCommentById(offerCommentId);
            if (commentEntity == null)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferCommentNotExist });

            var currentUser = MembershipManager.Instance.GetUser(User.Identity.Name);

            if (!commentEntity.UserId.Equals(currentUser.UserId) && !(User.IsInRole("Administrator") || User.IsInRole("Tester") || User.IsInRole("Developer")))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.AccessDenied });


            var actionStatus = OfferManager.Instance.UpdateCommentForOffer(commentEntity, comment);
            if (actionStatus == EditStatus.Edited || actionStatus== EditStatus.RejectedByNonEffectiveQuery)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.OK });
            if (actionStatus ==EditStatus.RejectedByAccessDenied)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.AccessDenied });
            return Ok(new ServerResponse() { Status = ServerResponseStatus.ServerInternalError });
        }


        [Authorize(Roles = "Company, Resturant, Administrator, Tester, Developer")]
        [HttpPost]
        [Route("api/offer/fetchOfferComments")]
        public IHttpActionResult FetchOfferComments(MultiPageRequestModel model)
        {
            if (model == null)
                return BadRequest();
            
            int take = model.Limit.HasValue ? model.Limit.Value : 20;
            int skip = 0;
            if (model.Page.HasValue)
            {
                skip = (model.Page.Value - 1) * take;
            }

            var offer = OfferManager.Instance.GetOffer(model.Id);
            if (offer == null)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferNotExist });
            int count = offer.OfferComments.Count();
            var comments = offer.OfferComments.OrderByDescending(e => e.Datetime).Skip(skip).Take(take).Select(e => new CommentModel() {
                Comment=e.CommentText, Firstname=e.MembershipUser.FirstName, SecondName=e.MembershipUser.LastName, isOnline=false, UserId=e.MembershipUser.UserId,UserName=e.MembershipUser.UserName,Thumbnail=e.MembershipUser.Picture
            }).ToArray();

            foreach (var comment in comments)
            {
                string imgSrc = Offtick.Web.DrOnlinerEnterpriseWeb.Infrustracture.GeneralHelper.GetThumbnail32Of(comment.UserId, comment.Thumbnail, false);
                if (System.IO.File.Exists(imgSrc))
                {
                    Image img = Image.FromFile(imgSrc);
                    using (MemoryStream mStream = new MemoryStream())
                    {
                        img.Save(mStream, img.RawFormat);
                        string base64 = Convert.ToBase64String(mStream.ToArray());
                        comment.Thumbnail = base64;
                    }
                }
                else comment.Thumbnail = "";
            }

            return Ok(new ServerResponse() { Status = ServerResponseStatus.OK, Data = new { Data = comments, Count = count } ,DataType= ServerResponseDataType.OfferCommentList.ToString()});
        }



        [Authorize(Roles = "Company, Resturant, Administrator, Tester, Developer")]
        [HttpPost]
        [Route("api/offer/likeDislikeOffer")]
        public IHttpActionResult LikeDislikeOffer(Guid offerId)
        {
            var offer = OfferManager.Instance.GetOffer(offerId);
            if (offer == null)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferNotExist });

            var user = MembershipManager.Instance.GetUser(User.Identity.Name);
            long countOfLikes = OfferManager.Instance.LikeOrDislike(user, offer);
            return Ok(new ServerResponse() { Status = ServerResponseStatus.OK });
        }

        [Authorize(Roles = "Company, Resturant, Administrator, Tester, Developer")]
        [HttpPost]
        [Route("api/offer/isOfferLiked")]
        public IHttpActionResult IsOfferLiked(Guid offerId)
        {

            var offer = OfferManager.Instance.GetOffer(offerId);
            if (offer == null)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferNotExist });
            var user = MembershipManager.Instance.GetUser(User.Identity.Name);
            bool liked = offer.OfferLikes.FirstOrDefault(e => e.UserId.Equals(user.UserId))!=null;
            return Ok(new ServerResponse() { Status = ServerResponseStatus.OK ,Data=liked,DataType= ServerResponseDataType.Boolean.ToString()});
        }


        [Authorize(Roles = "Company, Resturant, Administrator, Tester, Developer")]
        [HttpPost]
        [Route("api/offer/fetchOfferLikes")]
        public IHttpActionResult FetchOfferLikes(MultiPageRequestModel model)
        {
            if (model == null)
                return BadRequest();

            int take = model.Limit.HasValue ? model.Limit.Value : 20;
            int skip = 0;
            if (model.Page.HasValue)
            {
                skip = (model.Page.Value - 1) * take;
            }

            var offer = OfferManager.Instance.GetOffer(model.Id);
            if (offer == null)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferNotExist });
            int count = offer.OfferLikes.Count();
            var comments = offer.OfferLikes.OrderByDescending(e => e.Datetime).Skip(skip).Take(take).Select(e => new MembershipUserLightModel()
            {
                Firstname = e.MembershipUser.FirstName,
                SecondName = e.MembershipUser.LastName,
                isOnline = false,
                UserId = e.MembershipUser.UserId,
                UserName = e.MembershipUser.UserName,
                Thumbnail = e.MembershipUser.Picture
            }).ToArray();

            foreach (var comment in comments)
            {
                string imgSrc = Offtick.Web.DrOnlinerEnterpriseWeb.Infrustracture.GeneralHelper.GetThumbnail32Of(comment.UserId, comment.Thumbnail, false);
                if (System.IO.File.Exists(imgSrc))
                {
                    Image img = Image.FromFile(imgSrc);
                    using (MemoryStream mStream = new MemoryStream())
                    {
                        img.Save(mStream, img.RawFormat);
                        string base64 = Convert.ToBase64String(mStream.ToArray());
                        comment.Thumbnail = base64;
                    }
                }
                else comment.Thumbnail = "";
            }

            return Ok(new ServerResponse() { Status = ServerResponseStatus.OK, Data = new { Data = comments, Count = count }, DataType = ServerResponseDataType.MembershipUserLightModelList.ToString() });
        }

       
        [HttpPost]
        [Route("api/offer/fetchMainPageOffers")]
        public IHttpActionResult FetchMainPageOffers(MultiPageRequestModel model)
        {
            if (model == null)
                return BadRequest();

            int take = model.Limit.HasValue ? model.Limit.Value : 20;
            int skip = 0;
            if (model.Page.HasValue)
            {
                skip = (model.Page.Value - 1) * take;
            }
            Guid? cityId = model.Id == default(Guid) ? null : (Guid?)model.Id;
            var user = MembershipManager.Instance.GetUser(User.Identity.Name);
            var offers = Offtick.Business.Services.Managers.OfferManager.Instance.getMainPageFoodOffers(cityId, user);
            int count = offers.Count();
            var query = offers.Skip(skip).Take(take);
            IList<object> offersDTO = new List<object>();
            foreach (var offer in query)
            {
                if(offer.OfferType==(short)EnumOfferType.Food)
                    offersDTO.Add(getDTOof(offer));
                if (offer.OfferType == (short)EnumOfferType.Tour)
                    offersDTO.Add(getTourDTOof(offer));
                if (offer.OfferType == (short)EnumOfferType.Post || offer.OfferType == (short)EnumOfferType.Video)
                    offersDTO.Add(getImageVideoDTOof(offer));
            }
            return Ok(new ServerResponse() { Status = ServerResponseStatus.OK, Data = new { Data = offersDTO, Count = count }, DataType = ServerResponseDataType.OfferDTOList.ToString() });
        }

        [HttpPost]
        [Route("api/offer/fetchSimularOffers")]
        public IHttpActionResult FetchSimularOffers(MultiPageRequestModel model)
        {
            if (model == null)
                return BadRequest();

            int take = model.Limit.HasValue ? model.Limit.Value : 20;
            int skip = 0;
            if (model.Page.HasValue)
            {
                skip = (model.Page.Value - 1) * take;
            }
            var offer = Offtick.Business.Services.Managers.OfferManager.Instance.GetOffer(model.Id);
            if (offer == null)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferNotExist });
             var offers = Offtick.Business.Services.Managers.OfferManager.Instance.getSimularOffers(offer);
            var user = MembershipManager.Instance.GetUser(User.Identity.Name);
            
            int count = offers.Count();
            var query = offers.Skip(skip).Take(take);
            IList<object> offersDTO = new List<object>();
            foreach (var o in query)
            {
                if (o.OfferType == (short)EnumOfferType.Food)
                {
                    var dto = getDTOof(o);
                    if(dto!=null)
                        offersDTO.Add(dto);
                }
                    
                if (o.OfferType == (short)EnumOfferType.Tour)
                {
                    var dto = getTourDTOof(o);
                    if(dto!=null)
                        offersDTO.Add(dto);
                }
                    
                if (o.OfferType == (short)EnumOfferType.Post || o.OfferType == (short)EnumOfferType.Video)
                    offersDTO.Add(getImageVideoDTOof(o));
            }
            return Ok(new ServerResponse() { Status = ServerResponseStatus.OK, Data = new { Data = offersDTO, Count = count }, DataType = ServerResponseDataType.OfferDTOList.ToString() });
        }

        [Authorize(Roles = "Company, Resturant, Administrator, Tester, Developer")]
        [HttpPost]
        [Route("api/offer/fetchWallOffers")]
        public IHttpActionResult FetchWallOffers(MultiPageRequestModel model)
        {
            if (model == null)
                return BadRequest();

            int take = model.Limit.HasValue ? model.Limit.Value : 20;
            int skip = 0;
            if (model.Page.HasValue)
            {
                skip = (model.Page.Value - 1) * take;
            }
            Guid? cityId = model.Id == default(Guid) ? null : (Guid?)model.Id;
            var user = MembershipManager.Instance.GetUser(User.Identity.Name);
            var offers = Offtick.Business.Services.Managers.OfferManager.Instance.getWallOffersForUser(user);
            int count = offers.Count();
            var query = offers.Skip(skip).Take(take);
            IList<object> offersDTO = new List<object>();
            foreach (var offer in query)
            {
                if (offer.OfferType == (short)EnumOfferType.Food)
                    offersDTO.Add(getDTOof(offer));
                if (offer.OfferType == (short)EnumOfferType.Tour)
                    offersDTO.Add(getTourDTOof(offer));
                if (offer.OfferType == (short)EnumOfferType.Post || offer.OfferType == (short)EnumOfferType.Video)
                    offersDTO.Add(getImageVideoDTOof(offer));
            }
            return Ok(new ServerResponse() { Status = ServerResponseStatus.OK, Data = new { Data = offersDTO, Count = count }, DataType = ServerResponseDataType.OfferDTOList.ToString() });
        }


        [Authorize(Roles = "Company, Resturant, Administrator, Tester, Developer")]
        [HttpPost]
        [Route("api/offer/fetchProfileOffers")]
        public IHttpActionResult FetchProfileOffers(MultiPageRequestModel model)
        {
            if (model == null)
                return BadRequest();

            int take = model.Limit.HasValue ? model.Limit.Value : 20;
            int skip = 0;
            if (model.Page.HasValue)
            {
                skip = (model.Page.Value - 1) * take;
            }
            
            var user = MembershipManager.Instance.GetUser(model.Id);
            if (user == null)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.UsernameNotExisted });
            var offers = Offtick.Business.Services.Managers.OfferManager.Instance.GetPublicOffersFor(user,  MembershipManager.Instance.GetUser(User.Identity.Name));

            int count = offers.Count();
            var query = offers.Skip(skip).Take(take);
            IList<object> offersDTO = new List<object>();
            foreach (var offer in query)
            {
                if (offer.OfferType == (short)EnumOfferType.Food)
                    offersDTO.Add(getDTOof(offer));
                if (offer.OfferType == (short)EnumOfferType.Tour)
                    offersDTO.Add(getTourDTOof(offer));
                if (offer.OfferType == (short)EnumOfferType.Post || offer.OfferType == (short)EnumOfferType.Video)
                    offersDTO.Add(getImageVideoDTOof(offer));
            }
            return Ok(new ServerResponse() { Status = ServerResponseStatus.OK, Data = new { Data = offersDTO, Count = count }, DataType = ServerResponseDataType.OfferDTOList.ToString() });
        }


        private OfferDTO getDTOof(Offer offer)
        {
           var dto = new OfferDTO();
            var o = offer.OfferFoods.FirstOrDefault();
            if (o == null)
                return null;
            var u = o.Offer.MembershipUser;
            dto.OwnerId = u.UserId;
            dto.OwnerFirstName = u.FirstName;
            dto.OwnerSecondName = u.LastName;
            dto.OwnerUserName = u.UserName;
            //offerDTO.Audeiences=o.
            dto.Conditions = o.Conditions;
            dto.Descriptions = o.Descriptions;
            dto.DiscountPercent = o.DiscountPercent;
            dto.DiscountPrice = o.DiscountPrice;
            dto.DiscountTitle = o.DiscountPriceTitle;
            dto.ExpireDate = o.TimeToExpire.HasValue ? o.TimeToExpire.Value : 0;
            foreach (var img in o.OfferFoodImages.Where(e => e.ImageType == (short)FoodOfferImageType.FoodImage))
                dto.GalleryImages.Add(img.OfferFoodImageId.ToString());
            dto.HasAddedValueTax = o.AddedValueTax.HasValue ? o.AddedValueTax.ToString() : "";
            dto.LocationAddress = offer.LocationAddress;
            dto.LocationCityId = offer.LocationCityId.HasValue ? offer.LocationCityId.Value.ToString() : "";
            dto.MainImage = offer.FileName;
            foreach (var img in o.OfferFoodImages.Where(e => e.ImageType == (short)FoodOfferImageType.FoodMenu))
                dto.MenuImages.Add(img.Name);
            dto.OfferId = offer.OfferId;
            dto.OfftickOfferTypeId = o.OfftickOfferTypeId;
            dto.OriginalPrice = o.OriginalPrice;
            dto.OriginalTitle = o.OriginalPriceTitle;
            dto.Quantity = o.Quantity;
            //dto.Remaining=o.Quantity-mainOffer.o
            dto.ShowIncomeInPercentage = o.ShowIncomeInPercentage.ToString();
            //dto.Sold
            dto.Specification = o.Specifications;
            dto.Title = offer.Title;
            dto.TitleLong = offer.Body;
            dto.Unit = o.DiscountPriceUnit;
            dto.Url = offer.Url;
            return dto;
        }

        private TourDTO getTourDTOof(Offer offer)
        {
            var dto = new TourDTO();
            var o = offer.OfferTours.FirstOrDefault();
            if (o == null)
                return null;
            var u = o.Offer.MembershipUser;
            dto.OwnerId = u.UserId;
            dto.OwnerFirstName = u.FirstName;
            dto.OwnerSecondName = u.LastName;
            dto.OwnerUserName = u.UserName;
            
            dto.Conditions = o.Conditions;
            dto.Descriptions = o.Descriptions;
            dto.DiscountPercent = o.DiscountPercent;
            dto.DiscountPrice = o.DiscountPrice;
            dto.DiscountTitle = o.DiscountPriceTitle;
            dto.ExpireDate = o.TimeToExpire.HasValue ? o.TimeToExpire.Value : 0;
            foreach (var img in o.OfferTourImages.Where(e => e.ImageType == (short)FoodOfferImageType.FoodImage))
                dto.GalleryImages.Add(img.OfferTourImageId.ToString());
            dto.HasAddedValueTax = o.AddedValueTax.HasValue ? o.AddedValueTax.ToString() : "";
            dto.LocationAddress = offer.LocationAddress;
            dto.LocationCityId = offer.LocationCityId.HasValue ? offer.LocationCityId.Value.ToString() : "";
            dto.MainImage = offer.FileName;
            foreach (var img in o.OfferTourImages.Where(e => e.ImageType == (short)FoodOfferImageType.FoodMenu))
                dto.MenuImages.Add(img.Name);
            dto.OfferId = offer.OfferId;
            //dto.OfftickOfferTypeId = o.OfftickOfferTypeId;
            dto.OriginalPrice = o.OriginalPrice.HasValue?o.OriginalPrice.Value:0;
            dto.OriginalTitle = o.OriginalPriceTitle;
            dto.Quantity = o.Quantity;
            //dto.Remaining=o.Quantity-mainOffer.o
            dto.ShowIncomeInPercentage = o.ShowIncomeInPercentage.ToString();
            //dto.Sold
            dto.Specification = o.Specifications;
            dto.Title = offer.Title;
            dto.TitleLong = offer.Body;
            dto.Unit = o.DiscountPriceUnit;
            dto.Url = offer.Url;

            dto.ShowBuyButton = o.ShowOffButton;
            dto.ShowOffButton = o.ShowOffButton;
            dto.ShowInstantButton = o.ShowInstantButton;
            dto.SourceCityId = o.SourceCityId;
            dto.SourceCity = o.SourceCityId.HasValue ? o.MembershipCity.CityName : string.Empty;
            dto.DestinationCityId = o.DestinationCityId;
            dto.DestinationCity = o.DestinationCityId.HasValue ? o.MembershipCity1.CityName : string.Empty;
            dto.GoDateTime = o.WentDateTime;
            dto.ReturnDateTime = o.ReturnDateTime;
            dto.AgancyName = o.AgancyName;
            dto.DurationTime = o.DurationTime;
            dto.TravelTypeId = o.TravelTypeId;
            dto.TravelType = o.TravelType.Name;

            return dto;
        }
        private  ImageVideoDTO getImageVideoDTOof(Offer offer)
        {
            var dto = new ImageVideoDTO();
            
            var u = offer.MembershipUser;
            dto.OwnerId = u.UserId;
            dto.OwnerFirstName = u.FirstName;
            dto.OwnerSecondName = u.LastName;
            dto.OwnerUserName = u.UserName;
            dto.Title = offer.Title;
            dto.Description = offer.Body;
            dto.FileName = offer.FileName;

            return dto;
        }


        private OfferOrderDTO getDTOof(OfferFoodOrder order)
        {
            OfferOrderDTO dto = new OfferOrderDTO();
            dto.DateTime = order.DateTime;
            dto.Description = order.Description;
            dto.PaymentAmount = order.PaymentAmount.HasValue?order.PaymentAmount.Value:0;
            dto.Quantity = order.Quantity;
            dto.OfferOrderId = order.SaleOrderId;
            dto.SaleRefrenceId = order.SaleRefrenceId;
            dto.BuyerUserId = order.UserId;
            foreach (var detail in order.OfferFoodOrderDetails)
                dto.Details.Add(new OfferOrderDetailDTO()
                {
                    FileName = detail.FileName,
                    OfferCode=detail.OrderCode,
                     UseStatus=detail.Status
                }) ;
            
            return dto;
        }


    }
}
