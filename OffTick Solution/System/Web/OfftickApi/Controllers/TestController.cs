﻿using Offtick.Business.Services.Storage;
using Offtick.Business.Web;
using Offtick.Web.Api.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;

namespace Offtick.Web.Api.Controllers
{
    //public class TestController : ApiController
    public class TestController : WepApiBaseController
    {
        /* private  readonly IDataContextContainer dct;
         public TestController(IDataContextContainer obj)
         {
             dct = obj;
             DataContextManager.SetDataContextContainer((DataContextContainer)dct);
         }*/
        public TestController(IDataContextContainer obj):base(obj)
        {          
        }
    

        //This resource is For all types of role
        [Authorize(Roles = "SuperAdmin, Admin, User")]
        [HttpGet]
        [Route("api/test/resource1")]
        public IHttpActionResult GetResource1()
        {
            
            var identity = (ClaimsIdentity)User.Identity;
            var email = identity.Claims.FirstOrDefault(e => e.Type == "Email").Value;
         //   var clientName = identity.Claims.FirstOrDefault(e => e.Type == "ClientName").Value;
          //  var clientSecret = identity.Claims.FirstOrDefault(e => e.Type == "ClientSecret").Value;
            return Ok("Hello: " + identity.Name+ ", email:" + email);
        }



        //This resource is only For Admin and SuperAdmin role
        [Authorize(Roles = "SuperAdmin, Admin")]
        [HttpPost]
        [Route("api/test/revokeRefreshToken")]
        public IHttpActionResult RevokeRefreshToken()
        {
            string userName = "admin";
            var identity = (ClaimsIdentity)User.Identity;
            var rereshToken=Offtick.Business.Services.Managers.AuthenticationManager.Instance.FindRefreshToken(userName, "client1");
            Offtick.Business.Services.Managers.AuthenticationManager.Instance.RemoveRefreshToken(rereshToken);
                //ar.FindRefreshToken(userName, "client1"));

            return Ok("revoke refreshtoken for " + userName + ",by :" + identity.Name);
        }
        //This resource is only For SuperAdmin role
        [Authorize(Roles = "SuperAdmin")]
        [HttpGet]
        [Route("api/test/resource3")]
        public IHttpActionResult GetResource3()
        {
            var identity = (ClaimsIdentity)User.Identity;
            var roles = identity.Claims
                        .Where(c => c.Type == ClaimTypes.Role)
                        .Select(c => c.Value);
            return Ok("Hello " + identity.Name + "Your Role(s) are: " + string.Join(",", roles.ToList()));
        }
    }
}
