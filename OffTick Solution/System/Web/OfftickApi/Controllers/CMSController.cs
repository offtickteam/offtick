﻿using Offtick.Business.Membership;
using Offtick.Business.Services.Managers;
using Offtick.Business.Services.Storage;
using Offtick.Business.Web;
using Offtick.Core.EnumTypes;
using Offtick.Web.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Offtick.Web.Api.Controllers
{
    public class CMSController : WepApiBaseController
    {
        public CMSController(IDataContextContainer obj) : base(obj)
        {
        }

        [Authorize(Roles = "Personal, Company, Resturant, Administrator, Tester, Developer")]
        [HttpPost]
        [Route("api/cms/addMenu")]
        public IHttpActionResult addMenu(Offtick.Data.Entities.Common.Models.MenuModel menu)
        {
            if (menu == null) return BadRequest();
            if(String.IsNullOrEmpty(menu.Title))
                return  Ok( new ServerResponse() {  Status= Models.ServerResponseStatus.InvalidParameters, ErrorCode= Models.ErrorCode.MenuTitleIsEmpty});
            if(menu.MenuType<0 ||menu.MenuType>2)
                return Ok(new ServerResponse() { Status = Models.ServerResponseStatus.InvalidParameters, ErrorCode = Models.ErrorCode.MenuTypeIsInvalid });

            var user = MembershipManager.Instance.GetUser(menu.UserId);
            if(user==null)
                return Ok(new ServerResponse() { Status = Models.ServerResponseStatus.InvalidParameters, ErrorCode = Models.ErrorCode.UsernameNotExisted });

            var curentUser = MembershipManager.Instance.GetUser(User.Identity.Name);
            if (menu.ParentId.HasValue)
            {
                var parentMenu=MenuBuilder.Instance.GetMenu(menu.ParentId.Value);
                if(parentMenu==null)
                    return Ok(new ServerResponse() { Status = Models.ServerResponseStatus.InvalidParameters, ErrorCode = Models.ErrorCode.MenuParentNotExisted });

                if (!parentMenu.UserId.Equals(curentUser.UserId) && !(User.IsInRole("Administrator") || User.IsInRole("Tester") || User.IsInRole("Developer")))
                    return Ok(new ServerResponse() { Status = ServerResponseStatus.AccessDenied });
            }
            if (!menu.UserId.Equals(curentUser.UserId) && !(User.IsInRole("Administrator") || User.IsInRole("Tester") || User.IsInRole("Developer")))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.AccessDenied });


            AddStatus status = MenuBuilder.Instance.AddMenu(menu.ParentId,
              LangaugeManager.Instance.GetDefaultLangaugeId(),
              menu.Title, (MenuType)menu.MenuType, menu.Url, menu.IconUrl, menu.IsSystemMenu,string.Empty,menu.UserId);
            if(status== AddStatus.Added)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.OK });
            return Ok(new ServerResponse() { Status = ServerResponseStatus.ServerInternalError });
        }


        [Authorize(Roles = "Personal, Company, Resturant, Administrator, Tester, Developer")]
        [HttpPost]
        [Route("api/cms/UpdateMenu")]
        public IHttpActionResult UpdateMenu(Guid menuId,string title)
        {
            
            if (String.IsNullOrEmpty(title))
                return Ok(new ServerResponse() { Status = Models.ServerResponseStatus.InvalidParameters, ErrorCode = Models.ErrorCode.MenuTitleIsEmpty });
            var menu = MenuBuilder.Instance.GetMenu(menuId);
            if (menu == null)
                return Ok(new ServerResponse() { Status = Models.ServerResponseStatus.InvalidParameters, ErrorCode = Models.ErrorCode.MenuNotExisted });

            var curentUser = MembershipManager.Instance.GetUser(User.Identity.Name);
            if (!menu.UserId.Equals(curentUser.UserId) && !(User.IsInRole("Administrator") || User.IsInRole("Tester") || User.IsInRole("Developer")))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.AccessDenied });

            EditStatus status = MenuBuilder.Instance.EditMenuTitle(menuId, title);
            if(status== EditStatus.Edited || status == EditStatus.RejectedByNonEffectiveQuery)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.OK });
            return Ok(new ServerResponse() { Status = ServerResponseStatus.ServerInternalError });

        }

        [Authorize(Roles = "Personal, Company, Resturant, Administrator, Tester, Developer")]
        [HttpPost]
        [Route("api/cms/deleteMenu")]
        public IHttpActionResult DeleteMenu(Guid menuId)
        {
            var menu = MenuBuilder.Instance.GetMenu(menuId);
            if (menu == null)
                return Ok(new ServerResponse() { Status = Models.ServerResponseStatus.InvalidParameters, ErrorCode = Models.ErrorCode.MenuNotExisted });

            var curentUser = MembershipManager.Instance.GetUser(User.Identity.Name);
            if (!menu.UserId.Equals(curentUser.UserId) && !(User.IsInRole("Administrator") || User.IsInRole("Tester") || User.IsInRole("Developer")))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.AccessDenied });

            DeleteStatus status = MenuBuilder.Instance.DeleteMenu(menuId);
            if(status== DeleteStatus.Deleted)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.OK });
            else if (status == DeleteStatus.RejectedByExistSubItems)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.AccessDenied, ErrorCode= ErrorCode.CandidateMenuToDeleteHasSubItem });
            return Ok(new ServerResponse() { Status = ServerResponseStatus.ServerInternalError });
        }
        

        [Authorize(Roles = "Personal, Company, Resturant, Administrator, Tester, Developer")]
        [HttpPost]
        [Route("api/cms/addMenuContent")]
        public IHttpActionResult AddMenuContent(ManageContentOfMenu model)
        {
            if (model == null) return BadRequest();

            var menu = MenuBuilder.Instance.GetMenu(model.MenuId);
            if (menu == null)
                return Ok(new ServerResponse() { Status = Models.ServerResponseStatus.InvalidParameters, ErrorCode = Models.ErrorCode.MenuNotExisted });

            var curentUser = MembershipManager.Instance.GetUser(User.Identity.Name);
            if (!menu.UserId.Equals(curentUser.UserId) && !(User.IsInRole("Administrator") || User.IsInRole("Tester") || User.IsInRole("Developer")))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.AccessDenied });

            /*if(!model.LanguageId.HasValue)
                return Ok(new ServerResponse() { Status = Models.ServerResponseStatus.InvalidParameters, ErrorCode = Models.ErrorCode.LanguageIdEmpty });
            var language = LangaugeManager.Instance.GetLangauge(model.LanguageId.Value);
            if (language==null)
                return Ok(new ServerResponse() { Status = Models.ServerResponseStatus.InvalidParameters, ErrorCode = Models.ErrorCode.LanguageNotExisted });*/

            if(string.IsNullOrEmpty(model.Content))
                return Ok(new ServerResponse() { Status = Models.ServerResponseStatus.InvalidParameters, ErrorCode = Models.ErrorCode.MenuContentIsEmpty });

            AddStatus status = MenuBuilder.Instance.AddMenuContent(model.MenuId, model.LanguageId.Value, model.Content);
            if (status == AddStatus.Added)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.OK });
            return Ok(new ServerResponse() { Status = ServerResponseStatus.ServerInternalError });

        }


        [Authorize(Roles = "Personal, Company, Resturant, Administrator, Tester, Developer")]
        [HttpPost]
        [Route("api/cms/updateMenuContent")]
        public IHttpActionResult UpdateMenuContent(ManageContentOfMenu model)
        {
            if (model == null) return BadRequest();

            var menu = MenuBuilder.Instance.GetMenu(model.MenuId);
            if (menu == null)
                return Ok(new ServerResponse() { Status = Models.ServerResponseStatus.InvalidParameters, ErrorCode = Models.ErrorCode.MenuNotExisted });

            var curentUser = MembershipManager.Instance.GetUser(User.Identity.Name);
            if (!menu.UserId.Equals(curentUser.UserId) && !(User.IsInRole("Administrator") || User.IsInRole("Tester") || User.IsInRole("Developer")))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.AccessDenied });
            
           /* if (!model.LanguageId.HasValue)
                return Ok(new ServerResponse() { Status = Models.ServerResponseStatus.InvalidParameters, ErrorCode = Models.ErrorCode.LanguageIdEmpty });
            var language = LangaugeManager.Instance.GetLangauge(model.LanguageId.Value);
            if (language == null)
                return Ok(new ServerResponse() { Status = Models.ServerResponseStatus.InvalidParameters, ErrorCode = Models.ErrorCode.LanguageNotExisted });*/


            if (string.IsNullOrEmpty(model.Content))
                return Ok(new ServerResponse() { Status = Models.ServerResponseStatus.InvalidParameters, ErrorCode = Models.ErrorCode.MenuContentIsEmpty });

            EditStatus status = MenuBuilder.Instance.EditMenuContentByMenuId(model.MenuId,0,model.Content);
            if (status == EditStatus.Edited || status == EditStatus.RejectedByNonEffectiveQuery)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.OK });
            return Ok(new ServerResponse() { Status = ServerResponseStatus.ServerInternalError });

        }


        [Authorize(Roles = "Personal, Company, Resturant, Administrator, Tester, Developer")]
        [HttpPost]
        [Route("api/cms/fetchMenus")]
        public IHttpActionResult FetchMenus(Guid userId)
        {

            var candidateUser = MembershipManager.Instance.GetUser(userId);
            if (candidateUser==null)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode= ErrorCode.UsernameNotExisted });

            var curentUser = MembershipManager.Instance.GetUser(User.Identity.Name);
            if (!userId.Equals(curentUser.UserId) && !(User.IsInRole("Administrator") || User.IsInRole("Tester") || User.IsInRole("Developer")))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.AccessDenied });

            var menus=candidateUser.Menus.Select(e=> new Offtick.Data.Entities.Common.Models.MenuModel {  IconUrl=e.IconUrl,  LastDateTime=e.LastDateTime, MenuId=e.MenuId, MenuOrder=e.MenuOrder, MenuType=e.MenuType, ParentId=e.ParentId, Title=e.Title, Url=e.Url, UserId=e.UserId}).ToList();
            return Ok(new ServerResponse() { Status = ServerResponseStatus.OK, Data=menus, DataType= ServerResponseDataType.MenuList.ToString() });
        }


        [Authorize(Roles = "Personal, Company, Resturant, Administrator, Tester, Developer")]
        [HttpPost]
        [Route("api/cms/fetchMenuTypes")]
        public IHttpActionResult FetchMenuTypes()
        {
            Dictionary<int, string> dic = new Dictionary<int, string>();
            foreach (MenuType type in Enum.GetValues(typeof(MenuType)))
            {
                dic.Add((int)type, type.ToString());
            }
            return Ok(new ServerResponse() { Status = ServerResponseStatus.OK, Data = dic, DataType = ServerResponseDataType.Dictionary.ToString() });
        }
        
    }
}