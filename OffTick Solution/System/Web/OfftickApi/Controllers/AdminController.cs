﻿using Offtick.Business.Membership;
using Offtick.Business.Services.Domain;
using Offtick.Business.Services.Managers;
using Offtick.Business.Services.RealtimeSearvices;
using Offtick.Business.Services.RealtimeSearvices.EventDrivenDispatcher.Notification;
using Offtick.Business.Services.Storage;
using Offtick.Business.Web;
using Offtick.Core.EnumTypes;
using Offtick.Core.Utility;
using Offtick.Core.Utility.PersianTools;
using Offtick.Data.Entities.Common;
using Offtick.Web.Api.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Offtick.Web.Api.Controllers
{
    public class AdminController : WepApiBaseController
    {
        public AdminController(IDataContextContainer obj) : base(obj)
        {
            //SqlServerTypes.Utilities.LoadNativeAssemblies(Server.MapPath("~"));
        }

        [Authorize(Roles = "Administrator")]
        [HttpPost]
        [Route("api/admin/sendMagfaSMS")]
        public IHttpActionResult SendMagfaSMS(string receipt, string textMessage)
        {
            if (string.IsNullOrWhiteSpace(receipt))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.MagfaReceptInvalidReceipt });
            if (string.IsNullOrWhiteSpace(textMessage))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.MagfaTextMessageInvalidSize });

            var configManager = ConfigFactory.GetConfigManager(ConfigManagerType.WebConfig);
            var smsMagfaEndpint = configManager.ReadSetting("smsMagfaEndpint");
            var smsMagfaSender = configManager.ReadSetting("smsMagfaSender");
            var smsMagfaUserName = configManager.ReadSetting("smsMagfaUserName");
            var smsMagfaPassword = configManager.ReadSetting("smsMagfaPassword");
            var smsMagfaDomain = configManager.ReadSetting("smsMagfaDomain");

            long result = SmsService.Instance.SendMagfaSms(smsMagfaEndpint, smsMagfaDomain, smsMagfaUserName, smsMagfaPassword, smsMagfaSender, receipt, textMessage);
            if (result > 120)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.OK, ErrorCode = ErrorCode.None });

            string errorMessage = Offtick.Business.SmsService.SmsErrorCodes.GetErrorCode(result);
            string messageToClient = string.Format("ErrorCode:{0} means: {1}:", result, errorMessage);
            return Ok(new ServerResponse() { Status = ServerResponseStatus.ServerInternalError, Error = messageToClient });

        }


        [Authorize(Roles = "Administrator")]
        [HttpPost]
        [Route("api/admin/resetPasswordByAdmin")]
        public IHttpActionResult ResetPasswordByAdmin(string userNameToReset)
        {
            string adminUserName = MembershipManager.Instance.GetCurrentUserName();
            var checkUser = MembershipManager.Instance.GetUser(userNameToReset);
            if (checkUser == null)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.UsernameNotExisted });


            PasswordGenerator.Password pass = new PasswordGenerator.Password(true, true, true, false, 8, 3);
            string password = pass.Next();
            var editStatus = MembershipManager.Instance.ResetPassword(checkUser, password);
            if (editStatus == Offtick.Core.EnumTypes.EditStatus.Edited)
            {
                string msg = string.Format("پسورد کاربر {0} به مقدار {1} تغییر پیدا کرد", checkUser.UserName, password);

                ChatService chatService = new ChatService();
                var chatServiceResponse = chatService.SRpcSendMessage(new SendMessageNotification()
                {
                    FromUserName = adminUserName,
                    Message = msg,
                    ToUserName = adminUserName,
                    Flags = 1,
                    MessageType = "0",
                    Datetime = DateTime.Now.ToString(),
                    DatetimeFa = PersianDateConvertor.ToKhorshidiDateTime(DateTime.Now, false),
                });
                return Ok(new ServerResponse() { Status = ServerResponseStatus.OK, ErrorCode = ErrorCode.None, Message = msg });
            }
            return Ok(new ServerResponse() { Status = ServerResponseStatus.ServerInternalError });

        }


        [Authorize(Roles = "Administrator")]
        [HttpPost]
        [Route("api/admin/suspendUser")]
        public IHttpActionResult SuspendUser(string userName)
        {
            if (string.IsNullOrWhiteSpace(userName))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.UsernameEmpty });
            var checkUser = MembershipManager.Instance.GetUser(userName);
            if (checkUser == null)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.UsernameNotExisted });
            var editStatus = MembershipManager.Instance.ChangeStatus(checkUser, MembershipStatus.InActive);
            if (editStatus == Offtick.Core.EnumTypes.EditStatus.Edited || editStatus == Offtick.Core.EnumTypes.EditStatus.RejectedByNonEffectiveQuery)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.OK });
            return Ok(new ServerResponse() { Status = ServerResponseStatus.ServerInternalError });
        }


        [Authorize(Roles = "Administrator")]
        [HttpPost]
        [Route("api/admin/activeUser")]
        public IHttpActionResult ActiveUser(string userName)
        {
            if (string.IsNullOrWhiteSpace(userName))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.UsernameEmpty });
            var checkUser = MembershipManager.Instance.GetUser(userName);
            if (checkUser == null)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.UsernameNotExisted });
            var editStatus = MembershipManager.Instance.ChangeStatus(checkUser, MembershipStatus.Active);
            if (editStatus == Offtick.Core.EnumTypes.EditStatus.Edited || editStatus == Offtick.Core.EnumTypes.EditStatus.RejectedByNonEffectiveQuery)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.OK });
            return Ok(new ServerResponse() { Status = ServerResponseStatus.ServerInternalError });
        }


        [Authorize(Roles = "Administrator")]
        [HttpPost]
        [Route("api/admin/fetchUsers")]
        public IHttpActionResult FetchUsers(LoadUserModel model)
        {
            if (model == null)
                return BadRequest();
            int take = model.Limit.HasValue ? model.Limit.Value : 20;
            int skip = 0;
            if (model.Page.HasValue)
            {
                skip = (model.Page.Value - 1) * take;
            }
            var users = MembershipManager.Instance.GetUsersByCriteria(model.Name, model.UserName, model.Email, model.MBTI, model.DateFrom, model.DateTo, model.PhoneNumber, model.SuspendedUser);
            int total = users.Count();
            if (!string.IsNullOrEmpty(model.SortBy) && !string.IsNullOrEmpty(model.Direction))
            {
                if (model.Direction.Trim() == "asc")
                {
                    switch (model.SortBy)
                    {
                        case "FirstName":
                            users = users.OrderBy(q => q.FirstName);
                            break;
                        case "LastName":
                            users = users.OrderBy(q => q.LastName);
                            break;
                        case "UserName":
                            users = users.OrderBy(q => q.UserName);
                            break;
                        case "RegisterDateTime":
                            users = users.OrderBy(q => q.RegisterDateTime);
                            break;
                        case "Email":
                            users = users.OrderBy(q => q.Email);
                            break;
                        case "MBTI":
                            users = users.OrderBy(q => q.ApplicationMBTIResults.Select(e => e.Result));
                            break;
                        case "PhoneNumber":
                            users = users.OrderBy(q => q.PhoneNumber);
                            break;
                    }
                }
                else
                {
                    switch (model.SortBy)
                    {
                        case "FirstName":
                            users = users.OrderByDescending(q => q.FirstName);
                            break;
                        case "LastName":
                            users = users.OrderByDescending(q => q.LastName);
                            break;
                        case "UserName":
                            users = users.OrderByDescending(q => q.UserName);
                            break;
                        case "RegisterDateTime":
                            users = users.OrderByDescending(q => q.RegisterDateTime);
                            break;
                        case "Email":
                            users = users.OrderByDescending(q => q.Email);
                            break;
                        case "MBTI":
                            users = users.OrderByDescending(q => q.ApplicationMBTIResults.Select(e => e.Result));
                            break;
                        case "PhoneNumber":
                            users = users.OrderByDescending(q => q.PhoneNumber);
                            break;
                    }
                }
            }
            var records = users.Skip(skip).Take(take).ToList().Select(e => new
            {
                UserId = e.UserId,
                FirstName = e.FirstName,
                LastName = e.LastName,
                UserName = e.UserName,
                RegisterDateTime = e.RegisterDateTime.HasValue ? e.RegisterDateTime.Value.ToString("yyyy/MM/dd HH:mm:ss") : string.Empty,
                Email = e.Email,
                MBTI = e.ApplicationMBTIResults.Select(m => m.Result),
                PhoneNumber = e.PhoneNumber,
                Status = e.MembershipUserStatus.FirstOrDefault().MembershipStatu.StatusModel,
            }).ToArray();
            return Ok(new ServerResponse() { Status = ServerResponseStatus.OK, DataType = ServerResponseDataType.FetchUserForAdmin.ToString(), Data = new { records, total } });
        }


        [Authorize(Roles = "Administrator")]
        [HttpPost]
        [Route("api/admin/sendMessageToUser")]
        public IHttpActionResult SendMessageToUser(SendMessageModel model)
        {
            if (model == null || string.IsNullOrWhiteSpace(model.ReceiverUserName))
                return (Ok(new ServerResponse { Status = ServerResponseStatus.InvalidParameters }));
            if (string.IsNullOrEmpty(model.MessageBody))
                return (Ok(new ServerResponse { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.SendMessageBodyEmpty }));

            var receiverUser = MembershipManager.Instance.GetUser(model.ReceiverUserName);
            if (receiverUser == null)
                return (Ok(new ServerResponse { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.UsernameNotExisted }));

            if (string.IsNullOrWhiteSpace(model.MethodType) || !new ArrayList() { "SMS", "EMAIL", "DOMAINMESSAGE" }.Contains(model.MethodType.ToUpper()))
                return (Ok(new ServerResponse { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.SendMessageInvalidType }));
            switch (model.MethodType.ToUpper())
            {
                case "SMS":
                    if (string.IsNullOrEmpty(receiverUser.PhoneNumber))
                        return Ok(new ServerResponse { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.PhonenumberNotExisted });
                    else
                    {
                        var response = SmsService.Instance.SendMagfaSms(receiverUser.PhoneNumber, model.MessageBody);
                        break;
                    }
                case "EMAIL":
                    if (string.IsNullOrEmpty(receiverUser.Email))
                        return Ok(new ServerResponse { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.EmailNotExisted });
                    else
                    {
                        SMTPService mailService = new SMTPService();
                        mailService.SendMail(receiverUser.Email, "Offtick", model.MessageBody);
                        break;
                    }
                case "DOMAINMESSAGE":
                    ChatService chatService = new ChatService();
                    var chatServiceResponse = chatService.SRpcSendMessage(new SendMessageNotification()
                    {
                        FromUserName = this.User.Identity.Name,
                        Message = model.MessageBody,
                        ToUserName = model.ReceiverUserName,
                        Flags = 1,
                        MessageType = "0",
                        Datetime = DateTime.Now.ToString(),
                        DatetimeFa = PersianDateConvertor.ToKhorshidiDateTime(DateTime.Now, false),
                    });
                    return Ok(new ServerResponse { Status = ServerResponseStatus.OK, Data = chatServiceResponse });
            }
            return Ok(new ServerResponse { Status = ServerResponseStatus.OK });
        }

        [Authorize(Roles = "Administrator")]
        [HttpPost]
        [Route("api/admin/confirmProfileStatus")]
        public IHttpActionResult ConfirmProfileStatus(Guid userId)
        {
            var checkUser = MembershipManager.Instance.GetUser(userId);
            if (checkUser == null)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.UsernameNotExisted });
            EditStatus editStatus;
            editStatus = MembershipManager.Instance.ChangeProfileStatus(checkUser, ConfirmStatus.Confirmed);
            if (editStatus == Offtick.Core.EnumTypes.EditStatus.Edited || editStatus == Offtick.Core.EnumTypes.EditStatus.RejectedByNonEffectiveQuery)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.OK });
            return Ok(new ServerResponse() { Status = ServerResponseStatus.ServerInternalError });
        }

        [Authorize(Roles = "Administrator")]
        [HttpPost]
        [Route("api/admin/notConfirmProfileStatus")]
        public IHttpActionResult NotConfirmProfileStatus(Guid userId)
        {
            var checkUser = MembershipManager.Instance.GetUser(userId);
            if (checkUser == null)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.UsernameNotExisted });
            EditStatus editStatus;
            editStatus = MembershipManager.Instance.ChangeProfileStatus(checkUser, ConfirmStatus.NotConfirmed);
            if (editStatus == Offtick.Core.EnumTypes.EditStatus.Edited || editStatus == Offtick.Core.EnumTypes.EditStatus.RejectedByNonEffectiveQuery)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.OK });
            return Ok(new ServerResponse() { Status = ServerResponseStatus.ServerInternalError });
        }


        [Authorize(Roles = "Administrator")]
        [HttpPost]
        [Route("api/admin/confirmToPublishOnMainPage")]
        public IHttpActionResult ConfirmPublishOnMainPage(Guid userId)
        {
            var checkUser = MembershipManager.Instance.GetUser(userId);
            if (checkUser == null)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.UsernameNotExisted });
            EditStatus editStatus;
            editStatus = MembershipManager.Instance.ChangePublishOnMainPageStatus(checkUser, ConfirmStatus.Confirmed);
            if (editStatus == Offtick.Core.EnumTypes.EditStatus.Edited || editStatus == Offtick.Core.EnumTypes.EditStatus.RejectedByNonEffectiveQuery)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.OK });
            return Ok(new ServerResponse() { Status = ServerResponseStatus.ServerInternalError });
        }


        [Authorize(Roles = "Administrator")]
        [HttpPost]
        [Route("api/admin/notConfirmToPublishOnMainPage")]
        public IHttpActionResult NotConfirmToPublishOnMainPage(Guid userId)
        {
            var checkUser = MembershipManager.Instance.GetUser(userId);
            if (checkUser == null)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.UsernameNotExisted });
            EditStatus editStatus;
            editStatus = MembershipManager.Instance.ChangePublishOnMainPageStatus(checkUser, ConfirmStatus.NotConfirmed);
            if (editStatus == Offtick.Core.EnumTypes.EditStatus.Edited || editStatus == Offtick.Core.EnumTypes.EditStatus.RejectedByNonEffectiveQuery)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.OK });
            return Ok(new ServerResponse() { Status = ServerResponseStatus.ServerInternalError });
        }


        [Authorize(Roles = "Administrator")]
        [HttpPost]
        [Route("api/admin/fetchSites")]
        public IHttpActionResult FetchSites(FetchSiteModel model)
        {
            if (model == null)
                return BadRequest();
            int take = model.Limit.HasValue ? model.Limit.Value : 20;
            int skip = 0;
            if (model.Page.HasValue)
                skip = (model.Page.Value - 1) * take;


            var users = MembershipManager.Instance.GetUsersProfileByCriteria(model.Name, model.UserName, model.Email, model.DateFrom, model.DateTo, model.PhoneNumber, model.SuspendedSite, model.PermitToDistributeOnMainSite);
            int total = users.Count();
            if (!string.IsNullOrEmpty(model.SortBy) && !string.IsNullOrEmpty(model.Direction))
            {
                if (model.Direction.Trim() == "asc")
                {
                    switch (model.SortBy)
                    {
                        case "FirstName":
                            users = users.OrderBy(q => q.FirstName);
                            break;
                        case "LastName":
                            users = users.OrderBy(q => q.LastName);
                            break;
                        case "UserName":
                            users = users.OrderBy(q => q.UserName);
                            break;
                        case "RegisterDateTime":
                            users = users.OrderBy(q => q.RegisterDateTime);
                            break;
                        case "Email":
                            users = users.OrderBy(q => q.Email);
                            break;
                        case "MBTI":
                            users = users.OrderBy(q => q.ApplicationMBTIResults.Select(e => e.Result));
                            break;
                        case "PhoneNumber":
                            users = users.OrderBy(q => q.PhoneNumber);
                            break;
                    }
                }
                else
                {
                    switch (model.SortBy)
                    {
                        case "FirstName":
                            users = users.OrderByDescending(q => q.FirstName);
                            break;
                        case "LastName":
                            users = users.OrderByDescending(q => q.LastName);
                            break;
                        case "UserName":
                            users = users.OrderByDescending(q => q.UserName);
                            break;
                        case "RegisterDateTime":
                            users = users.OrderByDescending(q => q.RegisterDateTime);
                            break;
                        case "Email":
                            users = users.OrderByDescending(q => q.Email);
                            break;
                        case "MBTI":
                            users = users.OrderByDescending(q => q.ApplicationMBTIResults.Select(e => e.Result));
                            break;
                        case "PhoneNumber":
                            users = users.OrderByDescending(q => q.PhoneNumber);
                            break;
                    }
                }
            }

            var records = users.Skip(skip).Take(take).ToList().Select(e => new
            {
                UserId = e.UserId,
                FirstName = e.FirstName,
                LastName = e.LastName,
                UserName = e.UserName,
                RegisterDateTime = e.RegisterDateTime.HasValue ? e.RegisterDateTime.Value.ToString("yyyy/MM/dd HH:mm:ss") : string.Empty,
                Email = e.Email,
                MBTI = e.ApplicationMBTIResults.Select(m => m.Result),
                PhoneNumber = e.PhoneNumber,
                Status = e.MembershipUserStatus.FirstOrDefault().MembershipStatu.StatusModel,
                ProfileStatus = e.ProfileSetting != null ? e.ProfileSetting.ConfirmStatu.Name : ConfirmStatus.NotDecide.ToString(),
                MainPagePublishStatus = e.ProfileSetting != null ? e.ProfileSetting.ConfirmStatu2.Name : ConfirmStatus.NotDecide.ToString(),



            }).ToArray();
            // return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "search offers", offers.ToArray(), ActionResponseFromServerToClient.OffersServerToClientKey));
            return Ok(new ServerResponse() { Status = ServerResponseStatus.OK, DataType = ServerResponseDataType.FetchSiteForAdmin.ToString(), Data = new { records, total } });
        }


        [Authorize(Roles = "Administrator")]
        [HttpPost]
        [Route("api/admin/confirmOffer")]
        public IHttpActionResult ConfirmOffer(Guid offerId)
        {
            var offer = OfferManager.Instance.GetOffer(offerId);
            if (offer == null)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferNotExist });
            EditStatus editStatus = OfferManager.Instance.ConfirmOffer(offer);
            if (editStatus == Offtick.Core.EnumTypes.EditStatus.Edited || editStatus == Offtick.Core.EnumTypes.EditStatus.RejectedByNonEffectiveQuery)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.OK });
            return Ok(new ServerResponse() { Status = ServerResponseStatus.ServerInternalError });
        }


        [Authorize(Roles = "Administrator")]
        [HttpPost]
        [Route("api/admin/notConfirmOffer")]
        public IHttpActionResult NotConfirmOffer(Guid offerId)
        {
            var offer = OfferManager.Instance.GetOffer(offerId);
            if (offer == null)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferNotExist });
            EditStatus editStatus = OfferManager.Instance.NotConfirmOffer(offer);
            if (editStatus == Offtick.Core.EnumTypes.EditStatus.Edited || editStatus == Offtick.Core.EnumTypes.EditStatus.RejectedByNonEffectiveQuery)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.OK });
            return Ok(new ServerResponse() { Status = ServerResponseStatus.ServerInternalError });
        }



        [Authorize(Roles = "Administrator")]
        [HttpPost]
        [Route("api/admin/suspendOffer")]
        public IHttpActionResult SuspendOffer(Guid offerId)
        {
            var offer = OfferManager.Instance.GetOffer(offerId);
            if (offer == null)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferNotExist });

            var foodOffer = offer.OfferFoods.FirstOrDefault();
            if (foodOffer == null)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.AccessDenied, ErrorCode = ErrorCode.OfferNotAllowdedToChangeStatus });
            EditStatus editStatus;
            if (foodOffer.OfferStatusId == (short)OfferExecutionStatus.Running)
                editStatus = OfferManager.Instance.ChangeStatusForOffer(foodOffer, OfferExecutionStatus.Suspended);
            //else if (foodOffer.OfferStatusId == (short)OfferExecutionStatus.Suspended)
            //  editStatus = OfferManager.Instance.ChangeStatusForOffer(foodOffer, OfferExecutionStatus.Running);
            else
                return Ok(new ServerResponse() { Status = ServerResponseStatus.AccessDenied, ErrorCode = ErrorCode.OfferChangeStatusNotAcceptable });
            if (editStatus == Offtick.Core.EnumTypes.EditStatus.Edited || editStatus == Offtick.Core.EnumTypes.EditStatus.RejectedByNonEffectiveQuery)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.OK });
            return Ok(new ServerResponse() { Status = ServerResponseStatus.ServerInternalError });
        }
        [Authorize(Roles = "Administrator")]
        [HttpPost]
        [Route("api/admin/runOffer")]
        public IHttpActionResult RunOffer(Guid offerId)
        {
            var offer = OfferManager.Instance.GetOffer(offerId);
            if (offer == null)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.OfferNotExist });
            var foodOffer = offer.OfferFoods.FirstOrDefault();
            if (foodOffer == null)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.AccessDenied, ErrorCode = ErrorCode.OfferNotAllowdedToChangeStatus });

            EditStatus editStatus;
            if (foodOffer.OfferStatusId == (short)OfferExecutionStatus.Suspended)
                editStatus = OfferManager.Instance.ChangeStatusForOffer(foodOffer, OfferExecutionStatus.Running);
            else
                return Ok(new ServerResponse() { Status = ServerResponseStatus.AccessDenied, ErrorCode = ErrorCode.OfferChangeStatusNotAcceptable });
            if (editStatus == Offtick.Core.EnumTypes.EditStatus.Edited || editStatus == Offtick.Core.EnumTypes.EditStatus.RejectedByNonEffectiveQuery)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.OK });
            return Ok(new ServerResponse() { Status = ServerResponseStatus.ServerInternalError });
        }



        [Authorize(Roles = "Administrator")]
        [HttpPost]
        [Route("api/admin/fetchOffers")]
        public IHttpActionResult FetchOffers(FetchOfferModel model)
        {
            Console.Write(model.OfferType + ", " + model.RoleType + ", " + model.ConfirmStatus + ", " + model.IsOnlyReported);
            int take = model.Limit.HasValue ? model.Limit.Value : 20;
            int skip = 0;
            if (model.Page.HasValue)
            {
                skip = (model.Page.Value - 1) * take;
                //records = query.Skip(start).Take(limit.Value).ToList();
            }

            Nullable<EnumOfferType> offerTypeEnum = !model.OfferType.HasValue ? (EnumOfferType?)null : (EnumOfferType)model.OfferType;
            Nullable<RoleType> roleTypeEnum = !model.RoleType.HasValue ? (RoleType?)null : (RoleType)model.RoleType;
            Nullable<ConfirmStatus> offerStatusEnum = !model.ConfirmStatus.HasValue ? (ConfirmStatus?)null : (ConfirmStatus)model.ConfirmStatus;
            var offers = OfferManager.Instance.getOffersByCriteria(!string.IsNullOrEmpty(model.SearchStr) ? model.SearchStr.Split(' ') : null, offerTypeEnum, roleTypeEnum, offerStatusEnum, model.IsOnlyReported);
            int total = offers.Count();
            if (!string.IsNullOrEmpty(model.SortBy) && !string.IsNullOrEmpty(model.Direction))
            {
                if (model.Direction.Trim() == "asc")
                {
                    switch (model.SortBy)
                    {
                        case "Title":
                            offers = offers.OrderBy(q => q.Title);
                            break;
                        case "Owner":
                            offers = offers.OrderBy(q => q.MembershipUser.UserName);
                            break;
                        case "DateTime":
                            offers = offers.OrderBy(q => q.DateTime);
                            break;
                        case "ConfrimDateTime":
                            offers = offers.OrderBy(q => q.ConfirmDateTime);
                            break;
                        case "IsConfirm":
                            offers = offers.OrderBy(q => q.ConfirmStatusId);
                            break;
                        case "Status":
                            offers = offers.OrderBy(q => q.OfferFoods.Select(e => e.OfferStatusId));
                            break;
                    }
                }
                else
                {
                    switch (model.SortBy)
                    {
                        case "Title":
                            offers = offers.OrderByDescending(q => q.Title);
                            break;
                        case "Owner":
                            offers = offers.OrderByDescending(q => q.MembershipUser.UserName);
                            break;
                        case "DateTime":
                            offers = offers.OrderByDescending(q => q.DateTime);
                            break;
                        case "ConfrimDateTime":
                            offers = offers.OrderByDescending(q => q.ConfirmDateTime);
                            break;
                        case "IsConfirm":
                            offers = offers.OrderByDescending(q => q.ConfirmStatusId);
                            break;
                        case "Status":
                            offers = offers.OrderByDescending(q => q.OfferFoods.Select(e => e.OfferStatusId));
                            break;
                    }
                }
            }

            var records = offers.Skip(skip).Take(take).ToList().Select(e => new
            {
                Body = e.Body,
                OfferId = e.OfferId,
                Title = e.Title,
                UserId = e.SenderUserId,
                UserName = e.MembershipUser.UserName,
                UserDisplayName = e.MembershipUser.FirstName,
                DateTime = e.DateTime.ToString("yyyy/MM/dd HH:mm:ss"),
                ConfirmDateTime = e.ConfirmDateTime.HasValue ? e.ConfirmDateTime.Value.ToString("yyyy/MM/dd HH:mm:ss") : string.Empty,
                HasReport = e.OfferReports.FirstOrDefault() != null,
                IsConfirm = e.ConfirmStatu.Name,
                Status = e.OfferFoods.FirstOrDefault() != null ? e.OfferFoods.FirstOrDefault().OfferStatu.Name : string.Empty,
            }).ToArray();
            // return Json(new ActionResponseFromServerToClient(ActionResponseFromServerToClientResultStatusEnum.Success, string.Empty, "search offers", offers.ToArray(), ActionResponseFromServerToClient.OffersServerToClientKey));
            return Ok(new ServerResponse() { Status = ServerResponseStatus.OK, DataType = ServerResponseDataType.FetchOfferForAdmin.ToString(), Data = new { records, total } });
        }

        [Authorize(Roles = "Administrator")]
        [HttpPost]
        [Route("api/admin/FetchLanguages")]
        public IHttpActionResult FetchLanguages()
        {
            var records = LangaugeManager.Instance.GetLangauges().Select(e => new
            {
                LanguageId = e.LanguageId,
                TitleEnglish = e.TitleEnglish,
                TitleLocal = e.TitleLocal,
                Caluture = e.Caluture
            });
            return Ok(new ServerResponse() { Status = ServerResponseStatus.OK, DataType = ServerResponseDataType.Languages.ToString(), Data = records });
        }


        [Authorize(Roles = "Administrator")]
        [HttpPost]
        [Route("api/admin/addLanguage")]
        public IHttpActionResult AddLanguage(LanguageModel model)
        {
            if (model == null)
                return BadRequest();
            if (!model.LanguageId.HasValue)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.LanguageIdEmpty });
            if (string.IsNullOrWhiteSpace(model.Caluture))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.LanguageCaltureEmpty });
            if (string.IsNullOrWhiteSpace(model.TitleEnglish))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.LanguageEnglishTitleEmpty });
            if (string.IsNullOrWhiteSpace(model.TitleLocal))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.LanguageLocalTitleEmpty });

            var existedById = LangaugeManager.Instance.GetLangaugeById(model.LanguageId.Value);
            if (existedById != null)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.LanguageExistedBefore });

            var existedByValue = LangaugeManager.Instance.GetLangauge(model.TitleEnglish, model.TitleLocal, model.Caluture);
            if (existedByValue != null)
            {
                if (existedByValue.Caluture.ToUpper().Equals(model.Caluture.ToUpper()))
                    return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.LanguageCaltureDuplicate });
                if (existedByValue.TitleEnglish.ToUpper().Equals(model.TitleEnglish.ToUpper()))
                    return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.LanguageEnglishTitleDuplicate });
                if (existedByValue.TitleLocal.ToUpper().Equals(model.TitleLocal.ToUpper()))
                    return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.LanguageLocalTitleDuplicate });
            }



            var addStatus = LangaugeManager.Instance.AddLangauge(model.LanguageId.Value, model.TitleEnglish, model.TitleLocal, model.Caluture);
            if (addStatus == AddStatus.Added || addStatus == AddStatus.RejectedByNonEffectiveQuery)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.OK });
            return Ok(new ServerResponse() { Status = ServerResponseStatus.ServerInternalError });
        }


        [Authorize(Roles = "Administrator")]
        [HttpPost]
        [Route("api/admin/updateLanguage")]
        public IHttpActionResult UpdateLanguage(LanguageModel model)
        {
            if (model == null)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.LanguageIdEmpty });

            if (!model.LanguageId.HasValue)
                return BadRequest();

            if (string.IsNullOrWhiteSpace(model.Caluture))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.LanguageCaltureEmpty });
            if (string.IsNullOrWhiteSpace(model.TitleEnglish))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.LanguageEnglishTitleEmpty });
            if (string.IsNullOrWhiteSpace(model.TitleLocal))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.LanguageLocalTitleEmpty });
            var existed = LangaugeManager.Instance.GetLangaugeById(model.LanguageId.Value);
            if (existed == null)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.LanguageNotExisted });

            var anotherExistedByValue = LangaugeManager.Instance.GetLangauge(model.LanguageId, model.TitleEnglish, model.TitleLocal, model.Caluture);
            if (anotherExistedByValue != null)
            {
                if (anotherExistedByValue.Caluture.ToUpper().Equals(model.Caluture.ToUpper()))
                    return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.LanguageCaltureDuplicate });
                if (anotherExistedByValue.TitleEnglish.ToUpper().Equals(model.TitleEnglish.ToUpper()))
                    return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.LanguageEnglishTitleDuplicate });
                if (anotherExistedByValue.TitleLocal.ToUpper().Equals(model.TitleLocal.ToUpper()))
                    return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.LanguageLocalTitleDuplicate });
            }

            var editStatus = LangaugeManager.Instance.UpdateLangauge(model.LanguageId.Value, model.TitleEnglish, model.TitleLocal, model.Caluture);
            if (editStatus == EditStatus.Edited || editStatus == EditStatus.RejectedByNonEffectiveQuery)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.OK });
            return Ok(new ServerResponse() { Status = ServerResponseStatus.ServerInternalError });
        }

        [Authorize(Roles = "Administrator")]
        [HttpPost]
        [Route("api/admin/fetchErrorCodes")]
        public IHttpActionResult FetchErrorCodes()
        {
            Dictionary<int, string> dic = new Dictionary<int, string>();
            foreach (ErrorCode errorCode in Enum.GetValues(typeof(ErrorCode)))
            {
                dic.Add((int)errorCode, errorCode.ToString());
            }
            return Ok(new ServerResponse() { Status = ServerResponseStatus.OK, Data = dic, DataType = ServerResponseDataType.Dictionary.ToString() });
        }

        [Authorize(Roles = "Administrator")]
        [HttpPost]
        [Route("api/admin/addMultiLanguageErrorCode")]
        public IHttpActionResult AddMultiLanguageErrorCode(MultiLanguageErrorCodeModel model)
        {
            if (model == null) return BadRequest();
            if (string.IsNullOrWhiteSpace(model.Meaning))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.MultiLanguageMeaningEmpty });
            if (string.IsNullOrWhiteSpace(model.ErrorCode))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.MultiLanguageErrorCodeEmpty });
            if (string.IsNullOrWhiteSpace(model.Package))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.MultiLanguagePackeEmpty });
            var existedLanguage = LangaugeManager.Instance.GetLangaugeById(model.LanguageId);
            if (existedLanguage == null)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.LanguageNotExisted });

            var existedByValue = LangaugeManager.Instance.GetMultiLanguageErrorCode(model.LanguageId, model.ErrorCode, model.Package);
            if (existedByValue != null)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.MultiLanguageErrorCodeDuplicate });

            var addStatus = LangaugeManager.Instance.AddMultiLanguageErrorCode(model.LanguageId, model.ErrorCode, model.Meaning, model.Package);
            if (addStatus == AddStatus.Added || addStatus == AddStatus.RejectedByNonEffectiveQuery)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.OK });
            return Ok(new ServerResponse() { Status = ServerResponseStatus.ServerInternalError });
        }

        [Authorize(Roles = "Administrator")]
        [HttpPost]
        [Route("api/admin/updateMultiLanguageErrorCode")]
        public IHttpActionResult UpdateMultiLanguageErrorCode(MultiLanguageErrorCodeModel model)
        {
            if (model == null) return BadRequest();
            if (!model.MultiLanguageId.HasValue)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.MultiLanguageIdEmpty });
            if (string.IsNullOrWhiteSpace(model.Meaning))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.MultiLanguageMeaningEmpty });
            if (string.IsNullOrWhiteSpace(model.ErrorCode))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.MultiLanguageErrorCodeEmpty });
            if (string.IsNullOrWhiteSpace(model.Package))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.MultiLanguagePackeEmpty });
            var existedLanguage = LangaugeManager.Instance.GetLangaugeById(model.LanguageId);
            if (existedLanguage == null)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.LanguageNotExisted });

            var editStatus = LangaugeManager.Instance.UpdateMultiLanguageErrorCode(model.MultiLanguageId.Value, model.LanguageId, model.ErrorCode, model.Meaning, model.Package);
            if (editStatus == EditStatus.Edited || editStatus == EditStatus.RejectedByNonEffectiveQuery)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.OK });
            if (editStatus == EditStatus.RejectedByNotExit)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.MultiLanguageNotExist });
            return Ok(new ServerResponse() { Status = ServerResponseStatus.ServerInternalError });
        }


        [Authorize(Roles = "Administrator")]
        [HttpPost]
        [Route("api/admin/fetchMultiLanguageErrorCodes")]
        public IHttpActionResult FetchMultiLanguageErrorCodes()
        {
            var records = LangaugeManager.Instance.GetMultiLanguageErrorCodes().Select(e => new
            {
                e.MultiLanguageId,
                e.LanguageId,
                e.Meaning,
                e.ErrorCode,
                e.Package
            });
            return Ok(new ServerResponse() { Status = ServerResponseStatus.OK, DataType = ServerResponseDataType.MultiLanguageErrorCodes.ToString(), Data = records });
        }


        [Authorize(Roles = "Administrator")]
        [HttpPost]
        [Route("api/admin/fetchClients")]
        public IHttpActionResult FetchClients()
        {
            var records = Offtick.Business.Services.Managers.AuthenticationManager.Instance.getAllOfClients().Select(e => new
            {
                e.Active,
                e.AllowedOrigin,
                e.ClientId,
                e.ClientName,
                e.ClientSecret,
                e.CreatedOn,
                e.RefreshTokenLifeTime
            });
            return Ok(new ServerResponse() { Status = ServerResponseStatus.OK, DataType = ServerResponseDataType.Clients.ToString(), Data = records });
        }

        [Authorize(Roles = "Administrator")]
        [HttpPost]
        [Route("api/admin/addClient")]
        public IHttpActionResult AddClient(ClientModel model)
        {
            if (model == null) return BadRequest();
            if (string.IsNullOrWhiteSpace( model.ClientName))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.ClientNameEmpty });
            if (string.IsNullOrWhiteSpace(model.ClientSecret))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.ClientSecretEmpty });
            if (model.RefreshTokenLifeTime<10)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.RefreshTokeLifeTimeInvalidValue });
            var existedBefore = Offtick.Business.Services.Managers.AuthenticationManager.Instance.GetMembershipClient(model.ClientName);
            if(existedBefore!=null)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.ClientNameDuplicate });

            model.AllowedOrigin = string.IsNullOrWhiteSpace(model.AllowedOrigin) ? "*" : model.AllowedOrigin;
            var addStatus=Offtick.Business.Services.Managers.AuthenticationManager.Instance.AddMembershipClient(model.ClientName, model.ClientSecret, model.Active, model.RefreshTokenLifeTime, model.AllowedOrigin);
            if (addStatus == AddStatus.Added || addStatus == AddStatus.RejectedByNonEffectiveQuery)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.OK });
            return Ok(new ServerResponse() { Status = ServerResponseStatus.ServerInternalError });
        }


        [Authorize(Roles = "Administrator")]
        [HttpPost]
        [Route("api/admin/updateClient")]
        public IHttpActionResult UpdateClient(ClientModel model)
        {
            if (model == null) return BadRequest();
            if(!model.ClientId.HasValue)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.ClientIdEmpty });
            if (string.IsNullOrWhiteSpace(model.ClientName))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.ClientNameEmpty });
            if (string.IsNullOrWhiteSpace(model.ClientSecret))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.ClientSecretEmpty });
            if (model.RefreshTokenLifeTime < 10)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.RefreshTokeLifeTimeInvalidValue });

            var existed= Offtick.Business.Services.Managers.AuthenticationManager.Instance.GetMembershipClient(model.ClientId.Value);
            if(existed==null)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.ClientNotExist });
            var existedBeforeByValue = Offtick.Business.Services.Managers.AuthenticationManager.Instance.GetMembershipClient(model.ClientId.Value, model.ClientName);
            if (existedBeforeByValue != null)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.ClientNameDuplicate });

            model.AllowedOrigin = string.IsNullOrWhiteSpace(model.AllowedOrigin) ? "*" : model.AllowedOrigin;
            var editStatus = Offtick.Business.Services.Managers.AuthenticationManager.Instance.EditMembershipClient(model.ClientId.Value, model.ClientName, model.ClientSecret, model.Active, model.RefreshTokenLifeTime, model.AllowedOrigin);
            if (editStatus == EditStatus.Edited || editStatus == EditStatus.RejectedByNonEffectiveQuery)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.OK });
            return Ok(new ServerResponse() { Status = ServerResponseStatus.ServerInternalError });
        }


        [Authorize(Roles = "Administrator")]
        [HttpPost]
        [Route("api/admin/fetchRefreshTokens")]
        public IHttpActionResult FetchRefreshTokens()
        {
            var records = Offtick.Business.Services.Managers.AuthenticationManager.Instance.GetRefreshTokens().Select(e => new
            {
                e.RefreshTokenId,
                e.ClientId,
                e.ExpiredTime,
                e.IssuedTime,
                e.MembershipClient.ClientName,
                e.MembershipUser.UserName,
            });
            return Ok(new ServerResponse() { Status = ServerResponseStatus.OK, DataType = ServerResponseDataType.RefreshTokens.ToString(), Data = records });
        }

        [Authorize(Roles = "Administrator")]
        [HttpPost]
        [Route("api/admin/revokeRefreshToken")]
        public IHttpActionResult RevokeRefreshToken(RevokeRefreshTokenModel model)
        {
            if (model == null) return BadRequest();
            if (string.IsNullOrWhiteSpace(model.UserName))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.UsernameEmpty });
            if (string.IsNullOrWhiteSpace(model.ClientName))
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.ClientNameEmpty });

            var refreshToken = Offtick.Business.Services.Managers.AuthenticationManager.Instance.FindRefreshToken(model.UserName, model.ClientName);
            if (refreshToken == null)
                return Ok(new ServerResponse() { Status = ServerResponseStatus.InvalidParameters, ErrorCode = ErrorCode.RefreshTokenNotExisted });
            Offtick.Business.Services.Managers.AuthenticationManager.Instance.RemoveRefreshToken(refreshToken);
            return Ok(new ServerResponse() { Status = ServerResponseStatus.OK });
        }
    }
}
